﻿$.snl.ir.controls.chart = function (configObj) {
    var self = this;
    var chart;
    var config;
    var chartType
    var chartdata;
    var chartConfig;
    var chartEvents;
    var initialSeries = [], drilldownSeries = [], seriesNames = [], yAxis = [];
    var maxpoints = 0;
    var chartElementCnfg;
    var xAxisType = "category";
    var height;
    var width;
    var xAxisTypeFormat;
    var pointStart;
    var pointInterval;
    var stacking = "normal";
    var isDrillDown = false;
    var marginRight = 35;
    var currentCulture = window.snl.ir.globalVars.dividendPageViewModel.BaseViewModel.CurrentCulture;

    function init(configObj) {
        var loadStockScript = configObj.loadStockScript ? configObj.loadStockScript : false;

        var configuration = $.snl.ir.GetConfiguration("Chart.xml", configObj.configUrlQueryArgs).Chart;

        $.preferCulture(currentCulture);

        chartDiv = configObj.chartDiv;
        chartConfig = configObj.chartConfig;
        chartType = configObj.chartType;
        chartdata = configObj.data;
        xAxisType = (configObj.xAxisType) ? configObj.xAxisType : xAxisType;
        height = configObj.height;
        width = configObj.width;
        xAxisTypeFormat = configObj.xAxisTypeFormat;
        pointStart = configObj.pointStart;
        pointInterval = configObj.pointInterval;
        chartEvents = configObj.events;

        //To disable stacking use 0
        stacking = (configObj.stacking == 0) ? null : stacking;

        //to avoid fixed marginRight assigned to chart
        marginRight = (configObj.marginRight == 0) ? null : marginRight;
       
        $.each(chartdata.Series, function (index, item) {
            data = [];
            
            $.each(item.Values, function (i, value) {
                var point = {};

                if (item.IsDrillDown) {
                    data[i] = [value.Key, value.Value];
                }
                else {
                    if (i == 0 && value.Value == 0) {
                        point.y = null;
                    }
                    else {
                        point.key = value.Key;
                        point.y = value.Value;
                        point.name = value.Name;
                    }
                    if (chartdata.EnableDrillDown) {
                        point.drilldown = value.Drilldown;
                        point.name = value.Name;
                    }

                    if (value.TransparentColumn) {
                        var rgbcolor = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(item.Color.trim());
                        var r = rgbcolor? parseInt(rgbcolor[1], 16) : 0;
                        var g = rgbcolor ? parseInt(rgbcolor[2], 16): 0;
                        var b = rgbcolor ? parseInt(rgbcolor[3], 16): 0;        
                        point.color = 'rgba(' + r + ',' + g + ',' + b + ',0.30)';
                    }
                    data.push(point);
                }
            });

            maxpoints = Math.max(maxpoints, data.length);
            
            seriesNames.push(item.Name);
            
            var s = {
                name: item.Name,
                id: item.Id,
                data: data,
                color: item.Color,
                showInLegend: item.ShowInLegend,
                type: item.Type,
                yAxis: item.YAxisIndex 
            };

            if (item.IsDrillDown) {
                drilldownSeries.push(s);
            }
            else {
                initialSeries.push(s);
            }
        });

        $.each(chartdata.YAxis, function (index, item) {
            yAxis.push({
                title:
                    {
                        text: item.Title,
                        style: {
                            fontWeight: 'normal',
                            backgroundColor: '#fff'
                        }
                    },
                labels:
                    {
                        format: (configObj.pointValueFormat ? configObj.pointValueFormat : '{value:,.2f}'),
                        formatter: function () { return $.format(this.value,"n2"); }
                    },
                opposite: item.IsOpposite,
                showEmpty: false //To hide y-axis label if we hide series by clicking on legend name
            });

            
        });

        if (loadStockScript) {
            $.when($.getScript($.snl.ir.vars.siteURL + 'Scripts/Highcharts/js/highcharts.js').fail(handleError))
            .done(function () {
                $.when(
                    $.getScript($.snl.ir.vars.siteURL + 'Scripts/Highcharts/js/modules/drilldown.js').fail(handleError),
                    $.getScript($.snl.ir.vars.siteURL + 'Scripts/Highcharts/js/modules/exporting.js').fail(handleError)
                ).then(
                    function () {
                        setupChart(configuration);
                    },
                    handleError
                );
                });
        }
        else {
            setupChart(configuration);
        }
    }

    function handleError(jqxhr, settings, exception) {
        $.snl.ir.Log(exception);
    }

    function setupChart(configuration) {
        var defaultOptions = Highcharts.setOptions({});
        
        $(chartDiv).highcharts({
            lang: {
                drillUpText: '◁'
            },
            credits: {
                text: 'S&P Global Market Intelligence',
                href: 'http://www.spglobal.com/marketintelligence',
                enabled: configuration.DisableCredits && configuration.DisableCredits.toLowerCase() == 'true' ? false : true
            },
            drilldown: {
                drillUpButton: {
                    relativeTo: 'spacingBox',
                    position: {
                        y: 2,
                        x: 3
                    }
                }
            },
            chart: {
                type: chartType,
                height: height,
                width: width,
                marginRight: marginRight,
                backgroundColor: configuration.BackgroundColor ? configuration.BackgroundColor : chartdata.BackgroundColor,
                borderColor: configuration.BorderColor ? configuration.BorderColor : null,
                borderWidth: configuration.BorderWidth ? configuration.BorderWidth : null,
                borderRadius: configuration.BorderRadius ? configuration.BorderRadius : null,
                events: {
                    load: function (event) {
                        this.credits.element.onclick = function () {
                            window.open('http://www.snl.com', '_blank');
                        }
                        self.chart = this;
                        
                        if (chartEvents && chartEvents.loadCallBack)
                            chartEvents.loadCallBack()
                    },
                    drilldown: function (event) {
                        self.chart.xAxis[0].options.tickInterval = chartdata.XAxis.DrilldownTickInterval;
                        isDrillDown = true;
                    },
                    drillup: function (event) {
                        self.chart.xAxis[0].options.tickInterval = chartdata.XAxis.TickInterval;
                        isDrillDown = false;
                    }
                }
            },
            subtitle: {
                text: configuration.SubTitle ? configuration.SubTitle : null
            },
            title: {
                text: configuration.Title ? configuration.Title : chartdata.Title
            },
            tooltip: {
                enabled: configuration.DisableTooltip && configuration.DisableTooltip.toLowerCase() == 'true' ? false : true,
                borderColor: configuration.TooltipBorderColor ? configuration.TooltipBorderColor : (chartConfig && chartConfig.toolTip && chartConfig.toolTip.borderColor ? chartConfig.toolTip.borderColor : null),
                backgroundColor: configuration.TooltipBackgroundColor ? configuration.TooltipBackgroundColor : null,
                borderRadius: configuration.TooltipBorderRadius ? configuration.TooltipBorderRadius : null,
                useHTML: configuration.UseHTML ? configuration.UseHTML : (chartConfig && chartConfig.toolTip && chartConfig.toolTip.useHTML ? chartConfig.toolTip.useHTML : false),
                style: configuration.TooltipStyle ? configuration.TooltipStyle : (chartConfig && chartConfig.toolTip && chartConfig.toolTip.style ? chartConfig.toolTip.style : defaultOptions.tooltip.style),
                formatter: function () {
                    var toolTip;
                    var pointName;
                    var pointValueFormatted;
                    var decimalPlaces;

                    pointName = chartdata.XAxis.Categories ? this.point.key : this.point.name;
                    
                    if (chartConfig && chartConfig.toolTip && chartConfig.toolTip.pointValueDecimalPlaces) {
                        decimalPlaces = chartConfig.toolTip.pointValueDecimalPlaces();
                    } else {
                        decimalPlaces = (this.point.y.toString().indexOf(".") > 0 ? 2 : 0);
                    }

                    pointValueFormatted = $.format(this.point.y,"n"+decimalPlaces)
                    pointValueFormatted = pointValueFormatted.indexOf("-") == 0 ? "<span style=\"color:red;\">" + (currentCulture.toLowerCase() == "en-us" ?"("+ pointValueFormatted.replace("-", "")+")" : pointValueFormatted) + "</span>" : pointValueFormatted;
                    
                    toolTip = configuration.TooltipHeader;
                    toolTip += configuration.TooltipBody;
                    
                    toolTip = toolTip.replace("{pointValueFormatted}", pointValueFormatted);
                    toolTip = toolTip.replace("{pointName}", pointName);

                    toolTip = toolTip.replace("{this.series.name}", this.series.name);
                    toolTip = toolTip.replace("{this.point.name}",  this.point.name);
                    toolTip = toolTip.replace("{this.point.y}", this.point.y);

                    return toolTip;
                }
            },
            xAxis: {
                type: xAxisType,
                title: {
                    text: chartdata.XAxis.Title,
                    style: {
                        fontWeight:  'normal'
                    }
                },
                tickInterval: (chartdata.XAxis.TickInterval > 0) ? chartdata.XAxis.TickInterval : null,
                labels: {
                    "format": xAxisTypeFormat ?  "{value:" + xAxisTypeFormat + "}" : ""
                },
                categories: chartdata.XAxis.Categories ? chartdata.XAxis.Categories : null
            },
            yAxis: yAxis,
            plotOptions: {
                area: {
                    turboThreshold : 0 
                },
                column: {
                    stacking: stacking
                },
                series: {
                    dataLabels: {
                        enabled: false,
                        color: '#FFFFFF'
                    },
                    pointStart: pointStart,
                    pointInterval: pointInterval,
                    shadow: false 
                }
            },
            legend: {
                align: configuration.LegendAlign ? configuration.LegendAlign : null,
                layout: configuration.LegendLayout ? configuration.LegendLayout : null,
                verticalAlign: 'bottom',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: false,
                enabled: configuration.DisableLegend && configuration.DisableLegend.toLowerCase() == 'true' ? false : true,
                title:
                    {
                        text: configuration.LegendTitle ? configuration.LegendTitle : null,
                        style:
                            {
                                color: configuration.LegendTextColor ? configuration.LegendTextColor : null
                            }
                    }
            },
            series: initialSeries,
            drilldown: {
                series: drilldownSeries
            },
            navigation: {
                buttonOptions: {
                    enabled: false
                }
            }
        });
    }

    self.exportChart = function (exportOptions, chartOptions) {
        var oldChart = self.chart;

        self.chart.exportChart(exportOptions, chartOptions);

        self.chart = oldChart;
    };

    self.getSVG = function () {
        var oldChart = self.chart;
        var svg = self.chart.getSVG();
        self.chart = oldChart;
        return svg;
    }

    self.getIsDrillDown = function () {
        return isDrillDown;
    };
    
    init(configObj);
}