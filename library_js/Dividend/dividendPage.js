﻿Controller = "Dividend";
var calTotalDivPaymentWidth;
if (!String.prototype.format) {
    String.prototype.format = function() {
        var args = arguments[0];
        return this.replace(/{(\d+)}/g, function(match, number) {
            return typeof args[number] != 'undefined'
              ? args[number]
              : match
            ;
        });
    };
}

$(function () {
    $.snl.ir.models.dividend.dividendData = window.snl.ir.globalVars.dividendPageViewModel;
    var chart;

    window.snl.ir.globalVars.exportCallback = function (model) {
        if (chart != undefined)
        {
            model.Chart.SVG = chart.getSVG();
        }
    }
    var setupDividendCalculator = function () {
        var dividendData = $.snl.ir.models.dividend.dividendData;

        $.preferCulture(window.snl.ir.globalVars.dividendPageViewModel.BaseViewModel.CurrentCulture);

        function DividendCalculatorModel() {
            var self = this;
            var defaultSecurity =window.snl.ir.globalVars.dividendPageViewModel.CalculatorKeyFndg!=null?window.snl.ir.globalVars.dividendPageViewModel.CalculatorKeyFndg: dividendData.Securities[0].KeyFndg;

            self.findPeriodItem = function (periodDivs, key) {
                var result;

                $.each(periodDivs, function (idx, item) {
                    if (item.Key == key) {
                        result = item;
                        return false;
                    }
                });

                return result;
            };

            self.objectToArray = function (object) {
                var periodDivs = [];

                if (object != null && typeof(object) != 'undefined') {
                    if (object.Annualized != null) {
                        periodDivs.push({ "text": object.AnnualizedText, "key": object.Annualized });
                    }
                    if (object.MostRecent != null) {
                        periodDivs.push({ "text": object.MostRecentText, "key": object.MostRecent });
                    }
                    if (object.YTD != null) {
                        periodDivs.push({ "text": "YTD", "key": object.YTD });
                    }
                }

                return periodDivs;
            };

            self.selectedSecurityKey = defaultSecurity;

            self.securities = dividendData.Securities;
            self.periods = self.objectToArray(self.findPeriodItem(dividendData.PeriodDivs, defaultSecurity));

            self.populatePeriodOptions = function () {
                if ($("#calculatorPeriod option").length == 0) {
                    
                    $.each(self.periods, function (idx, item) {
                        if (item.key == window.snl.ir.globalVars.dividendPageViewModel.CalculatorPeriod) 
                        {                           
                          $("#calculatorPeriod").append($("<option>").text(item.text).val(item.key).attr("selected", "selected"));
                        } 
                        else {
                            $("#calculatorPeriod").append($("<option>").text(item.text).val(item.key));
                        }
                    });
                }
            }

            self.resetCalcSectionStyle = function () {
                $("#calculatorShares").removeClass("errMsgBox");
                $("#calculatorErr").css("display", "none");
                                             
                // check the CalculatorError Error if null the that would the first time page is loaded or user when click on the calculate button
                // the reson to put check in this funciton is for tool kit print preview  and resetCalcSectionStyle is executed at the end.
                if (window.snl.ir.globalVars.dividendPageViewModel.CalculatorError != null) { 

                    $("#calculatorShares").addClass("errMsgBox");
                    $("#calculatorErr").text("Please enter numeric characters only.");
                    $("#calculatorResultsInnerPanel").css({ "visibility": "hidden", "position": "absolute" });
                    $("#calculatorErr").css("display", "block");

                }
                else
                {
                    $("#calculatorResultsInnerPanel").css({"visibility": "visible", "position": "relative" });
                }

                $("#calculatorDividendPerSharePanel").addClass("col-sm-12 col-md-6");
                $("#calculatorDividendPerSharePanel").removeClass("irwHaveResult");
                $("#calculatorTotalDividendPaymentPanel").addClass("col-sm-12 col-md-6");
                $("#calculatorTotalDividendPaymentPanel").removeClass("irwHaveResult");
            }

            self.setCalcSectionStyle = function () {                
                calTotalDivPaymentWidth = $("#calculatorResultsPanel").width() - 30; //Set Screen Width                
                if ($("#calculatorTotalDividendPayment").width() >= calTotalDivPaymentWidth && $("#calculatorTotalDividendPayment").width() != 0) {
                    $("#calculatorTotalDividendPayment").addClass("irwbreak-word").width(calTotalDivPaymentWidth);
                } else {
                    $("#calculatorTotalDividendPayment").removeAttr("style").removeClass("irwbreak-word");
                }

                if ($("#calculatorTotalDividendPayment").text().length < 8) {
                    $("#calculatorDividendPerSharePanel").addClass("col-sm-12 col-md-6");
                    $("#calculatorTotalDividendPaymentPanel").addClass("col-sm-12 col-md-6");
                } else if ($("#calculatorTotalDividendPayment").text().length > 8) {
                    $("#calculatorDividendPerSharePanel").removeClass("col-sm-12 col-md-6");
                    $("#calculatorDividendPerSharePanel").addClass("irwHaveResult");
                    $("#calculatorTotalDividendPaymentPanel").removeClass("col-sm-12 col-md-6");
                    $("#calculatorTotalDividendPaymentPanel").addClass("irwHaveResult");
                }
            }

            self.calculate = function () {
                var shares = $("#calculatorShares").val();
                var selectedPeriodKey = parseFloat($("#calculatorPeriod").val());
                window.snl.ir.globalVars.dividendPageViewModel.CalculatorError = null; // make the error null
                 self.resetCalcSectionStyle();

                if (shares) {
                    var sharesParsed = parseFloat(shares);

                    if (!isNaN(shares) && (sharesParsed == parseInt(shares)) && sharesParsed > 0) {
                        var result = sharesParsed * selectedPeriodKey;

                        $("#calculatorDividendPerShare").text(window.snl.ir.globalVars.dividendPageViewModel.BaseViewModel.CurrencySymbol + $.format(selectedPeriodKey,"n2"));
                        $("#calculatorTotalDividendPayment").text(window.snl.ir.globalVars.dividendPageViewModel.BaseViewModel.CurrencySymbol + $.format(result,"n2"));
                    } else {
                        $("#calculatorShares").addClass("errMsgBox");

                        $("#calculatorErr").text("Please enter numeric characters only.");

                        $("#calculatorResultsInnerPanel").css({ "visibility": "hidden", "position": "absolute" });
                        $("#calculatorErr").css("display", "block");
                        window.snl.ir.globalVars.dividendPageViewModel.CalculatorError = $("#calculatorErr").text();
                    }
                } else {
                    $("#calculatorDividendPerShare").text("-");
                    $("#calculatorTotalDividendPayment").text("-");
                }

                self.setCalcSectionStyle();

                window.snl.ir.globalVars.dividendPageViewModel.CalculatorKeyFndg = $("#calculatorKeyFndg").val();
                window.snl.ir.globalVars.dividendPageViewModel.CalculatorPeriod = selectedPeriodKey;
                window.snl.ir.globalVars.dividendPageViewModel.CalculatorShares = shares;
                window.snl.ir.globalVars.dividendPageViewModel.DividendPerShare = $("#calculatorDividendPerShare").text();
                window.snl.ir.globalVars.dividendPageViewModel.TotalDividendPayment = $("#calculatorTotalDividendPayment").text();
               
               
            }
        }

        if (dividendData.Securities.length > 0) {
            var calculatorObj = new DividendCalculatorModel();

            calculatorObj.populatePeriodOptions();

            calculatorObj.resetCalcSectionStyle();
            calculatorObj.setCalcSectionStyle();
           
            $("#calculatorKeyFndg").change(function () {
               
                var key = $(this).val();
                var selected = calculatorObj.findPeriodItem(dividendData.PeriodDivs, key);

                calculatorObj.periods = calculatorObj.objectToArray(selected);
                calculatorObj.selectedPeriodKey = selected.Annualized;

                $("#calculatorPeriod option").remove();
                calculatorObj.populatePeriodOptions();
            });

            $("#calculatorCalculate").click(function () {
                calculatorObj.calculate();
            });

            $("#calculatorShares").keypress(function (e) {
                if (e.which == 13) {
                    window.event.cancelBubble = true;
                    window.event.returnValue = false;
                    calculatorObj.calculate();
                }
            });

            var displayCalculator = false;
            var defaultSecurity = (dividendData.Securities.length > 0 ? dividendData.Securities[0].KeyFndg : -1);
            var dividendPeriod = null;

            $.each(dividendData.PeriodDivs, function (idx, item) {
                if (item.Key == defaultSecurity) {
                    dividendPeriod = item;
                }
            });

            if (
                dividendData.Securities.length > 0
                && dividendPeriod != null
                && (dividendPeriod.Annualized != null || dividendPeriod.MostRecent != null || dividendPeriod.YTD != null)
                ) {
                displayCalculator = true;
            }

            if (displayCalculator) {
                $("#dividendCalculatorPanel").css("display", "block");
            }
        }
    }

    var populateDividendData = function () {
        var dividendData = $.snl.ir.models.dividend.dividendData;
       
        if ($("#calculatorKeyFndg option").length == 0) {
            $.each(dividendData.Securities, function (idx, item) {              
              
                if (item.TradingSymbol == null || item.ExhangeShortName == null)
                {
                  
                    if (item.KeyFndg == window.snl.ir.globalVars.dividendPageViewModel.CalculatorKeyFndg) {
                      
                        $("#calculatorKeyFndg").append($("<option selected=\"selected\">").text(item.FndgType).val(item.KeyFndg).attr("selected", "selected"));   

                    }
                    else {
                        $("#calculatorKeyFndg").append($("<option>").text(item.FndgType).val(item.KeyFndg));
                    }
                }
                else {

                    if (item.KeyFndg == window.snl.ir.globalVars.dividendPageViewModel.CalculatorKeyFndg) {
                       
                        $("#calculatorKeyFndg").append($("<option>").text(item.TradingSymbol + " - " + item.ExhangeShortName).val(item.KeyFndg).attr("selected", "selected"));
                    }
                    else {
                        $("#calculatorKeyFndg").append($("<option>").text(item.TradingSymbol + " - " + item.ExhangeShortName).val(item.KeyFndg));
                    }

                    }
            });
        }

        setupDividendCalculator();
        bindChart(true);
        
    }

    var bindChart = function (loadStockScript) {
             
        if ($.snl.ir.models.dividend.dividendData.Chart == null || $.snl.ir.models.dividend.dividendData.Settings.DisplayGraph != true) {
            return;
        }
        else {
            var config = {
                chartDiv: $('#chart'),
                loadStockScript: loadStockScript,
                chartType: 'column',
                data: $.snl.ir.models.dividend.dividendData.Chart,
                chartConfig: {
                    toolTip: {
                        pointValueDecimalPlaces:
                            function () {
                                var decimalPlaces;

                                if (chart.getIsDrillDown()) {
                                    decimalPlaces = 4;
                                }
                                else {
                                    decimalPlaces = 2;
                                }

                                return decimalPlaces;
                            }
                    }
                }
            };

            chart = new $.snl.ir.controls.chart(config);
            
        }
    }

    populateDividendData();
});

var allPrevGrids = [];

function YearSelected(key, value, model) {
    var prevModel = model[value];
    var sum = 0;
   
    $.each(prevModel, function (index, item) {
        sum += item.Amount;
    });

    allPrevGrids[key].populateGrid(model[value], [value, $.format(sum,"n4")]);
   
    //$(".prevYearTable.irwResponsiveTable").trigger('footable_redraw');
    /*Dividend & Footable Init*/
    //if ($('.prevYearTable.irwResponsiveTable').hasClass('breakpoint')) {
    //    $('.prevYearTable.irwResponsiveTable').find('.irwDivTotal td').each(function (i) {
    //        $(this).removeAttr("colspan").removeAttr("class").removeAttr("style");
    //        $(this).children('.footable-toggle').hide();
    //    });
    //}

}

function RenderGrids(key, currentGridId, previousGridId, currentModel, prevModel, currentTotal, prevTotal) {

    var columns = [];

    columns.push({
        key: "Key",
        dataType: "string",
        hidden: true
    });
    columns.push({
        headerText: "<a href='' class='FinancialData' Title='' ItemName='1506' data-toggle='modal' data-target='.PopupModel'>Ex-Dividend Date</a>",
        key: "ExDividendDate",
        dataType: "date"
    });
    columns.push({
        headerText: "<a href='' class='FinancialData' Title='' ItemName='1505' data-toggle='modal' data-target='.PopupModel'>Record Date</a>",
        key: "RecordDate",
        dataType: "date"
    });
    columns.push({
        headerText: "<a href='' class='FinancialData' Title='' ItemName='1504' data-toggle='modal' data-target='.PopupModel'>Announce Date</a>",
        key: "AnnounceDate",
        dataType: "date"
    });
    columns.push({
        headerText: "<a href='' class='FinancialData' Title='' ItemName='1507' data-toggle='modal' data-target='.PopupModel'>Pay Date</a>",
        key: "PayDate",
        dataType: "date"
    });
    columns.push({
        headerText: "<a href='' class='FinancialData' Title='' ItemName='1508' data-toggle='modal' data-target='.PopupModel'>Amount</a>",
        key: "Amount",
        dataType: "number"
    });
    columns.push({
        headerText: "<a href='' class='FinancialData' Title='' ItemName='1509' data-toggle='modal' data-target='.PopupModel'>Frequency</a>",
        key: "Frequency",
        dataType: "string"
    });

    
    var currentGridConfig = {
        gridElement: $(currentGridId)[0],
        primaryKey: "Key",
        columns: columns,
        footerRow: "<tr class='irwDivTotal'>" +
                       "<td ><div class='irwTotalDivPane'><strong>Total dividends paid in {0}</strong></div></td>" +
                        "<td><strong></strong></td>" +
                        "<td><strong></strong></td>" +
                         "<td><strong></strong></td>" +
                        "<td><strong class='text-primary'>{1}</strong></td>" +
                        "<td><strong></strong></td>" +

                    "</tr>",
        otherOptions: [
           { name: "width", value: null },
           {
               name: "headerCellRendered",
               value: function (evt, ui) {
                   if (ui.columnKey == "RecordDate" || ui.columnKey == "AnnounceDate" || ui.columnKey == "PayDate" || ui.columnKey == "Frequency") {
                       ui.th.attr("data-hide", "phone,tablet");
                    }
               }
           }
        ]
    };

    var prevGridConfig = {
        gridElement: $(previousGridId)[0],
        primaryKey: "Key",
        columns: columns,
        footerRow: "<tr class='irwDivTotal'>" +
                       "<td ><div class='irwTotalDivPane'><strong>Total dividends paid in {0}</strong></div></td>" +
                       "<td><strong></strong></td>" +
                       "<td><strong></strong></td>" +
                        "<td><strong></strong></td>" +
                       "<td><strong class='text-primary'>{1}</strong></td>" +
                       "<td><strong></strong></td>" +

                   "</tr>",
        otherOptions: [
            { name: "width", value: null },
            {
                name: "headerCellRendered",
                value: function (evt, ui) {
                    if (ui.columnKey == "RecordDate" || ui.columnKey == "AnnounceDate" || ui.columnKey == "PayDate" || ui.columnKey == "Frequency") {
                        ui.th.attr("data-hide", "phone,tablet");
                    }
                }
            }

        ]
    };

    var prevDivGrid = new $.snl.ir.controls.grid(prevGridConfig);
    var currentDivGrid = new $.snl.ir.controls.grid(currentGridConfig);

    currentDivGrid.populateGrid(currentModel.Value, [currentModel.Key, currentTotal]);
    prevDivGrid.populateGrid(prevModel.Value, [prevModel.Key, prevTotal]);

    allPrevGrids[key] = prevDivGrid;
}

function RenderSplitGrids(key,splitGridId,splitModel) {
    
    var splitColumns = [];

    splitColumns.push({
        key: "Key",
        dataType: "string",
        hidden: true
    });
    splitColumns.push({
        headerText: "<a href='' class='FinancialData' Title='' ItemName='1506' data-toggle='modal' data-target='.PopupModel'>Ex-Dividend Date</a>",
        key: "ExDividendDate",
        dataType: "date"
    });
    splitColumns.push({
        headerText: "<a href='' class='FinancialData' Title='' ItemName='1505' data-toggle='modal' data-target='.PopupModel'>Record Date</a>",
        key: "RecordDate",
        dataType: "date"
    });
    splitColumns.push({
        headerText: "<a href='' class='FinancialData' Title='' ItemName='1504' data-toggle='modal' data-target='.PopupModel'>Announce Date</a>",
        key: "AnnounceDate",
        dataType: "date"
    });
    splitColumns.push({
        headerText: "<a href='' class='FinancialData' Title='' ItemName='1507' data-toggle='modal' data-target='.PopupModel'>Pay Date</a>",
        key: "DivPay",
        dataType: "date"
    });

    splitColumns.push({
        headerText: "<a href='' class='FinancialData' Title='' ItemName='30872' data-toggle='modal' data-target='.PopupModel'>Type of Dividend</a>",
        key: "StockSplitDescription",
        dataType: "string",
        template: "${StockSplitDescription}"
    });

    var splitGridGridConfig = {
        gridElement: $(splitGridId)[0],
        primaryKey: "Key",
        columns: splitColumns,
        otherOptions: [
            { name: "width", value: null },
            {
                name: "headerCellRendered",
                value: function (evt, ui) {
                    if (ui.columnKey == "RecordDate" || ui.columnKey == "AnnounceDate" || ui.columnKey == "DivPay") {
                        ui.th.attr("data-hide", "phone,tablet");
                    }
                }
            }

        ]
    };

    splitGrid = new $.snl.ir.controls.grid(splitGridGridConfig);
    splitGrid.populateGrid(splitModel);

}
$(document).ready(function () {        
    $('.irwRangeSlider').slider({
        tooltip: 'show'
    });

    $(".irwRangeSlider").on("slide", function (slideEvt) {
        if (slideEvt.value[0].toString() != "NaN") {
            $(this).parents(".irwBoxHeader").find(".irwDivsYearsFrom").text($(this).find(".tooltip-inner").text());
        }
        else {
            return false;
        }
    });
});

function renderSearchData(key, json) {
    $("#searchData" + key).css("opacity", "0.5");
  
    var model = "";
    model = JSON.parse(json);
    
    $("#searchData" + key).html("");
    $("#GridP_" + key).html("");

    var period = $("#p" + key).text().split("-");
    var startYear = "0";
    var endYear = "0";

    if (period.length == 2) {
        startYear = period[0].trim();
        endYear = period[1].trim();
        window.snl.ir.globalVars.dividendPageViewModel.Data[key].StartYear = startYear;
        window.snl.ir.globalVars.dividendPageViewModel.Data[key].EndYear = endYear;
        window.snl.ir.globalVars.dividendPageViewModel.Data[key].IsModified = true;
    }
    
    if (endYear < startYear) {
        $("#searchData" + key).html("No Dividends data is available");
        $("#searchData" + key).css("opacity", "1");
        return false;
    }

    for (var i = endYear; i >= startYear; i--) {

        var prevModel = model[i];
        var sum = 0;
        if (prevModel != undefined) {
            $.each(prevModel, function (index, item) {
                sum += item.Amount;
            });

            var tableElement = document.createElement("table");
            var tableElementId = "GridP_" + key + "_" + i;
            tableElement.id = tableElementId;
            tableElement.setAttribute("class", "prevYearTable table tableGrid irwResponsiveTable");
            tableElement.setAttribute("data-sort",false);

            $("#searchData" + key).append(tableElement);

            var prevDivGrid = new $.snl.ir.controls.grid(configureGrid(tableElementId));
            prevDivGrid.populateGrid(model[i], [i, $.format(sum,"n4")]);
            //$("#searchData" + key +" .prevYearTable.irwResponsiveTable").trigger('footable_redraw');
            /*Dividend & Footable Init*/
            //if ($('.prevYearTable.irwResponsiveTable').hasClass('breakpoint')) {
            //    $('.prevYearTable.irwResponsiveTable').find('.irwDivTotal td').each(function (i) {
            //        //$(this).removeAttr("colspan").removeAttr("class").removeAttr("style");
            //        $(this).children('.footable-toggle').hide();
            //    });
            //}
           
         //   $('#' + tableElementId).footable();

            
            
//Fixed design issue only for FF
            setTimeout(
              function() 
              {
                  $('.irwResponsiveTable').footable();
                  $('.irwDivTotal').css("position", "relative");
              }, 700);
        }
    }
    $("#searchData" + key).css("opacity", "1");
    return false;
}
$(window).resize(function () {
    $('.irwDivTotal').css("position", "inherit");
    setTimeout(function () {
        $('.irwDivTotal').css("position", "relative");
        self.resetCalcSectionStyle();
        self.setCalcSectionStyle();
    }, 100);
});

setTimeout(function () {
    $('.irwDivTotal').css("position", "relative");
}, 1000);

function configureGrid(key) {

    var columns = [];

    columns.push({
        key: "Key",
        dataType: "string",
        hidden: true
    });
    columns.push({
        headerText: "<a href='' class='FinancialData' Title='' ItemName='1506' data-toggle='modal' data-target='.PopupModel'>Ex-Dividend Date</a>",
        key: "ExDividendDate",
        dataType: "date"
    });
    columns.push({
        headerText: "<a href='' class='FinancialData' Title='' ItemName='1505' data-toggle='modal' data-target='.PopupModel'>Record Date</a>",
        key: "RecordDate",
        dataType: "date"
    });
    columns.push({
        headerText: "<a href='' class='FinancialData' Title='' ItemName='1504' data-toggle='modal' data-target='.PopupModel'>Announce Date</a>",
        key: "AnnounceDate",
        dataType: "date"
    });
    columns.push({
        headerText: "<a href='' class='FinancialData' Title='' ItemName='1507' data-toggle='modal' data-target='.PopupModel'>Pay Date</a>",
        key: "PayDate",
        dataType: "date"
    });
    columns.push({
        headerText: "<a href='' class='FinancialData' Title='' ItemName='1508' data-toggle='modal' data-target='.PopupModel'>Amount</a>",
        key: "Amount",
        dataType: "number"
    });
    columns.push({
        headerText: "<a href='' class='FinancialData' Title='' ItemName='1509' data-toggle='modal' data-target='.PopupModel'>Frequency</a>",
        key: "Frequency",
        dataType: "string"
    });

    var prevGridConfig = {
        gridElement: $("#" + key)[0],
        primaryKey: "Key",
        columns: columns,
        footerRow: "<tr class='irwDivTotal'>" +
                        "<td ><div class='irwTotalDivPane'><strong>Total dividends paid in {0}</strong></div></td>" +
                        "<td><strong></strong></td>" +
                        "<td><strong></strong></td>" +
                         "<td><strong></strong></td>" +
                        "<td><strong class='text-primary'>{1}</strong></td>" +
                        "<td><strong></strong></td>" +

                    "</tr>",
        otherOptions: [
            { name: "width", value: null },
            {
                name: "headerCellRendered",
                value: function (evt, ui) {
                    if (ui.columnKey == "RecordDate" || ui.columnKey == "AnnounceDate" || ui.columnKey == "PayDate" || ui.columnKey == "Frequency") {
                        ui.th.attr("data-hide", "phone,tablet");
                    }
                }
            }

        ]
    };

    return prevGridConfig;
}
