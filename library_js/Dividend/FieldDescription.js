﻿var Controller="CorporateProfile";
$.snl.ir.models.api.FieldDescription = {};
$.snl.ir.models.FieldDescription = {};

//----------------------------------popup data ----------------------------------------------------------------------------------------

$.snl.ir.models.api.FieldDescription.getPopupData = function (KeyDoc, Type, callBack) {
   
    var _urlBase = $.snl.ir.models.api.urlTemplate.replace("{Controller}", Controller);
    var _urlDocsData = _urlBase.replace("{Action}", "GetPopupData") + "?KeyDoc={KeyDoc}" + "&Type={Type}";
    var url = _urlDocsData.replace("{KeyDoc}", KeyDoc);
    url = url.replace("{Type}", Type);
    $.post(url)
         .done(function (response) {
             $.snl.ir.models.api.FieldDescription.PopupData = response;

             callBack();
         })
         .fail(function () {
             alert("error");
         })
         .always(function () {
             //alert("complete");
         });
    //window.open(url, "_blank", "toolbar=yes, scrollbars=yes, resizable=yes, top=500, left=500, width=400, height=400");
};


var getPopupData = function (KeyDoc, Type) {
    $("#PopupDive").html('');
    $("#ModelTitle").html(Type);
    $.snl.ir.models.api.FieldDescription.getPopupData(KeyDoc, Type,populatePopupData);
   
};
var populatePopupData = function () {

    var PopUpData = $.snl.ir.models.api.FieldDescription.PopupData;
    
    $(".PopupModel .irwLoading").hide();
    $("#PopupDive").html(PopUpData);
    $('.irwScrollbar').mCustomScrollbar({
        theme: "bootstrap-thin",
        advanced: {
            updateOnContentResize: true
        }
    });
   
   //  bindPage();  
}

$(document).delegate(".IrwFilingType", "click", function (e) {
    $(".irwGroupBack").hide();
    $(".PopupModel .irwLoading").show();
    var Type = $(this).attr('FileType');
    getPopupData(0, Type)
});
//End of---------PopupData------------------------------------------------------ 

//---------------------------------------GetDefinitionData--------------------------------------------------------------
$.snl.ir.models.api.FieldDescription.GetDefinitionData = function (itemName, keyItem,callBack) {
    var _urlBase = $.snl.ir.models.api.urlTemplate.replace("{Controller}", Controller);
    var _urlDocsData = _urlBase.replace("{Action}", "GetDefinition") + "?itemName={itemName}" + "&keyItem={keyItem}";
    var url = _urlDocsData.replace("{itemName}", itemName);
    url = url.replace("{keyItem}", keyItem);
  

    $.post(url)
         .done(function (response) {
             $.snl.ir.models.api.FieldDescription.DefinitionData = response;

             callBack();
         })
         .fail(function () {
             alert("error");
         })
         .always(function () {
             //alert("complete");
         });

   // window.open(url, "_blank", "toolbar=yes, scrollbars=yes, resizable=yes, top=500, left=500, width=400, height=400");

};
var PopulateDefinitionData = function () {

    var DefinitionDataData = $.snl.ir.models.api.FieldDescription.DefinitionData;
    $("#PopupDive").html(DefinitionDataData);
    
    $("#ModelTitle").html($(".irwHiddenCaption").attr("value"));
    $(".PopupModel .irwLoading").hide();
    $('.irwScrollbar').mCustomScrollbar({
        theme: "bootstrap-thin",
        advanced: {
            updateOnContentResize: true
        }
    });
   
}
var GetDefinitionData = function (itemName, keyItem, Title) {
    $("#PopupDive").html('');
     $("#ModelTitle").html('&nbsp;');
    $.snl.ir.models.api.FieldDescription.GetDefinitionData(itemName, keyItem,PopulateDefinitionData);
};
$(document).delegate(".FinancialData", "click", function (e) {
     
    $(".PopupModel .irwLoading").show();
    var ItemName = $(this).attr('ItemName');
    var Title = $(this).attr('Title');
    GetDefinitionData(ItemName, 0, Title);

});
//End of ---------------------------------------GetDefinitionData--------------------------------------------------------------


//---------------------------------------GetGroupDescriptions--------------------------------------------------------------

$.snl.ir.models.api.FieldDescription.GetGroupDescriptions = function (callBack) {
    var _urlBase = $.snl.ir.models.api.urlTemplate.replace("{Controller}", Controller);
    var _urlDocsData = _urlBase.replace("{Action}", "GetGroupDescriptions");
    console.log(_urlDocsData);
    $.post(_urlDocsData)
        .done(function (response) {
            $.snl.ir.models.api.FieldDescription.GroupDescriptionData = response;
            callBack();
        })
        .fail(function () {
            alert("error");
        })
        .always(function () {
            //alert("complete");
        });
   // window.open(_urlDocsData, "_blank", "toolbar=yes, scrollbars=yes, resizable=yes, top=500, left=500, width=400, height=400");

};

var PopulateGroupDescriptionData = function () {

    var GroupDescriptionData = $.snl.ir.models.api.FieldDescription.GroupDescriptionData;
    $("#PopupDive").html(GroupDescriptionData);
    $(".PopupModel .irwLoading").hide();
    
    $('.irwScrollbar').mCustomScrollbar({
        theme: "bootstrap-thin",
        advanced: {
            updateOnContentResize: true
        }
    });
  
}
var GetGroupDescriptions = function () {
    $("#PopupDive").html('');
    $("#ModelTitle").html('Group Descriptions');
    $.snl.ir.models.api.FieldDescription.GetGroupDescriptions(PopulateGroupDescriptionData);
};

$(document).delegate(".Descriptions", "click", function (e) {
    $(".irwGroupBack").hide();
    $(".PopupModel .irwLoading").show();
    GetGroupDescriptions();
});
//End of ---------------------------------------GetGroupDescriptions--------------------------------------------------------------



//---------------------------------------GetTypeDescriptions--------------------------------------------------------------

$.snl.ir.models.api.FieldDescription.GetTypeDescriptions = function (alias,callBack) {
    var _urlBase = $.snl.ir.models.api.urlTemplate.replace("{Controller}", Controller);
    var _urlDocsData = _urlBase.replace("{Action}", "GetTypeDescriptions") + "?alias={alias}";
    var url = _urlDocsData.replace("{alias}", alias);
  
    $.post(url)
        .done(function (response) {
            $.snl.ir.models.api.FieldDescription.TypeDescriptionData = response;

            callBack();
        })
        .fail(function () {
            alert("error");
        })
        .always(function () {
            //alert("complete");
        })
    //window.open(url, "_blank", "toolbar=yes, scrollbars=yes, resizable=yes, top=500, left=500, width=400, height=400");
};

var PopulateGetTypeDescriptionsData = function () {

    var TypeDescriptionsData = $.snl.ir.models.api.FieldDescription.TypeDescriptionData;
    $("#PopupDive").html(TypeDescriptionsData);
    $(".PopupModel .irwLoading").hide();
    $(".irwGroupBack").show();
    $('.irwScrollbar').mCustomScrollbar({
        theme: "bootstrap-thin",
        advanced: {
            updateOnContentResize: true
        }
    });
   
}

var GetTypeDescriptions = function (alias) {
    $("#PopupDive").html('');
    $("#ModelTitle").html(alias);
    $.snl.ir.models.api.FieldDescription.GetTypeDescriptions(alias,PopulateGetTypeDescriptionsData);
};

$(document).delegate(".TypeDescriptions", "click", function (e) {
    //if ($(this).hasClass("irwHasBackEnabled")) {
    //    $(".irwGroupBack").show()
    //}
    $(".PopupModel .irwLoading").show();
    var Alias = $(this).attr('Alias');
    GetTypeDescriptions(Alias);

});

//End of ---------------------------------------GetTypeDescriptions--------------------------------------------------------------


///----------------GetFndgsInformation------------------------------------
$.snl.ir.models.api.FieldDescription.GetFndgsInformation = function (callBack) {
    var _urlBase = $.snl.ir.models.api.urlTemplate.replace("{Controller}", Controller);
    var _urlDocsData = _urlBase.replace("{Action}", "GetFndgsInformation");
   
    $.post(_urlDocsData)
        .done(function (response) {
            $.snl.ir.models.api.FieldDescription.FndgsInformationData = response;

            callBack();
        })
        .fail(function () {
            alert("error");
        })
        .always(function () {
            //alert("complete");
        });
    // window.open(_urlDocsData, "_blank", "toolbar=yes, scrollbars=yes, resizable=yes, top=500, left=500, width=400, height=400");

};

var PopulateFndgsInformationData = function () {

    var FndgsInformationData = $.snl.ir.models.api.FieldDescription.FndgsInformationData;
    $("#PopupDive").html(FndgsInformationData);
    $(".PopupModel .irwLoading").hide();
    $('.irwScrollbar').mCustomScrollbar({
        theme: "bootstrap-thin",
        advanced: {
            updateOnContentResize: true
        }
    });
}
var GetFndgsInformation = function () {
    $("#PopupDive").html('');
    $("#ModelTitle").html('Funding Description');    
    $.snl.ir.models.api.FieldDescription.GetFndgsInformation(PopulateFndgsInformationData);
};

$(document).delegate(".FundingDescription", "click", function (e) {
    $(".PopupModel .irwLoading").show();
    GetFndgsInformation();
    
});
///End of ----------------GetFndgsInformation------------------------------------