﻿$.snl.ir.controls.grid = function(configObj) {
    var self = this;
    var gridElementCnfg;
	var igGridObj;
	var primaryKeyCnfg;
	var columnsCnfg;
	var featuresCnfg;
	var otherOptionsCnfg;
	var footerRowCnfg;
	
	self.grid = null;

	var init = function(configObj) {
	    gridElementCnfg = configObj.gridElement || gridElementCnfg;
		primaryKeyCnfg = configObj.primaryKey || primaryKeyCnfg;
		columnsCnfg = configObj.columns || columnsCnfg;
		featuresCnfg = configObj.features || featuresCnfg;
		otherOptionsCnfg = configObj.otherOptions || otherOptionsCnfg;
		footerRowCnfg = configObj.footerRow || footerRowCnfg;
		setupGrid();
		$.preferCulture(window.snl.ir.globalVars.dividendPageViewModel.BaseViewModel.CurrentCulture);
	}

	var setupGrid = function() {
	    var gridConfigObj = {
	        primaryKey: primaryKeyCnfg,
	        width: "100%",
	        autoCommit: true,
	        fixedHeaders: false,
	        autoGenerateColumns: false,
	        alternateRowStyles: false,
	        enableHoverStyles: false,
	        columns: columnsCnfg,
	        footerRow: footerRowCnfg,
	        features: [
                {
                    name: 'Updating',
                    enableAddRow: false,
                    enableDeleteRow: false,
                    editMode: "none"
                }
	        ]
	    };

	    if (otherOptionsCnfg) {
	        $.each(otherOptionsCnfg, function(idx, item) {
	            gridConfigObj[item.name] = item.value;
	        });
	    }

	    igGridObj = $(gridElementCnfg).igGrid(
	        gridConfigObj
	    );

	    $.each(columnsCnfg, function (index, col) {
	        switch (col.dataType) {
	            case 'number':
	                col.formatter = numberFormatter;
	                break;
	            case 'date':
	                col.formatter = dateFormatter;
	                break;
	          
	        }
	    });

		self.grid = igGridObj;
	}

	var numberFormatter = function divgridNumberFormatter(val) {	   
	    return $.format(val,"n4");
	}

	var dateFormatter = function divgridDateFormatter(val) {
	    if (val == null) {
	        return '-';
	    }
	    else {
	        return moment.utc(val).format(window.snl.ir.globalVars.dividendPageViewModel.BaseViewModel.shortDateformat.toUpperCase());
	    }
	}

	this.populateGrid = function(data, footerArgs) {
	    igGridObj.igGrid("dataSourceObject", data);
	    igGridObj.igGrid("dataBind");

	    if (footerRowCnfg) {
	        $(igGridObj.children()[3]).append(footerRowCnfg.format(footerArgs));
	    }
	}

	init(configObj);
}