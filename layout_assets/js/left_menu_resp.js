(function ($) {
    $.fn.rjNav = function (options) {
        var windowWidth;

        var settings = $.extend({
            // Set Classes only on below variables without "."
            childEleClass: "hasChild", // Here define class name for list item which has submenu.
            submenuClass: "submenu", // Here define class name for children ul submenu.
            toggleClass: "openNav", // this is status class for open and close submenu.
            menubuttonClass: "irMenu-Button", // give class of menu toggle button
            activeClass: "active-sub", // give class name what ever added as a active link in main li.

            // Set values on below variables
            MenulinkRedirect: true, // It will allow redirect to the link on second click for main Navigation.
            defaultShow: true, // do setting for default show or hide submenu for active element.
            action: "click", // Here define action , two action are here "click"/"hover".
            multipleSlide: true, // IF you want to open multiple dropdown on click of list item then make it true, else it will be open any one dropdown at a time.
            breakpoint: 768, // set break point for mobile device or false for remove breakpoint condition.
            menubutton: true // do you need menu toggle button for mobile device if yes then true else false.
        }, options);

        // do not change these variables.
        var fixedVar = {
            firstLevelItem: $(this).children("li"),
            mainBase: $(this)
        }

        // default show hide elements.
        fixedVar.firstLevelItem.addClass("rjNav-item");
        fixedVar.firstLevelItem.children("ul").addClass(settings.submenuClass);
        $("." + settings.submenuClass).hide();
        $("." + settings.menubuttonClass).hide();

        // Default Show Event
        if (settings.defaultShow) {
            $("." + settings.activeClass + " > ." + settings.submenuClass).stop(true).slideDown();
            $("." + settings.activeClass).addClass(settings.toggleClass);
        }

        // each menu item.
        fixedVar.firstLevelItem.each(function () {
            var $base = $(this);
            var base = this;
            if ($base.find("ul").is("." + settings.submenuClass)) {
                $base.addClass(settings.childEleClass);
            }

            init();

            // initial function.
            function init() {
                windowWidth = $(window).width();



                if (settings.action != "hover" || windowWidth < settings.breakpoint) {
                    $base.unbind("mouseenter");
                    $base.unbind("mouseleave");
                    $base.children("a").unbind("click");
                    $base.children("a").bind("click", function (e) {
                        if (settings.multipleSlide) {
                            multipleSlide(e);
                        }
                        else {
                            singleSlide(e);
                        }
                    })


                }
                else {
                    $base.children("a").unbind("click");
                    $base.unbind("mouseenter");
                    $base.unbind("mouseleave");

                    $base.bind("mouseenter", function (e) {
                        if (settings.multipleSlide) {
                            multipleSlide(e);
                        }
                        else {
                            singleSlide(e);
                        }
                    })
                    $base.bind("mouseleave", function (e) {
                        if (settings.multipleSlide) {
                            multipleSlide(e);
                        }
                        else {
                            singleSlide(e);
                        }
                    })
                }
            }

            // multipleSlide function.
            function multipleSlide(e) {
                if (settings.MenulinkRedirect) {
                    if ($base.children("." + settings.submenuClass).is(":visible")) {
                        $base.children("." + settings.submenuClass).stop(true).slideUp("normal");
                        $base.removeClass(settings.toggleClass);
                    }
                    else if ($base.children().is("." + settings.submenuClass)) {
                        e.preventDefault();
                        $base.children("." + settings.submenuClass).stop(true).slideDown("normal");
                        $base.addClass(settings.toggleClass);
                    }
                }
                else {
                    if ($base.children("." + settings.submenuClass).is(":visible")) {
                        e.preventDefault();
                        $base.children("." + settings.submenuClass).stop(true).slideUp("normal");
                        $base.removeClass(settings.toggleClass);
                    }
                    else if ($base.children().is("." + settings.submenuClass)) {
                        e.preventDefault();
                        $base.children("." + settings.submenuClass).stop(true).slideDown("normal");
                        $base.addClass(settings.toggleClass);
                    }
                }

            }

            // singleSlide function.
            function singleSlide(e) {
                if (settings.MenulinkRedirect) {
                    if ($base.children("." + settings.submenuClass).is(":visible")) {
                        fixedVar.firstLevelItem.removeClass(settings.toggleClass);
                        $("." + settings.submenuClass).stop(true).slideUp("normal");
                    }
                    else if ($base.children().is("." + settings.submenuClass)) {
                        e.preventDefault();
                        fixedVar.firstLevelItem.removeClass(settings.toggleClass);
                        $("." + settings.submenuClass).stop(true).slideUp("normal");
                        $base.children("." + settings.submenuClass).stop(true).slideDown("normal");
                        $base.addClass(settings.toggleClass);
                    }
                }
                else {
                    if ($base.children("." + settings.submenuClass).is(":visible")) {
                        e.preventDefault();
                        fixedVar.firstLevelItem.removeClass(settings.toggleClass);
                        $("." + settings.submenuClass).stop(true).slideUp("normal");
                    }
                    else if ($base.children().is("." + settings.submenuClass)) {
                        e.preventDefault();
                        fixedVar.firstLevelItem.removeClass(settings.toggleClass);
                        $("." + settings.submenuClass).stop(true).slideUp("normal");
                        $base.children("." + settings.submenuClass).stop(true).slideDown("normal");
                        $base.addClass(settings.toggleClass);
                    }
                }

            }

            // resentFunction.
            function reset() {
                $("." + settings.submenuClass).hide();
            }

        })

        // menu toggle button function.
        menuButtonFn();
        function menuButtonFn() {
            if (settings.breakpoint == false) {
                $("." + settings.menubuttonClass).show();
                fixedVar.mainBase.hide();
                $("." + settings.menubuttonClass).unbind("click");
                $("." + settings.menubuttonClass).bind("click", function () {
                    fixedVar.mainBase.stop(true).slideToggle();
                })
            }
            else {
                if (settings.menubutton && windowWidth < settings.breakpoint) {
                    $("." + settings.menubuttonClass).show();
                    fixedVar.mainBase.hide();
                    $("." + settings.menubuttonClass).unbind("click");
                    $("." + settings.menubuttonClass).bind("click", function () {
                        fixedVar.mainBase.stop(true).slideToggle();
                    })
                }
                else {
                    $("." + settings.menubuttonClass).hide();
                    fixedVar.mainBase.show();
                    $("." + settings.menubuttonClass).unbind("click");
                }
            }
        }
    };
}(jQuery));

$(document).ready(function () {
    $(".irMenu").rjNav({
        menubutton: false
    });
})

$(window).resize(function () {
    $(".irMenu").rjNav({
        menubutton: false
    });
})