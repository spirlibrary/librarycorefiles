<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns="http://www.w3.ord/TR/REC-html40"
  xmlns:irw="http://www.snl.com/xml/irw">

<xsl:output method="html" media-type="text/html" indent="no"/>

<!-- Start Default Template here-->

<!--
//This is the default template. Most changes are to either remove the company name, ticker, or exchange. you can also change the name of the page or remove this row all together.
//These templates below are broken up how the page is displayed.-->

<xsl:template name="Gov_Title">
<xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
  <TR>
    <TD CLASS="colordark" NOWRAP="NOWRAP">
      <SPAN CLASS="title1light"><xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/></SPAN>
      </TD>
    <TD CLASS="colordark" NOWRAP="NOWRAP" align="right" valign="top"><span class="subtitlelight"><xsl:value-of select="$CompanyNameCaps"/> (<xsl:value-of select="Company/Exchange"/> - <xsl:value-of select="Company/Ticker"/>)</span><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></TD>
  </TR>
    <TR class="default" align="left">
      <TD class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Header" disable-output-escaping="yes"/></span></TD>
  </TR>
</xsl:template>

<xsl:template name="Gov_Data">
<xsl:for-each select="Governance">
  <TR>
    <TD CLASS="data">
      <A TARGET="_new" TITLE="Select to view the governance">
      <xsl:attribute name="href">
        <xsl:value-of select="Link" />
      </xsl:attribute>
        <xsl:value-of select="Title" disable-output-escaping="yes" />
      </A>
    </TD>
    <TD CLASS="data">
      <xsl:value-of select="Date"/>
    </TD>
    <xsl:if test="IconLink != ''">
    <TD CLASS="data" ALIGN="center">

     
          <A TARGET="_new">
          <xsl:attribute name="HREF">
            <xsl:value-of select="IconLink" />
          </xsl:attribute>
            <IMG  BORDER="0" alt="">
            <xsl:attribute name="SRC">
              <xsl:value-of select="IconImage" />
            </xsl:attribute>
            </IMG>
            <BR />
            
          </A>

    </TD>
    </xsl:if>
    <xsl:if test="IconLink = ''">
      <TD CLASS="data" ALIGN="center">
        <IMG  BORDER="0" alt="">
        <xsl:attribute name="SRC">
          <xsl:value-of select="IconImage" />
        </xsl:attribute>
        </IMG>
        <BR />
        
      </TD>
    </xsl:if>
    <!--<TD CLASS="data" ALIGN="center"><xsl:value-of select="FileSize"/></TD> removed 120604-->
  </TR>
</xsl:for-each>

</xsl:template>
<xsl:template name="GSW_Data">
<xsl:choose>
<xsl:when test="Company/DocDownloadCount &gt; 1">
  <TR>
    <TD CLASS="data" NOWRAP="NOWRAP" colspan="4">
      <SPAN CLASS="defaultbold">Click the link to the right to download :</SPAN>
      <xsl:for-each select="Governance[string-length(SoftwareLink) &gt; 0]">
      <xsl:if test="normalize-space(SoftwareLink)">
        <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
        <xsl:choose>
          <xsl:when test="../UISettings/ShowDisclaimer = 'true'">
            <A TARGET="_new" title="{SoftwareName}">
            <xsl:variable name="keyinstn" select="../Company/KeyInstn" />
            <xsl:variable name="urljump" select="SoftwareLink"/>
            <xsl:variable name="url" select="concat('govdocsdisclaimer.aspx?IID=', $keyinstn, '&amp;website=', $urljump)" />
            <xsl:attribute name="HREF">
              <xsl:value-of select="$url" />
            </xsl:attribute>
              <xsl:value-of select="SoftwareName"/>
            </A>
          </xsl:when>
          <xsl:otherwise>
            <A TARGET="_new" title="{SoftwareName}">
            <xsl:attribute name="HREF">
              <xsl:value-of disable-output-escaping="yes" select="SoftwareLink"/>
            </xsl:attribute>
              <xsl:value-of select="SoftwareName"/>
            </A>
        </xsl:otherwise>
        </xsl:choose>
        <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
        <xsl:if test="not(position() = last())">
          <xsl:text disable-output-escaping="yes">|</xsl:text>
        </xsl:if>
      </xsl:if>
      </xsl:for-each>
    </TD>
  </TR>
</xsl:when>
<xsl:otherwise>
  <xsl:if test="Company/DocDownloadCount &gt; 0">
  <TR>
    <TD CLASS="data" NOWRAP="NOWRAP" colspan="4">
      <SPAN CLASS="defaultbold">Click the link to the right to download :</SPAN>
      <xsl:for-each select="Governance">
      <xsl:if test="normalize-space(SoftwareLink)">
        <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
        <xsl:choose>
          <xsl:when test="../UISettings/ShowDisclaimer = 'true'">
            <A TARGET="_new" title="{SoftwareName}">
            <xsl:variable name="keyinstn" select="../Company/KeyInstn" />
            <xsl:variable name="urljump" select="SoftwareLink"/>
            <xsl:variable name="url" select="concat('govdocsdisclaimer.aspx?IID=', $keyinstn, '&amp;website=', $urljump)" />
            <xsl:attribute name="HREF">
              <xsl:value-of select="$url" />
            </xsl:attribute>
              <xsl:value-of select="SoftwareName"/>
            </A>
          </xsl:when>
          <xsl:otherwise>
            <A TARGET="_new" title="{SoftwareName}">
            <xsl:attribute name="HREF">
              <xsl:value-of disable-output-escaping="yes" select="SoftwareLink"/>
            </xsl:attribute>
              <xsl:value-of select="SoftwareName"/>
            </A>
         </xsl:otherwise>
        </xsl:choose>
      </xsl:if>
      </xsl:for-each>
    </TD>
  </TR>
  </xsl:if>
</xsl:otherwise>
</xsl:choose>

</xsl:template>
<!-- Default Template Ends here. all templates below are ONLY if you are using the 'Style Template' section in the console. -->



















<!-- This is Template ONE option in the console under 'Style Template'
//This template is a striped down version of the main template. Everything (data) is displayed in single rows.
//The sections will be commented on where the rows are. you should be able to just move rows (entire chunks of data) where you need too. Edits to this section are rare, mostly due to size (width of the page).
-->
<xsl:template name="Gov_Title2">
<xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
  <tr>
    <td CLASS="title2colordark titletest" NOWRAP="NOWRAP" COLSPAN="2">
      <SPAN CLASS=""><xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/></SPAN><br /><span class="title2colordark titletest2"><xsl:value-of select="$CompanyNameCaps"/> (<xsl:value-of select="Company/Exchange"/> - <xsl:value-of select="Company/Ticker"/>)</span><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
      </td>
  </tr>
    <tr class="default" align="left">
      <td class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Header" disable-output-escaping="yes"/></span></td>
  </tr>
  <tr>
    <td>
      <table width="100%" cellpadding="0" cellspacing="0" height="1">
        <tr>
          <td class="colordark"><img src="" width="1" height="1"  alt="" /></td>
        </tr>
      </table>
    </td>
</tr>
</xsl:template>


<xsl:template name="Gov_Data2">
  <xsl:for-each select="Governance">
  <TR>
    <TD CLASS="default">
      <A TARGET="_new" TITLE="Select to view the governance">
      <xsl:attribute name="href">
        <xsl:value-of select="Link" />
      </xsl:attribute>
        <xsl:value-of select="Title" disable-output-escaping="yes" />
      </A>
    </TD>
    <TD CLASS="data">
      <xsl:value-of select="Date"/>
    </TD>
    <xsl:if test="IconLink != ''">
      <TD CLASS="data" ALIGN="center">
            <A TARGET="_new">
            <xsl:attribute name="HREF">
              <xsl:value-of select="IconLink" />
            </xsl:attribute>
              <IMG  BORDER="0"  alt="">
              <xsl:attribute name="SRC">
                <xsl:value-of select="IconImage" />
              </xsl:attribute>
              </IMG>
              <BR />
            </A>
      </TD>
    </xsl:if>
    <xsl:if test="IconLink = ''">
      <TD CLASS="data" ALIGN="center">
        <IMG  BORDER="0"  alt="">
        <xsl:attribute name="SRC">
          <xsl:value-of select="IconImage" />
        </xsl:attribute>
        </IMG>
        <BR />
        
      </TD>
    </xsl:if>
  </TR>
</xsl:for-each>
</xsl:template>

<xsl:template name="GSW_Data2">
<xsl:choose>
<xsl:when test="Company/DocDownloadCount &gt; 1">
  <TR>
    <TD CLASS="surr datashade" NOWRAP="NOWRAP" colspan="4">
      <table>
      <tr>
        <td class="default">
          <SPAN CLASS="defaultbold">Click the link to the right to download :</SPAN>
          <xsl:for-each select="Governance[string-length(SoftwareLink) &gt; 0]">
          <xsl:if test="normalize-space(SoftwareLink)">
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            <xsl:choose>
              <xsl:when test="../UISettings/ShowDisclaimer = 'true'">
                <A TARGET="_new">
                <xsl:variable name="keyinstn" select="../Company/KeyInstn" />
                <xsl:variable name="urljump" select="SoftwareLink"/>
                <xsl:variable name="url" select="concat('govdocsdisclaimer.aspx?IID=', $keyinstn, '&amp;website=', $urljump)" />
                <xsl:attribute name="HREF">
                  <xsl:value-of select="$url" />
                </xsl:attribute>
                  <xsl:value-of select="SoftwareName"/>
                </A>
              </xsl:when>
              <xsl:otherwise>
                <A TARGET="_new">
                <xsl:attribute name="HREF">
                  <xsl:value-of disable-output-escaping="yes" select="SoftwareLink"/>
                </xsl:attribute>
                  <xsl:value-of select="SoftwareName"/>
                </A>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            <xsl:if test="not(position() = last())">
              <xsl:text disable-output-escaping="yes">|</xsl:text>
            </xsl:if>
          </xsl:if>
          </xsl:for-each>
        </td>
      </tr>
      </table>
    </TD>
  </TR>
</xsl:when>
<xsl:otherwise>
  <xsl:if test="Company/DocDownloadCount &gt; 0">
  <TR>
    <TD  CLASS="surr datashade" NOWRAP="NOWRAP" colspan="4">
      <table>
      <tr>
        <td class="default">
          <SPAN CLASS="defaultbold">Click the link to the right to download :</SPAN>
          <xsl:for-each select="Governance">
          <xsl:if test="normalize-space(SoftwareLink)">
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
           <xsl:choose>
              <xsl:when test="../UISettings/ShowDisclaimer = 'true'">
                <A TARGET="_new">
                <xsl:variable name="keyinstn" select="../Company/KeyInstn" />
                <xsl:variable name="urljump" select="SoftwareLink"/>
                <xsl:variable name="url" select="concat('govdocsdisclaimer.aspx?IID=', $keyinstn, '&amp;website=', $urljump)" />
                <xsl:attribute name="HREF">
                  <xsl:value-of select="$url" />
                </xsl:attribute>
                  <xsl:value-of select="SoftwareName"/>
                </A>
              </xsl:when>
              <xsl:otherwise>
                <A TARGET="_new">
                <xsl:attribute name="HREF">
                  <xsl:value-of disable-output-escaping="yes" select="SoftwareLink"/>
                </xsl:attribute>
                  <xsl:value-of select="SoftwareName"/>
                </A>
             </xsl:otherwise>
            </xsl:choose>
          </xsl:if>
          </xsl:for-each>
        </td>
      </tr>
      </table>
    </TD>
  </TR>
  </xsl:if>
</xsl:otherwise>
</xsl:choose>

</xsl:template>

  <!-- Template ONE Ends here. -->





<!-- This is Template 3 -->
   <xsl:template name="Gov_Data3">
    <xsl:for-each select="Governance">
    <TR>
      <TD CLASS="default">
        <A TARGET="_new" TITLE="Select to view the governance">
        <xsl:attribute name="href">
          <xsl:value-of select="Link" />
        </xsl:attribute>
          <xsl:value-of select="Title" disable-output-escaping="yes" />
        </A>
      </TD>
      <TD CLASS="data">
        <xsl:value-of select="Date"/>
      </TD>
      <xsl:if test="IconLink != ''">
        <TD CLASS="data" ALIGN="center"> 
            <A TARGET="_new">
            <xsl:attribute name="HREF">
              <xsl:value-of select="IconLink" />
            </xsl:attribute>
            <IMG  BORDER="0"  alt="">
            <xsl:attribute name="SRC">
              <xsl:value-of select="IconImage" />
            </xsl:attribute>
            </IMG>
            <BR />    
            </A>
        </TD>
      </xsl:if>
      <xsl:if test="IconLink = ''">
        <TD CLASS="data" ALIGN="center">
        <IMG  BORDER="0"  alt="">
        <xsl:attribute name="SRC">
          <xsl:value-of select="IconImage" />
        </xsl:attribute>
        </IMG>
          <BR />
        </TD>
      </xsl:if>
    </TR>
   </xsl:for-each>
   </xsl:template>

   <xsl:template name="GSW_Data3">
   <xsl:choose>
   <xsl:when test="Company/DocDownloadCount &gt; 1">
    <TR>
      <TD CLASS="surr datashade" NOWRAP="NOWRAP" colspan="4">
        <table>
        <tr>
          <td class="default">
          <SPAN CLASS="defaultbold">Click the link to the right to download :</SPAN>
          <xsl:for-each select="Governance[string-length(SoftwareLink) &gt; 0]">
          <xsl:if test="normalize-space(SoftwareLink)">
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          <xsl:choose>
              <xsl:when test="../UISettings/ShowDisclaimer = 'true'">
                <A TARGET="_new" title="{SoftwareName}">
                <xsl:variable name="keyinstn" select="../Company/KeyInstn" />
                <xsl:variable name="urljump" select="SoftwareLink"/>
                <xsl:variable name="url" select="concat('govdocsdisclaimer.aspx?IID=', $keyinstn, '&amp;website=', $urljump)" />
                <xsl:attribute name="HREF">
                  <xsl:value-of select="$url" />
                </xsl:attribute>
                  <xsl:value-of select="SoftwareName"/>
                </A>
              </xsl:when>
              <xsl:otherwise>
                <A TARGET="_new" title="{SoftwareName}">
                <xsl:attribute name="HREF">
                  <xsl:value-of disable-output-escaping="yes" select="SoftwareLink"/>
                </xsl:attribute>
                <xsl:value-of select="SoftwareName"/>
                </A>
          </xsl:otherwise>
            </xsl:choose>
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            <xsl:if test="not(position() = last())">
              <xsl:text disable-output-escaping="yes">|</xsl:text>
            </xsl:if>
          </xsl:if>
          </xsl:for-each>
        </td>
      </tr>
      </table>
    </TD>
    </TR>
   </xsl:when>
   <xsl:otherwise>
    <xsl:if test="Company/DocDownloadCount &gt; 0">
    <TR>
      <TD  CLASS="surr datashade" NOWRAP="NOWRAP" colspan="4">
        <table>
        <tr>
          <td class="default">
            <SPAN CLASS="defaultbold">Click the link to the right to download :</SPAN>
            <xsl:for-each select="Governance">
            <xsl:if test="normalize-space(SoftwareLink)">
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
           <xsl:choose>
                <xsl:when test="../UISettings/ShowDisclaimer = 'true'">
                  <A TARGET="_new" title="{SoftwareName}">
                  <xsl:variable name="keyinstn" select="../Company/KeyInstn" />
                  <xsl:variable name="urljump" select="SoftwareLink"/>
                  <xsl:variable name="url" select="concat('govdocsdisclaimer.aspx?IID=', $keyinstn, '&amp;website=', $urljump)" />
                  <xsl:attribute name="HREF">
                    <xsl:value-of select="$url" />
                  </xsl:attribute>
                    <xsl:value-of select="SoftwareName"/>
                  </A>
                </xsl:when>
                <xsl:otherwise>
                  <A TARGET="_new" title="{SoftwareName}">
                  <xsl:attribute name="HREF">
                    <xsl:value-of disable-output-escaping="yes" select="SoftwareLink"/>
                  </xsl:attribute>
                    <xsl:value-of select="SoftwareName"/>
                  </A>
              </xsl:otherwise>
              </xsl:choose>
            </xsl:if>
            </xsl:for-each>
          </td>
        </tr>
        </table>
      </TD>
    </TR>
    </xsl:if>
   </xsl:otherwise>
   </xsl:choose>

   </xsl:template>

  <!-- Template ONE Ends here. -->


















<!-- Main XSLT templates are here. these are the templates which are actually being displayed in the page. this is for TOP LEVEL editing. if you dont need to use the individual templates above you can do your main editing here when pulling THESE templates below you must carry the top comment with them and uncomment them when you drop it into the company specific xslt.-->
<xsl:template match="irw:Governances">
<xsl:variable name="TemplateName" select="Company/TemplateName"/>
<xsl:choose>
<!-- Template 1 -->
<xsl:when test="$TemplateName = 'AltGovDocs1'">
<xsl:call-template name="TemplateONEstylesheet" />
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" CLASS="">
<xsl:call-template name="Title_S2"/>
  <TR ALIGN="CENTER">
    <TD CLASS="leftTOPbord" colspan="2">
      <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
        <tr>
          <td class="colorlight">
            <table border="0" cellspacing="0" cellpadding="4" width="100%">
             <xsl:if test="Company/ShowEmailNotificationLink = 'true'">
              <xsl:for-each select="CLIENTEMAILPREFS[NotificationType = '40']">
                <xsl:if test="NotificationEnabled = 'True'">
                  <TR>
                    <TD CLASS="data" nowrap="1" colspan="3">
                      <a>
                      <xsl:attribute name="href">
                        email.aspx?IID=<xsl:value-of select="../Company/KeyInstn"/>
                      </xsl:attribute>
                      Notify me of new governance documents posted to this site</a>.
                    </TD>
                  </TR>
                </xsl:if>
                </xsl:for-each>
              </xsl:if>
              <tr>
                <td class="surrleft datashade" NOWRAP="NOWRAP"><span class="defaultbold">Title</span></td>
                <td class="surr_topbot datashade" align="left"><span class="defaultbold">Date</span></td>
                <td class="surrright datashade" align="center"><span class="defaultbold">Format</span></td>
              </tr>
              <xsl:call-template name="Gov_Data2"/>
              <xsl:call-template name="GSW_Data2"/>
            </table>
          </td>
        </tr>
      </TABLE>
    </TD>
  </TR>
    <TR class="default" align="left">
      <TD class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span></TD>
  </TR>
</TABLE>
<xsl:call-template name="Copyright"/>
</xsl:when>
<!-- End Template 1 -->

<!-- Template 3 -->
<xsl:when test="$TemplateName = 'AltGovDocs3'">
<xsl:call-template name="TemplateTHREEstylesheet" />
<xsl:call-template name="Title_T3"/>

      <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%" class="table2">
        <tr>
          <td class="colorlight">
            <table border="0" cellspacing="0" cellpadding="4" width="100%">
              <xsl:if test="Company/ShowEmailNotificationLink = 'true'">
                <xsl:for-each select="CLIENTEMAILPREFS[NotificationType = '40']">
                  <xsl:if test="NotificationEnabled = 'True'">
                    <TR>
                      <TD CLASS="data" nowrap="1" colspan="3">
                        <a><xsl:attribute name="href">email.aspx?IID=<xsl:value-of select="../Company/KeyInstn"/></xsl:attribute>Notify me of new governance documents posted to this site</a>.
                      </TD>
                    </TR>
                  </xsl:if>
                </xsl:for-each>
              </xsl:if>
              <tr>
                <td colspan="3" valign="top">
                  <TABLE cellpadding="4" cellspacing="0" width="100%" class="table2">
                  <tr>
                    <td class=" datashade" NOWRAP="NOWRAP"><span class="defaultbold">Title</span></td>
                    <td class=" datashade" align="left"><span class="defaultbold">Date</span></td>
                    <td class=" datashade" align="center"><span class="defaultbold">Format</span></td>
                  </tr>
                    <xsl:call-template name="Gov_Data3"/>
                    <xsl:call-template name="GSW_Data3"/>
                  </TABLE>
                </td>
                </tr>
            </table>
          </td>
        </tr>
        <TR class="default" align="left">
      <TD class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span></TD>
  </TR>
      </TABLE>

<xsl:call-template name="Copyright"/>
</xsl:when>
<!-- End Template 3 -->

  <!-- Template 4 -->
  <xsl:when test="$TemplateName = 'AltGovDocs4'">    
    <xsl:call-template name="GovDocs_4" /> 
  </xsl:when>  
  <!-- End Template 4 -->

<!-- Template Default -->
<xsl:otherwise>
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" CLASS="colordark">
<xsl:call-template name="Title_S1"/>
  <TR ALIGN="CENTER">
    <TD CLASS="colordark" colspan="2">
      <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
        <tr>
          <td class="colorlight">
            <table border="0" cellspacing="0" cellpadding="4" width="100%">
              <xsl:if test="Company/ShowEmailNotificationLink = 'true'">
                <xsl:for-each select="CLIENTEMAILPREFS[NotificationType = '40']">
                  <xsl:if test="NotificationEnabled = 'True'">
                    <TR>
                      <TD CLASS="data" nowrap="1" colspan="3">
                        <a>
                        <xsl:attribute name="href">
                          email.aspx?IID=<xsl:value-of select="../Company/KeyInstn"/>
                        </xsl:attribute>
                        Notify me of new governance documents posted to this site</a>.
                      </TD>
                    </TR>
                  </xsl:if>
                </xsl:for-each>
              </xsl:if>
              <tr>
                <td class="header" NOWRAP="NOWRAP">Title</td>
                <td class="header" align="left">Date</td>
                <td class="header" align="center">Format</td>
                <!--<td class="header">File Size</td>-->
              </tr>
              <xsl:call-template name="Gov_Data"/>
              <xsl:call-template name="GSW_Data"/>
            </table>
          </td>
        </tr>
      </TABLE>
    </TD>
  </TR>
    <TR class="default" align="left">
      <TD class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span></TD>
  </TR>
</TABLE>
<xsl:call-template name="Copyright"/>
</xsl:otherwise>
</xsl:choose>
</xsl:template>
  
<!-- GovernancesDocs Template Style 4 -->
  <xsl:template name="GovDocs_4">
    <xsl:call-template name="GovDocsScriptResource_4"/>
	  <div id="outer">
		  <div id="Docs">			
			  <xsl:call-template name="Toolkit_Section_4"/>
			  <div id="FinDocSection">
				  <xsl:call-template name="Title_Head_4"/>
				  <xsl:call-template name="Header_4"/>
				  <xsl:call-template name="GovDocsDeta_4"/>
				  <xsl:call-template name="Footer_4"/>
				  <xsl:call-template name="Copyright_4"/>
			  </div>
		  </div>		
	  </div>
  </xsl:template>
  
  <xsl:template name="GovDocsScriptResource_4">
    <xsl:variable name="KeyInstn" select="Company/KeyInstn"/>	
    <xsl:call-template name="CommonScript_4" />
    <xsl:call-template name="CommonDocScript_4" />
  </xsl:template>

  <xsl:template name="GovDocsDeta_4">
	<xsl:choose>
	<xsl:when test="count(Governance) &gt; 0">
		<xsl:call-template name="GovDocsDetails_4"/>
		<xsl:call-template name="GovSWData_4"/>
	</xsl:when>
	<xsl:otherwise>
		<div id="FinDoc_Details" class="NoData">
			<xsl:value-of select="TitleInfo/TitleName"/> currently unavailable.
		</div>
	</xsl:otherwise>
	</xsl:choose>
  </xsl:template>

  <xsl:template name="GovDocsDetails_4">
	<div id="FinDoc_Details">
		<table border="0" cellspacing="0" cellpadding="0" width="100%" id="DocsSelectBox">
		<tr>
			<td valign="top">
				<table border="0" cellspacing="0" cellpadding="0" width="100%" class="head_table" id="altdatashade">
				<tr>
					<td class="bold" align="left" nowrap="" width="65%"><span class="bold">Title</span></td>
					<td class="bold" align="left" width="25%"><span class="bold">Date</span></td>
					<td class="bold" align="center" width="10%"><span class="bold">Format</span></td>
				</tr>
				<tbody rel="altdatashade">
				<xsl:for-each select="Governance">
				<tr>
				  <td align="left">
					<a target="_new" title="Select to view the governance" href="{Link}"><xsl:value-of select="Title" disable-output-escaping="yes" /></a>
				  </td>
				  <td align="left"><xsl:value-of select="Date"/></td>
				  <td align="center">
					<xsl:if test="IconLink != ''">
						<a target="_new" href="{IconLink}"><img border="0" src="{IconImage}"  alt=""/><br /></a>
					</xsl:if>
					<xsl:if test="IconLink = ''">
						<img  border="0" src="{IconImage}" alt="" /><br />
					</xsl:if>
				  </td>
				</tr>
			   </xsl:for-each>
				</tbody>
				</table>
			</td>
		</tr>
		</table>
	</div>
  </xsl:template>
  
  <xsl:template name="GovSWData_4">
	<xsl:choose>
	<xsl:when test="Company/DocDownloadCount &gt; 0">
	<div id="FinDoc_download">
		<span class="txt">Click the link to the right to download : </span>
		<xsl:for-each select="Governance[string-length(SoftwareLink) &gt; 0]">
			<xsl:if test="normalize-space(SoftwareLink)">
				<xsl:choose>
				<xsl:when test="../UISettings/ShowDisclaimer = 'true'">
					<a target="_new" title="{SoftwareName}">
					<xsl:variable name="keyinstn" select="../Company/KeyInstn" />
					<xsl:variable name="urljump" select="SoftwareLink"/>
					<xsl:variable name="url" select="concat('govdocsdisclaimer.aspx?IID=', $keyinstn, '&amp;website=', $urljump)" />
					<xsl:attribute name="href"><xsl:value-of select="$url" /></xsl:attribute><xsl:value-of select="SoftwareName"/>
					</a>
				</xsl:when>
				<xsl:otherwise>
					<a target="_new" href="{SoftwareLink}" title="{SoftwareName}"><xsl:value-of select="SoftwareName"/></a>
				</xsl:otherwise>
				</xsl:choose>
				<xsl:if test="not(position() = last())"><span class="pip">|</span></xsl:if>
			</xsl:if>
		</xsl:for-each>
	  </div>
  </xsl:when>
  </xsl:choose>
  </xsl:template>  
  <!-- End GovernancesDocs Template Style 4 -->

</xsl:stylesheet>


