<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns="http://www.w3.ord/TR/REC-html40"
  xmlns:snlo="http://www.snl.com/xml/objects"
  xmlns:snli="http://www.snl.com/xml/items"
  xmlns:irw="http://www.snl.com/xml/irw"
  xmlns:snl="http://www.snl.com/xml">


<xsl:output method="html" media-type="text/html" indent="no"/>
<xsl:decimal-format name="data" NaN="NA"/>


<!-- Start Default Template here-->

<!--
//This is the default template. Most changes are to either remove the company name, ticker, or exchange. you can also change the name of the page or remove this row all together.
//These templates below are broken up how the page is displayed.

 THESE PAGES ARE DISPLAYED AS:

DEFAULT TEMPLATE
FORMAT DATA TABLES
  BUILD PAGE TEMPLATES
    BUILD DATA TABLES


TEMPLATE ONE
FORMAT DATA TABLES
   BUILD PAGE TEMPLATES
    BUILD DATA TABLES

 END TEMPLATES

      PRINT PAGE

Specifically with the Financial Highlights Pages the templates are broken into two catagories. the first is stage one. stage one controls how the columns are put together (links, alignments and so one).
Then stage two. stage two you will notice will NOT have multiple templates. this is just the section responsible for removing, moving, or adding rows within the data. (if a company doesnt want to display and item or wants it in a different section)
if you have any questions please see Jodi, Charlie, Che.

NOTE: all templates at this point are currently the same end NOTE
-->



<!--FORMAT DATA TABLES -->
<xsl:template name="YearHeader">
  <xsl:param name="BP"/>

  <TR CLASS="data" VALIGN="bottom">
    <TD CLASS="data" NOWRAP="" ALIGN="center"><span class="defaultbold"></span></TD>

    <xsl:for-each select="irw:Element/snlo:FinlEOPBank">

      <xsl:if test="snlo:FinlFlowBank/snli:FiscalQuarter[@snl:key='Y']">
        <TD CLASS="data" NOWRAP="" ALIGN="center"><span class="defaultbold"><xsl:value-of select="snlo:FinlFlowBank/snli:FiscalYear"/>Y</span></TD>

        <xsl:for-each select="snlo:FinlFlowBank">
          <xsl:if test="snl:item[@snl:description='FiscalYear_Change']">
            <TD CLASS="data" NOWRAP="" ALIGN="center"><span class="defaultbold">
              <xsl:value-of select="snli:FiscalYear+1"/>/<BR /><xsl:value-of select="snli:FiscalYear"/><BR />
              <xsl:if test="$BP='Y'">Ch (bp)</xsl:if>
              <xsl:if test="$BP!='Y'">Ch (%)</xsl:if>
              </span>
            </TD>
          </xsl:if>
        </xsl:for-each>
      </xsl:if>

      <xsl:if test="snlo:FinlFlowBank/snli:FiscalQuarter[@snl:key!='Y']">
        <TD CLASS="data" NOWRAP="" ALIGN="center"><span class="defaultbold"><xsl:value-of select="snlo:FinlFlowBank/snli:FiscalYear"/> Q<xsl:value-of select="snlo:FinlFlowBank/snli:FiscalQuarter/@snl:key"/></span></TD>

        <xsl:for-each select="snlo:FinlFlowBank">
          <xsl:if test="snl:item[@snl:description='FiscalQuarter_Change']=4">
            <TD CLASS="data" NOWRAP="" ALIGN="center">
              <xsl:if test="$BP='Y'"><SPAN class="defaultbold">Y-Y<BR />Ch (bp)</SPAN></xsl:if>
              <xsl:if test="$BP!='Y'"><SPAN class="defaultbold">Y-Y<BR />Ch (%)</SPAN></xsl:if>
            </TD>
          </xsl:if>

          <xsl:if test="snl:item[@snl:description='FiscalQuarter_Change']=1">
            <TD CLASS="data" NOWRAP="" ALIGN="center">
              <xsl:if test="$BP='Y'"><SPAN class="defaultbold">Q-Q<BR />Ch (bp)</SPAN></xsl:if>
              <xsl:if test="$BP!='Y'"><SPAN class="defaultbold">Q-Q<BR />Ch (%)<sup>&#8224;</sup></SPAN></xsl:if>
            </TD>
          </xsl:if>
        </xsl:for-each>

      </xsl:if>

    </xsl:for-each>
  </TR>
</xsl:template>




<xsl:template name="EOPItem">
  <xsl:param name="Name"/>
  <xsl:param name="Description"/>
  <xsl:param name="FormatString"/>
  <xsl:param name="NegativeFormatString"/>
  <xsl:param name="ChangeFormatString"/>
  <xsl:param name="NegativeChangeFormatString"/>
  <xsl:param name="Asterisk"/>
  <xsl:param name="ChangeText"/>
    <tr CLASS="data" ALIGN="right">
        <td CLASS="data" NOWRAP="" ALIGN="left">
            <a href="#" class="fielddef" onClick="DefWindow({Company/KeyInstn},'{$Name}', 1); return false" title="{$Description}"><xsl:value-of select="$Description"/></a>
            <xsl:if test="$Asterisk=1">
    <xsl:if test="irw:Element/snlo:FinlEOPBank/snlo:FinlFlowBank/snli:FiscalQuarter[@snl:key!='Y']">
      <sup>*</sup>
    </xsl:if>
    </xsl:if>
        </td>
    <xsl:for-each select="irw:Element/snlo:FinlEOPBank">
      <xsl:variable name="Value" select="*[name(.)=concat('snli:',$Name)]"/>
      <td CLASS="data" NOWRAP="">
      <xsl:choose>
      <xsl:when test="$Value[@snl:meaningful]">
      NM
      </xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="$Value">
          <xsl:choose>
            <xsl:when test="$Value>=0">
              <xsl:value-of select="format-number($Value,$FormatString,'data')"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="format-number(-$Value,$NegativeFormatString,'data')"/>
            </xsl:otherwise>
          </xsl:choose>

          </xsl:when>
          <xsl:otherwise>NA</xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <xsl:for-each select="snlo:FinlFlowBank">
        <xsl:variable name="Change" select="snl:item[@snl:description=concat($Name,'_Change')]"/>
        <xsl:if test="$Change">
          <xsl:if test="format-number($Change, $ChangeFormatString)='NaN'">
            <td NOWRAP="" CLASS="datashade">
              <xsl:value-of select="$Change"/>
            </td>
          </xsl:if>
          <xsl:if test="format-number($Change, $ChangeFormatString)!='NaN'">
            <td NOWRAP="" CLASS="datashade">
              <xsl:choose>
                <xsl:when test="$Change>=0">
                  <xsl:value-of select="format-number($Change,$ChangeFormatString,'data')"/><xsl:value-of select="$ChangeText"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="format-number(-$Change,$NegativeChangeFormatString,'data')"/><xsl:value-of select="$ChangeText"/>
                </xsl:otherwise>
              </xsl:choose>
            </td>
          </xsl:if>
        </xsl:if>
      </xsl:for-each>
    </xsl:for-each>
  </tr>
</xsl:template>




<xsl:template name="FlowItem">
  <xsl:param name="Name"/>
  <xsl:param name="Description"/>
  <xsl:param name="FormatString"/>
  <xsl:param name="NegativeFormatString"/>
  <xsl:param name="ChangeFormatString"/>
  <xsl:param name="NegativeChangeFormatString"/>
  <xsl:param name="Asterisk"/>
  <xsl:param name="ChangeText"/>
    <tr CLASS="data" ALIGN="right">
        <td CLASS="data" NOWRAP="" ALIGN="left">
            <a href="#" class="fielddef" onClick="DefWindow({Company/KeyInstn},'{$Name}', 0); return false" title="{$Description}"><xsl:value-of select="$Description"/></a>
            <xsl:if test="$Asterisk=1"><xsl:if test="irw:Element/snlo:FinlEOPBank/snlo:FinlFlowBank/snli:FiscalQuarter[@snl:key!='Y']"><sup>*</sup></xsl:if></xsl:if>
        </td>
    <xsl:for-each select="irw:Element/snlo:FinlEOPBank">
      <xsl:variable name="Value" select="descendant::*[name(.)=concat('snli:',$Name)]"/>
      <td CLASS="data" NOWRAP="">
      <xsl:choose>
      <xsl:when test="$Value[@snl:meaningful]">
      NM
      </xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="$Value">
          <xsl:choose>
            <xsl:when test="$Value>=0">
              <xsl:value-of select="format-number($Value,$FormatString,'data')"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="format-number(-$Value,$NegativeFormatString,'data')"/>
            </xsl:otherwise>
          </xsl:choose>

          </xsl:when>
          <xsl:otherwise>NA</xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <xsl:for-each select="snlo:FinlFlowBank">
        <xsl:variable name="Change" select="snl:item[@snl:description=concat($Name,'_Change')]"/>
        <xsl:if test="$Change">
          <xsl:if test="format-number($Change, $ChangeFormatString)='NaN'">
            <td NOWRAP="" CLASS="datashade">
              <xsl:value-of select="$Change"/>
            </td>
          </xsl:if>
          <xsl:if test="format-number($Change, $ChangeFormatString)!='NaN'">
            <td NOWRAP="" CLASS="datashade">
            <xsl:choose>
              <xsl:when test="$Change>=0">
                <xsl:value-of select="format-number($Change,$ChangeFormatString,'data')"/><xsl:value-of select="$ChangeText"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="format-number(-$Change,$NegativeChangeFormatString,'data')"/><xsl:value-of select="$ChangeText"/>
              </xsl:otherwise>
            </xsl:choose>
            </td>
          </xsl:if>
        </xsl:if>
      </xsl:for-each>
    </xsl:for-each>
  </tr>
</xsl:template>



<xsl:template name="AsReportedItem">
  <xsl:param name="Name"/>
  <xsl:param name="Description"/>
  <xsl:param name="FormatString"/>
  <xsl:param name="NegativeFormatString"/>
  <xsl:param name="ChangeFormatString"/>
  <xsl:param name="NegativeChangeFormatString"/>
  <xsl:param name="Asterisk"/>
  <xsl:param name="EOP"/>
  <xsl:param name="ChangeText"/>
    <tr CLASS="data" ALIGN="right">
    <xsl:variable name="UseAsReported" select="descendant::snl:item[@snl:description=concat($Name,'Reported_UseAsReported')]"/>
        <td CLASS="data" NOWRAP="" ALIGN="left">
      <xsl:choose>
        <xsl:when test="$EOP=1">
                <a href="#" class="fielddef" onClick="DefWindow({Company/KeyInstn},'{$Name}', 1); return false" title="{$Description}"><xsl:value-of select="$Description"/></a>
        </xsl:when>
        <xsl:otherwise>
                <a href="#" class="fielddef" onClick="DefWindow({Company/KeyInstn},'{$Name}', 0); return false" title="{$Description}"><xsl:value-of select="$Description"/></a>
        </xsl:otherwise>
      </xsl:choose>
            <xsl:if test="$Asterisk=1"><xsl:if test="irw:Element/snlo:FinlEOPBank/snlo:FinlFlowBank/snli:FiscalQuarter[@snl:key!='Y']"><sup>*</sup></xsl:if></xsl:if>
        </td>
    <xsl:for-each select="irw:Element/snlo:FinlEOPBank">
      <xsl:choose>
        <xsl:when test="$UseAsReported">
          <xsl:variable name="Value" select="descendant::*[name(.)=concat('snli:',$Name, 'Reported')]"/>
          <td CLASS="data" NOWRAP="">
            <xsl:choose>
            <xsl:when test="$Value[@snl:meaningful]">NM</xsl:when>
            <xsl:otherwise>
              <xsl:choose>
                <xsl:when test="$Value">
                  <xsl:choose>
                    <xsl:when test="$Value>=0">
                      <xsl:value-of select="format-number($Value,$FormatString,'data')"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="format-number(-$Value,$NegativeFormatString,'data')"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:when>
                <xsl:otherwise>NA</xsl:otherwise>
              </xsl:choose>
            </xsl:otherwise>
            </xsl:choose>
          </td>
        </xsl:when>
        <xsl:otherwise><xsl:variable name="Value" select="descendant::*[name(.)=concat('snli:',$Name)]"/>     <td CLASS="data" NOWRAP="">
      <xsl:choose>
      <xsl:when test="$Value[@snl:meaningful]">NM</xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="$Value">
          <xsl:choose>
            <xsl:when test="$Value>=0">
              <xsl:value-of select="format-number($Value,$FormatString,'data')"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="format-number(-$Value,$NegativeFormatString,'data')"/>
            </xsl:otherwise>
          </xsl:choose>

          </xsl:when>
          <xsl:otherwise>NA</xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
      </xsl:choose>
      </td>
</xsl:otherwise>
      </xsl:choose>
      <xsl:for-each select="snlo:FinlFlowBank">
      <xsl:choose>
        <xsl:when test="$UseAsReported"><xsl:variable name="Change" select="snl:item[@snl:description=concat($Name,'Reported_Change')]"/>       <xsl:if test="$Change">
          <xsl:if test="format-number($Change, $ChangeFormatString)='NaN'">
            <td NOWRAP="" CLASS="datashade">
              <xsl:value-of select="$Change"/>
            </td>
          </xsl:if>
          <xsl:if test="format-number($Change, $ChangeFormatString)!='NaN'">
            <td NOWRAP="" CLASS="datashade">
            <xsl:choose>
              <xsl:when test="$Change>=0">
                <xsl:value-of select="format-number($Change,$ChangeFormatString,'data')"/><xsl:value-of select="$ChangeText"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="format-number(-$Change,$NegativeChangeFormatString,'data')"/><xsl:value-of select="$ChangeText"/>
              </xsl:otherwise>
            </xsl:choose>
            </td>
          </xsl:if>
        </xsl:if>
</xsl:when>
        <xsl:otherwise><xsl:variable name="Change" select="snl:item[@snl:description=concat($Name,'_Change')]"/>
                <xsl:if test="$Change">
          <xsl:if test="format-number($Change, $ChangeFormatString)='NaN'">
            <td NOWRAP="" CLASS="datashade">
              <xsl:value-of select="$Change"/>
            </td>
          </xsl:if>
          <xsl:if test="format-number($Change, $ChangeFormatString)!='NaN'">
            <td NOWRAP="" CLASS="datashade">
            <xsl:choose>
              <xsl:when test="$Change>=0">
                <xsl:value-of select="format-number($Change,$ChangeFormatString,'data')"/><xsl:value-of select="$ChangeText"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="format-number(-$Change,$NegativeChangeFormatString,'data')"/><xsl:value-of select="$ChangeText"/>
              </xsl:otherwise>
            </xsl:choose>
            </td>
          </xsl:if>
        </xsl:if>
        </xsl:otherwise>
      </xsl:choose>
      </xsl:for-each>
    </xsl:for-each>
  </tr>
</xsl:template>

<!--END FORMAT DATA TABLES -->







<xsl:template name="Trend_title">
<xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
<tr>
<td CLASS="colordark" nowrap="1"><span CLASS="title1light"><xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/></span></td>
<td CLASS="colordark" nowrap="1" align="right" valign="top"><span class="subtitlelight"><xsl:value-of select="$CompanyNameCaps"/>&#160;(<xsl:value-of select="Company/Exchange"/>&#160;-&#160;<xsl:value-of select="Company/Ticker"/>)</span><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
</tr>
<tr>
<td CLASS="colorlight" colspan="2">
<span class="default"><xsl:value-of select="Company/Header" disable-output-escaping="yes"/>
</span>
</td>
</tr>
</xsl:template>

<xsl:template name="Trend_data">
<tr>
<td CLASS="colorlight"><span class="default">
<br />For the definition of a financial field, click on the field name.</span><br /><br />
<table BORDER="0" CELLSPACING="1" CELLPADDING="3" WIDTH="100%" class="header">
<irw:SectionList>
<xsl:call-template name="Trend_balancesheet" />
<xsl:call-template name="Trend_income" />
<xsl:call-template name="Trend_pershare" />
<xsl:call-template name="Trend_performance" />
<xsl:call-template name="Trend_balance" />
<xsl:call-template name="Trend_assetratio" />
<xsl:call-template name="Trend_regratios" />
</irw:SectionList>
</table>
</td>
</tr>
</xsl:template>

<xsl:template name="Trend_switch">
<tr>
<td CLASS="colorlight" NOWRAP="" VALIGN="top">
<xsl:if test="irw:Element/snlo:FinlEOPBank/snlo:FinlFlowBank/snli:FiscalQuarter[@snl:key='Y']">
<SPAN CLASS="title2">Trend Analysis--Annual (<A><xsl:attribute name="href">./Finl.aspx?annual=0&amp;IID=<xsl:value-of select="Company/KeyInstn" /></xsl:attribute><SPAN CLASS="title2">click here for Quarterly Analysis</SPAN></A>)</SPAN>
</xsl:if>
<xsl:if test="irw:Element/snlo:FinlEOPBank/snlo:FinlFlowBank/snli:FiscalQuarter[@snl:key!='Y']">
<SPAN CLASS="title2">Trend Analysis--Quarterly (<A><xsl:attribute name="href">./Finl.aspx?annual=1&amp;IID=<xsl:value-of select="Company/KeyInstn" /></xsl:attribute><SPAN CLASS="title2">click here for Annual Analysis</SPAN></A>)</SPAN>
</xsl:if>
</td>
</tr>
</xsl:template>


<xsl:template name="Trend_footer">
<xsl:if test="irw:Element/snlo:FinlEOPBank/snlo:FinlFlowBank/snli:FiscalQuarter[@snl:key!='Y']">
<tr><td class="colorlight" colspan="6"><span class="default">&#160;&#160;<sup>&#8224;</sup>&#160;Q-Q Ch(%)= most recent quarter minus prior quarter annualized e.g. [(Q4-Q3)/Q3]*4</span><P><table cellpadding="0" cellspacing="0" width="0"><tr><td valign="top">&#160;&#160;*&#160;</td><td><span class="default">Percentages presented for individual quarters are annualized by taking the quarter amount and multiplying by four.</span></td></tr></table></P><br /></td></tr>
</xsl:if>
</xsl:template>




<xsl:template name="Trend_regratios">
    <irw:Section name="RegulatoryCapitalData">
      <irw:SectionHeader>
        <tr>
          <td NOWRAP="" CLASS="header" COLSPAN="7">Regulatory Capital Ratios (%)</td>
        </tr>
        <xsl:call-template name="YearHeader">
          <xsl:with-param name="BP">Y</xsl:with-param>
        </xsl:call-template>
      </irw:SectionHeader>

      <irw:SectionItems>
        <xsl:call-template name="EOPItem">
          <xsl:with-param name="Name">T1RatioAsRegBOCalc</xsl:with-param>
          <xsl:with-param name="Description">Tier 1 Capital Ratio</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0)</xsl:with-param>
        </xsl:call-template>
      </irw:SectionItems>
      <irw:SectionFooter>
        <tr>
          <td CLASS="colorlight" COLSPAN="7" height="7"></td>
        </tr>
      </irw:SectionFooter>
    </irw:Section>
  </xsl:template>


<xsl:template name="Trend_assetratio">
    <irw:Section name="AssetQualityRatios">
      <irw:SectionHeader>
        <tr>
          <td NOWRAP="" CLASS="header" COLSPAN="7">Asset Quality Ratios (%)</td>
        </tr>
        <xsl:call-template name="YearHeader">
          <xsl:with-param name="BP">Y</xsl:with-param>
        </xsl:call-template>
      </irw:SectionHeader>
      <irw:SectionItems>
        <xsl:call-template name="AsReportedItem">
          <xsl:with-param name="Name">NPAsToAssets</xsl:with-param>
          <xsl:with-param name="Description">Nonperforming Assets/ Assets</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="EOP">1</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="EOPItem">
          <xsl:with-param name="Name">ReservesToLoans</xsl:with-param>
          <xsl:with-param name="Description">Loan Loss Reserves/ Gross Loans</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="EOPItem">
          <xsl:with-param name="Name">ReservesToNPAs</xsl:with-param>
          <xsl:with-param name="Description">Loan Loss Reserves/ NPAs</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="FlowItem">
          <xsl:with-param name="Name">NetChargeoffsToLoans</xsl:with-param>
          <xsl:with-param name="Description">Net Charge-offs/ Avg Loans</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0)</xsl:with-param>
        </xsl:call-template>
      </irw:SectionItems>

      <irw:SectionFooter>
        <tr>
          <td CLASS="colorlight" COLSPAN="7" height="7"></td>
        </tr>
      </irw:SectionFooter>
    </irw:Section>
</xsl:template>


<xsl:template name="Trend_balance">
    <irw:Section name="BalanceSheetRatios">
      <irw:SectionHeader>
        <tr>
          <td NOWRAP="" CLASS="header" COLSPAN="7">Balance Sheet Ratios (%)</td>
        </tr>
        <xsl:call-template name="YearHeader">
          <xsl:with-param name="BP">Y</xsl:with-param>
        </xsl:call-template>
      </irw:SectionHeader>
      <irw:SectionItems>
        <xsl:call-template name="EOPItem">
          <xsl:with-param name="Name">TangibleEquityToAssets</xsl:with-param>
          <xsl:with-param name="Description">Tangible Equity/ Tangible Assets</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="EOPItem">
          <xsl:with-param name="Name">EquityToAssets</xsl:with-param>
          <xsl:with-param name="Description">Equity/ Assets</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0)</xsl:with-param>
        </xsl:call-template>
      </irw:SectionItems>
      <irw:SectionFooter>
      </irw:SectionFooter>
    </irw:Section>
</xsl:template>


<xsl:template name="Trend_performance">
    <irw:Section name="PerformanceRatios">
      <irw:SectionHeader>
        <tr>
          <td NOWRAP="" CLASS="header" COLSPAN="7">Performance Ratios (%)</td>
        </tr>
        <xsl:call-template name="YearHeader">
          <xsl:with-param name="BP">Y</xsl:with-param>
        </xsl:call-template>
      </irw:SectionHeader>
      <irw:SectionItems>
        <xsl:call-template name="AsReportedItem">
          <xsl:with-param name="Name">ROAA</xsl:with-param>
          <xsl:with-param name="Description">ROAA</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="AsReportedItem">
          <xsl:with-param name="Name">ROAE</xsl:with-param>
          <xsl:with-param name="Description">ROAE</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="AsReportedItem">
          <xsl:with-param name="Name">NetInterestMargin</xsl:with-param>
          <xsl:with-param name="Description">Net Interest Margin</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="EOPItem">
          <xsl:with-param name="Name">InvLoansToDeposits</xsl:with-param>
          <xsl:with-param name="Description">Loans / Deposits</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="AsReportedItem">
          <xsl:with-param name="Name">EfficiencyRatio</xsl:with-param>
          <xsl:with-param name="Description">Efficiency Ratio</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
        </xsl:call-template>
      </irw:SectionItems>
      <irw:SectionFooter>
        <tr>
          <td CLASS="colorlight" COLSPAN="7" height="7"></td>
        </tr>
      </irw:SectionFooter>
    </irw:Section>
        </xsl:template>

<xsl:template name="Trend_pershare">
    <irw:Section name="PerShareItems">
      <irw:SectionHeader>
        <tr>
          <td NOWRAP="" CLASS="header" COLSPAN="7">Per Share Items ($)</td>
        </tr>
        <xsl:call-template name="YearHeader">
          <xsl:with-param name="BP">N</xsl:with-param>
        </xsl:call-template>
      </irw:SectionHeader>
      <irw:SectionItems>
        <xsl:call-template name="EOPItem">
          <xsl:with-param name="Name">EquityPerShareForCalcs</xsl:with-param>
          <xsl:with-param name="Description">Book Value per Share</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="EOPItem">
          <xsl:with-param name="Name">TangibleEquityPerShare</xsl:with-param>
          <xsl:with-param name="Description">Tangible Book Value</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="FlowItem">
          <xsl:with-param name="Name">EPSDiluted</xsl:with-param>
          <xsl:with-param name="Description">Diluted EPS Before Extra</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="FlowItem">
          <xsl:with-param name="Name">EPSDilutedAfterExtra</xsl:with-param>
          <xsl:with-param name="Description">Diluted EPS After Extra</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="FlowItem">
          <xsl:with-param name="Name">Dividend</xsl:with-param>
          <xsl:with-param name="Description">Dividends Declared</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.0000</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.0000)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
      </irw:SectionItems>
      <irw:SectionFooter>
        <tr>
          <td CLASS="colorlight" COLSPAN="7" height="7"></td>
        </tr>
      </irw:SectionFooter>
    </irw:Section>
</xsl:template>



<xsl:template name="Trend_income">
    <irw:Section name="IncomeStatement">
        <irw:SectionHeader>
        <tr>
          <td NOWRAP="" CLASS="header" COLSPAN="7">Income Statement ($000)</td>
        </tr>
        <xsl:call-template name="YearHeader">
          <xsl:with-param name="BP">N</xsl:with-param>
        </xsl:call-template>
      </irw:SectionHeader>
      <irw:SectionItems>
        <xsl:call-template name="FlowItem">
          <xsl:with-param name="Name">NetInterestIncome</xsl:with-param>
          <xsl:with-param name="Description">Net Interest Income</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="FlowItem">
          <xsl:with-param name="Name">ProvisionForLoanLosses</xsl:with-param>
          <xsl:with-param name="Description">Provision for Loan Losses</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="FlowItem">
          <xsl:with-param name="Name">NonInterestIncome</xsl:with-param>
          <xsl:with-param name="Description">Noninterest Income</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="FlowItem">
          <xsl:with-param name="Name">NonInterestExpense</xsl:with-param>
          <xsl:with-param name="Description">Noninterest Expense</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="FlowItem">
          <xsl:with-param name="Name">NetIncomeBeforeTaxes</xsl:with-param>
          <xsl:with-param name="Description">Net Income Before Taxes</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="FlowItem">
          <xsl:with-param name="Name">Taxes</xsl:with-param>
          <xsl:with-param name="Description">Provision for Taxes</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="FlowItem">
          <xsl:with-param name="Name">NetIncome</xsl:with-param>
          <xsl:with-param name="Description">Net Income</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
      </irw:SectionItems>
      <irw:SectionFooter>
        <tr>
          <td CLASS="colorlight" COLSPAN="7" height="7"></td>
        </tr>
      </irw:SectionFooter>
    </irw:Section>
</xsl:template>

<xsl:template name="Trend_balancesheet">
    <irw:Section name="BalanceSheet">
      <irw:SectionHeader>
        <tr>
          <td NOWRAP="" CLASS="header" COLSPAN="7">Balance Sheet ($000)</td>
        </tr>
        <xsl:call-template name="YearHeader">
          <xsl:with-param name="BP">N</xsl:with-param>
        </xsl:call-template>
      </irw:SectionHeader>
      <irw:SectionItems>
        <xsl:call-template name="EOPItem">
          <xsl:with-param name="Name">InvLoansBefReserve</xsl:with-param>
          <xsl:with-param name="Description">Loans Held for Investment, before Reserves</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="EOPItem">
          <xsl:with-param name="Name">LoansHeldForSaleBefReserve</xsl:with-param>
          <xsl:with-param name="Description">Loans Held for Sale, before Reserves</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="EOPItem">
          <xsl:with-param name="Name">ReserveForLoanLosses</xsl:with-param>
          <xsl:with-param name="Description">Loan Loss Reserve</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="EOPItem">
          <xsl:with-param name="Name">NetLoans</xsl:with-param>
          <xsl:with-param name="Description">Net Loans Receivable</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="EOPItem">
          <xsl:with-param name="Name">Intangibles</xsl:with-param>
          <xsl:with-param name="Description">Intangible Assets</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="EOPItem">
          <xsl:with-param name="Name">Assets</xsl:with-param>
          <xsl:with-param name="Description">Total Assets</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="EOPItem">
          <xsl:with-param name="Name">Deposits</xsl:with-param>
          <xsl:with-param name="Description">Deposits</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="EOPItem">
          <xsl:with-param name="Name">DebtAndSubDebt</xsl:with-param>
          <xsl:with-param name="Description">Debt, Senior and Subordinated</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <!--<xsl:call-template name="EOPItem">
          <xsl:with-param name="Name">TrustPreferredSecurities150</xsl:with-param>
          <xsl:with-param name="Description">Trust Preferred Securities (FASB 150)</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="EOPItem">
          <xsl:with-param name="Name">TrustPreferredSecurities</xsl:with-param>
          <xsl:with-param name="Description">Trust Preferred Securities (Pre FASB 150)</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="EOPItem">
          <xsl:with-param name="Name">TrustPreferredSecuritiesTotal</xsl:with-param>
          <xsl:with-param name="Description">Total Trust Preferred</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>-->
        <xsl:call-template name="EOPItem">
          <xsl:with-param name="Name">EquityCommon</xsl:with-param>
          <xsl:with-param name="Description">Common Equity</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="EOPItem">
          <xsl:with-param name="Name">Equity</xsl:with-param>
          <xsl:with-param name="Description">Total Shareholders' Equity</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="EOPItem">
          <xsl:with-param name="Name">SharesOutstanding</xsl:with-param>
          <xsl:with-param name="Description">Shares Outstanding (actual)</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
      </irw:SectionItems>

      <irw:SectionFooter>
        <tr>
          <td CLASS="colorlight" COLSPAN="7" height="7"> </td>
        </tr>
      </irw:SectionFooter>
    </irw:Section>
    </xsl:template>
<!-- END -->



<!-- Template 1 START Here -->



<xsl:template name="Trend_data1">
<tr>
<td CLASS="colorlight"><span class="default">
<br />For the definition of a financial field, click on the field name.</span><br /><br />

<irw:SectionList>
<table BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
<xsl:call-template name="Trend_balancesheet1" />
<xsl:call-template name="Trend_income1" />
<xsl:call-template name="Trend_pershare1" />
<xsl:call-template name="Trend_performance1" />
<xsl:call-template name="Trend_balance1" />
<xsl:call-template name="Trend_assetratio1" />
<xsl:call-template name="Trend_regratios1" />
</table>
</irw:SectionList>

</td>
</tr>
</xsl:template>

<xsl:template name="YearHeader1">
  <xsl:param name="BP"/>

  <TR CLASS=" data" ALIGN="center">
    <TD CLASS=" data" NOWRAP="" ALIGN="center" width="45"><span class="defaultbold">&#160;</span></TD>

    <xsl:for-each select="irw:Element/snlo:FinlEOPBank">

      <xsl:if test="snlo:FinlFlowBank/snli:FiscalQuarter[@snl:key='Y']">
        <TD CLASS=" data" NOWRAP="" ALIGN="center" width="45"><span class="defaultbold"><xsl:value-of select="snlo:FinlFlowBank/snli:FiscalYear"/>Y</span></TD>

        <xsl:for-each select="snlo:FinlFlowBank">
          <xsl:if test="snl:item[@snl:description='FiscalYear_Change']">
            <TD CLASS=" data" NOWRAP="" ALIGN="center" width="45"><span class="defaultbold">
              <xsl:value-of select="snli:FiscalYear+1"/>/<BR /><xsl:value-of select="snli:FiscalYear"/><BR />
              <xsl:if test="$BP='Y'">Ch (bp)</xsl:if>
              <xsl:if test="$BP!='Y'">Ch (%)</xsl:if>
              </span>
            </TD>
          </xsl:if>
        </xsl:for-each>
      </xsl:if>

      <xsl:if test="snlo:FinlFlowBank/snli:FiscalQuarter[@snl:key!='Y']">
        <TD CLASS=" data" NOWRAP="" ALIGN="center" width="45"><span class="defaultbold"><xsl:value-of select="snlo:FinlFlowBank/snli:FiscalYear"/> Q<xsl:value-of select="snlo:FinlFlowBank/snli:FiscalQuarter/@snl:key"/></span></TD>

        <xsl:for-each select="snlo:FinlFlowBank">
          <xsl:if test="snl:item[@snl:description='FiscalQuarter_Change']=4">
            <TD CLASS=" data" NOWRAP="" ALIGN="center" width="45">
              <xsl:if test="$BP='Y'"><SPAN class="defaultbold">Y-Y<BR />Ch (bp)</SPAN></xsl:if>
              <xsl:if test="$BP!='Y'"><SPAN class="defaultbold">Y-Y<BR />Ch (%)</SPAN></xsl:if>
            </TD>
          </xsl:if>

          <xsl:if test="snl:item[@snl:description='FiscalQuarter_Change']=1">
            <TD CLASS=" data" NOWRAP="" ALIGN="center" width="45">
              <xsl:if test="$BP='Y'"><SPAN class="defaultbold">Q-Q<BR />Ch (bp)</SPAN></xsl:if>
              <xsl:if test="$BP!='Y'"><SPAN class="defaultbold">Q-Q<BR />Ch (%)<sup>&#8224;</sup></SPAN></xsl:if>
            </TD>
          </xsl:if>
        </xsl:for-each>

      </xsl:if>

    </xsl:for-each>
  </TR>
</xsl:template>


<xsl:template name="Trend_balancesheet1">
  <tr>
    <td>
      <table BORDER="0"  CELLSPACING="0" CELLPADDING="3" WIDTH="100%">
    <irw:Section name="BalanceSheet">
      <irw:SectionHeader>
        <tr>
          <td NOWRAP="" class="datashade surr" COLSPAN="7">Balance Sheet ($000)</td>
        </tr>
        <xsl:call-template name="YearHeader1">
          <xsl:with-param name="BP">N</xsl:with-param>
        </xsl:call-template>
      </irw:SectionHeader>
      <irw:SectionItems>
        <xsl:call-template name="EOPItem1">
          <xsl:with-param name="Name">InvLoansBefReserve</xsl:with-param>
          <xsl:with-param name="Description">Loans Held for Investment, before Reserves</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="EOPItem1">
          <xsl:with-param name="Name">LoansHeldForSaleBefReserve</xsl:with-param>
          <xsl:with-param name="Description">Loans Held for Sale, before Reserves</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="EOPItem1">
          <xsl:with-param name="Name">ReserveForLoanLosses</xsl:with-param>
          <xsl:with-param name="Description">Loan Loss Reserve</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="EOPItem1">
          <xsl:with-param name="Name">NetLoans</xsl:with-param>
          <xsl:with-param name="Description">Net Loans Receivable</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="EOPItem1">
          <xsl:with-param name="Name">Intangibles</xsl:with-param>
          <xsl:with-param name="Description">Intangible Assets</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="EOPItem1">
          <xsl:with-param name="Name">Assets</xsl:with-param>
          <xsl:with-param name="Description">Total Assets</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="EOPItem1">
          <xsl:with-param name="Name">Deposits</xsl:with-param>
          <xsl:with-param name="Description">Deposits</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="EOPItem1">
          <xsl:with-param name="Name">DebtAndSubDebt</xsl:with-param>
          <xsl:with-param name="Description">Debt, Senior and Subordinated</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="EOPItem1">
          <xsl:with-param name="Name">EquityCommon</xsl:with-param>
          <xsl:with-param name="Description">Common Equity</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="EOPItem1">
          <xsl:with-param name="Name">Equity</xsl:with-param>
          <xsl:with-param name="Description">Total Shareholders' Equity</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="EOPItem1">
          <xsl:with-param name="Name">SharesOutstanding</xsl:with-param>
          <xsl:with-param name="Description">Shares Outstanding (actual)</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
      </irw:SectionItems>
    </irw:Section>
    </table>
    </td>
  </tr>
  <tr>
    <td height="10"></td>
  </tr>

</xsl:template>

<xsl:template name="Trend_income1">
<tr>
    <td>
      <table BORDER="0"  CELLSPACING="0" CELLPADDING="3" WIDTH="100%">
    <irw:Section name="IncomeStatement">
        <irw:SectionHeader>
        <tr>
          <td NOWRAP="" class="datashade surr" COLSPAN="7">Income Statement ($000)</td>
        </tr>
        <xsl:call-template name="YearHeader1">
          <xsl:with-param name="BP">N</xsl:with-param>
        </xsl:call-template>
      </irw:SectionHeader>
      <irw:SectionItems>
        <xsl:call-template name="FlowItem1">
          <xsl:with-param name="Name">NetInterestIncome</xsl:with-param>
          <xsl:with-param name="Description">Net Interest Income</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="FlowItem1">
          <xsl:with-param name="Name">ProvisionForLoanLosses</xsl:with-param>
          <xsl:with-param name="Description">Provision for Loan Losses</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="FlowItem1">
          <xsl:with-param name="Name">NonInterestIncome</xsl:with-param>
          <xsl:with-param name="Description">Noninterest Income</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="FlowItem1">
          <xsl:with-param name="Name">NonInterestExpense</xsl:with-param>
          <xsl:with-param name="Description">Noninterest Expense</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="FlowItem1">
          <xsl:with-param name="Name">NetIncomeBeforeTaxes</xsl:with-param>
          <xsl:with-param name="Description">Net Income Before Taxes</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="FlowItem1">
          <xsl:with-param name="Name">Taxes</xsl:with-param>
          <xsl:with-param name="Description">Provision for Taxes</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="FlowItem1">
          <xsl:with-param name="Name">NetIncome</xsl:with-param>
          <xsl:with-param name="Description">Net Income</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
      </irw:SectionItems>
    </irw:Section>
  </table>
      </td>
    </tr>
    <tr>
      <td height="10"></td>
    </tr>

</xsl:template>


<xsl:template name="Trend_pershare1">
<tr>
    <td>
      <table BORDER="0"  CELLSPACING="0" CELLPADDING="3" WIDTH="100%">
    <irw:Section name="PerShareItems">
      <irw:SectionHeader>
        <tr>
          <td NOWRAP="" class="datashade surr" COLSPAN="7">Per Share Items ($)</td>
        </tr>
        <xsl:call-template name="YearHeader1">
          <xsl:with-param name="BP">N</xsl:with-param>
        </xsl:call-template>
      </irw:SectionHeader>
      <irw:SectionItems>
        <xsl:call-template name="EOPItem1">
          <xsl:with-param name="Name">EquityPerShareForCalcs</xsl:with-param>
          <xsl:with-param name="Description">Book Value per Share</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="EOPItem1">
          <xsl:with-param name="Name">TangibleEquityPerShare</xsl:with-param>
          <xsl:with-param name="Description">Tangible Book Value</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="FlowItem1">
          <xsl:with-param name="Name">EPSDiluted</xsl:with-param>
          <xsl:with-param name="Description">Diluted EPS Before Extra</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="FlowItem1">
          <xsl:with-param name="Name">EPSDilutedAfterExtra</xsl:with-param>
          <xsl:with-param name="Description">Diluted EPS After Extra</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="FlowItem1">
          <xsl:with-param name="Name">Dividend</xsl:with-param>
          <xsl:with-param name="Description">Dividends Declared</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.0000</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.0000)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
      </irw:SectionItems>
    </irw:Section>
    </table>
          </td>
        </tr>
        <tr>
          <td height="10"></td>
    </tr>
</xsl:template>

<xsl:template name="Trend_performance1">
<tr>
    <td>
      <table BORDER="0"  CELLSPACING="0" CELLPADDING="3" WIDTH="100%">
    <irw:Section name="PerformanceRatios">
      <irw:SectionHeader>
        <tr>
          <td NOWRAP="" class="datashade surr" COLSPAN="7">Performance Ratios (%)</td>
        </tr>
        <xsl:call-template name="YearHeader1">
          <xsl:with-param name="BP">Y</xsl:with-param>
        </xsl:call-template>
      </irw:SectionHeader>
      <irw:SectionItems>
        <xsl:call-template name="AsReportedItem1">
          <xsl:with-param name="Name">ROAA</xsl:with-param>
          <xsl:with-param name="Description">ROAA</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="AsReportedItem1">
          <xsl:with-param name="Name">ROAE</xsl:with-param>
          <xsl:with-param name="Description">ROAE</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="AsReportedItem1">
          <xsl:with-param name="Name">NetInterestMargin</xsl:with-param>
          <xsl:with-param name="Description">Net Interest Margin</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="EOPItem1">
          <xsl:with-param name="Name">InvLoansToDeposits</xsl:with-param>
          <xsl:with-param name="Description">Loans / Deposits</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="AsReportedItem1">
          <xsl:with-param name="Name">EfficiencyRatio</xsl:with-param>
          <xsl:with-param name="Description">Efficiency Ratio</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
        </xsl:call-template>
      </irw:SectionItems>
    </irw:Section>
    </table>
    </td>
  </tr>
  <tr>
    <td height="10"></td>
</tr>
</xsl:template>

<xsl:template name="Trend_balance1">
<tr>
  <td>
    <table BORDER="0"  CELLSPACING="0" CELLPADDING="3" WIDTH="100%">
    <irw:Section name="BalanceSheetRatios">
      <irw:SectionHeader>
        <tr>
          <td NOWRAP="" class="datashade surr" COLSPAN="7">Balance Sheet Ratios (%)</td>
        </tr>
        <xsl:call-template name="YearHeader1">
          <xsl:with-param name="BP">Y</xsl:with-param>
        </xsl:call-template>
      </irw:SectionHeader>
      <irw:SectionItems>
        <xsl:call-template name="EOPItem1">
          <xsl:with-param name="Name">TangibleEquityToAssets</xsl:with-param>
          <xsl:with-param name="Description">Tangible Equity/ Tangible Assets</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="EOPItem1">
          <xsl:with-param name="Name">EquityToAssets</xsl:with-param>
          <xsl:with-param name="Description">Equity/ Assets</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0)</xsl:with-param>
        </xsl:call-template>
      </irw:SectionItems>
      <irw:SectionFooter>
      </irw:SectionFooter>
    </irw:Section>
    </table>
    </td>
  </tr>
  <tr>
    <td height="10"></td>
  </tr>
</xsl:template>


<xsl:template name="Trend_assetratio1">
<tr>
  <td>
    <table BORDER="0"  CELLSPACING="0" CELLPADDING="3" WIDTH="100%">
    <irw:Section name="AssetQualityRatios">
      <irw:SectionHeader>
        <tr>
          <td NOWRAP="" class="datashade surr" COLSPAN="7">Asset Quality Ratios (%)</td>
        </tr>
        <xsl:call-template name="YearHeader1">
          <xsl:with-param name="BP">Y</xsl:with-param>
        </xsl:call-template>
      </irw:SectionHeader>
      <irw:SectionItems>
        <xsl:call-template name="AsReportedItem1">
          <xsl:with-param name="Name">NPAsToAssets</xsl:with-param>
          <xsl:with-param name="Description">Nonperforming Assets/ Assets</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="EOP">1</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="EOPItem1">
          <xsl:with-param name="Name">ReservesToLoans</xsl:with-param>
          <xsl:with-param name="Description">Loan Loss Reserves/ Gross Loans</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="EOPItem1">
          <xsl:with-param name="Name">ReservesToNPAs</xsl:with-param>
          <xsl:with-param name="Description">Loan Loss Reserves/ NPAs</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="FlowItem1">
          <xsl:with-param name="Name">NetChargeoffsToLoans</xsl:with-param>
          <xsl:with-param name="Description">Net Charge-offs/ Avg Loans</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0)</xsl:with-param>
        </xsl:call-template>
      </irw:SectionItems>
    </irw:Section>
    </table>
      </td>
    </tr>
    <tr>
      <td height="10"></td>
  </tr>
</xsl:template>

<xsl:template name="Trend_regratios1">
<tr>
  <td>
    <table BORDER="0"  CELLSPACING="0" CELLPADDING="3" WIDTH="100%">
    <irw:Section name="RegulatoryCapitalData">
      <irw:SectionHeader>
        <tr>
          <td NOWRAP="" class="datashade surr" COLSPAN="7">Regulatory Capital Ratios (%)</td>
        </tr>
        <xsl:call-template name="YearHeader1">
          <xsl:with-param name="BP">Y</xsl:with-param>
        </xsl:call-template>
      </irw:SectionHeader>

      <irw:SectionItems>
        <xsl:call-template name="EOPItem1">
          <xsl:with-param name="Name">T1RatioAsRegBOCalc</xsl:with-param>
          <xsl:with-param name="Description">Tier 1 Capital Ratio</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0)</xsl:with-param>
        </xsl:call-template>
      </irw:SectionItems>
    </irw:Section>
    </table>
          </td>
        </tr>
        <tr>
          <td height="10"></td>
  </tr>

</xsl:template>


  <xsl:template name="EOPItem1">
    <xsl:param name="Name"/>
    <xsl:param name="Description"/>
    <xsl:param name="FormatString"/>
    <xsl:param name="NegativeFormatString"/>
    <xsl:param name="ChangeFormatString"/>
    <xsl:param name="NegativeChangeFormatString"/>
    <xsl:param name="Asterisk"/>
    <xsl:param name="ChangeText"/>
      <tr class="data" ALIGN="right">
          <td class="data" NOWRAP="" ALIGN="left">
              <a href="#" class="fielddef" onClick="DefWindow({Company/KeyInstn},'{$Name}', 1); return false"  title="{$Description}"><xsl:value-of select="$Description"/></a>
              <xsl:if test="$Asterisk=1">
      <xsl:if test="irw:Element/snlo:FinlEOPBank/snlo:FinlFlowBank/snli:FiscalQuarter[@snl:key!='Y']">
        <sup>*</sup>
      </xsl:if>
      </xsl:if>
          </td>
      <xsl:for-each select="irw:Element/snlo:FinlEOPBank">
        <xsl:variable name="Value" select="*[name(.)=concat('snli:',$Name)]"/>
        <td class="data" NOWRAP="">
        <xsl:choose>
        <xsl:when test="$Value[@snl:meaningful]">
        NM
        </xsl:when>
        <xsl:otherwise>
          <xsl:choose>
            <xsl:when test="$Value">
            <xsl:choose>
              <xsl:when test="$Value>=0">
                <xsl:value-of select="format-number($Value,$FormatString,'data')"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="format-number(-$Value,$NegativeFormatString,'data')"/>
              </xsl:otherwise>
            </xsl:choose>

            </xsl:when>
            <xsl:otherwise>NA</xsl:otherwise>
          </xsl:choose>
        </xsl:otherwise>
        </xsl:choose>
        </td>
        <xsl:for-each select="snlo:FinlFlowBank">
          <xsl:variable name="Change" select="snl:item[@snl:description=concat($Name,'_Change')]"/>
          <xsl:if test="$Change">
            <xsl:if test="format-number($Change, $ChangeFormatString)='NaN'">
              <td NOWRAP="" CLASS="datashade data">
                <xsl:value-of select="$Change"/>
              </td>
            </xsl:if>
            <xsl:if test="format-number($Change, $ChangeFormatString)!='NaN'">
              <td NOWRAP="" CLASS="datashade data">
                <xsl:choose>
                  <xsl:when test="$Change>=0">
                    <xsl:value-of select="format-number($Change,$ChangeFormatString,'data')"/><xsl:value-of select="$ChangeText"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="format-number(-$Change,$NegativeChangeFormatString,'data')"/><xsl:value-of select="$ChangeText"/>
                  </xsl:otherwise>
                </xsl:choose>
              </td>
            </xsl:if>
          </xsl:if>
        </xsl:for-each>
      </xsl:for-each>
    </tr>
</xsl:template>



<xsl:template name="FlowItem1">
  <xsl:param name="Name"/>
  <xsl:param name="Description"/>
  <xsl:param name="FormatString"/>
  <xsl:param name="NegativeFormatString"/>
  <xsl:param name="ChangeFormatString"/>
  <xsl:param name="NegativeChangeFormatString"/>
  <xsl:param name="Asterisk"/>
  <xsl:param name="ChangeText"/>
    <tr class="data" ALIGN="right">
        <td class="data" NOWRAP="" ALIGN="left">
            <a href="#" class="fielddef" onClick="DefWindow({Company/KeyInstn},'{$Name}', 0); return false" title="{$Description}"><xsl:value-of select="$Description"/></a>
            <xsl:if test="$Asterisk=1"><xsl:if test="irw:Element/snlo:FinlEOPBank/snlo:FinlFlowBank/snli:FiscalQuarter[@snl:key!='Y']"><sup>*</sup></xsl:if></xsl:if>
        </td>
    <xsl:for-each select="irw:Element/snlo:FinlEOPBank">
      <xsl:variable name="Value" select="descendant::*[name(.)=concat('snli:',$Name)]"/>
      <td class="data" NOWRAP="">
      <xsl:choose>
      <xsl:when test="$Value[@snl:meaningful]">
      NM
      </xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="$Value">
          <xsl:choose>
            <xsl:when test="$Value>=0">
              <xsl:value-of select="format-number($Value,$FormatString,'data')"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="format-number(-$Value,$NegativeFormatString,'data')"/>
            </xsl:otherwise>
          </xsl:choose>

          </xsl:when>
          <xsl:otherwise>NA</xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <xsl:for-each select="snlo:FinlFlowBank">
        <xsl:variable name="Change" select="snl:item[@snl:description=concat($Name,'_Change')]"/>
        <xsl:if test="$Change">
          <xsl:if test="format-number($Change, $ChangeFormatString)='NaN'">
            <td NOWRAP="" CLASS="datashade data">
              <xsl:value-of select="$Change"/>
            </td>
          </xsl:if>
          <xsl:if test="format-number($Change, $ChangeFormatString)!='NaN'">
            <td NOWRAP="" CLASS="datashade data">
            <xsl:choose>
              <xsl:when test="$Change>=0">
                <xsl:value-of select="format-number($Change,$ChangeFormatString,'data')"/><xsl:value-of select="$ChangeText"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="format-number(-$Change,$NegativeChangeFormatString,'data')"/><xsl:value-of select="$ChangeText"/>
              </xsl:otherwise>
            </xsl:choose>
            </td>
          </xsl:if>
        </xsl:if>
      </xsl:for-each>
    </xsl:for-each>
  </tr>
</xsl:template>

<xsl:template name="AsReportedItem1">
  <xsl:param name="Name"/>
  <xsl:param name="Description"/>
  <xsl:param name="FormatString"/>
  <xsl:param name="NegativeFormatString"/>
  <xsl:param name="ChangeFormatString"/>
  <xsl:param name="NegativeChangeFormatString"/>
  <xsl:param name="Asterisk"/>
  <xsl:param name="EOP"/>
  <xsl:param name="ChangeText"/>
    <tr class="data" ALIGN="right">
    <xsl:variable name="UseAsReported" select="descendant::snl:item[@snl:description=concat($Name,'Reported_UseAsReported')]"/>
        <td class="data" NOWRAP="" ALIGN="left">
      <xsl:choose>
        <xsl:when test="$EOP=1">
                <a href="#" class="fielddef" onClick="DefWindow({Company/KeyInstn},'{$Name}', 1); return false" title="{$Description}"><xsl:value-of select="$Description"/></a>
        </xsl:when>
        <xsl:otherwise>
                <a href="#" class="fielddef" onClick="DefWindow({Company/KeyInstn},'{$Name}', 0); return false" title="{$Description}"><xsl:value-of select="$Description"/></a>
        </xsl:otherwise>
      </xsl:choose>
            <xsl:if test="$Asterisk=1"><xsl:if test="irw:Element/snlo:FinlEOPBank/snlo:FinlFlowBank/snli:FiscalQuarter[@snl:key!='Y']"><sup>*</sup></xsl:if></xsl:if>
        </td>
    <xsl:for-each select="irw:Element/snlo:FinlEOPBank">
      <xsl:choose>
        <xsl:when test="$UseAsReported">
          <xsl:variable name="Value" select="descendant::*[name(.)=concat('snli:',$Name, 'Reported')]"/>
      <td class="data" NOWRAP="">
        <xsl:choose>
        <xsl:when test="$Value[@snl:meaningful]">NM</xsl:when>
        <xsl:otherwise>
        <xsl:choose>
        <xsl:when test="$Value">
        <xsl:choose>
          <xsl:when test="$Value>=0">
            <xsl:value-of select="format-number($Value,$FormatString,'data')"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="format-number(-$Value,$NegativeFormatString,'data')"/>
          </xsl:otherwise>
        </xsl:choose>
        </xsl:when>
        <xsl:otherwise>NA</xsl:otherwise>
        </xsl:choose>
        </xsl:otherwise>
        </xsl:choose>
      </td>
      </xsl:when>
      <xsl:otherwise><xsl:variable name="Value" select="descendant::*[name(.)=concat('snli:',$Name)]"/>     <td class="data" NOWRAP="">
      <xsl:choose>
      <xsl:when test="$Value[@snl:meaningful]">NM</xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="$Value">
          <xsl:choose>
            <xsl:when test="$Value>=0">
              <xsl:value-of select="format-number($Value,$FormatString,'data')"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="format-number(-$Value,$NegativeFormatString,'data')"/>
            </xsl:otherwise>
          </xsl:choose>

          </xsl:when>
          <xsl:otherwise>NA</xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
      </xsl:choose>
      </td>
</xsl:otherwise>
      </xsl:choose>
      <xsl:for-each select="snlo:FinlFlowBank">
      <xsl:choose>
        <xsl:when test="$UseAsReported"><xsl:variable name="Change" select="snl:item[@snl:description=concat($Name,'Reported_Change')]"/>       <xsl:if test="$Change">
          <xsl:if test="format-number($Change, $ChangeFormatString)='NaN'">
            <td NOWRAP="" CLASS="datashade data">
              <xsl:value-of select="$Change"/>
            </td>
          </xsl:if>
          <xsl:if test="format-number($Change, $ChangeFormatString)!='NaN'">
            <td NOWRAP="" CLASS="datashade data">
            <xsl:choose>
              <xsl:when test="$Change>=0">
                <xsl:value-of select="format-number($Change,$ChangeFormatString,'data')"/><xsl:value-of select="$ChangeText"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="format-number(-$Change,$NegativeChangeFormatString,'data')"/><xsl:value-of select="$ChangeText"/>
              </xsl:otherwise>
            </xsl:choose>
            </td>
          </xsl:if>
        </xsl:if>
</xsl:when>
        <xsl:otherwise><xsl:variable name="Change" select="snl:item[@snl:description=concat($Name,'_Change')]"/>
                <xsl:if test="$Change">
          <xsl:if test="format-number($Change, $ChangeFormatString)='NaN'">
            <td NOWRAP="" CLASS="datashade data">
              <xsl:value-of select="$Change"/>
            </td>
          </xsl:if>
          <xsl:if test="format-number($Change, $ChangeFormatString)!='NaN'">
            <td NOWRAP="" CLASS="datashade table1_item">
            <xsl:choose>
              <xsl:when test="$Change>=0">
                <xsl:value-of select="format-number($Change,$ChangeFormatString,'data')"/><xsl:value-of select="$ChangeText"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="format-number(-$Change,$NegativeChangeFormatString,'data')"/><xsl:value-of select="$ChangeText"/>
              </xsl:otherwise>
            </xsl:choose>
            </td>
          </xsl:if>
        </xsl:if>
        </xsl:otherwise>
      </xsl:choose>
      </xsl:for-each>
    </xsl:for-each>
  </tr>
</xsl:template>




<!-- Template 1 ENDS Here -->





<!-- Template 3 START Here -->
<xsl:template name="Trend_data3">
<tr>
<td CLASS="colorlight"><span class="default">
<br />For the definition of a financial field, click on the field name.</span><br /><br />

<irw:SectionList>
<table BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
<xsl:call-template name="Trend_balancesheet3" />
<xsl:call-template name="Trend_income3" />
<xsl:call-template name="Trend_pershare3" />
<xsl:call-template name="Trend_performance3" />
<xsl:call-template name="Trend_balance3" />
<xsl:call-template name="Trend_assetratio3" />
<xsl:call-template name="Trend_regratios3" />
</table>
</irw:SectionList>

</td>
</tr>
</xsl:template>

<xsl:template name="YearHeader3">
  <xsl:param name="BP"/>

  <TR CLASS="table1_item data" ALIGN="center">
    <TD CLASS="table1_item data" NOWRAP="" ALIGN="center"><span class="defaultbold">&#160;</span></TD>

    <xsl:for-each select="irw:Element/snlo:FinlEOPBank">

      <xsl:if test="snlo:FinlFlowBank/snli:FiscalQuarter[@snl:key='Y']">
        <TD CLASS="table1_item data" NOWRAP="" ALIGN="center"><span class="defaultbold"><xsl:value-of select="snlo:FinlFlowBank/snli:FiscalYear"/>Y</span></TD>

        <xsl:for-each select="snlo:FinlFlowBank">
          <xsl:if test="snl:item[@snl:description='FiscalYear_Change']">
            <TD CLASS="table1_item data" NOWRAP="" ALIGN="center" width="45"><span class="defaultbold">
              <xsl:value-of select="snli:FiscalYear+1"/>/<BR /><xsl:value-of select="snli:FiscalYear"/><BR />
              <xsl:if test="$BP='Y'">Ch (bp)</xsl:if>
              <xsl:if test="$BP!='Y'">Ch (%)</xsl:if>
              </span>
            </TD>
          </xsl:if>
        </xsl:for-each>
      </xsl:if>

      <xsl:if test="snlo:FinlFlowBank/snli:FiscalQuarter[@snl:key!='Y']">
        <TD CLASS="table1_item data" NOWRAP="" ALIGN="center"><span class="defaultbold"><xsl:value-of select="snlo:FinlFlowBank/snli:FiscalYear"/> Q<xsl:value-of select="snlo:FinlFlowBank/snli:FiscalQuarter/@snl:key"/></span></TD>

        <xsl:for-each select="snlo:FinlFlowBank">
          <xsl:if test="snl:item[@snl:description='FiscalQuarter_Change']=4">
            <TD CLASS="table1_item data" NOWRAP="" ALIGN="center">
              <xsl:if test="$BP='Y'"><SPAN class="defaultbold">Y-Y<BR />Ch (bp)</SPAN></xsl:if>
              <xsl:if test="$BP!='Y'"><SPAN class="defaultbold">Y-Y<BR />Ch (%)</SPAN></xsl:if>
            </TD>
          </xsl:if>

          <xsl:if test="snl:item[@snl:description='FiscalQuarter_Change']=1">
            <TD CLASS="table1_item data" NOWRAP="" ALIGN="center">
              <xsl:if test="$BP='Y'"><SPAN class="defaultbold">Q-Q<BR />Ch (bp)</SPAN></xsl:if>
              <xsl:if test="$BP!='Y'"><SPAN class="defaultbold">Q-Q<BR />Ch (%)<sup>&#8224;</sup></SPAN></xsl:if>
            </TD>
          </xsl:if>
        </xsl:for-each>

      </xsl:if>

    </xsl:for-each>
  </TR>
</xsl:template>


<xsl:template name="Trend_balancesheet3">
  <tr>
    <td>
      <table BORDER="0" class="table1" CELLSPACING="0" CELLPADDING="3" WIDTH="100%">
    <irw:Section name="BalanceSheet">
      <irw:SectionHeader>
        <tr>
          <td NOWRAP="" ALIGN="left" class="table1_item header" COLSPAN="7">Balance Sheet ($000)</td>
        </tr>
        <xsl:call-template name="YearHeader3">
          <xsl:with-param name="BP">N</xsl:with-param>
        </xsl:call-template>
      </irw:SectionHeader>
      <irw:SectionItems>
        <xsl:call-template name="EOPItem3">
          <xsl:with-param name="Name">InvLoansBefReserve</xsl:with-param>
          <xsl:with-param name="Description">Loans Held for Investment, before Reserves</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="EOPItem3">
          <xsl:with-param name="Name">LoansHeldForSaleBefReserve</xsl:with-param>
          <xsl:with-param name="Description">Loans Held for Sale, before Reserves</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="EOPItem3">
          <xsl:with-param name="Name">ReserveForLoanLosses</xsl:with-param>
          <xsl:with-param name="Description">Loan Loss Reserve</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="EOPItem3">
          <xsl:with-param name="Name">NetLoans</xsl:with-param>
          <xsl:with-param name="Description">Net Loans Receivable</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="EOPItem3">
          <xsl:with-param name="Name">Intangibles</xsl:with-param>
          <xsl:with-param name="Description">Intangible Assets</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
          <xsl:call-template name="EOPItem3">
          <xsl:with-param name="Name">Assets</xsl:with-param>
          <xsl:with-param name="Description">Total Assets</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="EOPItem3">
          <xsl:with-param name="Name">Deposits</xsl:with-param>
          <xsl:with-param name="Description">Deposits</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="EOPItem3">
          <xsl:with-param name="Name">DebtAndSubDebt</xsl:with-param>
          <xsl:with-param name="Description">Debt, Senior and Subordinated</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="EOPItem3">
          <xsl:with-param name="Name">EquityCommon</xsl:with-param>
          <xsl:with-param name="Description">Common Equity</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="EOPItem3">
          <xsl:with-param name="Name">Equity</xsl:with-param>
          <xsl:with-param name="Description">Total Shareholders' Equity</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="EOPItem3">
          <xsl:with-param name="Name">SharesOutstanding</xsl:with-param>
          <xsl:with-param name="Description">Shares Outstanding (actual)</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
      </irw:SectionItems>
    </irw:Section>
    </table>
    </td>
  </tr>
  <tr>
    <td height="10"></td>
  </tr>

</xsl:template>

<xsl:template name="Trend_income3">
<tr>
    <td>
      <table BORDER="0" class="table1" CELLSPACING="0" CELLPADDING="3" WIDTH="100%">
    <irw:Section name="IncomeStatement">
        <irw:SectionHeader>
        <tr>
          <td NOWRAP="" ALIGN="left" class="table1_item header" COLSPAN="7">Income Statement ($000)</td>
        </tr>
        <xsl:call-template name="YearHeader3">
          <xsl:with-param name="BP">N</xsl:with-param>
        </xsl:call-template>
      </irw:SectionHeader>
      <irw:SectionItems>
        <xsl:call-template name="FlowItem3">
          <xsl:with-param name="Name">NetInterestIncome</xsl:with-param>
          <xsl:with-param name="Description">Net Interest Income</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="FlowItem3">
          <xsl:with-param name="Name">ProvisionForLoanLosses</xsl:with-param>
          <xsl:with-param name="Description">Provision for Loan Losses</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="FlowItem3">
          <xsl:with-param name="Name">NonInterestIncome</xsl:with-param>
          <xsl:with-param name="Description">Noninterest Income</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="FlowItem3">
          <xsl:with-param name="Name">NonInterestExpense</xsl:with-param>
          <xsl:with-param name="Description">Noninterest Expense</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="FlowItem3">
          <xsl:with-param name="Name">NetIncomeBeforeTaxes</xsl:with-param>
          <xsl:with-param name="Description">Net Income Before Taxes</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="FlowItem3">
          <xsl:with-param name="Name">Taxes</xsl:with-param>
          <xsl:with-param name="Description">Provision for Taxes</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="FlowItem3">
          <xsl:with-param name="Name">NetIncome</xsl:with-param>
          <xsl:with-param name="Description">Net Income</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
      </irw:SectionItems>
    </irw:Section>
  </table>
      </td>
    </tr>
    <tr>
      <td height="10"></td>
    </tr>

</xsl:template>


<xsl:template name="Trend_pershare3">
<tr>
    <td>
      <table BORDER="0" class="table1" CELLSPACING="0" CELLPADDING="3" WIDTH="100%">
    <irw:Section name="PerShareItems">
      <irw:SectionHeader>
        <tr>
          <td NOWRAP="" ALIGN="left" class="table1_item header" COLSPAN="7">Per Share Items ($)</td>
        </tr>
        <xsl:call-template name="YearHeader3">
          <xsl:with-param name="BP">N</xsl:with-param>
        </xsl:call-template>
      </irw:SectionHeader>
      <irw:SectionItems>
        <xsl:call-template name="EOPItem3">
          <xsl:with-param name="Name">EquityPerShareForCalcs</xsl:with-param>
          <xsl:with-param name="Description">Book Value per Share</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="EOPItem3">
          <xsl:with-param name="Name">TangibleEquityPerShare</xsl:with-param>
          <xsl:with-param name="Description">Tangible Book Value</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="FlowItem3">
          <xsl:with-param name="Name">EPSDiluted</xsl:with-param>
          <xsl:with-param name="Description">Diluted EPS Before Extra</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="FlowItem3">
          <xsl:with-param name="Name">EPSDilutedAfterExtra</xsl:with-param>
          <xsl:with-param name="Description">Diluted EPS After Extra</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="FlowItem3">
          <xsl:with-param name="Name">Dividend</xsl:with-param>
          <xsl:with-param name="Description">Dividends Declared</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.0000</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.0000)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0.00)</xsl:with-param>
        </xsl:call-template>
      </irw:SectionItems>
    </irw:Section>
    </table>
          </td>
        </tr>
        <tr>
          <td height="10"></td>
    </tr>
</xsl:template>

<xsl:template name="Trend_performance3">
<tr>
    <td>
      <table BORDER="0" class="table1" CELLSPACING="0" CELLPADDING="3" WIDTH="100%">
    <irw:Section name="PerformanceRatios">
      <irw:SectionHeader>
        <tr>
          <td NOWRAP="" ALIGN="left" class="table1_item header" COLSPAN="7">Performance Ratios (%)</td>
        </tr>
        <xsl:call-template name="YearHeader3">
          <xsl:with-param name="BP">Y</xsl:with-param>
        </xsl:call-template>
      </irw:SectionHeader>
      <irw:SectionItems>
        <xsl:call-template name="AsReportedItem3">
          <xsl:with-param name="Name">ROAA</xsl:with-param>
          <xsl:with-param name="Description">ROAA</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="AsReportedItem3">
          <xsl:with-param name="Name">ROAE</xsl:with-param>
          <xsl:with-param name="Description">ROAE</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="AsReportedItem3">
          <xsl:with-param name="Name">NetInterestMargin</xsl:with-param>
          <xsl:with-param name="Description">Net Interest Margin</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="EOPItem3">
          <xsl:with-param name="Name">InvLoansToDeposits</xsl:with-param>
          <xsl:with-param name="Description">Loans / Deposits</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="AsReportedItem3">
          <xsl:with-param name="Name">EfficiencyRatio</xsl:with-param>
          <xsl:with-param name="Description">Efficiency Ratio</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
        </xsl:call-template>
      </irw:SectionItems>
    </irw:Section>
    </table>
    </td>
  </tr>
  <tr>
    <td height="10"></td>
</tr>
</xsl:template>

<xsl:template name="Trend_balance3">
<tr>
  <td>
    <table BORDER="0" class="table1" CELLSPACING="0" CELLPADDING="3" WIDTH="100%">
    <irw:Section name="BalanceSheetRatios">
      <irw:SectionHeader>
        <tr>
          <td NOWRAP="" ALIGN="left" class="table1_item header" COLSPAN="7">Balance Sheet Ratios (%)</td>
        </tr>
        <xsl:call-template name="YearHeader3">
          <xsl:with-param name="BP">Y</xsl:with-param>
        </xsl:call-template>
      </irw:SectionHeader>
      <irw:SectionItems>
        <xsl:call-template name="EOPItem3">
          <xsl:with-param name="Name">TangibleEquityToAssets</xsl:with-param>
          <xsl:with-param name="Description">Tangible Equity/ Tangible Assets</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="EOPItem3">
          <xsl:with-param name="Name">EquityToAssets</xsl:with-param>
          <xsl:with-param name="Description">Equity/ Assets</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0)</xsl:with-param>
        </xsl:call-template>
      </irw:SectionItems>
      <irw:SectionFooter>
      </irw:SectionFooter>
    </irw:Section>
    </table>
    </td>
  </tr>
  <tr>
    <td height="10"></td>
  </tr>
</xsl:template>


<xsl:template name="Trend_assetratio3">
<tr>
  <td>
    <table BORDER="0" class="table1" CELLSPACING="0" CELLPADDING="3" WIDTH="100%">
    <irw:Section name="AssetQualityRatios">
      <irw:SectionHeader>
        <tr>
          <td NOWRAP="" ALIGN="left" class="table1_item header" COLSPAN="7">Asset Quality Ratios (%)</td>
        </tr>
        <xsl:call-template name="YearHeader3">
          <xsl:with-param name="BP">Y</xsl:with-param>
        </xsl:call-template>
      </irw:SectionHeader>
      <irw:SectionItems>
        <xsl:call-template name="AsReportedItem3">
          <xsl:with-param name="Name">NPAsToAssets</xsl:with-param>
          <xsl:with-param name="Description">Nonperforming Assets/ Assets</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0)</xsl:with-param>
          <xsl:with-param name="EOP">1</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="EOPItem3">
          <xsl:with-param name="Name">ReservesToLoans</xsl:with-param>
          <xsl:with-param name="Description">Loan Loss Reserves/ Gross Loans</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="EOPItem3">
          <xsl:with-param name="Name">ReservesToNPAs</xsl:with-param>
          <xsl:with-param name="Description">Loan Loss Reserves/ NPAs</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0)</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="FlowItem3">
          <xsl:with-param name="Name">NetChargeoffsToLoans</xsl:with-param>
          <xsl:with-param name="Description">Net Charge-offs/ Avg Loans</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0)</xsl:with-param>
        </xsl:call-template>
      </irw:SectionItems>
    </irw:Section>
    </table>
      </td>
    </tr>
    <tr>
      <td height="10"></td>
  </tr>
</xsl:template>

<xsl:template name="Trend_regratios3">
<tr>
  <td>
    <table BORDER="0" class="table1" CELLSPACING="0" CELLPADDING="3" WIDTH="100%">
    <irw:Section name="RegulatoryCapitalData">
      <irw:SectionHeader>
        <tr>
          <td NOWRAP="" ALIGN="left" class="table1_item header" COLSPAN="7">Regulatory Capital Ratios (%)</td>
        </tr>
        <xsl:call-template name="YearHeader3">
          <xsl:with-param name="BP">Y</xsl:with-param>
        </xsl:call-template>
      </irw:SectionHeader>

      <irw:SectionItems>
        <xsl:call-template name="EOPItem3">
          <xsl:with-param name="Name">T1RatioAsRegBOCalc</xsl:with-param>
          <xsl:with-param name="Description">Tier 1 Capital Ratio</xsl:with-param>
          <xsl:with-param name="FormatString">#,##0.00</xsl:with-param>
          <xsl:with-param name="ChangeFormatString">#,##0</xsl:with-param>
          <xsl:with-param name="NegativeFormatString">(#,##0.00)</xsl:with-param>
          <xsl:with-param name="NegativeChangeFormatString">(#,##0)</xsl:with-param>
        </xsl:call-template>
      </irw:SectionItems>
    </irw:Section>
</table>
    </td>
  </tr>
  <tr>
    <td height="10"></td>
</tr>

</xsl:template>


<xsl:template name="EOPItem3">
  <xsl:param name="Name"/>
  <xsl:param name="Description"/>
  <xsl:param name="FormatString"/>
  <xsl:param name="NegativeFormatString"/>
  <xsl:param name="ChangeFormatString"/>
  <xsl:param name="NegativeChangeFormatString"/>
  <xsl:param name="Asterisk"/>
  <xsl:param name="ChangeText"/>
  <tr class="table1_item" ALIGN="right">
    <td class="table1_item" NOWRAP="" ALIGN="left" width="45%">
      <a href="#" class="fielddef" onClick="DefWindow({Company/KeyInstn},'{$Name}', 1); return false" title="{$Description}"><xsl:value-of select="$Description"/></a>
      <xsl:if test="$Asterisk=1">
    <xsl:if test="irw:Element/snlo:FinlEOPBank/snlo:FinlFlowBank/snli:FiscalQuarter[@snl:key!='Y']">
      <sup>*</sup>
    </xsl:if>
    </xsl:if>
    </td>
    <xsl:for-each select="irw:Element/snlo:FinlEOPBank">
      <xsl:variable name="Value" select="*[name(.)=concat('snli:',$Name)]"/>
      <td class="table1_item" NOWRAP="">
      <xsl:choose>
      <xsl:when test="$Value[@snl:meaningful]">
      NM
      </xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="$Value">
          <xsl:choose>
            <xsl:when test="$Value>=0">
              <xsl:value-of select="format-number($Value,$FormatString,'data')"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="format-number(-$Value,$NegativeFormatString,'data')"/>
            </xsl:otherwise>
          </xsl:choose>

          </xsl:when>
          <xsl:otherwise>NA</xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <xsl:for-each select="snlo:FinlFlowBank">
        <xsl:variable name="Change" select="snl:item[@snl:description=concat($Name,'_Change')]"/>
        <xsl:if test="$Change">
          <xsl:if test="format-number($Change, $ChangeFormatString)='NaN'">
            <td NOWRAP="" CLASS="datashade table1_item">
              <xsl:value-of select="$Change"/>
            </td>
          </xsl:if>
          <xsl:if test="format-number($Change, $ChangeFormatString)!='NaN'">
            <td NOWRAP="" CLASS="datashade table1_item">
              <xsl:choose>
                <xsl:when test="$Change>=0">
                  <xsl:value-of select="format-number($Change,$ChangeFormatString,'data')"/><xsl:value-of select="$ChangeText"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="format-number(-$Change,$NegativeChangeFormatString,'data')"/><xsl:value-of select="$ChangeText"/>
                </xsl:otherwise>
              </xsl:choose>
            </td>
          </xsl:if>
        </xsl:if>
      </xsl:for-each>
    </xsl:for-each>
  </tr>
</xsl:template>



<xsl:template name="FlowItem3">
  <xsl:param name="Name"/>
  <xsl:param name="Description"/>
  <xsl:param name="FormatString"/>
  <xsl:param name="NegativeFormatString"/>
  <xsl:param name="ChangeFormatString"/>
  <xsl:param name="NegativeChangeFormatString"/>
  <xsl:param name="Asterisk"/>
  <xsl:param name="ChangeText"/>
    <tr class="table1_item" ALIGN="right">
        <td class="table1_item" NOWRAP="" ALIGN="left" width="45%">
            <a href="#" class="fielddef" onClick="DefWindow({Company/KeyInstn},'{$Name}', 0); return false" title="{$Description}"><xsl:value-of select="$Description"/></a>
            <xsl:if test="$Asterisk=1"><xsl:if test="irw:Element/snlo:FinlEOPBank/snlo:FinlFlowBank/snli:FiscalQuarter[@snl:key!='Y']"><sup>*</sup></xsl:if></xsl:if>
        </td>
    <xsl:for-each select="irw:Element/snlo:FinlEOPBank">
      <xsl:variable name="Value" select="descendant::*[name(.)=concat('snli:',$Name)]"/>
      <td class="table1_item" NOWRAP="">
      <xsl:choose>
      <xsl:when test="$Value[@snl:meaningful]">
      NM
      </xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="$Value">
          <xsl:choose>
            <xsl:when test="$Value>=0">
              <xsl:value-of select="format-number($Value,$FormatString,'data')"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="format-number(-$Value,$NegativeFormatString,'data')"/>
            </xsl:otherwise>
          </xsl:choose>

          </xsl:when>
          <xsl:otherwise>NA</xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <xsl:for-each select="snlo:FinlFlowBank">
        <xsl:variable name="Change" select="snl:item[@snl:description=concat($Name,'_Change')]"/>
        <xsl:if test="$Change">
          <xsl:if test="format-number($Change, $ChangeFormatString)='NaN'">
            <td NOWRAP="" CLASS="datashade table1_item">
              <xsl:value-of select="$Change"/>
            </td>
          </xsl:if>
          <xsl:if test="format-number($Change, $ChangeFormatString)!='NaN'">
            <td NOWRAP="" CLASS="datashade table1_item">
            <xsl:choose>
              <xsl:when test="$Change>=0">
                <xsl:value-of select="format-number($Change,$ChangeFormatString,'data')"/><xsl:value-of select="$ChangeText"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="format-number(-$Change,$NegativeChangeFormatString,'data')"/><xsl:value-of select="$ChangeText"/>
              </xsl:otherwise>
            </xsl:choose>
            </td>
          </xsl:if>
        </xsl:if>
      </xsl:for-each>
    </xsl:for-each>
  </tr>
</xsl:template>

<xsl:template name="AsReportedItem3">
  <xsl:param name="Name"/>
  <xsl:param name="Description"/>
  <xsl:param name="FormatString"/>
  <xsl:param name="NegativeFormatString"/>
  <xsl:param name="ChangeFormatString"/>
  <xsl:param name="NegativeChangeFormatString"/>
  <xsl:param name="Asterisk"/>
  <xsl:param name="EOP"/>
  <xsl:param name="ChangeText"/>
    <tr class="table1_item" ALIGN="right">
    <xsl:variable name="UseAsReported" select="descendant::snl:item[@snl:description=concat($Name,'Reported_UseAsReported')]"/>
        <td class="table1_item" NOWRAP="" ALIGN="left" width="45%">
      <xsl:choose>
        <xsl:when test="$EOP=1">
                <a href="#" class="fielddef" onClick="DefWindow({Company/KeyInstn},'{$Name}', 1); return false" title="{$Description}"><xsl:value-of select="$Description"/></a>
        </xsl:when>
        <xsl:otherwise>
                <a href="#" class="fielddef" onClick="DefWindow({Company/KeyInstn},'{$Name}', 0); return false" title="{$Description}"><xsl:value-of select="$Description"/></a>
        </xsl:otherwise>
      </xsl:choose>
            <xsl:if test="$Asterisk=1"><xsl:if test="irw:Element/snlo:FinlEOPBank/snlo:FinlFlowBank/snli:FiscalQuarter[@snl:key!='Y']"><sup>*</sup></xsl:if></xsl:if>
        </td>
    <xsl:for-each select="irw:Element/snlo:FinlEOPBank">
      <xsl:choose>
        <xsl:when test="$UseAsReported">
          <xsl:variable name="Value" select="descendant::*[name(.)=concat('snli:',$Name, 'Reported')]"/>
      <td class="table1_item" NOWRAP="">
        <xsl:choose>
        <xsl:when test="$Value[@snl:meaningful]">NM</xsl:when>
        <xsl:otherwise>
        <xsl:choose>
        <xsl:when test="$Value">
        <xsl:choose>
          <xsl:when test="$Value>=0">
            <xsl:value-of select="format-number($Value,$FormatString,'data')"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="format-number(-$Value,$NegativeFormatString,'data')"/>
          </xsl:otherwise>
        </xsl:choose>
        </xsl:when>
        <xsl:otherwise>NA</xsl:otherwise>
        </xsl:choose>
        </xsl:otherwise>
        </xsl:choose>
      </td>
      </xsl:when>
      <xsl:otherwise><xsl:variable name="Value" select="descendant::*[name(.)=concat('snli:',$Name)]"/>
      <td class="table1_item" NOWRAP="">
      <xsl:choose>
      <xsl:when test="$Value[@snl:meaningful]">NM</xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="$Value">
          <xsl:choose>
            <xsl:when test="$Value>=0">
              <xsl:value-of select="format-number($Value,$FormatString,'data')"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="format-number(-$Value,$NegativeFormatString,'data')"/>
            </xsl:otherwise>
          </xsl:choose>

          </xsl:when>
          <xsl:otherwise>NA</xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
      </xsl:choose>
      </td>
      </xsl:otherwise>
      </xsl:choose>
      <xsl:for-each select="snlo:FinlFlowBank">
      <xsl:choose>
        <xsl:when test="$UseAsReported"><xsl:variable name="Change" select="snl:item[@snl:description=concat($Name,'Reported_Change')]"/>       <xsl:if test="$Change">
          <xsl:if test="format-number($Change, $ChangeFormatString)='NaN'">
            <td NOWRAP="" CLASS="datashade table1_item">
              <xsl:value-of select="$Change"/>
            </td>
          </xsl:if>
          <xsl:if test="format-number($Change, $ChangeFormatString)!='NaN'">
            <td NOWRAP="" CLASS="datashade table1_item">
            <xsl:choose>
              <xsl:when test="$Change>=0">
                <xsl:value-of select="format-number($Change,$ChangeFormatString,'data')"/><xsl:value-of select="$ChangeText"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="format-number(-$Change,$NegativeChangeFormatString,'data')"/><xsl:value-of select="$ChangeText"/>
              </xsl:otherwise>
            </xsl:choose>
            </td>
          </xsl:if>
        </xsl:if>
        </xsl:when>
        <xsl:otherwise><xsl:variable name="Change" select="snl:item[@snl:description=concat($Name,'_Change')]"/>
                <xsl:if test="$Change">
          <xsl:if test="format-number($Change, $ChangeFormatString)='NaN'">
            <td NOWRAP="" CLASS="datashade table1_item">
              <xsl:value-of select="$Change"/>
            </td>
          </xsl:if>
          <xsl:if test="format-number($Change, $ChangeFormatString)!='NaN'">
            <td NOWRAP="" CLASS="datashade table1_item">
            <xsl:choose>
              <xsl:when test="$Change>=0">
                <xsl:value-of select="format-number($Change,$ChangeFormatString,'data')"/><xsl:value-of select="$ChangeText"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="format-number(-$Change,$NegativeChangeFormatString,'data')"/><xsl:value-of select="$ChangeText"/>
              </xsl:otherwise>
            </xsl:choose>
            </td>
          </xsl:if>
        </xsl:if>
        </xsl:otherwise>
      </xsl:choose>
      </xsl:for-each>
    </xsl:for-each>
  </tr>
</xsl:template>


<!-- END Template 3 -->

<xsl:template match="irw:Page">
<SCRIPT LANGUAGE="JavaScript">
function DefWindow(KeyInstn, ItemName, EOP) {
var page = &quot;definitions.aspx?IID=&quot; + KeyInstn + &quot;&amp;Item=&quot; + ItemName + &quot;&amp;EOP=&quot; + EOP;
var winprops = "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=300,height=130";
popup  = window.open(page, "Definitions", winprops)
}
</SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
function TermsWindow(lKeyInstn) {
var page = &quot;disclaimer.aspx?IID=&quot; + lKeyInstn;
var winprops = "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=600,height=500";
popup  = window.open(page, "TermsofUse", winprops)
}
</SCRIPT>

<xsl:variable name="TemplateName" select="Company/TemplateName"/>
<xsl:variable name="keyinstn" select="Company/KeyInstn" />
<xsl:variable name="keyindex" select="Company/KeyIndex" />
<xsl:variable name="formaction" select="concat('finl.aspx?IID=', $keyinstn)"></xsl:variable>

<xsl:choose>
<!-- Template ONE -->
<xsl:when test="$TemplateName = 'AltFinl1'">
<xsl:call-template name="TemplateONEstylesheet" />

<xsl:call-template name="TemplateONEstylesheet" />
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" CLASS="">
<xsl:call-template name="Title_S2"/>
  <TR ALIGN="CENTER">
      <TD CLASS="leftTOPbord" colspan="2">
        <form method="POST" id="form1" name="form1"><xsl:attribute name="action"><xsl:value-of select="$formaction"/></xsl:attribute>
          <TABLE BORDER="0" WIDTH="100%" CELLPADDING="6" cellspacing="0">
            <xsl:call-template name="Trend_switch" />
            <xsl:call-template name="Trend_data1" />
            <xsl:call-template name="Trend_footer" />
          </TABLE>
        </form>
      </TD>
  </TR>
  <tr>
    <td></td>
  </tr>
  <tr>
    <td CLASS="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span></td>
  </tr>
</TABLE>
<xsl:call-template name="Copyright"/>
</xsl:when>
<!-- End Template ONE -->


<!-- Template 3 -->
<xsl:when test="$TemplateName = 'AltFinl3'">
<xsl:call-template name="TemplateTHREEstylesheet" />
<xsl:call-template name="Title_T3" />
<table BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" CLASS="colordark" ID="Table2">
  <tr>
    <td CLASS="colordark" colspan="2">
        <form method="POST" id="form1" name="form1"><xsl:attribute name="action"><xsl:value-of select="$formaction"/></xsl:attribute>
          <table BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%">
            <xsl:call-template name="Trend_switch" />
            <xsl:call-template name="Trend_data3" />
            <xsl:call-template name="Trend_footer" />
          </table>
        </form>
      </td>
  </tr>
  <tr>
    <td></td>
  </tr>
  <tr>
    <td CLASS="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span></td>
  </tr>
</table>
<xsl:call-template name="Copyright"/>
</xsl:when>
<!-- End Template THREE -->


<!-- Default Template  -->
<xsl:otherwise>
<!-- Copy from here down

<xsl:template match="irw:IRW/irw:Page">

<SCRIPT LANGUAGE="JavaScript">
function DefWindow(KeyInstn, ItemName, EOP) {
var page = &quot;definitions.aspx?IID=&quot; + KeyInstn + &quot;&amp;Item=&quot; + ItemName + &quot;&amp;EOP=&quot; + EOP;
var winprops = "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=300,height=100";
popup  = window.open(page, "Definitions", winprops)
}
</SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
function TermsWindow(lKeyInstn) {
var page = &quot;disclaimer.aspx?IID=&quot; + lKeyInstn;
var winprops = "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=600,height=500";
popup  = window.open(page, "TermsofUse", winprops)
}
</SCRIPT>

<xsl:variable name="keyinstn" select="Company/KeyInstn" />
<xsl:variable name="keyindex" select="Company/KeyIndex" />
<xsl:variable name="formaction" select="concat('finl.aspx?IID=', $keyinstn)"></xsl:variable>
<form method="POST" id="form1" name="form1"><xsl:attribute name="action"><xsl:value-of select="$formaction"/></xsl:attribute>
-->
<table BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" CLASS="colordark" ID="Table1">
  <xsl:call-template name="Title_S1" />
  <tr ALIGN="CENTER">
    <td CLASS="colordark" colspan="2">
        <form method="POST" id="form1" name="form1"><xsl:attribute name="action"><xsl:value-of select="$formaction"/></xsl:attribute>
          <table BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%">
            <xsl:call-template name="Trend_switch" />
            <xsl:call-template name="Trend_data" />
            <xsl:call-template name="Trend_footer" />
          </table>
        </form>
    </td>
  </tr>
  <tr>
    <td></td>
  </tr>
  <tr>
    <td CLASS="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span></td>
  </tr>
</table>
<xsl:call-template name="Copyright"/>
<!--
</form>
</xsl:template>

End Copy Here
-->
</xsl:otherwise>

<!--End  Default Template -->

</xsl:choose>
</xsl:template>

</xsl:stylesheet>
