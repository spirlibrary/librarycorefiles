<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.ord/TR/REC-html40"
	xmlns:irw="http://www.snl.com/xml/irw">

<xsl:output method="html" media-type="text/html" indent="no"/>
<xsl:decimal-format name="data" NaN="NA"/>

<!-- Default Build for Branch Mapping -->

<xsl:template name="BranchMap_Title">
	<xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
	<TR>
		<TD CLASS="colordark" NOWRAP="true" border="0" align="left"><SPAN CLASS="title1light"><xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/></SPAN></TD>
		<TD CLASS="colordark" NOWRAP="true" align="right" valign="top" border="0">
			<span class="subtitlelight"><xsl:value-of select="$CompanyNameCaps"/> (<xsl:value-of select="Company/Exchange"/> - <xsl:value-of select="Company/Ticker"/>)</span><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
		</TD>
	</TR>
	<TR class="default" align="left">
		<TD class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Header" disable-output-escaping="yes"/></span></TD>
	</TR>
</xsl:template>

<xsl:template name="FooterTag">
  <xsl:if test="Company/Footer != ''">
    <tr class="colorlight">
      <td colspan="2"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></td>
    </tr>    
  </xsl:if>
</xsl:template>
  
<xsl:template name="FooterTag3">
<xsl:if test="Company/Footer != ''">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="data">
  <tr>
    <td class="data"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></td>
  </tr>
  </table>
</xsl:if>
</xsl:template>

<xsl:template name="PropertyData">
	<table id="propertydata" cellspacing="0" cellpadding="2" width="100%">
	<tr>
		<td align="left" valign="bottom" class="header">Property</td>
		<td align="left" valign="bottom" class="header">Property Type</td>
		<td align="left" valign="bottom" class="header">Address</td>
		<td align="left" valign="bottom" class="header">City</td>
		<td align="left" valign="bottom" class="header">State</td>
		<td align="left" valign="bottom" class="header">Zip</td>
		<td mytype="comma_numeric" align="right" valign="bottom" nowrap="" class="header">Percent Owned</td>
	</tr>

	<xsl:for-each select="PropertyInfo">
	<tr>
		<td align="left" class="data"><xsl:value-of select="PropertyName" disable-output-escaping="yes"/></td>
		<td valign="top" class="data"><xsl:value-of select="KeyPropertyType" disable-output-escaping="yes" /></td>
		<td valign="top" class="data"><xsl:value-of select="Address" disable-output-escaping="yes" /></td>
		<td valign="top" class="data"><xsl:value-of select="City"/></td>
		<td valign="top" class="data"><xsl:value-of select="State"/></td>
		<td valign="top" class="data"><xsl:value-of select="Zip"/></td>
		<td valign="top" class="data" align="right"><xsl:value-of select="format-number(PropertyPercentOwned,'#,##0.00','data')"/></td>
	</tr>
	</xsl:for-each>

	</table>
</xsl:template>

<xsl:template name="ZoomImage">
  <xsl:param name="ImageID"/>
  <xsl:param name="CurrentZoomLevel"/>
  
  <xsl:variable name="ImageLevel" select="number(substring($ImageID, 21, string-length($ImageID)-20)) "/>
  <xsl:variable name="q">'</xsl:variable>
	<td class="cellbglight">
		<label class="visuallyhidden" for="{$ImageID}">Branch</label>
		<input>
		  <xsl:attribute name="type">image</xsl:attribute>
		  <xsl:attribute name="id"><xsl:value-of select="$ImageID"/></xsl:attribute>
		  <xsl:attribute name="Width">11px</xsl:attribute>
		  <xsl:attribute name="Height">15px</xsl:attribute>
		  <xsl:attribute name="src">
			<xsl:choose>
			  <xsl:when test="number($CurrentZoomLevel)=$ImageLevel">images/zoom_arrow_current.gif</xsl:when>
			  <xsl:when test="number($CurrentZoomLevel)&gt;$ImageLevel">images/zoom_arrow_in.gif</xsl:when>
			  <xsl:when test="number($CurrentZoomLevel)&lt;$ImageLevel">images/zoom_arrow_out.gif</xsl:when>
			</xsl:choose>		  
		  </xsl:attribute>  
		  <xsl:attribute name="onClick"><xsl:value-of select="concat('setAction(this.form, ', $q, 'Zoom', $ImageLevel, $q, ');')"/></xsl:attribute>
		</input>
	</td>
</xsl:template>


<!-- End Default Build for Branch Mapping -->












<!-- Start Alternative Build for Branch Mapping -->

<xsl:template name="BranchMap_Title_new">
	<xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
	<TR>
		<TD CLASS="title2colordark titletest" nowrap="" valign="top"><xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/><br /><IMG SRC="" WIDTH="2" HEIGHT="1" alt="" /><span class="title2colordark titletest2"><xsl:value-of select="$CompanyNameCaps"/>&#160;(<xsl:value-of select="Company/Exchange"/>&#160;-&#160;<xsl:value-of select="Company/Ticker"/>)&#160;</span></TD>
		<TD CLASS="" nowrap="" align="right" valign="top"></TD>
	</TR>
</xsl:template>


<xsl:template name="ZoomImage_new">
  <xsl:param name="ImageID"/>
  <xsl:param name="CurrentZoomLevel"/>  
  <xsl:variable name="ImageLevel" select="number(substring($ImageID, 21, string-length($ImageID)-20)) "/>
  <xsl:variable name="q">'</xsl:variable>
	<td class="cellbglight">
		<label class="visuallyhidden" for="{$ImageID}">Branch</label>
		<input>
		  <xsl:attribute name="type">image</xsl:attribute>
		  <xsl:attribute name="id"><xsl:value-of select="$ImageID"/></xsl:attribute>
		  <xsl:attribute name="Width">11px</xsl:attribute>
		  <xsl:attribute name="Height">15px</xsl:attribute>
		  <xsl:attribute name="src">
			<xsl:choose>
			  <xsl:when test="number($CurrentZoomLevel)=$ImageLevel">images/branchon.gif</xsl:when>
			  <xsl:when test="number($CurrentZoomLevel)&gt;$ImageLevel">images/branchoff.gif</xsl:when>
			  <xsl:when test="number($CurrentZoomLevel)&lt;$ImageLevel">images/branchoff.gif</xsl:when>
			</xsl:choose>		  
		  </xsl:attribute>  
		  <xsl:attribute name="onClick"><xsl:value-of select="concat('setAction(this.form, ', $q, 'Zoom', $ImageLevel, $q, ');')"/></xsl:attribute>
		</input>
	</td>
</xsl:template>



<xsl:template name="ZoomImage_small">
  <xsl:param name="ImageID"/>
  <xsl:param name="CurrentZoomLevel"/>  
  <xsl:variable name="ImageLevel" select="number(substring($ImageID, 21, string-length($ImageID)-20)) "/>
  <xsl:variable name="q">'</xsl:variable>
	<td class="cellbglight">
		<label class="visuallyhidden" for="{$ImageID}">Branch</label>
		<input>
		  <xsl:attribute name="type">image</xsl:attribute>
		  <xsl:attribute name="id"><xsl:value-of select="$ImageID"/></xsl:attribute>
		  <xsl:attribute name="Width">9px</xsl:attribute>
		  <xsl:attribute name="Height">9px</xsl:attribute>
		  <xsl:attribute name="src">
			<xsl:choose>
			  <xsl:when test="number($CurrentZoomLevel)=$ImageLevel">images/branchon_small.gif</xsl:when>
			  <xsl:when test="number($CurrentZoomLevel)&gt;$ImageLevel">images/branchoff_small.gif</xsl:when>
			  <xsl:when test="number($CurrentZoomLevel)&lt;$ImageLevel">images/branchoff_small.gif</xsl:when>
			</xsl:choose>		  
		  </xsl:attribute>  
		  <xsl:attribute name="onClick"><xsl:value-of select="concat('setAction(this.form, ', $q, 'Zoom', $ImageLevel, $q, ');')"/></xsl:attribute>
		</input>
	</td>
</xsl:template>



<!-- End Alternative Build -->

<!-- Alternate Branch Page Build -->
<!--



-->
<!-- START TEMPLATE 1 -->

<xsl:template name="PropertyData2">
		<table class="sortable" id="propertydata" cellspacing="0" cellpadding="2" width="100%">
		<tr>
			<td align="left" valign="bottom" class="surrleft header">Property</td>
			<td align="left" valign="bottom" class="surr_topbot header">Property Type</td>
			<td align="left" valign="bottom" class="surr_topbot header">Address</td>
			<td align="left" valign="bottom" class="surr_topbot header">City</td>
			<td align="left" valign="bottom" class="surr_topbot header">State</td>
			<td align="left" valign="bottom" class="surr_topbot header">Zip</td>
			<td mytype="comma_numeric" align="right" valign="bottom" nowrap="" class="surrright header">Percent Owned</td>
		</tr>

	<xsl:for-each select="PropertyInfo">
	<tr>
		<td align="left" class="data"><xsl:value-of select="PropertyName" disable-output-escaping="yes"/></td>
		<td valign="top" class="data"><xsl:value-of select="KeyPropertyType" disable-output-escaping="yes" /></td>
		<td valign="top" class="data"><xsl:value-of select="Address" disable-output-escaping="yes" /></td>
		<td valign="top" class="data"><xsl:value-of select="City"/></td>
		<td valign="top" class="data"><xsl:value-of select="State"/></td>
		<td valign="top" class="data"><xsl:value-of select="Zip"/></td>
		<td valign="top" class="data" align="right"><xsl:value-of select="format-number(PropertyPercentOwned,'#,##0.00','data')"/></td>
	</tr>
	</xsl:for-each>

	</table>
</xsl:template>

<!-- END TEMPLATE1-->


<!-- START TEMPLATE 3 -->


<xsl:template name="PropertyData3">
	<table class="sortable table1 table1_item" id="propertydata" cellspacing="0" cellpadding="2" width="100%">
	<tr>
		<td align="left" valign="bottom" class="table1_item header">Property</td>
		<td align="left" valign="bottom" class="table1_item header">Property Type</td>
		<td align="left" valign="bottom" class="table1_item header">Address</td>
		<td align="left" valign="bottom" class="table1_item header">City</td>
		<td align="left" valign="bottom" class="table1_item header">State</td>
		<td align="left" valign="bottom" class="table1_item header">Zip</td>
		<td mytype="comma_numeric" align="right" valign="bottom" nowrap="" class="table1_item header">Percent Owned</td>
	</tr>

	<xsl:for-each select="PropertyInfo">
	<tr>
		<td align="left" class="data"><xsl:value-of select="PropertyName" disable-output-escaping="yes"/></td>
		<td valign="top" class="data"><xsl:value-of select="KeyPropertyType" disable-output-escaping="yes" /></td>
		<td valign="top" class="data"><xsl:value-of select="Address" disable-output-escaping="yes"/></td>
		<td valign="top" class="data"><xsl:value-of select="City"/></td>
		<td valign="top" class="data"><xsl:value-of select="State"/></td>
		<td valign="top" class="data"><xsl:value-of select="Zip"/></td>
		<td valign="top" class="data" align="right"><xsl:value-of select="format-number(PropertyPercentOwned,'#,##0.00','data')"/></td>
	</tr>
	</xsl:for-each>

	</table>
</xsl:template>

<!-- END TEMPLATE3-->


<xsl:template match="irw:PropertyMap">
<xsl:variable name="NameTemplate" select="/irw:IRW/*/Company/URL"/> 
<xsl:variable name="TemplateName" select="Company/TemplateName"/> 
  <style type="text/css">   
     @import url("IRStyle.css");
  </style>
  <div id="outer" class="Propertymap">
  <xsl:choose>
    <!-- template1-->
    <xsl:when test="$TemplateName = 'AltPropertyMap1' or contains($NameTemplate, 'template=1')">
      <xsl:call-template name="TemplateONEstylesheet" />
      <table border="0" cellspacing="0" cellpadding="3" width="100%" CLASS="data">
        <xsl:call-template name="Title_S2"/>   
        <TR ALIGN="CENTER">
          <td CLASS="leftTOPbord" colspan="2">            
            <form id="branchmapform" name="branchmapform" method="post">
				<fieldset>
					<legend>Branchform</legend>
              <xsl:call-template name="MapDataTable" />
              <table width="100%" border="0" cellspacing="0" cellpadding="3" class="data">
                <tr>
                  <td valign="top" align="center">
                    <xsl:call-template name="BuildMap"/>
                  </td>
                </tr>
                <tr><td vAlign="top" align="center">&#160;</td></tr>              
                <xsl:if test="count(PropertyInfo) &gt; 0 and PropertyMapOptions/DisplayPropertyAttributes = 'true'">
				<tr>
                  <td valign="top">                                         
                      <xsl:call-template name="PropertyData5" />
                  </td>
                </tr>         
                </xsl:if>                     
              </table>
				</fieldset>
			</form>
            <xsl:call-template name="FooterTag3"/>
          </td>
        </TR>
      </table>
    </xsl:when>
    <!-- template3-->
    <xsl:when test="$TemplateName = 'AltPropertyMap3' or contains($NameTemplate, 'template=3')">
      <xsl:call-template name="TemplateTHREEstylesheet"/>
      <xsl:call-template name="Title_T3"/>
      <TABLE border="0" cellspacing="0" cellpadding="3" width="100%" class="table2">      
        <TR ALIGN="CENTER">
          <TD CLASS="data" valign="top">
            <form id="branchmapform" name="branchmapform" method="post">
				<fieldset>
					<legend>Branchform</legend>
					<xsl:call-template name="MapDataTable" />
					<table width="100%" border="0" cellspacing="0" cellpadding="3" class="data">
						<tr>
							<td vAlign="top" align="center">
								<xsl:call-template name="BuildMap"/>
							</td>
						</tr>
						<tr>
							<td vAlign="top" align="center">&#160;</td>
						</tr>
						<xsl:if test="count(PropertyInfo) &gt; 0 and PropertyMapOptions/DisplayPropertyAttributes = 'true'">
							<tr>
								<td valign="top">
									<xsl:call-template name="PropertyData5" />
								</td>
							</tr>
						</xsl:if>
					</table>
				</fieldset>
					</form>
            <xsl:call-template name="FooterTag3"/>
          </TD>
        </TR>
      </TABLE>
    </xsl:when>

    <!-- Default template -->
    <xsl:otherwise>      
      <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" CLASS="colordark">
        <xsl:call-template name="Title_S1" />
        <TR ALIGN="CENTER">
          <TD CLASS="colordark" colspan="2" valign="top">
            <form id="branchmapform" name="branchmapform" method="post">

				<fieldset>
					<legend>Branchform</legend>
					<xsl:call-template name="MapDataTable" />
					<table width="100%" border="0" cellspacing="0" cellpadding="3" class="colorlight">
						<tr>
							<td vAlign="top" align="center">
								<xsl:call-template name="BuildMap" />
							</td>
						</tr>
						<tr>
							<td vAlign="top" align="center">&#160;</td>
						</tr>
						<xsl:if test="count(PropertyInfo) &gt; 0 and PropertyMapOptions/DisplayPropertyAttributes = 'true'">
							<tr>
								<td valign="top">
									<table id="Propertytable" border="0" cellspacing="0" cellpadding="0" class="header">
										<tr>
											<td valign="top">
												<xsl:call-template name="PropertyData5" />
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</xsl:if>
					</table>
				</fieldset>
					</form>
          </TD>          
        </TR>
        <xsl:call-template name="FooterTag" />           
      </TABLE>
    </xsl:otherwise>
  </xsl:choose> 
      </div>
  </xsl:template>

  <xsl:template name="BuildMap">
      <table id="Table2" CellPadding="0" CellSpacing="0" BorderWidth="0" border="0" width="100%" height="480">
          <tr valign="top">
              <td valign="top" align="center" nowrap="1">
                  <table id="Table3" CellPadding="0" CellSpacing="0" BorderWidth="0" border="0" width="540" class="branchmap">
                      <tr>
                          <td valign="top" align="left">
                              <xsl:value-of select="Map/ImgSrc" disable-output-escaping="yes"/>
                          </td>
                      </tr>
                  </table>
              </td>
          </tr>
          <tr><td valign="top" align="left" class="bottomSpace">&#160;</td></tr>
      </table>
  </xsl:template>   
    

<xsl:template name="MapDataTable">
  <xsl:call-template name="CommonScript_4" />    
  <xsl:if test="not(contains(translate(Company/URL,$ucletters,$lcletters), 'print=1'))">
  <script language="JavaScript" >
      <![CDATA[
      var oTable;      
      //var PropMap;
      var $pmap = jQuery.noConflict();
      function drawmap(Query, count, id) 
      {      
        var widgets = dijit.findWidgets(BBmap);
        dojo.forEach(widgets, function(w) {
        w.destroyRecursive(true);
        });                
        dojo.empty(BBmap);
        var label= $pmap(".legendItem").html().replace(/(\([^)]+\))/,'(' + count +')');        
        $pmap(".legendItem").html(label);
        var layerQuery = BankBranchMap.DynamicLayers;        
        layerQuery[0]["LayerQuery"]= "KeyInstn = " + ]]><xsl:value-of select="Company/KeyInstn"/><![CDATA[ + " and KeyProp in (" + Query + ")";        
        BankBranchMap.DynamicLayers = layerQuery;
        BankBranchMap.loadMap();
        
        ]]><xsl:if test="count(PropertyInfo) &gt; 0"><![CDATA[
        
        if(id != undefined && ]]><xsl:value-of select="PropertyMapOptions/MapPointSelectable"/><![CDATA[ == true){
             BankBranchMap.HighlightMap("and KeyProp = " +id );
        }
        
        ]]></xsl:if> <![CDATA[
         
        updateScrollbar(); //Reset Horizontal Vertical scrollbar
         
    }
    
      function fnSearch()
      { 
        var count = 0;
        var Key="";           
        var id;
          ]]><xsl:if test="PropertyMapOptions/DisplayPropertyAttributes = 'true'">
            <![CDATA[          
          $pmap(".keyprop").each(function () {       
           if (this.parentNode.className.toLowerCase().indexOf("row_selected") >= 0)
            { 
              id = $pmap.trim($pmap(this).html());
            }
           Key =  $pmap.trim($pmap(this).html()) + ',' + Key;
           count++;
          });
          ]]></xsl:if> <![CDATA[
          
         ]]><xsl:if test="PropertyMapOptions/DisplayPropertyAttributes = 'false'"><xsl:for-each select="PropertyInfo"><![CDATA[
           Key =  ]]><xsl:value-of select="keyprop"/><![CDATA[ + ',' + Key;
            count++;
         ]]></xsl:for-each></xsl:if><![CDATA[      
        Key = Key.substring(0, Key.length - 1);            
        drawmap(Key, count, id);
      }     
      
       function HighlightGrid(features, layer)
          {          
              $pmap.each(features, function(index, value) {
                var loopbrk;                
               	$pmap(".keyprop").each(function () {                                
                    if($pmap(this).text() == value.attributes.KeyProp){                                   
                        $pmap("#propertydata tr").removeClass('row_selected');
                        $pmap(this).parent().addClass('row_selected');
						$pmap("DIV.dataTables_scrollBody").mCustomScrollbar("scrollTo",".row_selected");
						
                        loopbrk=false;                        
                        return loopbrk;
                    }
                });
                return loopbrk;
            });					  
          }      
      
    ]]>
  </script>
    <xsl:if test="count(PropertyInfo/*[2]) &gt; 0 and PropertyMapOptions/DisplayPropertyAttributes = 'true'">
      <script language="javascript" type="text/javascript" src="{$ScriptPath}/datatables/jquery.dataTables.min.js"></script>
      
      <script language="javascript" type="text/javascript">        
      <![CDATA[ 
        var mapPointClickable, mapWidth, MapHight;
        $pmap(document).ready(function() {            
            mapPointClickable = ']]><xsl:value-of select="PropertyMapOptions/MapPointSelectable"/><![CDATA[';            
            mapWidth = $pmap("#outer").width() - 10;
					  $pmap("#MapdataTables, #MapdataTables .display").width(mapWidth);	
            $pmap("#MapdataTables").width(mapWidth);
			MapHight = '200';
			var hMapData = $pmap('#propertydata tbody').outerHeight() + $pmap('#propertydata tbody tr').length - 1;
			   if ( hMapData < '200') {
				MapHight = hMapData
			   }
         
         $pmap.fn.dataTableExt.oSort['numeric_ignore_nan-asc']  = function(x,y) {
            if (isNaN(x) && isNaN(y)) return ((x < y) ? 1 : ((x > y) ?  -1 : 0));
 
            if (isNaN(x)) return 1;
            if (isNaN(y)) return -1;
 
            x = parseFloat( x );
            y = parseFloat( y );
            return ((x < y) ? -1 : ((x > y) ?  1 : 0));
        };
  
        $pmap.fn.dataTableExt.oSort['numeric_ignore_nan-desc'] = function(x,y) {
            if (isNaN(x) && isNaN(y)) return ((x < y) ? 1 : ((x > y) ?  -1 : 0));
 
            if (isNaN(x)) return 1;
            if (isNaN(y)) return -1;
 
            x = parseFloat( x );
            y = parseFloat( y );
            return ((x < y) ?  1 : ((x > y) ? -1 : 0));
        };
         
           $pmap(window).load(function(){           
               oTable  =  $pmap('#propertydata').dataTable({ "sScrollY": MapHight, "sScrollX": "100%", "bSort": true, "bLengthChange": false, "bStateSave": false, "bProcessing": false, "bInfo": false, "bPaginate": false, "aaSorting": [[ 1, "asc" ]], "aoColumns": [ { "bSearchable": false}
			]]>
			   <xsl:if test="count(PropertyInfo/PropertyName) &gt; 0"><![CDATA[,null]]></xsl:if>
			   <xsl:if test="count(PropertyInfo/KeyPropertyType) &gt; 0"><![CDATA[,null]]></xsl:if>
			   <xsl:if test="count(PropertyInfo/PropertySize) &gt; 0"><![CDATA[,null]]></xsl:if>
			   <xsl:if test="count(PropertyInfo/Address) &gt; 0"><![CDATA[,null]]></xsl:if>
			   <xsl:if test="count(PropertyInfo/City) &gt; 0"><![CDATA[,null]]></xsl:if>
			   <xsl:if test="count(PropertyInfo/State) &gt; 0"><![CDATA[,null]]></xsl:if>
			   <xsl:if test="count(PropertyInfo/County) &gt; 0"><![CDATA[,null]]></xsl:if>
			   <xsl:if test="count(PropertyInfo/Country) &gt; 0"><![CDATA[,null]]></xsl:if>
			   <xsl:if test="count(PropertyInfo/Zip) &gt; 0"><![CDATA[,null]]></xsl:if>
			   <xsl:if test="count(PropertyInfo/MSA) &gt; 0"><![CDATA[,null]]></xsl:if>
			   <xsl:if test="count(PropertyInfo/PropertyPercentOwned) &gt; 0"><![CDATA[, {"sType": "numeric_ignore_nan" }]]></xsl:if>
			<![CDATA[  
              ]             
          });
              $pmap("input#search_docs").bind("keyup",fnSearch);      
              $pmap("#MapdataTables, #Propertytable").css("visibility","visible");
              $pmap("#propertydata").css({"visibility":"visible","position":"relative", "bottom": "0px"}); 
			  
			  var Headercolor = $pmap("div.dataTables_scrollHeadInner table.display td:last").css("background-color");			  
			  $pmap(".dataTables_scrollHead").css({
				'background-color' : Headercolor				
			  });		
				$pmap("#propertydata tbody tr td:last-child").each(function(){					
					$pmap(this).css({'padding-right' : '15px'});
				});	  
            });
             
            $pmap("#propertydata tbody").click(function(event) {
		           $pmap(oTable.fnSettings().aoData).each(function (){			        
						    $pmap(this.nTr).removeClass('row_selected');
		          });
		          $pmap(event.target.parentNode).addClass('row_selected');
                var id = $pmap(event.target.parentNode).find('td.keyprop').text();
                if(]]><xsl:value-of select="PropertyMapOptions/MapPointSelectable"/><![CDATA[ == true){
                  BankBranchMap.HighlightMap("and KeyProp = " +id );				     
                }
	          });
	    });
      ]]>
      </script>
      
	  <xsl:call-template name="jQueryslimScroll" />
    </xsl:if>
  </xsl:if>
</xsl:template>
  
<xsl:template name="PropertyData5">
<xsl:variable name="NameTemplate" select="/irw:IRW/*/Company/URL"/>
<xsl:variable name="TemplateName" select="/irw:IRW/*/Company/TemplateName"/> 
	<div id="MapdataTables" class="MapdataTables">
	<xsl:choose>
		<xsl:when test="$TemplateName = 'AltPropertyMap1' or contains($NameTemplate, 'template=1')"><xsl:attribute name="class">MapdataTables mapstyle1</xsl:attribute></xsl:when>
		<xsl:when test="$TemplateName = 'AltPropertyMap3' or contains($NameTemplate, 'template=3')"><xsl:attribute name="class">MapdataTables mapstyle3</xsl:attribute></xsl:when>
		<xsl:otherwise><xsl:attribute name="class">MapdataTables mapstyle0 data</xsl:attribute></xsl:otherwise>
	</xsl:choose> 
	  <table id="propertydata" class="display" border="0" cellspacing="0" cellpadding="3">
      <thead>
	    <tr>
		  <td valign="top" align="left" style="display:none"><span>Key Property</span></td>
        <xsl:if test="count(PropertyInfo/PropertyName) &gt; 0">
		      <td valign="top" class="header">
				<xsl:choose>
					<xsl:when test="$TemplateName = 'AltPropertyMap1' or contains($NameTemplate, 'template=1') or $TemplateName = 'AltPropertyMap3' or contains($NameTemplate, 'template=3')"><xsl:attribute name="class">datashade defaultbold</xsl:attribute></xsl:when>
					<xsl:otherwise><xsl:attribute name="class">header</xsl:attribute></xsl:otherwise>
				</xsl:choose>
			  
			  <span>Property</span></td>
        </xsl:if>
        <xsl:if test="count(PropertyInfo/KeyPropertyType) &gt; 0">
		      <td valign="top" align="left" class="header">
				<xsl:choose>
					<xsl:when test="$TemplateName = 'AltPropertyMap1' or contains($NameTemplate, 'template=1') or $TemplateName = 'AltPropertyMap3' or contains($NameTemplate, 'template=3')"><xsl:attribute name="class">datashade defaultbold</xsl:attribute></xsl:when>
					<xsl:otherwise><xsl:attribute name="class">header</xsl:attribute></xsl:otherwise>
				</xsl:choose><span>Property Type</span></td>
        </xsl:if>  
        <xsl:if test="count(PropertyInfo/PropertySize) &gt; 0">
          <td valign="top" align="left" class="header">
				<xsl:choose>
					<xsl:when test="$TemplateName = 'AltPropertyMap1' or contains($NameTemplate, 'template=1') or $TemplateName = 'AltPropertyMap3' or contains($NameTemplate, 'template=3')"><xsl:attribute name="class">datashade defaultbold</xsl:attribute></xsl:when>
					<xsl:otherwise><xsl:attribute name="class">header</xsl:attribute></xsl:otherwise>
				</xsl:choose><span>Property Size</span></td>
        </xsl:if>  
        <xsl:if test="count(PropertyInfo/Address) &gt; 0">
		      <td valign="top" align="left" class="header">
				<xsl:choose>
					<xsl:when test="$TemplateName = 'AltPropertyMap1' or contains($NameTemplate, 'template=1') or $TemplateName = 'AltPropertyMap3' or contains($NameTemplate, 'template=3')"><xsl:attribute name="class">datashade defaultbold</xsl:attribute></xsl:when>
					<xsl:otherwise><xsl:attribute name="class">header</xsl:attribute></xsl:otherwise>
				</xsl:choose><span>Address</span></td>
        </xsl:if>  
        <xsl:if test="count(PropertyInfo/City) &gt; 0">
		      <td valign="top" align="left" class="header">
				<xsl:choose>
					<xsl:when test="$TemplateName = 'AltPropertyMap1' or contains($NameTemplate, 'template=1') or $TemplateName = 'AltPropertyMap3' or contains($NameTemplate, 'template=3')"><xsl:attribute name="class">datashade defaultbold</xsl:attribute></xsl:when>
					<xsl:otherwise><xsl:attribute name="class">header</xsl:attribute></xsl:otherwise>
				</xsl:choose><span>City</span></td>
        </xsl:if>  
        <xsl:if test="count(PropertyInfo/State) &gt; 0">
		      <td valign="top" align="left" class="header">
				<xsl:choose>
					<xsl:when test="$TemplateName = 'AltPropertyMap1' or contains($NameTemplate, 'template=1') or $TemplateName = 'AltPropertyMap3' or contains($NameTemplate, 'template=3')"><xsl:attribute name="class">datashade defaultbold</xsl:attribute></xsl:when>
					<xsl:otherwise><xsl:attribute name="class">header</xsl:attribute></xsl:otherwise>
				</xsl:choose><span>State</span></td>
        </xsl:if>  
        <xsl:if test="count(PropertyInfo/County) &gt; 0">
          <td valign="top" align="left" class="header">
				<xsl:choose>
					<xsl:when test="$TemplateName = 'AltPropertyMap1' or contains($NameTemplate, 'template=1') or $TemplateName = 'AltPropertyMap3' or contains($NameTemplate, 'template=3')"><xsl:attribute name="class">datashade defaultbold</xsl:attribute></xsl:when>
					<xsl:otherwise><xsl:attribute name="class">header</xsl:attribute></xsl:otherwise>
				</xsl:choose><span>County</span></td>
        </xsl:if>  
        <xsl:if test="count(PropertyInfo/Country) &gt; 0">
          <td valign="top" align="left" class="header">
				<xsl:choose>
					<xsl:when test="$TemplateName = 'AltPropertyMap1' or contains($NameTemplate, 'template=1') or $TemplateName = 'AltPropertyMap3' or contains($NameTemplate, 'template=3')"><xsl:attribute name="class">datashade defaultbold</xsl:attribute></xsl:when>
					<xsl:otherwise><xsl:attribute name="class">header</xsl:attribute></xsl:otherwise>
				</xsl:choose><span>Country</span></td>
        </xsl:if>  
        <xsl:if test="count(PropertyInfo/Zip) &gt; 0">
		      <td valign="top" align="left" class="header">
				<xsl:choose>
					<xsl:when test="$TemplateName = 'AltPropertyMap1' or contains($NameTemplate, 'template=1') or $TemplateName = 'AltPropertyMap3' or contains($NameTemplate, 'template=3')"><xsl:attribute name="class">datashade defaultbold</xsl:attribute></xsl:when>
					<xsl:otherwise><xsl:attribute name="class">header</xsl:attribute></xsl:otherwise>
				</xsl:choose><span>Zip</span></td>
        </xsl:if>  
        <xsl:if test="count(PropertyInfo/MSA) &gt; 0">
          <td valign="top" align="left" class="header">
				<xsl:choose>
					<xsl:when test="$TemplateName = 'AltPropertyMap1' or contains($NameTemplate, 'template=1') or $TemplateName = 'AltPropertyMap3' or contains($NameTemplate, 'template=3')"><xsl:attribute name="class">datashade defaultbold</xsl:attribute></xsl:when>
					<xsl:otherwise><xsl:attribute name="class">header</xsl:attribute></xsl:otherwise>
				</xsl:choose><span>MSA</span></td>
        </xsl:if>  
        <xsl:if test="count(PropertyInfo/PropertyPercentOwned) &gt; 0">
		      <td valign="top" align="right" class="header">
				<xsl:choose>
					<xsl:when test="$TemplateName = 'AltPropertyMap1' or contains($NameTemplate, 'template=1') or $TemplateName = 'AltPropertyMap3' or contains($NameTemplate, 'template=3')"><xsl:attribute name="class">datashade defaultbold</xsl:attribute></xsl:when>
					<xsl:otherwise><xsl:attribute name="class">header</xsl:attribute></xsl:otherwise>
				</xsl:choose><span>Percent Owned</span></td>
        </xsl:if>          
	    </tr>
    </thead>
    <tbody>
	    <xsl:for-each select="PropertyInfo">
	    <tr>
        <xsl:if test="position() mod 2 = 0">
		  <xsl:if test="not(contains(translate(Company/URL,$ucletters,$lcletters), 'print=1'))">
			<xsl:attribute name="class">even</xsl:attribute>
		  </xsl:if>                
        </xsl:if>	
         <td class="keyprop" align="right" valign="top" style="display:none">
            <xsl:value-of select="keyprop"/>
         </td>
         <xsl:if test="count(PropertyName) &gt; 0">
		    <td align="left"><xsl:value-of select="PropertyName" disable-output-escaping="yes"/></td>
              </xsl:if>
        <xsl:if test="count(KeyPropertyType) &gt; 0">
		    <td valign="top" align="left"><xsl:value-of select="KeyPropertyType" disable-output-escaping="yes" /></td>
            </xsl:if>  
        <xsl:if test="count(PropertySize) &gt; 0">
        <td valign="top" align="left"><xsl:value-of select="PropertySize" disable-output-escaping="yes" /></td>
           </xsl:if>  
        <xsl:if test="count(Address) &gt; 0">
		    <td valign="top" align="left"><xsl:value-of select="Address" disable-output-escaping="yes" /></td>
           </xsl:if>  
        <xsl:if test="count(City) &gt; 0">
		    <td valign="top" align="left"><xsl:value-of select="City"/></td>
           </xsl:if>  
        <xsl:if test="count(State) &gt; 0">
		    <td valign="top" align="left"><xsl:value-of select="State"/></td>
           </xsl:if>  
        <xsl:if test="count(County) &gt; 0">          
        <td valign="top" align="left"><xsl:value-of select="County"/></td>
          </xsl:if>  
        <xsl:if test="count(Country) &gt; 0">
        <td valign="top" align="left"><xsl:value-of select="Country"/></td>
           </xsl:if>  
        <xsl:if test="count(Zip) &gt; 0">
		    <td valign="top" align="left"><xsl:value-of select="Zip"/></td>
           </xsl:if>  
        <xsl:if test="count(MSA) &gt; 0">
        <td valign="top" align="left"><xsl:value-of select="MSA"/></td>
           </xsl:if>  
        <xsl:if test="count(PropertyPercentOwned) &gt; 0">
		    <td valign="top" align="right"><xsl:value-of select="format-number(PropertyPercentOwned,'#,##0.00','data')"/></td>
          </xsl:if>          
        
	    </tr>
	    </xsl:for-each>
    </tbody>
	</table>
  </div>
</xsl:template>
  
</xsl:stylesheet>
