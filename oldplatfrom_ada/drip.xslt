<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.ord/TR/REC-html40"
	xmlns:irw="http://www.snl.com/xml/irw">

<xsl:output method="html" media-type="text/html" indent="no"/>


    <!-- templated message so it remains the same between templates 0, 1 & 3-->
    <xsl:template name="nodatadescription">
        Description of this page is not available.
    </xsl:template>

    
<!-- Start Default Template here-->

<!--
//This is the default template. Most changes are to either remove the company name, ticker, or exchange. you can also change the name of the page or remove this row all together. 
//These templates below are broken up how the page is displayed.
 
-->

<xsl:template name="Drip_header"> 
<xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
	<tr>
		<td CLASS="colordark" NOWRAP="NOWRAP">
			<SPAN CLASS="title1light"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text><xsl:value-of disable-output-escaping="yes" select="TitleInfo/TitleName"/></SPAN> 
	    </td>
		<td CLASS="colordark" NOWRAP="NOWRAP" align="right" valign="top"><span class="subtitlelight"><xsl:value-of select="$CompanyNameCaps"/> (<xsl:value-of select="Company/Exchange"/> - <xsl:value-of select="Company/Ticker"/>)</span><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
	</tr>
		<tr class="default" align="left">
			<td class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Header" disable-output-escaping="yes"/></span></td>
	</tr>
</xsl:template>  


<xsl:template name="Drip_data"> 
         <tr> 
           <td COLSPAN="2" class="large">

               <xsl:choose>
                   <xsl:when test="count(DripTable/DripText) > 0">
                       <!--  <b><xsl:value-of select="Company/InstnName" disable-output-escaping="yes"/></b><BR /> -->
                       <!--<xsl:value-of select="Company/Text" disable-output-escaping="yes"/>-->
                       <xsl:value-of select="DripTable/DripText" disable-output-escaping="yes"/>
                   </xsl:when>
                   <xsl:otherwise>
                       <xsl:call-template name="nodatadescription"/>
                   </xsl:otherwise>
               </xsl:choose>
               
               <P>
                   <a title="Click Here">
                       <xsl:attribute name="href">
                           infoRequest.aspx?IID=<xsl:value-of select="Company/KeyInstn"/>
                       </xsl:attribute>Click Here
                   </a>
                   <span class="defaultbold">
                       to receive more  information from <xsl:value-of select="Company/InstnName" disable-output-escaping="yes"/>
                   </span>
               </P>

           </td>
         </tr>
</xsl:template>   
         
       <xsl:template name="Drip_footer"> 
	<tr class="default" align="left">
		<td class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span></td>
	</tr>
</xsl:template>   
         
    
<!-- Default Template Ends here. all templates below are ONLY if you are using the 'Style Template' section in the console. -->



















<!-- This is Template ONE option in the console under 'Style Template'
//This template is a striped down version of the main template. Everything (data) is displayed in single rows.
//The sections will be commented on where the rows are. you should be able to just move rows (entire chunks of data) where you need too. Edits to this section are rare, mostly due to size (width of the page).
-->
 
  
<xsl:template name="Drip_header2">
<xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
<TR>
<TD CLASS=" titletest" nowrap="" colspan="2"><xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/><br /><IMG SRC="" WIDTH="2" HEIGHT="1" alt="" /><span class=" titletest2"><xsl:value-of select="$CompanyNameCaps"/>&#160;(<xsl:value-of select="Company/Exchange"/>&#160;-&#160;<xsl:value-of select="Company/Ticker"/>)&#160;</span></TD>

</TR>
	<TR class="default" align="left">
		<TD class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Header" disable-output-escaping="yes"/></span></TD>
	</TR>
</xsl:template>
       
         
<xsl:template name="Drip_data2"> 
	 <table BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%">
	 <tr> 
	     <td class="default" valign="top">
			<table BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%">
			<tr>
				<td>
					<!--<span class=" titletest2"><xsl:value-of select="Company/InstnName" disable-output-escaping="yes"/></span><BR />-->
					<span class="default">
					 <!--<xsl:value-of select="Company/Text" disable-output-escaping="yes"/>-->
                        <xsl:choose>
                            <xsl:when test="count(DripTable/DripText) > 0">
                                <xsl:value-of select="DripTable/DripText" disable-output-escaping="yes"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:call-template name="nodatadescription"/>                                
                            </xsl:otherwise>
                        </xsl:choose>
					</span>
				</td>
			 </tr>
			 <tr>
				<td class="surr datashade">
					<table BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%">
						<tr>
							<td>
								<a><xsl:attribute name="href">inforequest.aspx?IID=<xsl:value-of select="Company/KeyInstn"/></xsl:attribute>Click Here</a><span class=" titletest2"> to receive more  information from <xsl:value-of select="Company/InstnName" disable-output-escaping="yes"/> </span>
							</td>
						 </tr>
					 </table>
				</td>
			 </tr>
			 </table>	
	       </td>
	   </tr>
	   </table>
</xsl:template> 
	  
<xsl:template name="Drip_footer2"> 
	<tr class="default" align="left">
		<td class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span></td>
	</tr>
</xsl:template>   
         
          
         
  <!-- Template ONE Ends here. -->
  
  
  
  
<!-- Start Template 3 -->


<xsl:template name="Drip_header3">
<xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
	<TR>
		<TD CLASS="defaultbold" nowrap="" colspan="2"><xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/><br /><IMG SRC="" WIDTH="2" HEIGHT="1" alt=""/><span class="defaultbold"><xsl:value-of select="$CompanyNameCaps"/>&#160;(<xsl:value-of select="Company/Exchange"/>&#160;-&#160;<xsl:value-of select="Company/Ticker"/>)&#160;</span></TD>

	</TR>
	<TR class="default" align="left">
		<TD class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Header" disable-output-escaping="yes"/></span></TD>
	</TR>
</xsl:template>



<xsl:template name="Drip_data3"> 
<table BORDER="0" CELLSPACING="0" CELLPADDING="4" WIDTH="100%">
	<tr> 
		<td class="default" valign="top">
			<table BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%">
				<tr>
					<td><!--<span class="defaultbold"><xsl:value-of select="Company/InstnName" disable-output-escaping="yes"/></span><BR />-->
					<!--<span class="default"><xsl:value-of select="Company/Text" disable-output-escaping="yes"/></span></td>-->
                    <span class="default">
                        <xsl:choose>
                            <xsl:when test="count(DripTable/DripText) > 0">
                                <xsl:value-of select="DripTable/DripText" disable-output-escaping="yes"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:call-template name="nodatadescription"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </span></td>
				</tr>
				<tr>
					<td class="datashade">
						<table BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%">
							<tr>
								<td><a><xsl:attribute name="href">inforequest.aspx?IID=<xsl:value-of select="Company/KeyInstn"/></xsl:attribute>Click Here</a><span class="defaultbold"> to receive more  information from <xsl:value-of select="Company/InstnName" disable-output-escaping="yes"/> </span></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>	
		</td>
	</tr>
</table>
</xsl:template> 



<xsl:template name="Drip_footer3"> 
	<tr class="default" align="left">
		<td class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span></td>
	</tr>
</xsl:template>   
  
  <!-- End Template 3 -->
  
  
  
  
<!-- Main XSLT templates are here. these are the templates which are actually being displayed in the page. this is for TOP LEVEL editing. if you dont need to use the individual templates above you can do your main editing here 

when pulling THESE templates below you must carry the top comment with them and uncomment them when you drop it into the company specific xslt.




-->



<xsl:template match="irw:Drip">
<xsl:variable name="TemplateName" select="Company/TemplateName"/>
<xsl:choose>
<!-- Template 2 -->
<xsl:when test="$TemplateName = 'AltDrip1'">
		<xsl:call-template name="TemplateONEstylesheet"/> 
	<table BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" CLASS="">
	<xsl:call-template name="Title_S2"/>
	   <tr ALIGN="CENTER">
	     <td CLASS="leftTOPbord" colspan="2" valign="top">  
	 <table BORDER="0" CELLSPACING="0" CELLPADDING="4" WIDTH="100%">
	   <tr> 
	     <td CLASS="colorlight" COLSPAN="2" valign="top"> 
	       <table BORDER="0" CELLSPACING="0" CELLPADDING="4" WIDTH="100%">   
	       <tr>
	       <td  valign="top"><xsl:call-template name="Drip_data2"/></td></tr>
	       </table>
	     </td>
	   </tr>
	</table> 
	</td>
	   </tr>
	 <xsl:call-template name="Drip_footer2"/>       
	</table> 
<xsl:call-template name="Copyright"/>
</xsl:when>
<!-- End Template 2 -->

<!-- Template 3 -->
<xsl:when test="$TemplateName = 'AltDrip3'">
<xsl:call-template name="TemplateTHREEstylesheet"/>
<xsl:call-template name="Title_T3"/>
<table BORDER="0" CELLSPACING="0" CELLPADDING="4" WIDTH="100%" class="table2">
	<tr> 
		<td CLASS="colorlight" COLSPAN="2" valign="top"> 
			<table BORDER="0" CELLSPACING="0" CELLPADDING="4" WIDTH="100%">   
				<tr>
					<td  valign="top"><xsl:call-template name="Drip_data3"/></td>
				</tr>
			</table>
		</td>
	</tr>
	<xsl:call-template name="Drip_footer3"/>
</table> 
<xsl:call-template name="Copyright"/>
</xsl:when>
<!-- End Template 3 -->


<!-- Template Default -->
<xsl:otherwise>
<table BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" CLASS="colordark">
<xsl:call-template name="Title_S1"/>
   <tr ALIGN="CENTER">
     <td CLASS="colordark" colspan="2">  
 <table BORDER="0" CELLSPACING="0" CELLPADDING="4" WIDTH="100%">
   <tr ALIGN="CENTER"> 
     <td CLASS="colorlight" COLSPAN="2"> 
       <table BORDER="0" CELLSPACING="0" CELLPADDING="4" WIDTH="100%">   
<xsl:call-template name="Drip_data"/>         
       </table>
     </td>
   </tr>
</table> 
</td>
   </tr>
 <xsl:call-template name="Drip_footer"/>       
</table> 
<xsl:call-template name="Copyright"/>
</xsl:otherwise>
</xsl:choose>
</xsl:template>
</xsl:stylesheet>
