<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.ord/TR/REC-html40"
	xmlns:irw="http://www.snl.com/xml/irw">

  <xsl:output method="html" media-type="text/html" indent="no"/>
  
    <!-- Start Default Template here-->

  <!--
//This is the default template. Most changes are to either remove the company name, ticker, or exchange. you can also change the name of the page or remove this row all together.
//These templates below are broken up how the page is displayed.
 -->

  <xsl:template name="HeaderNews">
    <xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
    <tr>
      <td CLASS="colordark" nowrap="">
        <SPAN CLASS="title1light">
          <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          <xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/>
        </SPAN>
      </td>
      <td CLASS="colordark" nowrap="" align="right" valign="top">
        <span class="subtitlelight">
          <xsl:value-of select="$CompanyNameCaps"/> (<xsl:value-of select="Company/Exchange"/> - <xsl:value-of select="Company/Ticker"/>)
        </span>
        <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
      </td>
    </tr>
  </xsl:template>

  <xsl:template name="NewsHeader">
    <tr class="default" align="left">
      <td class="colorlight default" colspan="2">
        <span class="default">
          <xsl:value-of select="Company/Header" disable-output-escaping="yes"/>
        </span>
      </td>
    </tr>
  </xsl:template>

  <xsl:template name="NotifyText">
    <form name="MaskForm" id="MaskForm"  method="Post">
		<fieldset>
			<legend>Maskform</legend>
		
      <tr>
        <td class="colorlight default" align="left" colspan="2">
          <table border="0" cellspacing="0" cellpadding="3" width="auto">
            <tr class="data">
              <xsl:variable name="keyinstn" select="Company/KeyInstn" />
              <td class="data" nowrap="1">
                <BR/>
                <xsl:attribute name="action">
                  <xsl:value-of select="concat('YearlyNews.aspx?IID=', $keyinstn, '&amp;mode=1')"/>
                </xsl:attribute>
                <xsl:if test="PageParameters/SuppressTypeDisplay = 'false'">
                  <span class="defaultbold">Display: </span>
					<label class="visuallyhidden" for="TypeMask">Display</label>
                  <select name="TypeMask" id="TypeMask" onchange="document.getElementById('MaskForm').submit();">
                    <xsl:for-each select="AvailableTypes">
                      <option>
                        <xsl:attribute name="value">
                          <xsl:value-of select="TypeValue"/>
                        </xsl:attribute>
                        <xsl:if test="Selected != ''">
                          <xsl:attribute name="selected">
                            <xsl:value-of select="Selected"/>
                          </xsl:attribute>
                        </xsl:if>
                        <xsl:value-of select="TypeText"/>
                      </option>
                    </xsl:for-each>
                  </select>
                </xsl:if>
              </td>
              <xsl:if test="PageParameters/SuppressDateDisplay = 'false'">
                <xsl:if test="count(AvailableYears/Year) &gt; 0">
                  <td class="data" nowrap="1">
                    <BR/>
                    <xsl:attribute name="action">
                      <xsl:value-of select="concat('YearlyNews.aspx?IID=', $keyinstn, '&amp;mode=1')"/>
                    </xsl:attribute>
                    <span class="defaultbold">Year: </span>
					  <label class="visuallyhidden" for="YearMask">Year</label>
                    <select name="YearMask" id="YearMask" onchange="document.getElementById('MaskForm').submit();">
                      <xsl:for-each select="AvailableYears">
                        <option>
                          <xsl:attribute name="value">
                            <xsl:value-of select="Year"/>
                          </xsl:attribute>
                          <xsl:if test="Year = 'All Years'">
                            <xsl:attribute name="value">0</xsl:attribute>
                          </xsl:if>
                          <xsl:if test="Selected != ''">
                            <xsl:attribute name="selected">
                              <xsl:value-of select="Selected"/>
                            </xsl:attribute>
                          </xsl:if>
                          <xsl:value-of select="Year"/>
                        </option>
                      </xsl:for-each>
                    </select>
                  </td>
                </xsl:if>
              </xsl:if>
            </tr>
          </table>
        </td>
      </tr>
		</fieldset>
    </form>
    <xsl:if test="Company/ShowEmailNotifyLink = 'true'">
      <xsl:if test="normalize-space(RSSLinks/InfoLink)">
        <xsl:for-each select="CLIENTEMAILPREFS[NotificationType = '16']">
          <xsl:if test="NotificationEnabled = 'True'">
            <TR>
              <TD class="colorlight default" nowrap="1" colspan="2">
                <table border="0" cellspacing="0" cellpadding="3" width="100%">
                  <tr>
                    <td class="colorlight default" nowrap="1" valign="top">
                      <a>
                        <xsl:attribute name="href">
                          email.aspx?IID=<xsl:value-of select="../Company/KeyInstn"/>
                        </xsl:attribute>Notify me when new press releases are posted to this site
                      </a>.
                    </td>
                    <td class="colorlight default" nowrap="1" align="right" valign="top">
                      <a>
                        <xsl:attribute name="href">
                          <xsl:value-of select="../RSSLinks/FeedLink"/>
                        </xsl:attribute>
                        <img src="images/rss_xml.gif" border="0" alt="RSS xml"/>
                      </a>
                      <a>
                        <xsl:variable name="Keyinstn" select="../Company/KeyInstn" />
                        <xsl:variable name="RSSLinks" select="../RSSLinks/InfoLink" />
                        <xsl:variable name="RSSurl" select="concat($RSSLinks, '?iid=', $Keyinstn)" />
                        <xsl:attribute name="href">
                          <xsl:value-of select="$RSSurl"/>
                        </xsl:attribute>
                        <img src="images/rss_button.gif" border="0" alt="RSS button"/>
                      </a>
                    </td>
                  </tr>
                </table>
              </TD>
            </TR>
          </xsl:if>
          <xsl:if test="not(NotificationEnabled = 'True')">
            <TR>
              <TD class="colorlight default" nowrap="1" align="right" colspan="2">
                <a>
                  <xsl:attribute name="href">
                    <xsl:value-of select="../RSSLinks/FeedLink"/>
                  </xsl:attribute>
                  <img src="images/rss_xml.gif" border="0" alt="RSS xml"/>
                </a>
                <a>
                  <xsl:variable name="Keyinstn" select="../Company/KeyInstn" />
                  <xsl:variable name="RSSLinks" select="../RSSLinks/InfoLink" />
                  <xsl:variable name="RSSurl" select="concat($RSSLinks, '?iid=', $Keyinstn)" />
                  <xsl:attribute name="href">
                    <xsl:value-of select="$RSSurl"/>
                  </xsl:attribute>
                  <img src="images/rss_button.gif" border="0" alt="RSS button"/>
                </a>
              </TD>
            </TR>
          </xsl:if>
        </xsl:for-each>
      </xsl:if>
      <xsl:if test="not(normalize-space(RSSLinks/InfoLink))">
        <xsl:for-each select="CLIENTEMAILPREFS[NotificationType = '16']">
          <xsl:if test="NotificationEnabled = 'True'">
            <TR>
              <TD CLASS="colorlight default" nowrap="1" colspan="2">
                <a>
                  <xsl:attribute name="href">
                    email.aspx?IID=<xsl:value-of select="../Company/KeyInstn"/>
                  </xsl:attribute>Notify me when new press releases are posted to this site
                </a>.
              </TD>
            </TR>
          </xsl:if>
        </xsl:for-each>
      </xsl:if>
    </xsl:if>
    <xsl:if test="not(Company/ShowEmailNotifyLink = 'true') and normalize-space(RSSLinks/InfoLink)">
      <TR>
        <TD class="colorlight default" nowrap="1" align="right" colspan="2">
          <a>
            <xsl:attribute name="href">
              <xsl:value-of select="../RSSLinks/FeedLink"/>
            </xsl:attribute>
            <img src="images/rss_xml.gif" border="0" alt="RSS xml"/>
          </a>
          <a>
            <xsl:variable name="Keyinstn" select="../Company/KeyInstn" />
            <xsl:variable name="RSSLinks" select="../RSSLinks/InfoLink" />
            <xsl:variable name="RSSurl" select="concat($RSSLinks, '?iid=', $Keyinstn)" />
            <xsl:attribute name="href">
              <xsl:value-of select="$RSSurl"/>
            </xsl:attribute>
            <img src="images/rss_button.gif" border="0" alt="RSS button"/>
          </a>
        </TD>
      </TR>
    </xsl:if>
  </xsl:template>



  <xsl:template name="string-replace" >
    <xsl:param name="string"/>
    <xsl:param name="from"/>
    <xsl:param name="to"/>
    <xsl:choose>
      <xsl:when test="contains($string,$from)">
        <xsl:value-of select="substring-before($string,$from)"/>
        <xsl:value-of select="$to"/>
        <xsl:call-template name="string-replace">
          <xsl:with-param name="string" select="substring-after($string,$from)"/>
          <xsl:with-param name="from" select="$from"/>
          <xsl:with-param name="to" select="$to"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$string"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>



  <xsl:template name="MorePressReleases">
    <xsl:param name="bold"/>
    <xsl:param name="spanclass"/>
    <xsl:variable name="keyinstn" select="Company/KeyInstn" />
    <!--<xsl:variable name="transType" select="translate(AvailableTypes[Selected != '']/TypeValue, '&amp;', '%26')"></xsl:variable>-->

    <xsl:variable name="transType">
      <xsl:call-template name="string-replace">
        <xsl:with-param name="from" select="'&amp;'"/>
        <xsl:with-param name="to" select="'%26'"/>
        <xsl:with-param name="string" select="AvailableTypes[Selected != '']/TypeValue"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="linkGuts" select="concat($keyinstn, '&amp;mode=1&amp;type=', $transType)"/>
    <!--<xsl:variable name="linkGuts" select="concat($keyinstn, '&amp;mode=1&amp;type=', AvailableTypes[Selected != '']/TypeValue)"/>-->
    <xsl:variable name="linkGutsPlusYear">
      <xsl:if test="Company/YearAll = 'true'">
        <xsl:value-of select="$linkGuts"/>
      </xsl:if>
      <xsl:if test="Company/YearAll = 'false'">
        <xsl:value-of select="concat($linkGuts, '&amp;Year=', Company/Year)"/>
      </xsl:if>
    </xsl:variable>
    <xsl:variable name="pressurl" select="concat('News.aspx?IID=', $linkGutsPlusYear)" />
    <xsl:variable name="yearlypressurl" select="concat('YearlyNews.aspx?IID=', $linkGutsPlusYear)" />

    <xsl:if test="Company/KeyPage = 201855">
      <xsl:attribute name="href">
        <xsl:value-of select="$yearlypressurl" />
      </xsl:attribute>
    </xsl:if>

    <xsl:if test="Company/KeyPage = 175">
      <xsl:attribute name="href">
        <xsl:value-of select="$pressurl" />
      </xsl:attribute>
    </xsl:if>

    <xsl:if test="$bold='true'">
      <xsl:attribute name="class">boldfielddef</xsl:attribute>
    </xsl:if>

    <span class="{$spanclass}">More Press Releases...</span>
  </xsl:template>


  <xsl:template name="PressReleases">
    <xsl:variable name="PageTitle">
      <xsl:value-of select="/irw:IRW/*/MenuEntry/Description[/irw:IRW/*/Company/KeyPage = ../KeyPage and not(../MenuParent=0)]"/>
    </xsl:variable>
    <table border="0" cellspacing="0" cellpadding="4" width="100%" id="PRTable">
      <tr class="header">
        <td class="header" nowrap="nowrap">
          <xsl:choose>
            <xsl:when test="$PageTitle != ''">
              <xsl:value-of select="$PageTitle" disable-output-escaping="yes"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/>
            </xsl:otherwise>
          </xsl:choose>
        </td>
      </tr>
      <tr class="data">
        <td colspan="2" class="data">
          <table border="0" cellspacing="0" cellpadding="4" width="100%">
            <xsl:for-each select="PressRelease">
              <tr class="data">
                <td class="data" valign="top" width="15%">
                  <xsl:value-of select="Date" disable-output-escaping="yes"/>
                </td>
                <td class="data" valign="top" align="left">
                  <a class="PRLink" title="{Headline}">
                    <xsl:variable name="url" select="Link" />
                    <xsl:variable name="isPreview" select="../Company/IsPreview" />
                    <xsl:attribute name="target">_new</xsl:attribute>
                    <xsl:attribute name="href">
                      <xsl:value-of select="$url" />
                      <xsl:if test="contains($isPreview, 'true')">
                        <xsl:if test="contains($url, 'file.aspx')">&amp;preview=1</xsl:if>
                      </xsl:if>
                    </xsl:attribute>
                    <xsl:value-of select="Headline" disable-output-escaping="yes"/>
                  </a>
                  <xsl:if test="KeyFileFormat = 7 and SuppressPreviewDisplay = 'false'">
                    <img class="PRPreviewLink" src="images/mini-mag.gif" />
                  </xsl:if>
                </td>
              </tr>
            </xsl:for-each>
          </table>
        </td>
      </tr>
      <xsl:if test="Company/DisplayMode=0" >
        <tr>
          <td align="right">
            <a>
              <xsl:call-template name="MorePressReleases">
                <xsl:with-param name="bold" select="'true'"/>
              </xsl:call-template>
              <!--<xsl:attribute name="class">boldfielddef</xsl:attribute>-->
              <!--1 More Press Releases...-->

              <!--<xsl:variable name="keyinstn" select="Company/KeyInstn" />
              <xsl:variable name="linkGuts" select="concat($keyinstn, '&amp;mode=1&amp;type=', AvailableTypes[Selected != '']/TypeValue)"/>
              <xsl:variable name="linkGutsPlusYear">
                <xsl:if test="Company/YearAll = 'true'">
                  <xsl:value-of select="$linkGuts"/>
                </xsl:if>
                <xsl:if test="Company/YearAll = 'false'">
                  <xsl:value-of select="concat($linkGuts, '&amp;Year=', Company/Year)"/>
                </xsl:if>
              </xsl:variable>
              <xsl:variable name="pressurl" select="concat('News.aspx?IID=', $linkGutsPlusYear)" />
              <xsl:variable name="yearlypressurl" select="concat('YearlyNews.aspx?IID=', $linkGutsPlusYear)" />-->

              <!--begin - should remain commented out-->
              <!--<xsl:variable name="pressurl" select="concat('News.aspx?IID=', $keyinstn, '&amp;mode=1&amp;type=', AvailableTypes[Selected != '']/TypeValue)" />-->

              <!--<xsl:variable name="yearlypressurl">
                <xsl:if test="Company/YearAll = 'true'">
                  <xsl:value-of select="concat('YearlyNews.aspx?IID=', $keyinstn, '&amp;mode=1&amp;type=', AvailableTypes[Selected != '']/TypeValue)" />
                </xsl:if>
                <xsl:if test="Company/YearAll = 'false'">
                  <xsl:value-of select="concat('YearlyNews.aspx?IID=', $keyinstn, '&amp;mode=1&amp;Year=', Company/Year, '&amp;type=', AvailableTypes[Selected != '']/TypeValue)" />
                </xsl:if>
              </xsl:variable>-->
              <!--<xsl:variable name="yearlypressurl">
                <xsl:variable name="tempLink" select="concat('YearlyNews.aspx?IID=', $keyinstn, '&amp;mode=1&amp;type=', AvailableTypes[Selected != '']/TypeValue)"/>
                <xsl:if test="Company/YearAll = 'true'">
                  <xsl:value-of select="$tempLink"/>
                </xsl:if>
                <xsl:if test="Company/YearAll = 'false'">
                  <xsl:value-of select="concat($tempLink, '&amp;Year=', Company/Year)"/>
                </xsl:if>
              </xsl:variable>-->
              <!--end - should remain commented out-->

              <!--<xsl:attribute name="class">boldfielddef</xsl:attribute>

              -->
              <!--<xsl:if test="normalize-space(Company/Year)">-->
              <!--
              <xsl:if test="Company/KeyPage = 201855">
                <xsl:attribute name="href">
                  <xsl:value-of select="$yearlypressurl" />
                </xsl:attribute>
              </xsl:if>

              -->
              <!--<xsl:if test="not(normalize-space(Company/Year))">-->
              <!--
              <xsl:if test="Company/KeyPage = 175">
                <xsl:attribute name="href">
                  <xsl:value-of select="$pressurl" />
                </xsl:attribute>
              </xsl:if>
              More Press Releases...-->
            </a>
          </td>
        </tr>
      </xsl:if>
    </table>
  </xsl:template>




  <xsl:template name="SourceTag">
    <xsl:choose>
      <xsl:when test="Company/TotalNewsArticles &gt; Company/RowsPerPage or Company/TotalPressReleases &gt; Company/RowsPerPage">
        <xsl:variable name="EndPage" select="Company/Start + Company/RowsPerPage - 1"/>
        <tr class="colorlight default">
          <td class="colorlight default">
            <span class="default">
              &#160;<i>Source: S&amp;P Global Market Intelligence</i>
            </span>
          </td>
          <td class="colorlight default" valign="top">&#160;</td>
        </tr>
        <tr VALIGN="TOP">
          <td class="colorlight default defaultbold">
            <xsl:if test="Company/DisplayMode = 1">
              Rows <xsl:value-of select="Company/Start"/> through
              <xsl:choose>
                <xsl:when test="$EndPage &gt; Company/TotalPressReleases">
                  <xsl:value-of select="Company/TotalPressReleases"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="$EndPage"/>
                </xsl:otherwise>
              </xsl:choose>
              of <xsl:value-of select="Company/TotalPressReleases"/>
            </xsl:if>

            <xsl:if test="Company/DisplayMode = 2">
              Rows <xsl:value-of select="Company/Start"/> through
              <xsl:choose>
                <xsl:when test="$EndPage &gt; Company/TotalNewsArticles">
                  <xsl:value-of select="Company/TotalNewsArticles"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="$EndPage"/>
                </xsl:otherwise>
              </xsl:choose>
              of <xsl:value-of select="Company/TotalNewsArticles"/>
            </xsl:if>
          </td>
          <td class="colorlight default defaultbold" align="right">
            <xsl:for-each select="Pagination">
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              <xsl:choose>
                <xsl:when test="CurrentPage = 'false'">
                  <a target="_self" title="{PageLabel}">
                    <xsl:attribute name="href">
                      <xsl:value-of select="PageLink"/>
                    </xsl:attribute>
                    <xsl:value-of select="PageLabel"/>
                  </a>
                </xsl:when>
                <xsl:otherwise>
                  <span class="defaultbold">
                    <xsl:value-of select="PageLabel"/>
                  </span>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:for-each>
          </td>
        </tr>
      </xsl:when>
      <xsl:otherwise>
        <tr class="colorlight default">
          <td class="colorlight default">
            <span class="default">
              &#160;<i>Source: S&amp;P Global Market Intelligence</i>
            </span>
          </td>
          <td class="colorlight default" valign="top">&#160;</td>
        </tr>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="FooterTag">
    <xsl:if test="Company/Footer != ''">
      <tr class="colorlight default">
        <td colspan="2" class="colorlight default">
          <span class="default">
            <xsl:value-of select="Company/Footer" disable-output-escaping="yes"/>
          </span>
        </td>
      </tr>
    </xsl:if>
  </xsl:template>

  <!-- Default Template Ends here. all templates below are ONLY if you are using the 'Style Template' section in the console. -->

  <!-- This is Template ONE option in the console under 'Style Template'
//This template is a striped down version of the main template. Everything (data) is displayed in single rows.
//The sections will be commented on where the rows are. you should be able to just move rows (entire chunks of data) where you need too. Edits to this section are rare, mostly due to size (width of the page).
-->

  <xsl:template name="HeaderNews2">
    <xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
    <tr>
      <td class=" titletest" nowrap="nowrap" valign="top">
        <xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/>
        <br />
        <IMG SRC="" WIDTH="2" HEIGHT="1" alt="" />
        <span class=" titletest2">
          <xsl:value-of select="$CompanyNameCaps"/>&#160;(<xsl:value-of select="Company/Exchange"/>&#160;-&#160;<xsl:value-of select="Company/Ticker"/>)
        </span>
        <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <table class="" BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%" >
          <xsl:call-template name="NotifyText2"/>
        </table>
      </td>
    </tr>
    <tr class="default" align="left">
      <td class="colorlight default" colspan="2">
        <span class="default">
          <xsl:value-of select="Company/Header" disable-output-escaping="yes" />
        </span>
      </td>
    </tr>
  </xsl:template>

  <xsl:template name="NotifyText2">
    <xsl:if test="Company/ShowEmailNotifyLink = 'true'">
      <xsl:if test="normalize-space(RSSLinks/InfoLink)">
        <xsl:for-each select="CLIENTEMAILPREFS[NotificationType = '16']">
          <xsl:if test="NotificationEnabled = 'True'">
            <TR>
              <TD COLSPAN="2" CLASS="default" nowrap="1">
                <table width="100%" border="0">
                  <tr>
                    <td class="default" nowrap="1" valign="top">
                      <a>
                        <xsl:attribute name="href">
                          email.aspx?IID=<xsl:value-of select="../Company/KeyInstn"/>
                        </xsl:attribute>Notify me when new press releases are posted to this site
                      </a>.
                    </td>
                    <td class="default" nowrap="1" align="right" valign="top">
                      <a>
                        <xsl:attribute name="href">
                          <xsl:value-of select="../RSSLinks/FeedLink"/>
                        </xsl:attribute>
                        <img src="images/rss_xml.gif" border="0"  alt="RSS xml"/>
                      </a>
                      <a>
                        <xsl:variable name="Keyinstn" select="../Company/KeyInstn" />
                        <xsl:variable name="RSSLinks" select="../RSSLinks/InfoLink" />
                        <xsl:variable name="RSSurl" select="concat($RSSLinks, '?iid=', $Keyinstn)" />
                        <xsl:attribute name="href">
                          <xsl:value-of select="$RSSurl"/>
                        </xsl:attribute>
                        <img src="images/rss_button.gif" border="0"  alt="RSS botton"/>
                      </a>
                    </td>
                  </tr>
                </table>
              </TD>
            </TR>
          </xsl:if>
          <xsl:if test="not(NotificationEnabled = 'True')">
            <TR>
              <TD COLSPAN="4" class="default" nowrap="1" align="right">
                <a>
                  <xsl:attribute name="href">
                    <xsl:value-of select="../RSSLinks/FeedLink"/>
                  </xsl:attribute>
                  <img src="images/rss_xml.gif" border="0"  alt="RSS xml"/>
                </a>
                <a>
                  <xsl:variable name="Keyinstn" select="../Company/KeyInstn" />
                  <xsl:variable name="RSSLinks" select="../RSSLinks/InfoLink" />
                  <xsl:variable name="RSSurl" select="concat($RSSLinks, '?iid=', $Keyinstn)" />
                  <xsl:attribute name="href">
                    <xsl:value-of select="$RSSurl"/>
                  </xsl:attribute>
                  <img src="images/rss_button.gif" border="0"  alt="RSS button"/>
                </a>
              </TD>
            </TR>
          </xsl:if>
        </xsl:for-each>
      </xsl:if>
      <xsl:if test="not(normalize-space(RSSLinks/InfoLink))">
        <xsl:for-each select="CLIENTEMAILPREFS[NotificationType = '16']">
          <xsl:if test="NotificationEnabled = 'True'">
            <TR>
              <TD COLSPAN="4" CLASS="default" nowrap="1">
                <a>
                  <xsl:attribute name="href">
                    email.aspx?IID=<xsl:value-of select="../Company/KeyInstn"/>
                  </xsl:attribute>Notify me when new press releases are posted to this site
                </a>.
              </TD>
            </TR>
          </xsl:if>
        </xsl:for-each>
      </xsl:if>
    </xsl:if>
    <xsl:if test="not(Company/ShowEmailNotifyLink = 'true') and normalize-space(RSSLinks/InfoLink)">
      <TR>
        <TD COLSPAN="4" class="default" nowrap="1" align="right">
          <a>
            <xsl:attribute name="href">
              <xsl:value-of select="../RSSLinks/FeedLink"/>
            </xsl:attribute>
            <img src="images/rss_xml.gif" border="0"  alt="RSS xml"/>
          </a>
          <a>
            <xsl:variable name="Keyinstn" select="../Company/KeyInstn" />
            <xsl:variable name="RSSLinks" select="../RSSLinks/InfoLink" />
            <xsl:variable name="RSSurl" select="concat($RSSLinks, '?iid=', $Keyinstn)" />
            <xsl:attribute name="href">
              <xsl:value-of select="$RSSurl"/>
            </xsl:attribute>
            <img src="images/rss_button.gif" border="0"  alt="RSS button"/>
          </a>
        </TD>
      </TR>
    </xsl:if>
  </xsl:template>

  <xsl:template name="Year_Header2">
    <form name="MaskForm" id="MaskForm"  method="Post">
		<fieldset>
			<legend>Maskform</legend>
			<tr>
				<td class="data" align="left">
					<table border="0" cellspacing="0" cellpadding="3" width="100">
						<tr class="data">
							<xsl:variable name="keyinstn" select="Company/KeyInstn" />
							<td class="data" nowrap="1">
								<BR/>
								<xsl:attribute name="action">
									<xsl:value-of select="concat('YearlyNews.aspx?IID=', $keyinstn, '&amp;mode=1')"/>
								</xsl:attribute>
								<xsl:if test="PageParameters/SuppressTypeDisplay = 'false'">
									<span class="defaultbold">Display: </span>
									<label class="visuallyhidden" for="TypeMask">Display</label>
									<select name="TypeMask" id="TypeMask" onchange="document.getElementById('MaskForm').submit();">
										<xsl:for-each select="AvailableTypes">
											<option>
												<xsl:attribute name="value">
													<xsl:value-of select="TypeValue"/>
												</xsl:attribute>
												<xsl:if test="Selected != ''">
													<xsl:attribute name="selected">
														<xsl:value-of select="Selected"/>
													</xsl:attribute>
												</xsl:if>
												<xsl:value-of select="TypeText"/>
											</option>
										</xsl:for-each>
									</select>
								</xsl:if>
							</td>
							<xsl:if test="PageParameters/SuppressDateDisplay = 'false'">
								<xsl:if test="count(AvailableYears/Year) &gt; 0">
									<td class="data" nowrap="1">
										<BR/>
										<xsl:attribute name="action">
											<xsl:value-of select="concat('YearlyNews.aspx?IID=', $keyinstn, '&amp;mode=1')"/>
										</xsl:attribute>
										<span class="defaultbold">Year: </span>
										<label class="visuallyhidden" for="YearMask">YearMask</label>
										<select name="YearMask" id="YearMask" onchange="document.getElementById('MaskForm').submit();">
											<xsl:for-each select="AvailableYears">
												<option>
													<xsl:attribute name="value">
														<xsl:value-of select="Year"/>
													</xsl:attribute>
													<xsl:if test="Year = 'All Years'">
														<xsl:attribute name="value">0</xsl:attribute>
													</xsl:if>
													<xsl:if test="Selected != ''">
														<xsl:attribute name="selected">
															<xsl:value-of select="Selected"/>
														</xsl:attribute>
													</xsl:if>
													<xsl:value-of select="Year"/>
												</option>
											</xsl:for-each>
										</select>
									</td>
								</xsl:if>
							</xsl:if>
						</tr>
					</table>
				</td>
			</tr>
		</fieldset>
    </form>
  </xsl:template>

  <xsl:template name="PressReleases2">
    <table border="0" cellspacing="0" cellpadding="0" width="100%">
      <xsl:variable name="PageTitle">
        <xsl:value-of select="/irw:IRW/*/MenuEntry/Description[/irw:IRW/*/Company/KeyPage = ../KeyPage and not(../MenuParent=0)]"/>
      </xsl:variable>
      <xsl:choose>
        <xsl:when test="Company/TotalPressReleases=0">
        </xsl:when>
        <xsl:otherwise>
          <xsl:if test="Company/DisplayMode=1 or Company/DisplayMode=0 or string-length(Company/DisplayMode)=0">
            <tr class="data">
              <td class="data" colspan="2">
                <table border="0" cellspacing="0" cellpadding="4" width="100%">
                  <xsl:call-template name="NotifyText2"/>
                  <tr>
                    <td class="data" nowrap="nowrap">
                      <span class=" titletest2">
                        <xsl:choose>
                          <xsl:when test="$PageTitle != ''">
                            <xsl:value-of select="$PageTitle" disable-output-escaping="yes"/>
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/>
                          </xsl:otherwise>
                        </xsl:choose>
                      </span>
                    </td>
                    <xsl:choose>
                      <xsl:when  test="Company/DisplayMode=0" >
                        <td class="data"  align="right" nowrap="nowrap">
                          <a>
                            <!--<xsl:variable name="keyinstn" select="Company/KeyInstn" />
                            <xsl:variable name="linkGuts" select="concat($keyinstn, '&amp;mode=1&amp;type=', AvailableTypes[Selected != '']/TypeValue)"/>
                            <xsl:variable name="linkGutsPlusYear">
                              <xsl:if test="Company/YearAll = 'true'">
                                <xsl:value-of select="$linkGuts"/>
                              </xsl:if>
                              <xsl:if test="Company/YearAll = 'false'">
                                <xsl:value-of select="concat($linkGuts, '&amp;Year=', Company/Year)"/>
                              </xsl:if>
                            </xsl:variable>
                            <xsl:variable name="pressurl" select="concat('News.aspx?IID=', $linkGutsPlusYear)" />
                            <xsl:variable name="yearlypressurl" select="concat('YearlyNews.aspx?IID=', $linkGutsPlusYear)" />-->

                            <!--<xsl:variable name="pressurl" select="concat('News.aspx?IID=', $keyinstn, '&amp;mode=1&amp;type=', AvailableTypes[Selected != '']/TypeValue)" />
                            -->
                            <!--<xsl:variable name="yearlypressurl" select="concat('YearlyNews.aspx?IID=', $keyinstn, '&amp;mode=1&amp;Year=', Company/Year, '&amp;type=', AvailableTypes[Selected != '']/TypeValue)" />-->
                            <!--
                            <xsl:variable name="yearlypressurl">
                              <xsl:variable name="tempLink" select="concat('YearlyNews.aspx?IID=', $keyinstn, '&amp;mode=1&amp;type=', AvailableTypes[Selected != '']/TypeValue)"/>
                              <xsl:if test="Company/YearAll = 'true'">
                                <xsl:value-of select="$tempLink"/>
                              </xsl:if>
                              <xsl:if test="Company/YearAll = 'false'">
                                <xsl:value-of select="concat($tempLink, '&amp;Year=', Company/Year)"/>
                              </xsl:if>
                            </xsl:variable>-->

                            <!--<xsl:if test="normalize-space(Company/Year)">-->
                            <!--
                            <xsl:if test="Company/KeyPage = 201855">
                                <xsl:attribute name="href">
                                <xsl:value-of select="$yearlypressurl" />
                              </xsl:attribute>
                            </xsl:if>
                            -->
                            <!--<xsl:if test="not(normalize-space(Company/Year))">-->
                            <!--
                            <xsl:if test="Company/KeyPage = 175">
                              <xsl:attribute name="href">
                                <xsl:value-of select="$pressurl" />
                              </xsl:attribute>
                            </xsl:if>-->

                            <xsl:call-template name="MorePressReleases">
                              <xsl:with-param name="spanclass" select="' titletest2'"/>
                            </xsl:call-template>
                            <!--<span class=" titletest2">More Press Releases...</span>-->
                          </a>
                        </td>
                      </xsl:when>
                      <xsl:otherwise>
                        <td class="data" >&#160;</td>
                      </xsl:otherwise>
                    </xsl:choose>
                  </tr>
                  <tr class="data">
                    <td colspan="2" class="data">



                      <table border="0" cellspacing="0" cellpadding="4" width="100%" id="PRTable">
                        <tr>
                          <td class="surrleft datashade" valign="top" width="70">
                            <span class="defaultbold">Date</span>
                          </td>
                          <td class="surr_topbot datashade" valign="top" >
                            <span class="defaultbold">Headline</span>
                          </td>
                          <td class="surrright datashade" valign="top"  align="center">
                            <span class="defaultbold">Format</span>
                          </td>
                        </tr>
                        <xsl:for-each select="PressRelease">
                          <tr class="data">
                            <td class="data" valign="top">
                              <xsl:value-of select="Date" disable-output-escaping="yes"/>
                            </td>
                            <td class="data" valign="top" align="left">
                              <a class="PRLink" title="{Headline}">
                                <xsl:variable name="url" select="Link" />
                                <xsl:variable name="isPreview" select="../Company/IsPreview" />
                                <xsl:attribute name="target">_new</xsl:attribute>
                                <xsl:attribute name="href">
                                  <xsl:value-of select="$url" />
                                  <xsl:if test="contains($isPreview, 'true')">
                                    <xsl:if test="contains($url, 'file.aspx')">&amp;preview=1</xsl:if>
                                  </xsl:if>
                                </xsl:attribute>
                                <xsl:value-of select="Headline" disable-output-escaping="yes"/>
                              </a>
                              <xsl:if test="KeyFileFormat = 7 and SuppressPreviewDisplay = 'false'">
                                <img class="PRPreviewLink" src="images/mini-mag.gif" />
                              </xsl:if>
                            </td>                            
                              <TD CLASS="data" ALIGN="center">
                                <A TARGET="_new" title="View File">
                                    <xsl:variable name="url" select="Link" />
                                    <xsl:variable name="isPreview" select="../Company/IsPreview" />							
                                    <xsl:attribute name="href"><xsl:value-of select="$url" /><xsl:if test="contains($isPreview, 'true')"><xsl:if test="contains($url, 'file.aspx')">&amp;preview=1</xsl:if></xsl:if></xsl:attribute>
                                  <IMG  BORDER="0" alt="">
                                    <xsl:attribute name="SRC">
                                      <xsl:value-of select="IconImage" />
                                    </xsl:attribute>
                                  </IMG>
                                  <BR />
                                  <xsl:value-of select="IconFormat"/>
                                </A>
                              </TD>
                          </tr>
                        </xsl:for-each>
                        <xsl:if test="PressRelease/IconLink != ''">
                          <tr>
                            <td colspan="3">
                              Click the link to the right to download:
                              <A TARGET="_new" title="Download Adobe Acrobat">
                                <xsl:attribute name="HREF">http://get.adobe.com/reader/</xsl:attribute>
                                <xsl:value-of select="IconFormat"/>Adobe Acrobat
                              </A>
                            </td>
                          </tr>
                        </xsl:if>
                      </table>



                    </td>
                  </tr>

                </table>
              </td>
            </tr>
          </xsl:if>
        </xsl:otherwise>
      </xsl:choose>
    </table>
  </xsl:template>






  <xsl:template name="SourceTag2">
    <table>
      <xsl:choose>
        <xsl:when test="Company/TotalNewsArticles &gt; Company/RowsPerPage or Company/TotalPressReleases &gt; Company/RowsPerPage">
          <xsl:variable name="EndPage" select="Company/Start + Company/RowsPerPage - 1"/>
          <tr class="data">
            <td  class="data" colspan="2">
              <span class="default">
                &#160;<i>Source: S&amp;P Global Market Intelligence</i>
              </span>
            </td>
          </tr>
          <tr VALIGN="TOP" class="data">

            <xsl:if test="Company/DisplayMode = 1">
              <td class="data defaultbold">
                Rows <xsl:value-of select="Company/Start"/> through
                <xsl:choose>
                  <xsl:when test="$EndPage &gt; Company/TotalPressReleases">                    
                      <xsl:value-of select="Company/TotalPressReleases"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="$EndPage"/>
                  </xsl:otherwise>
                </xsl:choose>
                of <xsl:value-of select="Company/TotalPressReleases"/>
              </td>
            </xsl:if>

            <xsl:if test="Company/DisplayMode = 2">
              <td class="data defaultbold">
                Rows <xsl:value-of select="Company/Start"/> through
                <xsl:choose>
                  <xsl:when test="$EndPage &gt; Company/TotalNewsArticles">
                      <xsl:value-of select="Company/TotalNewsArticles"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="$EndPage"/>
                  </xsl:otherwise>
                </xsl:choose>
                of <xsl:value-of select="Company/TotalNewsArticles"/>
              </td>
            </xsl:if>

            <td class="data" ALIGN="RIGHT">
              <xsl:for-each select="Pagination">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                <xsl:choose>
                  <xsl:when test="CurrentPage = 'false'">
                    <a target="_self" title="{PageLabel}">
                      <xsl:attribute name="href">
                        <xsl:value-of select="PageLink"/>
                      </xsl:attribute>
                      <span class="i">
                        <xsl:value-of select="PageLabel"/>
                      </span>
                    </a>
                  </xsl:when>
                  <xsl:otherwise>
                    <span class="defaultbold">
                      <xsl:value-of select="PageLabel"/>
                    </span>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:for-each>
            </td>
          </tr>
        </xsl:when>
        <xsl:otherwise>
          <tr class="data">
            <td  class="data" colspan="2">
              <span class="default">
                &#160;<i>Source: S&amp;P Global Market Intelligence</i>
              </span>
            </td>
          </tr>
        </xsl:otherwise>
      </xsl:choose>

    </table>
  </xsl:template>





  <xsl:template name="FooterTag2">
    <xsl:if test="Company/Footer != ''">
      <tr>
        <td class="data" colspan="2">
          <span class="default">
            <xsl:value-of select="Company/Footer" disable-output-escaping="yes"/>
          </span>
        </td>
      </tr>
    </xsl:if>
  </xsl:template>
  <!-- Template ONE Ends here. -->








  <!-- This is Template 3 -->

  <xsl:template name="NotifyText3">

    <xsl:if test="Company/ShowEmailNotifyLink = 'true'">
      <xsl:if test="normalize-space(RSSLinks/InfoLink)">
        <xsl:for-each select="CLIENTEMAILPREFS[NotificationType = '16']">
          <xsl:if test="NotificationEnabled = 'True'">
            <TR>
              <TD COLSPAN="2" CLASS="default" nowrap="1">
                <table width="100%" border="0">
                  <tr>
                    <td class="default" nowrap="1" valign="top">
                      <a>
                        <xsl:attribute name="href">
                          email.aspx?IID=<xsl:value-of select="../Company/KeyInstn"/>
                        </xsl:attribute>
                        Notify me when new press releases are posted to this site
                      </a>.
                    </td>
                    <td class="default" nowrap="1" align="right" valign="top">
                      <a>
                        <xsl:attribute name="href">
                          <xsl:value-of select="../RSSLinks/FeedLink" disable-output-escaping="yes"/>
                        </xsl:attribute>
                        <img src="images/rss_xml.gif" border="0"  alt="RSS xml"/>
                      </a>

                      <a>
                        <xsl:variable name="Keyinstn" select="../Company/KeyInstn" />
                        <xsl:variable name="RSSLinks" select="../RSSLinks/InfoLink" />
                        <xsl:variable name="RSSurl" select="concat($RSSLinks, '?iid=', $Keyinstn)" />
                        <xsl:attribute name="href">
                          <xsl:value-of select="$RSSurl"/>
                        </xsl:attribute>
                        <img src="images/rss_button.gif" border="0"  alt="RSS button"/>
                      </a>
                    </td>
                  </tr>
                </table>
              </TD>
            </TR>
          </xsl:if>
          <xsl:if test="not(NotificationEnabled = 'True')">
            <TR>
              <TD COLSPAN="4" class="default" nowrap="1" align="right">
                <a>
                  <xsl:attribute name="href">
                    <xsl:value-of select="../RSSLinks/FeedLink"/>
                  </xsl:attribute>
                  <img src="images/rss_xml.gif" border="0"  alt="RSS xml"/>
                </a>

                <a>
                  <xsl:variable name="Keyinstn" select="../Company/KeyInstn" />
                  <xsl:variable name="RSSLinks" select="../RSSLinks/InfoLink" />
                  <xsl:variable name="RSSurl" select="concat($RSSLinks, '?iid=', $Keyinstn)" />
                  <xsl:attribute name="href">
                    <xsl:value-of select="$RSSurl"/>
                  </xsl:attribute>
                  <img src="images/rss_button.gif" border="0"  alt="RSS button"/>
                </a>
              </TD>
            </TR>
          </xsl:if>
        </xsl:for-each>
      </xsl:if>

      <xsl:if test="not(normalize-space(RSSLinks/InfoLink))">
        <xsl:for-each select="CLIENTEMAILPREFS[NotificationType = '16']">
          <xsl:if test="NotificationEnabled = 'True'">
            <TR>
              <TD CLASS="default" nowrap="1">
                <a>
                  <xsl:attribute name="href">
                    email.aspx?IID=<xsl:value-of select="../Company/KeyInstn"/>
                  </xsl:attribute>
                  Notify me when new press releases are posted to this site
                </a>.
              </TD>
            </TR>
          </xsl:if>
        </xsl:for-each>
      </xsl:if>
    </xsl:if>
    <xsl:if test="not(Company/ShowEmailNotifyLink = 'true') and normalize-space(RSSLinks/InfoLink)">
      <TR>
        <TD class="default" nowrap="1" align="right">
          <a>
            <xsl:attribute name="href">
              <xsl:value-of select="../RSSLinks/FeedLink" disable-output-escaping="yes"/>
            </xsl:attribute>
            <img src="images/rss_xml.gif" border="0"  alt="RSS xml"/>
          </a>

          <a>
            <xsl:variable name="Keyinstn" select="../Company/KeyInstn" />
            <xsl:variable name="RSSLinks" select="../RSSLinks/InfoLink" />
            <xsl:variable name="RSSurl" select="concat($RSSLinks, '?iid=', $Keyinstn)" />
            <xsl:attribute name="href">
              <xsl:value-of select="$RSSurl"/>
            </xsl:attribute>
            <img src="images/rss_button.gif" border="0"  alt="RSS button"/>
          </a>
        </TD>
      </TR>
    </xsl:if>

  </xsl:template>

  <xsl:template name="Year_Header3">
    <form name="MaskForm" id="MaskForm"  method="Post">
		<fieldset>
			<legend>Maskform</legend>
			<tr>
				<td class="data" align="left">
					<table border="0" cellspacing="0" cellpadding="3" width="100">
						<tr class="data">
							<xsl:variable name="keyinstn" select="Company/KeyInstn" />
							<td class="data" nowrap="1">
								<BR/>
								<xsl:attribute name="action">
									<xsl:value-of select="concat('YearlyNews.aspx?IID=', $keyinstn, '&amp;mode=1')"/>
								</xsl:attribute>
								<xsl:if test="PageParameters/SuppressTypeDisplay = 'false'">
									<span class="defaultbold">Display: </span>
									<label class="visuallyhidden" for="TypeMask">Display</label>
									<select name="TypeMask" id="TypeMask" onchange="document.getElementById('MaskForm').submit();">
										<xsl:for-each select="AvailableTypes">
											<option>
												<xsl:attribute name="value">
													<xsl:value-of select="TypeValue"/>
												</xsl:attribute>
												<xsl:if test="Selected != ''">
													<xsl:attribute name="selected">
														<xsl:value-of select="Selected"/>
													</xsl:attribute>
												</xsl:if>
												<xsl:value-of select="TypeText"/>
											</option>
										</xsl:for-each>
									</select>
								</xsl:if>
							</td>
							<xsl:if test="PageParameters/SuppressDateDisplay = 'false'">
								<xsl:if test="count(AvailableYears/Year) &gt; 0">
									<td class="data" nowrap="1">
										<BR/>
										<xsl:attribute name="action">
											<xsl:value-of select="concat('YearlyNews.aspx?IID=', $keyinstn, '&amp;mode=1')"/>
										</xsl:attribute>
										<span class="defaultbold">Year: </span>
										<label class="visuallyhidden" for="YearMask">Year</label>
										<select name="YearMask" id="YearMask" onchange="document.getElementById('MaskForm').submit();">
											<xsl:for-each select="AvailableYears">
												<option>
													<xsl:attribute name="value">
														<xsl:value-of select="Year"/>
													</xsl:attribute>
													<xsl:if test="Year = 'All Years'">
														<xsl:attribute name="value">0</xsl:attribute>
													</xsl:if>
													<xsl:if test="Selected != ''">
														<xsl:attribute name="selected">
															<xsl:value-of select="Selected"/>
														</xsl:attribute>
													</xsl:if>
													<xsl:value-of select="Year"/>
												</option>
											</xsl:for-each>
										</select>
									</td>
								</xsl:if>
							</xsl:if>
						</tr>
					</table>
				</td>
			</tr>
		</fieldset>
    </form>
  </xsl:template>

  <xsl:template name="PressReleases3">
    <xsl:variable name="PageTitle">
      <xsl:value-of select="/irw:IRW/*/MenuEntry/Description[/irw:IRW/*/Company/KeyPage = ../KeyPage and not(../MenuParent=0)]"/>
    </xsl:variable>
    <table border="0" cellspacing="0" cellpadding="0" width="100%">
      <xsl:choose>
        <xsl:when test="Company/TotalPressReleases=0">
          <tr>
            <td class="data" valign="top">&#160;</td>
          </tr>
        </xsl:when>
        <xsl:otherwise>
          <xsl:if test="Company/DisplayMode=1 or Company/DisplayMode=0 or string-length(Company/DisplayMode)=0">
            <tr>
              <td class="data" valign="top">
                <table border="0" cellspacing="0" cellpadding="4" width="100%">
                  <xsl:call-template name="NotifyText3"/>
                  <tr>
                    <td class="data" valign="top">
                      <table border="0" cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                          <td nowrap="nowrap" class="data" valign="top">
                            <span class="title2 titletest2">
                              <xsl:choose>
                                <xsl:when test="$PageTitle != ''">
                                  <xsl:value-of select="$PageTitle" disable-output-escaping="yes"/>
                                </xsl:when>
                                <xsl:otherwise>
                                  <xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/>
                                </xsl:otherwise>
                              </xsl:choose>
                            </span>
                          </td>
                          <xsl:choose>
                            <xsl:when  test="Company/DisplayMode=0" >
                              <td align="right" nowrap="nowrap" class="data">
                                <a>
                                  <!--<xsl:variable name="keyinstn" select="Company/KeyInstn" />
                                  <xsl:variable name="linkGuts" select="concat($keyinstn, '&amp;mode=1&amp;type=', AvailableTypes[Selected != '']/TypeValue)"/>
                                  <xsl:variable name="linkGutsPlusYear">
                                    <xsl:if test="Company/YearAll = 'true'">
                                      <xsl:value-of select="$linkGuts"/>
                                    </xsl:if>
                                    <xsl:if test="Company/YearAll = 'false'">
                                      <xsl:value-of select="concat($linkGuts, '&amp;Year=', Company/Year)"/>
                                    </xsl:if>
                                  </xsl:variable>
                                  <xsl:variable name="pressurl" select="concat('News.aspx?IID=', $linkGutsPlusYear)" />
                                  <xsl:variable name="yearlypressurl" select="concat('YearlyNews.aspx?IID=', $linkGutsPlusYear)" />-->

                                  <!--<xsl:variable name="pressurl" select="concat('News.aspx?IID=', $keyinstn, '&amp;mode=1&amp;type=', AvailableTypes[Selected != '']/TypeValue)" />
                                  -->
                                  <!--<xsl:variable name="yearlypressurl" select="concat('YearlyNews.aspx?IID=', $keyinstn, '&amp;mode=1&amp;Year=', Company/Year, '&amp;type=', AvailableTypes[Selected != '']/TypeValue)" />-->
                                  <!--
                                  <xsl:variable name="yearlypressurl">
                                    <xsl:variable name="tempLink" select="concat('YearlyNews.aspx?IID=', $keyinstn, '&amp;mode=1&amp;type=', AvailableTypes[Selected != '']/TypeValue)"/>
                                    <xsl:if test="Company/YearAll = 'true'">
                                      <xsl:value-of select="$tempLink"/>
                                    </xsl:if>
                                    <xsl:if test="Company/YearAll = 'false'">
                                      <xsl:value-of select="concat($tempLink, '&amp;Year=', Company/Year)"/>
                                    </xsl:if>
                                  </xsl:variable>-->

                                  <!--<xsl:if test="normalize-space(Company/Year)">-->
                                  <!--
                                  <xsl:if test="Company/KeyPage = 201855">
                                    <xsl:attribute name="href">
                                      <xsl:value-of select="$yearlypressurl" />
                                    </xsl:attribute>
                                  </xsl:if>
                                  -->
                                  <!--<xsl:if test="not(normalize-space(Company/Year))">-->
                                  <!--
                                  <xsl:if test="Company/KeyPage = 175">
                                    <xsl:attribute name="href">
                                      <xsl:value-of select="$pressurl" />
                                    </xsl:attribute>
                                  </xsl:if>-->

                                  <xsl:call-template name="MorePressReleases">
                                    <xsl:with-param name="spanclass" select="'defaultbold'"/>
                                  </xsl:call-template>
                                  <!--<span class="defaultbold">More Press Releases...</span>-->
                                </a>
                              </td>
                            </xsl:when>
                            <xsl:otherwise>
                              <td class="data">&#160;</td>
                            </xsl:otherwise>
                          </xsl:choose>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td class="data" valign="top">
                      <table border="0" cellspacing="0" cellpadding="4" width="100%" class="table2" id="PRTable">
                        <tr>
                          <td class="table1_item datashade" valign="top" width="70">
                            <span class="defaultbold">Date</span>
                          </td>
                          <td class="table1_item datashade" valign="top" >
                            <span class="defaultbold">Headline</span>
                          </td>
                          <td class="table1_item datashade" valign="top"  align="center">
                            <span class="defaultbold">Format</span>
                          </td>
                        </tr>
                        <xsl:for-each select="PressRelease">
                          <tr class="data">
                            <td class="data" valign="top">
                              <xsl:value-of select="Date" disable-output-escaping="yes"/>
                            </td>
                            <td class="data" valign="top" align="left">
                              <a class="PRLink" title="{Headline}">
                                <xsl:variable name="url" select="Link" />
                                <xsl:variable name="isPreview" select="../Company/IsPreview" />
                                <xsl:attribute name="target">_new</xsl:attribute>
                                <xsl:attribute name="href">
                                  <xsl:value-of select="$url" />
                                  <xsl:if test="contains($isPreview, 'true')">
                                    <xsl:if test="contains($url, 'file.aspx')">&amp;preview=1</xsl:if>
                                  </xsl:if>
                                </xsl:attribute>
                                <xsl:value-of select="Headline" disable-output-escaping="yes"/>
                              </a>
                              <xsl:if test="KeyFileFormat = 7 and SuppressPreviewDisplay = 'false'">
                                <img class="PRPreviewLink" src="images/mini-mag.gif" />
                              </xsl:if>
                            </td>
                              <TD CLASS="data" ALIGN="center">
                                <A TARGET="_new" title="View File">
                                    <xsl:variable name="url" select="Link" />
                                    <xsl:variable name="isPreview" select="../Company/IsPreview" />							
                                    <xsl:attribute name="href"><xsl:value-of select="$url" /><xsl:if test="contains($isPreview, 'true')"><xsl:if test="contains($url, 'file.aspx')">&amp;preview=1</xsl:if></xsl:if></xsl:attribute>
                                  <IMG  BORDER="0">
                                    <xsl:attribute name="SRC">
                                      <xsl:value-of select="IconImage" />
                                    </xsl:attribute>
                                  </IMG>
                                  <BR />
                                  <xsl:value-of select="IconFormat"/>
                                </A>
                              </TD>
                          </tr>
                        </xsl:for-each>
                        <xsl:if test="PressRelease/IconLink != ''">
                          <tr>
                            <td colspan="3">
                              Click the link to the right to download:
                              <A TARGET="_new" title="Download Adobe Acrobat">
                                <xsl:attribute name="HREF">http://get.adobe.com/reader/</xsl:attribute>
                                <xsl:value-of select="IconFormat"/>Adobe Acrobat
                              </A>
                            </td>
                          </tr>
                        </xsl:if>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </xsl:if>
        </xsl:otherwise>
      </xsl:choose>
    </table>
  </xsl:template>




  <xsl:template name="SourceTag3">
    <table border="0" cellspacing="0" cellpadding="0" width="100%" class="data">
      <xsl:choose>
        <xsl:when test="Company/TotalNewsArticles &gt; Company/RowsPerPage or Company/TotalPressReleases &gt; Company/RowsPerPage">
          <xsl:variable name="EndPage" select="Company/Start + Company/RowsPerPage - 1"/>
          <tr>
            <td class="data">
              <span class="default">
                &#160;<i>Source: S&amp;P Global Market Intelligence</i>
              </span>
            </td>
          </tr>
          <tr valign="top" class="data">

            <xsl:if test="Company/DisplayMode = 1">
              <td class="data defaultbold">
                Rows <xsl:value-of select="Company/Start"/> through
                <xsl:choose>
                  <xsl:when test="$EndPage &gt; Company/TotalPressReleases">
                      <xsl:value-of select="Company/TotalPressReleases"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="$EndPage"/>
                  </xsl:otherwise>
                </xsl:choose>
                of <xsl:value-of select="Company/TotalPressReleases"/>
              </td>
            </xsl:if>

            <xsl:if test="Company/DisplayMode = 2">
              <td class="data defaultbold">
                Rows <xsl:value-of select="Company/Start"/> through
                <xsl:choose>
                  <xsl:when test="$EndPage &gt; Company/TotalNewsArticles">                    
                      <xsl:value-of select="Company/TotalNewsArticles"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="$EndPage"/>
                  </xsl:otherwise>
                </xsl:choose>
                of <xsl:value-of select="Company/TotalNewsArticles"/>
              </td>
            </xsl:if>

            <td class="data" ALIGN="RIGHT">
              <xsl:for-each select="Pagination">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                <xsl:choose>
                  <xsl:when test="CurrentPage = 'false'">
                    <a target="_self" title="{PageLabel}">
                      <xsl:attribute name="href">
                        <xsl:value-of select="PageLink"/>
                      </xsl:attribute>
                      <span class="i">
                        <xsl:value-of select="PageLabel"/>
                      </span>
                    </a>
                  </xsl:when>
                  <xsl:otherwise>
                    <span class="defaultbold"><xsl:value-of select="PageLabel"/></span>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:for-each>
            </td>
          </tr>
        </xsl:when>
        <xsl:otherwise>
          <tr class="data">
            <td  class="data" colspan="2">
              <span class="default">
                &#160;<i>Source: S&amp;P Global Market Intelligence</i>
              </span>
            </td>
          </tr>
        </xsl:otherwise>
      </xsl:choose>

    </table>
  </xsl:template>





  <xsl:template name="FooterTag3">
    <xsl:if test="Company/Footer != ''">
      <tr>
        <td class="data">
          <span class="default">
            <xsl:value-of select="Company/Footer" disable-output-escaping="yes"/>
          </span>
        </td>
      </tr>
    </xsl:if>
  </xsl:template>
  <!-- Template 3 Ends here. -->



















  <xsl:template name="PressReleases2CurrentPR">
    <table border="0" cellspacing="0" cellpadding="0" width="100%" id="PRTable">
      <xsl:choose>
        <xsl:when test="Company/TotalPressReleases=0">
        </xsl:when>
        <xsl:otherwise>
          <xsl:if test="Company/DisplayMode=1 or Company/DisplayMode=0 or string-length(Company/DisplayMode)=0">
            <tr class="colorlight default">
              <td colspan="2">
                <table border="0" cellspacing="0" cellpadding="4" width="100%">
                  <tr>
                    <td nowrap="nowrap" colspan="2">
                      <span class="title2 titletest2">
                        Current&#160;<xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/>
                      </span>
                    </td>
                  </tr>
                  <tr class="data">
                    <td colspan="2" class="data">
                      <table border="0" cellspacing="0" cellpadding="4" width="100%">
                        <tr>
                          <td class="surrleft datashade" valign="top" width="70">
                            <span class="titletest2">Date</span>
                          </td>
                          <td class="surr_topbot datashade" valign="top" >
                            <span class="titletest2">Headline</span>
                          </td>
                          <td class="surrright datashade" valign="top"  align="center">
                            <span class="titletest2">Format</span>
                          </td>
                        </tr>
                        <xsl:for-each select="PressRelease[IsCurrentPressRelease = 'True']">
                          <tr class="data">
                            <td class="data" valign="top">
                              <xsl:value-of select="Date" disable-output-escaping="yes"/>
                            </td>
                            <td class="data" valign="top" align="left">
                              <a>
                                <xsl:variable name="url" select="Link" />
                                <xsl:attribute name="target">_new</xsl:attribute>
                                <xsl:attribute name="href">
                                  <xsl:value-of select="$url" />
                                </xsl:attribute>
                                <xsl:value-of select="Headline" disable-output-escaping="yes"/>
                              </a>
                              <xsl:if test="KeyFileFormat = 7 and SuppressPreviewDisplay = 'false'">
                                <img class="PRPreviewLink" src="images/mini-mag.gif" />
                              </xsl:if>
                            </td>
                              <TD CLASS="data" ALIGN="center">
                                <A TARGET="_new" title="View File">
                                    <xsl:variable name="url" select="Link" />
                                    <xsl:variable name="isPreview" select="../Company/IsPreview" />							
                                    <xsl:attribute name="href"><xsl:value-of select="$url" /><xsl:if test="contains($isPreview, 'true')"><xsl:if test="contains($url, 'file.aspx')">&amp;preview=1</xsl:if></xsl:if></xsl:attribute>
                                  <IMG  BORDER="0">
                                    <xsl:attribute name="SRC">
                                      <xsl:value-of select="IconImage" />
                                    </xsl:attribute>
                                  </IMG>
                                  <BR />
                                  <xsl:value-of select="IconFormat"/>
                                </A>
                              </TD>
                          </tr>
                        </xsl:for-each>


                      </table>
                    </td>
                  </tr>



                  <tr>
                    <td nowrap="nowrap">
                      <span class="title2 titletest2">
                        <xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/>
                      </span>
                    </td>
                    <td align="right" nowrap="nowrap">
                      <a>
                        <!--<xsl:variable name="keyinstn" select="Company/KeyInstn" />
                        <xsl:variable name="linkGuts" select="concat($keyinstn, '&amp;mode=1&amp;type=', AvailableTypes[Selected != '']/TypeValue)"/>
                        <xsl:variable name="linkGutsPlusYear">
                          <xsl:if test="Company/YearAll = 'true'">
                            <xsl:value-of select="$linkGuts"/>
                          </xsl:if>
                          <xsl:if test="Company/YearAll = 'false'">
                            <xsl:value-of select="concat($linkGuts, '&amp;Year=', Company/Year)"/>
                          </xsl:if>
                        </xsl:variable>
                        <xsl:variable name="pressurl" select="concat('News.aspx?IID=', $linkGutsPlusYear)" />
                        <xsl:variable name="yearlypressurl" select="concat('YearlyNews.aspx?IID=', $linkGutsPlusYear)" />-->

                        <!--<xsl:variable name="pressurl" select="concat('News.aspx?IID=', $keyinstn, '&amp;mode=1&amp;type=', AvailableTypes[Selected != '']/TypeValue)" />
                        -->
                        <!--<xsl:variable name="yearlypressurl" select="concat('YearlyNews.aspx?IID=', $keyinstn, '&amp;mode=1&amp;Year=', Company/Year, '&amp;type=', AvailableTypes[Selected != '']/TypeValue)" />-->
                        <!--
                        <xsl:variable name="yearlypressurl">
                          <xsl:variable name="tempLink" select="concat('YearlyNews.aspx?IID=', $keyinstn, '&amp;mode=1&amp;type=', AvailableTypes[Selected != '']/TypeValue)"/>
                          <xsl:if test="Company/YearAll = 'true'">
                            <xsl:value-of select="$tempLink"/>
                          </xsl:if>
                          <xsl:if test="Company/YearAll = 'false'">
                            <xsl:value-of select="concat($tempLink, '&amp;Year=', Company/Year)"/>
                          </xsl:if>
                        </xsl:variable>-->

                        <!--<xsl:if test="normalize-space(Company/Year)">-->
                        <!--
                        <xsl:if test="Company/KeyPage = 201855">
                          <xsl:attribute name="href">
                            <xsl:value-of select="$yearlypressurl" />
                          </xsl:attribute>
                        </xsl:if>
                        -->
                        <!--<xsl:if test="not(normalize-space(Company/Year))">-->
                        <!--
                        <xsl:if test="Company/KeyPage = 175">
                          <xsl:attribute name="href">
                            <xsl:value-of select="$pressurl" />
                          </xsl:attribute>
                        </xsl:if>-->

                        <xsl:call-template name="MorePressReleases">
                          <xsl:with-param name="spanclass" select="' titletest2'"/>
                        </xsl:call-template>
                        <!--<span class=" titletest2">More Press Releases...</span>-->
                      </a>
                    </td>
                  </tr>
                  <tr class="data">
                    <td colspan="2" class="data">
                      <table border="0" cellspacing="0" cellpadding="4" width="100%">
                        <tr>
                          <td class="surrleft datashade" valign="top" width="70">
                            <span class="titletest2">Date</span>
                          </td>
                          <td class="surr_topbot datashade" valign="top" >
                            <span class="titletest2">Headline</span>
                          </td>
                          <td class="surrright datashade" valign="top"  align="center">
                            <span class="titletest2">Format</span>
                          </td>
                        </tr>
                        <xsl:for-each select="PressRelease[IsCurrentPressRelease = 'False']">
                          <tr class="data">
                            <td class="data" valign="top">
                              <xsl:value-of select="Date" disable-output-escaping="yes"/>
                            </td>
                            <td class="data" valign="top" align="left">
                              <a>
                                <xsl:variable name="url" select="Link" />
                                <xsl:attribute name="target">_new</xsl:attribute>
                                <xsl:attribute name="href">
                                  <xsl:value-of select="$url" />
                                </xsl:attribute>
                                <xsl:value-of select="Headline" disable-output-escaping="yes"/>
                              </a>
                              <xsl:if test="KeyFileFormat = 7 and SuppressPreviewDisplay = 'false'">
                                <img class="PRPreviewLink" src="images/mini-mag.gif" />
                              </xsl:if>
                            </td>
                              <TD CLASS="data" ALIGN="center">
                                <A TARGET="_new" title="View File">
                                    <xsl:variable name="url" select="Link" />
                                    <xsl:variable name="isPreview" select="../Company/IsPreview" />							
                                    <xsl:attribute name="href"><xsl:value-of select="$url" /><xsl:if test="contains($isPreview, 'true')"><xsl:if test="contains($url, 'file.aspx')">&amp;preview=1</xsl:if></xsl:if></xsl:attribute>
                                  <IMG  BORDER="0">
                                    <xsl:attribute name="SRC">
                                      <xsl:value-of select="IconImage" />
                                    </xsl:attribute>
                                  </IMG>
                                  <BR />
                                  <xsl:value-of select="IconFormat"/>
                                </A>
                              </TD>
                          </tr>
                        </xsl:for-each>


                      </table>
                    </td>
                  </tr>



                </table>
              </td>
            </tr>
            <tr class="colorlight default">
              <td colspan="2">
                <span class="default">&#160;</span>
              </td>
            </tr>
          </xsl:if>
        </xsl:otherwise>
      </xsl:choose>
    </table>
  </xsl:template>


  <xsl:template name="PressReleases3CurrentPR">
    <table border="0" cellspacing="0" cellpadding="0" width="100%" id="PRTable">
      <xsl:choose>
        <xsl:when test="Company/TotalPressReleases=0">
        </xsl:when>
        <xsl:otherwise>
          <xsl:if test="Company/DisplayMode=1 or Company/DisplayMode=0 or string-length(Company/DisplayMode)=0">
            <tr class="colorlight default">
              <td colspan="2">
                <table border="0" cellspacing="0" cellpadding="4" width="100%">
                  <xsl:call-template name="NotifyText3"/>
                  <tr>
                    <td nowrap="nowrap" colspan="2">
                      <span class="title2 titletest2">
                        Current &#160;<xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/>
                      </span>
                    </td>
                    <!--td align="right" nowrap="nowrap">
	<span class=" titletest2">Current Press Releases...</span>
</td-->
                  </tr>
                  <tr class="data">
                    <td colspan="2" class="data">
                      <table border="0" cellspacing="0" cellpadding="4" width="100%">
                        <tr>
                          <td class="surrleft datashade" valign="top" width="70">
                            <span class="titletest2">Date</span>
                          </td>
                          <td class="surr_topbot datashade" valign="top" >
                            <span class="titletest2">Headline</span>
                          </td>
                          <td class="surrright datashade" valign="top"  align="center">
                            <span class="titletest2">Format</span>
                          </td>
                        </tr>
                        <xsl:for-each select="PressRelease[IsCurrentPressRelease = 'True']">
                          <tr class="data">
                            <td class="data" valign="top">
                              <xsl:value-of select="Date" disable-output-escaping="yes"/>
                            </td>
                            <td class="data" valign="top" align="left">
                              <a>
                                <xsl:variable name="url" select="Link" />
                                <xsl:attribute name="target">_new</xsl:attribute>
                                <xsl:attribute name="href">
                                  <xsl:value-of select="$url" />
                                </xsl:attribute>
                                <xsl:value-of select="Headline" disable-output-escaping="yes"/>
                              </a>
                              <xsl:if test="KeyFileFormat = 7 and SuppressPreviewDisplay = 'false'">
                                <img class="PRPreviewLink" src="images/mini-mag.gif" />
                              </xsl:if>
                            </td>
                              <TD CLASS="data" ALIGN="center">
                                <A TARGET="_new" title="View File"> 
                                    <xsl:variable name="url" select="Link" />
                                    <xsl:variable name="isPreview" select="../Company/IsPreview" />							
                                    <xsl:attribute name="href"><xsl:value-of select="$url" /><xsl:if test="contains($isPreview, 'true')"><xsl:if test="contains($url, 'file.aspx')">&amp;preview=1</xsl:if></xsl:if></xsl:attribute>
                                  <IMG  BORDER="0">
                                    <xsl:attribute name="SRC">
                                      <xsl:value-of select="IconImage" />
                                    </xsl:attribute>
                                  </IMG>
                                  <BR />
                                  <xsl:value-of select="IconFormat"/>
                                </A>
                              </TD>
                          </tr>
                        </xsl:for-each>


                      </table>
                    </td>
                  </tr>



                  <tr>
                    <td nowrap="nowrap">
                      <span class="title2 titletest2">
                        <xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/>
                      </span>
                    </td>
                    <td align="right" nowrap="nowrap">
                      <a>
                        <!--<xsl:variable name="keyinstn" select="Company/KeyInstn" />
                        <xsl:variable name="linkGuts" select="concat($keyinstn, '&amp;mode=1&amp;type=', AvailableTypes[Selected != '']/TypeValue)"/>
                        <xsl:variable name="linkGutsPlusYear">
                          <xsl:if test="Company/YearAll = 'true'">
                            <xsl:value-of select="$linkGuts"/>
                          </xsl:if>
                          <xsl:if test="Company/YearAll = 'false'">
                            <xsl:value-of select="concat($linkGuts, '&amp;Year=', Company/Year)"/>
                          </xsl:if>
                        </xsl:variable>
                        <xsl:variable name="pressurl" select="concat('News.aspx?IID=', $linkGutsPlusYear)" />
                        <xsl:variable name="yearlypressurl" select="concat('YearlyNews.aspx?IID=', $linkGutsPlusYear)" />-->

                        <!--<xsl:variable name="pressurl" select="concat('News.aspx?IID=', $keyinstn, '&amp;mode=1&amp;type=', AvailableTypes[Selected != '']/TypeValue)" />
                        -->
                        <!--<xsl:variable name="yearlypressurl" select="concat('YearlyNews.aspx?IID=', $keyinstn, '&amp;mode=1&amp;Year=', Company/Year, '&amp;type=', AvailableTypes[Selected != '']/TypeValue)" />-->
                        <!--
                        <xsl:variable name="yearlypressurl">
                          <xsl:variable name="tempLink" select="concat('YearlyNews.aspx?IID=', $keyinstn, '&amp;mode=1&amp;type=', AvailableTypes[Selected != '']/TypeValue)"/>
                          <xsl:if test="Company/YearAll = 'true'">
                            <xsl:value-of select="$tempLink"/>
                          </xsl:if>
                          <xsl:if test="Company/YearAll = 'false'">
                            <xsl:value-of select="concat($tempLink, '&amp;Year=', Company/Year)"/>
                          </xsl:if>
                        </xsl:variable>-->

                        <!--<xsl:if test="normalize-space(Company/Year)">-->
                        <!--
                        <xsl:if test="Company/KeyPage = 201855">
                          <xsl:attribute name="href">
                            <xsl:value-of select="$yearlypressurl" />
                          </xsl:attribute>
                        </xsl:if>
                        -->
                        <!--<xsl:if test="not(normalize-space(Company/Year))">-->
                        <!--
                        <xsl:if test="Company/KeyPage = 175">
                          <xsl:attribute name="href">
                            <xsl:value-of select="$pressurl" />
                          </xsl:attribute>
                        </xsl:if>-->

                        <xsl:call-template name="MorePressReleases">
                          <xsl:with-param name="spanclass" select="' titletest2'"/>
                        </xsl:call-template>
                        <!--<span class=" titletest2">More Press Releases...</span>-->
                      </a>
                    </td>
                  </tr>
                  <tr class="data">
                    <td colspan="2" class="data">
                      <table border="0" cellspacing="0" cellpadding="4" width="100%">
                        <tr>
                          <td class="surrleft datashade" valign="top" width="70">
                            <span class="titletest2">Date</span>
                          </td>
                          <td class="surr_topbot datashade" valign="top" >
                            <span class="titletest2">Headline</span>
                          </td>
                          <td class="surrright datashade" valign="top"  align="center">
                            <span class="titletest2">Format</span>
                          </td>
                        </tr>
                        <xsl:for-each select="PressRelease[IsCurrentPressRelease = 'False']">
                          <tr class="data">
                            <td class="data" valign="top">
                              <xsl:value-of select="Date" disable-output-escaping="yes"/>
                            </td>
                            <td class="data" valign="top" align="left">
                              <a>
                                <xsl:variable name="url" select="Link" />
                                <xsl:attribute name="target">_new</xsl:attribute>
                                <xsl:attribute name="href">
                                  <xsl:value-of select="$url" />
                                </xsl:attribute>
                                <xsl:value-of select="Headline" disable-output-escaping="yes"/>
                              </a>
                              <xsl:if test="KeyFileFormat = 7 and SuppressPreviewDisplay = 'false'">
                                <img class="PRPreviewLink" src="images/mini-mag.gif" />
                              </xsl:if>
                            </td>
                              <TD CLASS="data" ALIGN="center">
                                <A TARGET="_new" title="View File">
                                    <xsl:variable name="url" select="Link" />
                                    <xsl:variable name="isPreview" select="../Company/IsPreview" />							
                                    <xsl:attribute name="href"><xsl:value-of select="$url" /><xsl:if test="contains($isPreview, 'true')"><xsl:if test="contains($url, 'file.aspx')">&amp;preview=1</xsl:if></xsl:if></xsl:attribute>
                                  <IMG  BORDER="0">
                                    <xsl:attribute name="SRC">
                                      <xsl:value-of select="IconImage" />
                                    </xsl:attribute>
                                  </IMG>
                                  <BR />
                                  <xsl:value-of select="IconFormat"/>
                                </A>
                              </TD>
                          </tr>
                        </xsl:for-each>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr class="colorlight default">
              <td colspan="2">
                <span class="default">&#160;</span>
              </td>
            </tr>
          </xsl:if>
        </xsl:otherwise>
      </xsl:choose>
    </table>
  </xsl:template>



  <xsl:template name="PressReleasesCurrentPR">
    <table border="0" cellspacing="0" cellpadding="4" width="100%" id="PRTable">
      <tr class="header">
        <td class="header" nowrap="nowrap">
          Current&#160;<xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/>
        </td>
      </tr>
      <tr class="data">
        <td colspan="2" class="data">
          <table border="0" cellspacing="0" cellpadding="4" width="100%">
            <xsl:for-each select="PressRelease[IsCurrentPressRelease = 'True']">
              <tr class="data">
                <td class="data" valign="top" width="15%">
                  <xsl:value-of select="Date" disable-output-escaping="yes"/>
                </td>
                <td class="data" valign="top" align="left">
                  <a>
                    <xsl:variable name="url" select="Link" />
                    <xsl:attribute name="target">_new</xsl:attribute>
                    <xsl:attribute name="href">
                      <xsl:value-of select="$url" />
                    </xsl:attribute>
                    <xsl:value-of select="Headline" disable-output-escaping="yes"/>
                  </a>
                  <xsl:if test="KeyFileFormat = 7 and SuppressPreviewDisplay = 'false'">
                    <img class="PRPreviewLink" src="images/mini-mag.gif" />
                  </xsl:if>
                </td>
              </tr>
            </xsl:for-each>
          </table>
        </td>
      </tr>

      <xsl:if test="PressRelease/ArchiveLengthCountPRParent &lt; Company/TotalPressReleases">
        <tr class="header">
          <td class="header" nowrap="nowrap">
            <xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/>
          </td>
        </tr>
        <tr class="data">
          <td colspan="2" class="data">
            <table border="0" cellspacing="0" cellpadding="4" width="100%">
              <xsl:for-each select="PressRelease[IsCurrentPressRelease = 'False']">
                <tr class="data">
                  <td class="data" valign="top" width="15%">
                    <xsl:value-of select="Date" disable-output-escaping="yes"/>
                  </td>
                  <td class="data" valign="top" align="left">
                    <a>
                      <xsl:variable name="url" select="Link" />
                      <xsl:attribute name="target">_new</xsl:attribute>
                      <xsl:attribute name="href">
                        <xsl:value-of select="$url" />
                      </xsl:attribute>
                      <xsl:value-of select="Headline" disable-output-escaping="yes"/>
                    </a>
                    <xsl:if test="KeyFileFormat = 7 and SuppressPreviewDisplay = 'false'">
                      <img class="PRPreviewLink" src="images/mini-mag.gif" />
                    </xsl:if>
                  </td>
                </tr>
              </xsl:for-each>
            </table>
          </td>
        </tr>
        <tr>
          <td align="right">
            <a>
              <!--<xsl:variable name="keyinstn" select="Company/KeyInstn" />
              <xsl:variable name="linkGuts" select="concat($keyinstn, '&amp;mode=1&amp;type=', AvailableTypes[Selected != '']/TypeValue)"/>
              <xsl:variable name="linkGutsPlusYear">
                <xsl:if test="Company/YearAll = 'true'">
                  <xsl:value-of select="$linkGuts"/>
                </xsl:if>
                <xsl:if test="Company/YearAll = 'false'">
                  <xsl:value-of select="concat($linkGuts, '&amp;Year=', Company/Year)"/>
                </xsl:if>
              </xsl:variable>
              <xsl:variable name="pressurl" select="concat('News.aspx?IID=', $linkGutsPlusYear)" />
              <xsl:variable name="yearlypressurl" select="concat('YearlyNews.aspx?IID=', $linkGutsPlusYear)" />-->

              <!--<xsl:variable name="pressurl" select="concat('News.aspx?IID=', $keyinstn, '&amp;mode=1&amp;type=', AvailableTypes[Selected != '']/TypeValue)" />
              -->
              <!--<xsl:variable name="yearlypressurl" select="concat('YearlyNews.aspx?IID=', $keyinstn, '&amp;mode=1&amp;Year=', Company/Year, '&amp;type=', AvailableTypes[Selected != '']/TypeValue)" />-->
              <!--
              <xsl:variable name="yearlypressurl">
                <xsl:variable name="tempLink" select="concat('YearlyNews.aspx?IID=', $keyinstn, '&amp;mode=1&amp;type=', AvailableTypes[Selected != '']/TypeValue)"/>
                <xsl:if test="Company/YearAll = 'true'">
                  <xsl:value-of select="$tempLink"/>
                </xsl:if>
                <xsl:if test="Company/YearAll = 'false'">
                  <xsl:value-of select="concat($tempLink, '&amp;Year=', Company/Year)"/>
                </xsl:if>
              </xsl:variable>-->

              <!--<xsl:attribute name="class">boldfielddef</xsl:attribute>
              -->
              <!--<xsl:if test="normalize-space(Company/Year)">-->
              <!--
              <xsl:if test="Company/KeyPage = 201855">
                <xsl:attribute name="href">
                  <xsl:value-of select="$yearlypressurl" />
                </xsl:attribute>
              </xsl:if>
              -->
              <!--<xsl:if test="not(normalize-space(Company/Year))">-->
              <!--
              <xsl:if test="Company/KeyPage = 175">
                <xsl:attribute name="href">
                  <xsl:value-of select="$pressurl" />
                </xsl:attribute>
              </xsl:if>-->

              <xsl:call-template name="MorePressReleases">
                <xsl:with-param name="bold" select="'true'"/>
              </xsl:call-template>
              <!--<xsl:attribute name="class">boldfielddef</xsl:attribute>
              More Press Releases...-->
            </a>
          </td>
        </tr>
      </xsl:if>
    </table>
  </xsl:template>

  <xsl:template name="PRPreview">
    <xsl:if test="Company/TemplateName != 'AltNews4' or Company/TemplateName != 'AltYRNews4'">
		 <xsl:if test="$jquery = 'true'">
          <script language="javascript" type="text/javascript" src="{$jquerymin}"></script>
        </xsl:if>
	</xsl:if>
    <script language="javascript" type="text/javascript" src="javascript/irjson.js"></script>
    <script language="javascript" type="text/javascript">
      var currentKeyInstn = "<xsl:value-of select="Company/KeyInstn" />";
      <![CDATA[        
    $(document).ready(function() {
      $("body").append($("#PRPreview"));

      var header_color = "#9f9f9f";
      var previewLength = 500;
      var previewWidth = 500;
      var previewLeft = 0;

      if ($("#PRTable").length == 1) {
        $(".header").eq(0).each(function() {
          header_color = $(this).css("background-color");
        });

        $("#PRPreview").css("background-color", header_color);
        $("#SpeechBubble").css("border-top-color", header_color);

        var previewTimeout;
        previewWidth = $("#PRTable").outerWidth() - 22;
        previewLeft = $("#PRTable").offset().left + 12;
        var LoadedPRs = new Array();
      }

      $(".PRPreviewLink").mouseover(function() {
        clearTimeout(previewTimeout);
        var prLink = $(this).prev("a");
        var previewLinkTop = $(this).offset().top;
        var previewLinkLeft = $(this).offset().left;

        var prHref = prLink.attr("href");
        $("#PRPreviewLink").attr("href", prHref);

        var prKeyFile = prHref.substring(prHref.indexOf("FID=") + 4);
        if (prKeyFile.indexOf("&") >= 0) prKeyFile = prKeyFile.substring(0, prKeyFile.indexOf("&"));
        var webServiceURL = "PRService.svc/GetPRAbstract";

        var data = { keyFile: prKeyFile, keyInstn: currentKeyInstn };

        if (LoadedPRs[prKeyFile]) {
          $("#PRPreviewText").html(LoadedPRs[prKeyFile]);
          var PRTextHeight = $("#PRPreviewText").outerHeight() + 18;
          var previewTop = previewLinkTop - $("#PRPreview").outerHeight() - 18;
          var bubbleLeft = previewLinkLeft - previewLeft - 2;
          $("#SpeechBubble").css("left", bubbleLeft + "px");
          $("#PRPreview").css("top", previewTop + "px").css("left", previewLeft + "px");
		  if ($.browser.msie && $.browser.version.substr(0, 1) < 7) {
			var PRTextHeight = $("#PRPreviewText").outerHeight();
			$("#frmnews").height(PRTextHeight);
			$("#frmnews").css("top", previewTop + "px").css("left", previewLeft + "px");
		  }
          //$("#PRPreview").height(PRTextHeight);
        }
        else {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: webServiceURL,
            data: JSON.stringify(data),
            success: function(msg)
            {
              var prText = msg.d;
              if (prText.length > previewLength) prText = prText.substring(0, previewLength) + "...";

              LoadedPRs[prKeyFile] = prText;
              $("#PRPreviewText").html(prText);
              var PRTextHeight = $("#PRPreviewText").outerHeight() + 18;
              var previewTop = previewLinkTop - $("#PRPreview").outerHeight() - 18;
              var bubbleLeft = previewLinkLeft - previewLeft - 2;
              $("#SpeechBubble").css("left", bubbleLeft + "px");
              $("#PRPreview").css("top", previewTop + "px").css("left", previewLeft + "px");			  
              if ($.browser.msie && $.browser.version.substr(0, 1) < 7) { 
				var PRTextHeight = $("#PRPreviewText").outerHeight();
				$("#frmnews").height(PRTextHeight);
				$("#frmnews").css("top", previewTop + "px").css("left", previewLeft + "px");
			  }
              //$("#PRPreview").height(PRTextHeight);
            },
            error: function()
            {
                $("#PRPreview").css("left", "-2000px");
                if ($.browser.msie && $.browser.version.substr(0, 1) < 7) {
					$("#frmnews").css("left", "-2000px");
				}
            }
        });
        }
      });

      $(".PRPreviewLink").mouseout(function() {
        previewTimeout = setTimeout("clearPreview()", 500);
      });

      $("#PRPreview").width(previewWidth).mouseover(function() {
        clearTimeout(previewTimeout);
      }).mouseout(function() {
        previewTimeout = setTimeout("clearPreview()", 500);
      });
    });

    function clearPreview() {
      $("#PRPreview").css("left", "-2000px");
	  if ($.browser.msie && $.browser.version.substr(0, 1) < 7) {
		$("#frmnews").css("left", "-2000px"); 
	  }
    }
    ]]>
    </script>


    <style type="text/css">
      #PRPreview { position: absolute; left: -2000px; top: 0px; padding: 3px; text-align: left; cursor: pointer; background-color: #9f9f9f;}
      #PRHead{ padding:0px 3px 3px; }
      #PRPreview a, #PRPreview a span { font-size: 10px; text-decoration: none; }
	  #PRPreviewText { background-color: #f3f3f3; padding: 6px; line-height:15px;}
      #SpeechBubble { position: absolute; bottom: -16px; left: 0px; width: 0px; height: 0px; line-height: 0px; font-size: 0px; letter-spacing: 0px; border-color: transparent; border-style: solid; border-width: 16px 8px 0px; border-top-color:#9f9f9f; }
      .PRPreviewLink { margin:0px 0px -3px 6px; }
    </style>
	<xsl:comment><![CDATA[[if lte IE 6]><style type="text/css">#SpeechBubble {width: 16px; height: 16px;border: none; background:url("images/tooltips.gif") no-repeat scroll 0 0 transparent;} #frmnews{position: absolute;left: -2000px;padding: 3px;}</style><![endif]]]></xsl:comment>
    <xsl:variable name="url" select="PressRelease/Link"/>
    <xsl:comment><![CDATA[[if lte IE 6]><iframe id="frmnews" frameborder="0" scrolling="no"></iframe><![endif]]]></xsl:comment>
    <div id="PRPreview">
      <a id="PRPreviewLink" href="#" target="_blank" title="{Company/InstnName}">
        <div id="PRHead" class="header">
          <xsl:value-of select="Company/InstnName" />&#160;(<xsl:value-of select="Company/Exchange" />)
        </div>
        <div id="PRPreviewText"></div>
        <div id="SpeechBubble"></div>
      </a>
    </div>
  </xsl:template>
  <!-- Main XSLT templates are here. these are the templates which are actually being displayed in the page. this is for TOP LEVEL editing. if you dont need to use the individual templates above you can do your main editing here when pulling THESE templates below you must carry the top comment with them and uncomment them when you drop it into the company specific xslt.-->


  <xsl:template match="irw:IRW/irw:News">
    <xsl:variable name="TemplateName" select="Company/TemplateName"/>
    <xsl:choose>
      <!-- Template 1 -->
      <xsl:when test="$TemplateName = 'AltNews1' or $TemplateName = 'AltYRNews1'">
        <xsl:call-template name="TemplateONEstylesheet" />
        <table BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%">
          <xsl:call-template name="Title_S2"/>
          <tr align="center">
            <td class="leftTOPbord data" colspan="2">
              <xsl:choose>
                <xsl:when test="normalize-space(Company/Year) and (Company/TotalPressReleases + Company/TotalNewsArticles=0)">
                  <!-- If we're on yearly news, check this, otherwise just do it -->
                  <table border="0" cellspacing="0" cellpadding="4" width="100%">
                    <xsl:call-template name="Year_Header2"/>
                    <tr>
                      <td class="data" valign="top">
                        <span class="default">There are no documents available for the current year.</span>
                      </td>
                    </tr>
                  </table>
                </xsl:when>
                <xsl:otherwise>
                  <table border="0" cellspacing="0" cellpadding="4" width="100%">
                    <xsl:call-template name="Year_Header2"/>
                    <xsl:choose>
                      <xsl:when test="Company/TotalPressReleases=0">
                        <xsl:call-template name="NotifyText2"/>
                        <tr>
                          <td class="data" valign="top">There are currently no releases of this type available.</td>
                        </tr>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:if test="Company/DisplayMode=1 or Company/DisplayMode=0 or string-length(Company/DisplayMode)=0">
                          <tr>
                            <td class="data" valign="top">
                              <xsl:if test="PressRelease/ArchiveLengthCountPRParent > 0 and Company/DisplayMode=0">
                                <xsl:call-template name="PressReleases2CurrentPR"/>
                              </xsl:if>
                              <xsl:choose>
                                <xsl:when test="Company/DisplayMode=0">
                                  <xsl:if test="PressRelease/ArchiveLengthCountPRParent &lt; 0 or PressRelease/ArchiveLengthCountPRParent = 0">
                                    <xsl:call-template name="PressReleases2"/>
                                  </xsl:if>
                                </xsl:when>
                                <xsl:otherwise>
                                  <xsl:call-template name="PressReleases2"/>
                                </xsl:otherwise>
                              </xsl:choose>
                            </td>
                          </tr>
                          <tr>
                            <td class="botbord data" valign="top">&#160;</td>
                          </tr>
                        </xsl:if>
                      </xsl:otherwise>
                    </xsl:choose>

                    <xsl:choose>
                      <xsl:when test="Company/TotalNewsArticles=0">
                      </xsl:when>
                      <xsl:otherwise>

                      </xsl:otherwise>
                    </xsl:choose>
                    <tr>
                      <td class="data" valign="top">
                        <xsl:call-template name="SourceTag2"/>
                      </td>
                    </tr>
                  </table>
                </xsl:otherwise>
              </xsl:choose>
            </td>
          </tr>
          <xsl:call-template name="FooterTag2"/>
        </table>
        <xsl:call-template name="Copyright"/>
      </xsl:when>
      <!-- End Template 1 -->

      <!-- Template 4 -->
      <xsl:when test="$TemplateName = 'AltNews4' or $TemplateName = 'AltYRNews4'">
        <xsl:call-template name="News_4" />        
      </xsl:when>      
      <!-- End Template 4 -->
      
      <!-- Template 3 -->      
      <xsl:when test="$TemplateName = 'AltNews3' or $TemplateName = 'AltYRNews3'">
        <xsl:call-template name="TemplateTHREEstylesheet" />
        <xsl:call-template name="Title_T3"/>
        <table BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
          <tr align="center">
            <td class="data" valign="top">
              <xsl:choose>
                <xsl:when test="normalize-space(Company/Year) and (Company/TotalPressReleases + Company/TotalNewsArticles=0)">
                  <!-- If we're on yearly news, check this, otherwise just do it -->
                  <table BORDER="0" CELLSPACING="0" CELLPADDING="4" WIDTH="100%" ID="Table2">
                    <xsl:call-template name="Year_Header3"/>
                    <tr>
                      <td class="data" valign="top">
                        <span class="default">There are no documents available for the current year.</span>
                      </td>
                    </tr>
                  </table>
                </xsl:when>
                <xsl:otherwise>
                  <table BORDER="0" CELLSPACING="0" CELLPADDING="4" WIDTH="100%" ID="Table2" class="table2">
                    <xsl:call-template name="Year_Header3"/>
                    <xsl:choose>
                      <xsl:when test="Company/TotalPressReleases=0">
                        <xsl:call-template name="NotifyText3"/>
                        <tr>
                          <td class="data" valign="top">There are currently no releases of this type available.</td>
                        </tr>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:if test="Company/DisplayMode=1 or Company/DisplayMode=0 or string-length(Company/DisplayMode)=0">
                          <tr>
                            <td class="data" valign="top">
                              <xsl:if test="PressRelease/ArchiveLengthCountPRParent > 0 and Company/DisplayMode=0">
                                <xsl:call-template name="PressReleases3CurrentPR"/>
                              </xsl:if>
                              <xsl:choose>
                                <xsl:when test="Company/DisplayMode=0">
                                  <xsl:if test="PressRelease/ArchiveLengthCountPRParent &lt; 0 or PressRelease/ArchiveLengthCountPRParent = 0">
                                    <xsl:call-template name="PressReleases3"/>
                                  </xsl:if>
                                </xsl:when>
                                <xsl:otherwise>
                                  <xsl:call-template name="PressReleases3"/>
                                </xsl:otherwise>
                              </xsl:choose>
                            </td>
                          </tr>
                          <tr>
                            <td class="data" valign="top">&#160;</td>
                          </tr>
                        </xsl:if>
                      </xsl:otherwise>
                    </xsl:choose>

                    <xsl:choose>
                      <xsl:when test="Company/TotalNewsArticles=0"></xsl:when>
                      <xsl:otherwise>

                      </xsl:otherwise>
                    </xsl:choose>
                    <tr>
                      <td class="data" valign="top">
                        <xsl:call-template name="SourceTag3"/>
                      </td>
                    </tr>
                  </table>
                </xsl:otherwise>
              </xsl:choose>
            </td>
          </tr>
          <xsl:call-template name="FooterTag3"/>
        </table>
        <xsl:call-template name="Copyright"/>
      </xsl:when>
      <!-- End Template3 -->
      
      <!-- Template Default -->
      <xsl:otherwise>
        <table BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" CLASS="colordark">
          <xsl:call-template name="Title_S1"/>
          <!--<xsl:call-template name="NewsHeader"/>-->
          <tr align="center">
            <td class="colordark" colspan="2">
              <table border="0" cellspacing="0" cellpadding="4" width="100%">
                <xsl:call-template name="NotifyText"/>
                <xsl:choose>
                  <xsl:when test="normalize-space(Company/Year) and (Company/TotalPressReleases + Company/TotalNewsArticles=0)">
                    <!-- If we're on yearly news, check this, otherwise just do it -->
                    <tr class="colorlight default">
                      <td class="colorlight default" colspan="2">
                        <span class="default">There are no documents available for the current year.</span>
                      </td>
                    </tr>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:choose>
                      <xsl:when test="Company/TotalPressReleases=0">
                        <tr class="colorlight default">
                          <td class="colorlight default" colspan="2">There are currently no releases of this type available.</td>
                        </tr>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:if test="Company/DisplayMode=1 or Company/DisplayMode=0 or string-length(Company/DisplayMode)=0">
                          <tr class="colorlight default">
                            <td class="colorlight default" valign="top" colspan="2">
                              <xsl:if test="PressRelease/ArchiveLengthCountPRParent > 0 and Company/DisplayMode=0">
                                <xsl:call-template name="PressReleasesCurrentPR"/>
                              </xsl:if>
                              <xsl:choose>
                                <xsl:when test="Company/DisplayMode=0">
                                  <xsl:if test="PressRelease/ArchiveLengthCountPRParent &lt; 0 or PressRelease/ArchiveLengthCountPRParent = 0">
                                    <xsl:call-template name="PressReleases"/>
                                  </xsl:if>
                                </xsl:when>
                                <xsl:otherwise>
                                  <xsl:call-template name="PressReleases"/>
                                </xsl:otherwise>
                              </xsl:choose>
                            </td>
                          </tr>
                          <tr class="colorlight default">
                            <td class="colorlight default" valign="top" colspan="2">
                              <span class="default">&#160;</span>
                            </td>
                          </tr>
                        </xsl:if>
                      </xsl:otherwise>
                    </xsl:choose>

                    <xsl:choose>
                      <xsl:when test="Company/TotalNewsArticles=0">
                        <!--<tr class="colorlight default"><td colspan="2" class="colorlight default" valign="top">&#160;</td></tr>-->
                      </xsl:when>
                      <xsl:otherwise></xsl:otherwise>
                    </xsl:choose>
                  </xsl:otherwise>
                </xsl:choose>
                <xsl:call-template name="SourceTag"/>
              </table>
            </td>
          </tr>
          <xsl:call-template name="FooterTag"/>
        </table>
        <xsl:call-template name="Copyright"/>
      </xsl:otherwise>
    </xsl:choose>      
      <xsl:call-template name="PRPreview"/>
  </xsl:template>
  
  <!-- News Template Style 4 -->
  <xsl:template name="News_4">
    <xsl:call-template name="PressScriptResource_4"/>
	  <div id="outer">
		  <div id="News_Press">
        <xsl:call-template name="Toolkit_Section_4"/>
			  <div id="PRNews">
				  <xsl:call-template name="Title_Head_4"/>
			    <xsl:call-template name="Header_4"/>
				  <xsl:call-template name="YearHeader_4"/>
				  <xsl:call-template name="PressReleases_4"/>
				  <xsl:call-template name="SourceTag_4"/>
          <xsl:call-template name="Footer_4"/>
          <xsl:call-template name="Copyright_4"/>
			  </div>			
		  </div>		
	  </div>
  </xsl:template>  
  
  <xsl:template name="PressScriptResource_4">
    <xsl:variable name="KeyInstn" select="Company/KeyInstn"/>	
    <xsl:call-template name="CommonScript_4" />
    <xsl:if test="not(contains(translate(Company/URL,$ucletters,$lcletters), 'print=1'))">
      <script language="javascript" type="text/javascript" src="{$ScriptPath}press_temp1.js"></script>
      <xsl:if test="$devPath != ''">
        <script language="javascript" type="text/javascript" src="/Interactive/LookAndFeel/{$KeyInstn}/{$devPath}press_temp1.js"></script>	
      </xsl:if>
      <SCRIPT language="javascript" type="text/javascript">
        <![CDATA[
          var KeyInstn ="]]><xsl:value-of select="$KeyInstn"/><![CDATA[";	
          var devPath = "]]><xsl:value-of select="$devPath"/><![CDATA[";
          var num = "";
          st1=new String(window.location);
          i1=st1.indexOf('&style=');		
          for(j=i1 + 7;j<=i1 + 7;j++) { i3 = st1.charAt(j); num = num + i3; }		
          if(num == '/' || num == ''){ num = ''; }
          if(num == '1'){
            document.write('<link href="/Interactive/LookAndFeel/'+KeyInstn+'/'+devPath+'news_c.css" rel="stylesheet" type="text/css" />');
            document.write('<script language="javascript" type="text/javascript" src="/Interactive/LookAndFeel/'+KeyInstn+'/'+devPath+'news_c.js"><\/script>');         
          } else if(num == '0'){
            document.write('<link href="/Interactive/LookAndFeel/'+KeyInstn+'/'+devPath+'news_ir.css" rel="stylesheet" type="text/css" />');
            document.write('<script language="javascript" type="text/javascript" src="/Interactive/LookAndFeel/'+KeyInstn+'/'+devPath+'news_ir.js"><\/script>');
          }
        ]]>
      </SCRIPT>
     </xsl:if>
  </xsl:template>
  
  <xsl:template name="PressReleases_4">
	<div id="headline">
		<div class="headline">
			<xsl:choose>
			<xsl:when test="Company/TotalPressReleases=0"></xsl:when>
			<xsl:otherwise>
				<xsl:if test="Company/DisplayMode=1 or Company/DisplayMode=0 or string-length(Company/DisplayMode)=0">
					<table width="100%" border="0" cellspacing="0" cellpadding="0" class="head_table" id="PRTable">
					<tr>
						<th class="PRdate"><span class="bold">Date</span></th>
						<th class="PRHeadline"><span class="bold">Headline</span></th>
						<th class="PRFormat"><span class="bold">Format</span></th>
					</tr>
					<xsl:for-each select="PressRelease">
					<tr>
						<td valign="top" align="left"><xsl:value-of select="Date" disable-output-escaping="yes"/></td>
						<td valign="top" align="left">							
                            <xsl:choose>
                                <xsl:when test="KeyFileFormat = 7 and SuppressPreviewDisplay = 'false'">
                                    <div class="PRShow">
                                    <xsl:if test="not(contains(translate(../Company/URL,$ucletters,$lcletters), 'print=1'))">
                                        <xsl:attribute name="class">PRShow imgClose</xsl:attribute>
                                    </xsl:if>
                                        <a class="PRLink" href="javascript:void(0);" title="{Headline}"><xsl:value-of select="Headline" disable-output-escaping="yes"/></a>
                                    <xsl:if test="KeyFileFormat = 7 and SuppressPreviewDisplay = 'false'">
                                        <img class="PRPreviewLink" src="images/mini-mag.gif" />
                                    </xsl:if>
                                    </div>
                                </xsl:when>
                                <xsl:otherwise>
                                    <a class="PRLink" href="javascript:void(0);" target="_new" title="{Headline}">
                                    <xsl:variable name="url" select="Link" />
                                    <xsl:variable name="isPreview" select="../Company/IsPreview" />							
                                    <xsl:attribute name="href"><xsl:value-of select="$url" /><xsl:if test="contains($isPreview, 'true')"><xsl:if test="contains($url, 'file.aspx')">&amp;preview=1</xsl:if></xsl:if></xsl:attribute>
                                        <xsl:value-of select="Headline" disable-output-escaping="yes"/>
                                    </a>
                                </xsl:otherwise>
                            </xsl:choose>
						</td>
                        <td valign="top" align="center" class="newformat">
                            <ul>
                                <!--<xsl:if test="IconLink != ''">
                                    <li><a target="_new" href="{Link}"><img  border="0" src="{IconImage}" /><br /><xsl:value-of select="IconFormat"/></a></li>
                                </xsl:if>
                                <xsl:if test="IconLink = ''">-->
                                    <li><a target="_new">
                                    <xsl:variable name="url" select="Link" />
                                    <xsl:variable name="isPreview" select="../Company/IsPreview" />							
                                    <xsl:attribute name="href"><xsl:value-of select="$url" /><xsl:if test="contains($isPreview, 'true')"><xsl:if test="contains($url, 'file.aspx')">&amp;preview=1</xsl:if></xsl:if></xsl:attribute><img  border="0" src="{IconImage}" alt="{IconFormat}" /><br /><xsl:value-of select="IconFormat"/></a></li>
                                <!--</xsl:if>-->
                                <xsl:variable select ="KeyDoc" name="prKeyDoc" />
                                <xsl:for-each select="../RelatedDoc">
                                    <xsl:if test="$prKeyDoc = KeyDocPR">                
                                        <xsl:choose>
                                            <xsl:when test="WebSiteURL != ''">
                                                <li>
                                                    <a href="{WebSiteURL}" target="_new" title="{Headline}" alt="{Headline}">
                                                        <img  BORDER="0" SRC="{IconPath}" alt="" />
                                                    </a>
                                                </li> 
                                            </xsl:when>                  
                                            <xsl:otherwise>
                                                <li> 
                                                    <a target="_new" title="{Headline}">
                                                    <xsl:attribute name="href">file.aspx?IID=<xsl:value-of select="../Company/KeyInstn"/>&amp;FID=<xsl:value-of select='KeyFile'/></xsl:attribute>
                                                       
                                                        <img  border="0" src="{IconPath}" title="{Headline}" alt="{Headline}" />
                                                    </a>
                                                </li> 
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:if>
                                </xsl:for-each>
                            </ul>
                        </td> 
					</tr>
                    <xsl:if test="KeyFileFormat = 7 and SuppressPreviewDisplay = 'false'">
                    <tr class="FilePR" style="display: none">
                        <td class="datashade" valign="top"></td>
						<td class="datashade" colspan="2" valign="top">
                            <div class="ShowPRFile">
                            <xsl:variable name="url" select="Link" />
                            <xsl:variable name="isPreview" select="../Company/IsPreview" />	
                                <xsl:attribute name="lnk"><xsl:value-of select="$url" /><xsl:if test="contains($isPreview, 'true')"><xsl:if test="contains($url, 'file.aspx')">&amp;preview=1</xsl:if></xsl:if></xsl:attribute>
                                <span class="PRFileShow"></span>
                                <a class="LinkPRPreview" target="_blank" title="Continue Reading">                                
                                <xsl:attribute name="href"><xsl:value-of select="$url" /><xsl:if test="contains($isPreview, 'true')"><xsl:if test="contains($url, 'file.aspx')">&amp;preview=1</xsl:if></xsl:if></xsl:attribute>
                                Continue Reading</a>
                            </div>
                        </td>
					</tr>
                    </xsl:if>
					</xsl:for-each>
					<xsl:if test="PressRelease/IconLink != ''">
					<tr>
						<td colspan="3"><div class="AdobeAcrobat">Click the link to the right to download: <a target="_new" href="http://get.adobe.com/reader/" title="Download Adobe Acrobat"><xsl:value-of select="IconFormat"/> Adobe Acrobat</a></div></td>
					</tr>
					</xsl:if>
					</table>
				</xsl:if>
			</xsl:otherwise>			
			</xsl:choose>
		</div>
	</div>  
  </xsl:template>
  
  <xsl:template name="MoreLink_4">
  <div id="MoreLink">
	<xsl:choose>
		<xsl:when  test="Company/DisplayMode=0">
			<a>			
			<xsl:variable name="keyinstn" select="Company/KeyInstn" />
			<!--<xsl:variable name="transType" select="translate(AvailableTypes[Selected != '']/TypeValue, '&amp;', '%26')"></xsl:variable>-->
			<xsl:variable name="transType">
			  <xsl:call-template name="string-replace">
				<xsl:with-param name="from" select="'&amp;'"/>
				<xsl:with-param name="to" select="'%26'"/>
				<xsl:with-param name="string" select="AvailableTypes[Selected != '']/TypeValue"/>
			  </xsl:call-template>
			</xsl:variable>

			<xsl:variable name="linkGuts" select="concat($keyinstn, '&amp;mode=1&amp;type=', $transType)"/>
			<!--<xsl:variable name="linkGuts" select="concat($keyinstn, '&amp;mode=1&amp;type=', AvailableTypes[Selected != '']/TypeValue)"/>-->
			<xsl:variable name="linkGutsPlusYear">
			  <xsl:if test="Company/YearAll = 'true'"><xsl:value-of select="$linkGuts"/></xsl:if>
			  <xsl:if test="Company/YearAll = 'false'"><xsl:value-of select="concat($linkGuts, '&amp;Year=', Company/Year)"/></xsl:if>
			</xsl:variable>
			<xsl:variable name="pressurl" select="concat('News.aspx?IID=', $linkGutsPlusYear)" />
			<xsl:variable name="yearlypressurl" select="concat('YearlyNews.aspx?IID=', $linkGutsPlusYear)" />

			<xsl:if test="Company/KeyPage = 201855">
			  <xsl:attribute name="href"><xsl:value-of select="$yearlypressurl" /></xsl:attribute>
			</xsl:if>

			<xsl:if test="Company/KeyPage = 175">
			  <xsl:attribute name="href"><xsl:value-of select="$pressurl" /></xsl:attribute>
			</xsl:if>More Press Releases...
			</a>
		</xsl:when>
	</xsl:choose>
  </div>
  </xsl:template>
  
  <xsl:template name="YearHeader_4">
	<div id="Year_Header">
		<form name="MaskForm" id="MaskForm"  method="Post">
			<fieldset>
				<legend>Maskform</legend>
				<xsl:variable name="keyinstn" select="Company/KeyInstn" />
				<div class="DisplayNews">
					<xsl:attribute name="action">
						<xsl:value-of select="concat('YearlyNews.aspx?IID=', $keyinstn, '&amp;mode=1')"/>
					</xsl:attribute>
					<xsl:if test="PageParameters/SuppressTypeDisplay = 'false'">
						<span class="YearNewstxt">Display: </span>
						<label class="visuallyhidden" for="TypeMask">Display</label>
						<select class="activeselectbox" name="TypeMask" id="TypeMask" onchange="document.getElementById('MaskForm').submit();">
							<xsl:for-each select="AvailableTypes">
								<option value="{TypeValue}">
									<xsl:if test="Selected != ''">
										<xsl:attribute name="selected">
											<xsl:value-of select="Selected"/>
										</xsl:attribute>
									</xsl:if>
									<xsl:value-of select="TypeText"/>
								</option>
							</xsl:for-each>
						</select>
					</xsl:if>
				</div>
				<div class="YearNews">
					<xsl:if test="PageParameters/SuppressDateDisplay = 'false'">
						<xsl:if test="count(AvailableYears/Year) &gt; 0">
							<xsl:attribute name="action">
								<xsl:value-of select="concat('YearlyNews.aspx?IID=', $keyinstn, '&amp;mode=1')"/>
							</xsl:attribute>
							<span class="YearNewstxt">Year: </span>
							<label class="visuallyhidden" for="YearMask">Year</label>
							<select class="activeselectbox" name="YearMask" id="YearMask" onchange="document.getElementById('MaskForm').submit();">
								<xsl:for-each select="AvailableYears">
									<option value="{Year}">
										<xsl:if test="Year = 'All Years'">
											<xsl:attribute name="value">0</xsl:attribute>
										</xsl:if>
										<xsl:if test="Selected != ''">
											<xsl:attribute name="selected">
												<xsl:value-of select="Selected"/>
											</xsl:attribute>
										</xsl:if>
										<xsl:value-of select="Year"/>
									</option>
								</xsl:for-each>
							</select>
						</xsl:if>
					</xsl:if>
				</div>
			</fieldset>
		</form>
	<xsl:call-template name="MoreLink_4"/>
	</div>
  </xsl:template>

  <xsl:template name="SourceTag_4">
	<div id="news_Foot">		
		<div class="news_Foot">Source: S&amp;P Global Market Intelligence</div>			  
		<xsl:choose>
		<xsl:when test="Company/TotalNewsArticles &gt; Company/RowsPerPage or Company/TotalPressReleases &gt; Company/RowsPerPage">
			<xsl:variable name="EndPage" select="Company/Start + Company/RowsPerPage - 1"/>
			<div class="news_Pagination_left">
            <xsl:if test="Company/DisplayMode = 1">
              Rows <span class="pagevalue"><xsl:value-of select="Company/Start"/></span> through
              <span class="pagevalue"><xsl:choose>
                <xsl:when test="$EndPage &gt; Company/TotalPressReleases">
                  <xsl:value-of select="Company/TotalPressReleases"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="$EndPage"/>
                </xsl:otherwise>
              </xsl:choose></span>
              of <span class="pagevalue"><xsl:value-of select="Company/TotalPressReleases"/></span>
            </xsl:if>

            <xsl:if test="Company/DisplayMode = 2">
              Rows <xsl:value-of select="Company/Start"/> through
              <xsl:choose>
                <xsl:when test="$EndPage &gt; Company/TotalNewsArticles">
                  <xsl:value-of select="Company/TotalNewsArticles"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="$EndPage"/>
                </xsl:otherwise>
              </xsl:choose>
              of <xsl:value-of select="Company/TotalNewsArticles"/>
            </xsl:if>
			</div>
			<div class="news_Pagination_right">				
				<div class="PRPage">
					<ul class="pages">
					<xsl:for-each select="Pagination">
					<xsl:choose>
					<xsl:when test="CurrentPage = 'false'">
						<li><a target="_self" href="{PageLink}"><xsl:value-of select="PageLabel"/></a></li>
					</xsl:when>
					<xsl:otherwise>						
						<li class="select"><xsl:value-of select="PageLabel"/></li>
					</xsl:otherwise>
					</xsl:choose>					
					</xsl:for-each>	
					</ul>
				</div>
			</div>
		  </xsl:when>
		</xsl:choose>	
	</div>
  </xsl:template>
  
  <!-- End News Template Style 4 -->
  
</xsl:stylesheet>