<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns="http://www.w3.ord/TR/REC-html40"
    xmlns:irw="http://www.snl.com/xml/irw">

<xsl:output method="html" media-type="text/html" indent="no"/>

<!-- Default Template -->
<xsl:template name="EmailPopup_Title">
<xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
<TR>
  <TD CLASS="" NOWRAP="NOWRAP" border="0">
    <SPAN CLASS="title1light"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></SPAN>
  </TD>
  <TD CLASS="colordark" NOWRAP="NOWRAP" align="right" valign="top" border="0">
    <span class="subtitlelight"><xsl:value-of select="$CompanyNameCaps"/> (<xsl:value-of select="Company/Exchange"/> - <xsl:value-of select="Company/Ticker"/>)</span>
    <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
  </TD>
</TR>
<!--<xsl:if test="Company/Header != ''">
<TR class="default" align="left"><TD class="colorlight" colspan="2">
<span class="default"><xsl:value-of select="Company/Header" disable-output-escaping="yes"/></span>
</TD></TR>
</xsl:if>-->
</xsl:template>

<xsl:template name="EmailPopup_Form">
<xsl:variable name="sender" select="FormValues/Sender"/>
<xsl:variable name="senderMail" select="FormValues/SenderEmail"/>
<xsl:variable name="recipient" select="FormValues/Recipient"/>
<xsl:variable name="recipientMail" select="FormValues/RecipientEmail"/>
<xsl:variable name="comments" select="FormValues/Comments"/>
<xsl:variable name="error" select="ErrorMessage/Error"/>

<xsl:choose>
<xsl:when test="normalize-space($error)= 'Your email was sent!'">
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tr>
	<td colspan="2" class="data" align="center" height="340">
		<h1 class="title1light emailpopup"><xsl:value-of select="$error" disable-output-escaping="yes"/></h1>
	</td>
</tr>
<tr>
	<td class="data" width="90%">&#160;</td>
	<td height="28" style="cursor:hand;" onclick="Javascript:self.close()" class="header" align="center">
		<a class="emailpopup" href="Javascript:self.close()" title="Close">Close</a>
	</td>
</tr>
</table>
</xsl:when>
<xsl:otherwise>
<table border="0" cellspacing="0" cellpadding="4" width="100%" align="center">
<form method="POST" name="emailForm">
<input type="hidden" name="url"><xsl:attribute name="value"><xsl:value-of select="Company/URLEncodedURL"/></xsl:attribute></input>
<tr>
  <td  class="header"><strong>Email this page: <xsl:value-of select="TitleInfo/TitleName"  disable-output-escaping="yes"/>
	  </strong>
  </td>
</tr>
<tr>
  <td class="data"><span class="Small" style="color:#ff0000;">* required field</span></td>
</tr>
  <xsl:if test="string-length($error) &gt; 0">
	<tr>
	  <td class="data" style="font-weight:bold;">
		<xsl:value-of select="$error" disable-output-escaping="yes"/>
	  </td>
	</tr>
  </xsl:if>
<tr>
  <td class="data">
	<span class="Small" style="color:#ff0000;">*</span> Sender's Name:<br/>
	  <label class="visuallyhidden" for="sender">Sender</label>
	<input type="text" name="sender" size="25" id="sender">
	<xsl:attribute name="value"><xsl:value-of select="$sender"/></xsl:attribute>
	</input>
  </td>
</tr>
<tr>
  <td class="data">
	<span class="Small" style="color:#ff0000;">*</span> Sender's Email Address:<br/>
	  <label class="visuallyhidden" for="smail">Sender Email</label>
	  <input type="text" name="smail" size="25" id="smail">
	<xsl:attribute name="value"><xsl:value-of select="$senderMail"/></xsl:attribute>
	</input>
  </td>
</tr>
<tr>
  <td class="data">
	<span class="Small" style="color:#ff0000;">*</span> Recipient's Name:<br/>
	  <label class="visuallyhidden" for="recipient">Recipient Name</label>
	<input type="text" name="recipient" size="25" id="recipient">
	<xsl:attribute name="value"><xsl:value-of select="$recipient"/></xsl:attribute>
	</input>
  </td>
</tr>
<tr>
  <td class="data">
	<span class="Small" style="color:#ff0000;">*</span> Recipient's Email Address:<br/>
	  <label class="visuallyhidden" for="rmail">Recipient Email</label>
	<input type="text" name="rmail" size="25" id="rmail">
	<xsl:attribute name="value"><xsl:value-of select="$recipientMail"/></xsl:attribute>
	</input>
  </td>
</tr>
<tr>
  <td class="data">
	Enter your comments here:<br/>
	<textarea name="comments" cols="30" rows="3" class="data"><xsl:value-of select="$comments"/></textarea>
  </td>
</tr>
<tr>
  <td class="data">
    <span class="Small" style="color:#ff0000;">*</span> Please enter the text shown in the image below:<br/>
    <img alt="Captcha Code">
      <xsl:attribute name="src">
        <xsl:value-of select="Captcha/Src"/>
      </xsl:attribute>
    </img><br />
	  <label class="visuallyhidden" for="captcha">captcha</label>
    <input type="text" name="captcha" size="25" id="captcha"></input>
    <input type="hidden" name="captchaSrc">
      <xsl:attribute name="value">
        <xsl:value-of select="Captcha/Src"/>
      </xsl:attribute>
    </input>
  </td>
</tr>
<tr><td class="data"><input type="submit" value="Send"/></td></tr>
</form>
</table>
</xsl:otherwise>
</xsl:choose>

</xsl:template>

<xsl:template name="FooterTag">
<tr class="colorlight">
  <td colspan="2">
    <span class="large"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span>
  </td>
</tr>
</xsl:template>

<!-- Default Template -->


<!-- Template 1 -->
<xsl:template name="EmailPopup_Title2">
<xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
<TR>
  <TD CLASS="titletest" nowrap="" align="right" valign="top"><!--<xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/><br /><IMG SRC="" WIDTH="2" HEIGHT="1" />--><span class="titletest2"><xsl:value-of select="$CompanyNameCaps"/>&#160;(<xsl:value-of select="Company/Exchange"/>&#160;-&#160;<xsl:value-of select="Company/Ticker"/>)&#160;</span></TD>
</TR>
<!--<xsl:if test="Company/Header != ''">
<TR class="default" align="left">
  <TD class="colorlight"><span class="default"><xsl:value-of select="Company/Header" disable-output-escaping="yes"/></span></TD>
</TR>
</xsl:if>-->
</xsl:template>

<xsl:template name="EmailPopup_Form2">
<xsl:variable name="sender" select="FormValues/Sender"/>
<xsl:variable name="senderMail" select="FormValues/SenderEmail"/>
<xsl:variable name="recipient" select="FormValues/Recipient"/>
<xsl:variable name="recipientMail" select="FormValues/RecipientEmail"/>
<xsl:variable name="comments" select="FormValues/Comments"/>
<xsl:variable name="error" select="ErrorMessage/Error"/>

<xsl:choose>
<xsl:when test="normalize-space($error)= 'Your email was sent!'">
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tr>
	<td colspan="2" class="data" align="center" height="339">
		<h1 class="title1light emailpopup"><xsl:value-of select="$error" disable-output-escaping="yes"/></h1>
	</td>
</tr>
<tr>
	<td class="data" width="90%">&#160;</td>
	<td height="28" style="cursor:hand;" onclick="Javascript:self.close()" class="datashade defaultbold surr" align="center">
		<a class="emailpopup" href="Javascript:self.close()" title="Close">Close</a>
	</td>
</tr>
</table>
</xsl:when>
<xsl:otherwise>
<table border="0" cellspacing="0" cellpadding="4" width="100%" align="center">
<form method="POST" name="emailForm">
<input type="hidden" name="url"><xsl:attribute name="value"><xsl:value-of select="Company/URLEncodedURL"/></xsl:attribute></input>
	<tr>
		<td class="defaultbold">
			<strong>Email this page: <xsl:value-of select="TitleInfo/TitleName"  disable-output-escaping="yes"/>
		</strong>
	</td>
	</tr>
	<tr>
		<td class="data"><span class="Small" style="color:#ff0000;">* required field</span></td>
	</tr>
	<xsl:if test="string-length($error) &gt; 0">
	<tr>
		<td class="data" style="font-weight:bold;">
			<xsl:value-of select="$error" disable-output-escaping="yes"/>
		</td>
	</tr>
	</xsl:if>
	<tr>
		<td class="data">
			<span class="Small" style="color:#ff0000;">*</span> Sender's Name:<br/>
			<label class="visuallyhidden" for="sender">Sender</label>
			<input type="text" name="sender" size="25" id="sender" class="surr datashade">
			<xsl:attribute name="value"><xsl:value-of select="$sender"/></xsl:attribute>
			</input>
		</td>
	</tr>
	<tr>
		<td class="data">
			<span class="Small" style="color:#ff0000;">*</span> Sender's Email Address:<br/>
			<label class="visuallyhidden" for="smail">Sender Email</label>
			<input type="text" name="smail" size="25" class="surr datashade">
			<xsl:attribute name="value"><xsl:value-of select="$senderMail"/></xsl:attribute>
			</input>
		</td>
	</tr>
	<tr>
		<td class="data">
			<span class="Small" style="color:#ff0000;">*</span> Recipient's Name:<br/>
			<label class="visuallyhidden" for="recipient">recipient Name</label>
			<input type="text" name="recipient" id="recipient" size="25" class="surr datashade">
			<xsl:attribute name="value"><xsl:value-of select="$recipient"/></xsl:attribute>
			</input>
		</td>
	</tr>
	<tr>
		<td class="data">
			<span class="Small" style="color:#ff0000;">*</span> Recipient's Email Address:<br/>
			<label class="visuallyhidden" for="rmail">recipient email</label>
			<input type="text" name="rmail" size="25" id="rmail" class="surr datashade">
			<xsl:attribute name="value"><xsl:value-of select="$recipientMail"/></xsl:attribute>
			</input>
		</td>
	</tr>
	<tr>
		<td class="data">
			Enter your comments here:<br/>
			<label class="visuallyhidden" for="comments">comments</label>
			<textarea name="comments" id="comments" cols="30" rows="3" class="surr datashade"><xsl:value-of select="$comments"/></textarea>
		</td>
	</tr>
  <tr>
    <td class="data">
      <span class="Small" style="color:#ff0000;">*</span> Please enter the text shown in the image below:<br/>
      <img alt="Captcha Code">
        <xsl:attribute name="src">
          <xsl:value-of select="Captcha/Src"/>
        </xsl:attribute>
      </img><br />
		<label class="visuallyhidden" for="captcha">captcha Code</label>
      <input type="text" name="captcha" id="captcha" size="25"></input>
      <input type="hidden" name="captchaSrc">
        <xsl:attribute name="value">
          <xsl:value-of select="Captcha/Src"/>
        </xsl:attribute>
      </input>
    </td>
  </tr>
	<tr><td class="data"><input type="submit" value="Send" class="surr datashade"/></td></tr>
	</form>
	</table>
</xsl:otherwise>
</xsl:choose>
</xsl:template>


<xsl:template name="FooterTag2">
<tr class="colorlight">
  <td colspan="2">
    <span class="large"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span>
  </td>
</tr>
</xsl:template>

<!-- Template 1 -->

<!-- Template 3 -->
<xsl:template name="EmailPopup_Form3">
<xsl:variable name="sender" select="FormValues/Sender"/>
<xsl:variable name="senderMail" select="FormValues/SenderEmail"/>
<xsl:variable name="recipient" select="FormValues/Recipient"/>
<xsl:variable name="recipientMail" select="FormValues/RecipientEmail"/>
<xsl:variable name="comments" select="FormValues/Comments"/>
<xsl:variable name="error" select="ErrorMessage/Error"/>

<xsl:choose>
<xsl:when test="normalize-space($error) = 'Your email was sent!'">
<table border="0" cellspacing="0" cellpadding="0" width="100%" class="table2">
<tr>
	<td colspan="2" class="data" align="center" height="340">
		<h1 class="title1light emailpopup"><xsl:value-of select="$error" disable-output-escaping="yes"/></h1>
	</td>
</tr>
<tr>
	<td class="data" width="90%">&#160;</td>
	<td height="28" style="cursor:hand;" onclick="Javascript:self.close()" class="divider_left report_top datashade defaultbold" align="center">
		<a class="emailpopup" href="Javascript:self.close()" title="Close">Close</a>
	</td>
</tr>
</table>
</xsl:when>
<xsl:otherwise>
<table border="0" cellspacing="0" cellpadding="4" width="100%" align="center" class="table2">
<form method="POST" name="emailForm">
<input type="hidden" name="url"><xsl:attribute name="value"><xsl:value-of select="Company/URLEncodedURL"/></xsl:attribute></input>
<tr>
  <td  CLASS="datashade defaultbold table1_item"><strong>Email this page: <xsl:value-of select="TitleInfo/TitleName"  disable-output-escaping="yes"/>
	  </strong>
  </td>
</tr>
<tr>
  <td class="data"><span class="Small" style="color:#ff0000;">* required field</span></td>
</tr>
  <xsl:if test="string-length($error) &gt; 0">
    <tr>
      <td class="data" style="font-weight:bold;">
        <xsl:value-of select="$error" disable-output-escaping="yes"/>
      </td>
    </tr>
  </xsl:if>
<tr>
  <td class="data">
    <span class="Small" style="color:#ff0000;">*</span> Sender's Name:<br/>
	  <label class="visuallyhidden" for="sender">sender Name</label>
    <input type="text" name="sender" size="25" id="sender" class="table2 data">
    <xsl:attribute name="value"><xsl:value-of select="$sender"/></xsl:attribute>
    </input>
  </td>
</tr>
<tr>
  <td class="data">
    <span class="Small" style="color:#ff0000;">*</span> Sender's Email Address:<br/>
		<label class="visuallyhidden" for="smail">Sender Email</label>
    <input type="text" name="smail" size="25" class="table2 data">
    <xsl:attribute name="value"><xsl:value-of select="$senderMail"/></xsl:attribute>
    </input>
  </td>
</tr>
<tr>
  <td class="data">
    <span class="Small" style="color:#ff0000;">*</span> Recipient's Name:<br/>
	  <label class="visuallyhidden" for="recipient">recipient Name</label>
    <input type="text" name="recipient" size="25" class="table2 data">
    <xsl:attribute name="value"><xsl:value-of select="$recipient"/></xsl:attribute>
    </input>
  </td>
</tr>
<tr>
  <td class="data">
    <span class="Small" style="color:#ff0000;">*</span> Recipient's Email Address:<br/>
	  <label class="visuallyhidden" for="rmail">Recipient Email</label>
    <input type="text" name="rmail" size="25" class="table2 data">
    <xsl:attribute name="value"><xsl:value-of select="$recipientMail"/></xsl:attribute>
    </input>
  </td>
</tr>
<tr>
  <td class="data">
    Enter your comments here:<br/>
	  <label class="visuallyhidden" for="comments">comments</label>
    <textarea name="comments" cols="30" rows="3" id="comments" class="table2 data"><xsl:value-of select="$comments"/></textarea>
  </td>
</tr>
  <tr>
    <td class="data">
      <span class="Small" style="color:#ff0000;">*</span> Please enter the text shown in the image below:<br/>
      <img alt="Captcha Code">
        <xsl:attribute name="src">
          <xsl:value-of select="Captcha/Src"/>
        </xsl:attribute>
      </img><br />
		<label class="visuallyhidden" for="captcha">captcha code</label>
      <input type="text" name="captcha" size="25" id="captcha"></input>
      <input type="hidden" name="captchaSrc">
        <xsl:attribute name="value">
          <xsl:value-of select="Captcha/Src"/>
        </xsl:attribute>
      </input>
    </td>
  </tr>
<tr><td class="data"><input type="submit" value="Send"  class="table2 data"/></td></tr>
</form>
</table>
</xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template name="FooterTag3">
<tr>
  <td class="data">
    <span class="large"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span>
  </td>
</tr>
</xsl:template>

<!-- Template 3 -->



<xsl:template match="EmailPopup">
<HTML>
<head>
<title>Email This Page</title>

<xsl:variable name="KeyInstn" select="Company/KeyInstn"/>
<!--<xsl:variable name="error" select="ErrorMessage/Error"/>-->
<xsl:variable name="anchortag1" select="concat('/interactive/lookandfeel/', $KeyInstn, '/style.css')"/>
<xsl:variable name="anchortag2" select="concat('/Interactive/LookAndFeel/IRStyles/live_', $KeyInstn, '.css')"/>
<xsl:variable name="anchortag3" select="concat('/Interactive/LookAndFeel/IRStyles/live_', $KeyInstn, '_supl.css')"/>
<link REL="stylesheet" TYPE="text/css">
  <xsl:attribute name="href"><xsl:value-of select="$anchortag1"/></xsl:attribute>
</link>

<link REL="stylesheet" TYPE="text/css">
  <xsl:attribute name="href"><xsl:value-of select="$anchortag2"/></xsl:attribute>
</link>

<link REL="stylesheet" TYPE="text/css">
  <xsl:attribute name="href"><xsl:value-of select="$anchortag3"/></xsl:attribute>
</link>

<style type="text/css">
.emailpopup{
	FONT-SIZE: 18px;
	FONT-WEIGHT: normal;
}
a.emailpopup, a.emailpopup:link, a.emailpopup:visited, a.emailpopup:hover {
	color: #000000;
	font-family: arial, verdana, helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	text-decoration: none;
}
body{
 background:none;
 margin:0px;
}
</style>

</head>
<body class="data">
<xsl:variable name="TemplateName" select="Company/TemplateName"/>

<xsl:choose>
<!--<xsl:when test="$TemplateName = 'AltEmails1'">-->
<xsl:when test="contains(Company/TemplateName, '1')">
<!-- Template 1 -->
<xsl:call-template name="TemplateONEstylesheet"/>
<table BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%">

<xsl:call-template name="EmailPopup_Title2"/>

<TR>
  <TD ALIGN="left" VALIGN="top" CLASS="data">
    <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%">
    <TR>
      <TD ALIGN="left" VALIGN="top" CLASS="leftTOPbord">
          <xsl:call-template name="EmailPopup_Form2"/>
      </TD>
    </TR>
    </TABLE>
  </TD>
</TR>
<!--<xsl:if test="Company/Footer != ''">
  <xsl:call-template name="FooterTag2"/>
</xsl:if>-->

</table>
<!-- Template 1 -->
</xsl:when>

<!--<xsl:when test="$TemplateName = 'AltEmails3'">-->
<xsl:when test="contains(Company/TemplateName, '3')">
<!-- Template 3 -->
<xsl:call-template name="TemplateTHREEstylesheet"/>
<table BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%">
<!--<xsl:call-template name="EmailPopup_Title3"/>-->
<TR>
  <TD ALIGN="left" VALIGN="top" class="data">
    <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%">
    <TR>
      <TD ALIGN="left" VALIGN="top">
        <xsl:call-template name="EmailPopup_Form3"/>
      </TD>
    </TR>
    </TABLE>
  </TD>
</TR>
<!--<xsl:if test="Company/Footer != ''">
  <xsl:call-template name="FooterTag3"/>
</xsl:if>-->
</table>
<!-- Template 3 -->
</xsl:when>

<xsl:otherwise>
<style type="text/css">
a.emailpopup, a.emailpopup:link, a.emailpopup:visited, a.emailpopup:hover {
	color: #FFFFFF;
}
</style>
<!-- Default Template -->
<table BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" CLASS="">
  <!-- <xsl:call-template name="EmailPopup_Title"/> -->

<TR ALIGN="CENTER">
  <TD CLASS="" colspan="2">
    <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" ALIGN="center">
    <TR ALIGN="CENTER">
      <TD CLASS="colorlight">
          <xsl:call-template name="EmailPopup_Form"/>
      </TD>
    </TR>
    </TABLE>
  </TD>
</TR>
<!--<xsl:if test="Company/Footer != ''">
  <xsl:call-template name="FooterTag"/>
</xsl:if>-->
</table>
<!-- Default Template -->
</xsl:otherwise>
</xsl:choose>
</body>
</HTML>
</xsl:template>

</xsl:stylesheet>
