<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.ord/TR/REC-html40"
	xmlns:irw="http://www.snl.com/xml/irw">
	
<xsl:import href="Copyright.xslt"/>
<xsl:output method="html" media-type="text/html" indent="no"/>

<!--start xml break Email_Title -->

<xsl:template name="Email_es_Title">
<xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
	<TR>
		<TD CLASS="colordark" NOWRAP="NOWRAP">
			<SPAN CLASS="title1light">Email Notification</SPAN> 
	    </TD>
		<TD CLASS="colordark" NOWRAP="NOWRAP" align="right" valign="top"><span class="subtitlelight"><xsl:value-of select="$CompanyNameCaps"/> (<xsl:value-of select="Company/Exchange"/> - <xsl:value-of select="Company/Ticker"/>)</span><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></TD>
	</TR>
		<TR class="default" align="left">
			<TD class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Header" disable-output-escaping="yes"/></span></TD>
	</TR>
</xsl:template>

<!--start xml break EmailUpd_Data -->

<xsl:template name="EmailUpd_es_Data">
  <TR ALIGN="CENTER"> 
    <TD CLASS="colordark" colspan="2"> 
      <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="4" WIDTH="100%"> 
		<TR ALIGN="CENTER">
			<TD CLASS="colorlight">
				<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="4" WIDTH="100%"> 
				<xsl:variable name="KeyInstn" select="Company/KeyInstn"/>
				<xsl:variable name="FormAction" select="concat('emailupd_es.aspx?IID=', $KeyInstn)"></xsl:variable>
				<FORM method="POST" name="emailupd_esform"><xsl:attribute name="action"><xsl:value-of select="$FormAction"/></xsl:attribute>
					<fieldset>
						<legend>Email upload</legend>
					
					<INPUT TYPE="hidden" NAME="KeyMailingList"><xsl:attribute name="value"><xsl:value-of select="EmailUpdInfo/lKeyMailingList"/></xsl:attribute></INPUT>
				<xsl:if test="EmailUpdInfo/FormAction = 'ObjectError'">
					<TR>
						<TD CLASS="colorlight"><SPAN CLASS="datared">We apologize, but this feature is currently unavailable.  Please attempt your request at a later time.</SPAN></TD>
					</TR>
				</xsl:if>
				<xsl:if test="(EmailUpdInfo/FormAction = 'Success') or (EmailUpdInfo/FormAction = 'Cleared')">
					<TR>
					<TD CLASS="colorlight"><SPAN CLASS="defaultbold">Thank you!  Your changes have processed successfully.</SPAN></TD>
					</TR>				
				</xsl:if>
				<xsl:if test="EmailUpdInfo/Err != ''">
					<TR>
					<TD CLASS="colorlight"><SPAN CLASS="datared">Our records indicate that you already have email notification set-up.</SPAN></TD>
					</TR>
				</xsl:if>
				<TR>
					<TD CLASS="colorlight"><SPAN CLASS="default">The following checked items indicate your current preferences.  You may select new notification options or remove previous selections using this form.  Be sure to click the Update button upon completing your changes.</SPAN> </TD>
				</TR>
				<TR>
				 <TD>
					<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="4">
					<TR>
						<TD COLSPAN="3" CLASS="header">Events Calendar</TD>
					</TR>
					<TR>
						<TD CLASS="data" ALIGN="right" WIDTH="11%">
						<!-- Corporate Event Notifications -->
						<xsl:variable name="CorpEvent_Value" select="EmailUpdInfo/CorpEvent"/>
						<!-- *************************************************-->
						<xsl:choose>
						<xsl:when test="(EmailUpdInfo/FormAction = 'MakeUpdate') and (EmailUpdInfo/bCorpEvent = 'true')">
							<label class="visuallyhidden" for="events">events</label>
							<INPUT TYPE="checkbox" id="events" NAME="Events" CHECKED="Y">
								<xsl:attribute name="value"><xsl:value-of select="$CorpEvent_Value"/></xsl:attribute>
							</INPUT>														
						</xsl:when>
						<xsl:when test="(EmailUpdInfo/FormAction = 'Success') and (EmailUpdInfo/bCorpEventAdded = 'true')">
							<label class="visuallyhidden" for="events">events</label>
							<INPUT TYPE="checkbox" id="events" NAME="Events" CHECKED="Y">
								<xsl:attribute name="value"><xsl:value-of select="$CorpEvent_Value"/></xsl:attribute>
							</INPUT>														
						</xsl:when>						
						<xsl:otherwise>
							<label class="visuallyhidden" for="events">events</label>
							<INPUT TYPE="checkbox" id="events" NAME="Events">
								<xsl:attribute name="value"><xsl:value-of select="$CorpEvent_Value"/></xsl:attribute>
							</INPUT>													
						</xsl:otherwise>
						</xsl:choose>
						<!-- *************************************************-->												
						</TD>
						<TD CLASS="data" COLSPAN="2">Notify me of any new events added to the events calendar</TD>
					</TR>				
					<TR>
						<TD CLASS="data" ALIGN="right" WIDTH="11%">
						<!-- Corporate Event Reminders -->
						<xsl:variable name="CorpEventReminder_Value" select="EmailUpdInfo/CorpEventReminder"/>
						<!-- *************************************************-->
						<xsl:choose>
						<xsl:when test="(EmailUpdInfo/FormAction = 'MakeUpdate') and (EmailUpdInfo/bCorpEventReminder = 'true')">
							<label class="visuallyhidden" for="eventreminder">eventreminder</label>
							<INPUT TYPE="checkbox" NAME="EventReminder" CHECKED="Y" id="eventreminder">
								<xsl:attribute name="value"><xsl:value-of select="$CorpEventReminder_Value"/></xsl:attribute>
							</INPUT>														
						</xsl:when>
						<xsl:when test="(EmailUpdInfo/FormAction = 'Success') and (EmailUpdInfo/bCorpEventReminderAdded = 'true')">
							<label class="visuallyhidden" for="eventreminder">eventreminder</label>
							<INPUT TYPE="checkbox" NAME="EventReminder" CHECKED="Y" id="eventreminder">
								<xsl:attribute name="value"><xsl:value-of select="$CorpEventReminder_Value"/></xsl:attribute>
							</INPUT>														
						</xsl:when>						
						<xsl:otherwise>
							<label class="visuallyhidden" for="eventreminder">eventreminder</label>
							<INPUT TYPE="checkbox" NAME="EventReminder" id="eventreminder">
								<xsl:attribute name="value"><xsl:value-of select="$CorpEventReminder_Value"/></xsl:attribute>
							</INPUT>													
						</xsl:otherwise>
						</xsl:choose>
						<!-- *************************************************-->																		
						</TD>
						<TD CLASS="data" COLSPAN="2">Remind me two business days before any event</TD>
					</TR>	
					<TR>
						<TD CLASS="colorlight" COLSPAN="3">
							<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
						</TD>
					</TR>	
					<TR>
						<TD COLSPAN="3" CLASS="header">Documents</TD>
					</TR>	
					<!-- To be Hardcoded below-->					
					<TR>
						<TD CLASS="data" ALIGN="right" WIDTH="11%">
						<!-- Corporate Document Notifications -->
						<xsl:variable name="CorpDocument_Value" select="EmailUpdInfo/CorpDocument"/>
						<!-- *************************************************-->
						<xsl:choose>
						<xsl:when test="(EmailUpdInfo/FormAction = 'MakeUpdate') and (EmailUpdInfo/bCorpDocument = 'true')">
							<label class="visuallyhidden" for="Docs">Docs</label>
							<INPUT TYPE="checkbox" NAME="Docs" CHECKED="Y" id="Docs">
								<xsl:attribute name="value"><xsl:value-of select="$CorpDocument_Value"/></xsl:attribute>
							</INPUT>														
						</xsl:when>
						<xsl:when test="(EmailUpdInfo/FormAction = 'Success') and (EmailUpdInfo/bCorpDocumentAdded = 'true')">
							<label class="visuallyhidden" for="Docs">Docs</label>
							<INPUT TYPE="checkbox" NAME="Docs" CHECKED="Y" id="Docs">
								<xsl:attribute name="value"><xsl:value-of select="$CorpDocument_Value"/></xsl:attribute>
							</INPUT>														
						</xsl:when>						
						<xsl:otherwise>
							<label class="visuallyhidden" for="Docs">Docs</label>
							<INPUT TYPE="checkbox" NAME="Docs" id="Docs">
								<xsl:attribute name="value"><xsl:value-of select="$CorpDocument_Value"/></xsl:attribute>
							</INPUT>													
						</xsl:otherwise>
						</xsl:choose>
						<!-- *************************************************-->						
						</TD>
						<TD CLASS="data" COLSPAN="2">Notify me of any new company documents (Regulatory Filings and other Financial Documents)</TD>
					</TR>						
					<TR>
						<TD CLASS="data" ALIGN="right" WIDTH="11%">
						<!-- Corporate Document Notifications, excluding insider docs -->
						<xsl:variable name="EIT_Value" select="EmailUpdInfo/ExcludeInsiderTradingDocs"/>
						<!-- *************************************************-->
						<xsl:choose>
						<xsl:when test="(EmailUpdInfo/FormAction = 'MakeUpdate') and (EmailUpdInfo/bExcludeInsiderTradingDocs = 'true')">
							<label class="visuallyhidden" for="docsexcluding">docsexcluding</label>
							<INPUT TYPE="checkbox" NAME="DocsExcludingInsiderTrading" CHECKED="Y" id="docsexcluding">
								<xsl:attribute name="value"><xsl:value-of select="$EIT_Value"/></xsl:attribute>
							</INPUT>														
						</xsl:when>
						<xsl:when test="(EmailUpdInfo/FormAction = 'Success') and (EmailUpdInfo/bExcludeInsiderTradingDocsAdded = 'true')">
							<label class="visuallyhidden" for="docsexcluding">docsexcluding</label>
							<INPUT TYPE="checkbox" NAME="DocsExcludingInsiderTrading" CHECKED="Y" id="docsexcluding">
								<xsl:attribute name="value"><xsl:value-of select="$EIT_Value"/></xsl:attribute>
							</INPUT>														
						</xsl:when>						
						<xsl:otherwise>
							<label class="visuallyhidden" for="docsexcluding">docsexcluding</label>
							<INPUT TYPE="checkbox" NAME="DocsExcludingInsiderTrading" id="docsexcluding">
								<xsl:attribute name="value"><xsl:value-of select="$EIT_Value"/></xsl:attribute>
							</INPUT>													
						</xsl:otherwise>
						</xsl:choose>
						<!-- *************************************************-->						
						</TD>
						<TD CLASS="data" COLSPAN="2">Notify me of any new company documents, except insider trading documents</TD>
					</TR>											
					<TR>
						<TD CLASS="data" ALIGN="right" WIDTH="11%">
						<!-- Corporate Document Notifications, insider only -->
						<xsl:variable name="OIT_Value" select="EmailUpdInfo/iOnlyInsiderTradingDocs"/>
						<!-- *************************************************-->
						<xsl:choose>
						<xsl:when test="(EmailUpdInfo/FormAction = 'MakeUpdate') and (EmailUpdInfo/bOnlyInsiderTradingDocs = 'true')">
							<label class="visuallyhidden" for="onlinyinsider">onlinyinsider</label>
							<INPUT TYPE="checkbox" NAME="OnlyInsiderTradingDocs" CHECKED="Y" id="onlinyinsider">
								<xsl:attribute name="value"><xsl:value-of select="$OIT_Value"/></xsl:attribute>
							</INPUT>														
						</xsl:when>
						<xsl:when test="(EmailUpdInfo/FormAction = 'Success') and (EmailUpdInfo/bOnlyInsiderTradingDocsAdded = 'true')">
							<label class="visuallyhidden" for="onlinyinsider">onlinyinsider</label>
							<INPUT TYPE="checkbox" NAME="OnlyInsiderTradingDocs" CHECKED="Y" id="onlinyinsider">
								<xsl:attribute name="value"><xsl:value-of select="$OIT_Value"/></xsl:attribute>
							</INPUT>														
						</xsl:when>						
						<xsl:otherwise>
							<label class="visuallyhidden" for="onlinyinsider">onlinyinsider</label>
							<INPUT TYPE="checkbox" NAME="OnlyInsiderTradingDocs" id="onlinyinsider">
								<xsl:attribute name="value"><xsl:value-of select="$OIT_Value"/></xsl:attribute>
							</INPUT>													
						</xsl:otherwise>
						</xsl:choose>
						<!-- *************************************************-->						
						</TD>
						<TD CLASS="data" COLSPAN="2">Notify me of only new insider trading documents</TD>
					</TR>	
					<TR>
						<TD CLASS="colorlight" COLSPAN="3">
							<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
						</TD>
					</TR>	
					<TR>
						<TD COLSPAN="3" CLASS="header">News / Press Releases</TD>
					</TR>	
					<TR>
						<TD CLASS="data" ALIGN="right" WIDTH="11%">
						<!-- Press Release Notifications -->
						<xsl:variable name="News_Value" select="EmailUpdInfo/PressRelease"/>
						<!-- *************************************************-->
						<xsl:choose>
						<xsl:when test="(EmailUpdInfo/FormAction = 'MakeUpdate') and (EmailUpdInfo/bPressRelease = 'true')">
							<label class="visuallyhidden" for="news">news</label>
							<INPUT TYPE="checkbox" NAME="News" CHECKED="Y" id="news">
								<xsl:attribute name="value"><xsl:value-of select="$News_Value"/></xsl:attribute>
							</INPUT>														
						</xsl:when>
						<xsl:when test="(EmailUpdInfo/FormAction = 'Success') and (EmailUpdInfo/bPressReleaseAdded = 'true')">
							<label class="visuallyhidden" for="news">news</label>
							<INPUT TYPE="checkbox" NAME="News" CHECKED="Y" id="news">
								<xsl:attribute name="value"><xsl:value-of select="$News_Value"/></xsl:attribute>
							</INPUT>														
						</xsl:when>						
						<xsl:otherwise>
							<label class="visuallyhidden" for="news">news</label>
							<INPUT TYPE="checkbox" NAME="News" id="news">
								<xsl:attribute name="value"><xsl:value-of select="$News_Value"/></xsl:attribute>
							</INPUT>													
						</xsl:otherwise>
						</xsl:choose>
						<!-- *************************************************-->						
						</TD>
						<TD CLASS="data" COLSPAN="2">Notify me of any new company press releases</TD>
					</TR>
					<!--<TR>
						<TD CLASS="colorlight" COLSPAN="3">
							<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
						</TD>
					</TR>	-->
					<TR VALIGN="bottom" CLASS="colorlight">
						<TD CLASS="colorlight" COLSPAN="3" ALIGN="center">
							<INPUT TYPE="submit" NAME="Update" VALUE="Update" />
							<xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text>
							<INPUT TYPE="submit" NAME="Cancel" VALUE="Cancel" />
						</TD>					
					</TR>
					</TABLE>
				 </TD>				
				</TR>
					</fieldset>
				</FORM>
				</TABLE>
			</TD>
		</TR>
	  </TABLE>
    </TD>	
  </TR>
  <TR class="default" align="left">
	<TD class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span></TD>
  </TR>	  
</xsl:template>

<xsl:template match="irw:EmailUpds_es">

<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" CLASS="colordark">

	<xsl:call-template name="Email_es_Title"/>
	<xsl:call-template name="EmailUpd_es_Data"/>

</TABLE> 

<xsl:call-template name="Copyright"/>	
</xsl:template>

	
</xsl:stylesheet>

  