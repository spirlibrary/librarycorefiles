<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns="http://www.w3.ord/TR/REC-html40"
  xmlns:irw="http://www.snl.com/xml/irw">

  <xsl:output method="html" media-type="text/html" indent="no"/>


  <!-- Start default Template here-->
  <!-- SP:94675 [RC: 03/12/2010] Clean up for all templates-->
  <!-- 
SP : 93510 [ JR ] Dt.: 03 March 2010
==============================================
Improvement : Implement Events Calendar upgrades for Templates 1 and 0
-->
  <!-- 
SP : 93383 [ JR ] Dt.: 02 March 2010
==============================================
Bug Fixed : Event Calendar upgrade - add message when no events are available 
Additional Bug Fixed:
1) Page width problem due to wrong Table > TD Structure.
2) Datashade (background of Event Title) breaking problem has been fixed now.
3) Created new Arrow Icons to make it more clear, Sharper and Transparent.
-->
  <!--
//This is the default template. Most changes are to either remove the company name, ticker, or exchange. you can also change the name of the page or remove this row all together.
//These templates below are broken up how the page is displayed.

-->
  <xsl:template name="Calendar_Title">
    <xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
    <TR>
      <TD CLASS="colordark" NOWRAP="true" border="0" align="left">
        <SPAN CLASS="title1light">
          <xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/>
        </SPAN>
      </TD>
      <TD CLASS="colordark" NOWRAP="true" align="right" valign="top" border="0">
        <span class="subtitlelight">
          <xsl:value-of select="$CompanyNameCaps"/> (<xsl:value-of select="Company/Exchange"/> - <xsl:value-of select="Company/Ticker"/>)
        </span>
        <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
      </TD>
    </TR>
    <TR class="default" align="left">
      <TD class="colorlight" colspan="2">
        <span class="default">
          <xsl:value-of select="Company/Header" disable-output-escaping="yes"/>
        </span>
      </TD>
    </TR>
  </xsl:template>

  <xsl:template name="FooterTag">
    <tr class="colorlight">
      <td colspan="2">
        <span class="default">
          <xsl:value-of select="Company/Footer" disable-output-escaping="yes"/>
        </span>
      </td>
    </tr>
  </xsl:template>


  <xsl:template name="Calendar_EmailNotification">
    <xsl:variable name="KeyInstn" select="Company/KeyInstn"/>
    <xsl:variable name="Action" select="Calendar/ActionDescription"/>
    <xsl:variable name="SelectError" select="'SelectError'"/>
    <xsl:variable name="RemindError" select="'RemindError'"/>
    <xsl:variable name="UpdateError" select="'UpdateError'"/>
    <xsl:variable name="Success" select="'Success'"/>
    <xsl:for-each select="CLIENTEMAILPREFS[NotificationType = '3' and NotificationEnabled = 'True']">
      <TR>
        <TD COLSPAN="3" CLASS="data">
          <a>
            <xsl:attribute name="href">
              email.aspx?IID=<xsl:value-of select="../Company/KeyInstn"/>
            </xsl:attribute>Notify me of new Company events</a>.
        </TD>
      </TR>
    </xsl:for-each>
    <xsl:for-each select="CLIENTEMAILPREFS[NotificationType = '4' and NotificationEnabled = 'True']">
      <TR>
        <TD COLSPAN="3" CLASS="data">
          <a>
            <xsl:attribute name="href">
              email.aspx?IID=<xsl:value-of select="../Company/KeyInstn"/>
            </xsl:attribute>Send me reminders of Company events</a>.
        </TD>
      </TR>
    </xsl:for-each>
    <TR>
      <TD COLSPAN="3" CLASS="colorlight" valign="bottom">
        <xsl:choose>
          <xsl:when test="$Action = $SelectError">
            <span CLASS="datared">The Days Before field for one of your selections produces an invalid Email Reminder send date.  Please correct.</span>
          </xsl:when>
          <xsl:when test="$Action = $RemindError">
            <span CLASS="datared">We apologize, but this feature is currently unavailable.  Please attempt your request at a later time.</span>
          </xsl:when>
          <xsl:when test="$Action = $UpdateError">
            <span CLASS="datared">We apologize, but this feature is currently unavailable.  Please attempt your request at a later time.</span>
          </xsl:when>
          <xsl:when test="$Action = $Success">
            <span CLASS="defaultbold">Thank you!  Your request has processed successfully.</span>
          </xsl:when>
        </xsl:choose>
      </TD>
    </TR>
  </xsl:template>

  <xsl:template name="Calendar_EmailAddress">
    <xsl:variable name="Action" select="Calendar/ActionDescription"/>
    <xsl:variable name="EmailError" select="'EmailError'"/>
    <xsl:choose>
      <xsl:when test="$Action = $EmailError">
        <TR>
          <TD COLSPAN="2" CLASS="colorlight">
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </TD>
          <TD CLASS="colorlight">
            <SPAN CLASS="datared">An incorrect email address was entered.</SPAN>
          </TD>
        </TR>
      </xsl:when>
    </xsl:choose>

    <xsl:variable name="RowCount" select="Calendar/CompanyRowCount"/>
    <xsl:variable name="RowCountEmpty" select="0"/>
    <xsl:choose>
      <xsl:when test="$RowCount != $RowCountEmpty">
      </xsl:when>
    </xsl:choose>
  </xsl:template>


  <xsl:template name="Calendar_ErrMsg">
    <TR>
      <TD colspan="3">
        <span class="defaultbold">
          <xsl:value-of select="EmailError/Message"/>
        </span>
      </TD>
    </TR>
  </xsl:template>

  <xsl:template name="Calendar_Data">
    <table border="0" cellspacing="0" cellpadding="3" width="100%" class="calendarDataTable">
      <xsl:variable name="RowCount" select="Calendar/CompanyRowCount"/>
      <xsl:variable name="RowCountEmpty" select="0"/>
      <TR>
        <TD CLASS="header" align="left" valign="bottom">Event</TD>
        <xsl:if test="$RowCount != $RowCountEmpty">
          <TD CLASS="header" align="right" valign="bottom">Date</TD>
        </xsl:if>
      </TR>
      <xsl:variable name="KeyInstnValue" select="Company/KeyInstn"/>

      <xsl:choose>
        <xsl:when test="$RowCount != $RowCountEmpty ">
        </xsl:when>
        <xsl:otherwise>
          <TR>
            <TD CLASS="data">
              <strong>There are no events currently scheduled.</strong>
            </TD>
          </TR>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:for-each select="Calendar">
        <xsl:choose>
          <xsl:when test="$RowCount != $RowCountEmpty">
            <xsl:variable name="CompanyCalendarTitle" select="CompanyCalendarTitle"/>
            <xsl:variable name="KeyValue" select="Key"/>

            <xsl:variable name="StartDate" select="CompanyCalendarDate"/>
            <xsl:variable name="StartDay" select="substring-before($StartDate,' ')"/>
            <xsl:variable name="StartTime" select="substring-after($StartDate,' ')"/>

            <xsl:variable name="EndDate" select="CompanyCalendarDateEnd"/>
            <xsl:variable name="EndDay" select="substring-before($EndDate,' ')"/>
            <xsl:variable name="EndTime" select="substring-after($EndDate,' ')"/>

            <xsl:variable name="StartDateET" select="CompanyCalendarDateET"/>
            <xsl:variable name="EndDateET" select="CompanyCalendarDateEndET"/>            

            <xsl:variable name="EventName" select="concat('Event_', $KeyValue)"></xsl:variable>
            <xsl:variable name="RemindName" select="concat('Remind_', $KeyValue)"></xsl:variable>
            <xsl:variable name="RemindValue" select="CalendarNotifyDaysPrior"></xsl:variable>
            <xsl:variable name="StartDateName" select="concat('StartDate_', $KeyValue)"></xsl:variable>
            <xsl:variable name="EventType" select="EventType"/>
            <xsl:variable name="CalEventDetails" select="concat('CalEventDetails_', $KeyValue)"/>
            <xsl:variable name="ImageCalEventDetails" select="concat('ImageCalEventDetails_', $KeyValue)"/>
            <!-- Meridiem variable Print (AM/PM) value -->
            <xsl:variable name="Meridiem" select="substring-after($StartTime,' ')"/>

            <TR>
              <td CLASS="data showHideLinks" align="left" valign="top">
                <a href="javascript:void(0);" onclick="javascript:showHideData('{$CalEventDetails}','{$ImageCalEventDetails}')" class="plusMinusImage" title="Show hide Link">
                  <img id="{$ImageCalEventDetails}" src="javascript/committee/open.gif" border="0" alt="Open"/>
                </a>
                <xsl:choose>
                  <xsl:when test="$EventType = '0'">
                    <a href="javascript:void(0);" class="fielddef" onclick="javascript:showHideData('{$CalEventDetails}','{$ImageCalEventDetails}')" title="{CompanyCalendarTitle}">
                      <xsl:value-of select="CompanyCalendarTitle"/>
                    </a>
                  </xsl:when>
                  <xsl:when test="$EventType = '1'">
                    <a href="javascript:void(0);" class="fielddef" onclick="javascript:showHideData('{$CalEventDetails}','{$ImageCalEventDetails}')" title="{CompanyCalendarTitle}">
                      <xsl:value-of select="CompanyCalendarTitle"/>
                    </a>
                  </xsl:when>
                  <xsl:when test="$EventType = '2'">
                    <a href="javascript:void(0);" class="fielddef" onclick="javascript:showHideData('{$CalEventDetails}','{$ImageCalEventDetails}')" title="{CompanyCalendarTitle}">
                      <xsl:value-of select="CompanyCalendarTitle"/>
                    </a>
                  </xsl:when>
                  <xsl:when test="$EventType = '3'">
                    <a href="javascript:void(0);" class="fielddef" onclick="javascript:showHideData('{$CalEventDetails}','{$ImageCalEventDetails}')" title="{CompanyCalendarTitle}">
                      <xsl:value-of select="CompanyCalendarTitle"/>
                    </a>
                  </xsl:when>
                  <xsl:otherwise>
                    <a href="javascript:void(0);" class="fielddef" onclick="javascript:showHideData('{$CalEventDetails}','{$ImageCalEventDetails}')" title="{CompanyCalendarTitle}">
                      <xsl:value-of select="CompanyCalendarTitle"/>
                    </a>
                  </xsl:otherwise>
                </xsl:choose>
              </td>
              <xsl:choose>
                <xsl:when test="$EndDate != '' and ($EndDate != $StartDate or $EndTime != $StartTime) and CompanyCalendarDate != CompanyCalendarDateEnd">
                  <td CLASS="data" align="right" valign="top" class="eventdate">
                    <xsl:value-of select="$StartDay"/>
                    <xsl:if test="$StartTime != '12:00:00 AM' and $StartTime != ''">
                      <br/>
                      <!-- Code Display Hour and Minute and Hide Secounds -->
                      <xsl:choose>
                        <xsl:when test="string-length($StartTime) = '10'">
                          <xsl:value-of select="substring($StartTime, 0, 5)"/>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:value-of select="substring($StartTime, 0, 6)"/>
                        </xsl:otherwise>
                      </xsl:choose>&#160;<xsl:value-of select="$Meridiem"/>
                      &#160;<xsl:value-of select="TimeZoneLabel"/>
                    </xsl:if>&#160;-&#160;
                    <xsl:value-of select="$EndDay"/>
                    <xsl:if test="$EndTime != '12:00:00 AM' and $EndTime != ''">
                      <br/>
                      <xsl:value-of select="$EndTime"/>
                      &#160;<xsl:value-of select="TimeZoneLabel"/>
                    </xsl:if>

                    <xsl:choose>
                      <xsl:when test="$EndDate != ''">
                        <span>
                          <br/>
                          <A href="CalendarItem.ashx?keyInstn={$KeyInstnValue}&amp;keyEvent={$KeyValue}&amp;eventType={$EventType}&amp;eventTitle={$CompanyCalendarTitle}&amp;dtStart={$StartDateET}&amp;dtEnd={$EndDateET}" onMouseOver="window.status='Click here to add to iCalendar compliant calendars (ex.Outlook 2000 &amp; above, Netscape, Mozilla, etc.).'; return true" onfocus="window.status='Click here to add to iCalendar compliant calendars (ex.Outlook 2000 &amp; above, Netscape, Mozilla, etc.).'; return true" onMouseOut="window.status=''; return true" onblur="window.status=''; return true" title="Add To Calendar">[Add to Calendar]</A>
                        </span>
                      </xsl:when>
                      <xsl:otherwise>
                        <span>
                          <br/>
                          <A href="CalendarItem.ashx?keyInstn={$KeyInstnValue}&amp;keyEvent={$KeyValue}&amp;eventType={$EventType}&amp;eventTitle={$CompanyCalendarTitle}&amp;dtStart={$StartDateET}" onMouseOver="window.status='Click here to add to iCalendar compliant calendars (ex.Outlook 2000 &amp; above, Netscape, Mozilla, etc.).'; return true" onMouseOut="window.status=''; return true">[Add to Calendar]</A>
                        </span>
                      </xsl:otherwise>
                    </xsl:choose>

                  </td>
                </xsl:when>
                <xsl:otherwise>
                  <td CLASS="data" align="right" valign="top">
                    <xsl:value-of select="$StartDay"/>
                    <xsl:if test="$StartTime != '12:00:00 AM' and $StartTime != ''">
                      <br/>
                      <!-- Code Display Hour and Minute and Hide Secounds -->
                      <xsl:choose>
                        <xsl:when test="string-length($StartTime) = '10'">
                          <xsl:value-of select="substring($StartTime, 0, 5)"/>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:value-of select="substring($StartTime, 0, 6)"/>
                        </xsl:otherwise>
                      </xsl:choose>&#160;<xsl:value-of select="$Meridiem"/>
                      &#160;<xsl:value-of select="TimeZoneLabel"/>
                    </xsl:if>
                    <xsl:choose>
                      <xsl:when test="$EndDateET != ''">
                        <span>
                          <br/>
                          <A href="CalendarItem.ashx?keyInstn={$KeyInstnValue}&amp;keyEvent={$KeyValue}&amp;eventType={$EventType}&amp;eventTitle={$CompanyCalendarTitle}&amp;dtStart={$StartDateET}&amp;dtEnd={$EndDateET}"
                            onMouseOver="window.status='Click here to add to iCalendar compliant calendars (ex.Outlook 2000 &amp; above, Netscape, Mozilla, etc.).'; return true" onfocus="window.status='Click here to add to iCalendar compliant calendars (ex.Outlook 2000 &amp; above, Netscape, Mozilla, etc.).'; return true"
                            onMouseOut="window.status=''; return true" onblur="window.status=''; return true" title="Add to Calendar">[Add to Calendar]</A>
                        </span>
                      </xsl:when>
                      <xsl:otherwise>
                        <span>
                          <br/>
                          <A href="CalendarItem.ashx?keyInstn={$KeyInstnValue}&amp;keyEvent={$KeyValue}&amp;eventType={$EventType}&amp;eventTitle={$CompanyCalendarTitle}&amp;dtStart={$StartDateET}" onMouseOver="window.status='Click here to add to iCalendar compliant calendars (ex.Outlook 2000 &amp; above, Netscape, Mozilla, etc.).'; return true" onfocus="window.status='Click here to add to iCalendar compliant calendars (ex.Outlook 2000 &amp; above, Netscape, Mozilla, etc.).'; return true" onMouseOut="window.status=''; return true" onblur="window.status=''; return true" title="Add to Calendar">[Add to Calendar]</A>
                        </span>
                      </xsl:otherwise>
                    </xsl:choose>
                  </td>
                </xsl:otherwise>
              </xsl:choose>
            </TR>
            <tr>
              <td colspan="2" valign="top" class="data">
                <div id="{$CalEventDetails}" style="display:none;padding-left: 15px">
                  <xsl:call-template name="CalendarDetail">
                    <xsl:with-param name="paramEventKey" select="$KeyValue" />
                    <xsl:with-param name="paramEventType">
                      <xsl:choose>
                        <xsl:when test="$EventType = '0'">
                          <xsl:value-of select="3"/>
                        </xsl:when>
                        <xsl:when test="$EventType = '1'">
                          <xsl:value-of select="2"/>
                        </xsl:when>
                        <xsl:when test="$EventType = '2'">
                          <xsl:value-of select="1"/>
                        </xsl:when>
                        <xsl:when test="$EventType = '3'">
                          <xsl:value-of select="4"/>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:value-of select="-1"/>
                        </xsl:otherwise>
                      </xsl:choose>
                    </xsl:with-param>
                  </xsl:call-template>
                </div>
              </td>
            </tr>
          </xsl:when>
        </xsl:choose>
      </xsl:for-each>
      <INPUT TYPE="hidden" NAME="EventCount" value="{Calendar/CompanyRowCount}"/>
    </table>
    <br />
  </xsl:template>

  <xsl:template name="PastCalendar_Data">
    <table border="0" cellspacing="0" cellpadding="3" width="100%">
      <xsl:variable name="DisplayPast" select="PastCalendar/DisplayPast"/>
      <xsl:variable name="DoDisplayPast" select="1"/>
      <xsl:if test="$DisplayPast = $DoDisplayPast ">
        <xsl:variable name="RowCount" select="PastCalendar/CompanyRowCount"/>
        <xsl:variable name="RowCountEmpty" select="0"/>

        <TR>
          <TD CLASS="header" align="left" valign="bottom">Past Event</TD>
          <xsl:if test="$RowCount != $RowCountEmpty">
            <TD CLASS="header" align="right" valign="bottom">Date</TD>
          </xsl:if>
        </TR>

        <xsl:variable name="KeyInstnValue" select="Company/KeyInstn"/>

        <xsl:choose>
          <xsl:when test="$RowCount = $RowCountEmpty">
            <TR>
              <TD CLASS="data">
                <strong>There were no events scheduled within the set timeframe.</strong>
              </TD>
            </TR>
          </xsl:when>
        </xsl:choose>

        <xsl:for-each select="PastCalendar">
          <xsl:choose>
            <xsl:when test="$RowCount != $RowCountEmpty">
              <xsl:variable name="CompanyCalendarTitle" select="CompanyCalendarTitle"/>
              <xsl:variable name="KeyValue" select="Key"/>

              <xsl:variable name="StartDate" select="CompanyCalendarDate"/>
              <xsl:variable name="StartDay" select="substring-before($StartDate,' ')"/>
              <xsl:variable name="StartTime" select="substring-after($StartDate,' ')"/>

              <xsl:variable name="EndDate" select="CompanyCalendarDateEnd"/>
              <xsl:variable name="EndDay" select="substring-before($EndDate,' ')"/>
              <xsl:variable name="EndTime" select="substring-after($EndDate,' ')"/>

              <xsl:variable name="EventName" select="concat('Event_', $KeyValue)"></xsl:variable>
              <xsl:variable name="RemindName" select="concat('Remind_', $KeyValue)"></xsl:variable>
              <xsl:variable name="RemindValue" select="CalendarNotifyDaysPrior"></xsl:variable>
              <xsl:variable name="StartDateName" select="concat('StartDate_', $KeyValue)"></xsl:variable>
              <xsl:variable name="EventType" select="EventType"/>
              <xsl:variable name="PastCalEventDetails" select="concat('PastCalEventDetails_', $KeyValue)"/>
              <xsl:variable name="ImagePastCalEventDetails" select="concat('ImagePastCalEventDetails_', $KeyValue)"/>

              <TR>
                <td CLASS="data showHideLinks" align="left" valign="top">
                  <a href="javascript:void(0);" onclick="javascript:showHideData('{$PastCalEventDetails}','{$ImagePastCalEventDetails}')" class="plusMinusImage" title="Show Hide Link">
                    <img id="{$ImagePastCalEventDetails}" src="javascript/committee/open.gif" border="0" alt="Open"/>
                  </a>

                  <xsl:choose>
                    <xsl:when test="$EventType = '0'">
                      <a href="javascript:void(0);" class="fielddef" onclick="javascript:showHideData('{$PastCalEventDetails}','{$ImagePastCalEventDetails}')" title="{CompanyCalendarTitle}">
                        <xsl:value-of select="CompanyCalendarTitle"/>
                      </a>
                    </xsl:when>
                    <xsl:when test="$EventType = '1'">
                      <a href="javascript:void(0);" class="fielddef" onclick="javascript:showHideData('{$PastCalEventDetails}','{$ImagePastCalEventDetails}')" title="{CompanyCalendarTitle}">
                        <xsl:value-of select="CompanyCalendarTitle"/>
                      </a>
                    </xsl:when>
                    <xsl:when test="$EventType = '2'">
                      <a href="javascript:void(0);" class="fielddef" onclick="javascript:showHideData('{$PastCalEventDetails}','{$ImagePastCalEventDetails}')" title="{CompanyCalendarTitle}">
                        <xsl:value-of select="CompanyCalendarTitle"/>
                      </a>
                    </xsl:when>
                    <xsl:when test="$EventType = '3'">
                      <a href="javascript:void(0);" class="fielddef" onclick="javascript:showHideData('{$PastCalEventDetails}','{$ImagePastCalEventDetails}')" title="{CompanyCalendarTitle}">
                        <xsl:value-of select="CompanyCalendarTitle"/>
                      </a>
                    </xsl:when>
                    <xsl:otherwise>
                      <a href="javascript:void(0);" class="fielddef" onclick="javascript:showHideData('{$PastCalEventDetails}','{$ImagePastCalEventDetails}')" title="{CompanyCalendarTitle}">
                        <xsl:value-of select="CompanyCalendarTitle"/>
                      </a>
                    </xsl:otherwise>
                  </xsl:choose>
                </td>
                <xsl:choose>
                  <xsl:when test="$EndDate != '' and CompanyCalendarDate != CompanyCalendarDateEnd">
                    <td CLASS="data" align="right" valign="top">
                      <xsl:value-of select="$StartDay"/>&#160;-&#160;
                      <xsl:value-of select="$EndDay"/>
                      <xsl:if test="$EndTime != '12:00:00 AM'">
                        <br/>
                        <xsl:value-of select="$EndTime"/>
                      </xsl:if>
                    </td>
                  </xsl:when>
                  <xsl:otherwise>
                    <td CLASS="data" align="right" valign="top">
                      <xsl:value-of select="$StartDay"/>
                    </td>
                  </xsl:otherwise>
                </xsl:choose>
              </TR>
              <tr>
                <td colspan="2" valign="top" class="data">
                  <div id="{$PastCalEventDetails}" style="display:none;padding-left: 15px">
                    <xsl:call-template name="CalendarDetail">
                      <xsl:with-param name="paramEventKey" select="$KeyValue" />
                      <xsl:with-param name="paramEventType">
                        <xsl:choose>
                          <xsl:when test="$EventType = '0'">
                            <xsl:value-of select="3"/>
                          </xsl:when>
                          <xsl:when test="$EventType = '1'">
                            <xsl:value-of select="2"/>
                          </xsl:when>
                          <xsl:when test="$EventType = '2'">
                            <xsl:value-of select="1"/>
                          </xsl:when>
                          <xsl:when test="$EventType = '3'">
                            <xsl:value-of select="4"/>
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:value-of select="-1"/>
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:with-param>
                    </xsl:call-template>
                  </div>
                </td>
              </tr>
            </xsl:when>
          </xsl:choose>
        </xsl:for-each>

        <INPUT TYPE="hidden" NAME="EventCount">
          <xsl:attribute name="value">
            <xsl:value-of select="PastCalendar/CompanyRowCount"/>
          </xsl:attribute>
        </INPUT>
      </xsl:if>
    </table>
    <br />
  </xsl:template>
  <!-- default Template Ends here. all templates below are ONLY if you are using the 'Style Template' section in the console. -->



















  <!-- This is Template ONE option in the console under 'Style Template'
//This template is a striped down version of the main template. Everything (data) is displayed in single rows.
//The sections will be commented on where the rows are. you should be able to just move rows (entire chunks of data) where you need too. Edits to this section are rare, mostly due to size (width of the page).
-->
  <xsl:template name="Calendar_Title2">
    <xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
    <TR>
      <TD CLASS="title2colordark titletest" nowrap="" valign="top" align="left">
        <xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/>
        <br />
        <IMG SRC="" WIDTH="2" HEIGHT="1" alt="{$CompanyNameCaps}" />
        <span class="title2colordark titletest2">
          <xsl:value-of select="$CompanyNameCaps"/>&#160;(<xsl:value-of select="Company/Exchange"/>&#160;-&#160;<xsl:value-of select="Company/Ticker"/>)&#160;
        </span>
      </TD>

      <TD CLASS="" nowrap="" align="right" valign="top">
        <table width="100%" cellpadding="0" cellspacing="0">
          <tr>
            <td class="SURR DATASHADE">
              <TABLE>
                <xsl:call-template name="Calendar_EmailNotification2"/>
              </TABLE>
            </td>
          </tr>
        </table>
      </TD>
    </TR>
    <TR class="default" align="left">
      <TD class="colorlight" colspan="2">
        <span class="default">
          <xsl:value-of select="Company/Header" disable-output-escaping="yes"/>
        </span>
      </TD>
    </TR>

  </xsl:template>


  <xsl:template name="Calendar_EmailNotification2">
    <xsl:variable name="KeyInstn" select="Company/KeyInstn"/>
    <xsl:variable name="Action" select="Calendar/ActionDescription"/>
    <xsl:variable name="SelectError" select="'SelectError'"/>
    <xsl:variable name="RemindError" select="'RemindError'"/>
    <xsl:variable name="UpdateError" select="'UpdateError'"/>
    <xsl:variable name="Success" select="'Success'"/>
    <xsl:for-each select="CLIENTEMAILPREFS[NotificationType = '3' and NotificationEnabled = 'True']">
      <TR>
        <TD COLSPAN="3" CLASS="default">
          <a>
            <xsl:attribute name="href">
              email.aspx?IID=<xsl:value-of select="../Company/KeyInstn"/>
            </xsl:attribute>Notify me of new Company events</a>.
        </TD>
      </TR>
    </xsl:for-each>
    <xsl:for-each select="CLIENTEMAILPREFS[NotificationType = '4' and NotificationEnabled = 'True']">
      <TR>
        <TD COLSPAN="3" CLASS="default">
          <a>
            <xsl:attribute name="href">
              email.aspx?IID=<xsl:value-of select="../Company/KeyInstn"/>
            </xsl:attribute>Send me reminders of Company events</a>.
        </TD>
      </TR>
    </xsl:for-each>

    <TR>
      <TD COLSPAN="3" CLASS="" valign="bottom">
        <xsl:choose>
          <xsl:when test="$Action = $SelectError">
            <span CLASS="datared B">The Days Before field for one of your selections produces an invalid Email Reminder send date.  Please correct.</span>
          </xsl:when>
          <xsl:when test="$Action = $RemindError">
            <span CLASS="datared B">We apologize, but this feature is currently unavailable.  Please attempt your request at a later time.</span>
          </xsl:when>
          <xsl:when test="$Action = $UpdateError">
            <span CLASS="datared B">We apologize, but this feature is currently unavailable.  Please attempt your request at a later time.</span>
          </xsl:when>
          <xsl:when test="$Action = $Success">
            <span CLASS="defaultbold">Thank you!  Your request has processed successfully.</span>
          </xsl:when>
        </xsl:choose>
      </TD>
    </TR>
  </xsl:template>



  <xsl:template name="Calendar_EmailAddress2">
    <xsl:variable name="Action" select="Calendar/ActionDescription"/>
    <xsl:variable name="EmailError" select="'EmailError'"/>
    <xsl:choose>
      <xsl:when test="$Action = $EmailError">
        <TR>
          <TD COLSPAN="2" CLASS="colorlight">
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </TD>
          <TD CLASS="colorlight">
            <SPAN CLASS="datared">An incorrect email address was entered.</SPAN>
          </TD>
        </TR>
      </xsl:when>
    </xsl:choose>
    <xsl:variable name="RowCount" select="Calendar/CompanyRowCount"/>
    <xsl:variable name="RowCountEmpty" select="0"/>
    <xsl:choose>
      <xsl:when test="$RowCount != $RowCountEmpty">
      </xsl:when>
    </xsl:choose>
  </xsl:template>



  <xsl:template name="Calendar_ErrMsg2">
    <TR>
      <TD COLSPAN="3">
        <span class="defaultbold">
          <xsl:value-of select="EmailError/Message"/>
        </span>
      </TD>
    </TR>
  </xsl:template>



  <xsl:template name="Calendar_Data2">
    <table border="0" cellspacing="0" cellpadding="3" width="100%" class="calendarDataTable">
      <xsl:variable name="RowCount" select="Calendar/CompanyRowCount"/>
      <xsl:variable name="RowCountEmpty" select="0"/>
      <TR>
        <TD align="left" valign="bottom" class="surr datashade">
          <xsl:choose>
            <xsl:when test="$RowCount != $RowCountEmpty">
              <xsl:attribute name="class">surrleft datashade</xsl:attribute>
            </xsl:when>
          </xsl:choose>
          <span class="titletest2">Event</span>
        </TD>
        <xsl:if test="$RowCount != $RowCountEmpty">
          <TD align="right" CLASS="surrright datashade" valign="bottom">
            <span class="titletest2">Date</span>
          </TD>
        </xsl:if>
      </TR>
      <xsl:variable name="KeyInstnValue" select="Company/KeyInstn"/>
      <xsl:choose>
        <xsl:when test="$RowCount != $RowCountEmpty ">
        </xsl:when>
        <xsl:otherwise>
          <TR>
            <TD CLASS="data">
              <strong>There are no events currently scheduled.</strong>
            </TD>
          </TR>
        </xsl:otherwise>
      </xsl:choose>

      <xsl:for-each select="Calendar">
        <xsl:choose>

          <xsl:when test="$RowCount != $RowCountEmpty">
            <xsl:variable name="KeyValue" select="Key"/>
            <xsl:variable name="CompanyCalendarTitle" select="CompanyCalendarTitle"/>

            <xsl:variable name="StartDate" select="CompanyCalendarDate"/>
            <xsl:variable name="StartDay" select="substring-before($StartDate,' ')"/>
            <xsl:variable name="StartTime" select="substring-after($StartDate,' ')"/>

            <xsl:variable name="EndDate" select="CompanyCalendarDateEnd"/>
            <xsl:variable name="EndDay" select="substring-before($EndDate,' ')"/>
            <xsl:variable name="EndTime" select="substring-after($EndDate,' ')"/>

            <xsl:variable name="StartDateET" select="CompanyCalendarDateET"/>
            <xsl:variable name="EndDateET" select="CompanyCalendarDateEndET"/>

            <xsl:variable name="EventName" select="concat('Event_', $KeyValue)"></xsl:variable>
            <xsl:variable name="RemindName" select="concat('Remind_', $KeyValue)"></xsl:variable>
            <xsl:variable name="RemindValue" select="CalendarNotifyDaysPrior"></xsl:variable>
            <xsl:variable name="StartDateName" select="concat('StartDate_', $KeyValue)"></xsl:variable>
            <xsl:variable name="EventType" select="EventType"/>
            <xsl:variable name="CalEventDetails" select="concat('CalEventDetails_', $KeyValue)"/>
            <xsl:variable name="ImageCalEventDetails" select="concat('ImageCalEventDetails_', $KeyValue)"/>


            <!-- Meridiem variable Print (AM/PM) value -->
            <xsl:variable name="Meridiem" select="substring-after($StartTime,' ')"/>

            <TR>
              <td CLASS="data showHideLinks" valign="top" border="0">
                <a href="javascript:void(0);" onclick="javascript:showHideData('{$CalEventDetails}','{$ImageCalEventDetails}')" class="plusMinusImage" title="Show Hide Link">
                  <img id="{$ImageCalEventDetails}" src="javascript/committee/open.gif" border="0" alt="Open"/>
                </a>

                <xsl:choose>
                  <xsl:when test="$EventType = '0'">
                    <a href="javascript:void(0);" class="fielddef" onclick="javascript:showHideData('{$CalEventDetails}','{$ImageCalEventDetails}')" title="{CompanyCalendarTitle}">
                      <xsl:value-of select="CompanyCalendarTitle"/>
                    </a>
                  </xsl:when>
                  <xsl:when test="$EventType = '1'">
                    <a href="javascript:void(0);" class="fielddef" onclick="javascript:showHideData('{$CalEventDetails}','{$ImageCalEventDetails}')" title="{CompanyCalendarTitle}">
                      <xsl:value-of select="CompanyCalendarTitle"/>
                    </a>
                  </xsl:when>
                  <xsl:when test="$EventType = '2'">
                    <a href="javascript:void(0);" class="fielddef" onclick="javascript:showHideData('{$CalEventDetails}','{$ImageCalEventDetails}')" title="{CompanyCalendarTitle}">
                      <xsl:value-of select="CompanyCalendarTitle"/>
                    </a>
                  </xsl:when>
                  <xsl:when test="$EventType = '3'">
                    <a href="javascript:void(0);" class="fielddef" onclick="javascript:showHideData('{$CalEventDetails}','{$ImageCalEventDetails}')" title="{CompanyCalendarTitle}">
                      <xsl:value-of select="CompanyCalendarTitle"/>
                    </a>
                  </xsl:when>
                  <xsl:otherwise>
                    <a href="javascript:void(0);" class="fielddef" onclick="javascript:showHideData('{$CalEventDetails}','{$ImageCalEventDetails}')" title="{CompanyCalendarTitle}">
                      <xsl:value-of select="CompanyCalendarTitle"/>
                    </a>
                  </xsl:otherwise>
                </xsl:choose>
              </td>
              <xsl:choose>
                <xsl:when test="$EndDate != '' and CompanyCalendarDate != CompanyCalendarDateEnd">
                  <td CLASS="data" align="right" valign="top">
                    <xsl:value-of select="$StartDay"/>
                    <xsl:if test="$StartTime != '12:00:00 AM' and $StartTime != ''">
                      <br/>
                      <!-- Code Display Hour and Minute and Hide Secounds -->
                      <xsl:choose>
                        <xsl:when test="string-length($StartTime) = '10'">
                          <xsl:value-of select="substring($StartTime, 0, 5)"/>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:value-of select="substring($StartTime, 0, 6)"/>
                        </xsl:otherwise>
                      </xsl:choose>&#160;<xsl:value-of select="$Meridiem"/>
                    </xsl:if>&#160;-&#160;
                    <xsl:value-of select="$EndDay"/>
                    <xsl:if test="$EndTime != '12:00:00 AM' and $EndTime != ''">
                      <br/>
                      <xsl:value-of select="$EndTime"/>
                    </xsl:if>
                    <span>
                      <br/>
                      <A href="CalendarItem.ashx?keyInstn={$KeyInstnValue}&amp;keyEvent={$KeyValue}&amp;eventType={$EventType}&amp;eventTitle={$CompanyCalendarTitle}&amp;dtStart={$StartDateET}&amp;dtEnd={$EndDateET}"
                        onMouseOver="window.status='Click here to add to iCalendar compliant calendars (ex.Outlook 2000 &amp; above, Netscape, Mozilla, etc.).'; return true" onfocus="window.status='Click here to add to iCalendar compliant calendars (ex.Outlook 2000 &amp; above, Netscape, Mozilla, etc.).'; return true"
                        onMouseOut="window.status=''; return true" onblur="window.status=''; return true" title="Add to Calendar">[Add to Calendar]</A>
                    </span>
                  </td>
                </xsl:when>
                <xsl:otherwise>
                  <td CLASS="data" align="right" valign="top">
                    <xsl:value-of select="$StartDay"/>
                    <xsl:if test="$StartTime != '12:00:00 AM' and $StartTime != ''">
                      <br/>
                      <!-- Code Display Hour and Minute and Hide Secounds -->
                      <xsl:choose>
                        <xsl:when test="string-length($StartTime) = '10'">
                          <xsl:value-of select="substring($StartTime, 0, 5)"/>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:value-of select="substring($StartTime, 0, 6)"/>
                        </xsl:otherwise>
                      </xsl:choose>&#160;<xsl:value-of select="$Meridiem"/>
                    </xsl:if>
                    <span>
                      <br/>
                      <A href="CalendarItem.ashx?keyInstn={$KeyInstnValue}&amp;keyEvent={$KeyValue}&amp;eventType={$EventType}&amp;eventTitle={$CompanyCalendarTitle}&amp;dtStart={$StartDateET}"
                        onMouseOver="window.status='Click here to add to iCalendar compliant calendars (ex.Outlook 2000 &amp; above, Netscape, Mozilla, etc.).'; return true"
                        onMouseOut="window.status=''; return true">[Add to Calendar]</A>
                    </span>
                  </td>
                </xsl:otherwise>
              </xsl:choose>
            </TR>
            <tr>
              <td colspan="2" valign="top" class="data">
                <div id="{$CalEventDetails}" style="display:none;padding-left: 15px">
                  <xsl:call-template name="CalendarDetail">
                    <xsl:with-param name="paramEventKey" select="$KeyValue" />
                    <xsl:with-param name="paramEventType">
                      <xsl:choose>
                        <xsl:when test="$EventType = '0'">
                          <xsl:value-of select="3"/>
                        </xsl:when>
                        <xsl:when test="$EventType = '1'">
                          <xsl:value-of select="2"/>
                        </xsl:when>
                        <xsl:when test="$EventType = '2'">
                          <xsl:value-of select="1"/>
                        </xsl:when>
                        <xsl:when test="$EventType = '3'">
                          <xsl:value-of select="4"/>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:value-of select="-1"/>
                        </xsl:otherwise>
                      </xsl:choose>
                    </xsl:with-param>
                  </xsl:call-template>
                </div>
              </td>
            </tr>
          </xsl:when>
        </xsl:choose>
      </xsl:for-each>
      <INPUT TYPE="hidden" NAME="EventCount" class="input datashade">
        <xsl:attribute name="value">
          <xsl:value-of select="Calendar/CompanyRowCount"/>
        </xsl:attribute>
      </INPUT>
    </table>
    <br />
  </xsl:template>

  <xsl:template name="PastCalendar_Data2">
    <table border="0" cellspacing="0" cellpadding="3" width="100%">
      <xsl:variable name="DisplayPast" select="PastCalendar/DisplayPast"/>
      <xsl:variable name="DoDisplayPast" select="1"/>
      <xsl:if test="$DisplayPast = $DoDisplayPast ">
        <xsl:variable name="RowCount" select="PastCalendar/CompanyRowCount"/>
        <xsl:variable name="RowCountEmpty" select="0"/>
        <TR>
          <TD align="left" valign="bottom">
            <xsl:attribute name="class">
              <xsl:choose>
                <xsl:when test="$RowCount = $RowCountEmpty">surr datashade</xsl:when>
                <xsl:otherwise>surrleft datashade</xsl:otherwise>
              </xsl:choose>
            </xsl:attribute>
            <span class="titletest2">Past Event</span>
          </TD>
          <xsl:if test="$RowCount != $RowCountEmpty">
            <TD align="right" CLASS="surrright datashade" valign="bottom">
              <span class="titletest2">Date</span>
            </TD>
          </xsl:if>
        </TR>

        <xsl:variable name="KeyInstnValue" select="Company/KeyInstn"/>
        <xsl:choose>
          <xsl:when test="$RowCount = $RowCountEmpty">
            <TR>
              <TD CLASS="data">
                <strong>There were no events scheduled within the set timeframe.</strong>
              </TD>
            </TR>
          </xsl:when>
        </xsl:choose>
        <xsl:for-each select="PastCalendar">

          <xsl:choose>


            <xsl:when test="$RowCount != $RowCountEmpty">
              <xsl:variable name="CompanyCalendarTitle" select="CompanyCalendarTitle"/>
              <xsl:variable name="KeyValue" select="Key"/>

              <xsl:variable name="StartDate" select="CompanyCalendarDate"/>
              <xsl:variable name="StartDay" select="substring-before($StartDate,' ')"/>
              <xsl:variable name="StartTime" select="substring-after($StartDate,' ')"/>

              <xsl:variable name="EndDate" select="CompanyCalendarDateEnd"/>
              <xsl:variable name="EndDay" select="substring-before($EndDate,' ')"/>
              <xsl:variable name="EndTime" select="substring-after($EndDate,' ')"/>

              <xsl:variable name="EventName" select="concat('Event_', $KeyValue)"></xsl:variable>
              <xsl:variable name="RemindName" select="concat('Remind_', $KeyValue)"></xsl:variable>
              <xsl:variable name="RemindValue" select="CalendarNotifyDaysPrior"></xsl:variable>
              <xsl:variable name="StartDateName" select="concat('StartDate_', $KeyValue)"></xsl:variable>
              <xsl:variable name="EventType" select="EventType"/>
              <xsl:variable name="PastCalEventDetails" select="concat('PastCalEventDetails_', $KeyValue)"/>
              <xsl:variable name="ImagePastCalEventDetails" select="concat('ImagePastCalEventDetails_', $KeyValue)"/>

              <TR>
                <td CLASS="data showHideLinks" valign="top" border="0">
                  <a href="javascript:void(0);" onclick="javascript:showHideData('{$PastCalEventDetails}','{$ImagePastCalEventDetails}')" class="plusMinusImage" title="Show Hide Link">
                    <img id="{$ImagePastCalEventDetails}" src="javascript/committee/open.gif" border="0" alt="Open"/>
                  </a>

                  <xsl:choose>
                    <xsl:when test="$EventType = '0'">
                      <a href="javascript:void(0);" class="fielddef" onclick="javascript:showHideData('{$PastCalEventDetails}','{$ImagePastCalEventDetails}')" title="{CompanyCalendarTitle}">
                        <xsl:value-of select="CompanyCalendarTitle"/>
                      </a>
                    </xsl:when>
                    <xsl:when test="$EventType = '1'">
                      <a href="javascript:void(0);" class="fielddef" onclick="javascript:showHideData('{$PastCalEventDetails}','{$ImagePastCalEventDetails}')" title="{CompanyCalendarTitle}">
                        <xsl:value-of select="CompanyCalendarTitle"/>
                      </a>
                    </xsl:when>
                    <xsl:when test="$EventType = '2'">
                      <a href="javascript:void(0);" class="fielddef" onclick="javascript:showHideData('{$PastCalEventDetails}','{$ImagePastCalEventDetails}')" title="{CompanyCalendarTitle}">
                        <xsl:value-of select="CompanyCalendarTitle"/>
                      </a>
                    </xsl:when>
                    <xsl:when test="$EventType = '3'">
                      <a href="javascript:void(0);" class="fielddef" onclick="javascript:showHideData('{$PastCalEventDetails}','{$ImagePastCalEventDetails}')" title="{CompanyCalendarTitle}">
                        <xsl:value-of select="CompanyCalendarTitle"/>
                      </a>
                    </xsl:when>
                    <xsl:otherwise>
                      <a href="javascript:void(0);" class="fielddef" onclick="javascript:showHideData('{$PastCalEventDetails}','{$ImagePastCalEventDetails}')" title="{CompanyCalendarTitle}">
                        <xsl:value-of select="CompanyCalendarTitle"/>
                      </a>
                    </xsl:otherwise>
                  </xsl:choose>
                </td>
                <xsl:choose>
                  <xsl:when test="$EndDate != '' and CompanyCalendarDate != CompanyCalendarDateEnd">
                    <td CLASS="data" align="right" valign="top">
                      <xsl:value-of select="$StartDay"/>&#160;-&#160;
                      <xsl:value-of select="$EndDay"/>
                      <xsl:if test="$EndTime != '12:00:00 AM'">
                        <br/>
                        <xsl:value-of select="$EndTime"/>
                      </xsl:if>
                    </td>
                  </xsl:when>
                  <xsl:otherwise>
                    <td CLASS="data" align="right" valign="top">
                      <xsl:value-of select="$StartDay"/>
                    </td>
                  </xsl:otherwise>
                </xsl:choose>
              </TR>
              <tr>
                <td valign="top" class="data" colspan="2">
                  <div id="{$PastCalEventDetails}" style="display:none;padding-left: 15px">
                    <xsl:call-template name="CalendarDetail">
                      <xsl:with-param name="paramEventKey" select="$KeyValue" />
                      <xsl:with-param name="paramEventType">
                        <xsl:choose>
                          <xsl:when test="$EventType = '0'">
                            <xsl:value-of select="3"/>
                          </xsl:when>
                          <xsl:when test="$EventType = '1'">
                            <xsl:value-of select="2"/>
                          </xsl:when>
                          <xsl:when test="$EventType = '2'">
                            <xsl:value-of select="1"/>
                          </xsl:when>
                          <xsl:when test="$EventType = '3'">
                            <xsl:value-of select="4"/>
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:value-of select="-1"/>
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:with-param>
                    </xsl:call-template>
                  </div>
                </td>
              </tr>
            </xsl:when>
          </xsl:choose>
        </xsl:for-each>
        <INPUT TYPE="hidden" NAME="EventCount">
          <xsl:attribute name="value">
            <xsl:value-of select="PastCalendar/CompanyRowCount"/>
          </xsl:attribute>
        </INPUT>
      </xsl:if>
    </table>
    <br />
  </xsl:template>

  <!-- Template ONE Ends here. -->


  <!-- This is Template 3  -->
  <xsl:template name="Calendar_Title3">
    <xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
    <TR>
      <TD CLASS="title2colordark titletest" nowrap="" valign="top" align="left">
        <xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/>
        <br />
        <IMG SRC="" WIDTH="2" HEIGHT="1" alt="{$CompanyNameCaps}" />
        <span class="title2colordark titletest2">
          <xsl:value-of select="$CompanyNameCaps"/>&#160;(<xsl:value-of select="Company/Exchange"/>&#160;-&#160;<xsl:value-of select="Company/Ticker"/>)&#160;
        </span>
      </TD>

      <TD CLASS="" nowrap="" align="right" valign="top">
        <table width="100%" cellpadding="0" cellspacing="0">
          <tr>
            <td class="SURR DATASHADE">
              <TABLE>
                <xsl:call-template name="Calendar_EmailNotification2"/>
              </TABLE>
            </td>
          </tr>
        </table>
      </TD>
    </TR>
    <TR class="default" align="left">
      <TD class="colorlight" colspan="2">
        <span class="default">
          <xsl:value-of select="Company/Header" disable-output-escaping="yes"/>
        </span>
      </TD>
    </TR>

  </xsl:template>


  <xsl:template name="Calendar_EmailNotification3">
    <xsl:variable name="KeyInstn" select="Company/KeyInstn"/>
    <xsl:variable name="Action" select="Calendar/ActionDescription"/>
    <xsl:variable name="SelectError" select="'SelectError'"/>
    <xsl:variable name="RemindError" select="'RemindError'"/>
    <xsl:variable name="UpdateError" select="'UpdateError'"/>
    <xsl:variable name="Success" select="'Success'"/>
    <xsl:for-each select="CLIENTEMAILPREFS[NotificationType = '3' and NotificationEnabled = 'True']">
      <TR>
        <TD colspan="3" CLASS="default">
          <a>
            <xsl:attribute name="href">
              email.aspx?IID=<xsl:value-of select="../Company/KeyInstn"/>
            </xsl:attribute>Notify me of new Company events</a>.
        </TD>
      </TR>
    </xsl:for-each>
    <xsl:for-each select="CLIENTEMAILPREFS[NotificationType = '4' and NotificationEnabled = 'True']">
      <TR>
        <TD colspan="3" CLASS="default">
          <a>
            <xsl:attribute name="href">
              email.aspx?IID=<xsl:value-of select="../Company/KeyInstn"/>
            </xsl:attribute>Send me reminders of Company events</a>.
        </TD>
      </TR>
    </xsl:for-each>

    <TR>
      <TD colspan="3" CLASS="" valign="bottom">
        <xsl:choose>
          <xsl:when test="$Action = $SelectError">
            <span CLASS="datared B">The Days Before field for one of your selections produces an invalid Email Reminder send date.  Please correct.</span>
          </xsl:when>
          <xsl:when test="$Action = $RemindError">
            <span CLASS="datared B">We apologize, but this feature is currently unavailable.  Please attempt your request at a later time.</span>
          </xsl:when>
          <xsl:when test="$Action = $UpdateError">
            <span CLASS="datared B">We apologize, but this feature is currently unavailable.  Please attempt your request at a later time.</span>
          </xsl:when>
          <xsl:when test="$Action = $Success">
            <span CLASS="defaultbold">Thank you!  Your request has processed successfully.</span>
          </xsl:when>
        </xsl:choose>
      </TD>
    </TR>
  </xsl:template>



  <xsl:template name="Calendar_EmailAddress3">
    <xsl:variable name="Action" select="Calendar/ActionDescription"/>
    <xsl:variable name="EmailError" select="'EmailError'"/>
    <xsl:choose>
      <xsl:when test="$Action = $EmailError">
        <TR>
          <TD COLSPAN="2" CLASS="colorlight">
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </TD>
          <TD CLASS="colorlight">
            <SPAN CLASS="datared">An incorrect email address was entered.</SPAN>
          </TD>
        </TR>
      </xsl:when>
    </xsl:choose>
    <xsl:variable name="RowCount" select="Calendar/CompanyRowCount"/>
    <xsl:variable name="RowCountEmpty" select="0"/>
    <xsl:choose>
      <xsl:when test="$RowCount != $RowCountEmpty">
      </xsl:when>
    </xsl:choose>
  </xsl:template>



  <xsl:template name="Calendar_ErrMsg3">
    <TR>
      <TD colspan="3">
        <span class="defaultbold">
          <xsl:value-of select="EmailError/Message"/>
        </span>
      </TD>
    </TR>
  </xsl:template>



  <xsl:template name="Calendar_Data3">
    <xsl:variable name="RowCount" select="Calendar/CompanyRowCount"/>
    <xsl:variable name="RowCountEmpty" select="0"/>
    <table border="0" cellspacing="0" cellpadding="3" width="100%" class="table2 calendarDataTable">
      <TR>
        <TD CLASS="datashade table1_item" align="left" valign="bottom">
          <span class="defaultbold">Event</span>
        </TD>
        <xsl:if test="$RowCount != $RowCountEmpty">
          <TD CLASS="datashade table1_item" align="right" valign="bottom" colspan="4">
            <span class="defaultbold">Date</span>
          </TD>
        </xsl:if>
      </TR>
      <xsl:variable name="KeyInstnValue" select="Company/KeyInstn"/>
      <xsl:choose>
        <xsl:when test="$RowCount != $RowCountEmpty ">
        </xsl:when>
        <xsl:otherwise>
          <TR>
            <TD COLSPAN="5" CLASS="data">
              <strong>There are no events currently scheduled.</strong>
            </TD>
          </TR>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:for-each select="Calendar">
        <xsl:choose>

          <xsl:when test="$RowCount != $RowCountEmpty">
            <xsl:variable name="KeyValue" select="Key"/>
            <xsl:variable name="CompanyCalendarTitle" select="CompanyCalendarTitle"/>

            <xsl:variable name="StartDate" select="CompanyCalendarDate"/>
            <xsl:variable name="StartDay" select="substring-before($StartDate,' ')"/>
            <xsl:variable name="StartTime" select="substring-after($StartDate,' ')"/>

            <xsl:variable name="EndDate" select="CompanyCalendarDateEnd"/>
            <xsl:variable name="EndDay" select="substring-before($EndDate,' ')"/>
            <xsl:variable name="EndTime" select="substring-after($EndDate,' ')"/>

            <xsl:variable name="StartDateET" select="CompanyCalendarDateET"/>
            <xsl:variable name="EndDateET" select="CompanyCalendarDateEndET"/>
            
            <xsl:variable name="EventName" select="concat('Event_', $KeyValue)"></xsl:variable>
            <xsl:variable name="RemindName" select="concat('Remind_', $KeyValue)"></xsl:variable>
            <xsl:variable name="RemindValue" select="CalendarNotifyDaysPrior"></xsl:variable>
            <xsl:variable name="StartDateName" select="concat('StartDate_', $KeyValue)"></xsl:variable>
            <xsl:variable name="EventType" select="EventType"/>
            <xsl:variable name="CalEventDetails" select="concat('CalEventDetails_', $KeyValue)"/>
            <xsl:variable name="ImageCalEventDetails" select="concat('ImageCalEventDetails_', $KeyValue)"/>


            <!-- Meridiem variable Print (AM/PM) value -->
            <xsl:variable name="Meridiem" select="substring-after($StartTime,' ')"/>

            <TR>
              <td CLASS="data showHideLinks" valign="top" border="0">
                <a href="javascript:void(0);" onclick="javascript:showHideData('{$CalEventDetails}','{$ImageCalEventDetails}')" class="plusMinusImage" title="Show hide Link">
                  <img id="{$ImageCalEventDetails}" src="javascript/committee/open.gif" border="0" align="left" alt="Open"/>
                </a>

                <xsl:choose>
                  <xsl:when test="$EventType = '0'">
                    <a href="javascript:void(0);" class="fielddef" onclick="javascript:showHideData('{$CalEventDetails}','{$ImageCalEventDetails}')" title="{CompanyCalendarTitle}">
                      <xsl:value-of select="CompanyCalendarTitle"/>
                    </a>
                  </xsl:when>
                  <xsl:when test="$EventType = '1'">
                    <a href="javascript:void(0);" class="fielddef" onclick="javascript:showHideData('{$CalEventDetails}','{$ImageCalEventDetails}')" title="{CompanyCalendarTitle}">
                      <xsl:value-of select="CompanyCalendarTitle"/>
                    </a>
                  </xsl:when>
                  <xsl:when test="$EventType = '2'">
                    <a href="javascript:void(0);" class="fielddef" onclick="javascript:showHideData('{$CalEventDetails}','{$ImageCalEventDetails}')" title="{CompanyCalendarTitle}">
                      <xsl:value-of select="CompanyCalendarTitle"/>
                    </a>
                  </xsl:when>
                  <xsl:when test="$EventType = '3'">
                    <a href="javascript:void(0);" class="fielddef" onclick="javascript:showHideData('{$CalEventDetails}','{$ImageCalEventDetails}')" title="{CompanyCalendarTitle}">
                      <xsl:value-of select="CompanyCalendarTitle"/>
                    </a>
                  </xsl:when>
                  <xsl:otherwise>
                    <a href="javascript:void(0);" class="fielddef" onclick="javascript:showHideData('{$CalEventDetails}','{$ImageCalEventDetails}')" title="{CompanyCalendarTitle}">
                      <xsl:value-of select="CompanyCalendarTitle"/>
                    </a>
                  </xsl:otherwise>
                </xsl:choose>
              </td>
              <xsl:choose>
                <xsl:when test="$EndDate != '' and CompanyCalendarDate != CompanyCalendarDateEnd">
                  <td CLASS="data" align="right" valign="top">
                    <xsl:value-of select="$StartDay"/>
                    <xsl:if test="$StartTime != '12:00:00 AM' and $StartTime != ''">
                      <br/>
                      <!-- Code Display Hour and Minute and Hide Secounds -->
                      <xsl:choose>
                        <xsl:when test="string-length($StartTime) = '10'">
                          <xsl:value-of select="substring($StartTime, 0, 5)"/>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:value-of select="substring($StartTime, 0, 6)"/>
                        </xsl:otherwise>
                      </xsl:choose>&#160;<xsl:value-of select="$Meridiem"/>
                      &#160;<xsl:value-of select="TimeZoneLabel"/>
                    </xsl:if>&#160;-&#160;
                    <xsl:value-of select="$EndDay"/>
                    <xsl:if test="$EndTime != '12:00:00 AM' and $EndTime != ''">
                      <br/>
                      <xsl:value-of select="$EndTime"/>
                      &#160;<xsl:value-of select="TimeZoneLabel"/>
                    </xsl:if>
                    <span>
                      <br/>
                      <A href="CalendarItem.ashx?keyInstn={$KeyInstnValue}&amp;keyEvent={$KeyValue}&amp;eventType={$EventType}&amp;eventTitle={$CompanyCalendarTitle}&amp;dtStart={$StartDateET}&amp;dtEnd={$EndDateET}"
                        onMouseOver="window.status='Click here to add to iCalendar compliant calendars (ex.Outlook 2000 &amp; above, Netscape, Mozilla, etc.).'; return true" onfocus="window.status='Click here to add to iCalendar compliant calendars (ex.Outlook 2000 &amp; above, Netscape, Mozilla, etc.).'; return true"
                        onMouseOut="window.status=''; return true" onblur="window.status=''; return true" title="Add to Calendar">[Add to Calendar]</A>
                    </span>
                  </td>
                </xsl:when>
                <xsl:otherwise>
                  <td CLASS="data" align="right" valign="top">
                    <xsl:value-of select="$StartDay"/>
                    <xsl:if test="$StartTime != '12:00:00 AM' and $StartTime != ''">
                      <br/>
                      <!-- Code Display Hour and Minute and Hide Secounds -->
                      <xsl:choose>
                        <xsl:when test="string-length($StartTime) = '10'">
                          <xsl:value-of select="substring($StartTime, 0, 5)"/>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:value-of select="substring($StartTime, 0, 6)"/>
                        </xsl:otherwise>
                      </xsl:choose>&#160;<xsl:value-of select="$Meridiem"/>
                      &#160;<xsl:value-of select="TimeZoneLabel"/>
                    </xsl:if>
                    <span>
                      <br/>
                      <A href="CalendarItem.ashx?keyInstn={$KeyInstnValue}&amp;keyEvent={$KeyValue}&amp;eventType={$EventType}&amp;eventTitle={$CompanyCalendarTitle}&amp;dtStart={$StartDateET}"
                        onMouseOver="window.status='Click here to add to iCalendar compliant calendars (ex.Outlook 2000 &amp; above, Netscape, Mozilla, etc.).'; return true" onfocus="window.status='Click here to add to iCalendar compliant calendars (ex.Outlook 2000 &amp; above, Netscape, Mozilla, etc.).'; return true"
                        onMouseOut="window.status=''; return true" onblur="window.status=''; return true" title="Add to Calendar">[Add to Calendar]</A>
                    </span>
                  </td>
                </xsl:otherwise>
              </xsl:choose>
            </TR>
            <tr>
              <td colspan="2" valign="top" class="data">
                <div id="{$CalEventDetails}" style="display:none;padding-left: 15px">
                  <xsl:call-template name="CalendarDetail">
                    <xsl:with-param name="paramEventKey" select="$KeyValue" />
                    <xsl:with-param name="paramEventType">
                      <xsl:choose>
                        <xsl:when test="$EventType = '0'">
                          <xsl:value-of select="3"/>
                        </xsl:when>
                        <xsl:when test="$EventType = '1'">
                          <xsl:value-of select="2"/>
                        </xsl:when>
                        <xsl:when test="$EventType = '2'">
                          <xsl:value-of select="1"/>
                        </xsl:when>
                        <xsl:when test="$EventType = '3'">
                          <xsl:value-of select="4"/>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:value-of select="-1"/>
                        </xsl:otherwise>
                      </xsl:choose>
                    </xsl:with-param>
                  </xsl:call-template>
                </div>
              </td>
              <td>
              </td>
            </tr>

          </xsl:when>
        </xsl:choose>
      </xsl:for-each>
      <INPUT TYPE="hidden" NAME="EventCount" class="input datashade">
        <xsl:attribute name="value">
          <xsl:value-of select="Calendar/CompanyRowCount"/>
        </xsl:attribute>
      </INPUT>
    </table>
    <br />
  </xsl:template>

  <xsl:template name="PastCalendar_Data3">
    <table border="0" cellspacing="0" cellpadding="3" width="100%" class="table2">
      <xsl:variable name="DisplayPast" select="PastCalendar/DisplayPast"/>
      <xsl:variable name="DoDisplayPast" select="1"/>
      <xsl:if test="$DisplayPast = $DoDisplayPast ">
        <xsl:variable name="RowCount" select="PastCalendar/CompanyRowCount"/>
        <xsl:variable name="RowCountEmpty" select="0"/>

        <TR>
          <TD CLASS="datashade table1_item" align="left" valign="bottom">
            <span class="defaultbold">Past Event</span>
          </TD>
          <xsl:if test="$RowCount != $RowCountEmpty">
            <TD CLASS="datashade table1_item" valign="bottom" align="right">
              <span class="defaultbold">Date</span>
            </TD>
          </xsl:if>
        </TR>

        <xsl:variable name="KeyInstnValue" select="Company/KeyInstn"/>
        <xsl:choose>
          <xsl:when test="$RowCount = $RowCountEmpty">
            <TR>
              <TD CLASS="data">
                <strong>There were no events scheduled within the set timeframe.</strong>
              </TD>
            </TR>
          </xsl:when>

        </xsl:choose>
        <xsl:for-each select="PastCalendar">

          <xsl:choose>
            <xsl:when test="$RowCount != $RowCountEmpty">
              <xsl:variable name="CompanyCalendarTitle" select="CompanyCalendarTitle"/>
              <xsl:variable name="KeyValue" select="Key"/>

              <xsl:variable name="StartDate" select="CompanyCalendarDate"/>
              <xsl:variable name="StartDay" select="substring-before($StartDate,' ')"/>
              <xsl:variable name="StartTime" select="substring-after($StartDate,' ')"/>

              <xsl:variable name="EndDate" select="CompanyCalendarDateEnd"/>
              <xsl:variable name="EndDay" select="substring-before($EndDate,' ')"/>
              <xsl:variable name="EndTime" select="substring-after($EndDate,' ')"/>

              <xsl:variable name="EventName" select="concat('Event_', $KeyValue)"></xsl:variable>
              <xsl:variable name="RemindName" select="concat('Remind_', $KeyValue)"></xsl:variable>
              <xsl:variable name="RemindValue" select="CalendarNotifyDaysPrior"></xsl:variable>
              <xsl:variable name="StartDateName" select="concat('StartDate_', $KeyValue)"></xsl:variable>
              <xsl:variable name="EventType" select="EventType"/>
              <xsl:variable name="PastCalEventDetails" select="concat('PastCalEventDetails_', $KeyValue)"/>
              <xsl:variable name="ImagePastCalEventDetails" select="concat('ImagePastCalEventDetails_', $KeyValue)"/>


              <TR>
                <td CLASS="data showHideLinks" valign="top" border="0">
                  <a href="javascript:void(0);" onclick="javascript:showHideData('{$PastCalEventDetails}','{$ImagePastCalEventDetails}')" class="plusMinusImage" title="Show Hide Link">
                    <img id="{$ImagePastCalEventDetails}" src="javascript/committee/open.gif" border="0" alt="Open"/>
                  </a>

                  <xsl:choose>
                    <xsl:when test="$EventType = '0'">
                      <a href="javascript:void(0);" class="fielddef" onclick="javascript:showHideData('{$PastCalEventDetails}','{$ImagePastCalEventDetails}')" title="{CompanyCalendarTitle}" >
                        <xsl:value-of select="CompanyCalendarTitle"/>
                      </a>
                    </xsl:when>
                    <xsl:when test="$EventType = '1'">
                      <a href="javascript:void(0);" class="fielddef" onclick="javascript:showHideData('{$PastCalEventDetails}','{$ImagePastCalEventDetails}')" title="{CompanyCalendarTitle}">
                        <xsl:value-of select="CompanyCalendarTitle"/>
                      </a>
                    </xsl:when>
                    <xsl:when test="$EventType = '2'">
                      <a href="javascript:void(0);" class="fielddef" onclick="javascript:showHideData('{$PastCalEventDetails}','{$ImagePastCalEventDetails}')" title="{CompanyCalendarTitle}">
                        <xsl:value-of select="CompanyCalendarTitle"/>
                      </a>
                    </xsl:when>
                    <xsl:when test="$EventType = '3'">
                      <a href="javascript:void(0);" class="fielddef" onclick="javascript:showHideData('{$PastCalEventDetails}','{$ImagePastCalEventDetails}')" title="{CompanyCalendarTitle}">
                        <xsl:value-of select="CompanyCalendarTitle"/>
                      </a>
                    </xsl:when>
                    <xsl:otherwise>
                      <a href="javascript:void(0);" class="fielddef" onclick="javascript:showHideData('{$PastCalEventDetails}','{$ImagePastCalEventDetails}')" title="{CompanyCalendarTitle}">
                        <xsl:value-of select="CompanyCalendarTitle"/>
                      </a>
                    </xsl:otherwise>
                  </xsl:choose>
                </td>
                <xsl:choose>
                  <xsl:when test="$EndDate != '' and CompanyCalendarDate != CompanyCalendarDateEnd">
                    <td CLASS="data" align="right" valign="top">
                      <xsl:value-of select="$StartDay"/>&#160;-&#160;
                      <xsl:value-of select="$EndDay"/>
                      <xsl:if test="$EndTime != '12:00:00 AM'">
                        <br/>
                        <xsl:value-of select="$EndTime"/>
                      </xsl:if>
                    </td>

                  </xsl:when>
                  <xsl:otherwise>
                    <td CLASS="data" align="right" valign="top">
                      <xsl:value-of select="$StartDay"/>
                    </td>

                  </xsl:otherwise>
                </xsl:choose>
              </TR>
              <tr>
                <td colspan="2" valign="top" class="data">
                  <div id="{$PastCalEventDetails}" style="display:none;padding-left: 15px">
                    <xsl:call-template name="CalendarDetail">
                      <xsl:with-param name="paramEventKey" select="$KeyValue" />
                      <xsl:with-param name="paramEventType">
                        <xsl:choose>
                          <xsl:when test="$EventType = '0'">
                            <xsl:value-of select="3"/>
                          </xsl:when>
                          <xsl:when test="$EventType = '1'">
                            <xsl:value-of select="2"/>
                          </xsl:when>
                          <xsl:when test="$EventType = '2'">
                            <xsl:value-of select="1"/>
                          </xsl:when>
                          <xsl:when test="$EventType = '3'">
                            <xsl:value-of select="4"/>
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:value-of select="-1"/>
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:with-param>
                    </xsl:call-template>
                  </div>
                </td>
              </tr>
            </xsl:when>
          </xsl:choose>
        </xsl:for-each>
        <INPUT TYPE="hidden" NAME="EventCount">
          <xsl:attribute name="value">
            <xsl:value-of select="PastCalendar/CompanyRowCount"/>
          </xsl:attribute>
        </INPUT>
      </xsl:if>
    </table>
    <br />
  </xsl:template>




  <!-- Template 3 Ends here. -->

  <!-- Main Calendar Detail Starts here-->

  <xsl:template name="CalendarDetail" >
    <xsl:param name="paramEventKey" />
    <xsl:param name="paramEventType" />
    <xsl:if test="$paramEventType != '-1'">

      <xsl:for-each select="../CalendarDetail">       
        <xsl:if test="$paramEventType = EventType and $paramEventKey =EventKey">
          <xsl:choose>
            <xsl:when test="$paramEventType = '3'">
              <xsl:call-template name="CalendarDetail_Conference">
                <xsl:with-param name="confEventKey" select="$paramEventKey" />
                <xsl:with-param name="confEventType"  select="$paramEventType"/>
              </xsl:call-template>
            </xsl:when>
            <xsl:when test="$paramEventType = '2'">             
                <xsl:call-template name="CalendarDetail_ConferenceCall">                  
                  <xsl:with-param name="confCallEventKey" select="$paramEventKey" />              
                  
                  <xsl:with-param  name="confConferenceCallPhone" select="ConferenceCallPhone" />
                  <xsl:with-param  name="confConferenceCallURL" select="ConferenceCallURL" />
                  <xsl:with-param  name="confPersonName" select="PersonName" />
                  <xsl:with-param name="confTitle" select="Title"/>
                  <xsl:with-param name="confPhonePublished" select="PhonePublished"/>
                  <xsl:with-param name="confEmail" select="Email"/>
                  <xsl:with-param name="confCallEventType"  select="$paramEventType"/>
                  
                </xsl:call-template>              
            </xsl:when>
            <xsl:when test="$paramEventType = '1'">
              <xsl:call-template name="CalendarDetail_EarningsRelease">
                <xsl:with-param name="erEventKey" select="$paramEventKey" />
                <xsl:with-param  name="erExpectedEarningsRelease" select="ExpectedEarningsRelease" />
                <xsl:with-param  name="erExpectedEarningsReleaseEnds" select="ExpectedEarningsReleaseEnds" />
                <xsl:with-param  name="erDateEndedStandard" select="DateEndedStandard" />
                <xsl:with-param  name="erConferenceCallDetails" select="ConferenceCallDetails" />                
                <xsl:with-param name="erEventType"  select="$paramEventType"/>
              </xsl:call-template>
            </xsl:when>
            <xsl:when test="$paramEventType = '4'">
              <xsl:call-template name="CalendarDetail_CompanyMeeting">
                <xsl:with-param name="companyEventKey" select="$paramEventKey" />
                <xsl:with-param name="companyEventType"  select="$paramEventType"/>
              </xsl:call-template>
            </xsl:when>
          </xsl:choose>
        </xsl:if>       
      </xsl:for-each>
    </xsl:if>
    <xsl:if test="$paramEventType = '-1'">
      <xsl:for-each select="../CalendarDescription">
        <xsl:if test="$paramEventKey = Key">
          <xsl:call-template name="CalDesc_Detail"/>
        </xsl:if>
      </xsl:for-each>
    </xsl:if>
  </xsl:template>

  <!-- Main Calendar Detail Ends here-->
  <!-- Sub Calendar Detail : Conference Starts here-->

  <xsl:template name="CalendarDetail_Conference">
    <xsl:param name="confEventKey" />
    <xsl:param name="confEventType" />

    <table border="0" cellpadding="3" cellspacing="0" width="100%">
      <tr>
          <td class="data defaultbold" valign="top" align="left" colspan="2">Event Details</td>
      </tr>      
      <xsl:if test="Conference = '' and ConferenceURL = '' and ShortName = '' and KeyConferenceType ='' and ConferenceVenue ='' and City ='' and State ='' and  PersonName = '' and Title = '' and PhonePublished = '' and Email = '' and (../ConferencePresenters = '')">
        <tr>
          <td class="data" valign="top" align="left" width="100%">No details are available for this event.</td>
        </tr>
      </xsl:if>
      <tr>
        <td class="data" valign="top" align="left" width="125px">
          <span class="defaultbold">Conference:</span>
        </td>
        <td class="data" valign="top" align="left" width="100%">
          <xsl:value-of disable-output-escaping="yes" select="Conference" />
        </td>
      </tr>
      <tr>
        <td class="data" valign="top" align="left" width="125px">
          <span class="defaultbold">Website:</span>
        </td>
        <td class="data" valign="top" align="left" width="100%">
          <a target="_new" href="{ConferenceURL}" title="{ConferenceURL}">
            <xsl:value-of disable-output-escaping="yes" select="ConferenceURL"/>
          </a>
        </td>
      </tr>
      <tr>
        <td class="data" valign="top" align="left" width="125px">
          <span class="defaultbold">Host Company:</span>
        </td>
        <td class="data" valign="top" align="left" width="100%">
          <xsl:value-of disable-output-escaping="yes" select="ShortName" />
        </td>
      </tr>
      <tr>
        <td class="data" valign="top" align="left" width="125px" nowrap="">
          <span class="defaultbold">Conference Type:</span>
        </td>
        <td class="data" valign="top" align="left" width="100%">
          <xsl:value-of disable-output-escaping="yes" select="KeyConferenceType" />
        </td>
      </tr>
      <tr>
        <td class="data" valign="top" align="left" width="125px">
          <span class="defaultbold">Location:</span>
        </td>
        <td class="data" valign="top" align="left" width="100%">
          <xsl:value-of disable-output-escaping="yes" select="ConferenceVenue" /><br/>
          <xsl:value-of disable-output-escaping="yes" select="City"/>,&#160;<xsl:value-of disable-output-escaping="yes" select="State"/>
        </td>
      </tr>
      <tr>
        <td class="data" valign="top" align="left" width="125px">
          <span class="defaultbold">Contact:</span>
        </td>
        <td class="data" valign="top" align="left" width="100%">
          <xsl:if test="PersonName != ''">
            <xsl:value-of disable-output-escaping="yes" select="PersonName"/>
            <br/>
          </xsl:if>
          <xsl:if test="Title != ''">
            <xsl:value-of disable-output-escaping="yes" select="Title"/>
            <br/>
          </xsl:if>
          <xsl:if test="PhonePublished != ''">
            <xsl:value-of disable-output-escaping="yes" select="PhonePublished"/>
            <br/>
          </xsl:if>
          <xsl:if test="Email != ''">
            <xsl:value-of disable-output-escaping="yes" select="Email"/>
            <br/>
          </xsl:if>
        </td>
      </tr>
      <tr>
        <td class="data" valign="top" align="left" width="125px">
          <span class="defaultbold">Conference Presenters:</span>
        </td>
        <td class="data" valign="top" align="left" width="100%">
          <xsl:for-each select="../ConferencePresenters">
            <xsl:if test="$confEventKey = EventKey and $confEventType =EventType">
              <xsl:value-of disable-output-escaping="yes" select="ShortName"/>
              <xsl:if test="ConferencePresentationDate != '1/1/1000'">
                &#160;@&#160;<xsl:value-of select="ConferencePresentationDate"/>
              </xsl:if>
              <br/>
            </xsl:if>
          </xsl:for-each>
        </td>
      </tr>
    </table>
  </xsl:template>

  <!-- Sub Calendar Detail : Conference Ends here-->
  <!-- Sub Calendar Detail : ConferenceCall Starts here-->

  <xsl:template name="CalendarDetail_ConferenceCall">
    <xsl:param name="confCallEventKey" />
    
    <xsl:param name="confConferenceCallPhone" />
    <xsl:param name="confConferenceCallURL" />
    <xsl:param name="confPersonName" />
    <xsl:param name="confTitle" />
    <xsl:param name="confPhonePublished" />
    <xsl:param name="confEmail" />


    <xsl:param name="confCallEventType" />
   
    <xsl:for-each select="../EPSFFOData">
      <xsl:if test="$confCallEventKey = EventKey">
        <!--confPersonName  <xsl:value-of  select="../CalendarDetail/PersonName" />-->
        <br></br>
        
        
        
        <!--EventKey <xsl:value-of  select="EventKey" />
        confCallEventKey <xsl:value-of  select="$confCallEventKey" />-->
        
    <table border="0" cellpadding="3" cellspacing="0" width="100%">
      <tr>
        <td class="data defaultbold" valign="top" align="left" colspan="2">Event Details</td>
      </tr>
      <xsl:if test="$confConferenceCallPhone = '' and  $confConferenceCallURL = '' and  $confPersonName = '' and 
               (EPSFFOHeader = '')   and (YearAgoActual = '') and  (CurrentReported = '')
            and (ConsensusEstimate = '') and (OverUnder = '') " >
        <tr>
          <td class="data" valign="top" align="left" width="100%">No details are available for this event.</td>
        </tr>
      </xsl:if>     
      <xsl:if test="$confConferenceCallPhone != ''">
        <tr>
          <td class="data" valign="top" align="left" width="125px">
            <span class="defaultbold">Phone:</span>            
          </td>
          <td class="data" valign="top" align="left" width="100%">
            <xsl:value-of disable-output-escaping="yes" select="$confConferenceCallPhone" />           
          </td>
        </tr>
      </xsl:if>      
      <xsl:if test="$confConferenceCallURL != ''">
        <tr>
          <td class="data" valign="top" align="left" width="125px" nowrap="">
            <span class="defaultbold">Webcast URL:</span>
          </td>
          <td class="data" valign="top" align="left" width="100%">
            <a target="_new" href="{$confConferenceCallURL}" title="{confConferenceCallURL}">
              <xsl:value-of disable-output-escaping="yes" select="$confConferenceCallURL" />
            </a>
          </td>
        </tr>
      </xsl:if>
      <xsl:if test="$confPersonName != ''">
        <tr>
          <td class="data" valign="top" align="left" width="125px">
            <span class="defaultbold">Contact:</span>
          </td>
          <td class="data" valign="top" align="left" width="100%">
            <xsl:value-of disable-output-escaping="yes" select="$confPersonName" />
            <br/>
            <xsl:value-of disable-output-escaping="yes" select="$confTitle"/>
            <br/>
            <xsl:value-of disable-output-escaping="yes" select="$confPhonePublished"/>
            <br/>
            <xsl:value-of disable-output-escaping="yes" select="$confEmail"/>
            <br/>
          </td>
        </tr>
      </xsl:if>
      <xsl:variable name="KCCallSub" select="../CalendarDetail[EventKey = $confCallEventKey]/KeyConferenceCallSubject"></xsl:variable>      
      <xsl:if test="$KCCallSub != '1' and $KCCallSub != '2' and $KCCallSub != '4' and $KCCallSub != '5'" >
      <tr>
        <td class="data" valign="top" align="left" colspan="2">
          <table border="0" cellpadding="3" cellspacing="0" width="100%">
            <tr>
              <td class="data" valign="top" align="left" colspan="3">
                <span class="defaultbold">
                  <xsl:value-of select="EPSFFOHeader"/>
                </span>
              </td>
            </tr>
            <xsl:if test="YearAgoActual != ''">
              <tr>
                <td class="data" valign="top" align="left" nowrap="">&#160;&#160;&#160;&#160;</td>
                <td class="data" valign="top" align="left" nowrap="">
                  <span class="defaultbold">Year Ago Actual:</span>
                </td>
                <td class="data" valign="top" align="left" width="100%">
                  <xsl:value-of select="YearAgoActual"/>
                </td>
              </tr>
            </xsl:if>
            <xsl:if test="ConsensusEstimate != ''">
              <tr>
                <td class="data" valign="top" align="left" nowrap="">&#160;&#160;&#160;&#160;</td>
                <td class="data" valign="top" align="left" nowrap="">
                  <span class="defaultbold">Consensus Estimate:</span>
                </td>
                <td class="data" valign="top" align="left" width="100%">
                  <xsl:value-of select="ConsensusEstimate"/>
                </td>
              </tr>
            </xsl:if>
            <xsl:if test="CurrentReported != ''">
              <tr>
                <td class="data" valign="top" align="left" nowrap="">&#160;&#160;&#160;&#160;</td>
                <td class="data" valign="top" align="left" nowrap="">
                  <span class="defaultbold">Current Reported:</span>
                </td>
                <td class="data" valign="top" align="left" width="100%">
                  <xsl:value-of select="CurrentReported"/>
                </td>
              </tr>
            </xsl:if>
            <xsl:if test="OverUnder != ''">
              <tr>
                <td class="data" valign="top" align="left" nowrap="">&#160;&#160;&#160;&#160;</td>
                <td class="data" valign="top" align="left" nowrap="">
                  <span class="defaultbold">Over/Under:</span>
                </td>
                <td class="data" valign="top" align="left" width="100%">
                  <xsl:value-of select="OverUnder"/>
                </td>
              </tr>
            </xsl:if>
          </table>
        </td>
      </tr>
      </xsl:if>
    </table>
        
      </xsl:if>
    </xsl:for-each>
    
  </xsl:template>

  <!-- Sub Calendar Detail : ConferenceCall Ends here-->
  <!-- Sub Calendar Detail : CompanyMeeting Starts here-->

  <xsl:template name="CalendarDetail_CompanyMeeting">
    <table border="0" cellpadding="3" cellspacing="0" width="100%">
      <tr>
        <td class="data defaultbold" valign="top" align="left" colspan="2">Event Details</td>
      </tr>
      <xsl:if test="ConferenceVenue = '' and PersonName = '' and ConferenceCallDetails = ''">
        <tr>
          <td class="data" valign="top" align="left" width="100%">No details are available for this event.</td>
        </tr>
      </xsl:if>
      <xsl:if test="ConferenceVenue != ''">
        <tr>
          <td class="data" valign="top" align="left" width="125px">
            <span class="defaultbold">Location:</span>
          </td>
          <td class="data" valign="top" align="left" width="100%">
            <xsl:value-of disable-output-escaping="yes" select="ConferenceVenue" />
          </td>
        </tr>
        <xsl:if test="CityStatePublished != ''">
          <tr>
            <td class="data" valign="top" align="left" width="125px">
              <span class="defaultbold">Location:</span>
            </td>
            <td class="data" valign="top" align="left" width="100%">
              <xsl:value-of disable-output-escaping="yes" select="CityStatePublished"/>
            </td>
          </tr>
        </xsl:if>
      </xsl:if>
      <xsl:if test="PersonName != ''">
        <tr>
          <td class="data" valign="top" align="left" width="125px">
            <span class="defaultbold">Contact:</span>
          </td>
          <td class="data" valign="top" align="left" width="100%">
            <xsl:value-of disable-output-escaping="yes" select="PersonName"/>
            <br/>
            <xsl:value-of disable-output-escaping="yes" select="Title"/>
            <br/>
            <xsl:value-of disable-output-escaping="yes" select="PhonePublished"/>
            <br/>
            <a>
              <xsl:attribute name="href">
                mailto:<xsl:value-of disable-output-escaping="yes" select="Email"/>
              </xsl:attribute>
              <xsl:value-of disable-output-escaping="yes" select="Email"/>
            </a>
            <br/>
          </td>
        </tr>
      </xsl:if>
      <xsl:if test="ConferenceCallDetails != ''">
        <tr>
          <td class="data" valign="top" align="left" width="125px">
            <span class="defaultbold">Additional Information:</span>
          </td>
          <td class="data" valign="top" align="left" width="100%">
            <xsl:value-of disable-output-escaping="yes" select="ConferenceCallDetails"/>
          </td>
        </tr>
      </xsl:if>
    </table>
  </xsl:template>

  <!-- Sub Calendar Detail : CompanyMeeting Ends here-->
  <!-- Sub Calendar Detail : EarningsRelease Starts here-->

  <xsl:template name="CalendarDetail_EarningsRelease">
    <xsl:param name="erEventKey" />
    <xsl:param name="erExpectedEarningsRelease" />
    <xsl:param name="erExpectedEarningsReleaseEnds" />
    <xsl:param name="erDateEndedStandard" />
    <xsl:param name="erConferenceCallDetails" />
    
    <xsl:for-each select="../EPSFFOData">
      <xsl:if test="$erEventKey = EventKey">
        <!--EventKey <xsl:value-of  select="EventKey" />
        erEventKey <xsl:value-of  select="$erEventKey" />-->

    <table border="0" cellpadding="3" cellspacing="0" width="100%">
      <tr>
        <td class="data defaultbold" valign="top" align="left" colspan="2">Event Details</td>
      </tr>
      <xsl:if test="$erExpectedEarningsRelease = '' and  $erExpectedEarningsReleaseEnds = '' and $erDateEndedStandard = '' and $erConferenceCallDetails = '' 
         and (EPSFFOHeader = '')   and (YearAgoActual = '') and  (CurrentReported = '')
            and (ConsensusEstimate = '') and (OverUnder = '') " >
        <tr>
          <td class="data" valign="top" align="left" width="100%">No details are available for this event.</td>
        </tr>
      </xsl:if>
      <tr>
        <td class="data" valign="top" align="left" colspan="2">
          <span class="defaul">Date:&#160;</span>
          <xsl:value-of disable-output-escaping="yes" select="$erExpectedEarningsRelease"/>
          <xsl:if test="$erExpectedEarningsReleaseEnds != ''">
            &#160;-&#160;<xsl:value-of select="$erExpectedEarningsReleaseEnds"/>
          </xsl:if>
          <span class="defaul">
            <br/>Period Ended:&#160;
          </span>
          <xsl:value-of select="$erDateEndedStandard"/>
          <br/>
          <xsl:value-of disable-output-escaping="yes" select="$erConferenceCallDetails"/>
        </td>
      </tr>
      <tr>
        <td class="data" valign="top" align="left" colspan="2">
          <table border="0" cellpadding="3" cellspacing="0" width="100%">
            <tr>
              <td class="data" valign="top" align="left" colspan="3">
                <span class="defaultbold">
                  <xsl:value-of select="EPSFFOHeader"/>
                </span>
              </td>
            </tr>
            <xsl:if test="YearAgoActual != ''">
              <tr>
                <td class="data" valign="top" align="left" nowrap="">&#160;&#160;&#160;&#160;</td>
                <td class="data" valign="top" align="left" nowrap="">
                  <span class="defaultbold">Year Ago Actual:</span>
                </td>
                <td class="data" valign="top" align="left" width="100%">
                 <xsl:value-of select="YearAgoActual" />
                </td>
              </tr>
            </xsl:if>
            <xsl:if test="ConsensusEstimate != ''">
              <tr>
                <td class="data" valign="top" align="left" nowrap="">&#160;&#160;&#160;&#160;</td>
                <td class="data" valign="top" align="left" nowrap="">
                  <span class="defaultbold">Consensus Estimate:</span>
                </td>
                <td class="data" valign="top" align="left" width="100%">
                  <xsl:value-of select="ConsensusEstimate"/>
                </td>
              </tr>
            </xsl:if>
            <xsl:if test="CurrentReported != ''">
              <tr>
                <td class="data" valign="top" align="left" nowrap="">&#160;&#160;&#160;&#160;</td>
                <td class="data" valign="top" align="left" nowrap="">
                  <span class="defaultbold">Current Reported:</span>
                </td>
                <td class="data" valign="top" align="left" width="100%">
                  <xsl:value-of select="CurrentReported"/>
                </td>
              </tr>
            </xsl:if>
            <xsl:if test="OverUnder != ''">
              <tr>
                <td class="data" valign="top" align="left" nowrap="">&#160;&#160;&#160;&#160;</td>
                <td class="data" valign="top" align="left" nowrap="">
                  <span class="defaultbold">Over/Under:</span>
                </td>
                <td class="data" valign="top" align="left" width="100%">
                  <xsl:value-of select="OverUnder"/>
                </td>
              </tr>
            </xsl:if>
          </table>
        </td>
      </tr>
    </table>

      </xsl:if>
    </xsl:for-each>
  </xsl:template>

  <!-- Sub Calendar Detail : EarningsRelease Ends here-->

  <!-- Main Calendar Description(Calender detail enter through console) : Conference Starts here-->

  <xsl:template name="CalDesc_Detail">
    <table border="0" cellpadding="3" cellspacing="1" width="100%">
      <tr>
          <td class="data defaultbold" valign="top" align="left" colspan="2">Event Details</td>
      </tr>
      <xsl:if test="CompanyCalendarEvent = ''">
      <tr>
        <td class="data" valign="top" align="left" width="100%">No details are available for this event.</td>
      </tr>
      </xsl:if>
      <tr>
        <td class="data" valign="top" align="left" colspan="2">
          <xsl:value-of disable-output-escaping="yes" select="CompanyCalendarEvent" />
        </td>
      </tr>

    </table>
  </xsl:template>

  <!-- Main Calendar Description(Calender detail enter through console) : Conference Ends here-->

  <!-- Dynamic Calendar Starts here-->
  <xsl:template name="DynamicCal">
    <input name="txtcount" id="txtcount" type="hidden" value="{Events/EventCount}"/>
    <script type="text/javascript">
      <![CDATA[
		var Sumcount=0;
		Sumcount=parseInt(document.getElementById('txtcount').value);;
		var events = new Array();
		for (i = 0; i < Sumcount; i++) {
			events[i] = new Array(Sumcount);
		}
		var j=0;
		var pasttitle;
		var pastdate;
		var pastdaterange;
	]]>
    </script>

    <xsl:for-each select="Events">
      <input  type="hidden" value="{EventTitle}">
        <xsl:attribute name="id">
          <xsl:value-of  select="concat('txttitle_', position()-1)"/>
        </xsl:attribute>
        <xsl:attribute name="name">
          <xsl:value-of  select="concat('txttitle_', position()-1)"/>
        </xsl:attribute>
      </input>
      <input type="hidden" value="{EventDate}">
        <xsl:attribute name="id">
          <xsl:value-of  select="concat('txtdate_', position()-1)"/>
        </xsl:attribute>
        <xsl:attribute name="name">
          <xsl:value-of  select="concat('txtdate_', position()-1)"/>
        </xsl:attribute>
      </input>
      <input type="hidden" value="{EventDateRange}">
        <xsl:attribute name="id">
          <xsl:value-of  select="concat('txtdaterange_', position()-1)"/>
        </xsl:attribute>
        <xsl:attribute name="name">
          <xsl:value-of  select="concat('txtdaterange_', position()-1)"/>
        </xsl:attribute>
      </input>

      <script language="JavaScript" type="text/javascript">
        <![CDATA[
			pasttitle=document.getElementById('txttitle_'+ j).value;
			pastdate=document.getElementById('txtdate_'+ j).value;
			pastdaterange=document.getElementById('txtdaterange_'+ j).value;
			
			events[j][0]=pastdate;
			events[j][1]=pasttitle; 
			events[j][2]=pastdaterange; 
			j++; 
		]]>
      </script>
    </xsl:for-each>
    <table id="evtcal" border="0" cellpadding="3" cellspacing="0" width="100%">
      <tr>
        <td align="center" valign="top" width="240px" class="data">
          <div id="calendar">
            &#160;<!--  Dynamically Filled -->
          </div>
        </td>
        <td align="left" valign="top" width="5px" class="data">&#160;</td>
        <td align="left" valign="top" class="data">
          <table border="0" cellpadding="3" cellspacing="0" width="100%" class="data">
            <tr>
              <td class="header" id="eventdate">Calendar Instructions</td>
            </tr>
            <tr>
              <td valign="top">
                <div id="eventlist">Please select a highlighted date from the calendar to view company related information. This calendar requires JavaScript to be installed and activated on your browser.</div>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template name="DynamicCal2">
    <input name="txtcount" id="txtcount" type="hidden" value="{Events/EventCount}"/>
    <script type="text/javascript">
      <![CDATA[
		var Sumcount=0;
		Sumcount=parseInt(document.getElementById('txtcount').value);;
		var events = new Array();
		for (i = 0; i < Sumcount; i++) {
			events[i] = new Array(Sumcount);
		}
		var j=0;
		var pasttitle;
		var pastdate;
		var pastdaterange;
	]]>
    </script>

    <xsl:for-each select="Events">
      <input  type="hidden" value="{EventTitle}">
        <xsl:attribute name="id">
          <xsl:value-of  select="concat('txttitle_', position()-1)"/>
        </xsl:attribute>
        <xsl:attribute name="name">
          <xsl:value-of  select="concat('txttitle_', position()-1)"/>
        </xsl:attribute>
      </input>
      <input type="hidden" value="{EventDate}">
        <xsl:attribute name="id">
          <xsl:value-of  select="concat('txtdate_', position()-1)"/>
        </xsl:attribute>
        <xsl:attribute name="name">
          <xsl:value-of  select="concat('txtdate_', position()-1)"/>
        </xsl:attribute>
      </input>
      <input type="hidden" value="{EventDateRange}">
        <xsl:attribute name="id">
          <xsl:value-of  select="concat('txtdaterange_', position()-1)"/>
        </xsl:attribute>
        <xsl:attribute name="name">
          <xsl:value-of  select="concat('txtdaterange_', position()-1)"/>
        </xsl:attribute>
      </input>

      <script language="JavaScript" type="text/javascript">
        <![CDATA[
			pasttitle=document.getElementById('txttitle_'+ j).value;
			pastdate=document.getElementById('txtdate_'+ j).value;
			pastdaterange=document.getElementById('txtdaterange_'+ j).value;
			
			events[j][0]=pastdate;
			events[j][1]=pasttitle; 
			events[j][2]=pastdaterange; 
			j++; 
		]]>
      </script>
    </xsl:for-each>
    <table id="evtcal" border="0" cellpadding="3" cellspacing="0" width="100%">
      <tr>
        <td align="center" valign="top" width="240px" class="data">
          <div id="calendar">
            &#160;<!--  Dynamically Filled -->
          </div>
        </td>
        <td align="left" valign="top" width="5px" class="data">&#160;</td>
        <td align="left" valign="top" class="data">
          <table border="0" cellpadding="3" cellspacing="0" width="100%" class="data">
            <tr>
              <td class="datashade surr" id="eventdate">
                <span class="defaultbold">Calendar Instructions</span>
              </td>
            </tr>
            <tr>
              <td valign="top">
                <div id="eventlist">Please select a highlighted date from the calendar to view company related information. This calendar requires JavaScript to be installed and activated on your browser.</div>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template name="DynamicCal3">
    <input name="txtcount" id="txtcount" type="hidden" value="{Events/EventCount}"/>
    <script type="text/javascript">
      <![CDATA[
		var Sumcount=0;
		Sumcount=parseInt(document.getElementById('txtcount').value);;
		var events = new Array();
		for (i = 0; i < Sumcount; i++) {
			events[i] = new Array(Sumcount);
		}
		var j=0;
		var pasttitle;
		var pastdate;
		var pastdaterange;
	]]>
    </script>

    <xsl:for-each select="Events">
      <input  type="hidden" value="{EventTitle}">
        <xsl:attribute name="id">
          <xsl:value-of  select="concat('txttitle_', position()-1)"/>
        </xsl:attribute>
        <xsl:attribute name="name">
          <xsl:value-of  select="concat('txttitle_', position()-1)"/>
        </xsl:attribute>
      </input>
      <input type="hidden" value="{EventDate}">
        <xsl:attribute name="id">
          <xsl:value-of  select="concat('txtdate_', position()-1)"/>
        </xsl:attribute>
        <xsl:attribute name="name">
          <xsl:value-of  select="concat('txtdate_', position()-1)"/>
        </xsl:attribute>
      </input>
      <input type="hidden" value="{EventDateRange}">
        <xsl:attribute name="id">
          <xsl:value-of  select="concat('txtdaterange_', position()-1)"/>
        </xsl:attribute>
        <xsl:attribute name="name">
          <xsl:value-of  select="concat('txtdaterange_', position()-1)"/>
        </xsl:attribute>
      </input>

      <script language="JavaScript" type="text/javascript">
        <![CDATA[
			pasttitle=document.getElementById('txttitle_'+ j).value;
			pastdate=document.getElementById('txtdate_'+ j).value;
			pastdaterange=document.getElementById('txtdaterange_'+ j).value;
			
			events[j][0]=pastdate;
			events[j][1]=pasttitle; 
			events[j][2]=pastdaterange; 
			j++; 
		]]>
      </script>
    </xsl:for-each>
    <table id="evtcal" border="0" cellpadding="3" cellspacing="0" width="100%">
      <tr>
        <td align="center" valign="top" width="240px" class="data">
          <div id="calendar">
            &#160;<!--  Dynamically Filled -->
          </div>
        </td>
        <td align="left" valign="top" width="5px" class="data">&#160;</td>
        <td align="left" valign="top" class="data">
          <table border="0" cellpadding="3" cellspacing="0" width="100%" class="data">
            <tr>
              <td class="datashade table2" id="eventdate">
                <span class="defaultbold">Calendar Instructions</span>
              </td>
            </tr>
            <tr>
              <td valign="top">
                <div id="eventlist">Please select a highlighted date from the calendar to view company related information. This calendar requires JavaScript to be installed and activated on your browser.</div>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </xsl:template>
  <!-- Dynamic Calendar  Ends here-->

  <!-- Main XSLT templates are here. these are the templates which are actually being displayed in the page. this is for TOP LEVEL editing. if you dont need to use the individual templates above you can do your main editing here

when pulling THESE templates below you must carry the top comment with them and uncomment them when you drop it into the company specific xslt.




-->
  <xsl:template name="Calendar_Script">

    <!--<script language="JavaScript" type="text/javascript">
    var p= &#39;&#60;&#37;&#61; sXML &#37;&#62;&#39;;
  </script>-->
    <script language="javascript">
      function showHideData(obj,objImage) {
      var el = document.getElementById(obj);
      var elImage = document.getElementById(objImage);
      if (el.style.display != 'none') {
      el.style.display = 'none';
      elImage.src='javascript/committee/open.gif';
      } else {
      el.style.display = '';
      elImage.src='javascript/committee/close.gif';
      }
      }
    </script>
    <style type="text/css">
      .showHideLinks {
      width: 70%;
      }
      .showHideLinks a.plusMinusImage {
      float: left;
      margin-left: 0;
      padding-top: 2px;
      }
      .showHideLinks a {
      display: block;
      margin-left: 15px;
      }
    </style>
    <script language="javascript">
      function DefWindow(KeyInstn, Key) {
      var page = "caldesc.aspx?IID=" + KeyInstn + "&amp;Key=" + Key;
      var winprops = "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=640,height=250";
      window.open(page, "", winprops);
      }
    </script>
    <script language="javascript">
      function DefWindowAlt(KeyInstn, Key, KeyType) {
      var page = "calendardetail.aspx?IID=" + KeyInstn + "&amp;Key=" + Key + "&amp;KeyType=" + KeyType;
      var winprops = "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=640,height=300";
      window.open(page, "", winprops);
      }
    </script>
  </xsl:template>

  <xsl:template match="irw:Calendar">
    <xsl:variable name="TemplateName" select="Company/TemplateName"/>
    <xsl:call-template name="Calendar_Script"/>
    <xsl:choose>
      <!-- Template 2 -->
      <xsl:when test="$TemplateName = 'AltCalendar1'">
        <xsl:call-template name="TemplateONEstylesheet"/>
        <xsl:variable name="KeyInstn" select="Company/KeyInstn"/>
        <table border="0" cellspacing="0" cellpadding="3" width="100%" class="data">
          <xsl:call-template name="Title_S2"/>
          <xsl:variable name="FormAction" select="concat('calendar.aspx?IID=', $KeyInstn)"></xsl:variable>
          <form method="post" id="calendarform" name="calendarform">
            <xsl:attribute name="action">
              <xsl:value-of select="$FormAction"/>
            </xsl:attribute>
            <tr>
              <td class="leftTOPbord" valign="top" colspan="2">
                <table border="0" cellspacing="0" cellpadding="2" width="100%">
                  <tr align="">
                    <td class="data" valign="top">
                      <table border="0" cellspacing="0" cellpadding="4" width="100%">
                        <tr>
                          <td class="data" valign="top">
                            <xsl:call-template name="DynamicCal2"/>
                            <br/>
                            <table border="0" cellspacing="0" cellpadding="3" width="100%">
                              <xsl:call-template name="Calendar_EmailNotification2"/>
                              <xsl:if test="EmailError/Message != ''">
                                <xsl:call-template name="Calendar_ErrMsg2"/>
                              </xsl:if>
                            </table>
                            <br />
                            <xsl:call-template name="Calendar_Data2"/>
                            <xsl:call-template name="PastCalendar_Data2"/>
                            <xsl:call-template name="Calendar_EmailAddress2"/>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <!--<xsl:call-template name="Note_DisplayTime"/>-->
          </form>
          <xsl:call-template name="FooterTag"/>
        </table>
        <xsl:call-template name="Copyright"/>
      </xsl:when>

      <!-- Template 3 -->
      <xsl:when test="$TemplateName = 'AltCalendar3'">
        <xsl:call-template name="TemplateTHREEstylesheet"/>
        <xsl:variable name="KeyInstn" select="Company/KeyInstn"/>
        <xsl:call-template name="Title_T3"/>
        <table border="0" cellspacing="0" cellpadding="0" width="100%" class="table2">
          <xsl:variable name="FormAction" select="concat('calendar.aspx?IID=', $KeyInstn)"></xsl:variable>
          <form method="post" id="calendarform" name="calendarform">
            <xsl:attribute name="action">
              <xsl:value-of select="$FormAction"/>
            </xsl:attribute>
            <tr>
              <td class="data" valign="top">
                <table border="0" cellspacing="0" cellpadding="4" width="100%">
                  <tr>
                    <td class="data">
                      <xsl:call-template name="DynamicCal3"/>
                      <br/>
                      <table border="0" cellspacing="0" cellpadding="3" width="100%">
                        <xsl:call-template name="Calendar_EmailNotification3"/>
                        <xsl:if test="EmailError/Message != ''">
                          <xsl:call-template name="Calendar_ErrMsg3"/>
                        </xsl:if>
                      </table>
                      <br />
                      <xsl:call-template name="Calendar_Data3"/>
                      <xsl:call-template name="PastCalendar_Data3"/>
                      <xsl:call-template name="Calendar_EmailAddress3"/>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <!--<xsl:call-template name="Note_DisplayTime"/>-->
          </form>
          <xsl:call-template name="FooterTag"/>
        </table>
        <xsl:call-template name="Copyright"/>
      </xsl:when>
      <!-- End Template -->

      <!-- Template default -->
      <xsl:otherwise>
        <xsl:variable name="KeyInstn" select="Company/KeyInstn"/>
        <table border="0" cellspacing="0" cellpadding="3" width="100%" class="colordark">
          <xsl:call-template name="Title_S1"/>
          <xsl:variable name="FormAction" select="concat('calendar.aspx?IID=', $KeyInstn)"></xsl:variable>
          <form method="post" id="calendarform" name="calendarform">
            <xsl:attribute name="action">
              <xsl:value-of select="$FormAction"/>
            </xsl:attribute>
            <tr>
              <td class="colordark" colspan="2" valign="top">
                <table border="0" cellspacing="0" cellpadding="2" width="100%">
                  <tr>
                    <td class="colorlight" valign="top">
                      <table border="0" cellspacing="0" cellpadding="4" width="100%">
                        <tr>
                          <td class="colorlight" valign="top">
                            <xsl:call-template name="DynamicCal"/>
                            <br/>
                            <table border="0" cellspacing="0" cellpadding="3" width="100%">
                              <xsl:call-template name="Calendar_EmailNotification"/>
                              <xsl:if test="EmailError/Message != ''">
                                <xsl:call-template name="Calendar_ErrMsg"/>
                              </xsl:if>
                            </table>
                            <br />
                            <xsl:call-template name="Calendar_Data"/>
                            <xsl:call-template name="PastCalendar_Data"/>
                            <xsl:call-template name="Calendar_EmailAddress"/>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <!--<xsl:call-template name="Note_DisplayTime"/>-->
          </form>
          <xsl:call-template name="FooterTag"/>
        </table>
        <xsl:call-template name="Copyright"/>
      </xsl:otherwise>
    </xsl:choose>
    <script language="JavaScript" type="text/javascript" src="javascript/calevents.js"></script>
    <script language="JavaScript" type="text/javascript">
      <![CDATA[
		if ( document.getElementById('calendar') != null){ 
			changedate("return")
		}
	]]>
    </script>
  </xsl:template>
</xsl:stylesheet>
