<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns="http://www.w3.ord/TR/REC-html40"
  xmlns:irw="http://www.snl.com/xml/irw">

<xsl:output method="html" media-type="text/html" indent="no"/>

<!-- Start Default Template here-->

<!--
//This is the default template for the analyst coverage/estimates page. Most changes are to either remove the company name, ticker, or exchange. you can also change the name of the page or remove this row all together.
//These templates below are broken up how the page is displayed. -->

<xsl:template name="HEADER_ac">
<xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>

  <TR>
    <TD CLASS="colordark" nowrap="1" align="left"><SPAN CLASS="title1light"><xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/></SPAN></TD>
    <TD CLASS="colordark" nowrap="1" align="right" valign="top"><span class="subtitlelight"><xsl:value-of select="$CompanyNameCaps"/>&#160;(<xsl:value-of select="Company/Exchange"/>&#160;-&#160;<xsl:value-of select="Company/Ticker"/>)</span><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></TD>
  </TR>

  <TR class="default" align="left">
    <TD class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Header" disable-output-escaping="yes"/></span></TD>
  </TR>

</xsl:template>


<xsl:template name="eps_rec_table">

<xsl:variable name="GAAPDomain" select="Company/GAAP"/>
<xsl:variable name="REAL_ESTATE" select="6"/>
<xsl:variable name="INSURANCE" select="8"/>
<xsl:variable name="INSURANCE_BROKER" select="9"/>
  <tr>
    <td class="colorlight" valign="top">
      <table cellspacing="0" border="0" cellpadding="3" width="100%" class="colorlight">
        <tr>
        <td class="colorlight" colspan="7"><span class="title2">
        <xsl:choose>
        <xsl:when test="$GAAPDomain = $REAL_ESTATE">
        FFO
        </xsl:when>
        <xsl:otherwise>
        EPS
        </xsl:otherwise>
        </xsl:choose>
        Data ($)</span></td>
        </tr>
        <tr valign="bottom" class="colorlight">
          <td class="header" NOWRAP="1" width="14%">Year Ending</td>
          <td class="header" align="right" width="14%">Q1</td>
          <td class="header" align="right" width="14%">Q2</td>
          <td class="header" align="right" width="14%">Q3</td>
          <td class="header" align="right" width="14%">Q4</td>
          <td class="header" align="right" width="14%"><xsl:value-of select="EPSYear/FiscalYearName" /> FY</td>
          <xsl:if test="EPSYear/ShowCalendarYear = 'True'">
            <td class="header" align="right" width="14%">December CY</td>
          </xsl:if>
          <td class="header" align="center" NOWRAP="1" width="16%"># of Analysts<br />Reporting</td>
        </tr>
        <xsl:for-each select="EPSYear">
        <xsl:sort select="YearEnding"/>
          <tr valign="top" class="data">
            <td class="defaultbold"><xsl:value-of select="YearEnding"/></td>

            <td class="data" align="right">
              <xsl:choose>
              <xsl:when test="count(Q1) > 0">
                <xsl:if test="Q1Consensus = 'true'">
                <xsl:attribute name="class">defaultbold</xsl:attribute>
                </xsl:if>
                <xsl:value-of select="Q1"/>
              </xsl:when>
              <xsl:otherwise>
                -
              </xsl:otherwise>
              </xsl:choose>
            </td>

            <td class="data" align="right">
              <xsl:choose>
              <xsl:when test="count(Q2) > 0">
                <xsl:if test="Q2Consensus = 'true'">
                <xsl:attribute name="class">defaultbold</xsl:attribute>
                </xsl:if>
                <xsl:value-of select="Q2"/>
              </xsl:when>
              <xsl:otherwise>
                -
              </xsl:otherwise>
              </xsl:choose>
            </td>

            <td class="data" align="right">
              <xsl:choose>
              <xsl:when test="count(Q3) > 0">
                <xsl:if test="Q3Consensus = 'true'">
                <xsl:attribute name="class">defaultbold</xsl:attribute>
                </xsl:if>
                <xsl:value-of select="Q3"/>
              </xsl:when>
              <xsl:otherwise>
                -
              </xsl:otherwise>
              </xsl:choose>
            </td>

            <td class="data" align="right">
              <xsl:choose>
              <xsl:when test="count(Q4) > 0">
                <xsl:if test="Q4Consensus = 'true'">
                <xsl:attribute name="class">defaultbold</xsl:attribute>
                </xsl:if>
                <xsl:value-of select="Q4"/>
              </xsl:when>
              <xsl:otherwise>
                -
              </xsl:otherwise>
              </xsl:choose>
            </td>

            <td class="data" align="right">
              <xsl:choose>
              <xsl:when test="count(Y) > 0">
                <xsl:if test="YConsensus = 'true'">
                <xsl:attribute name="class">defaultbold</xsl:attribute>
                </xsl:if>
                <xsl:value-of select="Y"/>
              </xsl:when>
              <xsl:otherwise>
                -
              </xsl:otherwise>
              </xsl:choose>
            </td>

            <xsl:if test="ShowCalendarYear = 'True'">
              <td class="data" align="right">
                <xsl:choose>
                <xsl:when test="count(CalendarYear) > 0">
                  <xsl:if test="CYConsensus = 'true'">
                  <xsl:attribute name="class">defaultbold</xsl:attribute>
                  </xsl:if>
                  <xsl:value-of select="CalendarYear"/>
                </xsl:when>
                <xsl:otherwise>
                  -
                </xsl:otherwise>
                </xsl:choose>
              </td>
            </xsl:if>

            <td class="data" align="center">
              <xsl:choose>
              <xsl:when test="count(NumReporting) > 0">
                <xsl:value-of select="NumReporting"/>
              </xsl:when>
              <xsl:otherwise>
                -
              </xsl:otherwise>
              </xsl:choose>
            </td>
          </tr>
        </xsl:for-each>
        <tr>
          <td class="colorlight" colspan="7"><span class="default">NOTE: Actual
          <xsl:choose>
          <xsl:when test="$GAAPDomain = $REAL_ESTATE">
          FFO/Share
          </xsl:when>
          <xsl:when test="$GAAPDomain = $INSURANCE or $GAAPDomain = $INSURANCE_BROKER">
          Operating EPS
          </xsl:when>
          </xsl:choose>
          indicated in regular type,
          <span class="defaultbold">mean consensus estimates in bold.</span><br />
          <span stlye="font-style:italic">Source: S&amp;P Global Market Intelligence</span>
          </span>
          </td>
        </tr>
      </table>
      </td>
    <!--<td class="colorlight"><img src="/images/interactive/blank.gif" width="8" height="8" border="0" /><br /></td>
    <td valign="top">
    <xsl:call-template name="Recommend"/><br />
    </td>-->
  </tr>



  <xsl:if test="$GAAPDomain = $REAL_ESTATE ">
    <xsl:if test="count(AFFOEstimates) > 0">
      <tr>
    <td class="colorlight"><br/>
      <table cellspacing="0" border="0" cellpadding="3" width="100%" class="colorlight">
        <tr>
          <td class="colorlight" colspan="8">
            <span class="title2">AFFO Data ($)</span>
          </td>
        </tr>
        <tr valign="bottom" class="colorlight">
          <td class="header" NOWRAP="1" width="14%">Year Ending</td>
          <td class="header" align="right" width="14%">Q1</td>
          <td class="header" align="right" width="14%">Q2</td>
          <td class="header" align="right" width="14%">Q3</td>
          <td class="header" align="right" width="14%">Q4</td>
          <td class="header" align="right" width="14%"><xsl:value-of select="EPSYear/FiscalYearName" /> FY</td>
          <xsl:if test="EPSYear/ShowCalendarYear = 'True'">
            <td class="header" align="right" width="14%">December CY</td>
          </xsl:if>
          <td class="header" align="center" NOWRAP="1" width="16%"># of Analysts<br />Reporting</td>
        </tr>
        <xsl:for-each select="AFFOEstimates">
        <xsl:sort select="YearEnding"/>
          <tr valign="top" class="data">

            <td class="defaultbold"><xsl:value-of select="YearEnding"/></td>
            <td class="data" align="right">
              <xsl:choose>
              <xsl:when test="count(Q1) > 0">
                <xsl:if test="Q1Consensus = 'true'">
                <xsl:attribute name="class">defaultbold</xsl:attribute>
                </xsl:if>
                <xsl:value-of select="Q1"/>
              </xsl:when>
              <xsl:otherwise>
                -
              </xsl:otherwise>
              </xsl:choose>
            </td>

            <td class="data" align="right">
              <xsl:choose>
              <xsl:when test="count(Q2) > 0">
                <xsl:if test="Q2Consensus = 'true'">
                <xsl:attribute name="class">defaultbold</xsl:attribute>
                </xsl:if>
                <xsl:value-of select="Q2"/>
              </xsl:when>
              <xsl:otherwise>
                -
              </xsl:otherwise>
              </xsl:choose>
            </td>

            <td class="data" align="right">
              <xsl:choose>
              <xsl:when test="count(Q3) > 0">
                <xsl:if test="Q3Consensus = 'true'">
                <xsl:attribute name="class">defaultbold</xsl:attribute>
                </xsl:if>
                <xsl:value-of select="Q3"/>
              </xsl:when>
              <xsl:otherwise>
                -
              </xsl:otherwise>
              </xsl:choose>
            </td>

            <td class="data" align="right">
              <xsl:choose>
              <xsl:when test="count(Q4) > 0">
                <xsl:if test="Q4Consensus = 'true'">
                <xsl:attribute name="class">defaultbold</xsl:attribute>
                </xsl:if>
                <xsl:value-of select="Q4"/>
              </xsl:when>
              <xsl:otherwise>
                -
              </xsl:otherwise>
              </xsl:choose>
            </td>

            <td class="data" align="right">
              <xsl:choose>
              <xsl:when test="count(Y) > 0">
                <xsl:if test="YConsensus = 'true'">
                <xsl:attribute name="class">defaultbold</xsl:attribute>
                </xsl:if>
                <xsl:value-of select="Y"/>
              </xsl:when>
              <xsl:otherwise>
                -
              </xsl:otherwise>
              </xsl:choose>
            </td>

            <xsl:if test="ShowCalendarYear = 'True'">
              <td class="data" align="right">
                <xsl:choose>
                <xsl:when test="count(CalendarYear) > 0">
                  <xsl:if test="CYConsensus = 'true'">
                  <xsl:attribute name="class">defaultbold</xsl:attribute>
                  </xsl:if>
                  <xsl:value-of select="CalendarYear"/>
                </xsl:when>
                <xsl:otherwise>
                  -
                </xsl:otherwise>
                </xsl:choose>
              </td>
            </xsl:if>

            <td class="data" align="center">
              <xsl:choose>
              <xsl:when test="count(NumReporting) > 0">
                <xsl:value-of select="NumReporting"/>
              </xsl:when>
              <xsl:otherwise>
                -
              </xsl:otherwise>
              </xsl:choose>
            </td>
          </tr>
        </xsl:for-each>
      </table>
</td>
  </tr>
    </xsl:if>

    <xsl:if test="count(EPSEstimates) > 0">
  <tr>
    <td class="colorlight"><br />
      <table cellspacing="0" border="0" cellpadding="3" width="100%" class="colorlight">
        <tr>
          <td class="colorlight " colspan="7">
            <span class="title2">EPS Data ($)</span>
          </td>
        </tr>
        <tr valign="bottom" class="colorlight">
          <td class="header" NOWRAP="1" width="14%">Year Ending</td>
          <td class="header" align="right" width="14%">Q1</td>
          <td class="header" align="right" width="14%">Q2</td>
          <td class="header" align="right" width="14%">Q3</td>
          <td class="header" align="right" width="14%">Q4</td>
          <td class="header" align="right" width="14%"><xsl:value-of select="EPSYear/FiscalYearName" /> FY</td>
          <xsl:if test="EPSYear/ShowCalendarYear = 'True'">
            <td class="header" align="right" width="14%">December CY</td>
          </xsl:if>
          <td class="header" align="center" NOWRAP="1" width="16%"># of Analysts<br />Reporting</td>
        </tr>
        <xsl:for-each select="EPSEstimates">
        <xsl:sort select="YearEnding"/>
          <tr valign="top" class="data">
            <td class="defaultbold"><xsl:value-of select="YearEnding"/></td>

            <td class="data" align="right">
              <xsl:choose>
              <xsl:when test="count(Q1) > 0">
                <xsl:if test="Q1Consensus = 'true'">
                <xsl:attribute name="class">defaultbold</xsl:attribute>
                </xsl:if>
                <xsl:value-of select="Q1"/>
              </xsl:when>
              <xsl:otherwise>
                -
              </xsl:otherwise>
              </xsl:choose>
            </td>

            <td class="data" align="right">
              <xsl:choose>
              <xsl:when test="count(Q2) > 0">
                <xsl:if test="Q2Consensus = 'true'">
                <xsl:attribute name="class">defaultbold</xsl:attribute>
                </xsl:if>
                <xsl:value-of select="Q2"/>
              </xsl:when>
              <xsl:otherwise>
                -
              </xsl:otherwise>
              </xsl:choose>
            </td>

            <td class="data" align="right">
              <xsl:choose>
              <xsl:when test="count(Q3) > 0">
                <xsl:if test="Q3Consensus = 'true'">
                <xsl:attribute name="class">defaultbold</xsl:attribute>
                </xsl:if>
                <xsl:value-of select="Q3"/>
              </xsl:when>
              <xsl:otherwise>
                -
              </xsl:otherwise>
              </xsl:choose>
            </td>

            <td class="data" align="right">
              <xsl:choose>
              <xsl:when test="count(Q4) > 0">
                <xsl:if test="Q4Consensus = 'true'">
                <xsl:attribute name="class">defaultbold</xsl:attribute>
                </xsl:if>
                <xsl:value-of select="Q4"/>
              </xsl:when>
              <xsl:otherwise>
                -
              </xsl:otherwise>
              </xsl:choose>
            </td>

            <td class="data" align="right">
              <xsl:choose>
              <xsl:when test="count(Y) > 0">
                <xsl:if test="YConsensus = 'true'">
                <xsl:attribute name="class">defaultbold</xsl:attribute>
                </xsl:if>
                <xsl:value-of select="Y"/>
              </xsl:when>
              <xsl:otherwise>
                -
              </xsl:otherwise>
              </xsl:choose>
            </td>

            <xsl:if test="ShowCalendarYear = 'True'">
              <td class="data" align="right">
                <xsl:choose>
                <xsl:when test="count(CalendarYear) > 0">
                  <xsl:if test="CYConsensus = 'true'">
                  <xsl:attribute name="class">defaultbold</xsl:attribute>
                  </xsl:if>
                  <xsl:value-of select="CalendarYear"/>
                </xsl:when>
                <xsl:otherwise>
                  -
                </xsl:otherwise>
                </xsl:choose>
              </td>
            </xsl:if>

            <td class="data" align="center">
              <xsl:choose>
              <xsl:when test="count(NumReporting) > 0">
                <xsl:value-of select="NumReporting"/>
              </xsl:when>
              <xsl:otherwise>
                -
              </xsl:otherwise>
              </xsl:choose>
            </td>
          </tr>
        </xsl:for-each>
      </table>
</td>
  </tr>
    </xsl:if>

  </xsl:if>
  <!--<tr>
    <td align="center" class="colorlight" colspan="3">
    <a>
    <xsl:variable name="keyinstn" select="Company/KeyInstn" />
    <xsl:variable name="url" select="concat('EstimatesDisclaimer.aspx?IID=', $keyinstn)" />
    <xsl:attribute name="href">
    <xsl:value-of select="$url" />
    </xsl:attribute>Estimates Disclaimer</a><br /><br />
    </td>
  </tr>-->
</xsl:template>

<xsl:template name="Recommend">
<xsl:if test="count(Analyst) &gt; 0">

      <table cellspacing="0" border="0" cellpadding="3" class="" width="100%">
        <tr>
        <td class="title2" colspan="2">Recommendations</td>
        </tr>
        <tr>
          <td colspan="2" class="header" nowrap="1">Analysts' Current<br />Recommendations</td>
        </tr>
      <xsl:for-each select="OpinionSummary">
        <tr class="data">
          <td class="defaultbold" width="50%"><span class="defaultbold"><xsl:value-of select="Recommendation"/></span></td>
          <td class="data" width="50%" align="right"><xsl:value-of select="Count"/></td>
        </tr>
      </xsl:for-each>
      </table>
</xsl:if>
</xsl:template>

<xsl:template name="TransitionInfo">
  <xsl:if test="count(TransitionPage) > 0">
    <table cellspacing="0" border="0" cellpadding="3" class="header" width="100%">
      <tr>
      <td class="colorlight" colspan="4"><span class="title2">Transition Pages</span></td>
      </tr>
      <tr>
        <td class="header" nowrap="1">&#160;</td>
        <td class="header" nowrap="1">Includes Effects of</td>
        <td class="header" nowrap="1">Excludes Effects of</td>
        <td align="right" class="header" nowrap="1"># of Analysts</td>
      </tr>
    <xsl:for-each select="TransitionPage">
      <tr class="data">
        <xsl:if test="Action = 'Apply'">
          <td class="data">
            <a>
              <xsl:attribute name="href">
                analystcoverage.aspx?iid=<xsl:value-of select="../Company/KeyInstn" />&amp;ktb=<xsl:value-of select="KeyTransitionBasis" />
              </xsl:attribute>
              <xsl:value-of select="Action"/>
            </a>
          </td>
        </xsl:if>
        <xsl:if test="Action != 'Apply'">
          <td class="data"><xsl:value-of select="Action"/></td>
        </xsl:if>
        <td class="data" align="left"><xsl:value-of select="IncludeTransitionDesc"/></td>
        <td class="data" align="left"><xsl:value-of select="ExcludeTransitionDesc"/></td>
        <td class="data" align="right"><xsl:value-of select="NumberOfEstimatesInConsensus"/></td>
      </tr>
    </xsl:for-each>
    </table>
  </xsl:if>
</xsl:template>

<xsl:template name="TransitionInfo3">
  <xsl:if test="count(TransitionPage) > 0">
<tr>
<td><br/>
    <table cellspacing="0" border="1" cellpadding="3" class="header" width="100%">
      <tr>
      <td class="colorlight" colspan="4"><span class="title2">Transition Pages</span></td>
      </tr>
      <tr>
        <td class="header" nowrap="1">&#160;</td>
        <td class="header" nowrap="1">Includes Effects of</td>
        <td class="header" nowrap="1">Excludes Effects of</td>
        <td align="right" class="header" nowrap="1"># of Analysts</td>
      </tr>
    <xsl:for-each select="TransitionPage">
      <tr class="data">
        <xsl:if test="Action = 'Apply'">
          <td class="data">
            <a>
              <xsl:attribute name="href">
                analystcoverage.aspx?iid=<xsl:value-of select="../Company/KeyInstn" />&amp;ktb=<xsl:value-of select="KeyTransitionBasis" />
              </xsl:attribute>
              <xsl:value-of select="Action"/>
            </a>
          </td>
        </xsl:if>
        <xsl:if test="Action != 'Apply'">
          <td class="data"><xsl:value-of select="Action"/></td>
        </xsl:if>
        <td class="data" align="left"><xsl:value-of select="IncludeTransitionDesc"/></td>
        <td class="data" align="left"><xsl:value-of select="ExcludeTransitionDesc"/></td>
        <td class="data" align="right"><xsl:value-of select="NumberOfEstimatesInConsensus"/></td>
      </tr>
    </xsl:for-each>
    </table>
</td>
</tr>
  </xsl:if>
</xsl:template>

<xsl:template name="Covering_Analysts">
<xsl:if test="count(Analyst) &gt; 0">
  <tr>
    <td class="colorlight" colspan="2"><br/><span class="title2">Covering Analysts</span></td>
  </tr>
  <tr>
    <td class="colorlight" colspan="2">
      <table border="0" cellspacing="0" cellpadding="3" width="100%" class="colorlight">
        <tr>
          <td class="header">Firm</td>
          <td class="header">Analyst</td>
        </tr>
      <xsl:for-each select="Analyst">
      <xsl:sort select="Firm"/>
        <tr valign="top">
          <td class="data"><xsl:value-of select="Firm"/></td>
          <td class="data"><xsl:value-of select="PersonName"/><SPAN CLASS="dataalign"> </SPAN></td>
        </tr>
      </xsl:for-each>
      </table>
      <!--<xsl:if test="DataInfo/ContainsFactSetData = 'false'">
        <br/>
        <br/>
      </xsl:if>-->
    </td>
  </tr>
    <!--<xsl:if test="DataInfo/ContainsFactSetData = 'true'">
      <TR class="default" align="center">
        <TD class="colorlight" colspan="2">
          <span class="default">
            Copyright © <xsl:value-of select="Company/CurrentYear" /> FactSet Research Systems Inc.  All rights reserved.
          </span>
          <br/>
          <br/>
        </TD>
      </TR>
    </xsl:if>-->
</xsl:if>
</xsl:template>

<xsl:template name="DisclaimerText">
  <xsl:if test="Disclaimer/DisclaimerText != ''">
    <TR class="default" align="left">
     <TD class="colorlight" colspan="2">
      <span class="default">
        <xsl:value-of select="Disclaimer/DisclaimerText" disable-output-escaping="yes" />
      </span>
     </TD>
    </TR>
  </xsl:if>
</xsl:template>

<!-- Default Template Ends here. all templates below are ONLY if you are using the 'Style Template' section in the console. -->



















<!-- This is Template ONE option in the console under 'Style Template'
//This template is a striped down version of the main template. Everything (data) is displayed in single rows.
//The sections will be commented on where the rows are. you should be able to just move rows (entire chunks of data) where you need too. Edits to this section are rare, mostly due to size (width of the page).
-->
<xsl:template name="HEADER_ac2">
<xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
<TR>
<TD CLASS=" titletest" nowrap="" align="left"><xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/><br /><IMG SRC="" WIDTH="2" HEIGHT="1" alt="" /><span class="title2colordark titletest2"><xsl:value-of select="$CompanyNameCaps"/>&#160;(<xsl:value-of select="Company/Exchange"/>&#160;-&#160;<xsl:value-of select="Company/Ticker"/>)&#160;</span></TD>
<!--<TD CLASS="" nowrap="" align="right" valign="top">
<table width="100%" cellpadding="0" cellspacing="0" class="surr">
  <tr>
    <td align="center" class="datashade">
    <a>
    <xsl:variable name="keyinstn" select="Company/KeyInstn" />
    <xsl:variable name="url" select="concat('EstimatesDisclaimer.aspx?IID=', $keyinstn)" />
    <xsl:attribute name="href">
    <xsl:value-of select="$url" />
    </xsl:attribute>
    <xsl:attribute name="class">menu
    </xsl:attribute>Estimates Disclaimer</a><br />
    </td>
  </tr>
</table>
</TD>-->
</TR>
  <TR class="default" align="left">
    <TD class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Header" disable-output-escaping="yes"/></span></TD>
  </TR>
  <TR><TD height="1" class="colorlight" colspan="2">
    <table width="100%" cellpadding="0" cellspacing="0" height="1">
    <tr>
    <td class="colordark"><img src="" width="1" height="1" alt="" /></td>
    </tr>
    </table><br />
    </TD>
  </TR>
</xsl:template>

<xsl:template name="eps_rec_table2">
<tr><td>
<table cellspacing="0" border="0" cellpadding="0" width="100%">
<xsl:variable name="GAAPDomain" select="Company/GAAP"/>
<xsl:variable name="REAL_ESTATE" select="6"/>
<xsl:variable name="INSURANCE" select="8"/>
<xsl:variable name="INSURANCE_BROKER" select="9"/>
  <tr>
  <td class="colorlight" colspan="7"><span class="title2"><xsl:choose>
  <xsl:when test="$GAAPDomain = $REAL_ESTATE">FFO</xsl:when>
  <xsl:otherwise>EPS</xsl:otherwise>
  </xsl:choose> Data ($)</span></td>
  </tr>
  <tr>
    <td class="">
      <table cellspacing="0" border="0" cellpadding="3" width="100%" class="colorlight">

        <tr valign="surr" class="colorlight">
          <td class="surrleft datashade" NOWRAP="1" valign="bottom" width="14%"><span class="titletest2 ">Year Ending</span></td>
          <td class="surr_topbot datashade" align="right" valign="bottom" width="14%"><span class="titletest2 ">Q1</span></td>
          <td class="surr_topbot datashade" align="right" valign="bottom" width="14%"><span class="titletest2 ">Q2</span></td>
          <td class="surr_topbot datashade" align="right" valign="bottom" width="14%"><span class="titletest2 ">Q3</span></td>
          <td class="surr_topbot datashade" align="right" valign="bottom" width="14%"><span class="titletest2 ">Q4</span></td>
          <td class="surr_topbot datashade" align="right" valign="bottom" width="14%"><span class="titletest2 "><xsl:value-of select="EPSYear/FiscalYearName" /> FY</span></td>
          <xsl:if test="EPSYear/ShowCalendarYear = 'True'">
            <td class="surr_topbot datashade" align="right" valign="bottom" width="14%"><span class="titletest2 ">December CY</span></td>
          </xsl:if>
          <td class="surrright datashade" align="center" NOWRAP="1" valign="bottom" width="16%"><span class="titletest2 "># of Analysts<br />Reporting</span></td>
        </tr>
          <xsl:for-each select="EPSYear">
          <xsl:sort select="YearEnding"/>
            <tr valign="top" class="data">
              <td class="defaultbold"><xsl:value-of select="YearEnding"/></td>

              <td class="data" align="right">
                <xsl:choose>
                <xsl:when test="count(Q1) > 0">
                  <xsl:if test="Q1Consensus = 'true'">
                  <xsl:attribute name="class">defaultbold</xsl:attribute>
                  </xsl:if>
                  <xsl:value-of select="Q1"/>
                </xsl:when>
                <xsl:otherwise>
                  -
                </xsl:otherwise>
                </xsl:choose>
              </td>

              <td class="data" align="right">
                <xsl:choose>
                <xsl:when test="count(Q2) > 0">
                  <xsl:if test="Q2Consensus = 'true'">
                  <xsl:attribute name="class">defaultbold</xsl:attribute>
                  </xsl:if>
                  <xsl:value-of select="Q2"/>
                </xsl:when>
                <xsl:otherwise>
                  -
                </xsl:otherwise>
                </xsl:choose>
              </td>

              <td class="data" align="right">
                <xsl:choose>
                <xsl:when test="count(Q3) > 0">
                  <xsl:if test="Q3Consensus = 'true'">
                  <xsl:attribute name="class">defaultbold</xsl:attribute>
                  </xsl:if>
                  <xsl:value-of select="Q3"/>
                </xsl:when>
                <xsl:otherwise>
                  -
                </xsl:otherwise>
                </xsl:choose>
              </td>

              <td class="data" align="right">
                <xsl:choose>
                <xsl:when test="count(Q4) > 0">
                  <xsl:if test="Q4Consensus = 'true'">
                  <xsl:attribute name="class">defaultbold</xsl:attribute>
                  </xsl:if>
                  <xsl:value-of select="Q4"/>
                </xsl:when>
                <xsl:otherwise>
                  -
                </xsl:otherwise>
                </xsl:choose>
              </td>

              <td class="data" align="right">
                <xsl:choose>
                <xsl:when test="count(Y) > 0">
                  <xsl:if test="YConsensus = 'true'">
                  <xsl:attribute name="class">defaultbold</xsl:attribute>
                  </xsl:if>
                  <xsl:value-of select="Y"/>
                </xsl:when>
                <xsl:otherwise>
                  -
                </xsl:otherwise>
                </xsl:choose>
              </td>

              <xsl:if test="ShowCalendarYear = 'True'">
                <td class="data" align="right">
                  <xsl:choose>
                  <xsl:when test="count(CalendarYear) > 0">
                    <xsl:if test="CYConsensus = 'true'">
                    <xsl:attribute name="class">defaultbold</xsl:attribute>
                    </xsl:if>
                    <xsl:value-of select="CalendarYear"/>
                  </xsl:when>
                  <xsl:otherwise>
                    -
                  </xsl:otherwise>
                  </xsl:choose>
                </td>
              </xsl:if>

              <td class="data" align="center">
                <xsl:choose>
                <xsl:when test="count(NumReporting) > 0">
                  <xsl:value-of select="NumReporting"/>
                </xsl:when>
                <xsl:otherwise>
                  -
                </xsl:otherwise>
                </xsl:choose>
              </td>

            </tr>
          </xsl:for-each>

        <tr>
          <td class="surr datashade" colspan="8"><span class="default">NOTE: Actual
          <xsl:choose>
          <xsl:when test="$GAAPDomain = $REAL_ESTATE">
          FFO/Share
          </xsl:when>
          <xsl:when test="$GAAPDomain = $INSURANCE or $GAAPDomain = $INSURANCE_BROKER">
          Operating EPS
          </xsl:when>
          </xsl:choose>
          indicated in regular type,
          <span class="defaultbold">mean consensus estimates in bold.</span><br />
          <span stlye="font-style:italic">Source: S&amp;P Global Market Intelligence</span>
          </span>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <!--<tr>
    <td height="1" class="colorlight" colspan="2">
      <table width="100%" cellpadding="0" cellspacing="0" height="1">
        <tr>
          <td class="colordark"><img src="" width="1" height="1" /></td>
        </tr>
      </table><br />
    </td>
  </tr>-->

  <xsl:if test="$GAAPDomain = $REAL_ESTATE">
    <xsl:if test="count(AFFOEstimates) > 0">
      <tr>
        <td class="colorlight">
          <br/>
          <table cellspacing="0" border="0" cellpadding="3" width="100%" class="colorlight">
            <tr>
              <td class="colorlight" colspan="8">
                <span class="title2">AFFO Data ($)</span>
              </td>
            </tr>
            <tr class="colorlight">
              <td class="surrleft datashade" NOWRAP="1" valign="bottom" width="14%">
                <span class="titletest2 ">Year Ending</span>
              </td>
              <td class="surr_topbot datashade" align="right" valign="bottom" width="14%">
                <span class="titletest2 ">Q1</span>
              </td>
              <td class="surr_topbot datashade" align="right" valign="bottom" width="14%">
                <span class="titletest2 ">Q2</span>
              </td>
              <td class="surr_topbot datashade" align="right" valign="bottom" width="14%">
                <span class="titletest2 ">Q3</span>
              </td>
              <td class="surr_topbot datashade" align="right" valign="bottom" width="14%">
                <span class="titletest2 ">Q4</span>
              </td>
              <td class="surr_topbot datashade" align="right" valign="bottom" width="14%">
                <span class="titletest2 ">
                  <xsl:value-of select="AFFOEstimates/FiscalYearName" /> FY
                </span>
              </td>
              <xsl:if test="AFFOEstimates/ShowCalendarYear = 'True'">
                <td class="surr_topbot datashade" align="right" valign="bottom" width="14%">
                  <span class="titletest2 ">December CY</span>
                </td>
              </xsl:if>
              <td class="surrright datashade" align="center" NOWRAP="1" valign="bottom" width="16%">
                <span class="titletest2 ">
                  # of Analysts<br />Reporting
                </span>
              </td>
            </tr>
            <xsl:for-each select="AFFOEstimates">
              <xsl:sort select="YearEnding"/>
              <tr valign="top" class="data">
                <td class="defaultbold">
                  <xsl:value-of select="YearEnding"/>
                </td>

                <td class="data" align="right">
                  <xsl:choose>
                    <xsl:when test="count(Q1) > 0">
                      <xsl:if test="Q1Consensus = 'true'">
                        <xsl:attribute name="class">defaultbold</xsl:attribute>
                      </xsl:if>
                      <xsl:value-of select="Q1"/>
                    </xsl:when>
                    <xsl:otherwise>
                      -
                    </xsl:otherwise>
                  </xsl:choose>
                </td>

                <td class="data" align="right">
                  <xsl:choose>
                    <xsl:when test="count(Q2) > 0">
                      <xsl:if test="Q2Consensus = 'true'">
                        <xsl:attribute name="class">defaultbold</xsl:attribute>
                      </xsl:if>
                      <xsl:value-of select="Q2"/>
                    </xsl:when>
                    <xsl:otherwise>
                      -
                    </xsl:otherwise>
                  </xsl:choose>
                </td>

                <td class="data" align="right">
                  <xsl:choose>
                    <xsl:when test="count(Q3) > 0">
                      <xsl:if test="Q3Consensus = 'true'">
                        <xsl:attribute name="class">defaultbold</xsl:attribute>
                      </xsl:if>
                      <xsl:value-of select="Q3"/>
                    </xsl:when>
                    <xsl:otherwise>
                      -
                    </xsl:otherwise>
                  </xsl:choose>
                </td>

                <td class="data" align="right">
                  <xsl:choose>
                    <xsl:when test="count(Q4) > 0">
                      <xsl:if test="Q4Consensus = 'true'">
                        <xsl:attribute name="class">defaultbold</xsl:attribute>
                      </xsl:if>
                      <xsl:value-of select="Q4"/>
                    </xsl:when>
                    <xsl:otherwise>
                      -
                    </xsl:otherwise>
                  </xsl:choose>
                </td>

                <td class="data" align="right">
                  <xsl:choose>
                    <xsl:when test="count(Y) > 0">
                      <xsl:if test="YConsensus = 'true'">
                        <xsl:attribute name="class">defaultbold</xsl:attribute>
                      </xsl:if>
                      <xsl:value-of select="Y"/>
                    </xsl:when>
                    <xsl:otherwise>
                      -
                    </xsl:otherwise>
                  </xsl:choose>
                </td>

                <xsl:if test="ShowCalendarYear = 'True'">
                  <td class="data" align="right">
                    <xsl:choose>
                      <xsl:when test="count(CalendarYear) > 0">
                        <xsl:if test="CYConsensus = 'true'">
                          <xsl:attribute name="class">defaultbold</xsl:attribute>
                        </xsl:if>
                        <xsl:value-of select="CalendarYear"/>
                      </xsl:when>
                      <xsl:otherwise>
                        -
                      </xsl:otherwise>
                    </xsl:choose>
                  </td>
                </xsl:if>

                <td class="data" align="center">
                  <xsl:choose>
                    <xsl:when test="count(NumReporting) > 0">
                      <xsl:value-of select="NumReporting"/>
                    </xsl:when>
                    <xsl:otherwise>
                      -
                    </xsl:otherwise>
                  </xsl:choose>
                </td>
              </tr>
            </xsl:for-each>
          </table>
        </td>
      </tr>
    </xsl:if>

    <xsl:if test="count(EPSEstimates) > 0">
      <tr>
        <td class="colorlight">
          <br />
          <table cellspacing="0" border="0" cellpadding="3" width="100%" class="colorlight">
            <!-- Final-->
            <tr>
              <td class="colorlight" colspan="8">
                <span class="title2">EPS Data ($)</span>
              </td>
            </tr>
            <tr class="colorlight">
              <td class="surrleft datashade" NOWRAP="1" valign="bottom" width="14%">
                <span class="titletest2 ">Year Ending</span>
              </td>
              <td class="surr_topbot datashade" align="right" valign="bottom" width="14%">
                <span class="titletest2 ">Q1</span>
              </td>
              <td class="surr_topbot datashade" align="right" valign="bottom" width="14%">
                <span class="titletest2 ">Q2</span>
              </td>
              <td class="surr_topbot datashade" align="right" valign="bottom" width="14%">
                <span class="titletest2 ">Q3</span>
              </td>
              <td class="surr_topbot datashade" align="right" valign="bottom" width="14%">
                <span class="titletest2 ">Q4</span>
              </td>
              <td class="surr_topbot datashade" align="right" valign="bottom" width="14%">
                <span class="titletest2 ">
                  <xsl:value-of select="EPSYear/FiscalYearName" /> FY
                </span>
              </td>
              <xsl:if test="EPSYear/ShowCalendarYear = 'True'">
                <td class="surr_topbot datashade" align="right" valign="bottom" width="14%">
                  <span class="titletest2 ">December CY</span>
                </td>
              </xsl:if>
              <td class="surrright datashade" align="center" NOWRAP="1" valign="bottom" width="16%">
                <span class="titletest2 ">
                  # of Analysts<br />Reporting
                </span>
              </td>
            </tr>
            <xsl:for-each select="EPSEstimates">
              <xsl:sort select="YearEnding"/>
              <tr valign="top" class="data">
                <td class="defaultbold">
                  <xsl:value-of select="YearEnding"/>
                </td>

                <td class="data" align="right">
                  <xsl:choose>
                    <xsl:when test="count(Q1) > 0">
                      <xsl:if test="Q1Consensus = 'true'">
                        <xsl:attribute name="class">defaultbold</xsl:attribute>
                      </xsl:if>
                      <xsl:value-of select="Q1"/>
                    </xsl:when>
                    <xsl:otherwise>
                      -
                    </xsl:otherwise>
                  </xsl:choose>
                </td>

                <td class="data" align="right">
                  <xsl:choose>
                    <xsl:when test="count(Q2) > 0">
                      <xsl:if test="Q2Consensus = 'true'">
                        <xsl:attribute name="class">defaultbold</xsl:attribute>
                      </xsl:if>
                      <xsl:value-of select="Q2"/>
                    </xsl:when>
                    <xsl:otherwise>
                      -
                    </xsl:otherwise>
                  </xsl:choose>
                </td>

                <td class="data" align="right">
                  <xsl:choose>
                    <xsl:when test="count(Q3) > 0">
                      <xsl:if test="Q3Consensus = 'true'">
                        <xsl:attribute name="class">defaultbold</xsl:attribute>
                      </xsl:if>
                      <xsl:value-of select="Q3"/>
                    </xsl:when>
                    <xsl:otherwise>
                      -
                    </xsl:otherwise>
                  </xsl:choose>
                </td>

                <td class="data" align="right">
                  <xsl:choose>
                    <xsl:when test="count(Q4) > 0">
                      <xsl:if test="Q4Consensus = 'true'">
                        <xsl:attribute name="class">defaultbold</xsl:attribute>
                      </xsl:if>
                      <xsl:value-of select="Q4"/>
                    </xsl:when>
                    <xsl:otherwise>
                      -
                    </xsl:otherwise>
                  </xsl:choose>
                </td>

                <td class="data" align="right">
                  <xsl:choose>
                    <xsl:when test="count(Y) > 0">
                      <xsl:if test="YConsensus = 'true'">
                        <xsl:attribute name="class">defaultbold</xsl:attribute>
                      </xsl:if>
                      <xsl:value-of select="Y"/>
                    </xsl:when>
                    <xsl:otherwise>
                      -
                    </xsl:otherwise>
                  </xsl:choose>
                </td>

                <xsl:if test="ShowCalendarYear = 'True'">
                  <td class="data" align="right">
                    <xsl:choose>
                      <xsl:when test="count(CalendarYear) > 0">
                        <xsl:if test="CYConsensus = 'true'">
                          <xsl:attribute name="class">defaultbold</xsl:attribute>
                        </xsl:if>
                        <xsl:value-of select="CalendarYear"/>
                      </xsl:when>
                      <xsl:otherwise>
                        -
                      </xsl:otherwise>
                    </xsl:choose>
                  </td>
                </xsl:if>

                <td class="data" align="center">
                  <xsl:choose>
                    <xsl:when test="count(NumReporting) > 0">
                      <xsl:value-of select="NumReporting"/>
                    </xsl:when>
                    <xsl:otherwise>
                      -
                    </xsl:otherwise>
                  </xsl:choose>
                </td>
              </tr>
            </xsl:for-each>
          </table>
        </td>
      </tr>
    </xsl:if>
  </xsl:if>
<!--  <tr>
    <td height="1" class="colorlight" colspan="2">
      <table width="100%" cellpadding="0" cellspacing="0" height="1">
        <tr>
          <td class="colordark"><img src="" width="1" height="1" /></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
      <td align="center" class="colorlight" colspan="2">
      <a>
      <xsl:variable name="keyinstn" select="Company/KeyInstn" />
      <xsl:variable name="url" select="concat('EstimatesDisclaimer.aspx?IID=', $keyinstn)" />
      <xsl:attribute name="href">
      <xsl:value-of select="$url" />
      </xsl:attribute>Estimates Disclaimer</a>
      </td>
  </tr>-->
 <!-- <tr>
  <td class="">
  <xsl:if test="count(Analyst) &gt; 0">
  <table cellspacing="0" border="0" cellpadding="3" class="header" width="100%">
        <tr>
        <td class=" colorlight" colspan="2"><span class="title2">Recommendations</span><br /><span class=" titletest2">Analysts' Current Recommendations</span></td>
        </tr>
      <xsl:for-each select="OpinionSummary">
        <tr class="data">
          <td class="" width="50%"><span class="defaultbold"><xsl:value-of select="Recommendation"/></span></td>

          <td class="" width="50%" align="right"><xsl:value-of select="Count"/></td>
        </tr>
        <tr>
          <td height="1" class="colorlight" colspan="2">
            <table width="100%" cellpadding="0" cellspacing="0" height="1">
              <tr>
                <td class="datashade"><img src="" width="1" height="1" /></td>
              </tr>
            </table>
          </td>
        </tr>
      </xsl:for-each>
      <tr>
          <td height="1" class="colorlight" colspan="2"><br />
            <table width="100%" cellpadding="0" cellspacing="0" height="1">
              <tr>
                <td class="colordark"><img src="" width="1" height="1" /></td>
              </tr>
            </table><br />
          </td>
  </tr>
      </table>
    </xsl:if>
    </td>
  </tr>-->
</table>
</td>
</tr>
</xsl:template>

<xsl:template name="Covering_Analysts2">
<xsl:if test="count(Analyst) &gt; 0">
<tr><td><br/>
<table border="0" cellspacing="0" cellpadding="3" width="100%">
  <tr>
    <td class="colorlight" colspan="3">
      <span class="title2">Covering Analysts</span>
    </td>
  </tr>
  <tr>
    <td class="colorlight" colspan="3">
      <table border="0" cellspacing="0" cellpadding="3" width="100%" class="colorlight">
        <tr>
          <td class="surrleft datashade"><span class="titletest2 ">Firm</span></td>
          <td class="surr_topbot datashade"><span class="titletest2 ">Analyst</span></td>
          <td class="surrright datashade" align="left">&#160;<!--<span class="titletest2 ">&#160;&#160;&#160;&#160;&#160;&#160;&#160;Phone</span>--></td>
        </tr>
      <xsl:for-each select="Analyst">
      <xsl:sort select="Firm"/>
        <tr valign="top">
          <td class="data"><xsl:value-of select="Firm"/></td>
          <td class="data"><xsl:value-of select="PersonName"/><SPAN CLASS="dataalign"> </SPAN></td>
          <!--<td class="data" align="right"><xsl:value-of select="Phone"/><SPAN CLASS="dataalign"> </SPAN></td>-->
        </tr>
      </xsl:for-each>
      </table>
      <!--<xsl:if test="DataInfo/ContainsFactSetData = 'false'">
        <br/>
      </xsl:if>-->
    </td>
  </tr>
    <!--<xsl:if test="DataInfo/ContainsFactSetData = 'true'">
      <TR class="default" align="center">
        <TD class="colorlight" colspan="2">
          <span class="default">
            Copyright © <xsl:value-of select="Company/CurrentYear" /> FactSet Research Systems Inc.  All rights reserved.
          </span>
          <br/>
          <br/>
        </TD>
      </TR>
    </xsl:if>-->
</table>
</td></tr>
</xsl:if>
</xsl:template>

<!-- <xsl:template name="Individual_Estimate2">
  <xsl:variable name="GAAPDomain" select="Company/GAAP"/>
  <xsl:if test="count(Estimate) &gt; 0">

  <tr>
    <td class="colorlight" colspan="7"><span class="title2">Individual Estimates</span></td>
  </tr>
  <tr>
    <td class="colorlight" colspan="7">
      <TABLE border="0" cellspacing="0" cellpadding="3" width="100%" class="colorlight">
        <tr>
          <td class="surrleft datashade"><span class="titletest2 ">Period*</span></td>
          <td class="surr_topbot datashade"><span class="titletest2 ">Firm</span></td>
          <xsl:choose>
            <xsl:when test="$GAAPDomain = '6'">
              <td class="surr_topbot datashade" align="right"><span class="titletest2 ">FFO Estimate</span></td>
            </xsl:when>
            <xsl:otherwise>
              <td class="surr_topbot datashade" align="right"><span class="titletest2 ">EPS Estimate</span></td>
            </xsl:otherwise>
          </xsl:choose>
          <td class="surr_topbot datashade" align="center"><span class="titletest2 ">5Pt. Scale**</span></td>
          <td class="surr_topbot datashade"><span class="titletest2 ">Recommendation</span></td>
          <td class="surr_topbot datashade" align="right"><span class="titletest2 ">Last Confirmed</span></td>
          <td class="surrright datashade" align="right"><span class="titletest2 ">Originated</span></td>
        </tr>
      <xsl:for-each select="Estimate[DateEndedStandard &lt; ../UtilInfo/CurrentYear + 2]">
        <tr valign="top">
          <td class="data">
            <xsl:if test="(DateEndedStandard - ../UtilInfo/CurrentYear + 1) mod 2 != 0">
              <xsl:attribute name="class">datashade</xsl:attribute>
            </xsl:if>
            <span class="defaultbold"><xsl:value-of select="DateEndedStandard"/> Y</span>
          </td>
          <td class="data">
            <xsl:if test="(DateEndedStandard - ../UtilInfo/CurrentYear + 1) mod 2 != 0">
              <xsl:attribute name="class">datashade</xsl:attribute>
            </xsl:if>
            <xsl:value-of select="Firm"/>
          </td>
          <td class="data" align="right">
            <xsl:if test="(DateEndedStandard - ../UtilInfo/CurrentYear + 1) mod 2 != 0">
              <xsl:attribute name="class">datashade</xsl:attribute>
            </xsl:if>
            $<xsl:value-of select="EPSEstimated"/><SPAN CLASS="dataalign"> </SPAN>
          </td>
          <td class="data" align="center">
            <xsl:if test="(DateEndedStandard - ../UtilInfo/CurrentYear + 1) mod 2 != 0">
              <xsl:attribute name="class">datashade</xsl:attribute>
            </xsl:if>
            <xsl:value-of select="Scale"/>&#160;&#160;&#160;&#160;
          </td>
          <td class="data">
            <xsl:if test="(DateEndedStandard - ../UtilInfo/CurrentYear + 1) mod 2 != 0">
              <xsl:attribute name="class">datashade</xsl:attribute>
            </xsl:if>
            <xsl:value-of select="Recommendation"/>
          </td>
          <td class="data" align="right">
            <xsl:if test="(DateEndedStandard - ../UtilInfo/CurrentYear + 1) mod 2 != 0">
              <xsl:attribute name="class">datashade</xsl:attribute>
            </xsl:if>
            <xsl:value-of select="LastConfirmed"/><SPAN CLASS="dataalign"> </SPAN>
          </td>
          <td class="data" align="right">
            <xsl:if test="(DateEndedStandard - ../UtilInfo/CurrentYear + 1) mod 2 != 0">
              <xsl:attribute name="class">datashade</xsl:attribute>
            </xsl:if>
            <xsl:value-of select="Originated"/><SPAN CLASS="dataalign"> </SPAN>
          </td>
        </tr>
      </xsl:for-each>
        <tr>
          <td colspan="7" class="colorlight">
            <span class="default">
              <xsl:variable name="REAL_ESTATE" select="6"/>
              <xsl:choose>
                <xsl:when test="Company/GAAP = $REAL_ESTATE">Note: All estimates represent diluted FFO/share.
                </xsl:when>
                <xsl:otherwise>Note: All estimates represent diluted operating EPS from continuing operations.</xsl:otherwise>
              </xsl:choose>
            </span>
          </td>
        </tr>
        <tr>
          <td colspan="7" class="colorlight">
            <span class="default">* "Y" denotes calendar year estimate, otherwise non-standard fiscal year estimate.</span>
          </td>
        </tr>
        <tr>
          <td colspan="7" class="colorlight">
            <span class="default">** SNL standardizes the contributing firms' recommendation scales into a five-point range whereby one (1) represents the strongest possible recommendation and five (5) the weakest.</span>
          </td>
        </tr>
      </TABLE>
    </td>
  </tr>
  </xsl:if>
</xsl:template> -->

<xsl:template name="DisclaimerText2">
  <xsl:if test="Disclaimer/DisclaimerText != ''">
    <TR class="default" align="left">
     <TD class="colorlight" colspan="2">
      <span class="default">
        <xsl:value-of select="Disclaimer/DisclaimerText" disable-output-escaping="yes" />
      </span>
     </TD>
    </TR>
  </xsl:if>
</xsl:template>

<!-- Template ONE Ends here. -->





<!-- This is Template 3 -->

<xsl:template name="eps_rec_table3">

<xsl:variable name="GAAPDomain" select="Company/GAAP"/>
<xsl:variable name="REAL_ESTATE" select="6"/>
<xsl:variable name="INSURANCE" select="8"/>
<xsl:variable name="INSURANCE_BROKER" select="9"/>
<tr>
  <td class="colorlight" width="98%">
    <table cellspacing="0" border="0" cellpadding="3" width="100%" class="table2" >
    <tr>
      <td class="header" colspan="8">
      <xsl:choose>
      <xsl:when test="$GAAPDomain = $REAL_ESTATE">FFO</xsl:when>
      <xsl:otherwise>EPS</xsl:otherwise>
      </xsl:choose> Data ($)
      </td>
    </tr>
    <tr valign="bottom" class="colorlight">
      <td width="14%" class="datashade" NOWRAP="1"><span class="defaultbold">Year Ending</span></td>
      <td width="14%" class="datashade" align="right"><span class="defaultbold">Q1</span></td>
      <td width="14%" class="datashade" align="right"><span class="defaultbold">Q2</span></td>
      <td width="14%" class="datashade" align="right"><span class="defaultbold">Q3</span></td>
      <td width="14%" class="datashade" align="right"><span class="defaultbold">Q4</span></td>
      <td width="14%" class="datashade" align="right"><span class="defaultbold"><xsl:value-of select="EPSYear/FiscalYearName" /> FY</span></td>
      <xsl:if test="EPSYear/ShowCalendarYear = 'True'">
      <td width="14%" class="datashade" align="right"><span class="defaultbold">December CY</span></td>
      </xsl:if>
      <td width="16%" class="datashade" align="center" NOWRAP="1"><span class="defaultbold"># of Analysts<br />Reporting</span></td>
    </tr>
    <xsl:for-each select="EPSYear">
    <xsl:sort select="YearEnding"/>
    <tr valign="top" class="data">
      <td class="defaultbold"><xsl:value-of select="YearEnding"/></td>
      <td class="data" align="right">
      <xsl:choose>
        <xsl:when test="count(Q1) > 0">
        <xsl:if test="Q1Consensus = 'true'">
          <xsl:attribute name="class">defaultbold</xsl:attribute>
        </xsl:if>
        <xsl:value-of select="Q1"/>
        </xsl:when>
        <xsl:otherwise>-</xsl:otherwise>
      </xsl:choose>
      </td>

      <td class="data" align="right">
      <xsl:choose>
        <xsl:when test="count(Q2) > 0">
        <xsl:if test="Q2Consensus = 'true'">
          <xsl:attribute name="class">defaultbold</xsl:attribute>
        </xsl:if>
        <xsl:value-of select="Q2"/>
        </xsl:when>
        <xsl:otherwise>-</xsl:otherwise>
      </xsl:choose>
      </td>

      <td class="data" align="right">
      <xsl:choose>
        <xsl:when test="count(Q3) > 0">
        <xsl:if test="Q3Consensus = 'true'">
          <xsl:attribute name="class">defaultbold</xsl:attribute>
        </xsl:if>
        <xsl:value-of select="Q3"/>
        </xsl:when>
        <xsl:otherwise>-</xsl:otherwise>
      </xsl:choose>
      </td>

      <td class="data" align="right">
      <xsl:choose>
        <xsl:when test="count(Q4) > 0">
        <xsl:if test="Q4Consensus = 'true'">
          <xsl:attribute name="class">defaultbold</xsl:attribute>
        </xsl:if>
        <xsl:value-of select="Q4"/>
        </xsl:when>
        <xsl:otherwise>-</xsl:otherwise>
      </xsl:choose>
      </td>

      <td class="data" align="right">
      <xsl:choose>
        <xsl:when test="count(Y) > 0">
        <xsl:if test="YConsensus = 'true'">
          <xsl:attribute name="class">defaultbold</xsl:attribute>
        </xsl:if>
          <xsl:value-of select="Y"/>
        </xsl:when>
        <xsl:otherwise>-</xsl:otherwise>
      </xsl:choose>
      </td>

    <xsl:if test="ShowCalendarYear = 'True'">
      <td class="data" align="right">
      <xsl:choose>
        <xsl:when test="count(CalendarYear) > 0">
        <xsl:if test="CYConsensus = 'true'">
          <xsl:attribute name="class">defaultbold</xsl:attribute>
        </xsl:if>
        <xsl:value-of select="CalendarYear"/>
        </xsl:when>
        <xsl:otherwise>-</xsl:otherwise>
      </xsl:choose>
      </td>
    </xsl:if>

      <td class="data" align="center">
      <xsl:choose>
      <xsl:when test="count(NumReporting) > 0">
        <xsl:value-of select="NumReporting"/>
      </xsl:when>
      <xsl:otherwise>-</xsl:otherwise>
      </xsl:choose>
      </td>
    </tr>
    </xsl:for-each>

    <tr>
      <td class="colorlight" colspan="7">
        <span class="default">NOTE: Actual
        <xsl:choose>
          <xsl:when test="$GAAPDomain = $REAL_ESTATE">
          FFO/Share
          </xsl:when>
          <xsl:when test="$GAAPDomain = $INSURANCE or $GAAPDomain = $INSURANCE_BROKER">
            Operating EPS
          </xsl:when>
        </xsl:choose>
        indicated in regular type, <span class="defaultbold">mean consensus estimates in bold.</span><br />
        <span stlye="font-style:italic">Source: S&amp;P Global Market Intelligence</span>
        </span>
      </td>
    </tr>
    </table>
  </td>
 <!-- <td class="colorlight" width="5" ><img src="/images/interactive/blank.gif" width="5" height="8" border="0" /></td>
  <td class="colorlight" valign="top" width="5%">
    <xsl:call-template name="Recommend3"/>
  </td>-->
</tr>

  <xsl:if test="$GAAPDomain = $REAL_ESTATE">
    <xsl:if test="count(AFFOEstimates) > 0">
      <tr>
        <td class="colorlight">
          <br/>
          <table cellspacing="0" border="0" cellpadding="3" width="100%" class="colorlight table2">
            <tr>
              <td class="colorlight header" colspan="8">AFFO Data ($)</td>
            </tr>
            <tr valign="bottom" class="colorlight">
              <td width="14%" class="datashade" NOWRAP="1">
                <span class="defaultbold">Year Ending</span>
              </td>
              <td width="14%" class="datashade" align="right">
                <span class="defaultbold">Q1</span>
              </td>
              <td width="14%" class="datashade" align="right">
                <span class="defaultbold">Q2</span>
              </td>
              <td width="14%" class="datashade" align="right">
                <span class="defaultbold">Q3</span>
              </td>
              <td width="14%" class="datashade" align="right">
                <span class="defaultbold">Q4</span>
              </td>
              <td width="14%" class="datashade" align="right">
                <span class="defaultbold">
                  <xsl:value-of select="EPSYear/FiscalYearName" /> FY
                </span>
              </td>
              <xsl:if test="EPSYear/ShowCalendarYear = 'True'">
                <td width="14%" class="datashade" align="right">
                  <span class="defaultbold">December CY</span>
                </td>
              </xsl:if>
              <td width="16%" class="datashade" align="center" NOWRAP="1">
                <span class="defaultbold">
                  # of Analysts<br />Reporting
                </span>
              </td>
            </tr>
            <xsl:for-each select="AFFOEstimates">
              <xsl:sort select="YearEnding"/>
              <tr valign="top" class="data">
                <td class="defaultbold">
                  <xsl:value-of select="YearEnding"/>
                </td>
                <td class="data" align="right">
                  <xsl:choose>
                    <xsl:when test="count(Q1) > 0">
                      <xsl:if test="Q1Consensus = 'true'">
                        <xsl:attribute name="class">defaultbold</xsl:attribute>
                      </xsl:if>
                      <xsl:value-of select="Q1"/>
                    </xsl:when>
                    <xsl:otherwise>-</xsl:otherwise>
                  </xsl:choose>
                </td>

                <td class="data" align="right">
                  <xsl:choose>
                    <xsl:when test="count(Q2) > 0">
                      <xsl:if test="Q2Consensus = 'true'">
                        <xsl:attribute name="class">defaultbold</xsl:attribute>
                      </xsl:if>
                      <xsl:value-of select="Q2"/>
                    </xsl:when>
                    <xsl:otherwise>-</xsl:otherwise>
                  </xsl:choose>
                </td>

                <td class="data" align="right">
                  <xsl:choose>
                    <xsl:when test="count(Q3) > 0">
                      <xsl:if test="Q3Consensus = 'true'">
                        <xsl:attribute name="class">defaultbold</xsl:attribute>
                      </xsl:if>
                      <xsl:value-of select="Q3"/>
                    </xsl:when>
                    <xsl:otherwise>-</xsl:otherwise>
                  </xsl:choose>
                </td>

                <td class="data" align="right">
                  <xsl:choose>
                    <xsl:when test="count(Q4) > 0">
                      <xsl:if test="Q4Consensus = 'true'">
                        <xsl:attribute name="class">defaultbold</xsl:attribute>
                      </xsl:if>
                      <xsl:value-of select="Q4"/>
                    </xsl:when>
                    <xsl:otherwise>-</xsl:otherwise>
                  </xsl:choose>
                </td>

                <td class="data" align="right">
                  <xsl:choose>
                    <xsl:when test="count(Y) > 0">
                      <xsl:if test="YConsensus = 'true'">
                        <xsl:attribute name="class">defaultbold</xsl:attribute>
                      </xsl:if>
                      <xsl:value-of select="Y"/>
                    </xsl:when>
                    <xsl:otherwise>-</xsl:otherwise>
                  </xsl:choose>
                </td>

                <xsl:if test="ShowCalendarYear = 'True'">
                  <td class="data" align="right">
                    <xsl:choose>
                      <xsl:when test="count(CalendarYear) > 0">
                        <xsl:if test="CYConsensus = 'true'">
                          <xsl:attribute name="class">defaultbold</xsl:attribute>
                        </xsl:if>
                        <xsl:value-of select="CalendarYear"/>
                      </xsl:when>
                      <xsl:otherwise>-</xsl:otherwise>
                    </xsl:choose>
                  </td>
                </xsl:if>

                <td class="data" align="center">
                  <xsl:choose>
                    <xsl:when test="count(NumReporting) > 0">
                      <xsl:value-of select="NumReporting"/>
                    </xsl:when>
                    <xsl:otherwise>-</xsl:otherwise>
                  </xsl:choose>
                </td>
              </tr>
            </xsl:for-each>
          </table>
        </td>
      </tr>
    </xsl:if>

    <xsl:if test="count(EPSEstimates) > 0">
      <tr>
        <td class="colorlight">
          <br/>
          <table cellspacing="0" border="0" cellpadding="3" width="100%" class="colorlight table2">
            <tr>
              <td class="colorlight header" colspan="8">
                <span class="header">EPS Data ($)</span>
              </td>
            </tr>
            <tr valign="bottom" class="colorlight">
              <td width="14%" class="datashade" NOWRAP="1">
                <span class="defaultbold">Year Ending</span>
              </td>
              <td width="14%" class="datashade" align="right">
                <span class="defaultbold">Q1</span>
              </td>
              <td width="14%" class="datashade" align="right">
                <span class="defaultbold">Q2</span>
              </td>
              <td width="14%" class="datashade" align="right">
                <span class="defaultbold">Q3</span>
              </td>
              <td width="14%" class="datashade" align="right">
                <span class="defaultbold">Q4</span>
              </td>
              <td width="14%" class="datashade" align="right">
                <span class="defaultbold">
                  <xsl:value-of select="EPSYear/FiscalYearName" /> FY
                </span>
              </td>
              <xsl:if test="EPSYear/ShowCalendarYear = 'True'">
                <td width="14%" class="datashade" align="right">
                  <span class="defaultbold">December CY</span>
                </td>
              </xsl:if>
              <td width="16%" class="datashade" align="center" NOWRAP="1">
                <span class="defaultbold">
                  # of Analysts<br />Reporting
                </span>
              </td>
            </tr>
            <xsl:for-each select="EPSEstimates">
              <xsl:sort select="YearEnding"/>
              <tr valign="top" class="data">
                <td class="defaultbold">
                  <xsl:value-of select="YearEnding"/>
                </td>

                <td class="data" align="right">
                  <xsl:choose>
                    <xsl:when test="count(Q1) > 0">
                      <xsl:if test="Q1Consensus = 'true'">
                        <xsl:attribute name="class">defaultbold</xsl:attribute>
                      </xsl:if>
                      <xsl:value-of select="Q1"/>
                    </xsl:when>
                    <xsl:otherwise>
                      -
                    </xsl:otherwise>
                  </xsl:choose>
                </td>

                <td class="data" align="right">
                  <xsl:choose>
                    <xsl:when test="count(Q2) > 0">
                      <xsl:if test="Q2Consensus = 'true'">
                        <xsl:attribute name="class">defaultbold</xsl:attribute>
                      </xsl:if>
                      <xsl:value-of select="Q2"/>
                    </xsl:when>
                    <xsl:otherwise>
                      -
                    </xsl:otherwise>
                  </xsl:choose>
                </td>

                <td class="data" align="right">
                  <xsl:choose>
                    <xsl:when test="count(Q3) > 0">
                      <xsl:if test="Q3Consensus = 'true'">
                        <xsl:attribute name="class">defaultbold</xsl:attribute>
                      </xsl:if>
                      <xsl:value-of select="Q3"/>
                    </xsl:when>
                    <xsl:otherwise>
                      -
                    </xsl:otherwise>
                  </xsl:choose>
                </td>

                <td class="data" align="right">
                  <xsl:choose>
                    <xsl:when test="count(Q4) > 0">
                      <xsl:if test="Q4Consensus = 'true'">
                        <xsl:attribute name="class">defaultbold</xsl:attribute>
                      </xsl:if>
                      <xsl:value-of select="Q4"/>
                    </xsl:when>
                    <xsl:otherwise>
                      -
                    </xsl:otherwise>
                  </xsl:choose>
                </td>

                <td class="data" align="right">
                  <xsl:choose>
                    <xsl:when test="count(Y) > 0">
                      <xsl:if test="YConsensus = 'true'">
                        <xsl:attribute name="class">defaultbold</xsl:attribute>
                      </xsl:if>
                      <xsl:value-of select="Y"/>
                    </xsl:when>
                    <xsl:otherwise>
                      -
                    </xsl:otherwise>
                  </xsl:choose>
                </td>

                <xsl:if test="ShowCalendarYear = 'True'">
                  <td class="data" align="right">
                    <xsl:choose>
                      <xsl:when test="count(CalendarYear) > 0">
                        <xsl:if test="CYConsensus = 'true'">
                          <xsl:attribute name="class">defaultbold</xsl:attribute>
                        </xsl:if>
                        <xsl:value-of select="CalendarYear"/>
                      </xsl:when>
                      <xsl:otherwise>
                        -
                      </xsl:otherwise>
                    </xsl:choose>
                  </td>
                </xsl:if>

                <td class="data" align="center">
                  <xsl:choose>
                    <xsl:when test="count(NumReporting) > 0">
                      <xsl:value-of select="NumReporting"/>
                    </xsl:when>
                    <xsl:otherwise>
                      -
                    </xsl:otherwise>
                  </xsl:choose>
                </td>
              </tr>
            </xsl:for-each>
          </table>
        </td>
      </tr>
    </xsl:if>
  </xsl:if>
  <!--<tr>
    <td align="center" class="colorlight" colspan="3">
    <a>
    <xsl:variable name="keyinstn" select="Company/KeyInstn" />
    <xsl:variable name="url" select="concat('EstimatesDisclaimer.aspx?IID=', $keyinstn)" />
    <xsl:attribute name="href">
    <xsl:value-of select="$url" />
    </xsl:attribute>Estimates Disclaimer</a><br /><br />
    </td>
  </tr>-->
</xsl:template>


<xsl:template name="Covering_Analysts3">
<xsl:if test="count(Analyst) &gt; 0">
<tr>
<td><br/>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
  <tr>
    <td class="colorlight" colspan="3">
      <table border="0" cellspacing="0" cellpadding="3" width="100%" class="colorlight table2">
      <tr>
        <td class="colorlight header" colspan="3">Covering Analysts</td>
      </tr>
      <tr>
        <td class="datashade"><span class="defaultbold">Firm</span></td>
        <td class="datashade"><span class="defaultbold">Analyst</span></td>
        <!--<td class="datashade" align="left"><span class="defaultbold ">&#160;&#160;&#160;&#160;&#160;&#160;&#160;Phone</span></td>-->
      </tr>
      <xsl:for-each select="Analyst">
      <xsl:sort select="Firm"/>
        <tr valign="top">
          <td class="data"><xsl:value-of select="Firm"/></td>
          <td class="data"><xsl:value-of select="PersonName"/><SPAN CLASS="dataalign"> </SPAN></td>
          <!--<td class="data" align="right"><xsl:value-of select="Phone"/><SPAN CLASS="dataalign"> </SPAN></td>-->
        </tr>
      </xsl:for-each>
      <!--<xsl:if test="DataInfo/ContainsFactSetData = 'false'">
        <br/>
      </xsl:if>
          <xsl:if test="DataInfo/ContainsFactSetData = 'true'">
          <TR class="default" align="center">
            <TD class="colorlight" colspan="2">
              <span class="default">
                Copyright © <xsl:value-of select="Company/CurrentYear" /> FactSet Research Systems Inc.  All rights reserved.
              </span>
              <br/>
            </TD>
          </TR>
      </xsl:if>-->
      </table>
    </td>
  </tr>
</table>
</td>
</tr>
</xsl:if>
</xsl:template>

<!-- <xsl:template name="Individual_Estimate3">
  <xsl:variable name="GAAPDomain" select="Company/GAAP"/>
  <xsl:if test="count(Estimate) &gt; 0">
  <tr>
    <td class="colorlight" colspan="3"><br/>
      <TABLE border="0" cellspacing="0" cellpadding="3" width="100%" class="colorlight table2">
        <tr>
          <td class="colorlight header " colspan="7">Individual Estimates</td>
        </tr>
        <tr>
          <td class="surrleft datashade"><span class="defaultbold ">Period*</span></td>
          <td class="surr_topbot datashade"><span class="defaultbold">Firm</span></td>
          <xsl:choose>
          <xsl:when test="$GAAPDomain = '6'">
            <td class="surr_topbot datashade" align="right"><span class="defaultbold">FFO Estimate</span></td>
          </xsl:when>
          <xsl:otherwise>
            <td class="surr_topbot datashade" align="right"><span class="defaultbold">EPS Estimate</span></td>
          </xsl:otherwise>
          </xsl:choose>
          <td class="surr_topbot datashade" align="center"><span class="defaultbold">5Pt. Scale**</span></td>
          <td class="surr_topbot datashade"><span class="defaultbold">Recommendation</span></td>
          <td class="surr_topbot datashade" align="right"><span class="defaultbold">Last Confirmed</span></td>
          <td class="surrright datashade" align="right"><span class="defaultbold">Originated</span></td>
        </tr>
      <xsl:for-each select="Estimate[DateEndedStandard &lt; ../UtilInfo/CurrentYear + 2]">
        <tr valign="top">
          <td class="data">
            <xsl:if test="(DateEndedStandard - ../UtilInfo/CurrentYear + 1) mod 2 != 0">
              <xsl:attribute name="class">datashade</xsl:attribute>
            </xsl:if>
            <span class="defaultbold"><xsl:value-of select="DateEndedStandard"/> Y</span>
          </td>
          <td class="data">
            <xsl:if test="(DateEndedStandard - ../UtilInfo/CurrentYear + 1) mod 2 != 0">
              <xsl:attribute name="class">datashade</xsl:attribute>
            </xsl:if>
            <xsl:value-of select="Firm"/>
          </td>
          <td class="data" align="right">
            <xsl:if test="(DateEndedStandard - ../UtilInfo/CurrentYear + 1) mod 2 != 0">
              <xsl:attribute name="class">datashade</xsl:attribute>
            </xsl:if>
            $<xsl:value-of select="EPSEstimated"/><SPAN CLASS="dataalign"> </SPAN>
          </td>
          <td class="data" align="center">
            <xsl:if test="(DateEndedStandard - ../UtilInfo/CurrentYear + 1) mod 2 != 0">
              <xsl:attribute name="class">datashade</xsl:attribute>
            </xsl:if>
            <xsl:value-of select="Scale"/>&#160;&#160;&#160;&#160;
          </td>
          <td class="data">
            <xsl:if test="(DateEndedStandard - ../UtilInfo/CurrentYear + 1) mod 2 != 0">
              <xsl:attribute name="class">datashade</xsl:attribute>
            </xsl:if>
            <xsl:value-of select="Recommendation"/>
          </td>
          <td class="data" align="right">
            <xsl:if test="(DateEndedStandard - ../UtilInfo/CurrentYear + 1) mod 2 != 0">
              <xsl:attribute name="class">datashade</xsl:attribute>
            </xsl:if>
            <xsl:value-of select="LastConfirmed"/><SPAN CLASS="dataalign"> </SPAN>
          </td>
          <td class="data" align="right">
            <xsl:if test="(DateEndedStandard - ../UtilInfo/CurrentYear + 1) mod 2 != 0">
              <xsl:attribute name="class">datashade</xsl:attribute>
            </xsl:if>
            <xsl:value-of select="Originated"/><SPAN CLASS="dataalign"> </SPAN>
          </td>
        </tr>
      </xsl:for-each>
        <tr>
          <td colspan="7" class="colorlight">
            <span class="default">
              <xsl:variable name="REAL_ESTATE" select="6"/>
              <xsl:choose>
                <xsl:when test="Company/GAAP = $REAL_ESTATE">Note: All estimates represent diluted FFO/share.
                </xsl:when>
                <xsl:otherwise>Note: All estimates represent diluted operating EPS from continuing operations.</xsl:otherwise>
              </xsl:choose>
            </span>
          </td>
        </tr>
        <tr>
          <td colspan="7" class="colorlight">
            <span class="default">* "Y" denotes calendar year estimate, otherwise non-standard fiscal year estimate.</span>
          </td>
        </tr>
        <tr>
          <td colspan="7" class="colorlight">
            <span class="default">** SNL standardizes the contributing firms' recommendation scales into a five-point range whereby one (1) represents the strongest possible recommendation and five (5) the weakest.</span>
          </td>
        </tr>
      </TABLE>
    </td>
  </tr>
  </xsl:if>
</xsl:template> -->

<xsl:template name="Recommend3">
<xsl:if test="count(Analyst) &gt; 0">

      <table cellspacing="0" border="0" cellpadding="3" class="header table2" width="100%">
        <tr>
        <td class="header" colspan="2">Recommendations</td>
        </tr>
        <tr>
          <td colspan="2" class="datashade" nowrap="1"><span class="defaultbold">Analysts' Current<br />Recommendations</span></td>
        </tr>
      <xsl:for-each select="OpinionSummary">
        <tr class="data">
          <td class="defaultbold" width="50%"><span class="defaultbold"><xsl:value-of select="Recommendation"/></span></td>
          <td class="data" width="50%" align="right"><xsl:value-of select="Count"/></td>
        </tr>
      </xsl:for-each>
      </table>
</xsl:if>
</xsl:template>

<xsl:template name="DisclaimerText3">
  <xsl:if test="Disclaimer/DisclaimerText != ''">
    <TR class="default" align="left">
     <TD class="colorlight" colspan="2">
      <span class="default">
        <xsl:value-of select="Disclaimer/DisclaimerText" disable-output-escaping="yes" />
      </span>
     </TD>
    </TR>
  </xsl:if>
</xsl:template>

<!-- Template 3 ends -->



































<!-- Main XSLT templates are here. these are the templates which are actually being displayed in the page. this is for TOP LEVEL editing. if you dont need to use the individual templates above you can do your main editing here

when pulling THESE templates below you must carry the top comment with them and uncomment them when you drop it into the company specific xslt.

-->
<xsl:template match="irw:Coverage">
<xsl:variable name="TemplateName" select="Company/TemplateName"/>
<xsl:choose>


<!-- Template ONE -->
<xsl:when test="$TemplateName = 'AltCover1'">
<!-- Copy from here down

<xsl:template match="irw:Coverage">

-->
<xsl:call-template name="TemplateONEstylesheet" />
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" CLASS="">
  <xsl:call-template name="Title_S2"/>
  <TR ALIGN="">
    <TD CLASS="leftTOPbord" colspan="2" valign="top">
      <TABLE border="0" cellspacing="0" cellpadding="2" class="data" width="100%">
        <xsl:if test="count(EPSYear) > 0">
              <xsl:call-template name="eps_rec_table2"/>
              <!--<xsl:call-template name="Individual_Estimate2"/>-->
        </xsl:if>
        <xsl:call-template name="Covering_Analysts2"/>
        <xsl:if test="count(EPSYear) = 0">
          <tr>
            <td class="" colspan="2">
              <table cellspacing="0" border="0" cellpadding="0" width="100%">
                <tr>
                  <td class="colorlight" colspan="7">
                    <span class="title2">Estimates Summary</span>
                  </td>
                </tr>
                <tr>
                  <td class="colorlight" colspan="7"><span class="data">No estimates data is currently available for this company.</span>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
       </xsl:if>
      </TABLE>
    </TD>
  </TR>

<xsl:call-template name="DisclaimerText2"/>

<xsl:if test="Company/Footer != ''">
  <TR class="default" align="left">
    <TD class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span></TD>
</TR>
</xsl:if>
</TABLE>
<xsl:call-template name="Copyright"/>
<!--
</xsl:template>
End Copy Here
-->
</xsl:when>
<!-- End Template ONE -->



<!-- Template 3 -->
<xsl:when test="$TemplateName = 'AltCover3'">
<!-- Copy from here down
<xsl:template match="irw:Coverage">
-->
<xsl:call-template name="TemplateTHREEstylesheet" />
<xsl:call-template name="Title_T3"/>
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" CLASS="table2">
<TR ALIGN="">
  <TD CLASS="leftTOPbord" colspan="2">
    <TABLE border="0" cellspacing="0" cellpadding="2" class="data" width="100%">
    <xsl:if test="count(EPSYear) > 0">
    <xsl:call-template name="eps_rec_table3"/>
      <!--<xsl:call-template name="Individual_Estimate3"/>
      <xsl:call-template name="TransitionInfo3"/>-->
    </xsl:if>
   <xsl:call-template name="Covering_Analysts3"/>
    <xsl:if test="count(EPSYear) = 0">
    <tr>
      <td class="" colspan="2">
        <table cellspacing="0" border="0" cellpadding="0" width="100%">
        <tr>
          <td class="colorlight" colspan="7">
            <span class="title2">Estimates Summary</span>
          </td>
        </tr>
        <tr>
          <td class="colorlight" colspan="7"><span class="data">No estimates data is currently available for this company.</span>
          </td>
        </tr>
        </table>
      </td>
    </tr>
    </xsl:if>
    </TABLE>
  </TD>
</TR>

<xsl:call-template name="DisclaimerText3"/>

<xsl:if test="Company/Footer != ''">
<TR class="default" align="left">
  <TD class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span></TD>
</TR>
</xsl:if>
</TABLE>
<xsl:call-template name="Copyright"/>
<!--
</xsl:template>
End Copy Here
-->
</xsl:when>
<!-- End Template 3 -->






<!-- Default Template  -->
<xsl:otherwise>
<!-- Copy from here down
<xsl:template match="irw:Coverage">
-->
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" CLASS="colordark">
 <xsl:call-template name="Title_S1"/>
  <TR ALIGN="">
    <TD CLASS="colordark" colspan="2">
      <TABLE border="0" cellspacing="0" cellpadding="2" class="data" width="100%">
        <xsl:if test="count(EPSYear) > 0">
          <xsl:call-template name="eps_rec_table"/>
        </xsl:if>
        <xsl:if test="count(Analyst) > 0">
          <xsl:call-template name="Covering_Analysts"/>
        </xsl:if>
        <xsl:if test="count(EPSYear) = 0">
          <xsl:if test="count(Analyst) = 0">
            <tr>
              <td class="colorlight">
                <table cellspacing="0" border="0" cellpadding="3" width="100%" class="colorlight">
                  <tr>
                    <td class="colorlight" colspan="7">
                      <span class="title2">
                        Estimates Summary
                      </span>
                    </td>
                  </tr>
                  <tr>
                    <td class="colorlight" colspan="7">
                      <span class="data">
                        No estimates data is currently available for this company.
                      </span>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </xsl:if>
        </xsl:if>
      </TABLE>
    </TD>
  </TR>

<xsl:call-template name="DisclaimerText"/>

  <xsl:if test="Company/Footer != ''">
  <TR class="default" align="left">
    <TD class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span></TD>
  </TR>
  </xsl:if>
</TABLE>

<xsl:call-template name="Copyright"/>
<!--
</xsl:template>
End Copy Here
-->
</xsl:otherwise>
</xsl:choose>
</xsl:template>
</xsl:stylesheet>
