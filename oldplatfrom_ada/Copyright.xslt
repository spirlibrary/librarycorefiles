<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.ord/TR/REC-html40"
	xmlns:irw="http://www.snl.com/xml/irw">
	
<xsl:output method="html" media-type="text/html" indent="no"/>

<xsl:template name="Copyright">	
 <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%">
	<TR>
		<TD COLSPAN="2"> <IMG SRC="/images/interactive/blank.gif" WIDTH="100%" HEIGHT="30" BORDER="0" alt="layout background"/><BR /></TD> 		
	</TR>
   <xsl:if test="Company/MobileSiteAvailability = 'true' and Company/IsMobileBrowser = 'true'">
   <tr>
     <td ALIGN="CENTER" COLSPAN="2">
       <a class="bottom" title="View Mobile Site">
         <xsl:attribute name="href">
           Mobile/Redirect.aspx?iid=<xsl:value-of select="Company/KeyInstn"/>
         </xsl:attribute>View Mobile Site
       </a>
     </td>
   </tr>
   </xsl:if>
	<TR>   
		<TD ALIGN="CENTER" COLSPAN="2"><SCRIPT LANGUAGE="JavaScript"> function TermsWindow(lKeyInstn) { var page = "disclaimer.aspx?IID=" + lKeyInstn; var winprops = "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=600,height=500"; popup = window.open(page, "TermsofUse", winprops) } function ConfirmClick() { if (confirm("You are now leaving the <xsl:value-of select="Company/InstnName"/> site.")) { top.location.href = 'http://marketintelligence.spglobal.com'; }  } //  End --></SCRIPT>
		<span class="default"> Copyright <xsl:value-of select="Company/CurrentYear"/>, </span> <A class="fielddef"  HREF="http://marketintelligence.spglobal.com" target="_new" title="S&amp;P Global Market Intelligence">&#169; S&amp;P Global Market Intelligence</A>&#160;&#160;<A class="fielddef" title="Terms of Use"><xsl:attribute name="href">javascript:TermsWindow(<xsl:value-of select="Company/KeyInstn"/>)</xsl:attribute>Terms of Use</A></TD>    
	</TR> 
</TABLE> 
</xsl:template>


<xsl:template name="Copyrightalt">	
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%">
	<TR>
		<TD COLSPAN="2"> <IMG SRC="/images/interactive/blank.gif" WIDTH="100%" HEIGHT="30" BORDER="0" alt="layout background"/><BR /></TD> 		
	</TR>
  <xsl:if test="Company/MobileSiteAvailability = 'true' and Company/IsMobileBrowser = 'true'">
    <tr>
      <td ALIGN="CENTER" COLSPAN="2">
        <a class="bottom" title="View Mobile Site">
          <xsl:attribute name="href">
            Mobile/Redirect.aspx?iid=<xsl:value-of select="Company/KeyInstn"/>
          </xsl:attribute>View Mobile Site
        </a>
      </td>
    </tr>
  </xsl:if>
	<TR>
		<TD ALIGN="CENTER" COLSPAN="2"><SCRIPT LANGUAGE="JavaScript"> function TermsWindow(lKeyInstn) { var page = "disclaimer.aspx?IID=" + lKeyInstn; var winprops = "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=600,height=500"; popup = window.open(page, "TermsofUse", winprops) } function ConfirmClick() { if (confirm("You are now leaving the <xsl:value-of select="Company/InstnName"/> site.")) { top.location.href = 'http://marketintelligence.spglobal.com'; }  } //  End --></SCRIPT><STYLE type="text/css">.Default_Small {FONT-SIZE: 9px;FONT-COLOR: #000000}</STYLE>
		<span class="default"> Copyright <xsl:value-of select="Company/CurrentYear"/>, </span><A class="fielddef" HREF="http://marketintelligence.spglobal.com" target="_new" title="S&amp;P Global Market Intelligence"><span class="data"> &#169; S&amp;P Global Market Intelligence</span></A><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;</xsl:text><A class="fielddef" title="Terms of Use"><xsl:attribute name="href">javascript:TermsWindow(<xsl:value-of select="Company/KeyInstn"/>)</xsl:attribute><span class="data">Terms of Use</span></A></TD>
	</TR> 
</TABLE>
</xsl:template>


<xsl:template name="Copyright_etrend">	
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%">
	<TR>
		<TD COLSPAN="2"> <IMG SRC="/images/interactive/blank.gif" WIDTH="100%" HEIGHT="1" BORDER="0" alt="layout background"/>2<BR /></TD>
	</TR>
  <xsl:if test="Company/MobileSiteAvailability = 'true' and Company/IsMobileBrowser = 'true'">
    <tr>
      <td ALIGN="CENTER" COLSPAN="2">
        <a class="bottom" title="View Mobile Site">
          <xsl:attribute name="href">
            Mobile/Redirect.aspx?iid=<xsl:value-of select="Company/KeyInstn"/>
          </xsl:attribute>View Mobile Site
        </a>
      </td>
    </tr>
  </xsl:if>
	<TR>
		<TD ALIGN="CENTER" COLSPAN="2"><SCRIPT LANGUAGE="JavaScript"> function TermsWindow(lKeyInstn) { var page = "disclaimer.aspx?IID=" + lKeyInstn; var winprops = "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=600,height=500"; popup = window.open(page, "TermsofUse", winprops) } function ConfirmClick() { if (confirm("You are now leaving the <xsl:value-of select="Company/InstnName"/> site.")) { top.location.href = 'http://marketintelligence.spglobal.com'; }  } //  End --></SCRIPT><span class="default">&#169; Copyright <xsl:value-of select="Company/CurrentYear"/>, </span><A class="fielddef" HREF="http://marketintelligence.spglobal.com" target="_new" title="S&amp;P Global Market Intelligence">S&amp;P Global Market Intelligence</A><p align="center"><A class="fielddef" title="Terms of Use"><xsl:attribute name="href">javascript:TermsWindow(<xsl:value-of select="Company/KeyInstn"/>)</xsl:attribute>Terms of Use</A></p><BR/></TD>
	</TR>
</TABLE> 
</xsl:template>


<xsl:template name="Copyright_ftrend">	
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%">
	<TR>
		<TD COLSPAN="2"> <IMG SRC="/images/interactive/blank.gif" WIDTH="100%" HEIGHT="1" BORDER="0" alt="layout background"/><BR /></TD>
	</TR>
  <xsl:if test="Company/MobileSiteAvailability = 'true' and Company/IsMobileBrowser = 'true'">
    <tr>
      <td ALIGN="CENTER" COLSPAN="2">
        <a class="bottom" title="View Mobile Site">
          <xsl:attribute name="href">
            Mobile/Redirect.aspx?iid=<xsl:value-of select="Company/KeyInstn"/>
          </xsl:attribute>View Mobile Site
        </a>
      </td>
    </tr>
  </xsl:if>
	<TR>
		<TD ALIGN="CENTER" COLSPAN="2"><SCRIPT LANGUAGE="JavaScript"> function TermsWindow(lKeyInstn) { var page = "disclaimer.aspx?IID=" + lKeyInstn; var winprops = "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=600,height=500"; popup = window.open(page, "TermsofUse", winprops) } function ConfirmClick() { if (confirm("You are now leaving the <xsl:value-of select="Company/InstnName"/> site.")) { top.location.href = 'http://marketintelligence.spglobal.com'; }  } //  End --></SCRIPT><span class="default">&#169; Copyright <xsl:value-of select="Company/CurrentYear"/>, </span><A class="fielddef" HREF="http://marketintelligence.spglobal.com" target="_new" title="S&amp;P Global Market Intelligence">S&amp;P Global Market Intelligence</A><p align="center"><A class="fielddef" title="Terms of Use"><xsl:attribute name="href">javascript:TermsWindow(<xsl:value-of select="Company/KeyInstn"/>)</xsl:attribute>Terms of Use</A></p><BR/></TD>
	</TR>
</TABLE> 
</xsl:template>


<xsl:template name="Copyright_fintrend">	
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%">
	<TR>
		<TD COLSPAN="2"> <IMG SRC="/images/interactive/blank.gif" WIDTH="100%" HEIGHT="1" BORDER="0" alt="layout background"/><BR /></TD>
	</TR>
  <xsl:if test="Company/MobileSiteAvailability = 'true' and Company/IsMobileBrowser = 'true'">
    <tr>
      <td ALIGN="CENTER" COLSPAN="2">
        <a class="bottom" title="View Mobile Site">
          <xsl:attribute name="href">
            Mobile/Redirect.aspx?iid=<xsl:value-of select="Company/KeyInstn"/>
          </xsl:attribute>View Mobile Site
        </a>
      </td>
    </tr>
  </xsl:if>
	<TR>
		<TD ALIGN="CENTER" COLSPAN="2"><SCRIPT LANGUAGE="JavaScript"> function TermsWindow(lKeyInstn) { var page = "disclaimer.aspx?IID=" + lKeyInstn; var winprops = "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=600,height=500"; popup = window.open(page, "TermsofUse", winprops) } function ConfirmClick() { if (confirm("You are now leaving the <xsl:value-of select="Company/InstnName"/> site.")) { top.location.href = 'http://marketintelligence.spglobal.com'; }  } //  End --></SCRIPT><span class="default">&#169; Copyright <xsl:value-of select="Company/CurrentYear"/>, </span><A class="fielddef" HREF="http://marketintelligence.spglobal.com" target="_new" title="S&amp;P Global Market Intelligence">S&amp;P Global Market Intelligence</A><p align="center"><A class="fielddef"><xsl:attribute name="href" title="Terms of Use">javascript:TermsWindow(<xsl:value-of select="Company/KeyInstn"/>)</xsl:attribute>Terms of Use</A></p><BR/></TD>
	</TR>
</TABLE> 
</xsl:template>


<xsl:template name="Copyright_instrend">	
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%">
	<TR>
		<TD COLSPAN="2"> <IMG SRC="/images/interactive/blank.gif" WIDTH="100%" HEIGHT="1" BORDER="0" alt="layout background"/><BR /></TD>
	</TR>
  <xsl:if test="Company/MobileSiteAvailability = 'true' and Company/IsMobileBrowser = 'true'">
    <tr>
      <td ALIGN="CENTER" COLSPAN="2">
        <a class="bottom" title="View Mobile Site">
          <xsl:attribute name="href">
            Mobile/Redirect.aspx?iid=<xsl:value-of select="Company/KeyInstn"/>
          </xsl:attribute>View Mobile Site
        </a>
      </td>
    </tr>
  </xsl:if>
	<TR>
		<TD ALIGN="CENTER" COLSPAN="2"><SCRIPT LANGUAGE="JavaScript"> function TermsWindow(lKeyInstn) { var page = "disclaimer.aspx?IID=" + lKeyInstn; var winprops = "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=600,height=500"; popup = window.open(page, "TermsofUse", winprops) } function ConfirmClick() { if (confirm("You are now leaving the <xsl:value-of select="Company/InstnName"/> site.")) { top.location.href = 'http://marketintelligence.spglobal.com'; }  } //  End --></SCRIPT><span class="default">&#169; Copyright <xsl:value-of select="Company/CurrentYear"/>, </span><A class="fielddef" HREF="http://marketintelligence.spglobal.com" target="_new" title="S&amp;P Global Market Intelligence">S&amp;P Global Market Intelligence</A><p align="center"><A class="fielddef" title="Terms of Use"><xsl:attribute name="href">javascript:TermsWindow(<xsl:value-of select="Company/KeyInstn"/>)</xsl:attribute>Terms of Use</A></p><BR/></TD>
	</TR>
</TABLE> 
</xsl:template>


<xsl:template name="Copyright_retrend">	
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%">
	<TR>
		<TD COLSPAN="2"> <IMG SRC="/images/interactive/blank.gif" WIDTH="100%" HEIGHT="1" BORDER="0" alt="layout background"/><BR /></TD>
	</TR>
  <xsl:if test="Company/MobileSiteAvailability = 'true' and Company/IsMobileBrowser = 'true'">
    <tr>
      <td ALIGN="CENTER" COLSPAN="2">
        <a class="bottom" title="View Mobile Site">
          <xsl:attribute name="href">
            Mobile/Redirect.aspx?iid=<xsl:value-of select="Company/KeyInstn"/>
          </xsl:attribute>View Mobile Site
        </a>
      </td>
    </tr>
  </xsl:if>
	<TR>
		<TD ALIGN="CENTER" COLSPAN="2"><SCRIPT LANGUAGE="JavaScript"> function TermsWindow(lKeyInstn) { var page = "disclaimer.aspx?IID=" + lKeyInstn; var winprops = "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=600,height=500"; popup = window.open(page, "TermsofUse", winprops) } function ConfirmClick() { if (confirm("You are now leaving the <xsl:value-of select="Company/InstnName"/> site.")) { top.location.href = 'http://marketintelligence.spglobal.com'; }  } //  End --></SCRIPT><span class="default">&#169; Copyright <xsl:value-of select="Company/CurrentYear"/>, </span><A class="fielddef" HREF="http://marketintelligence.spglobal.com" target="_new" title="S&amp;P Global Market Intelligence">S&amp;P Global Market Intelligence</A><p align="center"><A class="fielddef" title="Terms of Use"><xsl:attribute name="href">javascript:TermsWindow(<xsl:value-of select="Company/KeyInstn"/>)</xsl:attribute>Terms of Use</A></p><BR/></TD>
	</TR>
</TABLE> 
</xsl:template>


<xsl:template name="Copyright_sptrend">	
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%">
	<TR>
		<TD COLSPAN="2"> <IMG SRC="/images/interactive/blank.gif" WIDTH="100%" HEIGHT="1" BORDER="0" alt="layout background"/><BR /></TD>
	</TR>
  <xsl:if test="Company/MobileSiteAvailability = 'true' and Company/IsMobileBrowser = 'true'">
    <tr>
      <td ALIGN="CENTER" COLSPAN="2">
        <a class="bottom" title="View Mobile Site">
          <xsl:attribute name="href">
            Mobile/Redirect.aspx?iid=<xsl:value-of select="Company/KeyInstn"/>
          </xsl:attribute>View Mobile Site
        </a>
      </td>
    </tr>
  </xsl:if>
	<TR>
		<TD ALIGN="CENTER" COLSPAN="2"><SCRIPT LANGUAGE="JavaScript"> function TermsWindow(lKeyInstn) { var page = "disclaimer.aspx?IID=" + lKeyInstn; var winprops = "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=600,height=500"; popup = window.open(page, "TermsofUse", winprops) } function ConfirmClick() { if (confirm("You are now leaving the <xsl:value-of select="Company/InstnName"/> site.")) { top.location.href = 'http://marketintelligence.spglobal.com'; }  } //  End --></SCRIPT><span class="default">&#169; Copyright <xsl:value-of select="Company/CurrentYear"/>, </span><A class="fielddef" HREF="http://marketintelligence.spglobal.com" target="_new" title="S&amp;P Global Market Intelligence">S&amp;P Global Market Intelligence</A><p align="center"><A class="fielddef" title="Terms of Use"><xsl:attribute name="href">javascript:TermsWindow(<xsl:value-of select="Company/KeyInstn"/>)</xsl:attribute>Terms of Use</A></p><BR/></TD>
	</TR>
</TABLE> 
</xsl:template>


<xsl:template name="Copyright_strend">	
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%">
	<TR>
		<TD COLSPAN="2"> <IMG SRC="/images/interactive/blank.gif" WIDTH="100%" HEIGHT="1" BORDER="0" alt="layout background"/><BR /></TD>
	</TR>
  <xsl:if test="Company/MobileSiteAvailability = 'true' and Company/IsMobileBrowser = 'true'">
    <tr>
      <td ALIGN="CENTER" COLSPAN="2">
        <a class="bottom" title="View Mobile Site">
          <xsl:attribute name="href">
            Mobile/Redirect.aspx?iid=<xsl:value-of select="Company/KeyInstn"/>
          </xsl:attribute>View Mobile Site
        </a>
      </td>
    </tr>
  </xsl:if>
	<TR>
		<TD ALIGN="CENTER" COLSPAN="2"><SCRIPT LANGUAGE="JavaScript"> function TermsWindow(lKeyInstn) { var page = "disclaimer.aspx?IID=" + lKeyInstn; var winprops = "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=600,height=500"; popup = window.open(page, "TermsofUse", winprops) } function ConfirmClick() { if (confirm("You are now leaving the <xsl:value-of select="Company/InstnName"/> site.")) { top.location.href = 'http://marketintelligence.spglobal.com'; }  } //  End --></SCRIPT><span class="default">&#169; Copyright <xsl:value-of select="Company/CurrentYear"/>, </span><A class="fielddef" HREF="http://marketintelligence.spglobal.com" target="_new" title="S&amp;P Global Market Intelligence">S&amp;P Global Market Intelligence</A><p align="center"><A class="fielddef" title="Terms of Use"><xsl:attribute name="href">javascript:TermsWindow(<xsl:value-of select="Company/KeyInstn"/>)</xsl:attribute>Terms of Use</A></p><BR/></TD>
	</TR>
</TABLE> 
</xsl:template>

</xsl:stylesheet>