<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.ord/TR/REC-html40"
	xmlns:irw="http://www.snl.com/xml/irw">

<xsl:output method="html" media-type="text/html" indent="no"/>

<xsl:template match="CorpProfileDisclaimer">
<HTML>
<head><title>Disclaimer</title></head>
<xsl:variable name="KeyInstn" select="Company/KeyInstn"/>
<xsl:variable name="anchortag1" select="concat('/interactive/lookandfeel/', $KeyInstn, '/style.css')"/>
<xsl:variable name="anchortag2" select="concat('/Interactive/LookAndFeel/IRStyles/live_', $KeyInstn, '.css')"/>
<xsl:variable name="anchortag3" select="concat('/Interactive/LookAndFeel/IRStyles/live_', $KeyInstn, '_suppl.css')"/>
<link REL="stylesheet" TYPE="text/css"><xsl:attribute name="href"><xsl:value-of select="$anchortag1"/></xsl:attribute></link>
<link REL="stylesheet" TYPE="text/css"><xsl:attribute name="href"><xsl:value-of select="$anchortag2"/></xsl:attribute></link>
<link REL="stylesheet" TYPE="text/css"><xsl:attribute name="href"><xsl:value-of select="$anchortag3"/></xsl:attribute></link>
<body>

<xsl:variable name="TemplateName" select="Company/TemplateName"/>
<xsl:choose>
<!-- Template 2 -->
<xsl:when test="$TemplateName = 'AltDisclaim1'">
<SCRIPT LANGUAGE="JavaScript">
	function OpenDisclaimerWindow(url) {
	var page = url;
	var winprops = "toolbar=yes,location=yes,directories=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes";
	window.open(page, "", winprops);
	self.close();
	}
</SCRIPT>
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" CLASS="colordark">
<xsl:call-template name="TemplateONEstylesheet" />
<xsl:call-template name="Title_S2"/>
	<TR ALIGN="CENTER">
		<TD CLASS="leftTOPbord" colspan="2">
			<TABLE border="0" cellspacing="0" cellpadding="2"  width="100%">
				<tr>
					<td class="colorlight"><br />
						<span class="default"><xsl:value-of select="Disclaimer/DisclaimerText" disable-output-escaping="yes"/></span>
					<br /><br /></td>
				</tr>
				<tr>
					<td	align="center" class="colorlight" colspan="4">
						<a href="#" class="fielddef" onClick="self.close()" title="Back">
							Back
						</a>
						&#160;&#160;&#160;&#160;&#160;&#160;
						<xsl:variable name="url" select="UISettings/website" />
						<a href="#" class="fielddef" onClick="OpenDisclaimerWindow('{$url}'); return true" title="Continue">
							Continue
						</a>
						<br /><br />
					</td>	
				</tr>
			</TABLE>
		</TD>
	</TR>
</TABLE>
</xsl:when>
<!-- Template Default -->
<xsl:otherwise>
<SCRIPT LANGUAGE="JavaScript">
	function OpenDisclaimerWindow(url) {
	var page = url;
	var winprops = "toolbar=yes,location=yes,directories=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes";
	window.open(page, "", winprops);
	self.close();
	}
</SCRIPT>
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" CLASS="colordark">
<xsl:call-template name="Title_S1"/>
	<TR ALIGN="CENTER">
		<TD CLASS="colordark" colspan="2">
			<TABLE border="0" cellspacing="0" cellpadding="2" class="data" width="100%">
				<tr>
					<td class="colorlight"><br />
						<span class="default"><xsl:value-of select="Disclaimer/DisclaimerText" disable-output-escaping="yes"/></span><br /><br /></td>
				</tr>
				<tr>
					<td	align="center" class="colorlight" colspan="4">
						<a href="#" class="fielddef" onClick="self.close()" title="Back">
							Back
						</a>
						&#160;&#160;&#160;&#160;&#160;&#160;
						<xsl:variable name="url" select="UISettings/website" />
						<a href="#" class="fielddef" onClick="OpenDisclaimerWindow('{$url}'); return true" title="Continue">
							Continue
						</a>
						<br /><br />
					</td>	
				</tr>
			</TABLE>
		</TD>
	</TR>
</TABLE>
</xsl:otherwise>


</xsl:choose>



<xsl:call-template name="Copyright"/>

</body>
</HTML>
</xsl:template>

</xsl:stylesheet>