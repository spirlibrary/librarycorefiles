<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.ord/TR/REC-html40"
	xmlns:irw="http://www.snl.com/xml/irw">

    <xsl:output method="html" media-type="text/html" indent="no"/>
    <xsl:decimal-format name="data" NaN="NA"/>

    <!-- Start Default Template here-->

    <!--
//This is the default template. Most changes are to either remove the company name, ticker, or exchange. you can also change the name of the page or remove this row all together. 
//These templates below are broken up how the page is displayed.
-->
    <xsl:template name="HEADER_HistoricPrices">
        <xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
        <TR>
            <TD CLASS="colordark" nowrap="" ALIGN="LEFT">
                <SPAN CLASS="title1light">
                    <xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/>
                </SPAN>
            </TD>
            <TD CLASS="colordark" NOWRAP="" align="right" valign="middle">
                <span class="subtitlelight"><xsl:value-of select="$CompanyNameCaps"/>&#160;(<xsl:value-of select="Company/Exchange"/>&#160;-&#160;<xsl:value-of select="Company/Ticker"/>)&#160;
                </span>
            </TD>
        </TR>
        <TR class="default" align="left">
            <TD class="colorlight" colspan="2">
                <span class="default">
                    <xsl:value-of select="Company/Header" disable-output-escaping="yes"/>
                </span>
            </TD>
        </TR>
    </xsl:template>



    <xsl:template name="HistoricPrices_data">
        <xsl:variable name="rowCount" select="count(Prices)+1"/>

        <STYLE TYPE="text/css">
.error_bold
{
COLOR: #ff0000;
FONT-SIZE: 9px;
FONT-WEIGHT: bold;
TEXT-DECORATION: none
}
</STYLE>

        <xsl:choose>
            <xsl:when test="count(Prices) &gt; 0">
                <xsl:variable name="keyinstn" select="Company/KeyInstn"/>
                <xsl:variable name="formaction" select="concat('HistoricPrices.aspx?IID=', $keyinstn)"></xsl:variable>
                <form method="POST" id="form1" name="form1">
					<fieldset>
						<legend>Form1</legend>
						<xsl:attribute name="action">
							<xsl:value-of select="$formaction"/>
						</xsl:attribute>
						<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="2" width="100%" ID="TABLE2">
							<xsl:if test="not(contains(translate(Company/URL,$ucletters,$lcletters), 'print=1'))">
								<TR>
									<TD align="left" class="header" colspan="2">Search Prices</TD>
								</TR>
							</xsl:if>
							<TR>
								<xsl:if test="not(contains(translate(Company/URL,$ucletters,$lcletters), 'print=1'))">
									<TD align="left" class="data" valign="top">
										<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="2" width="250px" ID="TABLE3">
											<TR>
												<TD align="" class="data">
													<span class="defaultbold">Start Date: </span>
												</TD>
												<TD align="right" >
													<label class="visuallyhidden" for="startdate">startdate</label>
													<input class="input" type="text" id="startdate" name="startdate" size="10"/>
												</TD>
												<xsl:if test="FormVals/StartDate != ''">
													<input type="hidden" name="hdnStartDate">
														<xsl:attribute name="value">
															<xsl:value-of select="FormVals/StartDate"/>
														</xsl:attribute>
													</input>
												</xsl:if>
												<xsl:if test="FormVals/FormPrint = '1'">
													<input type="hidden" name="FormPrint">
														<xsl:attribute name="value">
															<xsl:value-of select="FormVals/FormPrint"/>
														</xsl:attribute>
													</input>
												</xsl:if>
											</TR>
											<TR>
												<TD class="data">
													<span class="defaultbold">End Date: </span>
												</TD>
												<TD align="right" >
													<label class="visuallyhidden" for="enddate">enddate</label>
													<input class="input" type="text" id="enddate" name="enddate" size="10"/>
												</TD>
												<xsl:if test="FormVals/EndDate != ''">
													<input type="hidden" name="hdnEndDate">
														<xsl:attribute name="value">
															<xsl:value-of select="FormVals/EndDate"/>
														</xsl:attribute>
													</input>
												</xsl:if>
											</TR>
											<TR>
												<TD align="" >
													<input class="submit" type="submit" id="enter" name="enter" value="Apply"/>
												</TD>
												<td class="data" nowrap="nowrap"  colspan="" align="right">Ex: 01/01/2000</td>
											</TR>
											<xsl:if test="Errors/DateError">
												<tr>
													<td class="error_bold" colspan="2">
														<xsl:value-of select="Errors/DateError" disable-output-escaping="yes"/>
													</td>
												</tr>
											</xsl:if>
										</TABLE>
									</TD>
								</xsl:if>
								<TD align="rigth" class="data" valign="top">
									<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" width="100%" ID="TABLE3">
										<tr>
											<TD width="30%" align="right" class="header">Date&#160;&#160;&#160;&#160;&#160;&#160;</TD>
											<TD align="center" class="header">Price</TD>
											<TD align="center" class="header">High</TD>
											<TD align="center" class="header">Low</TD>
											<TD align="right" class="header">Volume</TD>
										</tr>
										<xsl:for-each select="Prices">
											<TR>
												<TD align="right" class="data">
													<span class="default">
														<xsl:value-of select="substring-before(Date,' ')"/>
													</span>
												</TD>
												<TD align="center" class="data">
													<span class="default">
														<xsl:value-of select="format-number(Price,'#,##0.00','data')"/>
													</span>
												</TD>
												<xsl:choose>
													<xsl:when test="string-length(High) &gt; 0">
														<TD align="center" class="data">
															<span class="default">
																<xsl:value-of select="format-number(High,'#,##0.00','data')"/>
															</span>
														</TD>
														<TD align="center" class="data">
															<span class="default">
																<xsl:value-of select="format-number(Low,'#,##0.00','data')"/>
															</span>
														</TD>
													</xsl:when>
													<xsl:otherwise>
														<TD align="center" class="data">
															<span class="default">
																<xsl:value-of select="format-number(Price,'#,##0.00','data')"/>
															</span>
														</TD>
														<TD align="center" class="data">
															<span class="default">
																<xsl:value-of select="format-number(Price,'#,##0.00','data')"/>
															</span>
														</TD>
													</xsl:otherwise>
												</xsl:choose>
												<TD align="right" class="data">
													<span class="default">
														<xsl:value-of select="format-number(Volume,'#,##0','data')"/>
													</span>
												</TD>
											</TR>
										</xsl:for-each>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
					</fieldset>
                </form>
            </xsl:when>
            <xsl:otherwise>
                <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="2" width="100%" ID="TABLE2">
                    <tr>
                        <td>
                            <span class="defaultbold">
                                <xsl:choose>
                                    <xsl:when test="substring(Company/InstnName,string-length(Company/InstnName)) = '.'">
			No historic pricing data is available for <xsl:value-of select="Company/InstnName"/>
			</xsl:when>
                                    <xsl:otherwise>
			No historic pricing data is available for <xsl:value-of select="Company/InstnName"/>.
			</xsl:otherwise>
                                </xsl:choose>
                            </span>
                        </td>
                    </tr>
                </TABLE>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <!-- Default Template Ends here. all templates below are ONLY if you are using the 'Style Template' section in the console. -->


    <!-- This is Template ONE option in the console under 'Style Template'
//This template is a striped down version of the main template. Everything (data) is displayed in single rows.
//The sections will be commented on where the rows are. you should be able to just move rows (entire chunks of data) where you need too. Edits to this section are rare, mostly due to size (width of the page).
-->
    <xsl:template name="HEADER_HistoricPrices2">
        <xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
        <TR>
            <TD CLASS="titletest" nowrap="" colspan="2" align="left">
                <xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/>
                <br />
                <IMG SRC="" WIDTH="2" HEIGHT="1" alt="" />
                <span class="title2colordark titletest2"><xsl:value-of select="$CompanyNameCaps"/>&#160;(<xsl:value-of select="Company/Exchange"/>&#160;-&#160;<xsl:value-of select="Company/Ticker"/>)&#160;
                </span>
            </TD>
        </TR>
        <TR class="default" align="left">
            <TD class="colorlight" colspan="2">
                <span class="default">
                    <xsl:value-of select="Company/Header" disable-output-escaping="yes"/>
                </span>
            </TD>
        </TR>
    </xsl:template>

    <xsl:template name="HistoricPrices_data2a">
        <STYLE TYPE="text/css">
.error_bold
{
COLOR: #ff0000;
FONT-SIZE: 9px;
FONT-WEIGHT: bold;
TEXT-DECORATION: none
}
</STYLE>
        <xsl:choose>
            <xsl:when test="count(Prices) &gt; 0">
                <xsl:variable name="keyinstn" select="Company/KeyInstn"/>
                <xsl:variable name="formaction" select="concat('HistoricPrices.aspx?IID=', $keyinstn)"></xsl:variable>
                <form method="POST" id="form1" name="form1">
					<fieldset>
						<legend>Form1</legend>
						<xsl:attribute name="action">
							<xsl:value-of select="$formaction"/>
						</xsl:attribute>
						<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="2" width="100%" ID="TABLE3">
							<TR>
								<xsl:if test="not(contains(translate(Company/URL,$ucletters,$lcletters), 'print=1'))">
									<TD class="data" valign="top">
										<table border="0" width="250px" CELLSPACING="0" CELLPADDING="3">
											<tr>
												<td>
													<span class="defaultbold">Start Date: </span>
												</td>
												<td>
													<label class="visuallyhidden" for="startdate">startdate</label>
													<input type="text" id="startdate" name="startdate" size="10" CLASS="input" />
												</td>
											</tr>
											<tr>
												<td>
													<span class="defaultbold">End Date:</span>
												</td>
												<td>
													<label class="visuallyhidden" for="enddate">enddate</label>
													<input type="text" id="enddate" name="enddate" size="10" CLASS="input" />
												</td>
											</tr>
											<tr>
												<td>
													<input type="submit" id="enter" name="enter" value="Apply" CLASS="submit"/>
												</td>
												<td class="data" nowrap="nowrap"  colspan="" align="">Ex: 01/01/2000</td>
											</tr>
											<xsl:if test="Errors/DateError">
												<TR>
													<TD class="error_bold" colspan="2">
														<xsl:value-of select="Errors/DateError" disable-output-escaping="yes"/>
													</TD>
												</TR>
											</xsl:if>
										</table>
									</TD>
								</xsl:if>
								<TD class="data" valign="top">
									<xsl:call-template name="HistoricPrices_output2"/>
								</TD>
							</TR>
						</TABLE>
					</fieldset>
                </form>
            </xsl:when>
            <xsl:otherwise>
                <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="2" width="100%" ID="TABLE3">
                    <tr>
                        <td valign="top">
                            <span class="defaultbold">
                                <xsl:choose>
                                    <xsl:when test="substring(Company/InstnName,string-length(Company/InstnName)) = '.'">
			  No historic pricing data is available for <xsl:value-of select="Company/InstnName"/>
			  </xsl:when>
                                    <xsl:otherwise>
			  No historic pricing data is available for <xsl:value-of select="Company/InstnName"/>.
			  </xsl:otherwise>
                                </xsl:choose>
                            </span>
                        </td>
                    </tr>
                </TABLE>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="HistoricPrices_output2">
        <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="2" width="100%">
            <TR>
                <TD align="center" class="surrleft datashade">
                    <span class="defaultbold">Date</span>
                </TD>
                <TD align="center" class=" datashade surr_topbot">
                    <span class="defaultbold">Price</span>
                </TD>
                <TD align="center" class="datashade surr_topbot">
                    <span class="defaultbold">High</span>
                </TD>
                <TD align="center" class="datashade surr_topbot">
                    <span class="defaultbold">Low</span>
                </TD>
                <TD align="right" class="surrright datashade">
                    <span class="defaultbold">Volume</span>
                </TD>
            </TR>
            <xsl:for-each select="Prices">
                <TR>
                    <TD width="30%" align="center" class="data">
                        <span class="default">
                            <xsl:value-of select="substring-before(Date,' ')"/>
                        </span>
                    </TD>
                    <TD align="center" class="data">
                        <span class="default">
                            <xsl:value-of select="format-number(Price,'#,##0.00','data')"/>
                        </span>
                    </TD>
                    <xsl:choose>
                        <xsl:when test="string-length(High) &gt; 0">
                            <TD align="center" class="data">
                                <span class="default">
                                    <xsl:value-of select="format-number(High,'#,##0.00','data')"/>
                                </span>
                            </TD>
                            <TD align="center" class="data">
                                <span class="default">
                                    <xsl:value-of select="format-number(Low,'#,##0.00','data')"/>
                                </span>
                            </TD>
                        </xsl:when>
                        <xsl:otherwise>
                            <TD align="center" class="data">
                                <span class="default">
                                    <xsl:value-of select="format-number(Price,'#,##0.00','data')"/>
                                </span>
                            </TD>
                            <TD align="center" class="data">
                                <span class="default">
                                    <xsl:value-of select="format-number(Price,'#,##0.00','data')"/>
                                </span>
                            </TD>
                        </xsl:otherwise>
                    </xsl:choose>
                    <TD align="right" class="data">
                        <span class="default">
                            <xsl:value-of select="format-number(Volume,'#,##0','data')"/>
                        </span>
                    </TD>
                </TR>
            </xsl:for-each>
        </TABLE>
    </xsl:template>
    <!-- Template ONE Ends here. -->





    <!-- Template 3 -->
    <xsl:template name="HistoricPrices_data3a">
        <STYLE TYPE="text/css">
.error_bold
{
COLOR: #ff0000;
FONT-SIZE: 9px;
FONT-WEIGHT: bold;
TEXT-DECORATION: none
}
</STYLE>
        <xsl:choose>
            <xsl:when test="count(Prices) &gt; 0">
                <xsl:variable name="keyinstn" select="Company/KeyInstn"/>
                <xsl:variable name="formaction" select="concat('HistoricPrices.aspx?IID=', $keyinstn)"></xsl:variable>
                <form method="POST" id="form1" name="form1">
					<fieldset>
						<legend>Form1</legend>
					
                    <xsl:attribute name="action">
                        <xsl:value-of select="$formaction"/>
                    </xsl:attribute>
                    <table BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" class="table2">
                        <xsl:if test="not(contains(translate(Company/URL,$ucletters,$lcletters), 'print=1'))">
                        <TR>
                            <TD  class="datashade table1_item" colspan="2">
                                <span class="defaultbold">Search Prices</span>
                            </TD>
                        </TR>
                        </xsl:if>
                        <TR>
                            <xsl:if test="not(contains(translate(Company/URL,$ucletters,$lcletters), 'print=1'))">
                                <td valign="top">
                                    <table cellpadding="3" cellspacing="0" border="0" width="250px">
                                        <tr>
                                            <td>
                                                <span class="defaultbold">Start Date: </span>
                                            </td>
                                            <td align="right">
												<label class="visuallyhidden" for="startdate">startdate</label>
												<input type="text" id="startdate" name="startdate" size="10" CLASS="input" />
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="defaultbold">End Date:</span>
                                            </td>
                                            <td align="right">
												<label class="visuallyhidden" for="enddate">enddate</label>
                                                <input type="text" id="enddate" name="enddate" size="10" CLASS="input" />
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="data" nowrap="nowrap" align="right" colspan="2">Ex: 01/01/2000</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" align="right">
                                                <input type="submit" id="enter" name="enter" value="Apply" CLASS="submit"/>
                                            </td>
                                            <td></td>
                                        </tr>
                                    </table>
                                </td>
                            </xsl:if>
                            <td align="right">
                                <xsl:call-template name="HistoricPrices_output3"/>
                                <xsl:if test="Errors/DateError">
                                    <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                        <TR>
                                            <TD class="error_bold" >
                                                <xsl:value-of select="Errors/DateError" disable-output-escaping="yes"/>
                                            </TD>
                                        </TR>
                                    </table>
                                </xsl:if>
                            </td>
                        </TR>
                    </table>
					</fieldset>
				</form>
            </xsl:when>
            <xsl:otherwise>
                <table BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" class="table2">
                    <tr>
                        <td align="center">
                            <span class="defaultbold" >
                                <xsl:choose>
                                    <xsl:when test="substring(Company/InstnName,string-length(Company/InstnName)) = '.'"><br />No historic pricing data is available for <xsl:value-of select="Company/InstnName"/>
		</xsl:when>
                                    <xsl:otherwise><br />No historic pricing data is available for <xsl:value-of select="Company/InstnName"/>.
		</xsl:otherwise>
                                </xsl:choose>
                            </span>
                        </td>
                    </tr>
                </table>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="HistoricPrices_output3">
        <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" width="100%" class="table1">
            <TR>
                <TD width="30%" align="right" class="datashade table1_item">
                    <span class="defaultbold">Date&#160;&#160;&#160;&#160;&#160;&#160;</span>
                </TD>
                <TD align="center" class="datashade table1_item">
                    <span class="defaultbold">Price</span>
                </TD>
                <TD align="center" class="datashade table1_item">
                    <span class="defaultbold">High</span>
                </TD>
                <TD align="center" class="datashade table1_item">
                    <span class="defaultbold">Low</span>
                </TD>
                <TD align="right" class="datashade table1_item">
                    <span class="defaultbold">Volume</span>
                </TD>
            </TR>
            <xsl:for-each select="Prices">
                <TR>
                    <TD width="30%" align="right" class="table1_item">
                        <span class="default">
                            <xsl:value-of select="substring-before(Date,' ')"/>
                        </span>
                    </TD>
                    <TD align="center" class="table1_item">
                        <span class="default">
                            <xsl:value-of select="format-number(Price,'#,##0.00','data')"/>
                        </span>
                    </TD>
                    <xsl:choose>
                        <xsl:when test="string-length(High) &gt; 0">
                            <TD align="center" class="table1_item">
                                <span class="default">
                                    <xsl:value-of select="format-number(High,'#,##0.00','data')"/>
                                </span>
                            </TD>
                            <TD align="center" class="table1_item">
                                <span class="default">
                                    <xsl:value-of select="format-number(Low,'#,##0.00','data')"/>
                                </span>
                            </TD>
                        </xsl:when>
                        <xsl:otherwise>
                            <TD align="center" class="table1_item">
                                <span class="default">
                                    <xsl:value-of select="format-number(Price,'#,##0.00','data')"/>
                                </span>
                            </TD>
                            <TD align="center" class="table1_item">
                                <span class="default">
                                    <xsl:value-of select="format-number(Price,'#,##0.00','data')"/>
                                </span>
                            </TD>
                        </xsl:otherwise>
                    </xsl:choose>
                    <TD align="right" class="table1_item">
                        <span class="default">
                            <xsl:value-of select="format-number(Volume,'#,##0','data')"/>
                        </span>
                    </TD>
                </TR>
            </xsl:for-each>
        </TABLE>
    </xsl:template>
    <!-- Template 3 Ends -->


    <!-- DatePicker function Apply in all templates -->
    <xsl:template name="HistoricTextSearch">
        <xsl:if test="not(contains(translate(Company/URL,$ucletters,$lcletters), 'print=1'))">
            <xsl:if test="$Showdatepicker = 'true'">                
                <xsl:if test="$jquery = 'true'">
                  <script language="javascript" type="text/javascript" src="{$jquerymin}"></script>
                </xsl:if>
                <link rel="stylesheet" href="javascript/calendar/ui.datepicker.css" type="text/css"/>
                <script language="javascript" type="text/javascript" src="javascript/ui.datepicker.js"></script>
                <script type="text/javascript">
                    <![CDATA[
                        (function( $ )
                        {
                            $(function() 
                            {
                                var dates = $( "#startdate, #enddate" ).datepicker({ dateFormat: 'mm/dd/yy', buttonImage: 'javascript/calendar/calendar.gif', buttonImageOnly: true, showOn: 'both', closeAtTop: false, showStatus: true });                  
                             });
                        })( jQuery );
                    ]]>
                </script>
            </xsl:if>
        </xsl:if>
    </xsl:template>

    <!-- Main XSLT templates are here. these are the templates which are actually being displayed in the page. this is for TOP LEVEL editing. if you dont need to use the individual templates above you can do your main editing here when pulling THESE templates below you must carry the top comment with them and uncomment them when you drop it into the company specific xslt. -->


    <xsl:template match="irw:HistoricPrices">
        <xsl:variable name="TemplateName" select="Company/TemplateName"/>
        <xsl:call-template name="HistoricTextSearch" />
        <xsl:choose>
            <!-- Template 1 -->
            <xsl:when test="$TemplateName = 'AltHistPrice1'">
                <xsl:call-template name="TemplateONEstylesheet" />
                <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" CLASS="">
                    <xsl:call-template name="Title_S2"/>
                    <TR ALIGN="CENTER">
                        <TD CLASS="leftTOPbord" colspan="2" class="data">
                            <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="4" WIDTH="100%" ID="TABLE1">
                                <TR>
                                    <TD VALIGN="TOP">
                                        <xsl:call-template name="HistoricPrices_data2a"/>
                                    </TD>
                                </TR>
                            </TABLE>
                        </TD>
                    </TR>
                    <xsl:if test="Company/Footer != ''">
                        <TR class="data" ALIGN="left">
                            <TD colspan="2">
                                <span class="default">
                                    <xsl:value-of select="Company/Footer" disable-output-escaping="yes"/>
                                </span>
                            </TD>
                        </TR>
                    </xsl:if>
                </TABLE>
                <xsl:call-template name="Copyright"/>
            </xsl:when>
            <!-- End Template 2 -->

            <!-- Template 3 -->
            <xsl:when test="$TemplateName = 'AltHistPrice3'">
                <xsl:call-template name="TemplateTHREEstylesheet" />
                <xsl:call-template name="Title_T3"/>
                <xsl:call-template name="HistoricPrices_data3a"/>
                <xsl:if test="Company/Footer != ''">
                    <table BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" class="data">
                        <tr>
                            <td class="data" align="left" valign="top">
                                <span class="default">
                                    <xsl:value-of select="Company/Footer" disable-output-escaping="yes"/>
                                </span>
                            </td>
                        </tr>
                    </table>
                </xsl:if>
                <xsl:call-template name="Copyright"/>
            </xsl:when>
            <!-- End Template 3 -->


            <xsl:otherwise>
                <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" CLASS="colordark">
                    <xsl:call-template name="Title_S1"/>
                    <TR ALIGN="CENTER">
                        <TD CLASS="colordark" colspan="2">
                            <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" ID="TABLE1">
                                <TR class="colorlight">
                                    <TD VALIGN="TOP" CLASS="colorlight">
                                        <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="2" width="100%" ID="TABLE2" class="data">
                                            <TR>
                                                <TD align="left">
                                                    <xsl:call-template name="HistoricPrices_data"/>
                                                </TD>
                                            </TR>
                                        </TABLE>
                                    </TD>
                                </TR>
                            </TABLE>
                        </TD>
                    </TR>
                    <xsl:if test="Company/Footer != ''">
                        <TR class="colorlight" ALIGN="left">
                            <TD colspan="2">
                                <span class="default">
                                    <xsl:value-of select="Company/Footer" disable-output-escaping="yes"/>
                                </span>
                            </TD>
                        </TR>
                    </xsl:if>
                </TABLE>
                <xsl:call-template name="Copyright"/>
            </xsl:otherwise>


        </xsl:choose>



    </xsl:template>


</xsl:stylesheet>
