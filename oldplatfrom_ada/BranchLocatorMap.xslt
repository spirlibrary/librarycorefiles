<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
]>

<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.ord/TR/REC-html40"
    xmlns:v="urn:schemas-microsoft-com:vml"
	xmlns:irw="http://www.snl.com/xml/irw">

  <xsl:output method="html" media-type="text/html" indent="no"/>
  <xsl:decimal-format name="data" NaN="NA"/>



  <!--##SNL TEXT REPLACEMENT-->

  <!-- Start Default Template here-->

  <!--Template for title and instructions of form -->
  <xsl:template name="FormTitle">
    <table border="0" width="100%" align="left" cellpadding="3" cellspacing="0" class="header">

      <tr>
        <th class="header" align="left" valign="top">
        <xsl:if test="count(Error) &gt; 0 ">
        	<xsl:attribute name="colspan">2</xsl:attribute>
        </xsl:if>
        Find Branch Locations</th>
      </tr>

      <xsl:call-template name="LocationRadius" />

      <tr>
        <xsl:if test="FormOptions/MiniForm = 'true'">
          <xsl:attribute name="style">
            display: none;
          </xsl:attribute>
        </xsl:if>
        <td class="data" align="left" valign="top" width="100%">An asterisk (*) indicates a required field.</td>
      </tr>

      <tr>
        <xsl:if test="FormOptions/MiniForm = 'true'">
          <xsl:attribute name="style">
            display: none;
          </xsl:attribute>
        </xsl:if>
        <td class="data" align="left" valign="top">
          Searches must contain a City + State or Zip Code
          <br />
          <br />
        </td>
      </tr>

      <xsl:if test="count(Error) &gt; 0 ">
        <tr>
          <td class="data" align="left" valign="top">
            <span class="data" name="errortext" style="color:#FF0000">
              <xsl:value-of select="Error/ErrorString"  disable-output-escaping="yes"/>
              <br />
              <br />
            </span>
          </td>
        </tr>
      </xsl:if>

      <xsl:call-template name="Address" />

      <tr>
        <td class="data" align="left" valign="top">
          <br />
          <input class="default" type="reset" value="Clear"/>&#160;<input class="default" type="button" value="Search" id="SearchButton" onclick="load(this.form)"/>
        </td>
      </tr>

    </table>
  </xsl:template>

  <!--Template for location type choices and radius -->
  <xsl:template name="LocationRadius">
    <xsl:variable name='type' select="FormInfo/Type"/>
    <tr>
      <td align="left" class="data">
        <xsl:for-each select="FacilityType">
			<label class="visuallyhidden" for="FacilityType">FacilityType</label>
          <input class="data" type="checkbox" name="FacilityType" id="FacilityType" onclick="unshowAll();">
            <xsl:attribute name="value">
              <xsl:value-of select="KeyBankFacilityType"/>
            </xsl:attribute>
            <xsl:if test="contains($type, KeyBankFacilityType)">
              <xsl:attribute name="checked">
                true
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="string-length($type) = 0 and KeyBankFacilityType = '0'">
              <xsl:attribute name="checked">
                true
              </xsl:attribute>
            </xsl:if>
          </input>
          <xsl:value-of select="BankFacilityType"/>&nbsp;&nbsp;
        </xsl:for-each>
		  <label class="visuallyhidden" for="selectAll">selectAll</label>
        <input class="data" type="checkbox" name="selectAll" id="selectAll" value="1" onclick="showAll();" />Show All
        <br />
        <br />
      </td>
      <input type="hidden" name="Radius" value="0" />
      <!--We want to leave this commented out for reference in case a client asks for radius functionality-->
      <!--<td class="data" rowspan="4" align="left" valign="top">
          <xsl:variable name="radius" select="FormInfo/Radius"/>
          <select name="Radius" class="data">
            <xsl:choose>
              <xsl:when test="not($radius)">
                <option class="data" value="5" Selected="selected">5 miles</option>
              </xsl:when>
              <xsl:when test="contains($radius,'5')">
                <option class="data" value="5" Selected="selected">5 miles</option>
              </xsl:when>
              <xsl:otherwise>
                <option class="data" value="5">5 miles</option>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
              <xsl:when test="contains($radius,'10')">
                <option class="data" value="10" Selected="selected">10 miles</option>
              </xsl:when>
              <xsl:otherwise>
                <option class="data" value="10">10 miles</option>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
              <xsl:when test="contains($radius,'20')">
                <option class="data" value="20" Selected="selected">20 miles</option>
              </xsl:when>
              <xsl:otherwise>
                <option class="data" value="20">20 miles</option>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
              <xsl:when test="contains($radius,'50')">
                <option class="data" value="50" Selected="selected">50 miles</option>
              </xsl:when>
              <xsl:otherwise>
                <option class="data" value="50">50 miles</option>
              </xsl:otherwise>
            </xsl:choose>
          </select>
        </td>-->
    </tr>
  </xsl:template>

  <!--Template for address entry part of form -->
  <xsl:template name="Address">
    <tr>
      <td class="data" align="left" valign="top">

        <table border="0" width="50%" align="left" cellpadding="2" cellspacing="0">
          <tr valign="middle">
            <xsl:if test="FormOptions/MiniForm = 'true'">
              <xsl:attribute name="style">
                display: none;
              </xsl:attribute>
            </xsl:if>
            <td align="right" class="data">Street:</td>
            <td align="left" colspan="3">
				<label class="visuallyhidden" for="Street">Street</label>
              <input class="data" type="text" name="Street" ID="Street" size="59">
                <xsl:attribute name="Value">
                  <xsl:value-of select="FormInfo/Street"/>
                </xsl:attribute>
              </input>
            </td>
          </tr>
          <tr valign="middle">
            <xsl:if test="FormOptions/MiniForm = 'true'">
              <xsl:attribute name="style">
                display: none;
              </xsl:attribute>
            </xsl:if>
            <td align="right" class="data">*City:</td>
            <td class="data"  align="left">
				<label class="visuallyhidden" for="City">City</label>
              <input class="data" type="text" name="City" ID="City">
                <xsl:attribute name="Value">
                  <xsl:value-of select="FormInfo/City"/>
                </xsl:attribute>
              </input>
            </td>
            <td align="right" class="data" nowrap="">&#160;AND *State:</td>
            <td class="data" align="left">
				<label class="visuallyhidden" for="State">State</label>
              <select class="data" name="State" ID="State">
                <xsl:call-template name="States" />
              </select>
            </td>
          </tr>
          <tr valign="middle">
            <xsl:if test="FormOptions/MiniForm = 'true'">
              <xsl:attribute name="style">
                display: none;
              </xsl:attribute>
            </xsl:if>
            <td align="right" class="data">AND/OR</td>
            <td  align="left" colspan="3">&#160;</td>
          </tr>
          <tr valign="middle">
            <xsl:choose>
              <xsl:when test="FormOptions/MiniForm = 'true'">
                <td align="left" class="data">Zip Code:</td>
              </xsl:when>
              <xsl:otherwise>
                <td align="right" class="data">*ZIP:</td>
              </xsl:otherwise>
            </xsl:choose>
            <td  align="left" colspan="3">
				<label class="visuallyhidden" for="ZIP">ZIP</label>
              <input class="data" type="text" name="ZIP" id="ZIP">
                <xsl:variable name='zip' select="FormInfo/Zip"/>
                <xsl:attribute name="Value">
                  <xsl:choose>
                    <xsl:when test="contains($zip,'Enter')"></xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="FormInfo/Zip"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:attribute>
              </input>
            </td>
          </tr>
        </table>

      </td>
    </tr>
  </xsl:template>

  <!--Template for last part of form with hours preferences and display options -->
  <xsl:template name="ResultsChoices">
    <span class="defaultbold">To receive more accurate results, you may refine your search by selecting from the criteria below.</span>
    <table border="0" width="100%" align="left" cellpadding="1" cellspacing="0" class="header" >
      <tr>
        <td class="header" align="left" valign="top">

          <table border="0" width="100%" align="left" cellpadding="3" cellspacing="0" class="header">
            <tr valign="top">
              <th width="300px" align="left" class="header" valign="middle">Extended Hours</th>
              <th colspan="2" align="left" class="header" valign="middle">Results Display</th>
            </tr>
            <tr valign="top">
              <td align="left" class="data">
                <tr>
                  <td class="data">
					  <label class="visuallyhidden" for="extendend">extendend</label>
                    <select name="extended" class="data" id="extendend">
                      <xsl:variable name="ext" select="FormInfo/Extended"/>
                      <xsl:choose>
                        <xsl:when test="contains($ext,'NoPref')">
                          <option class="data" value="NoPref" SELECTED="selected">No Preference</option>
                          <option class="data" value="Yes">Yes</option>
                          <option class="data" value="No">No</option>
                        </xsl:when>
                        <xsl:when test="contains($ext,'Yes')">
                          <option class="data" value="NoPref" >No Preference</option>
                          <option class="data" value="Yes" SELECTED="selected">Yes</option>
                          <option class="data" value="No">No</option>
                        </xsl:when>
                        <xsl:when test="contains($ext,'No')">
                          <option class="data" value="NoPref" >No Preference</option>
                          <option class="data" value="Yes" >Yes</option>
                          <option class="data" value="No" SELECTED="selected">No</option>
                        </xsl:when>
                        <xsl:otherwise>
                          <option class="data" value="NoPref" SELECTED="selected">No Preference</option>
                          <option class="data" value="Yes">Yes</option>
                          <option class="data" value="No">No</option>
                        </xsl:otherwise>
                      </xsl:choose>
                    </select>
                  </td>
                  <td colspan="2" align="left" class="data">
                    <xsl:variable name="disp" select="FormInfo/Display"/>
                    <xsl:choose>
                      <xsl:when test="contains($disp,'ListAndMap')">
						  <label class="visuallyhidden" for="locationmap">locationmap</label>
                        <input class="data" type="radio" name="ResultDisp" value="ListAndMap" checked="checked" nowrap="" id="locationmap"/>Locations and Map<br />
						  <label class="visuallyhidden" for="LocationOnly">LocationOnly</label>
						  <input class="data" type="radio" name="ResultDisp" value="LocationOnly" id="LocationOnly"  />Locations Only
                      </xsl:when>
                      <xsl:when test="contains($disp,'LocationOnly')">
						  <label class="visuallyhidden" for="locationmap">locationmap</label>
						  <input class="data" type="radio" name="ResultDisp" value="ListAndMap"  nowrap="" id="locationmap"/>Locations and Map<br />
						  <label class="visuallyhidden" for="LocationOnly">LocationOnly</label>
						  <input class="data" type="radio" name="ResultDisp" value="LocationOnly" checked="checked" id="LocationOnly" />Locations Only
                      </xsl:when>
                      <xsl:otherwise>
						  <label class="visuallyhidden" for="locationmap">locationmap</label>
                        <input class="data" type="radio" name="ResultDisp" value="ListAndMap" checked="checked" nowrap="" id="locationmap"/>Locations and Map<br />
						  <label class="visuallyhidden" for="LocationOnly">LocationOnly</label>
						  <input class="data" type="radio" name="ResultDisp" value="LocationOnly" id="LocationOnly" />Locations Only
                      </xsl:otherwise>
                    </xsl:choose>
                  </td>
                </tr>
                <tr>
                  <td align="left" nowrap="" class="data">
                    <textarea class="data" name="errortext" rows ="3" cols="40" readonly="yes" style="border:0px; overflow:auto; color:#FF0000">
                      <xsl:if test="count(Error) &gt; 0 ">
                        <xsl:value-of select="Error/ErrorString"  disable-output-escaping="yes"/>
                      </xsl:if>
                    </textarea>
                  </td>
                  <td class="data" align="left" valign="middle" nowrap="" style="padding-left:30px">
                    <input class="default" type="reset" value="Clear"/>&#160;<input class="default" type="button" value="Search" id="SearchButton" onclick="load(this.form)"/>
                  </td>
                  <td align="left" class="data" width="100%">&#160;</td>
                </tr>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template name="MapData">
    <xsl:variable name="keyinstn" select="Company/KeyInstn"/>
    <xsl:variable name="formaction" select="concat('BranchLocatorMap.aspx?IID=', $keyinstn, '&amp;generateMap=1')"></xsl:variable>
    <div id="SearchForm">
      <table border="0" width="100%" align="left" cellpadding="0" cellspacing="0">

        <form name="branchsearch" id="branchsearch" method="POST">
			<fieldset>
				<legend>Branch Search</legend>
			
          <xsl:attribute name="action">
            <xsl:value-of select="$formaction"/>
          </xsl:attribute>
          <tr>
            <td class="colorlight" valign="top">

              <table border="0" width="100%" align="left" cellpadding="1" cellspacing="0" class="header">
                <tr>
                  <td class="header" align="left" valign="top">
                    <xsl:call-template name="FormTitle"/>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <!--<tr>
            <td class="colorlight" valign="top">&#160;</td>
          </tr>
          <tr>
            <td class="colorlight" valign="top">

              <table border="0" width="100%" align="left" cellpadding="1" cellspacing="0" class="header">
                <tr>
                  <td class="header" align="left" valign="top">
                    <xsl:call-template name="LocationRadius"/>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td class="colorlight" valign="top">&#160;</td>
          </tr>
          <tr>
            <td class="colorlight" valign="top">

              <table border="0" width="100%" align="left" cellpadding="1" cellspacing="0" class="header">
                <tr>
                  <td class="header" align="left" valign="top">
                    <xsl:call-template name="Address"/>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td class="colorlight" valign="top">&#160;</td>
          </tr>-->
          <tr>
            <td class="colorlight" valign="top" align="left">
              <xsl:variable name="ext" select="FormInfo/Extended"/>
              <xsl:variable name="disp" select="FormInfo/Display"/>

              <input type="hidden" name="extended" class="data">
                <xsl:attribute name="Value">
                  <xsl:choose>
                    <xsl:when test="contains($ext,'NoPref')">
                      <xsl:value-of select="$ext" disable-output-escaping="yes"/>
                    </xsl:when>
                    <xsl:when test="contains($ext,'Yes')">
                      <xsl:value-of select="$ext" disable-output-escaping="yes"/>
                    </xsl:when>
                    <xsl:when test="contains($ext,'No')">
                      <xsl:value-of select="$ext" disable-output-escaping="yes"/>
                    </xsl:when>
                    <xsl:otherwise>NoPref</xsl:otherwise>
                  </xsl:choose>
                </xsl:attribute>
              </input>

              <input type="hidden" name="ResultDisp" class="data">
                <xsl:attribute name="Value">
                  <xsl:choose>
                    <xsl:when test="contains($disp,'ListAndMap')">
                      <xsl:value-of select="$disp" disable-output-escaping="yes"/>
                    </xsl:when>
                    <xsl:when test="contains($disp,'LocationOnly')">
                      <xsl:value-of select="$disp" disable-output-escaping="yes"/>
                    </xsl:when>
                    <xsl:otherwise>ListAndMap</xsl:otherwise>
                  </xsl:choose>
                </xsl:attribute>
              </input>

              <!--<xsl:call-template name="ResultsChoices"/>-->
            </td>
          </tr>
          <tr>
            <td class="colorlight" valign="top">&#160;</td>
          </tr>
          <input type="hidden" name="HomeLat" id="HomeLat"></input>
          <input type="hidden" name="HomeLong" id="HomeLong"></input>
          <tr>
            <td class="colorlight" valign="top">
              <xsl:call-template name="DummyMap"/>
            </td>
          </tr>
</fieldset>
        </form>
      </table>
    </div>
  </xsl:template>

  <xsl:template name="MapResults">
    <script>
      homeaddy='<xsl:value-of select="FormInfo/HomeAddress"/>';
      homelat='<xsl:value-of select="HomePoint/latitude"/>';
      homelong='<xsl:value-of select="HomePoint/longitude"/>';
    </script>
    <xsl:variable name="outputtype" select="FormInfo/Display"/>

    <xsl:if test="contains($outputtype,'LocationOnly')">
      <xsl:call-template name="Map"/>
      <xsl:call-template name="ResultOptions"/>

      <table border="0" width="100%" align="left" cellpadding="1" cellspacing="0">
        <tr>
          <td>
            <xsl:call-template name="DrivingHomeAddress"/>
          </td>
        </tr>
        <tr>
          <td class="header" align="left" valign="top">
            <xsl:call-template name="ShowListResults"/>
          </td>
        </tr>
      </table>
    </xsl:if>

    <xsl:if test="contains($outputtype,'ListAndMap')">
      <xsl:call-template name="Map"/>
      <xsl:call-template name="ResultOptions"/>

      <table border="0" width="100%" align="left" cellpadding="1" cellspacing="0">
        <tr>
          <td>
            <xsl:call-template name="DrivingHomeAddress"/>
          </td>
        </tr>
        <tr>
          <td class="header" align="left" valign="top">
            <xsl:call-template name="ShowListResults"/>
          </td>
        </tr>
      </table>

    </xsl:if>
  </xsl:template>

  <xsl:template name="DrivingHomeAddress">
    <div id="DrivingHome" name="DrivingHome" style="display:none">

      <table border="0" width="100%" align="left" cellpadding="3" cellspacing="0" class="header">
        <tr>
          <th class="header" align="left" valign="top">Starting Address:</th>
        </tr>
        <tr>
          <td class="data" align="left" valign="top">
            <table border="0" width="50%" align="left" cellpadding="2" cellspacing="0">
              <tr valign="middle">
                <td align="right" class="data">Street:</td>
                <td align="left" colspan="3">
					<label class="visuallyhidden" for="Street">Street</label>
					<input class="data" type="text" name="Street" ID="Street" size="59">
                    <xsl:attribute name="value">
                      <xsl:value-of select="FormInfo/Street"/>
                    </xsl:attribute>
                  </input>
                </td>
              </tr>
              <tr valign="middle">
                <td align="right" class="data">*City:</td>
                <td class="data"  align="left">
					<label class="visuallyhidden" for="City">City</label>
                  <input class="data" type="text" name="City" ID="City">
                    <xsl:attribute name="VALUE">
                      <xsl:value-of select="FormInfo/City"/>
                    </xsl:attribute>
                  </input>
                </td>
                <td align="right" class="data" nowrap="">AND *State: </td>
                <td class="data"  align="left">
					<label class="visuallyhidden" for="State">State</label>
					<select class="data" name="State" ID="State">
						<xsl:call-template name="States" />
					</select>
                <!--<input class="data"  type="text" name="State" ID="State">
                    <xsl:attribute name="value">
                      <xsl:value-of select="FormInfo/State"/>
                    </xsl:attribute>
                  </input>-->
                </td>
              </tr>
              <tr valign="middle">
                <td align="right" class="data">OR</td>
                <td  align="left" colspan="3">&#160;</td>
              </tr>
              <tr valign="middle">
                <td align="right" class="data">*ZIP:</td>
                <td  align="left" colspan="3">
					<label class="visuallyhidden" for="ZIP">ZIP</label>
                  <input class="data" type="text" name="ZIP" id="ZIP">
                    <xsl:variable name='zip' select="FormInfo/Zip"/>
                    <xsl:attribute name="Value">
                      <xsl:choose>
                        <xsl:when test="contains($zip,'Enter')"></xsl:when>
                        <xsl:otherwise>
                          <xsl:value-of select="FormInfo/Zip"/>
                        </xsl:otherwise>
                      </xsl:choose>
                    </xsl:attribute>
                  </input>
                </td>
                <td>
                  <input class="datashade surr" type="button" value="Update" onclick="javascript:geoCodeDriving()"/>

                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </div>
  </xsl:template>

  <xsl:template name="ShowListResults">
    <div name="ListResultDiv" id="ListResultDiv">
      <table name="ListResults" id="ListResults" cellpadding="3" cellspacing="0" width="100%" class="header" border="0">
        <tr valign="top">
          <th align="left" class="header" nowrap="">#</th>
          <th width="10%" align="left" class="header">Type</th>
          <th align="left" class="header">Address</th>
          <th align="left" class="header">Hours</th>
          <th width="10%" align="left" class="header">Directions</th>
        </tr>
        <xsl:variable name="keyinstn" select="Company/KeyInstn"/>

        <xsl:for-each select="BranchesInRadius">
          <xsl:variable name="BankOrder" select="Order"/>
          <tr valign="top">
            <td valign="top" align="left" class="data" nowrap="">
              <xsl:value-of select="$BankOrder"/>
            </td>
            <td valign="top" align="left" class="data">
              <xsl:value-of select="bankfacilitytype"/>
              <!--<xsl:variable name="btype" select="keybankfacilitytype"/>
              <xsl:choose>
                <xsl:when test="$btype = 0">Branch</xsl:when>
                <xsl:when test="$btype = 1">ATM</xsl:when>
                <xsl:when test="$btype = 2">Loan Office</xsl:when>
              </xsl:choose>-->
            </td>
            <td valign="top" align="left" class="data">
              <span class="defaultbold">
                <xsl:choose>
                  <xsl:when test="count(bankfacility) = 0">
                    <xsl:variable name="bankKey" select="keyBranch"/>
                    <xsl:variable name="detailLink" select="concat(concat(concat('branchdetails.aspx?iid=',$keyinstn),'&amp;keybranch='),$bankKey)"/>
                    <a title="{BFNNoCode}">
                      <xsl:attribute name="href">
                        <xsl:value-of select="$detailLink"/>
                      </xsl:attribute>
                      <xsl:value-of select="BFNNoCode"/>
                    </a>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:variable name="bankKey" select="bankfacility"/>
                    <xsl:variable name="detailLink" select="concat(concat(concat('branchdetails.aspx?iid=',$keyinstn),'&amp;bankfacility='),$bankKey)"/>
                    <a title="{BFNNoCode}">
                      <xsl:variable name="detailZip" select="concat('&amp;Zip=',../FormInfo/Zip)" />
                      <xsl:variable name="detailStreet" select="concat('&amp;Street=',../FormInfo/Street)" />
                      <xsl:variable name="detailCity" select="concat('&amp;City=',../FormInfo/City)" />
                      <xsl:variable name="detailState" select="concat('&amp;State=',../FormInfo/State)" />
                      <xsl:attribute name="href">
                        <xsl:value-of select="$detailLink"/>
                        <xsl:value-of select="$detailZip"/>
                        <xsl:value-of select="$detailStreet"/>
                        <xsl:value-of select="$detailCity"/>
                        <xsl:value-of select="$detailState"/>
                      </xsl:attribute>
                      <xsl:value-of select="BFNNoCode"/>
                    </a>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
              <xsl:if test="BankFacilityName !=''">
                <br />
              </xsl:if>
              <xsl:if test="Description !=''">
                <xsl:value-of select="Description" disable-output-escaping="yes"/>
                <br />
              </xsl:if>

              <xsl:value-of select="StreetNoCd"/>
              <xsl:if test="Street2NoCd != ''"><br /></xsl:if>

              <xsl:value-of select="Street2NoCd"/><br />
              <xsl:value-of select="cityNoCd"/>,
              <xsl:value-of select="state"/>&#160;<xsl:value-of select="zip"/>
              <xsl:if test="phone != ''">
                <br />Phone: <xsl:value-of select="phone"/>
              </xsl:if>
              <xsl:if test="fax != ''">
                <br />Fax: <xsl:value-of select="fax"/>
              </xsl:if>
              <br /><br />
            </td>
            <td valign="top" align="left" class="data">
              <xsl:variable name="myString" select="bankfacilityhours"/>
              <xsl:variable name="myNewString">
                <xsl:call-template name="replaceString">
                  <xsl:with-param name="stringIn" select="string($myString)"/>
                  <xsl:with-param name="charsIn" select="'&lt;br />'"/>
                  <xsl:with-param name="charsOut" select="' '"/>
                </xsl:call-template>
              </xsl:variable>
              <xsl:variable name="finalString">
                <xsl:call-template name="replaceString_a">
                  <xsl:with-param name="stringIn" select="string($myNewString)"/>
                  <xsl:with-param name="charsIn" select="' '"/>
                  <xsl:with-param name="charsOut" select="' '"/>
                </xsl:call-template>
              </xsl:variable>
              <xsl:value-of select="$finalString" disable-output-escaping="yes" />&nbsp;
            </td>
            <td valign="top" align="left" class="data">
              <span name="milesAway" id="milesAway">
                <xsl:value-of select="Distance"/>
              </span> miles
              <br />
              <a title="Drive It!">
                <xsl:attribute name="href">
                  <xsl:value-of select="DrivingUrl"/>
                </xsl:attribute>
                Drive It!
              </a>
              <br />
              <a title="Map It!">
                <xsl:attribute name="href">
                  <xsl:value-of select="concat(concat('javascript:mapSingleBranch(',$BankOrder),')')"/>
                </xsl:attribute>
                Map It!
              </a>
            </td>
          </tr>



        </xsl:for-each>

      </table>
    </div>
      <table width="100%" class="data">
        <tr></tr>
        <tr id="paginationRow" style="display:block">
          <td align="right" class="data">
          <xsl:for-each select="Pagination">
              &nbsp;
              <xsl:choose>
                <xsl:when test="CurrentPage = 'true'">
                  <xsl:value-of select="PageLabel"/>
                </xsl:when>
                <xsl:otherwise>
                  <a title="{PageLabel}">
                    <xsl:attribute name="href">
                      <xsl:choose>
                        <xsl:when test="contains(../Company/URL, '&amp;Start=')">
                          <xsl:value-of select="substring-before(../Company/URL, '&amp;Start=')"/>&amp;Start=<xsl:value-of select="ResultStart"/>
                        </xsl:when>
                        <xsl:when test="contains(../Company/URL, '&amp;start=')">
                          <xsl:value-of select="substring-before(../Company/URL, '&amp;start=')"/>&amp;Start=<xsl:value-of select="ResultStart"/>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:value-of select="../Company/URL"/>&amp;Start=<xsl:value-of select="ResultStart"/>
                        </xsl:otherwise>
                      </xsl:choose>
                    </xsl:attribute>
                    <xsl:value-of select="PageLabel"/>
                  </a>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
          </td>
        </tr>
      </table>

  </xsl:template>

  <!-- Default Template Ends here. all templates below are ONLY if you are using the 'Style Template' section in the console. -->


<!--************************************************************-->
<!--*****************		ONE		************************-->
<!--************************************************************-->
<!-- Start Template ONE here-->
<!--Template for title and instructions of form -->
<xsl:template name="FormTitle2">
<table border="0" width="100%" align="left" cellpadding="3" cellspacing="0">
	<tr>
		<th class="datashade defaultbold surr" align="left" valign="top">
			<xsl:if test="count(Error) &gt; 0 ">
				<xsl:attribute name="colspan">2</xsl:attribute>
			</xsl:if>
			Find Branch Locations</th>
	</tr>

		<xsl:call-template name="LocationRadius2" />

	<tr>
        <xsl:if test="FormOptions/MiniForm = 'true'">
			<xsl:attribute name="style">
			display: none;
			</xsl:attribute>
        </xsl:if>
		<td class="data" align="left" valign="top" width="100%">An asterisk (*) indicates a required field.</td>
	</tr>
	<tr>
        <xsl:if test="FormOptions/MiniForm = 'true'">
          <xsl:attribute name="style">
            display: none;
          </xsl:attribute>
        </xsl:if>
		<td class="data" align="left" valign="top">Searches must contain a City + State or Zip Code</td>
	</tr>
	<xsl:if test="count(Error) &gt; 0 ">
	<tr>
		<td rowspan="2" class="data" align="right" valign="top">
		<textarea name="errortext" rows ="2" cols="40" readonly="yes" style="border:0px; overflow:auto;">
		  <xsl:if test="count(Error) &gt; 0 ">
			<xsl:attribute name="style">border:1px; overflow:auto; color:#FF0000</xsl:attribute>
			<xsl:attribute name="class">datashade surr</xsl:attribute>
			<xsl:value-of select="Error/ErrorString"  disable-output-escaping="yes"/>
		  </xsl:if>
		</textarea>
		</td>
	</tr>
	</xsl:if>

		<xsl:call-template name="Address2" />

	<tr>
		<td class="data" align="left" valign="top">
		  <br />
		  <input class="default" type="reset" value="Clear"/>&#160;<input class="default" type="button" value="Search" id="SearchButton" onclick="load(this.form)"/>
		</td>
	</tr>
</table>
</xsl:template>
<!--Template for location type choices and radius -->
<xsl:template name="LocationRadius2">
	<xsl:variable name='type' select="FormInfo/Type"/>
	<!--<table border="0" width="100%" align="left" cellpadding="3" cellspacing="0">
		<tr>
			<th align="left" valign="top" class="datashade defaultbold surrleft" width="300px"> Select the type(s) of locations to search. </th>
			<th align="left" valign="top" class="datashade defaultbold surrright" nowrap=""> Select a search radius </th>
		</tr>-->
		<tr>
			<td align="left" class="data">
				<xsl:for-each select="FacilityType">
					<label class="visuallyhidden" for="FacilityType">FacilityType</label>
					<input class="data" type="checkbox" name="FacilityType" id="FacilityType" onclick="unshowAll();">
						<xsl:attribute name="value">
							<xsl:value-of select="KeyBankFacilityType"/>
						</xsl:attribute>
						<xsl:if test="contains($type, KeyBankFacilityType)">
							<xsl:attribute name="checked">
								true
							</xsl:attribute>
						</xsl:if>
						<xsl:if test="string-length($type) = 0 and KeyBankFacilityType = '0'">
							<xsl:attribute name="checked">
							true
							</xsl:attribute>
						</xsl:if>
					</input>
					<xsl:value-of select="BankFacilityType"/>&nbsp;&nbsp;
				</xsl:for-each>
				<label class="visuallyhidden" for="selectAll">selectAll</label>
				<input class="data" type="checkbox" name="selectAll" id="selectAll" value="1" onclick="showAll();" />Show All
			<br />
			<br />
			</td>
			<input type="hidden" name="Radius" value="0" />
		</tr>
		<!--We want to leave this commented out for reference in case a client asks for radius functionality-->
		<!--<tr>
			<td align="left" class="data">
				<xsl:choose>
					<xsl:when test="contains($type,'0')">
						<input class="data" type="checkbox" id="Branch" name="Branch" value="Branch" checked="true" />
						Branches
					</xsl:when>
					<xsl:when test="not($type)">
						<input class="data" type="checkbox" id="Branch" name="Branch" value="Branch" checked="true" />
						Branches
					</xsl:when>
					<xsl:otherwise>
						<input class="data" type="checkbox" id="Branch" name="Branch" value="Branch" />
						Branches
					</xsl:otherwise>
				</xsl:choose>
			</td>
			<td class="divider_left data" rowspan="4" align="left" valign="top"><xsl:variable name="radius" select="FormInfo/Radius"/>
				<select name="Radius" class="datashade">
					<xsl:choose>
						<xsl:when test="not($radius)">
							<option class="datashade" value="5" Selected="selected">5 miles</option>
						</xsl:when>
						<xsl:when test="contains($radius,'5')">
							<option class="datashade" value="5" Selected="selected">5 miles</option>
						</xsl:when>
						<xsl:otherwise>
							<option class="datashade" value="5">5 miles</option>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:choose>
						<xsl:when test="contains($radius,'10')">
							<option class="datashade" value="10" Selected="selected">10 miles</option>
						</xsl:when>
						<xsl:otherwise>
							<option class="datashade" value="10">10 miles</option>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:choose>
						<xsl:when test="contains($radius,'20')">
							<option class="datashade" value="20" Selected="selected">20 miles</option>
						</xsl:when>
						<xsl:otherwise>
							<option class="datashade" value="20">20 miles</option>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:choose>
						<xsl:when test="contains($radius,'50')">
							<option class="datashade" value="50" Selected="selected">50 miles</option>
						</xsl:when>
						<xsl:otherwise>
							<option class="datashade" value="50">50 miles</option>
						</xsl:otherwise>
					</xsl:choose>
				</select>
			</td>
		</tr>
		<tr>
			<td align="left" class="data"><xsl:choose>
					<xsl:when test="contains($type,'1')">
						<input class="data" type="checkbox" id="ATM" name="ATM" value="ATM" checked="true"/>
						ATMs
					</xsl:when>
					<xsl:otherwise>
						<input class="data" type="checkbox" id="ATM" name="ATM" value="ATM" />
						ATMs
					</xsl:otherwise>
				</xsl:choose>
			</td>
		</tr>
		<tr>
			<td align="left" class="data"><xsl:choose>
					<xsl:when test="contains($type,'2')">
						<input class="data" type="checkbox" id="LoanOffices" name="LoanOffices" value="LoanOffices" checked="true"/>
						Loan Offices
					</xsl:when>
					<xsl:otherwise>
						<input class="data" type="checkbox" id="LoanOffices" name="LoanOffices" value="LoanOffices" />
						Loan Offices
					</xsl:otherwise>
				</xsl:choose>
			</td>
		</tr>
	</table>-->
</xsl:template>
<!--Template for address entry part of form -->
<xsl:template name="Address2">
	<!--<table border="0" width="100%" align="left" cellpadding="3" cellspacing="0">
		<tr>
			<th class="datashade defaultbold surr" align="left" valign="top">Address:</th>
		</tr>-->
		<tr>
			<td class="data" align="left" valign="top"><table border="0" width="50%" align="left" cellpadding="2" cellspacing="0">
					<tr valign="middle">
						<xsl:if test="FormOptions/MiniForm = 'true'">
						<xsl:attribute name="style">
						display: none;
						</xsl:attribute>
						</xsl:if>
						<td align="right" class="data">Street:</td>
						<td align="left" colspan="3">
							<label class="visuallyhidden" for="Street">Street</label>
							<input class="data surr" type="text" name="Street" ID="Street" size="59" >
								<xsl:attribute name="Value">
									<xsl:value-of select="FormInfo/Street"/>
								</xsl:attribute>
							</input>
						</td>
					</tr>
					<tr valign="middle">
			            <xsl:if test="FormOptions/MiniForm = 'true'">
			              <xsl:attribute name="style">
			                display: none;
			              </xsl:attribute>
			            </xsl:if>
						<td align="right" class="data">*City:</td>
						<td class="data"  align="left">
							<label class="visuallyhidden" for="City">City</label>
							<input class="data surr" type="text" name="City" ID="City">
								<xsl:attribute name="Value">
									<xsl:value-of select="FormInfo/City"/>
								</xsl:attribute>
							</input>
						</td>
						<td align="right" class="data" nowrap="">&#160;AND *State:</td>
						<td class="data"  align="left">
							<label class="visuallyhidden" for="State">State</label>
							<select class="datashade surr" name="State" ID="State">
								<xsl:call-template name="States" />
							</select>
						</td>
					</tr>
					<tr valign="middle">
			            <xsl:if test="FormOptions/MiniForm = 'true'">
			              <xsl:attribute name="style">
			                display: none;
			              </xsl:attribute>
			            </xsl:if>
						<td align="right" class="data">AND/OR</td>
						<td  align="left" colspan="3">&#160;</td>
					</tr>
					<tr valign="middle">
			            <xsl:choose>
			              <xsl:when test="FormOptions/MiniForm = 'true'">
			                <td align="left" class="data">Zip Code:</td>
			              </xsl:when>
			              <xsl:otherwise>
			                <td align="right" class="data">*ZIP:</td>
			              </xsl:otherwise>
			            </xsl:choose>
						<td  align="left" colspan="3">
							<label class="visuallyhidden" for="ZIP">ZIP</label>
							<input type="text" name="ZIP" id="ZIP">
								<xsl:variable name='zip' select="FormInfo/Zip"/>
								<xsl:attribute name="Value">
								  <xsl:choose>
									<xsl:when test="contains($zip,'Enter')"></xsl:when>
									<xsl:otherwise>
									  <xsl:value-of select="FormInfo/Zip"/>
									</xsl:otherwise>
								  </xsl:choose>
								</xsl:attribute>
							</input>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	<!--</table>-->
</xsl:template>
<!--Template for last part of form with hours preferences and display options -->
<xsl:template name="ResultsChoices2">
	<table border="0" width="100%" align="left" cellpadding="2" cellspacing="0">
		<tr>
			<td class="defaultbold" align="left" valign="middle">To receive more accurate results, you may refine your search by selecting from the criteria below.</td>
		</tr>
		<tr>
			<td class="data" align="left" valign="top"><table border="0" width="100%" align="left" cellpadding="3" cellspacing="0">
					<tr valign="top">
						<th align="left" class="datashade defaultbold surrleft" valign="middle">Extended Hours</th>
						<th colspan="2" align="left" class="datashade defaultbold surrright" valign="middle">Results Display</th>
					</tr>
					<tr valign="top">
						<td align="left" class="data">
							<label class="visuallyhidden" for="extendend">extendend</label>
						<select name="extended" id="extendend">
								<xsl:variable name="ext" select="FormInfo/Extended"/>
								<xsl:choose>
									<xsl:when test="contains($ext,'NoPref')">
										<option value="NoPref" SELECTED="selected">No Preference</option>
										<option value="Yes">Yes</option>
										<option value="No">No</option>
									</xsl:when>
									<xsl:when test="contains($ext,'Yes')">
										<option value="NoPref" >No Preference</option>
										<option value="Yes" SELECTED="selected">Yes</option>
										<option value="No">No</option>
									</xsl:when>
									<xsl:when test="contains($ext,'No')">
										<option value="NoPref" >No Preference</option>
										<option value="Yes" >Yes</option>
										<option value="No" SELECTED="selected">No</option>
									</xsl:when>
									<xsl:otherwise>
										<option value="NoPref" SELECTED="selected">No Preference</option>
										<option value="Yes">Yes</option>
										<option value="No">No</option>
									</xsl:otherwise>
								</xsl:choose>
							</select>
						</td>
						<td colspan="2" align="left" class="data divider_left">
							<xsl:variable name="disp" select="FormInfo/Display"/>
							<xsl:choose>
								<xsl:when test="contains($disp,'ListAndMap')">
									<label class="visuallyhidden" for="locationmap">locationmap</label>
									<input class="data" type="radio" name="ResultDisp" value="ListAndMap" checked="checked" nowrap="" id="locationmap"/>Locations and Map<br />
									<label class="visuallyhidden" for="LocationOnly">LocationOnly</label>
									<input class="data" type="radio" name="ResultDisp" value="LocationOnly" id="LocationOnly" />Locations Only
								</xsl:when>
								<xsl:when test="contains($disp,'LocationOnly')">
									<label class="visuallyhidden" for="locationmap">locationmap</label>
									<input class="data" type="radio" name="ResultDisp" value="ListAndMap"  nowrap=""  id="locationmap"/>Locations and Map<br />
									<label class="visuallyhidden" for="LocationOnly">LocationOnly</label>
									<input class="data" type="radio" name="ResultDisp" value="LocationOnly" checked="checked" id="LocationOnly" />Locations Only
								</xsl:when>
								<xsl:otherwise>
									<label class="visuallyhidden" for="locationmap">locationmap</label>
									<input class="data" type="radio" name="ResultDisp" value="ListAndMap" checked="checked" nowrap=""  id="locationmap"/>Locations and Map<br />
									<label class="visuallyhidden" for="LocationOnly">LocationOnly</label>
									<input class="data" type="radio" name="ResultDisp" value="LocationOnly" id="LocationOnly"/>Locations Only
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</tr>
					<tr valign="top">
						<td align="left" nowrap="" class="data">
							<textarea name="errortext" rows ="2" cols="40" readonly="yes" style="border:0px; overflow:auto;">
                  <xsl:if test="count(Error) &gt; 0 ">
                    <xsl:attribute name="style">border:1px; overflow:auto; color:#FF0000</xsl:attribute>
                    <xsl:attribute name="class">datashade surr</xsl:attribute>
                    <xsl:value-of select="Error/ErrorString"  disable-output-escaping="yes"/>
                  </xsl:if>
                </textarea>
						</td>
						<td class="data divider_left" align="left" valign="middle" nowrap="" style="padding-left:30px">
							<input type="reset" value="Clear"/>
							&#160;
							<input type="button" value="Search" onClick="load(this.form)"/>
						</td>
						<td align="left" class="data" width="100%">&#160;</td>
					</tr>
				</table></td>
		</tr>
	</table>
</xsl:template>
<xsl:template name="MapData2">
	<xsl:variable name="keyinstn" select="Company/KeyInstn"/>
	<xsl:variable name="formaction" select="concat('BranchLocatorMap.aspx?IID=', $keyinstn, '&amp;generateMap=1')"></xsl:variable>
	<div id="SearchForm">
		<table border="0" width="100%" align="left" cellpadding="0" cellspacing="0">
			<form name="branchsearch" id="branchsearch" method="POST">
				<fieldset>
					<legend>Branch Search</legend>
					<xsl:attribute name="action">
						<xsl:value-of select="$formaction"/>
					</xsl:attribute>
					<tr>
						<td class="data" valign="top">
							<xsl:call-template name="FormTitle2"/>
						</td>
					</tr>
					<!--<tr>
					<td class="data" valign="top">&#160;</td>
				</tr>
				<tr>
					<td class="data" valign="top"><xsl:call-template name="LocationRadius2"/>

					</td>
				</tr>
				<tr>
					<td class="data" valign="top">&#160;</td>
				</tr>
				<tr>
					<td class="data" valign="top"><xsl:call-template name="Address2"/>

					</td>
				</tr>
				<tr>
					<td class="data" valign="top">&#160;</td>
				</tr>-->
					<tr>
						<td class="data" valign="top">
							<xsl:variable name="ext" select="FormInfo/Extended"/>
							<xsl:variable name="disp" select="FormInfo/Display"/>
							<input type="hidden" name="extended" class="datashade surr">
								<xsl:attribute name="Value">
									<xsl:choose>
										<xsl:when test="contains($ext,'NoPref')">
											<xsl:value-of select="$ext" disable-output-escaping="yes"/>
										</xsl:when>
										<xsl:when test="contains($ext,'Yes')">
											<xsl:value-of select="$ext" disable-output-escaping="yes"/>
										</xsl:when>
										<xsl:when test="contains($ext,'No')">
											<xsl:value-of select="$ext" disable-output-escaping="yes"/>
										</xsl:when>
										<xsl:otherwise>NoPref</xsl:otherwise>
									</xsl:choose>
								</xsl:attribute>
							</input>
							<input type="hidden" name="ResultDisp" class="datashade surr">
								<xsl:attribute name="Value">
									<xsl:choose>
										<xsl:when test="contains($disp,'ListAndMap')">
											<xsl:value-of select="$disp" disable-output-escaping="yes"/>
										</xsl:when>
										<xsl:when test="contains($disp,'LocationOnly')">
											<xsl:value-of select="$disp" disable-output-escaping="yes"/>
										</xsl:when>
										<xsl:otherwise>ListAndMap</xsl:otherwise>
									</xsl:choose>
								</xsl:attribute>
							</input>
							<!--<input class="datashade surr" type="reset" value="Clear"/>
						&#160;
						<input class="datashade surr" type="button" value="Search" onClick="load(this.form)"/>
						<xsl:call-template name="ResultsChoices2"/>-->
						</td>
					</tr>
					<tr>
						<td class="data" valign="top">&#160;</td>
					</tr>
					<input type="hidden" name="HomeLat" id="HomeLat"></input>
					<input type="hidden" name="HomeLong" id="HomeLong"></input>
					<tr>
						<td class="data" valign="top">
							<xsl:call-template name="DummyMap"/>
						</td>
					</tr>
				</fieldset>
			</form>
		</table>
	</div>
</xsl:template>
<xsl:template name="MapResults2">
	<script>
      homeaddy='<xsl:value-of select="FormInfo/HomeAddress"/>';
      homelat='<xsl:value-of select="HomePoint/latitude"/>';
      homelong='<xsl:value-of select="HomePoint/longitude"/>';
    </script>
	<xsl:variable name="outputtype" select="FormInfo/Display"/>
	<xsl:if test="contains($outputtype,'LocationOnly')">
		<xsl:call-template name="Map"/>

		<xsl:call-template name="ResultOptions"/>

		<xsl:call-template name="DrivingHomeAddress"/>

		<xsl:call-template name="ShowListResults2"/>

	</xsl:if>
	<xsl:if test="contains($outputtype,'ListAndMap')">
		<xsl:call-template name="Map"/>

		<xsl:call-template name="ResultOptions"/>

		<xsl:call-template name="DrivingHomeAddress"/>

		<xsl:call-template name="ShowListResults2"/>

	</xsl:if>
</xsl:template>
<xsl:template name="ShowListResults2">
	<div name="ListResultDiv" id="ListResultDiv">
		<table name="ListResults" id="ListResults" cellpadding="3" cellspacing="0" width="100%" class="topbord" border="0">
			<tr valign="top">
				<th  nowrap="" align="left" class="datashade defaultbold leftBOTbord">#</th>
				<th width="10%" align="left" class="datashade defaultbold botbord">Type</th>
				<th align="left" class="datashade defaultbold botbord">Address</th>
				<th align="left" class="datashade defaultbold botbord">Hours</th>
				<th width="10%" align="left" class="datashade defaultbold rightBOTbord">Directions</th>
			</tr>
			<xsl:variable name="keyinstn" select="Company/KeyInstn"/>
			<xsl:for-each select="BranchesInRadius">
				<xsl:variable name="BankOrder" select="Order"/>
				<tr valign="top">
					<td valign="top" align="left" class="data botbord" nowrap="">
						<xsl:value-of select="$BankOrder"/>
					</td>
					<td valign="top" align="left" class="data botbord">
						<xsl:value-of select="bankfacilitytype"/>
						<!--<xsl:variable name="btype" select="keybankfacilitytype"/>
						<xsl:choose>
							<xsl:when test="$btype = 0">Branch</xsl:when>
							<xsl:when test="$btype = 1">ATM</xsl:when>
							<xsl:when test="$btype = 2">Loan Office</xsl:when>
						</xsl:choose>-->
					</td>
					<td valign="top" align="left" class="data botbord"><span class="defaultbold">
						<xsl:choose>
							<xsl:when test="count(bankfacility) = 0">
								<xsl:variable name="bankKey" select="keyBranch"/>
								<xsl:variable name="detailLink" select="concat(concat(concat('branchdetails.aspx?iid=',$keyinstn),'&amp;keybranch='),$bankKey)"/>
								<a title="{BFNNoCode}">
								<xsl:attribute name="href">
									<xsl:value-of select="$detailLink"/>
								</xsl:attribute>
								<!--<xsl:value-of select="BankFacilityName"/>-->
								<xsl:value-of select="BFNNoCode"/>
								</a>
							</xsl:when>
							<xsl:otherwise>
								<xsl:variable name="bankKey" select="bankfacility"/>
								<xsl:variable name="detailLink" select="concat(concat(concat('branchdetails.aspx?iid=',$keyinstn),'&amp;bankfacility='),$bankKey)"/>
								<!--<a>
								<xsl:attribute name="href">
									<xsl:value-of select="$detailLink"/>
								</xsl:attribute>
								<xsl:value-of select="BankFacilityName"/>
								</a>-->
								<a title="{BFNNoCode}">
			                      <xsl:variable name="detailZip" select="concat('&amp;Zip=',../FormInfo/Zip)" />
			                      <xsl:variable name="detailStreet" select="concat('&amp;Street=',../FormInfo/Street)" />
			                      <xsl:variable name="detailCity" select="concat('&amp;City=',../FormInfo/City)" />
			                      <xsl:variable name="detailState" select="concat('&amp;State=',../FormInfo/State)" />
			                      <xsl:attribute name="href">
			                        <xsl:value-of select="$detailLink"/>
			                        <xsl:value-of select="$detailZip"/>
			                        <xsl:value-of select="$detailStreet"/>
			                        <xsl:value-of select="$detailCity"/>
			                        <xsl:value-of select="$detailState"/>
			                      </xsl:attribute>
			                      <xsl:value-of select="BFNNoCode"/>
			                    </a>
							</xsl:otherwise>
						</xsl:choose>
						</span>
						<xsl:if test="BankFacilityName !=''">
							<br />
						</xsl:if>
						<xsl:if test="Description !=''">
							<xsl:value-of select="Description" disable-output-escaping="yes"/> <br />
						</xsl:if>
						<xsl:value-of select="StreetNoCd"/>
						<xsl:if test="Street2NoCd != ''">
							<br />
						</xsl:if>
						<xsl:value-of select="Street2NoCd"/><br />
						<xsl:value-of select="cityNoCd"/>,
						<xsl:value-of select="state"/>&#160;<xsl:value-of select="zip"/>
						<xsl:if test="phone != ''">
							<br />Phone: <xsl:value-of select="phone"/>
						</xsl:if>
						<xsl:if test="fax != ''">
							<br />Fax: <xsl:value-of select="fax"/>
						</xsl:if>
						<br /><br />
					</td>
					<td valign="top" align="left" class="data botbord">
						<xsl:variable name="myString" select="bankfacilityhours"/>
						<xsl:variable name="myNewString">
						<xsl:call-template name="replaceString">
							<xsl:with-param name="stringIn" select="string($myString)"/>
							<xsl:with-param name="charsIn" select="'&lt;br />'"/>
							<xsl:with-param name="charsOut" select="' '"/>
						</xsl:call-template>
						</xsl:variable>
						<xsl:variable name="finalString">
						<xsl:call-template name="replaceString_a">
							<xsl:with-param name="stringIn" select="string($myNewString)"/>
							<xsl:with-param name="charsIn" select="' '"/>
							<xsl:with-param name="charsOut" select="' '"/>
						</xsl:call-template>
						</xsl:variable>
						<xsl:value-of select="$finalString" disable-output-escaping="yes" />&nbsp;
					</td>
					<td valign="top" align="left" class="data botbord">
						<!--<xsl:value-of select="Distance"/> miles <br />
						<a>
						<xsl:attribute name="href"> <xsl:value-of select="DrivingUrl"/> </xsl:attribute>
						Drive It! </a> <br />
						<a>
						<xsl:attribute name="href"> <xsl:value-of select="concat(concat('javascript:mapSingleBranch(',$BankOrder),')')"/> </xsl:attribute>
						Map It! </a>-->
						<span name="milesAway" id="milesAway">
							<xsl:value-of select="Distance"/>
						</span> miles
						<br />
						<a title="Drive It!" >
							<xsl:attribute name="href">
								<xsl:value-of select="DrivingUrl"/>
							</xsl:attribute>
						Drive It!
						</a>
						<br />
						<a title="Map It!">
							<xsl:attribute name="href">
								<xsl:value-of select="concat(concat('javascript:mapSingleBranch(',$BankOrder),')')"/>
							</xsl:attribute>
						Map It!
						</a>
					</td>
				</tr>
			</xsl:for-each>
		</table>
	</div>
	<table width="100%" class="data">
		<tr></tr>
		<tr id="paginationRow" style="display:block">
			<td align="right" class="data">
				<xsl:for-each select="Pagination">
					&nbsp;
					<xsl:choose>
						<xsl:when test="CurrentPage = 'true'">
							<xsl:value-of select="PageLabel"/>
						</xsl:when>
						<xsl:otherwise>
							<a title="{PageLabel}">
							<xsl:attribute name="href">
							<xsl:choose>
								<xsl:when test="contains(../Company/URL, '&amp;Start=')">
									<xsl:value-of select="substring-before(../Company/URL, '&amp;Start=')"/>&amp;Start=<xsl:value-of select="ResultStart"/>
								</xsl:when>
								<xsl:when test="contains(../Company/URL, '&amp;start=')">
									<xsl:value-of select="substring-before(../Company/URL, '&amp;start=')"/>&amp;Start=<xsl:value-of select="ResultStart"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="../Company/URL"/>&amp;Start=<xsl:value-of select="ResultStart"/>
								</xsl:otherwise>
							</xsl:choose>
							</xsl:attribute>
							<xsl:value-of select="PageLabel"/> </a>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</td>
		</tr>
	</table>
</xsl:template>
<!-- ONE Template Ends here.-->


<!--************************************************************-->
<!--*****************		Three		************************-->
<!--************************************************************-->
<!-- Start Template THREE here-->
 <!--Template for title and instructions of form -->
  <xsl:template name="FormTitle3">
    <table border="0" width="100%" align="left" cellpadding="3" cellspacing="0" class="table2">
      <tr>
        <th class="datashade defaultbold table1_item" align="left" valign="top">
          <xsl:if test="count(Error) &gt; 0 ">
            <xsl:attribute name="colspan">2</xsl:attribute>
          </xsl:if>Find a Location
        </th>
      </tr>
      <xsl:call-template name="LocationRadius3" />

		<tr>
		<xsl:if test="FormOptions/MiniForm = 'true'">
			<xsl:attribute name="style">
				display: none;
			</xsl:attribute>
			</xsl:if>
        <td class="data" align="left" valign="top" width="100%">An asterisk (*) indicates a required field.</td>
        <xsl:if test="count(Error) &gt; 0 ">
          <td rowspan="2" class="data" align="right" valign="top">
            <textarea  name="errortext" rows ="2" cols="40" readonly="yes" style="border:0px; overflow:auto;">
              <xsl:if test="count(Error) &gt; 0 ">
                <xsl:attribute name="style">border:1px; overflow:auto; color:#FF0000</xsl:attribute>
                <xsl:attribute name="class">datashade table2</xsl:attribute>
                <xsl:value-of select="Error/ErrorString"  disable-output-escaping="yes"/>
              </xsl:if>
            </textarea>
          </td>
        </xsl:if>
      </tr>
      <tr>
      <xsl:if test="FormOptions/MiniForm = 'true'">
						<xsl:attribute name="style">
							display: none;
						</xsl:attribute>
			</xsl:if>
        <td class="data" align="left" valign="top">Searches must contain a City + State or Zip Code</td>
      </tr>
      <xsl:call-template name="Address3" />
			<tr>
				<td class="data" align="left" valign="top">
					<br />
					<input class="default" type="reset" value="Clear"/>&#160;<input class="default" type="button" value="Search" id="SearchButton" onclick="load(this.form)"/>
				</td>
			</tr>

    </table>
  </xsl:template>

  <!--Template for location type choices and radius -->
  <xsl:template name="LocationRadius3">
    <xsl:variable name='type' select="FormInfo/Type"/>

      <tr>
        <td align="left" class="data">
         	<xsl:for-each select="FacilityType">
				<label class="visuallyhidden" for="FacilityType">FacilityType</label>
					<input class="data" type="checkbox" name="FacilityType" id="FacilityType" onClick="unshowAll();">
					<xsl:attribute name="value"> <xsl:value-of select="KeyBankFacilityType"/> </xsl:attribute>
					<xsl:if test="contains($type, KeyBankFacilityType)">
						<xsl:attribute name="checked"> true </xsl:attribute>
					</xsl:if>
					<xsl:if test="string-length($type) = 0 and KeyBankFacilityType = '0'">
						<xsl:attribute name="checked"> true </xsl:attribute>
					</xsl:if>
					</input>
					<xsl:value-of select="BankFacilityType"/>&nbsp;&nbsp;
				</xsl:for-each>
			<label class="visuallyhidden" for="selectAll">selectAll</label>
				<input class="data" type="checkbox" name="selectAll" id="selectAll" value="1" onClick="showAll();" />
				Show All <br />
				<br />
        </td>
       <input type="hidden" name="Radius" value="0" />
      </tr>


  </xsl:template>

<!--Template for address entry part of form -->
<xsl:template name="Address3">

	<tr>
		<td class="data" align="left" valign="top">
			<table border="0" width="50%" align="left" cellpadding="2" cellspacing="0" class="data">
				<tr valign="middle">
					<xsl:if test="FormOptions/MiniForm = 'true'">
					  <xsl:attribute name="style">
						 display: none;
					  </xsl:attribute>
					</xsl:if>
					<td align="right" class="data">Street:</td>
					<td align="left" colspan="3">
						<label class="visuallyhidden" for="Street">Street</label>
						<input type="text" name="Street" ID="Street" size="59">
						<xsl:attribute name="Value"> <xsl:value-of select="FormInfo/Street"/> </xsl:attribute>
						</input>
					</td>
				</tr>
				<tr valign="middle">
					<xsl:if test="FormOptions/MiniForm = 'true'">
					  <xsl:attribute name="style">
						 display: none;
					  </xsl:attribute>
					</xsl:if>
					<td align="right" class="data">*City:</td>
					<td class="data"  align="left">
						<label class="visuallyhidden" for="City">City</label>
						<input type="text" name="City" ID="City">
						<xsl:attribute name="Value"> <xsl:value-of select="FormInfo/City"/> </xsl:attribute>
						</input>
					</td>
					<td align="right" class="data" nowrap="">&#160;AND *State: </td>
					<td class="data"  align="left">
						<label class="visuallyhidden" for="State">State</label>
						<select name="State" ID="State">
							<xsl:call-template name="States" />
						</select>
					</td>
				</tr>
				<tr valign="middle">
					<xsl:if test="FormOptions/MiniForm = 'true'">
					  <xsl:attribute name="style">
						 display: none;
					  </xsl:attribute>
					</xsl:if>
					<td align="right" class="data">AND/OR</td>
					<td  align="left" colspan="3">&#160;</td>
				</tr>
				<tr valign="middle">
					<xsl:choose>
					  <xsl:when test="FormOptions/MiniForm = 'true'">
						 <td align="left" class="data">Zip Code:</td>
					  </xsl:when>
					  <xsl:otherwise>
						 <td align="right" class="data">*ZIP:</td>
					  </xsl:otherwise>
					</xsl:choose>
					<td  align="left" colspan="3">
						<label class="visuallyhidden" for="ZIP">ZIP</label>
					  <input type="text" name="ZIP" id="ZIP">
						 <xsl:variable name='zip' select="FormInfo/Zip"/>
						 <xsl:attribute name="Value">
							<xsl:choose>
							  <xsl:when test="contains($zip,'Enter')"></xsl:when>
							  <xsl:otherwise>
								 <xsl:value-of select="FormInfo/Zip"/>
							  </xsl:otherwise>
							</xsl:choose>
						 </xsl:attribute>
					  </input>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</xsl:template>

<!--Template for last part of form with hours preferences and display options -->
<xsl:template name="ResultsChoices3">
	<table border="0" width="100%" align="left" cellpadding="2" cellspacing="0">
		<tr>
			<td class="defaultbold" align="left" valign="middle">To receive more accurate results, you may refine your search by selecting from the criteria below.</td>
		</tr>
		<tr>
			<td class="data" align="left" valign="top"><table border="0" width="100%" align="left" cellpadding="3" cellspacing="0" class="table2">
					<tr valign="top">
						<th align="left" class="datashade defaultbold table1_item" valign="middle">Extended Hours</th>
						<th colspan="2" align="left" class="datashade defaultbold table1_item divider_left" valign="middle">Results Display</th>
					</tr>
					<tr valign="top">
						<td align="left" class="data">
							<label class="visuallyhidden" for="extendend">extendend</label>
							<select name="extended" class="datashade table2" id="extendend">
								<xsl:variable name="ext" select="FormInfo/Extended"/>
								<xsl:choose>
									<xsl:when test="contains($ext,'NoPref')">
										<option value="NoPref" SELECTED="selected">No Preference</option>
										<option value="Yes">Yes</option>
										<option value="No">No</option>
									</xsl:when>
									<xsl:when test="contains($ext,'Yes')">
										<option value="NoPref" >No Preference</option>
										<option value="Yes" SELECTED="selected">Yes</option>
										<option value="No">No</option>
									</xsl:when>
									<xsl:when test="contains($ext,'No')">
										<option value="NoPref" >No Preference</option>
										<option value="Yes" >Yes</option>
										<option value="No" SELECTED="selected">No</option>
									</xsl:when>
									<xsl:otherwise>
										<option value="NoPref" SELECTED="selected">No Preference</option>
										<option value="Yes">Yes</option>
										<option value="No">No</option>
									</xsl:otherwise>
								</xsl:choose>
							</select>
						</td>
						<td colspan="2" align="left" class="data divider_left"><xsl:variable name="disp" select="FormInfo/Display"/>
							<xsl:choose>
								<xsl:when test="contains($disp,'ListAndMap')">
									<label class="visuallyhidden" for="locationmap">locationmap</label>
									<input type="radio" name="ResultDisp" value="ListAndMap" checked="checked" nowrap="" id="locationmap"/>
									Locations and Map<br />
									<label class="visuallyhidden" for="LocationOnly">LocationOnly</label>
									<input type="radio" name="ResultDisp" value="LocationOnly" id="LocationOnly" />
									Locations Only
								</xsl:when>
								<xsl:when test="contains($disp,'LocationOnly')">
									<label class="visuallyhidden" for="locationmap">locationmap</label>
									<input type="radio" name="ResultDisp" value="ListAndMap"  nowrap="" id="locationmap"/>
									Locations and Map<br />
									<label class="visuallyhidden" for="LocationOnly">LocationOnly</label>
									<input type="radio" name="ResultDisp" value="LocationOnly" checked="checked"  id="LocationOnly"/>
									Locations Only
								</xsl:when>
								<xsl:otherwise>
									<label class="visuallyhidden" for="locationmap">locationmap</label>
									<input type="radio" name="ResultDisp" value="ListAndMap" checked="checked" nowrap="" id="locationmap"/>
									Locations and Map<br />
									<label class="visuallyhidden" for="LocationOnly">LocationOnly</label>
									<input type="radio" name="ResultDisp" value="LocationOnly" id="LocationOnly" />
									Locations Only
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</tr>
					<tr valign="top">
						<td align="left" nowrap="" class="data"><textarea class="data" name="errortext" rows ="3" cols="40" readonly="yes" style="border:0px; overflow:auto; color:#FF0000">
		                      <xsl:if test="count(Error) &gt; 0 ">
		                        <xsl:value-of select="Error/ErrorString"  disable-output-escaping="yes"/>
		                      </xsl:if>
		                    </textarea>
						</td>
						<td class="data divider_left" align="left" valign="middle" nowrap="" style="padding-left:30px"><input type="reset" value="Clear"/>
							&#160;
							<input type="button" value="Search" onClick="load(this.form)"/>
						</td>
						<td align="left" class="data" width="100%">&#160;</td>
					</tr>
				</table></td>
		</tr>
	</table>
</xsl:template>
<xsl:template name="MapResults3">
	<script>
      homeaddy='<xsl:value-of select="FormInfo/HomeAddress"/>';
      homelat='<xsl:value-of select="HomePoint/latitude"/>';
      homelong='<xsl:value-of select="HomePoint/longitude"/>';
    </script>
	<xsl:variable name="outputtype" select="FormInfo/Display"/>
	<xsl:if test="contains($outputtype,'LocationOnly')">
		<xsl:call-template name="Map"/>

		<xsl:call-template name="ResultOptions"/>

		<xsl:call-template name="DrivingHomeAddress"/>

		<xsl:call-template name="ShowListResults3"/>

	</xsl:if>
	<xsl:if test="contains($outputtype,'ListAndMap')">
		<xsl:call-template name="Map"/>

		<xsl:call-template name="ResultOptions"/>

		<xsl:call-template name="DrivingHomeAddress"/>

		<xsl:call-template name="ShowListResults3"/>

	</xsl:if>
</xsl:template>

  <xsl:template name="ShowListResults3">
    <div name="ListResultDiv" id="ListResultDiv">
      <table name="ListResults" id="ListResults" cellpadding="3" cellspacing="0" width="100%" class="table1" border="0">
        <tr valign="top">
          <th align="left" class="datashade defaultbold table1_item" nowrap="">#</th>
          <th align="left" class="datashade defaultbold table1_item" width="10%" >Type</th>
          <th align="left" class="datashade defaultbold table1_item">Address</th>
          <th align="left" class="datashade defaultbold table1_item">Hours</th>
          <th align="left" class="datashade defaultbold table1_item" width="10%" >Directions</th>
        </tr>
        <xsl:variable name="keyinstn" select="Company/KeyInstn"/>
        <xsl:for-each select="BranchesInRadius">
          <xsl:variable name="BankOrder" select="Order"/>
          <tr valign="top">
            <td valign="top" align="left" class="data table1_item" nowrap="">
              <xsl:value-of select="$BankOrder"/>
            </td>
            <td valign="top" align="left" class="data table1_item">
            <xsl:value-of select="bankfacilitytype"/>
             <!-- <xsl:variable name="btype" select="keybankfacilitytype"/>
              <xsl:choose>
                <xsl:when test="$btype = 0">Banking Center</xsl:when>
                <xsl:when test="$btype = 1">ATM</xsl:when>
                <xsl:when test="$btype = 2">Insurance</xsl:when>
              </xsl:choose>-->
            </td>
            <td valign="top" align="left" class="data table1_item">
              <span class="defaultbold">
                <xsl:choose>
                  <xsl:when test="count(bankfacility) = 0">
                    <xsl:variable name="bankKey" select="keyBranch"/>
                    <xsl:variable name="detailLink" select="concat(concat(concat('branchdetails.aspx?iid=',$keyinstn),'&amp;keybranch='),$bankKey)"/>
                    <a title="{BFNNoCode}">
                      <xsl:attribute name="href">
                        <xsl:value-of select="$detailLink"/>
                      </xsl:attribute>
                     <!-- <xsl:value-of select="BankFacilityName"/>-->
                     <xsl:value-of select="BFNNoCode"/>
                    </a>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:variable name="bankKey" select="bankfacility"/>
                    <xsl:variable name="detailLink" select="concat(concat(concat('branchdetails.aspx?iid=',$keyinstn),'&amp;bankfacility='),$bankKey)"/>
                   <!-- <a>
                      <xsl:attribute name="href">
                        <xsl:value-of select="$detailLink"/>
                      </xsl:attribute>
                      <xsl:value-of select="BankFacilityName"/>
                    </a>-->
                    <a title="{BFNNoCode}">
											<xsl:variable name="detailZip" select="concat('&amp;Zip=',../FormInfo/Zip)" />
											<xsl:variable name="detailStreet" select="concat('&amp;Street=',../FormInfo/Street)" />
											<xsl:variable name="detailCity" select="concat('&amp;City=',../FormInfo/City)" />
											<xsl:variable name="detailState" select="concat('&amp;State=',../FormInfo/State)" />
											<xsl:attribute name="href">
												<xsl:value-of select="$detailLink"/>
												<xsl:value-of select="$detailZip"/>
												<xsl:value-of select="$detailStreet"/>
												<xsl:value-of select="$detailCity"/>
												<xsl:value-of select="$detailState"/>
											</xsl:attribute>
											<xsl:value-of select="BFNNoCode"/>
			                    </a>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
              <xsl:if test="BankFacilityName !=''">
                <br />
              </xsl:if>
              <xsl:if test="Description !=''">
                <xsl:value-of select="Description" disable-output-escaping="yes"/>
                <br />
              </xsl:if>
              <xsl:value-of select="StreetNoCd"/>
              <xsl:if test="Street2NoCd != ''">
                <br />
              </xsl:if>

              <xsl:value-of select="Street2NoCd"/><br />
              <xsl:value-of select="cityNoCd"/>,
              <xsl:value-of select="state"/>&#160;<xsl:value-of select="zip"/>
              <xsl:if test="phone != ''">
                <br />Phone: <xsl:value-of select="phone"/>
              </xsl:if>
              <xsl:if test="fax != ''">
                <br />Fax: <xsl:value-of select="fax"/>
              </xsl:if>
              <br /><br />
            </td>
            <td valign="top" align="left" class="data table1_item">
              <xsl:variable name="myString" select="bankfacilityhours"/>

              <xsl:variable name="myNewString">
                <xsl:call-template name="replaceString">
                  <xsl:with-param name="stringIn" select="string($myString)"/>
                  <xsl:with-param name="charsIn" select="'&lt;br />'"/>
                  <xsl:with-param name="charsOut" select="' '"/>
                </xsl:call-template>
              </xsl:variable>
              <xsl:variable name="finalString">
                <xsl:call-template name="replaceString_a">
                  <xsl:with-param name="stringIn" select="string($myNewString)"/>
                  <xsl:with-param name="charsIn" select="' '"/>
                  <xsl:with-param name="charsOut" select="' '"/>
                </xsl:call-template>
              </xsl:variable>
              <!--<xsl:choose>
              	<xsl:when test="$finalString = 'ATM' or $finalString = 'Insurance'">&#160;</xsl:when>
                <xsl:when test="$finalString = 'Hours Not Available'">&#160;</xsl:when>
                <xsl:otherwise>-->
                  <xsl:value-of select="$finalString" disable-output-escaping="yes" />
               <!-- </xsl:otherwise>
              </xsl:choose>-->
&nbsp;
            </td>
            <td valign="top" align="left" class="data table1_item">
             <!-- <xsl:value-of select="Distance"/> miles
              <br />
              <a>
                <xsl:attribute name="href">
                  <xsl:value-of select="DrivingUrl"/>
                </xsl:attribute>
                Drive It!
              </a>
              <br />
              <a>
                <xsl:attribute name="href">
                  <xsl:value-of select="concat(concat('javascript:mapSingleBranch(',$BankOrder),')')"/>
                </xsl:attribute>
                Map It!
              </a>-->
              <span name="milesAway" id="milesAway">
								<xsl:value-of select="Distance"/>
							</span> miles
							<br />
							<a title="Drive It!">
								<xsl:attribute name="href">
									<xsl:value-of select="DrivingUrl"/>
								</xsl:attribute>
							Drive It!
							</a>
							<br />
							<a title="Map It!">
								<xsl:attribute name="href">
									<xsl:value-of select="concat(concat('javascript:mapSingleBranch(',$BankOrder),')')"/>
								</xsl:attribute>
							Map It!
						</a>
            </td>
          </tr>

        </xsl:for-each>

      </table>
    </div>
     <table width="100%" class="data">
		        <tr></tr>
		        <tr id="paginationRow" style="display:block">
		          <td align="right" class="data">
		          <xsl:for-each select="Pagination">
		              &nbsp;
		              <xsl:choose>
		                <xsl:when test="CurrentPage = 'true'">
		                  <xsl:value-of select="PageLabel"/>
		                </xsl:when>
		                <xsl:otherwise>
		                  <a title="{PageLabel}">
		                    <xsl:attribute name="href">
		                      <xsl:choose>
		                        <xsl:when test="contains(../Company/URL, '&amp;Start=')">
		                          <xsl:value-of select="substring-before(../Company/URL, '&amp;Start=')"/>&amp;Start=<xsl:value-of select="ResultStart"/>
		                        </xsl:when>
		                        <xsl:when test="contains(../Company/URL, '&amp;start=')">
		                          <xsl:value-of select="substring-before(../Company/URL, '&amp;start=')"/>&amp;Start=<xsl:value-of select="ResultStart"/>
		                        </xsl:when>
		                        <xsl:otherwise>
		                          <xsl:value-of select="../Company/URL"/>&amp;Start=<xsl:value-of select="ResultStart"/>
		                        </xsl:otherwise>
		                      </xsl:choose>
		                    </xsl:attribute>
		                    <xsl:value-of select="PageLabel"/>
		                  </a>
		                </xsl:otherwise>
		              </xsl:choose>
		          </xsl:for-each>
		          </td>
		        </tr>
      </table>
  </xsl:template>

  <xsl:template name="MapData3">
    <xsl:variable name="keyinstn" select="Company/KeyInstn"/>
    <xsl:variable name="formaction" select="concat('BranchLocatorMap.aspx?IID=', $keyinstn, '&amp;generateMap=1')"></xsl:variable>
    <div id="SearchForm">
      <table border="0" width="100%" align="left" cellpadding="0" cellspacing="0">

        <form name="branchsearch" id="branchsearch" method="POST">
			<fieldset>
				<legend>Branch Search</legend>
				<xsl:attribute name="action">
					<xsl:value-of select="$formaction"/>
				</xsl:attribute>

				<tr>
					<td class="data" valign="top">
						<xsl:call-template name="FormTitle3"/>
					</td>
				</tr>

				<tr>
					<td class="data" valign="top">
						<xsl:variable name="ext" select="FormInfo/Extended"/>
						<xsl:variable name="disp" select="FormInfo/Display"/>

						<input type="hidden" name="extended" class="datashade table2">
							<xsl:attribute name="Value">
								<xsl:choose>
									<xsl:when test="contains($ext,'NoPref')">
										<xsl:value-of select="$ext" disable-output-escaping="yes"/>
									</xsl:when>
									<xsl:when test="contains($ext,'Yes')">
										<xsl:value-of select="$ext" disable-output-escaping="yes"/>
									</xsl:when>
									<xsl:when test="contains($ext,'No')">
										<xsl:value-of select="$ext" disable-output-escaping="yes"/>
									</xsl:when>
									<xsl:otherwise>NoPref</xsl:otherwise>
								</xsl:choose>
							</xsl:attribute>
						</input>

						<input type="hidden" name="ResultDisp" class="datashade table2">
							<xsl:attribute name="Value">
								<xsl:choose>
									<xsl:when test="contains($disp,'ListAndMap')">
										<xsl:value-of select="$disp" disable-output-escaping="yes"/>
									</xsl:when>
									<xsl:when test="contains($disp,'LocationOnly')">
										<xsl:value-of select="$disp" disable-output-escaping="yes"/>
									</xsl:when>
									<xsl:otherwise>ListAndMap</xsl:otherwise>
								</xsl:choose>
							</xsl:attribute>
						</input>

						<!--<input class="datashade table2" type="reset" value="Clear"/>&#160;<input class="datashade table2" type="button" value="Search" onclick="load(this.form)"/>
              <xsl:call-template name="ResultsChoices3"/>-->
					</td>
				</tr>
				<tr>
					<td class="data" valign="top">&#160;</td>
				</tr>
				<input type="hidden" name="HomeLat" id="HomeLat"></input>
				<input type="hidden" name="HomeLong" id="HomeLong"></input>
				<tr>
					<td class="data" valign="top">
						<xsl:call-template name="DummyMap"/>
					</td>
				</tr>
			</fieldset>
        </form>

      </table>
    </div>
  </xsl:template>
<!-- THREE Template Ends here.-->



  <!-- Bellow Template Use in All Template -->

  <!--Template that holds map div that will be used by google for a map if requested by user -->
  <xsl:template name="DummyMap">
    <div id="DummyMap" style="width:0px; height: 0px; display:none;" class="surr table2"></div>
  </xsl:template>

  <xsl:template name="Map">
    <div id="mapContainer" style="width: 100%; text-align: center;">
      <div id="map" style="width: 100%; height: 512px; margin-left: auto; margin-right: auto;" class="surr table2"></div>
    </div>
    <div id="keyPage" style="display:none;">
      <xsl:value-of select="Company/KeyPage" />
    </div>
    <div id="keyInstn" style="display:none;">
      <xsl:value-of select="Company/KeyInstn" />
    </div>
    <div id="directionsString" style="display:none;"></div>
    <SCRIPT LANGUAGE="JavaScript">
      function EmailPopup(KeyInstn, URL) {
      var directions = document.getElementById("directionsString").innerHTML;
      URL = URL + directions;
      var page = "EmailPopup.aspx?IID=" + KeyInstn + "&amp;url=" + URL;
      var winprops = "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=640,height=400";
      window.open(page, "", winprops);
      }
    </SCRIPT>
  </xsl:template>

  <xsl:template name="CreateMarkersForListOnly">
    <script>
      function initialize() {
      createHomeMarker('<xsl:value-of select="HomePoint/latitude"/>','<xsl:value-of select="HomePoint/longitude"/>',<xsl:value-of select="FormInfo/Radius"/>);
      <xsl:for-each select="BranchesInRadius">
        populateMarkerArray(map,'<xsl:value-of select="latitude"/>', '<xsl:value-of select="longitude"/>', '<xsl:value-of select="BankFacilityName"/>','<xsl:value-of select="Street"/>','<xsl:value-of select="Street2"/>','<xsl:value-of select="city"/>','<xsl:value-of select="state"/>','<xsl:value-of select="zip"/>','<xsl:value-of select="Order"/>','<xsl:value-of select="phone"/>');
      </xsl:for-each>
      }
    </script>
  </xsl:template>

  <xsl:template name="ShowMapResults">
    <script>
      var thisLocation = String(window.location);

      function initialize() {
        if (thisLocation.toLowerCase().indexOf("print") > -1) {
          createPDFMap();
        }
        else {
          createMap();
        }

      createHomePoint('<xsl:value-of select="HomePoint/latitude"/>','<xsl:value-of select="HomePoint/longitude"/>',<xsl:value-of select="FormInfo/Radius"/>);

      <xsl:if test="Destination/HasDestination = 'true'">
        setDestination('<xsl:value-of select="Destination/DestinationLat"/>','<xsl:value-of select="Destination/DestinationLong"/>','<xsl:value-of select="Destination/DestinationAddress"/>','<xsl:value-of select="Destination/DestinationName"/>');
      </xsl:if>

      <xsl:for-each select="BranchesInRadius">
        plotBranch(map,'<xsl:value-of select="latitude"/>', '<xsl:value-of select="longitude"/>', '<xsl:value-of select="BankFacilityName"/>','<xsl:value-of select="Street"/>','<xsl:value-of select="Street2"/>','<xsl:value-of select="city"/>','<xsl:value-of select="state"/>','<xsl:value-of select="zip"/>','<xsl:value-of select="Order"/>','<xsl:value-of select="phone"/>','<xsl:value-of select="onlineimagefilepath"/>');
      </xsl:for-each>
      }
      
      google.maps.event.addDomListener(window, 'load', initialize);
    </script>
  </xsl:template>

  <xsl:template name="ResultOptions">
    <table width="100%" cellpadding="3" cellspacing="0">
      <tr>
        <td>
          <div id="differentDirs" style="display:none; font-size:11px;">For directions to a new location, please click on the location number and then "Drive It!"</div>
          <xsl:if test="count(BranchesInRadius)=99">
            Search results have been limited to a maximum of 99 branches.
          </xsl:if>
        </td>

        <td align="center" valign="">
          <table border="0" width="150" cellpadding="0" cellspacing="0" class=""
          style="border-top: 1px solid #000000;
		border-right-width: 1px;
		border-left-width: 1px;
		border-right-style: solid;
		border-left-style: solid;
		border-right-color: #000000;
		border-left-color: #000000;
		border-bottom-color: #000000;
		border-bottom-width: 1px;
		border-bottom-style: solid;
		">
            <tr>
              <td valign="top" align="center" nowrap="" >
                <A style="text-decoration: none;font-size:11px;" title="New Search">
                  <xsl:variable name="customXSLT" select="substring-before(substring-after(Company/URL, 'xslt='), '&amp;')" />
                  <xsl:attribute name="href">
                    <xsl:value-of select="substring-after(substring-before(Company/URL, '?'), '.com:81')"/>?iid=<xsl:value-of select="Company/KeyInstn"/>
                    <xsl:if test="string-length($customXSLT) > 0">&amp;xslt=<xsl:value-of select="$customXSLT"/></xsl:if>
                  </xsl:attribute>
                  New Search
                </A>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template name="replaceString">
    <xsl:param name="stringIn"/>
    <xsl:param name="charsIn"/>
    <xsl:param name="charsOut"/>
    <xsl:choose>
      <xsl:when test="contains($stringIn,$charsIn)">
        <xsl:value-of select="concat(substring-before($stringIn,$charsIn),$charsOut)"/>
        <xsl:call-template name="replaceString">
          <xsl:with-param name="stringIn" select="substring-after($stringIn,$charsIn)"/>
          <xsl:with-param name="charsIn" select="$charsIn"/>
          <xsl:with-param name="charsOut" select="$charsOut"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$stringIn"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="replaceString_a">
    <xsl:param name="stringIn"/>
    <xsl:param name="charsIn"/>
    <xsl:param name="charsOut"/>
    <xsl:choose>
      <xsl:when test="contains($stringIn,$charsIn)">
        <xsl:value-of select="concat(substring-before($stringIn,$charsIn),$charsOut)"/>
        <xsl:call-template name="replaceString_a">
          <xsl:with-param name="stringIn" select="substring-after($stringIn,$charsIn)"/>
          <xsl:with-param name="charsIn" select="$charsIn"/>
          <xsl:with-param name="charsOut" select="$charsOut"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$stringIn"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="TransactionError">
    <td colspan="2">
      <br />
      	<xsl:value-of select="TransactionError/Error" />
      <br />
      <br />
    </td>
  </xsl:template>

  <xsl:template name="States">
    <xsl:variable name="enteredState" select="FormInfo/State" />
    <option value="">
      	<xsl:attribute name="class">
	        <xsl:choose>
	        <xsl:when test="*/Company/TemplateName != ''">datashade</xsl:when>
	        <xsl:otherwise>default</xsl:otherwise>
	        </xsl:choose>
        </xsl:attribute></option>
    <xsl:for-each select="State">
      <xsl:variable name="currentState" select="Abbreviation" />
      <option>
      	<xsl:attribute name="class">
	        <xsl:choose>
	        <xsl:when test="*/Company/TemplateName != ''">datashade</xsl:when>
	        <xsl:otherwise>default</xsl:otherwise>
	        </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name="value">
          <xsl:value-of select="Abbreviation" />
        </xsl:attribute>
        <xsl:if test="$enteredState = $currentState">
          <xsl:attribute name="selected">
            selected
          </xsl:attribute>
        </xsl:if>
        <xsl:value-of select="StateName" />
      </option>
    </xsl:for-each>
  </xsl:template>

  <!-- 
    This template includes the default javascript that must be included for the Branch Locator Map to work.
    If possible DO NOT override this template in a company specific stylesheet or create a custom BranchLocatorMap.js file for a company.
    If a company needs specific edits to BranchLocatorMap.js, include new JavaScript in the BranchLocatorJSAlt template in their
    company specific stylsheet which will override the necessary functions from BranchLocatorMap.js
  -->
  <xsl:template name="BranchLocatorJS">
    <xsl:if test="BranchesInRadius">
      <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?client=gme-snlf&amp;sensor=false"></script>
    </xsl:if>
    <script type="text/javascript" src="javascript/GraphNew/jquery.min.js"></script>
    <script type="text/javascript" src="javascript/GraphNew/jquery.blockUI.js"></script>
    <script type="text/javascript" src="BranchLocatorMap.js"></script>
    
    
  </xsl:template>

  <xsl:template name="BranchLocatorJSAlt">
    <!-- 
      This template is purposely empty by default. Any company specific JS changes should be done in a new JS file referenced
      here in their company specific stylesheet.
     -->  
  </xsl:template>

  <!-- End Template Use in All Template -->













  <!-- Main XSLT templates are here. these are the templates which are actually being displayed in the page. this is for TOP LEVEL editing. if you dont need to use the individual templates above you can do your main editing here when pulling THESE templates below you must carry the top comment with them and uncomment them when you drop it into the company specific xslt.
-->

  

  <xsl:template match="irw:BranchLocatorMap">
    <xsl:variable name="TemplateName" select="Company/TemplateName"/>
    <xsl:call-template name="BranchLocatorJS" />
    <xsl:call-template name="BranchLocatorJSAlt" />

    <!-- Code use all in template -->
    

    
<!-- End -->

    <xsl:choose>
      <!-- Template 1 -->
      <xsl:when test="$TemplateName = 'AltBranchLoc1'">
        <!-- Copy Template1 here down
<xsl:template match="irw:BranchLocatorMap">
<xsl:call-template name="BranchLocatorMap_Header" /> -->

        <xsl:call-template name="TemplateONEstylesheet" />
        <table border="0" cellspacing="0" cellpadding="3" width="100%" class="">
          <xsl:call-template name="Title_S2"/>
          <tr align="left">
            <td class="leftTOPbord" colspan="2">
              <a name="drivingAddy"></a>
              <table border="0" cellspacing="0" cellpadding="3" width="100%" id="table1">
                <tr class="colorlight">
                  <td valign="top" class="colorlight">
                    <xsl:if test="count(BranchesInRadius) = 0">
                      <xsl:call-template name="MapData2"/>
                    </xsl:if>

                    <xsl:if test="count(BranchesInRadius) &gt; 0">
                      <xsl:call-template name="MapResults2"/>
                    </xsl:if>
                  </td>
                </tr>
              </table>
              <!-- for IE to work script must be called outside of table-->
              <xsl:if test="count(BranchesInRadius) &gt; 0">
                <xsl:variable name="outtype" select="FormInfo/Display"/>
                <xsl:if test="contains($outtype,'ListAndMap')">
                  <xsl:call-template name="ShowMapResults"/>
                </xsl:if>
                <xsl:if test="contains($outtype,'LocationOnly')">
                  <xsl:call-template name="CreateMarkersForListOnly"/>
                </xsl:if>
              </xsl:if>
            </td>
          </tr>
          <xsl:if test="Company/Footer !=''">
            <tr class="data">
              <td class="data" align="left" valign="top" colspan="2">
                <span class="default">
                  <xsl:value-of select="Company/Footer" disable-output-escaping="yes"/>
                </span>
              </td>
            </tr>
          </xsl:if>
        </table>

        <xsl:call-template name="Copyright"/>
        <!--</xsl:template>
End Copy Here
-->
      </xsl:when>
      <!-- End Template 2 -->





      <!-- Template 3 -->
      <xsl:when test="$TemplateName = 'AltBranchLoc3'">
        <!-- Copy Template3 here down
<xsl:template match="irw:BranchLocatorMap">
<xsl:call-template name="BranchLocatorMap_Header" /> -->
        <xsl:call-template name="TemplateTHREEstylesheet" />
        <xsl:call-template name="Title_T3"/>
        <table border="0" cellspacing="0" cellpadding="3" width="100%" class="table2">
          <tr align="center">
            <td class="data">
              <a name="drivingAddy"></a>
              <table border="0" cellspacing="0" cellpadding="3" width="100%" id="TABLE1">
                <tr class="data">
                  <td valign="top" class="data">
                    <xsl:if test="count(BranchesInRadius) = 0">
                      <xsl:call-template name="MapData3"/>
                    </xsl:if>

                    <xsl:if test="count(BranchesInRadius) &gt; 0">
                      <xsl:call-template name="MapResults3"/>
                    </xsl:if>
                  </td>
                </tr>
              </table>
              <!-- for IE to work script must be called outside of table-->
              <xsl:if test="count(BranchesInRadius) &gt; 0">
                <xsl:variable name="outtype" select="FormInfo/Display"/>
                <xsl:if test="contains($outtype,'ListAndMap')">
                  <xsl:call-template name="ShowMapResults"/>
                </xsl:if>
                <xsl:if test="contains($outtype,'LocationOnly')">
                  <xsl:call-template name="CreateMarkersForListOnly"/>
                </xsl:if>
              </xsl:if>
            </td>
          </tr>
          <xsl:if test="Company/Footer !=''">
            <tr class="data">
              <td class="data" align="left" valign="top">
                <span class="default">
                  <xsl:value-of select="Company/Footer" disable-output-escaping="yes"/>
                </span>
              </td>
            </tr>
          </xsl:if>
        </table>
        <xsl:call-template name="Copyright"/>
        <!--</xsl:template>
End Copy Here
-->
      </xsl:when>
      <!-- End Template 3 -->

      <xsl:otherwise>
        <!-- Copy Default Template here down
<xsl:template match="irw:BranchLocatorMap">
<xsl:call-template name="BranchLocatorMap_Header" /> -->
        <table border="0" cellspacing="0" cellpadding="3" width="100%" class="">
          <xsl:call-template name="Title_S1"/>
          <tr>
            <xsl:choose>
              <xsl:when test="count(TransactionError) &gt; 0">
                <xsl:call-template name="TransactionError"/>
              </xsl:when>
              <xsl:otherwise>
                <td class="colordark" colspan="2">
                  <a name="drivingAddy"></a>
                  <table border="0" cellspacing="0" cellpadding="3" width="100%" id="TABLE1">
                    <tr class="colorlight">
                      <td valign="top" class="colorlight">
                        <xsl:if test="count(BranchesInRadius) = 0">
                          <xsl:call-template name="MapData"/>
                        </xsl:if>

                        <xsl:if test="count(BranchesInRadius) &gt; 0">
                          <xsl:call-template name="MapResults"/>
                        </xsl:if>
                      </td>
                    </tr>
                  </table>
                  <!-- for IE to work script must be called outside of table-->
                  <xsl:if test="count(BranchesInRadius) &gt; 0">
                    <xsl:variable name="outtype" select="FormInfo/Display"/>
                    <xsl:if test="contains($outtype,'ListAndMap')">
                      <xsl:call-template name="ShowMapResults"/>
                    </xsl:if>
                    <xsl:if test="contains($outtype,'LocationOnly')">
                      <xsl:call-template name="CreateMarkersForListOnly"/>
                    </xsl:if>
                  </xsl:if>
                </td>
              </xsl:otherwise>
            </xsl:choose>
          </tr>
          <xsl:if test="Company/Footer !=''">
            <tr class="colorlight">
              <td class="colorlight" align="left" valign="top" colspan="2">
                <span class="default">
                  <xsl:value-of select="Company/Footer" disable-output-escaping="yes"/>
                </span>
              </td>
            </tr>
          </xsl:if>
        </table>
        <xsl:call-template name="Copyright"/>
        <!--</xsl:template>
End Copy Here
-->
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
