<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.ord/TR/REC-html40"
	xmlns:irw="http://www.snl.com/xml/irw">

<xsl:output method="html" media-type="text/html" indent="no"/>

<xsl:template name="CalDesc_Title">
	<xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>

	<TR>
		<TD CLASS="colordark" NOWRAP="NOWRAP" border="0">
			<SPAN CLASS="title1light"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>Event Description</SPAN>
		</TD>
		<TD CLASS="colordark" NOWRAP="NOWRAP" align="right" valign="top" border="0">
			<span class="subtitlelight"><xsl:value-of select="$CompanyNameCaps"/> (<xsl:value-of select="Company/Exchange"/> - <xsl:value-of select="Company/Ticker"/>)</span><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
		</TD>
	</TR>
	<TR class="default" align="left">
		<TD class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Header" disable-output-escaping="yes"/></span></TD>
	</TR>
</xsl:template>

<xsl:template name="CalDesc_Detail">
	<TR>
		<td CLASS="data" valign="top" border="0" align="left">
		<span CLASS="defaultbold" >Event:
		<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
		</span><br/><br/>
		</td>
		<td CLASS="data" valign="top" border="0" align="left"><xsl:value-of select="CalendarDescription/CompanyCalendarTitle"></xsl:value-of></td>
	</TR>
	<tr>
		<td CLASS="data" valign="top" border="0" align="left"><span CLASS="defaultbold">Date:<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></span></td>
		<td CLASS="data" valign="top" border="0" align="left"><xsl:value-of select="CalendarDescription/CompanyCalendarDate" disable-output-escaping="yes"></xsl:value-of></td>
	</tr>
	<tr><td CLASS="data" valign="top" border="0" align="center" colspan="2">
	<span CLASS="defaultbold" style="border-bottom:1px dotted #666666; font-size:14px" >Event details</span><br/><br/>
	</td></tr>
	<tr>
		<td CLASS="data" valign="top" align="left" colspan="2" border="0">
		<xsl:value-of disable-output-escaping="yes" select="CalendarDescription/CompanyCalendarEvent"></xsl:value-of></td>
	</tr>
</xsl:template>

<xsl:template name="FooterTag">
<tr class="colorlight">
<td colspan="2"><span class="large"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span></td>
</tr>
</xsl:template>

<xsl:template match="CalDesc">
<HTML>
<head><title>Event Description</title></head>
<xsl:variable name="KeyInstn" select="Company/KeyInstn"/>
<xsl:variable name="anchortag1" select="concat('/interactive/lookandfeel/', $KeyInstn, '/style.css')"/>
<xsl:variable name="anchortag2" select="concat('/Interactive/LookAndFeel/IRStyles/live_', $KeyInstn, '.css')"/>
<xsl:variable name="anchortag3" select="concat('/Interactive/LookAndFeel/IRStyles/default', '.css')"/>
<link REL="stylesheet" TYPE="text/css"><xsl:attribute name="href"><xsl:value-of select="$anchortag1"/></xsl:attribute></link>
<link REL="stylesheet" TYPE="text/css"><xsl:attribute name="href"><xsl:value-of select="$anchortag2"/></xsl:attribute></link>
<link REL="stylesheet" TYPE="text/css"><xsl:attribute name="href"><xsl:value-of select="$anchortag3"/></xsl:attribute></link>
<style>
.data,.defaultbold, a:link, a:hover, a:visited{
font-size:12px;
font-family:verdana
}
</style>
<body>
<table BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%">
	<!--<xsl:call-template name="Title_S1"/>-->

	<TR ALIGN="CENTER">
		<TD  colspan="2">
			<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" ALIGN="center">
				<TR ALIGN="CENTER">
					<TD >
						<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="4" WIDTH="100%" align="center">
						<xsl:call-template name="CalDesc_Detail"/>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
<xsl:call-template name="FooterTag"/>
	<tr>
		<td colspan="2" class="colorlight" align="center" border="0"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
	</tr>
	<!--<tr>
		<form>
		<td colspan="2" class="colorlight" align="center" border="0">
			<input type="button" onClick="window.close()" Value="Close Window"></input>
		</td>
		</form>
	</tr>-->
</table>
</body>
</HTML>
</xsl:template>



</xsl:stylesheet>
