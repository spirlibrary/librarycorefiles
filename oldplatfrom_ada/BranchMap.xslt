<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.ord/TR/REC-html40"
	xmlns:irw="http://www.snl.com/xml/irw">

<xsl:output method="html" media-type="text/html" indent="no"/>
<xsl:decimal-format name="data" NaN="NA"/>

<!-- Default Build for Branch Mapping -->

<xsl:template name="BranchMap_Title">
	<xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
	<TR>
		<TD CLASS="colordark" NOWRAP="true" border="0" align="left"><SPAN CLASS="title1light"><xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/></SPAN></TD>
		<TD CLASS="colordark" NOWRAP="true" align="right" valign="top" border="0">
			<span class="subtitlelight"><xsl:value-of select="$CompanyNameCaps"/> (<xsl:value-of select="Company/Exchange"/> - <xsl:value-of select="Company/Ticker"/>)</span><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
		</TD>
	</TR>
	<TR class="default" align="left">
		<TD class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Header" disable-output-escaping="yes"/></span></TD>
	</TR>
</xsl:template>

<xsl:template name="FooterTag">
  <xsl:if test="Company/Footer != ''">
    <tr class="colorlight">
      <td colspan="2"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></td>
    </tr>
  </xsl:if>
</xsl:template>
  
<xsl:template name="FooterTag3">
<xsl:if test="Company/Footer != ''">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="data">
  <tr>
    <td class="data"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></td>
  </tr>
  </table>
</xsl:if>
</xsl:template>



<xsl:template name="BranchData">
	<table id="branchdata" cellspacing="0" cellpadding="2" width="100%" >
	<tr>
		<td align="left" valign="bottom" class="header">Branch</td>
		<td align="left" valign="bottom" class="header">Description</td>
		<td align="left" valign="bottom" class="header">City</td>
		<td align="left" valign="bottom" class="header">State</td>
		<td mytype="comma_numeric" align="right" valign="bottom" nowrap="" class="header">&#160;Deposits ($000)</td>
	</tr>
	<xsl:for-each select="BranchInfo">
	<tr>
		<td valign="top" class="data"><xsl:value-of select="BranchLevelName" disable-output-escaping="yes"/></td>
		<td valign="top" class="data"><xsl:value-of select="Description"/></td>
		<td valign="top" class="data"><xsl:value-of select="City"/></td>
		<td valign="top" class="data"><xsl:value-of select="State"/></td>
		<td align="right" valign="top" class="data"><xsl:value-of select="format-number(Deposits,'#,##0','data')"/></td>
	</tr>
	</xsl:for-each>

	</table>
</xsl:template>

<xsl:template name="PropertyData">
	<table id="propertydata" cellspacing="0" cellpadding="2" width="100%">
	<tr>
		<td align="left" valign="bottom" class="header">Property</td>
		<td align="left" valign="bottom" class="header">Property Type</td>
		<td align="left" valign="bottom" class="header">Address</td>
		<td align="left" valign="bottom" class="header">City</td>
		<td align="left" valign="bottom" class="header">State</td>
		<td align="left" valign="bottom" class="header">Zip</td>
		<td mytype="comma_numeric" align="right" valign="bottom" nowrap="" class="header">Percent Owned</td>
	</tr>

	<xsl:for-each select="PropertyInfo">
	<tr>
		<td align="left" class="data"><xsl:value-of select="PropertyName" disable-output-escaping="yes"/></td>
		<td valign="top" class="data"><xsl:value-of select="KeyPropertyType" disable-output-escaping="yes" /></td>
		<td valign="top" class="data"><xsl:value-of select="Address" disable-output-escaping="yes" /></td>
		<td valign="top" class="data"><xsl:value-of select="City"/></td>
		<td valign="top" class="data"><xsl:value-of select="State"/></td>
		<td valign="top" class="data"><xsl:value-of select="Zip"/></td>
		<td valign="top" class="data" align="right"><xsl:value-of select="format-number(PropertyPercentOwned,'#,##0.00','data')"/></td>
	</tr>
	</xsl:for-each>

	</table>
</xsl:template>


<xsl:template name="PlantData">
	<table id="plantdata" cellspacing="0" cellpadding="2" width="100%">
	<tr>
		<td align="left" valign="bottom" class="header">Power Plant</td>
		<td align="left" valign="bottom" class="header">Plant Type</td>
		<td align="left" valign="bottom" class="header">Region</td>
		<td align="left" valign="bottom" class="header">Sub Region</td>
		<td align="left" valign="bottom" class="header">Electric Generation Status</td>
		<td align="left" valign="bottom" class="header">Operating Status</td>
		<td mytype="comma_numeric" align="right" valign="bottom" nowrap="" class="header">Generating Capacity</td>
	</tr>

	<xsl:for-each select="PlantInfo">
	<tr>
		<td valign="top" class="data"><xsl:value-of select="PowerPlant" disable-output-escaping="yes"/></td>
		<td valign="top" class="data"><xsl:value-of select="KeyEnergyPlantType"/></td>
		<td valign="top" class="data"><xsl:value-of select="KeyNercRegion"/></td>
		<td valign="top" class="data"><xsl:value-of select="KeyNercSubRegion"/></td>
		<td valign="top" class="data"><xsl:value-of select="KeyElectricGenerationStatus"/></td>
		<td valign="top" class="data"><xsl:value-of select="KeyPowerPlantOperatingStatus"/></td>
		<td valign="top" class="data" align="right"><xsl:value-of select="format-number(GeneratingCapacity,'#,##0.00','data')"/></td>
	</tr>
	</xsl:for-each>

	</table>
</xsl:template>

<xsl:template name="ZoomImage">
  <xsl:param name="ImageID"/>
  <xsl:param name="CurrentZoomLevel"/>
  
  <xsl:variable name="ImageLevel" select="number(substring($ImageID, 21, string-length($ImageID)-20)) "/>
  <xsl:variable name="q">'</xsl:variable>
	<td class="cellbglight">
		<label class="visuallyhidden" for="{$ImageID}">Zoom</label>
		<input>
		  <xsl:attribute name="type">image</xsl:attribute>
		  <xsl:attribute name="id"><xsl:value-of select="$ImageID"/></xsl:attribute>
		  <xsl:attribute name="Width">11px</xsl:attribute>
		  <xsl:attribute name="Height">15px</xsl:attribute>
		  <xsl:attribute name="src">
			<xsl:choose>
			  <xsl:when test="number($CurrentZoomLevel)=$ImageLevel">images/zoom_arrow_current.gif</xsl:when>
			  <xsl:when test="number($CurrentZoomLevel)&gt;$ImageLevel">images/zoom_arrow_in.gif</xsl:when>
			  <xsl:when test="number($CurrentZoomLevel)&lt;$ImageLevel">images/zoom_arrow_out.gif</xsl:when>
			</xsl:choose>		  
		  </xsl:attribute>  
		  <xsl:attribute name="onClick"><xsl:value-of select="concat('setAction(this.form, ', $q, 'Zoom', $ImageLevel, $q, ');')"/></xsl:attribute>
		</input>
	</td>
</xsl:template>


<!-- End Default Build for Branch Mapping -->












<!-- Start Alternative Build for Branch Mapping -->

<xsl:template name="BranchMap_Title_new">
	<xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
	<TR>
		<TD CLASS="title2colordark titletest" nowrap="" valign="top"><xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/><br /><IMG SRC="" WIDTH="2" HEIGHT="1" alt="" /><span class="title2colordark titletest2"><xsl:value-of select="$CompanyNameCaps"/>&#160;(<xsl:value-of select="Company/Exchange"/>&#160;-&#160;<xsl:value-of select="Company/Ticker"/>)&#160;</span></TD>
		<TD CLASS="" nowrap="" align="right" valign="top"></TD>
	</TR>
</xsl:template>


<xsl:template name="ZoomImage_new">
  <xsl:param name="ImageID"/>
  <xsl:param name="CurrentZoomLevel"/>  
  <xsl:variable name="ImageLevel" select="number(substring($ImageID, 21, string-length($ImageID)-20)) "/>
  <xsl:variable name="q">'</xsl:variable>
	<td class="cellbglight">
		<label class="visuallyhidden" for="{$ImageID}">Branch</label>
		<input>
		  <xsl:attribute name="type">image</xsl:attribute>
		  <xsl:attribute name="id"><xsl:value-of select="$ImageID"/></xsl:attribute>
		  <xsl:attribute name="Width">11px</xsl:attribute>
		  <xsl:attribute name="Height">15px</xsl:attribute>
		  <xsl:attribute name="src">
			<xsl:choose>
			  <xsl:when test="number($CurrentZoomLevel)=$ImageLevel">images/branchon.gif</xsl:when>
			  <xsl:when test="number($CurrentZoomLevel)&gt;$ImageLevel">images/branchoff.gif</xsl:when>
			  <xsl:when test="number($CurrentZoomLevel)&lt;$ImageLevel">images/branchoff.gif</xsl:when>
			</xsl:choose>		  
		  </xsl:attribute>  
		  <xsl:attribute name="onClick"><xsl:value-of select="concat('setAction(this.form, ', $q, 'Zoom', $ImageLevel, $q, ');')"/></xsl:attribute>
		</input>
	</td>
</xsl:template>



<xsl:template name="ZoomImage_small">
  <xsl:param name="ImageID"/>
  <xsl:param name="CurrentZoomLevel"/>  
  <xsl:variable name="ImageLevel" select="number(substring($ImageID, 21, string-length($ImageID)-20)) "/>
  <xsl:variable name="q">'</xsl:variable>
	<td class="cellbglight">
		<label class="visuallyhidden" for="{$ImageID}">Branch</label>
		<input>
		  <xsl:attribute name="type">image</xsl:attribute>
		  <xsl:attribute name="id"><xsl:value-of select="$ImageID"/></xsl:attribute>
		  <xsl:attribute name="Width">9px</xsl:attribute>
		  <xsl:attribute name="Height">9px</xsl:attribute>
		  <xsl:attribute name="src">
			<xsl:choose>
			  <xsl:when test="number($CurrentZoomLevel)=$ImageLevel">images/branchon_small.gif</xsl:when>
			  <xsl:when test="number($CurrentZoomLevel)&gt;$ImageLevel">images/branchoff_small.gif</xsl:when>
			  <xsl:when test="number($CurrentZoomLevel)&lt;$ImageLevel">images/branchoff_small.gif</xsl:when>
			</xsl:choose>		  
		  </xsl:attribute>  
		  <xsl:attribute name="onClick"><xsl:value-of select="concat('setAction(this.form, ', $q, 'Zoom', $ImageLevel, $q, ');')"/></xsl:attribute>
		</input>
	</td>
</xsl:template>



<!-- End Alternative Build -->

<!-- Alternate Branch Page Build -->
<!--



-->
<!-- START TEMPLATE 1 -->

<xsl:template name="BranchData2">
	<table id="branchdata" cellspacing="0" cellpadding="2" width="100%" >
		<tr>
			<td align="left" valign="bottom" class="surrleft header">Branch</td>
			<td align="left" valign="bottom" class="surr_topbot header">Description</td>
			<td align="left" valign="bottom" class="surr_topbot header">City</td>
			<td align="left" valign="bottom" class="surr_topbot header">State</td>
			<td mytype="comma_numeric" align="right" valign="bottom" nowrap="" class="surrright header">&#160;Deposits ($000)</td>
	</tr>
	
	<xsl:for-each select="BranchInfo">
	<tr>
		<td valign="top" class="data"><xsl:value-of select="BranchLevelName" disable-output-escaping="yes"/></td>
		<td valign="top" class="data"><xsl:value-of select="Description"/></td>
		<td valign="top" class="data"><xsl:value-of select="City"/></td>
		<td valign="top" class="data"><xsl:value-of select="State"/></td>
		<td align="right" valign="top" class="data"><xsl:value-of select="format-number(Deposits,'#,##0','data')"/></td>
	</tr>
	</xsl:for-each>

	</table>
</xsl:template>

<xsl:template name="PropertyData2">
		<table class="sortable" id="propertydata" cellspacing="0" cellpadding="2" width="100%">
		<tr>
			<td align="left" valign="bottom" class="surrleft header">Property</td>
			<td align="left" valign="bottom" class="surr_topbot header">Property Type</td>
			<td align="left" valign="bottom" class="surr_topbot header">Address</td>
			<td align="left" valign="bottom" class="surr_topbot header">City</td>
			<td align="left" valign="bottom" class="surr_topbot header">State</td>
			<td align="left" valign="bottom" class="surr_topbot header">Zip</td>
			<td mytype="comma_numeric" align="right" valign="bottom" nowrap="" class="surrright header">Percent Owned</td>
		</tr>

	<xsl:for-each select="PropertyInfo">
	<tr>
		<td align="left" class="data"><xsl:value-of select="PropertyName" disable-output-escaping="yes"/></td>
		<td valign="top" class="data"><xsl:value-of select="KeyPropertyType" disable-output-escaping="yes" /></td>
		<td valign="top" class="data"><xsl:value-of select="Address" disable-output-escaping="yes" /></td>
		<td valign="top" class="data"><xsl:value-of select="City"/></td>
		<td valign="top" class="data"><xsl:value-of select="State"/></td>
		<td valign="top" class="data"><xsl:value-of select="Zip"/></td>
		<td valign="top" class="data" align="right"><xsl:value-of select="format-number(PropertyPercentOwned,'#,##0.00','data')"/></td>
	</tr>
	</xsl:for-each>

	</table>
</xsl:template>


<xsl:template name="PlantData2">
	<table class="sortable" id="plantdata" cellspacing="0" cellpadding="2" width="100%">
	<tr>
		<td align="left" valign="bottom" class="surrleft header">Power Plant</td>
		<td align="left" valign="bottom" class="surr_topbot header">Plant Type</td>
		<td align="left" valign="bottom" class="surr_topbot header">Region</td>
		<td align="left" valign="bottom" class="surr_topbot header">Sub Region</td>
		<td align="left" valign="bottom" class="surr_topbot header">Electric Generation Status</td>
		<td align="left" valign="bottom" class="surr_topbot header">Operating Status</td>
		<td mytype="comma_numeric" align="right" valign="bottom" nowrap="" class="surrright header">Generating Capacity</td>
	</tr>

	<xsl:for-each select="PlantInfo">
	<tr>
		<td valign="top" class="data"><xsl:value-of select="PowerPlant" disable-output-escaping="yes"/></td>
		<td valign="top" class="data"><xsl:value-of select="KeyEnergyPlantType"/></td>
		<td valign="top" class="data"><xsl:value-of select="KeyNercRegion"/></td>
		<td valign="top" class="data"><xsl:value-of select="KeyNercSubRegion"/></td>
		<td valign="top" class="data"><xsl:value-of select="KeyElectricGenerationStatus"/></td>
		<td valign="top" class="data"><xsl:value-of select="KeyPowerPlantOperatingStatus"/></td>
		<td valign="top" class="data" align="right"><xsl:value-of select="format-number(GeneratingCapacity,'#,##0.00','data')"/></td>
	</tr>
	</xsl:for-each>

	</table>
</xsl:template>



<!-- END TEMPLATE1-->


<!-- START TEMPLATE 3 -->

<xsl:template name="BranchData3">
	<table class="sortable table1 table1_item" id="branchdata" cellspacing="0" cellpadding="2" width="100%" >
	<tr>
		<td align="left" valign="bottom" class="table1_item header">Branch</td>
		<td align="left" valign="bottom" class="table1_item header">Description</td>
		<td align="left" valign="bottom" class="table1_item header">City</td>
		<td align="left" valign="bottom" class="table1_item header">State</td>
		<td mytype="comma_numeric" align="right" valign="bottom" nowrap="" class="table1_item header">&#160;Deposits ($000)</td>
	</tr>
	<xsl:for-each select="BranchInfo">
	<tr>
		<td valign="top" class="data"><xsl:value-of select="BranchLevelName" disable-output-escaping="yes"/></td>
		<td valign="top" class="data"><xsl:value-of select="Description"/></td>
		<td valign="top" class="data"><xsl:value-of select="City"/></td>
		<td valign="top" class="data"><xsl:value-of select="State"/></td>
		<td align="right" valign="top" class="data"><xsl:value-of select="format-number(Deposits,'#,##0','data')"/></td>
	</tr>
	</xsl:for-each>

	</table>
</xsl:template>

<xsl:template name="PropertyData3">
	<table class="sortable table1 table1_item" id="propertydata" cellspacing="0" cellpadding="2" width="100%">
	<tr>
		<td align="left" valign="bottom" class="table1_item header">Property</td>
		<td align="left" valign="bottom" class="table1_item header">Property Type</td>
		<td align="left" valign="bottom" class="table1_item header">Address</td>
		<td align="left" valign="bottom" class="table1_item header">City</td>
		<td align="left" valign="bottom" class="table1_item header">State</td>
		<td align="left" valign="bottom" class="table1_item header">Zip</td>
		<td mytype="comma_numeric" align="right" valign="bottom" nowrap="" class="table1_item header">Percent Owned</td>
	</tr>

	<xsl:for-each select="PropertyInfo">
	<tr>
		<td align="left" class="data"><xsl:value-of select="PropertyName" disable-output-escaping="yes"/></td>
		<td valign="top" class="data"><xsl:value-of select="KeyPropertyType" disable-output-escaping="yes" /></td>
		<td valign="top" class="data"><xsl:value-of select="Address" disable-output-escaping="yes"/></td>
		<td valign="top" class="data"><xsl:value-of select="City"/></td>
		<td valign="top" class="data"><xsl:value-of select="State"/></td>
		<td valign="top" class="data"><xsl:value-of select="Zip"/></td>
		<td valign="top" class="data" align="right"><xsl:value-of select="format-number(PropertyPercentOwned,'#,##0.00','data')"/></td>
	</tr>
	</xsl:for-each>

	</table>
</xsl:template>


<xsl:template name="PlantData3">
	<table class="sortable table1 table1_item" id="plantdata" cellspacing="0" cellpadding="2" width="100%">
	<tr>
		<td align="left" valign="bottom" class="table1_item header">Power Plant</td>
		<td align="left" valign="bottom" class="table1_item header">Plant Type</td>
		<td align="left" valign="bottom" class="table1_item header">Region</td>
		<td align="left" valign="bottom" class="table1_item header">Sub Region</td>
		<td align="left" valign="bottom" class="table1_item header">Electric Generation Status</td>
		<td align="left" valign="bottom" class="table1_item header">Operating Status</td>
		<td mytype="comma_numeric" align="right" valign="bottom" nowrap="" class="table1_item header">Generating Capacity</td>
	</tr>

	<xsl:for-each select="PlantInfo">
	<tr>
		<td valign="top" class="data"><xsl:value-of select="PowerPlant" disable-output-escaping="yes"/></td>
		<td valign="top" class="data"><xsl:value-of select="KeyEnergyPlantType"/></td>
		<td valign="top" class="data"><xsl:value-of select="KeyNercRegion"/></td>
		<td valign="top" class="data"><xsl:value-of select="KeyNercSubRegion"/></td>
		<td valign="top" class="data"><xsl:value-of select="KeyElectricGenerationStatus"/></td>
		<td valign="top" class="data"><xsl:value-of select="KeyPowerPlantOperatingStatus"/></td>
		<td valign="top" class="data" align="right"><xsl:value-of select="format-number(GeneratingCapacity,'#,##0.00','data')"/></td>
	</tr>
	</xsl:for-each>

	</table>
</xsl:template>



<!-- END TEMPLATE3-->


<xsl:template match="irw:BranchMap">
<xsl:variable name="NameTemplate" select="/irw:IRW/*/Company/URL"/> 
<xsl:variable name="TemplateName" select="Company/TemplateName"/> 
  <style type="text/css">   
     @import url("IRStyle.css");
  </style>
<div id="outer" class="Branchmap">
  <xsl:choose>
    <!-- template1-->
    <xsl:when test="$TemplateName = 'AltBranchMap1' or contains($NameTemplate, 'template=1')">
      <xsl:call-template name="TemplateONEstylesheet" />
      <table border="0" cellspacing="0" cellpadding="3" width="100%" CLASS="data">
        <xsl:call-template name="Title_S2"/>   
        <TR ALIGN="CENTER">
          <td CLASS="leftTOPbord" colspan="2">            
            <form id="branchmapform" name="branchmapform" method="post">
				<fieldset>
					<legend>Branchfrom</legend>				
                <xsl:call-template name="BranchMapDataTable" />
              <table width="100%" border="0" cellspacing="0" cellpadding="3" class="data">
                <tr>
                  <td valign="top" align="center">
                    <xsl:call-template name="BuildMap"/>
                  </td>
                </tr>
                <tr><td vAlign="top" align="center">&#160;</td></tr>         
				<xsl:if test="(count(BranchInfo) &gt; 0 or count(PropertyInfo) &gt; 0 or count(PlantInfo) &gt; 0) and PropertyMapOptions/DisplayPropertyAttributes = 'true'">
                <tr>
                  <td valign="top">
                    <xsl:if test="count(BranchInfo) &gt; 0 and PropertyMapOptions/DisplayPropertyAttributes = 'true'">
                      <xsl:call-template name="BranchData5" />
                    </xsl:if>
                    <xsl:if test="count(PropertyInfo) &gt; 0 and PropertyMapOptions/DisplayPropertyAttributes = 'true'">
                      <xsl:call-template name="PropertyData5" />
                    </xsl:if>
                    <xsl:if test="count(PlantInfo) &gt; 0 and PropertyMapOptions/DisplayPropertyAttributes = 'true'">
                      <xsl:call-template name="PlantData5" />
                    </xsl:if>
                  </td>
                </tr>
				</xsl:if>
              </table>
				</fieldset>
            </form>
            <xsl:call-template name="FooterTag3"/>
          </td>
        </TR>
      </table>
    </xsl:when>
    <!-- template3-->
    <xsl:when test="$TemplateName = 'AltBranchMap3' or contains($NameTemplate, 'template=3')">
      <xsl:call-template name="TemplateTHREEstylesheet"/>
      <xsl:call-template name="Title_T3"/>
      <TABLE border="0" cellspacing="0" cellpadding="3" width="100%" class="table2">      
        <TR ALIGN="CENTER">
          <TD CLASS="data" valign="top">
            <form id="branchmapform" name="branchmapform" method="post">
				<fieldset>
					<legend>Branchfrom</legend>
					<xsl:call-template name="BranchMapDataTable" />
					<table width="100%" border="0" cellspacing="0" cellpadding="3" class="data">
						<tr>
							<td vAlign="top" align="center">
								<xsl:call-template name="BuildMap"/>
							</td>
						</tr>
						<tr>
							<td vAlign="top" align="center">&#160;</td>
						</tr>
						<xsl:if test="(count(BranchInfo) &gt; 0 or count(PropertyInfo) &gt; 0 or count(PlantInfo) &gt; 0) and PropertyMapOptions/DisplayPropertyAttributes = 'true'">
							<tr>
								<td valign="top">
									<xsl:if test="count(BranchInfo) &gt; 0 and PropertyMapOptions/DisplayPropertyAttributes = 'true'">
										<xsl:call-template name="BranchData5" />
									</xsl:if>
									<xsl:if test="count(PropertyInfo) &gt; 0 and PropertyMapOptions/DisplayPropertyAttributes = 'true'">
										<xsl:call-template name="PropertyData5" />
									</xsl:if>
									<xsl:if test="count(PlantInfo) &gt; 0 and PropertyMapOptions/DisplayPropertyAttributes = 'true'">
										<xsl:call-template name="PlantData5" />
									</xsl:if>
								</td>
							</tr>
						</xsl:if>
					</table>
				</fieldset>
					</form>
            <xsl:call-template name="FooterTag3"/>
          </TD>
        </TR>
      </TABLE>
    </xsl:when>

    <!-- Default template -->
    <xsl:otherwise>
      <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" CLASS="colordark">
        <xsl:call-template name="Title_S1" />
        <TR ALIGN="CENTER">
          <TD CLASS="colordark" colspan="2" valign="top">
            <form id="branchmapform" name="branchmapform" method="post">
				<fieldset>
					<legend>Branchfrom</legend>
					<xsl:call-template name="BranchMapDataTable" />
					<table width="100%" border="0" cellspacing="0" cellpadding="3" class="colorlight">
						<tr>
							<td vAlign="top" align="center">
								<xsl:call-template name="BuildMap" />
							</td>
						</tr>
						<tr>
							<td vAlign="top" align="center">&#160;</td>
						</tr>
						<xsl:if test="(count(BranchInfo) &gt; 0 or count(PropertyInfo) &gt; 0 or count(PlantInfo) &gt; 0) and PropertyMapOptions/DisplayPropertyAttributes = 'true'">
							<tr>
								<td valign="top">
									<table id="Propertytable" border="0" cellspacing="0" cellpadding="0" class="header">
										<tr>
											<td valign="top">
												<xsl:if test="count(BranchInfo) &gt; 0 and PropertyMapOptions/DisplayPropertyAttributes = 'true'">
													<xsl:call-template name="BranchData5" />
												</xsl:if>
												<xsl:if test="count(PropertyInfo) &gt; 0 and PropertyMapOptions/DisplayPropertyAttributes = 'true'">
													<xsl:call-template name="PropertyData5" />
												</xsl:if>
												<xsl:if test="count(PlantInfo) &gt; 0 and PropertyMapOptions/DisplayPropertyAttributes = 'true'">
													<xsl:call-template name="PlantData5" />
												</xsl:if>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</xsl:if>
					</table>
				</fieldset>
					</form>
          </TD>          
        </TR>
        <xsl:call-template name="FooterTag" />
      </TABLE>
    </xsl:otherwise>
  </xsl:choose>
</div>
</xsl:template>




    <xsl:template name="BuildMap">
        <table id="Table2" CellPadding="0" CellSpacing="0" BorderWidth="0" border="0" width="100%" height="480">
            <tr valign="top">
                <td valign="top" align="center" nowrap="1">
                    <table id="Table3" CellPadding="0" CellSpacing="0" BorderWidth="0" border="0" width="540" class="branchmap">
                        <tr>
                            <td valign="top" align="left">
                                <xsl:value-of select="Map/ImgSrc" disable-output-escaping="yes"/>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr><td valign="top" align="left" class="bottomSpace">&#160;</td></tr>
        </table>
    </xsl:template>


  <xsl:template name="BranchMapDataTable">
    <xsl:call-template name="CommonScript_4" />
	  <xsl:if test="not(contains(translate(Company/URL,$ucletters,$lcletters), 'print=1'))">		
			<script language="javascript" type="text/javascript">
			  <![CDATA[
			  
				var $pmap = jQuery.noConflict();
				var oTable;
			   function drawmap(Query, count, id) 
				 {                     
						   var widgets = dijit.findWidgets(BBmap);
						   dojo.forEach(widgets, function(w) {               
							  w.destroyRecursive(true);
						   });          
							 
						   dojo.empty(BBmap);
						   var label= $pmap(".legendItem").html().replace(/(\([^)]+\))/,'(' + count +')');
							$pmap(".legendItem").html(label);
           
               var layerQuery = BankBranchMap.DynamicLayers;        
            
						   var proptype="";                  
						   switch(layerQuery[0]["LayerIndex"])
						   {
								case 13: 
								   proptype = "KeyProp";                           
									break;
								case 5:
									proptype = "BRANCH_ID";                           
									break;
								case 14:
								   proptype = "KeyPowerPlant";                          
								   break;
							}
							
							layerQuery[0]["LayerQuery"]= "KeyInstn = " + ]]><xsl:value-of select="Company/KeyInstn"/><![CDATA[ + " and "+ proptype +" in (" + Query + ")";                   
						
							BankBranchMap.loadMap();
             
                ]]><xsl:if test="PropertyMapOptions/MapPointSelectable">
                  <![CDATA[
                
              if(id != undefined && ]]><xsl:value-of select="PropertyMapOptions/MapPointSelectable"/><![CDATA[ == true){
                  BankBranchMap.HighlightMap("and "+ proptype +" = " +id );
              }
                   ]]>
            </xsl:if> <![CDATA[
			
			updateScrollbar(); //Reset Horizontal Vertical scrollbar
      }
      function fnSearch()
      {
      var Key="";
      var count = 0;
      var id;

      $pmap(".KeyItem").each(function () {
      if (this.parentNode.className.toLowerCase().indexOf("row_selected") >= 0)
      {
      id = $pmap.trim($pmap(this).html());
      }
      Key =  $pmap.trim($pmap(this).html()) + ',' + Key;
      count++;
      });	  
      Key = Key.substring(0, Key.length - 1);
      drawmap(Key, count, id);
      }

      function HighlightGrid(features, layer)
      {

      switch(layer)
      {
      case "13":

      $pmap.each(features, function(index, value) {
      var loopbrk;
      $pmap(".KeyItem").each(function () {
      if($pmap(this).text() == value.attributes.KeyProp){
      $pmap("#propertydata tr").removeClass('row_selected');
      $pmap(this).parent().addClass('row_selected');
      $pmap("DIV.dataTables_scrollBody").mCustomScrollbar("scrollTo",".row_selected");
      loopbrk=false;
      return loopbrk;
      }
      });
      return loopbrk;
      });
      break;

      case "5":

      $pmap.each(features, function(index, value) {
      var loopbrk;
      $pmap(".KeyItem").each(function () {
      if($pmap(this).text() == value.attributes.BRANCH_ID){
      $pmap("#propertydata tr").removeClass('row_selected');
      $pmap(this).parent().addClass('row_selected');
      $pmap("DIV.dataTables_scrollBody").mCustomScrollbar("scrollTo",".row_selected");
      loopbrk=false;
      return loopbrk;
      }
      });
      return loopbrk;
      });
      break;

      case "14":

      $pmap.each(features, function(index, value) {
      var loopbrk;
      $pmap(".KeyItem").each(function () {
      if($pmap(this).text() == value.attributes.KeyPowerPlant){
      $pmap("#propertydata tr").removeClass('row_selected');
      $pmap(this).parent().addClass('row_selected');
      $pmap("DIV.dataTables_scrollBody").mCustomScrollbar("scrollTo",".row_selected");
      loopbrk=false;
      return loopbrk;
      }
      });
      return loopbrk;
      });
      break;
      }

      }
      ]]>
			</script>
	
		<xsl:if test="(count(BranchInfo) &gt; 0 or count(PropertyInfo) &gt; 0 or count(PlantInfo) &gt; 0) and PropertyMapOptions/DisplayPropertyAttributes = 'true'">
			<script language="javascript" type="text/javascript" src="{$ScriptPath}/datatables/jquery.dataTables.min.js"></script>
			<script language="javascript" type="text/javascript">        
				<![CDATA[ 		
				var mapPointClickable, mapWidth, MapHight;
				$pmap(document).ready(function() { 
					$pmap("#MapdataTables, #Propertytable").css("visibility","hidden");
            mapPointClickable = ']]><xsl:value-of select="PropertyMapOptions/MapPointSelectable"/><![CDATA[';
          
					mapWidth = $pmap("#outer").width() - 10;
					$pmap("#MapdataTables, #MapdataTables .display").width(mapWidth);	
          
         $pmap.fn.dataTableExt.oSort['currency-asc'] = function(a,b) {
           var x = a == "-" ? 0 : a.replace( /,/g, "" );
           var y = b == "-" ? 0 : b.replace( /,/g, "" );
           x = parseFloat( x );
           y = parseFloat( y );
           return x - y;
          };
           
          $pmap.fn.dataTableExt.oSort['currency-desc'] = function(a,b) {
           var x = a == "-" ? 0 : a.replace( /,/g, "" );
           var y = b == "-" ? 0 : b.replace( /,/g, "" );
           x = parseFloat( x );
           y = parseFloat( y );
           return y - x;
          };

					$pmap(window).load(function(){						
						MapHight = '200';
						var hMapData = $pmap('#propertydata tbody').outerHeight() + $pmap('#propertydata tbody tr').length - 1;
						   if ( hMapData < '200') {
								MapHight = hMapData
						   }		  
						var tableClass = $pmap("#propertydata").attr('class').split(' ').pop();	
						if(tableClass == "BranchData"){
							oTable = $pmap('#propertydata').dataTable({ "sScrollY": MapHight, "sScrollX": "100%", "bSort": true, "bLengthChange": false, "bStateSave": false, "bProcessing": false, "bInfo": false, "bPaginate": false, "aaSorting": [[ 1, "asc" ]] , "aoColumns": [ { "bSearchable": false}, null, null, null, null,  {"sType": "currency" }]});
						} else {
							 oTable = $pmap('#propertydata').dataTable({ "sScrollY": MapHight, "sScrollX": "100%", "bSort": true, "bLengthChange": false, "bStateSave": false, "bProcessing": false, "bInfo": false, "bPaginate": false, "aaSorting": [[ 1, "asc" ]],"aoColumns": [ { "bSearchable": false}, null, null, null, null, null, null, null]});					
						}
					  
					  $pmap("input#search_docs").bind("keyup",fnSearch);      
					  $pmap("#MapdataTables, #Propertytable").css("visibility","visible");
					  $pmap("#propertydata").css({"visibility":"visible","position":"relative", "bottom": "0px"});					  			
						if(tableClass == "BranchData"){
							$pmap("#propertydata.BranchData tbody tr td:nth-child(6)").each(function(){					
								$pmap(this).css({'padding-right' : '15px'});
							});
              
               $pmap("#propertydata tbody").click(function(event) {
		          $pmap(oTable.fnSettings().aoData).each(function (){			          
						    $pmap(this.nTr).removeClass('row_selected');
		          });
		          $pmap(event.target.parentNode).addClass('row_selected');  //.hide();            
				      var id = $pmap(event.target.parentNode).find('td.KeyItem').text(); 
              
              if(]]><xsl:value-of select="PropertyMapOptions/MapPointSelectable"/><![CDATA[ == true){
                  BankBranchMap.HighlightMap("and BRANCH_ID = " +id );
                }
	          });
            
						} else {
							$pmap("#propertydata tbody tr td:last-child").each(function(){					
								$pmap(this).css({'padding-right' : '15px'});
							});
              
               $pmap("#propertydata tbody").click(function(event) {
		          $pmap(oTable.fnSettings().aoData).each(function (){			          
						    $pmap(this.nTr).removeClass('row_selected');
		          });
		          $pmap(event.target.parentNode).addClass('row_selected');  //.hide();            
				      var id = $pmap(event.target.parentNode).find('td.KeyItem').text(); 
              if(]]><xsl:value-of select="PropertyMapOptions/MapPointSelectable"/><![CDATA[ == true){
                  BankBranchMap.HighlightMap("and KeyProp = " +id );
               }
	          });
						}
					  
						var Headercolor = $pmap("div.dataTables_scrollHeadInner table.display td:last").css("background-color");
						$pmap(".dataTables_scrollHead").css({
							'background-color' : Headercolor				
						});
					});
            
				});
			  ]]>
			  </script>        
			<xsl:call-template name="jQueryslimScroll" />
		</xsl:if>
	</xsl:if>
  </xsl:template>
  
  <xsl:template name="PropertyData5">
<xsl:variable name="NameTemplate" select="/irw:IRW/*/Company/URL"/>
<xsl:variable name="TemplateName" select="/irw:IRW/*/Company/TemplateName"/> 
	<div id="MapdataTables" class="MapdataTables">
	<xsl:choose>
		<xsl:when test="$TemplateName = 'AltBranchMap1' or contains($NameTemplate, 'template=1')"><xsl:attribute name="class">MapdataTables mapstyle1</xsl:attribute></xsl:when>
		<xsl:when test="$TemplateName = 'AltBranchMap3' or contains($NameTemplate, 'template=3')"><xsl:attribute name="class">MapdataTables mapstyle3</xsl:attribute></xsl:when>
		<xsl:otherwise><xsl:attribute name="class">MapdataTables mapstyle0 data</xsl:attribute></xsl:otherwise>
	</xsl:choose>       
	  <table id="propertydata" class="display PropertyData" border="0" cellspacing="0" cellpadding="3">
        <thead>
          <tr>
			<td valign="top" align="left" style="display:none">
				<span>Key</span>
			</td>
            <td valign="top" align="left" class="header">
				<xsl:choose>
					<xsl:when test="$TemplateName = 'AltBranchMap1' or contains($NameTemplate, 'template=1') or $TemplateName = 'AltBranchMap3' or contains($NameTemplate, 'template=3')"><xsl:attribute name="class">datashade defaultbold</xsl:attribute></xsl:when>
					<xsl:otherwise><xsl:attribute name="class">header</xsl:attribute></xsl:otherwise>
				</xsl:choose>
              <span>Property</span>
            </td>
            <td valign="top" align="left" class="header">
				<xsl:choose>
					<xsl:when test="$TemplateName = 'AltBranchMap1' or contains($NameTemplate, 'template=1') or $TemplateName = 'AltBranchMap3' or contains($NameTemplate, 'template=3')"><xsl:attribute name="class">datashade defaultbold</xsl:attribute></xsl:when>
					<xsl:otherwise><xsl:attribute name="class">header</xsl:attribute></xsl:otherwise>
				</xsl:choose>
              <span>Property Type</span>
            </td>
            <td valign="top" align="left" class="header">
				<xsl:choose>
					<xsl:when test="$TemplateName = 'AltBranchMap1' or contains($NameTemplate, 'template=1') or $TemplateName = 'AltBranchMap3' or contains($NameTemplate, 'template=3')"><xsl:attribute name="class">datashade defaultbold</xsl:attribute></xsl:when>
					<xsl:otherwise><xsl:attribute name="class">header</xsl:attribute></xsl:otherwise>
				</xsl:choose>
              <span>Address</span>
            </td>
            <td valign="top" align="left" class="header">
				<xsl:choose>
					<xsl:when test="$TemplateName = 'AltBranchMap1' or contains($NameTemplate, 'template=1') or $TemplateName = 'AltBranchMap3' or contains($NameTemplate, 'template=3')"><xsl:attribute name="class">datashade defaultbold</xsl:attribute></xsl:when>
					<xsl:otherwise><xsl:attribute name="class">header</xsl:attribute></xsl:otherwise>
				</xsl:choose>
              <span>City</span>
            </td>
            <td valign="top" align="left" class="header">
				<xsl:choose>
					<xsl:when test="$TemplateName = 'AltBranchMap1' or contains($NameTemplate, 'template=1') or $TemplateName = 'AltBranchMap3' or contains($NameTemplate, 'template=3')"><xsl:attribute name="class">datashade defaultbold</xsl:attribute></xsl:when>
					<xsl:otherwise><xsl:attribute name="class">header</xsl:attribute></xsl:otherwise>
				</xsl:choose>
              <span>State</span>
            </td>
            <td valign="top" align="left" class="header">
				<xsl:choose>
					<xsl:when test="$TemplateName = 'AltBranchMap1' or contains($NameTemplate, 'template=1') or $TemplateName = 'AltBranchMap3' or contains($NameTemplate, 'template=3')"><xsl:attribute name="class">datashade defaultbold</xsl:attribute></xsl:when>
					<xsl:otherwise><xsl:attribute name="class">header</xsl:attribute></xsl:otherwise>
				</xsl:choose>
              <span>Zip</span>
            </td>
            <td valign="top" align="right" class="header">
				<xsl:choose>
					<xsl:when test="$TemplateName = 'AltBranchMap1' or contains($NameTemplate, 'template=1') or $TemplateName = 'AltBranchMap3' or contains($NameTemplate, 'template=3')"><xsl:attribute name="class">datashade defaultbold</xsl:attribute></xsl:when>
					<xsl:otherwise><xsl:attribute name="class">header</xsl:attribute></xsl:otherwise>
				</xsl:choose>
              <span>Percent Owned</span>
            </td>            
          </tr>
        </thead>
        <tbody>
          <xsl:for-each select="PropertyInfo">
            <tr>
              <xsl:if test="position() mod 2 = 0">
				<xsl:if test="not(contains(translate(Company/URL,$ucletters,$lcletters), 'print=1'))">
					<xsl:attribute name="class">even</xsl:attribute>
				</xsl:if>                
              </xsl:if>
              <td valign="top" class="KeyItem" align="right" style="display:none">
                <xsl:value-of select="KeyBranch"/>
              </td>
              <td valign="top" align="left">
                <xsl:value-of select="PropertyName" disable-output-escaping="yes"/>
              </td>
              <td valign="top" align="left">
                <xsl:value-of select="KeyPropertyType" disable-output-escaping="yes" />
              </td>
              <td valign="top" align="left">
                <xsl:value-of select="Address" disable-output-escaping="yes" />
              </td>
              <td valign="top" align="left">
                <xsl:value-of select="City"/>
              </td>
              <td valign="top" align="left">
                <xsl:value-of select="State"/>
              </td>
              <td valign="top" align="left">
                <xsl:value-of select="Zip"/>
              </td>
              <td valign="top" align="right">
                <xsl:value-of select="format-number(PropertyPercentOwned,'#,##0.00','data')"/>
              </td>              
            </tr>
          </xsl:for-each>
        </tbody>
      </table>
   
    
    </div>
  </xsl:template>



  <xsl:template name="BranchData5">
<xsl:variable name="NameTemplate" select="/irw:IRW/*/Company/URL"/>
<xsl:variable name="TemplateName" select="/irw:IRW/*/Company/TemplateName"/> 
	<div id="MapdataTables" class="MapdataTables">
	<xsl:choose>
		<xsl:when test="$TemplateName = 'AltBranchMap1' or contains($NameTemplate, 'template=1')"><xsl:attribute name="class">MapdataTables mapstyle1</xsl:attribute></xsl:when>
		<xsl:when test="$TemplateName = 'AltBranchMap3' or contains($NameTemplate, 'template=3')"><xsl:attribute name="class">MapdataTables mapstyle3</xsl:attribute></xsl:when>
		<xsl:otherwise><xsl:attribute name="class">MapdataTables mapstyle0 data</xsl:attribute></xsl:otherwise>
	</xsl:choose> 
      <table id="propertydata" class="display BranchData" border="0" cellspacing="0" cellpadding="3">
        <thead>
          <tr>
            <td valign="top" align="left" style="display:none" class="header">
				<xsl:choose>
					<xsl:when test="$TemplateName = 'AltBranchMap1' or contains($NameTemplate, 'template=1') or $TemplateName = 'AltBranchMap3' or contains($NameTemplate, 'template=3')"><xsl:attribute name="class">datashade defaultbold</xsl:attribute></xsl:when>
					<xsl:otherwise><xsl:attribute name="class">header</xsl:attribute></xsl:otherwise>
				</xsl:choose>
              <span>Key</span>
            </td>
            <td valign="top" align="left" class="header">
				<xsl:choose>
					<xsl:when test="$TemplateName = 'AltBranchMap1' or contains($NameTemplate, 'template=1') or $TemplateName = 'AltBranchMap3' or contains($NameTemplate, 'template=3')"><xsl:attribute name="class">datashade defaultbold</xsl:attribute></xsl:when>
					<xsl:otherwise><xsl:attribute name="class">header</xsl:attribute></xsl:otherwise>
				</xsl:choose>
              <span>Branch</span>
            </td>
            <td valign="top" align="left" class="header">
				<xsl:choose>
					<xsl:when test="$TemplateName = 'AltBranchMap1' or contains($NameTemplate, 'template=1') or $TemplateName = 'AltBranchMap3' or contains($NameTemplate, 'template=3')"><xsl:attribute name="class">datashade defaultbold</xsl:attribute></xsl:when>
					<xsl:otherwise><xsl:attribute name="class">header</xsl:attribute></xsl:otherwise>
				</xsl:choose>
              <span>Description</span>
            </td>
            <td valign="top" align="left" class="header">
				<xsl:choose>
					<xsl:when test="$TemplateName = 'AltBranchMap1' or contains($NameTemplate, 'template=1') or $TemplateName = 'AltBranchMap3' or contains($NameTemplate, 'template=3')"><xsl:attribute name="class">datashade defaultbold</xsl:attribute></xsl:when>
					<xsl:otherwise><xsl:attribute name="class">header</xsl:attribute></xsl:otherwise>
				</xsl:choose>
              <span>City</span>
            </td>
            <td valign="top" align="left" class="header">
				<xsl:choose>
					<xsl:when test="$TemplateName = 'AltBranchMap1' or contains($NameTemplate, 'template=1') or $TemplateName = 'AltBranchMap3' or contains($NameTemplate, 'template=3')"><xsl:attribute name="class">datashade defaultbold</xsl:attribute></xsl:when>
					<xsl:otherwise><xsl:attribute name="class">header</xsl:attribute></xsl:otherwise>
				</xsl:choose>
              <span>State</span>
            </td>
            <td valign="top" align="right" class="header">
				<xsl:choose>
					<xsl:when test="$TemplateName = 'AltBranchMap1' or contains($NameTemplate, 'template=1') or $TemplateName = 'AltBranchMap3' or contains($NameTemplate, 'template=3')"><xsl:attribute name="class">datashade defaultbold</xsl:attribute></xsl:when>
					<xsl:otherwise><xsl:attribute name="class">header</xsl:attribute></xsl:otherwise>
				</xsl:choose>
              <span>Deposits ($000)</span>
            </td>
            <!--<td valign="top" style="display:none" class="header">
				<xsl:choose>
					<xsl:when test="$TemplateName = 'AltBranchMap1' or contains($NameTemplate, 'template=1') or $TemplateName = 'AltBranchMap3' or contains($NameTemplate, 'template=3')"><xsl:attribute name="class">datashade defaultbold</xsl:attribute></xsl:when>
					<xsl:otherwise><xsl:attribute name="class">header</xsl:attribute></xsl:otherwise>
				</xsl:choose>           
            </td>
            <td valign="top" style="display:none" class="header">
				<xsl:choose>
					<xsl:when test="$TemplateName = 'AltBranchMap1' or contains($NameTemplate, 'template=1') or $TemplateName = 'AltBranchMap3' or contains($NameTemplate, 'template=3')"><xsl:attribute name="class">datashade defaultbold</xsl:attribute></xsl:when>
					<xsl:otherwise><xsl:attribute name="class">header</xsl:attribute></xsl:otherwise>
				</xsl:choose>            
            </td>-->      
           
          </tr>
        </thead>
        <tbody>
          <xsl:for-each select="BranchInfo">
            <tr>
              <xsl:if test="position() mod 2 = 0">
				        <xsl:if test="not(contains(translate(Company/URL,$ucletters,$lcletters), 'print=1'))">
					        <xsl:attribute name="class">even</xsl:attribute>
				        </xsl:if>                
              </xsl:if>
              <td class="KeyItem" align="right" valign="top" style="display:none">
                <xsl:value-of select="KeyBranch"/>
              </td>
              <td valign="top" align="left">
                <xsl:value-of select="BranchLevelName" disable-output-escaping="yes"/>
              </td>
              <td valign="top" align="left">
                <xsl:value-of select="Description"/>
              </td>
              <td valign="top" align="left">
                <xsl:value-of select="City"/>
              </td>
              <td valign="top" align="left">
                <xsl:value-of select="State"/>
              </td>
              <td align="right" valign="top">
                <xsl:value-of select="format-number(Deposits,'#,##0','data')"/>
              </td>
              <!--<td valign="top" style="display:none">              
              </td>
              <td valign="top" style="display:none">               
              </td>-->
            </tr>

          </xsl:for-each>
        </tbody>
      </table>
    
    </div>
  </xsl:template>




  <xsl:template name="PlantData5">
<xsl:variable name="NameTemplate" select="/irw:IRW/*/Company/URL"/>
<xsl:variable name="TemplateName" select="/irw:IRW/*/Company/TemplateName"/> 
	<div id="MapdataTables" class="MapdataTables">
	<xsl:choose>
		<xsl:when test="$TemplateName = 'AltBranchMap1' or contains($NameTemplate, 'template=1')"><xsl:attribute name="class">MapdataTables mapstyle1</xsl:attribute></xsl:when>
		<xsl:when test="$TemplateName = 'AltBranchMap3' or contains($NameTemplate, 'template=3')"><xsl:attribute name="class">MapdataTables mapstyle3</xsl:attribute></xsl:when>
		<xsl:otherwise><xsl:attribute name="class">MapdataTables mapstyle0 data</xsl:attribute></xsl:otherwise>
	</xsl:choose> 
      <table id="propertydata" class="display PlantData" border="0" cellspacing="0" cellpadding="3">
        <thead>
          <tr>
            <td valign="top" align="left" class="header">
				<xsl:choose>
					<xsl:when test="$TemplateName = 'AltBranchMap1' or contains($NameTemplate, 'template=1') or $TemplateName = 'AltBranchMap3' or contains($NameTemplate, 'template=3')"><xsl:attribute name="class">datashade defaultbold</xsl:attribute></xsl:when>
					<xsl:otherwise><xsl:attribute name="class">header</xsl:attribute></xsl:otherwise>
				</xsl:choose>
              <span>Power Plant</span>
            </td>
            <td valign="top" align="left" class="header">
				<xsl:choose>
					<xsl:when test="$TemplateName = 'AltBranchMap1' or contains($NameTemplate, 'template=1') or $TemplateName = 'AltBranchMap3' or contains($NameTemplate, 'template=3')"><xsl:attribute name="class">datashade defaultbold</xsl:attribute></xsl:when>
					<xsl:otherwise><xsl:attribute name="class">header</xsl:attribute></xsl:otherwise>
				</xsl:choose>
              <span>Plant Type</span>
            </td>
            <td valign="top" align="left" class="header">
				<xsl:choose>
					<xsl:when test="$TemplateName = 'AltBranchMap1' or contains($NameTemplate, 'template=1') or $TemplateName = 'AltBranchMap3' or contains($NameTemplate, 'template=3')"><xsl:attribute name="class">datashade defaultbold</xsl:attribute></xsl:when>
					<xsl:otherwise><xsl:attribute name="class">header</xsl:attribute></xsl:otherwise>
				</xsl:choose>
              <span>Region</span>
            </td>
            <td valign="top" align="left" class="header">
				<xsl:choose>
					<xsl:when test="$TemplateName = 'AltBranchMap1' or contains($NameTemplate, 'template=1') or $TemplateName = 'AltBranchMap3' or contains($NameTemplate, 'template=3')"><xsl:attribute name="class">datashade defaultbold</xsl:attribute></xsl:when>
					<xsl:otherwise><xsl:attribute name="class">header</xsl:attribute></xsl:otherwise>
				</xsl:choose>
              <span>Sub Region</span>
            </td>
            <td valign="top" align="left" class="header">
				<xsl:choose>
					<xsl:when test="$TemplateName = 'AltBranchMap1' or contains($NameTemplate, 'template=1') or $TemplateName = 'AltBranchMap3' or contains($NameTemplate, 'template=3')"><xsl:attribute name="class">datashade defaultbold</xsl:attribute></xsl:when>
					<xsl:otherwise><xsl:attribute name="class">header</xsl:attribute></xsl:otherwise>
				</xsl:choose>
              <span>Electric Generation Status</span>
            </td>
            <td valign="top" align="left" class="header">
				<xsl:choose>
					<xsl:when test="$TemplateName = 'AltBranchMap1' or contains($NameTemplate, 'template=1') or $TemplateName = 'AltBranchMap3' or contains($NameTemplate, 'template=3')"><xsl:attribute name="class">datashade defaultbold</xsl:attribute></xsl:when>
					<xsl:otherwise><xsl:attribute name="class">header</xsl:attribute></xsl:otherwise>
				</xsl:choose>
              <span>Operating Status</span>
            </td>
            <td valign="top" align="right" class="header">
				<xsl:choose>
					<xsl:when test="$TemplateName = 'AltBranchMap1' or contains($NameTemplate, 'template=1') or $TemplateName = 'AltBranchMap3' or contains($NameTemplate, 'template=3')"><xsl:attribute name="class">datashade defaultbold</xsl:attribute></xsl:when>
					<xsl:otherwise><xsl:attribute name="class">header</xsl:attribute></xsl:otherwise>
				</xsl:choose>
              <span>Generating Capacity</span>
            </td>
            <td valign="top" align="right" style="display:none" class="header">
				<xsl:choose>
					<xsl:when test="$TemplateName = 'AltBranchMap1' or contains($NameTemplate, 'template=1') or $TemplateName = 'AltBranchMap3' or contains($NameTemplate, 'template=3')"><xsl:attribute name="class">datashade defaultbold</xsl:attribute></xsl:when>
					<xsl:otherwise><xsl:attribute name="class">header</xsl:attribute></xsl:otherwise>
				</xsl:choose>
              <span>Key</span>
            </td>
          </tr>
        </thead>
        <tbody>
        <xsl:for-each select="PlantInfo">
          <tr> 
              <xsl:if test="position() mod 2 = 0">
				        <xsl:if test="not(contains(translate(Company/URL,$ucletters,$lcletters), 'print=1'))">
					        <xsl:attribute name="class">even</xsl:attribute>
				        </xsl:if>                
              </xsl:if>	
            <td valign="top" align="left">
              <xsl:value-of select="PowerPlant" disable-output-escaping="yes"/>
            </td>
            <td valign="top" align="left">
              <xsl:value-of select="KeyEnergyPlantType"/>
            </td>
            <td valign="top" align="left">
              <xsl:value-of select="KeyNercRegion"/>
            </td>
            <td valign="top" align="left">
              <xsl:value-of select="KeyNercSubRegion"/>
            </td>
            <td valign="top" align="left">
              <xsl:value-of select="KeyElectricGenerationStatus"/>
            </td>
            <td valign="top" align="left">
              <xsl:value-of select="KeyPowerPlantOperatingStatus"/>
            </td>
            <td valign="top" align="right">
              <xsl:value-of select="format-number(GeneratingCapacity,'#,##0.00','data')"/>
            </td>
            <td class="KeyItem" align="right" valign="top" style="display:none">
              <xsl:value-of select="KeyBranch"/>
            </td>
          </tr>
        </xsl:for-each>
        </tbody>
      </table>
    
    </div>
  </xsl:template>


</xsl:stylesheet>


