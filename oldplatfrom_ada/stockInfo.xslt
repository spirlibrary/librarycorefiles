<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns="http://www.w3.ord/TR/REC-html40"
  xmlns:msxsl="urn:schemas-microsoft-com:xslt"
  xmlns:irw="http://www.snl.com/xml/irw">

  <xsl:output method="html" media-type="text/html" indent="no"/>
  <xsl:decimal-format name="data" NaN="NA"/>

  <!-- Start Default Template here-->

  <!--
//This is the default template. Most changes are to either remove the company name, ticker, or exchange. you can also change the name of the page or remove this row all together.
//These templates below are broken up how the page is displayed.

-->
  <xsl:template name="HEADER_Stock">
    <xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
    <TR>
      <TD CLASS="colordark" nowrap="">
        <SPAN CLASS="title1light">
          <xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/>
        </SPAN>
      </TD>
      <TD CLASS="colordark" NOWRAP="" align="right" valign="middle">
        <SPAN class="subtitlelight">
          <xsl:value-of select="$CompanyNameCaps"/>&#160;(<xsl:value-of select="Company/Exchange"/>&#160;-&#160;<xsl:value-of select="Company/Ticker"/>)&#160;
        </SPAN>
      </TD>
    </TR>
    <xsl:if test="Company/Header != ''">
      <TR class="default" align="left">
        <TD class="colorlight" colspan="2">
          <SPAN class="default">
            <xsl:value-of select="Company/Header" disable-output-escaping="yes"/>
          </SPAN>
        </TD>
      </TR>
    </xsl:if>
  </xsl:template>

  <xsl:template name="Selector_stock">
    <xsl:variable name="TradedIssuesCount" select="TradedIssues/RowCount"/>
    <xsl:choose>
      <xsl:when test="$TradedIssuesCount > 1">
        <TABLE BORDER="0" CELLSPACING="1" CELLPADDING="3">
          <TR>
            <TD CLASS="data">
              <SPAN class="defaultbold">Security:</SPAN>
            </TD>
            <TD CLASS="colorlight" align="left">
              <xsl:variable name="CurrentKeyFndg" select="Company/KeyFndg"/>
				<label class="visuallyhidden" for="KeyFndg">KeyFndg</label>
              <select NAME="KeyFndg" ID="KeyFndg" size="1" class="input">
                <xsl:for-each select="TradedIssues">
                  <xsl:value-of select="KeyFndg"/>
                  <option class="data">
                    <xsl:attribute name="value">
                      <xsl:value-of select="KeyFndg"/>
                    </xsl:attribute>

                    <xsl:if test="$CurrentKeyFndg=KeyFndg">
                      <xsl:attribute name="SELECTED">1</xsl:attribute>
                    </xsl:if>

                    <xsl:value-of select="TradingSymbol"/>
                  </option>
                </xsl:for-each>
              </select>
            </TD>
            <TD CLASS="colorlight" align="left">
              <input type="submit" NAME="submit" value="Apply" align="left" ID="Submit1"  class="submit"/>
            </TD>
          </TR>
        </TABLE>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <!-- REMOVE SELECTOR_STOCK FOR SL -->
  <xsl:template name="Descriptor_stock">
    <xsl:if test="TradedIssues/RowCount > 1">
      <TR>
        <TD CLASS="colorlight" colspan="3">
          <a>
            <xsl:attribute name="href">#</xsl:attribute>
            <xsl:attribute name="OnClick">
              window.open('fndgdesc.aspx?IID=<xsl:value-of select="Company/KeyInstn"/>', 'SecurityDescriptions', 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=400,height=300');
            </xsl:attribute>Select link to view descriptions of our traded funding issues.
          </a>
        </TD>
      </TR>
    </xsl:if>

  </xsl:template>
  <!-- REMOVE DESCRIPTOR_STOCK FOR SL -->




  <xsl:template name="Date_table_stock">
    <TABLE width="100%" cellpadding="3" cellspacing="1" border="0" CLASS="header">
      <TR>
        <TD class="header">
          Enter a date below to request the
          closing price for that specific day:
        </TD>
      </TR>
      <TR align="center" VALIGN="middle">
        <TD CLASS="data" height="80">
          <input type="text" NAME="date" size="12" ID="snlstock" class="input">
            <xsl:attribute name="onkeydown">
              KeydownStock(<xsl:value-of select="Company/KeyInstn"/>,<xsl:value-of select="Company/KeyFndg"/>, event);
            </xsl:attribute>
          </input>&#160;

          <input type="button" value="Enter" id="Button1" name="submit1" class="submit">
            <xsl:attribute name="OnClick">
              window.open('quotepop.aspx?IID=<xsl:value-of select="Company/KeyInstn"/>&amp;KeyFndg=<xsl:value-of select="Company/KeyFndg"/>&amp;date=' + document.getElementById('snlstock').value, 'ClosingPrice', 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=60')
            </xsl:attribute>
          </input>
          <br />
          <SPAN class="default">Format: MM/DD/YY</SPAN>
        </TD>
      </TR>
    </TABLE>
  </xsl:template>





  <xsl:template name="Chart_Drop_stock">
    <TR>
      <TD valign="top" colspan="3" CLASS="header">
        <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="2" WIDTH="100%" CLASS="header">
          <xsl:call-template name="Chart_Drop_Image_stock"/>
          <!-- <xsl:call-template name="Chart_Drop_Down_stock"/> -->
        </TABLE>
      </TD>
    </TR>
    <TR>
      <TD>
        <IMG SRC="/images/Interactive/blank.gif" WIDTH="8" HEIGHT="4" alt=""/>
      </TD>
      <TD>
        <IMG SRC="/images/Interactive/blank.gif" WIDTH="2" HEIGHT="8" alt=""/>
      </TD>
      <TD>
        <IMG SRC="/images/Interactive/blank.gif" WIDTH="8" HEIGHT="4" alt=""/>
      </TD>
    </TR>
  </xsl:template>

  <xsl:template name="Chart_Drop_Image_stock">
    <xsl:variable name="isPreview" select="Company/IsPreview" />
    <TR align="left">
      <TD CLASS="header" valign="TOP">Current Chart</TD>
    </TR>
    <TR align="left">
      <TD CLASS="data" valign="TOP" align="center" style="padding-top: 10px;">
        <div id="graphContainer"></div>
        <xsl:choose>
          <xsl:when test="contains(Company/URL, 'print=1') or contains(Company/URL, 'Print=1')">
            <xsl:variable name="KeyInstn" select="Company/KeyInstn" />
            <xsl:variable name="KeyFndg" select="Company/KeyFndg" />
            <xsl:variable name="StartDate" select="PeriodDates[KeyPeriod = ../GraphOptions/Period]/StartDate" />
            <xsl:variable name="EndDate" select="PeriodDates[KeyPeriod = ../GraphOptions/Period]/EndDate" />
            <img>
              <xsl:attribute name="src">
                StockChartImage.aspx?iid=<xsl:value-of select="$KeyInstn"/>&#38;KeyFndg=<xsl:value-of select="$KeyFndg"/>&#38;StartDate=<xsl:value-of select="$StartDate"/>&#38;EndDate=<xsl:value-of select="$EndDate"/>
              </xsl:attribute>
            </img>
          </xsl:when>
          <xsl:otherwise>
            <img id="graph" style="display:none;"  alt=""></img>
            <SPAN id="errorMsg" style="display:none;">The requested graph could not be constructed. Some data may not be available.  Please try changing the period or deselecting options.</SPAN>
            <embed id="LoadAnim" src="images/graphing/loading.swf" wmode="transparent" width="75" height="75" />
          </xsl:otherwise>
        </xsl:choose>
      </TD>
    </TR>
    <xsl:call-template name="ChartOptions"/>

  </xsl:template>

  <!-- updated for moving the download option in new row instead of column -->

  <xsl:template name="ChartOptions">
    <TR>
      <TD  align="middle" class="data">


        <TABLE border="0" width="100%" cellpadding="3" cellspacing="3">
          <TR align="left">
            <TD valign="top" colspan="2">

              <xsl:call-template name="UpdateChart"/>

            </TD>
          </TR>


          <!-- Updated Code -->
          <xsl:if test="contains(GraphComponents/AllowDataDownload,'True')">
            <TR align="left">
              <TD valign="top"  class="header">Downloads</TD>
            </TR>
            <TR align="left" class="data">
              <TD valign="middle">

                <xsl:call-template name="DownloadHistData"/>
                <xsl:if test="contains(GraphComponents/AllowDataDownload,'False')">&#160;</xsl:if>

              </TD>
            </TR>
            <TR align="left" class="data">
              <TD valign="middle">

                <xsl:call-template name="DownloadChart"/>
                <xsl:if test="contains(GraphOptions/Small,'False') or contains(GraphOptions/Medium,'False') or contains(GraphOptions/Large,'False')">&#160;</xsl:if>

              </TD>
            </TR>
          </xsl:if>
        </TABLE>
      </TD>
    </TR>
  </xsl:template>


  <xsl:template name="CloseTR">
    <xsl:text disable-output-escaping="yes">&lt;/TR&gt;</xsl:text>
  </xsl:template>

  <xsl:template name="OpenTR">
    <xsl:text disable-output-escaping="yes">&lt;TR&gt;</xsl:text>
  </xsl:template>

  <xsl:template name="DownloadChart">
    <xsl:variable name="peerCount" select="PeerCount/Count"/>
    <xsl:variable name="indexCount" select="IndexCount/Count"/>
    <xsl:variable name="keyInstn" select="Company/KeyInstn"/>
    <xsl:variable name="keyFndg" select="Company/KeyFndg"/>
    <xsl:variable name="isPreview" select="Company/IsPreview" />

    <TABLE border="0" cellpadding="3" cellspacing="0">
      <xsl:if test="contains(GraphOptions/Small,'True') or contains(GraphOptions/Medium,'True') or contains(GraphOptions/Large,'True')">
        <form name="downloadChartOptions" method="POST" action="StockInfoIFrame.aspx" target="_blank">
          <TR>
            <TD class="defaultbold">
              Chart Size:
			  <label class="visuallyhidden" for="downloadSize">downloadSize</label>
              <select name="downloadSize" id="downloadSize" class="input">
                <xsl:if test="contains(GraphOptions/Small,'True')">
                  <option value="1" class="data">Small</option>
                </xsl:if>
                <xsl:if test="contains(GraphOptions/Medium,'True')">
                  <option value="2" class="data">Medium</option>
                </xsl:if>
                <xsl:if test="contains(GraphOptions/Large,'True')">
                  <option value="3" class="data">Large</option>
                </xsl:if>
              </select>
            </TD>
            <TD class="defaultbold" valign="middle">

              <button id="ChartDownload" type="button">Download</button>
            </TD>
          </TR>
          <xsl:if test="contains($isPreview, 'true')">
            <input type="hidden" name="preview" value="1"/>
          </xsl:if>

          <input type="hidden" name="KeyInstn">
            <xsl:attribute name="value">
              <xsl:value-of select="$keyInstn"/>
            </xsl:attribute>
          </input>

          <input type="hidden" name="KeyFndg">
            <xsl:attribute name="value">
              <xsl:value-of select="$keyFndg"/>
            </xsl:attribute>
          </input>

          <xsl:for-each select="Peers">
            <xsl:variable name="name" select="FormName"/>

            <input type="hidden" value="">
              <xsl:attribute name="name">
                <xsl:value-of select="concat('_','',$name)"/>
              </xsl:attribute>

              <xsl:attribute name="id">
                <xsl:value-of select="concat('_','',$name)"/>
              </xsl:attribute>
            </input>
          </xsl:for-each>

          <xsl:for-each select="CompIndex">
            <xsl:variable name="name" select="FormName"/>
            <input type="hidden" value="">
              <xsl:attribute name="name">
                <xsl:value-of select="concat('_','',$name)"/>
              </xsl:attribute>

              <xsl:attribute name="id">
                <xsl:value-of select="concat('_','',$name)"/>
              </xsl:attribute>
            </input>
          </xsl:for-each>

          <input type="hidden" name="Peers">
            <xsl:attribute name="value">
              <xsl:value-of select="$peerCount"/>
            </xsl:attribute>
          </input>

          <input type="hidden" name="Indexes">
            <xsl:attribute name="value">
              <xsl:value-of select="$indexCount"/>
            </xsl:attribute>
          </input>
          <input type="hidden" name="_period" id="_period" value=""/>
        </form>
      </xsl:if>
    </TABLE>
  </xsl:template>
  <!-- End Download Chart Dropdown -->


  <!-- Download Historical Data Dropdown -->
  <xsl:template name="DownloadHistData">
    <xsl:variable name="keyInstn" select="Company/KeyInstn"/>
    <xsl:variable name="keyFndg" select="Company/KeyFndg"/>
    <xsl:variable name="isPreview" select="Company/IsPreview" />

    <TABLE border="0" cellpadding="3" cellspacing="0">
      <xsl:if test="contains(GraphComponents/AllowDataDownload,'True')">
        <form name="downloadDataOptions" method="POST" action="StockInfoIFrame.aspx" target="_blank">
          <TR align="left">
            <TD class="defaultbold" valign="middle" nowrap="">
              Historical Data Format:
			  <label class="visuallyhidden" for="downloadData">downloadData</label>
              <select name="downloadData" id="downloadData" class="input">
                <option value="html" class="data">HTML</option>
                <option value="csv" class="data">CSV</option>
              </select>
            </TD>
            <TD class="default" valign="middle">
              <input type="submit" value="Download" class="submit">
                <xsl:attribute name="onclick">
                  fillDownloadPeriod();
                </xsl:attribute>
              </input>
            </TD>
          </TR>
          <xsl:if test="contains($isPreview, 'true')">
            <input type="hidden" name="preview" value="1"/>
          </xsl:if>

          <input type="hidden" name="KeyInstn">
            <xsl:attribute name="value">
              <xsl:value-of select="$keyInstn"/>
            </xsl:attribute>
          </input>

          <input type="hidden" name="KeyFndg">
            <xsl:attribute name="value">
              <xsl:value-of select="$keyFndg"/>
            </xsl:attribute>
          </input>

          <input type="hidden" name="__period" id="__period" value=""/>
        </form>
      </xsl:if>
    </TABLE>
  </xsl:template>
  <!-- End Download Historical Data Dropdown -->

  <!-- Template 0 -->
  <xsl:template name="UpdateChart">
    <xsl:variable name="peerCount" select="PeerCount/Count"/>
    <xsl:variable name="indexCount" select="IndexCount/Count"/>
    <xsl:variable name="showPeers" select="GraphComponents/Peers"/>
    <xsl:variable name="keyInstn" select="Company/KeyInstn"/>
    <xsl:variable name="keyFndg" select="Company/KeyFndg"/>
    <xsl:variable name="showIndexes" select="GraphComponents/Indexes"/>
    <xsl:variable name="showSplits" select="GraphComponents/Splits"/>
    <xsl:variable name="showDivs" select="GraphComponents/Dividends"/>
    <xsl:variable name="showNews" select="GraphComponents/News"/>
    <xsl:variable name="showPRs" select="GraphComponents/PressReleases"/>
    <xsl:variable name="showFilings" select="GraphComponents/Filings"/>
    <xsl:variable name="showDocs" select="GraphComponents/Documents"/>
    <xsl:variable name="showDivYield" select="GraphComponents/DividendYield"/>
    <xsl:variable name="showReturn" select="GraphComponents/TotalReturn"/>
    <xsl:variable name="isPreview" select="Company/IsPreview" />
    <xsl:variable name="defaultPeriod" select="GraphOptions/Period" />
    <xsl:variable name="endDate" select="PeriodDates/EndDate" />

    <TABLE CELLPADDING="0" CELLSPACING="0" BORDER="0" WIDTH="100%">
      <form name="chartOptions" id="chartOptions"  method="POST">
        <!--action="StockInfoIFrame.aspx" target="theIFrame">-->
        <xsl:attribute name="onsubmit">
          expandIFrame(<xsl:value-of select="GraphSize/Width"/>,<xsl:value-of select="GraphSize/Height + 85"/>,<xsl:value-of select="$peerCount"/>,<xsl:value-of select="$indexCount"/>),document.getElementById("submitChart").disabled=true;
        </xsl:attribute>

        <TR>
          <TD valign="top">
            <TABLE ID="PeriodSelect" cellpadding="3" cellspacing="6" border="0" width="100%">
              <TR>
                <TD align="left" class="data" colspan="2">
                  <SPAN CLASS="defaultbold">Period: </SPAN>
					<label class="visuallyhidden" for="period">period</label>
                  <select id="period" name="period" class="input" >

                    <xsl:for-each select="PeriodDates">
                      <option value="{StartDate}">
                        <xsl:if test="$defaultPeriod = KeyPeriod">
                          <xsl:attribute name="Selected">1</xsl:attribute>
                        </xsl:if>
                        <xsl:value-of select="Desc"/>
                      </option>
                    </xsl:for-each>


                  </select><SPAN style="padding-left: 25px" class="defaultbold">Select Chart Type:</SPAN>&#32;<label class="visuallyhidden" for="TypeSelect">TypeSelect</label>
					  <select class="input" id="TypeSelect">
                    <option selected="selected" value="Area">Area</option>
                    <option value="Line">Line</option>
                    <option value="Column">Column</option>
                    <!--<option value="CandleStick">Candlestick</option>-->
                  </select>
                </TD>

                <xsl:if test="contains($isPreview, 'true')">
                  <input type="hidden" name="preview" value="1"/>
                </xsl:if>

                <input type="hidden" name="KeyInstn">
                  <xsl:attribute name="value">
                    <xsl:value-of select="$keyInstn"/>
                  </xsl:attribute>
                </input>
                <input type="hidden" name="KeyFndg">
                  <xsl:attribute name="value">
                    <xsl:value-of select="$keyFndg"/>
                  </xsl:attribute>
                </input>
                <input type="hidden" name="Peers">
                  <xsl:attribute name="value">
                    <xsl:value-of select="$peerCount"/>
                  </xsl:attribute>
                </input>
                <input type="hidden" name="Indexes">
                  <xsl:attribute name="value">
                    <xsl:value-of select="$indexCount"/>
                  </xsl:attribute>
                </input>

                <input type="hidden" id="EndDate1" name="EndDate1">
                  <xsl:attribute name="value">
                    <xsl:value-of select="$endDate"/>
                  </xsl:attribute>
                </input>


              </TR>
              <TR>
                <TD align="left" class="data" colspan="2">
                  <SPAN class="defaultbold">Start Date:&#32;</SPAN>
                  <input size="10" id="StartDate" type="text" class="input"/>
                  <SPAN style="padding-left: 10px" class="defaultbold">End Date:&#32;</SPAN>
                  <input size="10" id="EndDate2" type="text" class="input"/>
                </TD>
              </TR>
              <TR>
                <TD align="left" class="data" height="10px"></TD>
              </TR>
              <TR>
                <TD align="left" class="data">
                  <button class="submit" type="button" NAME="submitChart" ID="submitChart">Update Chart</button>
                </TD>
                <TD align="right" class="data">
                  <button class="submit" type="button" NAME="clearSelections" ID="clearSelections" onclick="this.form.reset()">Clear Selections</button>
                </TD>
              </TR>
            </TABLE>
          </TD>
        </TR>
        <TR align="left">
          <TD valign="top">

            <TABLE border="0" width="100%" cellspacing="0" cellpadding="1" class="colorlight">
              <TR>
                <!-- Peer checkboxes For Future release  -->
                <xsl:if test="contains($showPeers,'True')">
                  <TD valign="top" colspan="2">
                    <TABLE ID="PeerSelect" border="0" cellpadding="3" cellspacing="0" width="100%" class="colorlight">
                      <TR align="left">
                        <TD class="header" colspan="3" valign="middle">Select Peers</TD>
                      </TR>
                      <TR align="left">
                        <xsl:for-each select="Peers">
                          <xsl:variable name="instn" select="KeyInstnPeer"/>
                          <xsl:variable name="ticker" select="Ticker"/>
                          <xsl:variable name="name" select="FormName"/>
                          <xsl:variable name="fullPeerName" select="InstnName"/>
                          <xsl:choose>
                            <xsl:when test="position() mod 2 = 0">
                              <TD class="data" valign="middle">
                                <input type="checkbox">
                                  <xsl:attribute name="name">
                                    <xsl:value-of select="$name"/>
                                  </xsl:attribute>
                                  <xsl:attribute name="id">
                                    <xsl:value-of select="$name"/>
                                  </xsl:attribute>
                                  <xsl:attribute name="value">
                                    <xsl:value-of select="$instn"/>
                                  </xsl:attribute>
                                  <xsl:attribute name="onclick">
                                    clickCounter(<xsl:value-of select="$peerCount"/>,<xsl:value-of select="$indexCount"/>,this)
                                  </xsl:attribute>
                                </input>
                                <acronym>
                                  <xsl:attribute name="title">
                                    <xsl:value-of select="$fullPeerName"/>
                                  </xsl:attribute>
                                  &#160;<xsl:value-of select="Ticker"/>&#160;
                                </acronym>
                              </TD>
                              <xsl:call-template name="CloseTR"/>
                            </xsl:when>

                            <xsl:when test="position() mod 2 = 1">
                              <xsl:call-template name="OpenTR"/>
                              <TD  class="data" valign="middle">
                                <input type="checkbox">
                                  <xsl:attribute name="name">
                                    <xsl:value-of select="$name"/>
                                  </xsl:attribute>
                                  <xsl:attribute name="id">
                                    <xsl:value-of select="$name"/>
                                  </xsl:attribute>
                                  <xsl:attribute name="value">
                                    <xsl:value-of select="$instn"/>
                                  </xsl:attribute>
                                  <xsl:attribute name="onclick">
                                    clickCounter(<xsl:value-of select="$peerCount"/>,<xsl:value-of select="$indexCount"/>,this)
                                  </xsl:attribute>
                                </input>
                                <acronym>
                                  <xsl:attribute name="title">
                                    <xsl:value-of select="$fullPeerName"/>
                                  </xsl:attribute>
                                  &#160;<xsl:value-of select="Ticker"/>&#160;
                                </acronym>
                              </TD>
                            </xsl:when>
                            <xsl:otherwise>
                              <TD  class="data" valign="middle">
                                <input type="checkbox">
                                  <xsl:attribute name="name">
                                    <xsl:value-of select="$name"/>
                                  </xsl:attribute>
                                  <xsl:attribute name="id">
                                    <xsl:value-of select="$name"/>
                                  </xsl:attribute>
                                  <xsl:attribute name="value">
                                    <xsl:value-of select="$instn"/>
                                  </xsl:attribute>
                                  <xsl:attribute name="onclick">
                                    clickCounter(<xsl:value-of select="$peerCount"/>,<xsl:value-of select="$indexCount"/>,this)
                                  </xsl:attribute>
                                </input>
                                <acronym>
                                  <xsl:attribute name="title">
                                    <xsl:value-of select="$fullPeerName"/>
                                  </xsl:attribute>
                                  &#160;<xsl:value-of select="Ticker"/>&#160;
                                </acronym>
                              </TD>
                            </xsl:otherwise>
                          </xsl:choose>
                        </xsl:for-each>

                        <xsl:if test="not(peerCount mod 2 = 0)">
                          <xsl:call-template name="CloseTR"/>
                        </xsl:if>
                      </TR>
                    </TABLE>
                  </TD>
                </xsl:if>

                <!-- Index Check boxes -->
                <xsl:if test="contains($showIndexes,'True')">
                  <xsl:if test="$indexCount &gt; 0">
                    <TD valign="top">
                      <TABLE ID="IndexSelect" border="0" cellpadding="3" cellspacing="0" width="100%">
                        <TR align="left">
                          <TD class="header" valign="middle">Select Index</TD>
                        </TR>

                        <xsl:for-each select="CompIndex">
                          <xsl:variable name="index" select="KeyIndex"/>
                          <xsl:variable name="name" select="FormName"/>
                          <TR align="left">
                            <TD class="data" valign="middle">
                              <input type="checkbox">
                                <xsl:attribute name="name">
                                  <xsl:value-of select="$name"/>
                                </xsl:attribute>
                                <xsl:attribute name="id">
                                  <xsl:value-of select="$name"/>
                                </xsl:attribute>
                                <xsl:attribute name="value">
                                  <xsl:value-of select="$index"/>
                                </xsl:attribute>
                                <xsl:attribute name="onclick">
                                  clickCounter(<xsl:value-of select="$peerCount"/>,<xsl:value-of select="$indexCount"/>,this)
                                </xsl:attribute>
                              </input>
                              <xsl:value-of select="IndexShortName"/>
                            </TD>
                          </TR>
                        </xsl:for-each>
                      </TABLE>
                    </TD>
                  </xsl:if>
                </xsl:if>
                <!-- End Index Check boxes -->


                <!-- Splits Divs Filings Documents -->
                <xsl:if test="contains($showSplits,'True') or contains($showDivs,'True') or contains($showNews,'True') or contains($showPRs,'True') or contains($showFilings,'True') or contains($showDocs,'True')">
                  <TD valign="top">
                    <TABLE ID="EventSelector" border="0" cellpadding="3" cellspacing="0" width="100%">
                      <TR align="left">
                        <TD class="header" valign="middle">Select Events</TD>
                      </TR>

                      <xsl:if test="contains($showSplits,'True')">
                        <TR align="left">
                          <TD class="data" valign="middle">
                            <input type="checkbox" name="splits" value="Splits"/>
                            <IMG src="Images/graphing/leg_splits.gif"  alt="Splits"/> Splits
                          </TD>
                        </TR>
                      </xsl:if>

                      <xsl:if test="contains($showDivs,'True')">
                        <TR align="left">
                          <TD class="data" valign="middle">
                            <input type="checkbox" name="divs" value="Divs"/>
                            <IMG src="Images/graphing/leg_dividends.gif"  alt="Dividends"/> Dividends
                          </TD>
                        </TR>
                      </xsl:if>

                      <xsl:if test="contains($showNews,'True')">
                        <TR align="left">
                          <TD class="data" valign="middle">
                            <input type="checkbox" name="news" value="1"/>
                            <IMG src="Images/graphing/leg_newssummaries.gif"  alt="News Summaries"/> News Summaries
                          </TD>
                        </TR>
                      </xsl:if>

                      <xsl:if test="contains($showPRs,'True')">
                        <TR align="left">
                          <TD class="data" valign="middle">
                            <input type="checkbox" name="prs" value="PRs"  />
                            <IMG src="Images/graphing/leg_pressreleases.gif" alt="Press Releases"/> Press Releases
                          </TD>
                        </TR>
                      </xsl:if>

                      <xsl:if test="contains($showFilings,'True')">
                        <TR align="left">
                          <TD class="data" valign="middle">
                            <input type="checkbox" name="filings" value="Filings"/>
                            <IMG src="Images/graphing/leg_filings.gif" alt=" Filings"/> Filings
                          </TD>
                        </TR>
                      </xsl:if>

                      <xsl:if test="contains($showDocs,'True')">
                        <TR align="left">
                          <TD class="data" valign="middle">
                            <input type="checkbox" name="docs" value="Docs"/>
                            <IMG src="Images/graphing/leg_documents.gif" alt="Documents"/> Documents
                          </TD>
                        </TR>
                      </xsl:if>

                      <xsl:if test="contains($showSplits,'True') or contains($showDivs,'True') or contains($showNews,'True') or contains($showPRs,'True') or contains($showFilings,'True') or contains($showDocs,'True')">
                        <TR align="left">
                          <TD class="data" valign="middle">
                            <IMG src="Images/graphing/leg_multipleevents.gif" alt="Multiple Events"/> - Indicates Multiple Events
                          </TD>
                        </TR>
                      </xsl:if>
                    </TABLE>
                  </TD>
                </xsl:if>

                <!-- Performance Measures -->
                <TD valign="top">
                  <TABLE border="0" cellpadding="3" cellspacing="0" width="100%">

                    <xsl:if test="contains($showDivYield,'True') or contains($showReturn,'True')">
                      <TR align="left">
                        <TD class="header" valign="middle">Select Performance Measure</TD>
                      </TR>
                    </xsl:if>

                    <xsl:if test="contains($showDivYield,'True')">
                      <TR align="left">
                        <TD class="data" valign="middle">
                          <input id="DivYield" type="checkbox" name="divyield" value="1">
                            <xsl:attribute name="onclick">
                              clickCounter(<xsl:value-of select="$peerCount"/>,<xsl:value-of select="$indexCount"/>,this)
                            </xsl:attribute>
                          </input>
                          Dividend Yield (%)
                        </TD>
                      </TR>
                    </xsl:if>

                    <xsl:if test="contains($showReturn,'True')">
                      <TR align="left">
                        <TD class="data" valign="middle">
                          <input id="TotalReturn" type="checkbox" name="treturn" value="1">
                            <xsl:attribute name="onclick">
                              clickCounter(<xsl:value-of select="$peerCount"/>,<xsl:value-of select="$indexCount"/>,this)
                            </xsl:attribute>
                          </input>
                          Total Return (%)
                        </TD>
                      </TR>
                    </xsl:if>

                  </TABLE>
                </TD>
              </TR>



            </TABLE>

          </TD>

        </TR>
      </form>
    </TABLE>

  </xsl:template>

  <!-- End Template 0-->


  <xsl:template name="Chart_Drop_Down_stock"></xsl:template>

  <xsl:template name="Vol_Highlights_stock">
    <TABLE CELLSPACING="1" CELLPADDING="3" CLASS="header" WIDTH="100%" BORDER="0" ID="TABLE10">
      <TR VALIGN="bottom">
        <TD CLASS="header" WIDTH="40%">Volume Highlights</TD>
        <TD CLASS="header" align="center" WIDTH="28%" nowrap="">
          Avg Daily<br />Volume
        </TD>
        <TD CLASS="header" align="center" WIDTH="32%">
          %&#160;of&#160;Shares<br />Outstanding
        </TD>
      </TR>
      <TR>
        <TD CLASS="data" NOWRAP="" align="LEFT" width="40%">
          <SPAN class="defaultbold">One Day</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="VolumeHighlights/OneDayAvgDailyVolume"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="32%" align="right">
          <xsl:value-of select="format-number(VolumeHighlights/OneDayPercentSharesOutstanding,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
      </TR>
      <TR>
        <TD CLASS="data" NOWRAP="" align="LEFT" width="40%">
          <SPAN class="defaultbold">One Month</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="VolumeHighlights/OneMonthAvgDailyVolume"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="32%" align="right">
          <xsl:value-of select="format-number(VolumeHighlights/OneMonthPercentSharesOutstanding,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
      </TR>
      <TR>
        <TD CLASS="data" NOWRAP="" align="LEFT" width="40%">
          <SPAN class="defaultbold">Three Months</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="VolumeHighlights/ThreeMonthsAvgDailyVolume"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="32%" align="right">
          <xsl:value-of select="format-number(VolumeHighlights/ThreeMonthsPercentSharesOutstanding,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
      </TR>
      <TR>
        <TD CLASS="data" NOWRAP="" align="LEFT" width="40%">
          <SPAN class="defaultbold"> YTD</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="VolumeHighlights/YTDAvgDailyVolume"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="32%" align="right">
          <xsl:value-of select="format-number(VolumeHighlights/YTDPercentSharesOutstanding,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
      </TR>
      <TR>
        <TD CLASS="data" NOWRAP="" align="LEFT" width="40%">
          <SPAN class="defaultbold">One Year</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="VolumeHighlights/OneYearAvgDailyVolume"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="32%" align="right">
          <xsl:value-of select="format-number(VolumeHighlights/OneYearPercentSharesOutstanding,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
      </TR>
    </TABLE>
  </xsl:template>

  <xsl:template name="Fin_Data_stock">
    <TABLE BORDER="0" CELLSPACING="1" CELLPADDING="3" CLASS="header" WIDTH="100%" ID="TABLE11">
      <xsl:variable name="GAAPDomain" select="Company/GAAP"/>
      <xsl:variable name="INSURANCE_UNDERWRITER" select="8"/>
      <xsl:variable name="INSURANCE_BROKER" select="9"/>
      <xsl:variable name="THRIFT" select="2"/>
      <xsl:variable name="INVESTMENT_COMPANY" select="7"/>
      <xsl:variable name="REIT" select="6"/>
      <xsl:variable name="BANK" select="1"/>
      <xsl:variable name="ENERGY" select="10"/>
      <xsl:variable name="GAS" select="18"/>
      <xsl:variable name="FINTECH" select="15"/>
      <xsl:variable name="HOMEBUILDER" select="16"/>
      <xsl:variable name="GAMING" select="19"/>
      <xsl:variable name="COMMUNICATIONS" select="14"/>
      <xsl:variable name="MEDIA_ENT" select="20"/>
      <xsl:variable name="NEWMEDIA" select="21"/>
      <xsl:variable name="keyinstn" select="Company/KeyInstn"/>
      <xsl:choose>
        <xsl:when test="$GAAPDomain = $BANK or $GAAPDomain = $THRIFT">
          <TR VALIGN="TOP">
            <TD CLASS="header" NOWRAP="" WIDTH="60%" align="left">
              <br />Financial Data
            </TD>
            <TD CLASS="header" WIDTH="40%" valign="bottom" align="right">
              <xsl:value-of select="FinancialData/DateEnded"/>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EquityPerShareForCalcs', 1); return false" title="Book Value ($)">
                <SPAN CLASS="defaultbold">Book Value ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/BookValue,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'TangibleEquityPerShare', 1); return false" title="Tangible Book ($)">
                <SPAN CLASS="defaultbold">Tangible Book ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/TangibleBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSAfterExtraForCalcs', 0); return false" title="Quarter EPS ($)">
                <SPAN CLASS="defaultbold">Quarter EPS ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/QuarterEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSAfterExtraForCalcs', 0); return false" title="EPS ($)">
                <SPAN CLASS="defaultbold">
                  <xsl:value-of select="Company/FiscalPeriodDisplay"/> EPS ($)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/EPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'CoreEPS', 0); return false" title="Core EPS ($)">
                <SPAN CLASS="defaultbold">
                  <xsl:value-of select="Company/FiscalPeriodDisplay"/> Core EPS ($)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/CoreEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $INVESTMENT_COMPANY">
          <TR VALIGN="TOP">
            <TD CLASS="header" NOWRAP="" WIDTH="60%" align="left">
              <br />Financial Data
            </TD>
            <TD CLASS="header" WIDTH="40%" valign="bottom" align="right">
              <xsl:value-of select="FinancialData/DateEnded"/>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EquityPerShareForCalcs', 1); return false" title="Book Value ($">
                <SPAN CLASS="defaultbold">Book Value ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/BookValue,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'QuarterEPS', 1); return false" title="Quarter EPS ($)">
                <SPAN CLASS="defaultbold">Quarter EPS ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/QuarterEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSAfterExtraForCalcs', 0); return false" title="EPS ($)">
                <SPAN CLASS="defaultbold">
                  <xsl:value-of select="Company/FiscalPeriodDisplay"/> EPS ($)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/EPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $ENERGY or $GAAPDomain = $GAS">
          <TR VALIGN="TOP">
            <TD CLASS="header" NOWRAP="" WIDTH="60%" align="left">
              <br />Financial Data
            </TD>
            <TD CLASS="header" WIDTH="40%" valign="bottom" align="right">
              <xsl:value-of select="FinancialData/DateEnded"/>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'NetIncomeGrowth', 1); return false" title="Net Income Growth (%)">
                <SPAN CLASS="defaultbold">Net Income Growth (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/NetIncomeGrowth,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSBasic', 1); return false" title="Basic EPS ($)">
                <SPAN CLASS="defaultbold">Basic EPS ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/EPSBasic,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'DebtToTotalCapBookValue', 0); return false" title="Debt / Book Capitalization (%)">
                <SPAN CLASS="defaultbold">Debt / Book Capitalization (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/DebtToTotalCap,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'OperatingIncomeUtilityRpGrowth', 0); return false" title="Reported Net Operating Income Growth (%)">
                <SPAN CLASS="defaultbold">Reported Net Operating Income Growth (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/OperatingIncomeUtilityRpGrowth,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'OperatingAssetsEnergyGrowth', 0); return false" title="Operating Asset Growth (%)">
                <SPAN CLASS="defaultbold">Operating Asset Growth (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/OperatingAssetsEnergyGrowth,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $FINTECH">
          <TR VALIGN="TOP">
            <TD CLASS="header" NOWRAP="" WIDTH="60%" align="left">
              <br />Financial Data
            </TD>
            <TD CLASS="header" WIDTH="40%" valign="bottom" align="right">
              <xsl:value-of select="FinancialData/DateEnded"/>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},988, 1); return false" title="Diluted EPS after Extra ($)">
                <SPAN CLASS="defaultbold">Diluted EPS after Extra ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/EPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},10180, 1); return false" title="LTM EPS after Extra ($)">
                <SPAN CLASS="defaultbold">LTM EPS after Extra ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/LTMEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},9827, 0); return false" title="Book Value ($)">
                <SPAN CLASS="defaultbold">Book Value ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/BookValue,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},13487, 0); return false" title="Tangible Book Value per Share ($)">
                <SPAN CLASS="defaultbold">Tangible Book Value per Share ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/TangibleBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $REIT">
          <TR VALIGN="TOP">
            <TD CLASS="header" NOWRAP="" WIDTH="60%" align="left">
              <br />Quarterly Financial Data
            </TD>
            <TD CLASS="header" WIDTH="40%" valign="bottom" align="right">
              <xsl:value-of select="FinancialData/DateEnded"/>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'FFOPerShareForCalcs', 1); return false" title="FFO / Share ($)">
                <SPAN CLASS="defaultbold">FFO / Share ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/FFOPerShare,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'LTMFFOPerShare', 1); return false" title="FFO / Share ($)">
                <SPAN CLASS="defaultbold">
                  <xsl:value-of select="Company/FiscalPeriodDisplay"/> FFO / Share ($)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/LTMFFOPerShare,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'FADPerShare', 0); return false" title="FAD / Share ($)">
                <SPAN CLASS="defaultbold">FAD / Share ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/FADPerShare,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'FFOOperatingReported', 0); return false" title="Reported Operating FFO ($000)">
                <SPAN CLASS="defaultbold">Reported Operating FFO ($000)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="FinancialData/FFOOperatingReported"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'FFOOperatingReportedShare', 0); return false" title="Reported Operating FFO / Share ($)">
                <SPAN CLASS="defaultbold">Reported Operating FFO / Share ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/FFOOperatingReportedShare,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'SameStoreRevenueChange', 0); return false" title="Same-store NOI Change (%)">
                <SPAN CLASS="defaultbold">Same-store NOI Change (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/SameStoreRevenueChange,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'DebtToTotalCapFinl', 0); return false" title="Debt / Total Cap (%)">
                <SPAN CLASS="defaultbold">Debt / Total Cap (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/DebtToTotalCap,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $INSURANCE_BROKER or $GAAPDomain = $INSURANCE_UNDERWRITER">
          <TR VALIGN="TOP">
            <TD CLASS="header" NOWRAP="" WIDTH="60%" align="left">
              <br />Financial Data
            </TD>
            <TD CLASS="header" WIDTH="40%" valign="bottom" align="right">
              <xsl:value-of select="FinancialData/DateEnded"/>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EquityPerShareForCalcs', 1); return false" title="Book Value ($">
                <SPAN CLASS="defaultbold">Book Value ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/BookValue,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'TangibleEquityPerShare', 1); return false" title="Tangible Book ($)">
                <SPAN CLASS="defaultbold">Tangible Book ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/TangibleBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSAfterExtraForCalcs', 0); return false" title="Quarter EPS ($)">
                <SPAN CLASS="defaultbold">Quarter EPS ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/QuarterEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSAfterExtraForCalcs', 0); return false" title="EPS ($)">
                <SPAN CLASS="defaultbold">
                  <xsl:value-of select="Company/FiscalPeriodDisplay"/> EPS ($)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/EPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'CoreEPS', 0); return false" title="Operating EPS ($)">
                <SPAN CLASS="defaultbold">
                  <xsl:value-of select="Company/FiscalPeriodDisplay"/> Operating EPS ($)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/OperatingEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $HOMEBUILDER">
          <TR VALIGN="TOP">
            <TD CLASS="header" NOWRAP="" WIDTH="60%" align="left">
              <br />Financial Data
            </TD>
            <TD CLASS="header" WIDTH="40%" valign="bottom" align="right">
              <xsl:value-of select="FinancialData/DateEnded"/>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EquityPerShareForCalcs', 1); return false" title="Book Value ($)">
                <SPAN CLASS="defaultbold">Book Value ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/BookValue,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSAfterExtraForCalcs', 0); return false" title="Quarter EPS ($)">
                <SPAN CLASS="defaultbold">Quarter EPS ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/QuarterEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSAfterExtraForCalcs', 0); return false" title="EPS ($)">
                <SPAN CLASS="defaultbold">
                  <xsl:value-of select="Company/FiscalPeriodDisplay"/> EPS ($)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/EPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'OperatingMarginBuilder', 0); return false" title="Operating Margin ($)">
                <SPAN CLASS="defaultbold">Operating Margin ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/OperatingMarginBuilder,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $GAMING">
          <TR VALIGN="TOP">
            <TD CLASS="header" NOWRAP="" WIDTH="60%" align="left">
              <br />Financial Data
            </TD>
            <TD CLASS="header" WIDTH="40%" valign="bottom" align="right">
              <xsl:value-of select="FinancialData/DateEnded"/>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSAfterExtraForCalcs', 0); return false" title="Quarter EPS ($)">
                <SPAN CLASS="defaultbold">Quarter EPS ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/QuarterEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSAfterExtraForCalcs', 0); return false" title="LTM EPS ($)">
                <SPAN CLASS="defaultbold">LTM EPS ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/LTMEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'AvgTotalCapBookValueCalc', 1); return false" title="Book Value ($)">
                <SPAN CLASS="defaultbold">Book Value ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/AvgTotalCapBookValueCalc,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'GrossMarginDirectGaming', 0); return false" title="Gaming Margin ($)">
                <SPAN CLASS="defaultbold">Gaming Margin ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/GrossMarginDirectGaming,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'GrossMarginOperatingGaming', 0); return false" title="Operating Margin ($)">
                <SPAN CLASS="defaultbold">Operating Margin ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/GrossMarginOperatingGaming,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'DebtToTotalCapBookValue', 0); return false" title="Debt / Total Cap (%)">
                <SPAN CLASS="defaultbold">Debt / Total Cap (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/DebtToTotalCapBookValue,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $MEDIA_ENT or $GAAPDomain = $COMMUNICATIONS or $GAAPDomain = $NEWMEDIA">
          <TR VALIGN="TOP">
            <TD CLASS="header" NOWRAP="" WIDTH="60%" align="left">
              <br />Financial Data
            </TD>
            <TD CLASS="header" WIDTH="40%" valign="bottom" align="right">
              <xsl:value-of select="FinancialData/DateEnded"/>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'TotalCapBookValue', 0); return false" title="Total Capitalization ($M)">
                <SPAN CLASS="defaultbold">Total Capitalization ($M)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="FinancialData/BookValue"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSAfterExtraForCalcs', 0); return false" title="Quarter EPS ($)">
                <SPAN CLASS="defaultbold">Quarter EPS ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/QuarterEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSAfterExtraForCalcs', 0); return false" title="EPS ($)">
                <SPAN CLASS="defaultbold">
                  <xsl:value-of select="Company/FiscalPeriodDisplay"/> EPS ($)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/EPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'NetIncomeMargin', 0); return false" title="Net Income Margin (%)">
                <SPAN CLASS="defaultbold">Net Income Margin (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/NetIncome,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:otherwise>
          <TR VALIGN="TOP">
            <TD CLASS="header" NOWRAP="" WIDTH="60%" align="left">
              <br />Financial Data
            </TD>
            <TD CLASS="header" WIDTH="40%" valign="bottom" align="right">
              <xsl:value-of select="FinancialData/DateEnded"/>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EquityPerShareForCalcs', 1); return false" title="Book Value ($)">
                <SPAN CLASS="defaultbold">Book Value ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/BookValue,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'TangibleEquityPerShare', 1); return false" title="Tangible Book ($)">
                <SPAN CLASS="defaultbold">Tangible Book ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/TangibleBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSAfterExtraForCalcs', 0); return false" title="Quarter EPS ($)">
                <SPAN CLASS="defaultbold">Quarter EPS ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/QuarterEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSAfterExtraForCalcs', 0); return false" title="EPS ($)">
                <SPAN CLASS="defaultbold">
                  <xsl:value-of select="Company/FiscalPeriodDisplay"/> EPS ($)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/EPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:otherwise>
      </xsl:choose>
    </TABLE>
  </xsl:template>

  <xsl:template name="Stock_Price_stock">
    <TABLE CELLPADDING="3" CELLSPACING="1" CLASS="header" WIDTH="100%" BORDER="0" ID="TABLE13">
      <TR>
        <TD CLASS="header" WIDTH="40%" valign="bottom">Stock Price History</TD>
        <TD CLASS="header" align="RIGHT" WIDTH="28%" valign="bottom" nowrap="">
          &#160;<br />High ($)
        </TD>
        <TD CLASS="header" align="RIGHT" WIDTH="32%" valign="bottom" nowrap="">&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;Low ($)</TD>
      </TR>
      <TR>
        <TD CLASS="data" NOWRAP="" WIDTH="40%">
          <SPAN CLASS="defaultbold">One Day</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="format-number(StockPriceHistory/OneDayHigh,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="32%" align="right">
          <xsl:value-of select="format-number(StockPriceHistory/OneDayLow,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
      </TR>
      <TR>
        <TD CLASS="data" NOWRAP="" WIDTH="40%">
          <SPAN CLASS="defaultbold">One Month</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="format-number(StockPriceHistory/OneMonthHigh,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
        <TD class="data" WIDTH="32%" align="right">
          <xsl:value-of select="format-number(StockPriceHistory/OneMonthLow,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
      </TR>
      <TR>
        <TD CLASS="data" NOWRAP="" WIDTH="40%">
          <SPAN CLASS="defaultbold">Three Months</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="format-number(StockPriceHistory/ThreeMonthsHigh,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS=  "dataalign">&#160;</SPAN>
        </TD>
        <TD class="data" WIDTH="32%" align="right">
          <xsl:value-of select="format-number(StockPriceHistory/ThreeMonthsLow,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
      </TR>
      <TR>
        <TD CLASS="data" NOWRAP="" WIDTH="40%">
          <SPAN CLASS="defaultbold">Year-to-Date</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="format-number(StockPriceHistory/YTDHigh,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="32%" align="right">
          <xsl:value-of select="format-number(StockPriceHistory/YTDLow,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
      </TR>
      <TR>
        <TD CLASS="data" NOWRAP="" WIDTH="40%">
          <SPAN CLASS="defaultbold">One Year</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="format-number(StockPriceHistory/OneYearHigh,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="32%" align="right">
          <xsl:value-of select="format-number(StockPriceHistory/OneYearLow,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
      </TR>
      <TR>
        <TD CLASS="data" NOWRAP="" WIDTH="40%">
          <SPAN CLASS="defaultbold">Three Year</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="format-number(StockPriceHistory/ThreeYearsHigh,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="32%" align="right">
          <xsl:value-of select="format-number(StockPriceHistory/ThreeYearsLow,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
      </TR>
      <TR>
        <TD CLASS="data" NOWRAP="" WIDTH="40%">
          <SPAN CLASS="defaultbold">Three Year Date</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="StockPriceHistory/ThreeYearDateHigh"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="32%" align="right">
          <xsl:value-of select="StockPriceHistory/ThreeYearDateLow"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
      </TR>

    </TABLE>
  </xsl:template>

  <xsl:template name="Price_Ratios_stock">
    <TABLE BORDER="0" CELLSPACING="1" CELLPADDING="3" CLASS="header" WIDTH="100%" ID="TABLE14">
      <xsl:variable name="GAAPDomain" select="Company/GAAP"/>
      <xsl:variable name="INSURANCE_BROKER" select="9"/>
      <xsl:variable name="INVESTMENT_COMPANY" select="7"/>
      <xsl:variable name="THRIFT" select="2"/>
      <xsl:variable name="INSURANCE" select="8"/>
      <xsl:variable name="REIT" select="6"/>
      <xsl:variable name="BANK" select="1"/>
      <xsl:variable name="ENERGY" select="10"/>
      <xsl:variable name="GAS" select="18"/>
      <xsl:variable name="FINTECH" select="15"/>
      <xsl:variable name="HOMEBUILDER" select="16"/>
      <xsl:variable name="GAMING" select="19"/>
      <xsl:variable name="COMMUNICATIONS" select="14"/>
      <xsl:variable name="MEDIA_ENT" select="20"/>
      <xsl:variable name="NEWMEDIA" select="21"/>
      <xsl:variable name="keyinstn" select="Company/KeyInstn" />
      <xsl:variable name="keyspecialtaxstatus" select="Company/KeySpecialTaxStatus" />
      <xsl:choose>
        <xsl:when test="$GAAPDomain = $BANK or $GAAPDomain = $THRIFT">
          <TR VALIGN="TOP">
            <TD CLASS="header" NOWRAP="" colspan="2">
              <br />Current Pricing Ratios
            </TD>
          </TR>
          <TR valign="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 610, 0); return false" title="Price / Book (%)">
                <SPAN CLASS="defaultbold">Price / Book (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1843, 0); return false" title="Price / Tangible Book (%)">
                <SPAN CLASS="defaultbold">Price / Tangible Book (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToTangibleBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 609, 0); return false" title="Price / Earnings (x)">
                <SPAN CLASS="defaultbold">Price / Earnings (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1842, 0); return false" title="Price / EPS (x)">
                <SPAN CLASS="defaultbold">
                  Price / <xsl:value-of select="Company/FiscalPeriodDisplay"/> EPS (x)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9637, 0); return false" title="Core EPS (x)">
                <SPAN CLASS="defaultbold">
                  Price / <xsl:value-of select="Company/FiscalPeriodDisplay"/> Core EPS (x)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToCoreEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $HOMEBUILDER">
          <TR VALIGN="TOP">
            <TD CLASS="header" NOWRAP="" colspan="2">
              <br />Current Pricing Ratios
            </TD>
          </TR>
          <TR valign="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 610, 0); return false" title="Price / Book (%)">
                <SPAN CLASS="defaultbold">Price / Book (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9632, 0); return false" title="Price / Earnings (x)">
                <SPAN CLASS="defaultbold">Price / Earnings (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEarningsBeforeExtra,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9631, 0); return false" title="Price/ LTM EPS (x)">
                <SPAN CLASS="defaultbold">Price/ LTM EPS (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEarningsLTMBeforeExtra,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $GAMING">
          <TR VALIGN="TOP">
            <TD CLASS="header" NOWRAP="" colspan="2">
              <br />Current Pricing Ratios
            </TD>
          </TR>
          <TR valign="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 610, 0); return false" title="Price / Book (%)">
                <SPAN CLASS="defaultbold">Price / Book (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 609, 0); return false" title="Price / Earnings (x)">
                <SPAN CLASS="defaultbold">Price / Earnings (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1842, 0); return false" title="Price/ LTM Earnings (x)">
                <SPAN CLASS="defaultbold">Price/ LTM Earnings (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEarningsLTMBeforeExtra,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $INVESTMENT_COMPANY">
          <TR VALIGN="TOP">
            <TD CLASS="header" NOWRAP="" colspan="2">
              <br />Current Pricing Ratios
            </TD>
          </TR>
          <TR valign="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 610, 0); return false" title="Price / Book (%)">
                <SPAN CLASS="defaultbold">Price / Book (%) </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1843, 0); return false" title="Price / Earnings (x)">
                <SPAN CLASS="defaultbold">Price / Earnings (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 609, 0); return false" title="Price / EPS (x)">
                <SPAN CLASS="defaultbold">
                  Price / <xsl:value-of select="Company/FiscalPeriodDisplay"/> EPS (x)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1842, 0); return false" title="Price / EBITDA (x)">
                <SPAN CLASS="defaultbold">Price / EBITDA (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEBITDA,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $REIT">
          <TR VALIGN="TOP">
            <TD CLASS="header" NOWRAP="" colspan="2">
              <br />Current Pricing Ratios
            </TD>
          </TR>
          <xsl:choose>
            <xsl:when test="$keyspecialtaxstatus = '0'">
              <TR valign="TOP">
                <TD CLASS="data" NOWRAP="" WIDTH="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 610, 0); return false" title="Price / FFO (x)">
                    <SPAN CLASS="defaultbold">Price / FFO (x)</SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(CurrentPricingRatios/PriceToFFO,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
              <TR VALIGN="TOP">
                <TD CLASS="data" NOWRAP="" WIDTH="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1843, 0); return false" title="FFO Yield (%)">
                    <SPAN CLASS="defaultbold">FFO Yield (%)</SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(CurrentPricingRatios/FFOYield,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
              <TR VALIGN="TOP">
                <TD CLASS="data" NOWRAP="" WIDTH="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 609, 0); return false" title="Price /  FFO (x)">
                    <SPAN CLASS="defaultbold">
                      Price / <xsl:value-of select="Company/FiscalPeriodDisplay"/> FFO (x)
                    </SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(CurrentPricingRatios/PriceToLTMFFO,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
              <TR VALIGN="TOP">
                <TD CLASS="data" NOWRAP="" WIDTH="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1842, 0); return false" title="Price / EBITDA (x)">
                    <SPAN CLASS="defaultbold">Price / EBITDA (x)</SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEBITDA,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
              <TR VALIGN="TOP">
                <TD CLASS="data" NOWRAP="" WIDTH="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9637, 0); return false" title="Price / Earnings (x)">
                    <SPAN CLASS="defaultbold">Price / Earnings (x)</SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
              <TR VALIGN="TOP">
                <TD CLASS="data" NOWRAP="" WIDTH="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9637, 0); return false" title="Price /  Earnings (x)">
                    <SPAN CLASS="defaultbold">
                      Price / <xsl:value-of select="Company/FiscalPeriodDisplay"/> Earnings (x)
                    </SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(CurrentPricingRatios/PriceToLTMEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
            </xsl:when>
            <xsl:otherwise>
              <TR valign="TOP">
                <TD CLASS="data" NOWRAP="" WIDTH="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9633, 0); return false" title="Price / FFO (x)">
                    <SPAN CLASS="defaultbold">Price / FFO (x)</SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(CurrentPricingRatios/PriceToFFO,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
              <TR VALIGN="TOP">
                <TD CLASS="data" NOWRAP="" WIDTH="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9635, 0); return false" title="FFO Yield (%)">
                    <SPAN CLASS="defaultbold">FFO Yield (%)</SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(CurrentPricingRatios/FFOYield,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
              <TR VALIGN="TOP">
                <TD CLASS="data" NOWRAP="" WIDTH="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9634, 0); return false" title="Price/FFO (x)">
                    <SPAN CLASS="defaultbold">
                      Price / <xsl:value-of select="Company/FiscalPeriodDisplay"/> FFO (x)
                    </SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(CurrentPricingRatios/PriceToLTMFFO,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
              <TR VALIGN="TOP">
                <TD CLASS="data" NOWRAP="" WIDTH="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9638, 0); return false" title="Price / EBITDA (x)">
                    <SPAN CLASS="defaultbold">Price / EBITDA (x)</SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEBITDA,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
              <TR VALIGN="TOP">
                <TD CLASS="data" NOWRAP="" WIDTH="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 609, 0); return false" title="Price / Earnings (x)">
                    <SPAN CLASS="defaultbold">Price / Earnings (x)</SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
              <TR VALIGN="TOP">
                <TD CLASS="data" NOWRAP="" WIDTH="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1842, 0); return false" title="Price / Earnings (x)">
                    <SPAN CLASS="defaultbold">
                      Price / <xsl:value-of select="Company/FiscalPeriodDisplay"/> Earnings (x)
                    </SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(CurrentPricingRatios/PriceToLTMEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $INSURANCE_BROKER or $GAAPDomain = $INSURANCE">
          <TR VALIGN="TOP">
            <TD CLASS="header" NOWRAP="" colspan="2">
              <br />Current Pricing Ratios
            </TD>
          </TR>
          <TR valign="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 610, 0); return false" title="Price / Book (%)">
                <SPAN CLASS="defaultbold">Price / Book (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1843, 0); return false" title="Price / Tangible Book (%)">
                <SPAN CLASS="defaultbold">Price / Tangible Book (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToTangibleBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1843, 0); return false" title="Price / Book, excl. 115 (%)">
                <SPAN CLASS="defaultbold">Price / Book, excl. 115 (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToBookExcluding115,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 609, 0); return false" title="Price / Earnings (x)" >
                <SPAN CLASS="defaultbold">Price / Earnings (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1842, 0); return false" title="Price / EPS (x)">
                <SPAN CLASS="defaultbold">
                  Price / <xsl:value-of select="Company/FiscalPeriodDisplay"/> EPS (x)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9637, 0); return false" title="Price / Operating EPS (x)">
                <SPAN CLASS="defaultbold">
                  Price / <xsl:value-of select="Company/FiscalPeriodDisplay"/> Operating EPS (x)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToOperatingEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $ENERGY or $GAAPDomain = $GAS">
          <TR VALIGN="TOP">
            <TD CLASS="header" NOWRAP="" colspan="2">
              <br />Current Pricing Ratios
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1842, 0); return false" title="Price / EBITDA (x)">
                <SPAN CLASS="defaultbold">Price / EBITDA (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEBITDA,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9632, 0); return false" title="Price / Earnings (x)">
                <SPAN CLASS="defaultbold">Price / Earnings (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEarningsBeforeExtra,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR valign="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9631, 0); return false" title="Price / LTM Earnings (x)">
                <SPAN CLASS="defaultbold">Price / LTM Earnings (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToLTMEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $FINTECH">
          <TR VALIGN="TOP">
            <TD CLASS="header" NOWRAP="" colspan="2">
              <br />Current Pricing Ratios
            </TD>
          </TR>
          <TR valign="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 610, 0); return false" title="Price / Book (%)">
                <SPAN CLASS="defaultbold">Price / Book (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1843, 0); return false" title="Price / Tangible Book (%)">
                <SPAN CLASS="defaultbold">Price / Tangible Book (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToTangibleBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9632, 0); return false" title="Price / Earnings (x)">
                <SPAN CLASS="defaultbold">Price / Earnings (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR valign="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9631, 0); return false" title="Price / LTM Earnings (x)">
                <SPAN CLASS="defaultbold">Price / LTM Earnings (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToLTMEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9639, 0); return false" title="Price / EBITDA (x)">
                <SPAN CLASS="defaultbold">Price / EBITDA (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEBITDA,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $MEDIA_ENT or $GAAPDomain = $COMMUNICATIONS or $GAAPDomain = $NEWMEDIA">
          <TR VALIGN="TOP">
            <TD CLASS="header" NOWRAP="" colspan="2">
              <br />Current Pricing
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 609, 0); return false" title="Price / Earnings (x)">
                <SPAN CLASS="defaultbold">Price / Earnings (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1842, 0); return false" title="Price / EPS (x)">
                <SPAN CLASS="defaultbold">
                  Price / <xsl:value-of select="Company/FiscalPeriodDisplay"/> EPS (x)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:otherwise>
          <TR VALIGN="TOP">
            <TD CLASS="header" NOWRAP="" colspan="2">
              <br />Current Pricing Ratios
            </TD>
          </TR>
          <TR valign="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 610, 0); return false" title="Price / Book (%)">
                <SPAN CLASS="defaultbold">Price / Book (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1843, 0); return false" title="Price / Tangible Book (%)">
                <SPAN CLASS="defaultbold">Price / Tangible Book (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToTangibleBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 609, 0); return false" title="Price / Earnings (x)">
                <SPAN CLASS="defaultbold">Price / Earnings (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1842, 0); return false" title="Price / EPS (x)">
                <SPAN CLASS="defaultbold">
                  Price /<xsl:value-of select="Company/FiscalPeriodDisplay"/> EPS (x)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:otherwise>
      </xsl:choose>
    </TABLE>
  </xsl:template>

  <xsl:template name="Price_Change_stock">
    <xsl:variable name="mytradingsymbol" select="/irw:IRW/irw:StockInformation/TradedIssues/TradingSymbol[../KeyFndg = /irw:IRW/irw:StockInformation/Company/KeyFndg]"/>
    <TABLE CELLPADDING="3" CELLSPACING="1" CLASS="header" WIDTH="100%" BORDER="0" ID="TABLE16">
      <TR VALIGN="bottom">
        <TD CLASS="header" WIDTH="40%">Price Change&#160;(%)</TD>
        <TD CLASS="header" align="center" WIDTH="28%">
          <xsl:value-of select="$mytradingsymbol"/>
        </TD>
        <xsl:if test="Company/ComparativeIndexName != ''">
          <TD CLASS="header" align="center" WIDTH="32%">
            <xsl:value-of select="Company/ComparativeIndexName"/> Index
          </TD>
        </xsl:if>
      </TR>
      <TR >
        <TD CLASS="data" NOWRAP="" align="LEFT" WIDTH="40%">
          <SPAN CLASS="defaultbold">One Day</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="format-number(PriceChange/OneDay,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
          <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
        </TD>
        <xsl:if test="Company/ComparativeIndexName != ''">
          <TD CLASS="data" WIDTH="28%" align="right">
            <xsl:value-of select="format-number(PriceChange/OneDayIndex,'#,##0.00 ;(#,##0.00)','data')"/>
            <SPAN CLASS="dataalign">&#160;</SPAN>
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </TD>
        </xsl:if>
      </TR>
      <TR >
        <TD CLASS="data" NOWRAP="" align="LEFT" WIDTH="40%">
          <SPAN CLASS="defaultbold">One Month</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="format-number(PriceChange/OneMonth,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
          <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
        </TD>
        <xsl:if test="Company/ComparativeIndexName != ''">
          <TD CLASS="data" WIDTH="28%" align="right">
            <xsl:value-of select="format-number(PriceChange/OneMonthIndex,'#,##0.00 ;(#,##0.00)','data')"/>
            <SPAN CLASS="dataalign">&#160;</SPAN>
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </TD>
        </xsl:if>
      </TR>
      <TR >
        <TD CLASS="data" NOWRAP="" align="LEFT" WIDTH="40%">
          <SPAN CLASS="defaultbold">Three Month</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="format-number(PriceChange/ThreeMonth,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
          <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
        </TD>
        <xsl:if test="Company/ComparativeIndexName != ''">
          <TD CLASS="data" WIDTH="28%" align="right">
            <xsl:value-of select="format-number(PriceChange/ThreeMonthIndex,'#,##0.00 ;(#,##0.00)','data')"/>
            <SPAN CLASS="dataalign">&#160;</SPAN>
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </TD>
        </xsl:if>
      </TR>
      <TR >
        <TD CLASS="data" NOWRAP="" align="LEFT" WIDTH="40%">
          <SPAN CLASS="defaultbold">YTD</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="format-number(PriceChange/YTD,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
          <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
        </TD>
        <xsl:if test="Company/ComparativeIndexName != ''">
          <TD CLASS="data" WIDTH="28%" align="right">
            <xsl:value-of select="format-number(PriceChange/YTDIndex,'#,##0.00 ;(#,##0.00)','data')"/>
            <SPAN CLASS="dataalign">&#160;</SPAN>
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </TD>
        </xsl:if>
      </TR>
      <TR >
        <TD CLASS="data" NOWRAP="" align="LEFT" WIDTH="40%">
          <SPAN CLASS="defaultbold">One Year</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="format-number(PriceChange/OneYear,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
          <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
        </TD>
        <xsl:if test="Company/ComparativeIndexName != ''">
          <TD CLASS="data" WIDTH="28%" align="right">
            <xsl:value-of select="format-number(PriceChange/OneYearIndex,'#,##0.00 ;(#,##0.00)','data')"/>
            <SPAN CLASS="dataalign">&#160;</SPAN>
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </TD>
        </xsl:if>
      </TR>
    </TABLE>
  </xsl:template>

  <xsl:template name="Dividends_stock">
    <xsl:variable name="divkey">
      <xsl:choose>
        <xsl:when test="Dividends/IsDivAggregate = 'True'">46843</xsl:when>
        <xsl:otherwise>9640</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="divyieldkey">
      <xsl:choose>
        <xsl:when test="Dividends/IsDivAggregate = 'True'">46844</xsl:when>
        <xsl:otherwise>2425</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <TABLE BORDER="0" CELLSPACING="1" CELLPADDING="3" CLASS="header" WIDTH="100%" ID="TABLE17">
      <xsl:variable name="GAAPDomain" select="Company/GAAP"/>
      <xsl:variable name="REIT" select="6"/>
      <xsl:variable name="ENERGY" select="10"/>
      <xsl:variable name="GAS" select="18"/>
      <xsl:variable name="FINTECH" select="15"/>
      <xsl:variable name="keyinstn" select="Company/KeyInstn" />
      <xsl:variable name="keyspecialtaxstatus" select="Company/KeySpecialTaxStatus" />
      <xsl:choose>
        <xsl:when test="$GAAPDomain = $REIT">
          <TR VALIGN="TOP">
            <TD colspan="2" CLASS="header">Dividends</TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, {$divkey}, 0); return false" title="Quarterly Dividend ($)">
                <SPAN CLASS="defaultbold">Quarterly Dividend ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" width="40%" align="right">
              <xsl:value-of select="format-number(Dividends/QuarterlyDividend,'#,##0.0000 ;(#,##0.0000)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, {$divyieldkey}, 0); return false" title="Dividend Yield (%)">
                <SPAN CLASS="defaultbold">Dividend Yield (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" width="40%" align="right">
              <xsl:value-of select="format-number(Dividends/DividendYield,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <xsl:if test="Company/IsCommonStock='True'">
            <TR VALIGN="TOP">
              <!--### Change decimal places to 4 as per "Quarterly Dividend" above ###-->
              <TD CLASS="data" WIDTH="60%">
                <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 'Dividend', 0); return false" title="Dividends Announced ($)">
                  <SPAN CLASS="defaultbold">
                    <xsl:value-of select="Company/FiscalPeriodDisplay"/> Dividends Announced ($)
                  </SPAN>
                </a>
              </TD>
              <TD CLASS="data" width="40%" align="right">
                <xsl:value-of select="format-number(Dividends/Dividend,'#,##0.0000 ;(#,##0.0000)','data')"/>
                <SPAN CLASS="dataalign">&#160;</SPAN>
              </TD>
            </TR>
            <TR VALIGN="TOP">
              <xsl:choose>
                <xsl:when test="$keyspecialtaxstatus = '0'">
                  <TD CLASS="data" WIDTH="60%">
                    <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 'DividendPayout', 0); return false" title="Payout Ratio (%)">
                      <SPAN CLASS="defaultbold">
                        <xsl:value-of select="Company/FiscalPeriodDisplay"/> Payout Ratio (%)
                      </SPAN>
                    </a>
                  </TD>
                </xsl:when>
                <xsl:otherwise>
                  <TD CLASS="data" WIDTH="60%">
                    <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 'DividendPayoutFFO', 0); return false" title="Payout Ratio (%)">
                      <SPAN CLASS="defaultbold">
                        <xsl:value-of select="Company/FiscalPeriodDisplay"/> Payout Ratio (%)
                      </SPAN>
                    </a>
                  </TD>
                </xsl:otherwise>
              </xsl:choose>
              <TD CLASS="data" width="40%" align="right">
                <xsl:value-of select="format-number(Dividends/LTMPayoutRatio,'#,##0.00 ;(#,##0.00)','data')"/>
                <SPAN CLASS="dataalign">&#160;</SPAN>
              </TD>
            </TR>
          </xsl:if>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $ENERGY or $GAAPDomain = $GAS">
          <TR VALIGN="TOP">
            <TD colspan="2" CLASS="header">Dividends</TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 13498, 0); return false" title="Dividend Paid ($)">
                <SPAN CLASS="defaultbold">Dividend Paid ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" width="40%" align="right">
              <xsl:value-of select="format-number(Dividends/DividendPaid,'#,##0.0000 ;(#,##0.0000)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 3249, 0); return false" title="Dividend Payout (%)">
                <SPAN CLASS="defaultbold">Dividend Payout (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" width="40%" align="right">
              <xsl:value-of select="format-number(Dividends/DividendPayout,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9935, 0); return false" title="Dividend / Book Per Share (x)">
                <SPAN CLASS="defaultbold">Dividend / Book Per Share (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" width="40%" align="right">
              <xsl:value-of select="format-number(Dividends/DividendToBookPerShare,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $FINTECH">
          <TR VALIGN="TOP">
            <TD colspan="2" CLASS="header">Dividends</TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, {$divkey}, 0); return false" title="Quarterly Dividend ($)">
                <SPAN CLASS="defaultbold">Quarterly Dividend ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" width="40%" align="right">
              <xsl:value-of select="format-number(Dividends/QuarterlyDividend,'#,##0.0000 ;(#,##0.0000)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, {$divyieldkey}, 0); return false" title="Dividend Yield (%)">
                <SPAN CLASS="defaultbold">Dividend Yield (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" width="40%" align="right">
              <xsl:value-of select="format-number(Dividends/DividendYield,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <!--### Change decimal places to 4 as per "Quarterly Dividend" above ###-->
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 20350, 0); return false" title="LTM Dividends Announced ($)">
                <SPAN CLASS="defaultbold">LTM Dividends Announced ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" width="40%" align="right">
              <xsl:value-of select="format-number(Dividends/DivAnnouncedLTM,'#,##0.0000 ;(#,##0.0000)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 3249, 0); return false" title="Dividend Payout Ratio (%)">
                <SPAN CLASS="defaultbold">Dividend Payout Ratio (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" width="40%" align="right">
              <xsl:value-of select="format-number(Dividends/LTMPayoutRatio,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:otherwise>
          <TR VALIGN="TOP">
            <TD colspan="2" CLASS="header">Dividends</TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, {$divkey}, 0); return false" title="Quarterly Dividend ($)">
                <SPAN CLASS="defaultbold">Quarterly Dividend ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" width="40%" align="right">
              <xsl:value-of select="format-number(Dividends/QuarterlyDividend,'#,##0.0000 ;(#,##0.0000)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, {$divyieldkey}, 0); return false" title="Dividend Yield (%)">
                <SPAN CLASS="defaultbold">Dividend Yield (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" width="40%" align="right">
              <xsl:value-of select="format-number(Dividends/DividendYield,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <xsl:if test="Company/IsCommonStock='True'">
            <TR VALIGN="TOP">
              <!--### Change decimal places to 4 as per "Quarterly Dividend" above ###-->
              <TD CLASS="data" WIDTH="60%">
                <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 'Dividend', 0); return false" title="Dividend ($)">
                  <SPAN CLASS="defaultbold">
                    <xsl:value-of select="Company/FiscalPeriodDisplay"/> Dividend ($)
                  </SPAN>
                </a>
              </TD>
              <TD CLASS="data" width="40%" align="right">
                <xsl:value-of select="format-number(Dividends/Dividend,'#,##0.0000 ;(#,##0.0000)','data')"/>
                <SPAN CLASS="dataalign">&#160;</SPAN>
              </TD>
            </TR>
            <xsl:if test="Company/GAAP != 0">
            <TR VALIGN="TOP">
              <TD CLASS="data" WIDTH="60%">
                <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 'DividendPayout', 0); return false" title="Payout Ratio (%)">
                  <SPAN CLASS="defaultbold">
                    <xsl:value-of select="Company/FiscalPeriodDisplay"/> Payout Ratio (%)
                  </SPAN>
                </a>
              </TD>
              <TD CLASS="data" width="40%" align="right">
                <xsl:value-of select="format-number(Dividends/PayoutRatio,'#,##0.00 ;(#,##0.00)','data')"/>
                <SPAN CLASS="dataalign">&#160;</SPAN>
              </TD>
            </TR>
            </xsl:if>
          </xsl:if>
        </xsl:otherwise>
      </xsl:choose>
    </TABLE>
  </xsl:template>

  <xsl:template name="FinancialDataTables">
    <xsl:variable name="mytradingsymbol" select="/irw:IRW/irw:StockInformation/TradedIssues/TradingSymbol[../KeyFndg = /irw:IRW/irw:StockInformation/Company/KeyFndg]"/>
    <xsl:if test="Company/IsCommonStock='True'">
      <TR>
        <xsl:if test="not(Company/GAAP=0)">
          <TD valign="top">
            <xsl:call-template name="Vol_Highlights_stock"/>
          </TD>
          <TD>&#160;</TD>
          <TD valign="top">
            <xsl:call-template name="Fin_Data_stock"/>
          </TD>
        </xsl:if>
        <xsl:if test="Company/GAAP=0">
          <TD valign="top" colspan="3">
            <xsl:call-template name="Vol_Highlights_stock"/>
          </TD>
        </xsl:if>
      </TR>
      <TR>
        <TD valign="top">
          <IMG SRC="/images/Interactive/blank.gif" WIDTH="8" HEIGHT="4" alt=""/>
        </TD>
        <TD>
          <IMG SRC="/images/Interactive/blank.gif" WIDTH="2" HEIGHT="8" alt=""/>
        </TD>
        <TD valign="top">
          <IMG SRC="/images/Interactive/blank.gif" WIDTH="8" HEIGHT="4" alt=""/>
        </TD>
      </TR>
      <TR>
        <xsl:if test="not(Company/GAAP=0)">
          <TD valign="top">
            <xsl:call-template name="Stock_Price_stock"/>
          </TD>
          <TD CLASS="colorlight">
            <IMG SRC="/images/Interactive/blank.gif" WIDTH="2" HEIGHT="8" alt=""/>
          </TD>
          <TD valign="top">
            <xsl:call-template name="Price_Ratios_stock"/>
          </TD>
        </xsl:if>
        <xsl:if test="Company/GAAP=0">
          <TD valign="top" colspan="3">
            <xsl:call-template name="Stock_Price_stock"/>
          </TD>
        </xsl:if>
      </TR>
      <TR>
        <TD valign="top">
          <IMG SRC="/images/Interactive/blank.gif" WIDTH="8" HEIGHT="4" alt=""/>
        </TD>
        <TD>
          <IMG SRC="/images/Interactive/blank.gif" WIDTH="2" HEIGHT="8" alt=""/>
        </TD>
        <TD valign="top">
          <IMG SRC="/images/Interactive/blank.gif" WIDTH="8" HEIGHT="4" alt=""/>
        </TD>
      </TR>
      <TR>        
          <TD valign="top">
            <xsl:call-template name="Price_Change_stock"/>
          </TD>
          <TD>
            <IMG SRC="/images/Interactive/blank.gif" WIDTH="2" HEIGHT="10" alt=""/>
          </TD>
          <TD valign="top">
            <xsl:call-template name="Dividends_stock"/>
          </TD>
      </TR>
    </xsl:if>
    <xsl:if test="Company/IsCommonStock='False'">
      <TR>
        <TD valign="top">
          <xsl:call-template name="Vol_Highlights_stock"/>
        </TD>
        <TD>&#160;</TD>
        <TD valign="top">

          <TABLE CELLSPACING="1" CELLPADDING="3" CLASS="header" WIDTH="100%" BORDER="0" ID="TABLE16">
            <TR VALIGN="bottom">
              <TD CLASS="header" WIDTH="40%">
                <br />Price Change&#160;(%)
              </TD>
              <TD CLASS="header" align="right">
                <xsl:value-of select="$mytradingsymbol"/>
              </TD>
            </TR>
            <TR >
              <TD CLASS="data" NOWRAP="" align="LEFT" WIDTH="40%">
                <SPAN CLASS="defaultbold">One Day</SPAN>
              </TD>
              <TD CLASS="data" WIDTH="28%" align="right">
                <xsl:value-of select="format-number(PriceChange/OneDay,'#,##0.00 ;(#,##0.00)','data')"/>
                <SPAN CLASS="dataalign">&#160;</SPAN>
              </TD>
            </TR>
            <TR>
              <TD CLASS="data" NOWRAP="" align="LEFT" WIDTH="40%">
                <SPAN CLASS="defaultbold">One Month</SPAN>
              </TD>
              <TD CLASS="data" WIDTH="28%" align="right">
                <xsl:value-of select="format-number(PriceChange/OneMonth,'#,##0.00 ;(#,##0.00)','data')"/>
                <SPAN CLASS="dataalign">&#160;</SPAN>
              </TD>
            </TR>
            <TR >
              <TD CLASS="data" NOWRAP="" align="LEFT" WIDTH="40%">
                <SPAN CLASS="defaultbold">Three Month</SPAN>
              </TD>
              <TD CLASS="data" WIDTH="28%" align="right">
                <xsl:value-of select="format-number(PriceChange/ThreeMonth,'#,##0.00 ;(#,##0.00)','data')"/>
                <SPAN CLASS="dataalign">&#160;</SPAN>
              </TD>
            </TR>
            <TR >
              <TD CLASS="data" NOWRAP="" align="LEFT" WIDTH="40%">
                <SPAN CLASS="defaultbold">YTD</SPAN>
              </TD>
              <TD CLASS="data" WIDTH="28%" align="right">
                <xsl:value-of select="format-number(PriceChange/YTD,'#,##0.00 ;(#,##0.00)','data')"/>
                <SPAN CLASS="dataalign">&#160;</SPAN>
              </TD>
            </TR>
            <TR >
              <TD CLASS="data" NOWRAP="" align="LEFT" WIDTH="40%">
                <SPAN CLASS="defaultbold">One Year</SPAN>
              </TD>
              <TD CLASS="data" WIDTH="28%" align="right">
                <xsl:value-of select="format-number(PriceChange/OneYear,'#,##0.00 ;(#,##0.00)','data')"/>
                <SPAN CLASS="dataalign">&#160;</SPAN>
              </TD>
            </TR>
          </TABLE>
        </TD>
      </TR>
      <TR>
        <TD valign="top">
          <IMG SRC="/images/Interactive/blank.gif" WIDTH="8" HEIGHT="4" alt=""/>
        </TD>
        <TD>
          <IMG SRC="/images/Interactive/blank.gif" WIDTH="2" HEIGHT="8" alt=""/>
        </TD>
        <TD valign="top">
          <IMG SRC="/images/Interactive/blank.gif" WIDTH="8" HEIGHT="4" alt=""/>
        </TD>
      </TR>
      <TR>
          <TD valign="top">
            <xsl:call-template name="Stock_Price_stock"/>
          </TD>
          <TD CLASS="colorlight">
            <IMG SRC="/images/Interactive/blank.gif" WIDTH="2" HEIGHT="8" alt=""/>
          </TD>
          <TD valign="top">
            <xsl:call-template name="Dividends_stock"/>
          </TD>
      </TR>
    </xsl:if>
  </xsl:template>


  <xsl:template name="Quote_Table_stock">
    <xsl:variable name="keyinstn" select="Company/KeyInstn"/>
    <TABLE BORDER="0" CELLSPACING="1" CELLPADDING="3" WIDTH="100%" CLASS="header" ID="TABLE5">
      <TR VALIGN="BOTTOM">
        <TD CLASS="header" colspan="2">Current Quote</TD>
      </TR>
      <TR VALIGN="BOTTOM">
        <TD CLASS="data" NOWRAP="" WIDTH="50%">
          <SPAN CLASS="defaultbold">Last Sale</SPAN>
        </TD>
        <TD CLASS="data" NOWRAP="" WIDTH="50%">
          <SPAN CLASS="defaultbold">Last Close</SPAN>
        </TD>
      </TR>
      <TR align="center" VALIGN="top">
        <TD CLASS="data" NOWRAP="" WIDTH="50%">
          &#36;&#160;<xsl:value-of select="format-number(StockQuote/CurrentPrice, '###,##0.00')"/>
        </TD>
        <TD CLASS="data" NOWRAP="" WIDTH="50%">
          <xsl:choose>
            <xsl:when test="StockQuote/LastPrice > 0">
              &#36;&#160;<xsl:value-of select="format-number(StockQuote/LastPrice, '###,##0.00')"/>
            </xsl:when>
            <xsl:otherwise>
              NA
            </xsl:otherwise>
          </xsl:choose>
        </TD>
      </TR>
      <TR VALIGN="BOTTOM">
        <TD CLASS="data" NOWRAP="" WIDTH="50%">
          <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1850, 0); return false" title="Today's High">
            <SPAN CLASS="defaultbold">Today's High</SPAN>
          </a>
        </TD>
        <TD CLASS="data" NOWRAP="" WIDTH="50%">
          <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1851, 0); return false" title="Today's Low"> 
            <SPAN CLASS="defaultbold">Today's Low</SPAN>
          </a>
        </TD>
      </TR>
      <TR align="CENTER" VALIGN="TOP">
        <TD CLASS="data" NOWRAP="" WIDTH="50%">
          &#36;&#160;<xsl:choose>
            <xsl:when test="StockQuote/HighToday= 0">NA</xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="format-number(StockQuote/HighToday, '###,##0.00')"/>
            </xsl:otherwise>
          </xsl:choose>
        </TD>
        <TD CLASS="data" NOWRAP="" WIDTH="50%">
          &#36;&#160;<xsl:choose>
            <xsl:when test="StockQuote/LowToday= 0">NA</xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="format-number(StockQuote/LowToday, '###,##0.00')"/>
            </xsl:otherwise>
          </xsl:choose>
        </TD>
      </TR>
      <TR align="CENTER" VALIGN="TOP">
        <TD CLASS="data" NOWRAP="" WIDTH="50%" align="left">
          <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1855, 0); return false" title="Net Change">
            <SPAN CLASS="defaultbold">Net Change</SPAN>
          </a>
        </TD>
        <TD CLASS="data" NOWRAP="" WIDTH="50%" align="left">
          <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 2454, 0); return false" title="Today's Volume">
            <SPAN CLASS="defaultbold">Today's Volume</SPAN>
          </a>
        </TD>
      </TR>
      <TR align="CENTER" VALIGN="TOP">
        <TD CLASS="data" NOWRAP=""  WIDTH="50%">
          <xsl:variable name="NetChangeFromPreviousClose" select="StockQuote/NetChangeFromPreviousClose"/>
          <xsl:choose>
            <xsl:when test="$NetChangeFromPreviousClose != 0">
              <xsl:choose>
                <xsl:when test="$NetChangeFromPreviousClose &gt; 0">
                  &#36;&#160;<xsl:value-of select="format-number($NetChangeFromPreviousClose,'###,##0.00','data')"/>&#160;
                </xsl:when>
                <xsl:otherwise>
                  &#36;&#160;<xsl:value-of select="$NetChangeFromPreviousClose"/>&#160;
                </xsl:otherwise>
              </xsl:choose>
              <xsl:if test="StockQuote/NetChangeDirection = 'positive'">
                <SPAN class="default">
                  <IMG SRC="/images/Interactive/IR/uptriangle.gif" WIDTH="11" HEIGHT="10" BORDER="0" ALT="Up" />&#160;
                </SPAN>
              </xsl:if>
              <xsl:if test="StockQuote/NetChangeDirection = 'negative'">
                <SPAN class="default">
                  <IMG SRC="/images/Interactive/IR/downtriangle.gif" WIDTH="11" HEIGHT="10" BORDER="0" ALT="Down" />&#160;
                </SPAN>
              </xsl:if>
              <strong>
                <xsl:if test="StockQuote/NetChangeDirection = 'negative'">(</xsl:if><xsl:value-of select="format-number(StockQuote/PercentChangeFromPreviousClose, '###,##0.00')"/><xsl:if test="StockQuote/NetChangeDirection = 'negative'">)</xsl:if>&#37;
              </strong>
            </xsl:when>
            <xsl:otherwise>
              NA
            </xsl:otherwise>
          </xsl:choose>
        </TD>
        <TD CLASS="data" NOWRAP="1" WIDTH="50%">
          <xsl:value-of select="StockQuote/VolumeToday"/>
        </TD>
      </TR>
      <TR align="left" VALIGN="TOP">
        <TD CLASS="header" NOWRAP="1" colspan="2" align="center">
          <nobr>Data as of&#160;</nobr>
          <nobr>
            <xsl:value-of select="StockQuote/QuoteTimeStamp"/>
          </nobr>
        </TD>
      </TR>
      <TR align="left" VALIGN="TOP">
        <TD CLASS="header" NOWRAP="1" colspan="2" align="center">
          <nobr>Minimum 20 minute delay</nobr>
        </TD>
      </TR>
    </TABLE>
  </xsl:template>
  <!-- Default Template Ends here. all templates below are ONLY if you are using the 'Style Template' section in the console. -->



  <!-- This is Template ONE option in the console under 'Style Template'
//This template is a striped down version of the main template. Everything (data) is displayed in single rows.
//The sections will be commented on where the rows are. you should be able to just move rows (entire chunks of data) where you need too. Edits to this section are rare, mostly due to size (width of the page).
-->
  <xsl:template name="Selector_Stock2">
    <xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
    <xsl:variable name="TradedIssuesCount" select="TradedIssues/RowCount"/>
    <xsl:choose>
      <xsl:when test="$TradedIssuesCount > 1">
        <TR>
          <TD CLASS=" titletest" nowrap="" valign="top">
            <TABLE BORDER="0" CELLSPACING="1" CELLPADDING="3">
              <TR>
                <TD CLASS="default defaultbold">Security:</TD>
                <TD CLASS="default" align="left">
                  <xsl:variable name="CurrentKeyFndg" select="Company/KeyFndg"/>
					<label class="visuallyhidden" for="KeyFndg">KeyFndg</label>
                  <select NAME="KeyFndg" ID="KeyFndg" size="1" class="input">
                    <xsl:for-each select="TradedIssues">
                      <xsl:value-of select="KeyFndg"/>
                      <option>

                        <xsl:attribute name="value">
                          <xsl:value-of select="KeyFndg"/>
                        </xsl:attribute>

                        <xsl:if test="$CurrentKeyFndg=KeyFndg">
                          <xsl:attribute name="SELECTED">1</xsl:attribute>
                        </xsl:if>

                        <xsl:value-of select="TradingSymbol"/>
                      </option>
                    </xsl:for-each>
                  </select>
                </TD>
                <TD CLASS="default" align="left">
                  <input type="submit" NAME="submit" value="Apply" align="left" ID="Submit1" class="submit" />
                </TD>
              </TR>
            </TABLE>
          </TD>
        </TR>
      </xsl:when>
    </xsl:choose>
  </xsl:template>



  <xsl:template name="Chart_Drop_stock2">
    <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%">
      <xsl:call-template name="Chart_Drop_Image_stock2"/>
    </TABLE>
    <br />
  </xsl:template>

  <xsl:template name="ChartOptions2">
    <TABLE border="0" width="100%" cellpadding="3" cellspacing="3" class="data">
      <TR align="left">
        <TD valign="top">
          <xsl:call-template name="UpdateChart2"/>

        </TD>
      </TR>
      <xsl:if test="contains(GraphComponents/AllowDataDownload,'True')">
        <TR align="left">
          <TD valign="top" class="surr datashade">
            <SPAN class="defaultbold">Downloads</SPAN>
          </TD>
        </TR>
        <TR align="left" class="data">
          <TD valign="middle">

            <xsl:call-template name="DownloadHistData2"/>
            <xsl:if test="contains(GraphComponents/AllowDataDownload,'False')">&#160;</xsl:if>

          </TD>
        </TR>
        <TR align="left" class="data">
          <TD valign="middle">

            <xsl:call-template name="DownloadChart2"/>
            <xsl:if test="contains(GraphOptions/Small,'False') or contains(GraphOptions/Medium,'False') or contains(GraphOptions/Large,'False')">&#160;</xsl:if>

          </TD>
        </TR>
      </xsl:if>

    </TABLE>
  </xsl:template>

  <xsl:template name="DownloadChart2">
    <xsl:variable name="peerCount" select="PeerCount/Count"/>
    <xsl:variable name="indexCount" select="IndexCount/Count"/>
    <xsl:variable name="keyInstn" select="Company/KeyInstn"/>
    <xsl:variable name="keyFndg" select="Company/KeyFndg"/>
    <xsl:variable name="isPreview" select="Company/IsPreview" />

    <xsl:if test="contains(GraphOptions/Small,'True') or contains(GraphOptions/Medium,'True') or contains(GraphOptions/Large,'True')">

      <TABLE border="0" cellpadding="3" cellspacing="0">

        <form name="downloadChartOptions" method="POST" action="StockInfoIFrame.aspx" target="_blank">
          <TR align="left">
            <TD class="defaultbold" valign="middle" nowrap="">
              Chart Size:
			  <label class="visuallyhidden" for="downloadSize">downloadSize</label>
              <select name="downloadSize" id="downloadSize" class="input">
                <xsl:if test="contains(GraphOptions/Small,'True')">
                  <option value="1">

                    Small
                  </option>
                </xsl:if>
                <xsl:if test="contains(GraphOptions/Medium,'True')">
                  <option value="2">

                    Medium
                  </option>
                </xsl:if>
                <xsl:if test="contains(GraphOptions/Large,'True')">
                  <option value="3">

                    Large
                  </option>
                </xsl:if>
              </select>
            </TD>
            <TD class="defaultbold" valign="middle">
              <button id="ChartDownload" type="button">Download</button>
            </TD>
          </TR>
          <xsl:if test="contains($isPreview, 'true')">
            <input type="hidden" name="preview" value="1"/>
          </xsl:if>

          <input type="hidden" name="KeyInstn">
            <xsl:attribute name="value">
              <xsl:value-of select="$keyInstn"/>
            </xsl:attribute>
          </input>

          <input type="hidden" name="KeyFndg">
            <xsl:attribute name="value">
              <xsl:value-of select="$keyFndg"/>
            </xsl:attribute>
          </input>

          <xsl:for-each select="Peers">
            <xsl:variable name="name" select="FormName"/>

            <input type="hidden" value="">
              <xsl:attribute name="name">
                <xsl:value-of select="concat('_','',$name)"/>
              </xsl:attribute>

              <xsl:attribute name="id">
                <xsl:value-of select="concat('_','',$name)"/>
              </xsl:attribute>
            </input>
          </xsl:for-each>

          <xsl:for-each select="CompIndex">
            <xsl:variable name="name" select="FormName"/>

            <input type="hidden" value="">
              <xsl:attribute name="name">
                <xsl:value-of select="concat('_','',$name)"/>
              </xsl:attribute>

              <xsl:attribute name="id">
                <xsl:value-of select="concat('_','',$name)"/>
              </xsl:attribute>
            </input>
          </xsl:for-each>

          <input type="hidden" name="Peers">
            <xsl:attribute name="value">
              <xsl:value-of select="$peerCount"/>
            </xsl:attribute>
          </input>

          <input type="hidden" name="Indexes">
            <xsl:attribute name="value">
              <xsl:value-of select="$indexCount"/>
            </xsl:attribute>
          </input>

          <input type="hidden" name="_period" id="_period" value=""/>
        </form>
      </TABLE>
    </xsl:if>
  </xsl:template>
  <!-- End Download Chart Dropdown -->


  <!-- Download Historical Data Dropdown -->
  <xsl:template name="DownloadHistData2">
    <xsl:variable name="keyInstn" select="Company/KeyInstn"/>
    <xsl:variable name="keyFndg" select="Company/KeyFndg"/>
    <xsl:variable name="isPreview" select="Company/IsPreview" />

    <xsl:if test="contains(GraphComponents/AllowDataDownload,'True')">
      <TABLE border="0" cellpadding="3" cellspacing="0">
        <form name="downloadDataOptions" method="POST" action="StockInfoIFrame.aspx" target="_blank">
          <TR align="left">
            <TD class="defaultbold" valign="middle" nowrap="">
              Historical Data Format:
			  <label class="visuallyhidden" for="downloadData">downloadData</label>
              <select name="downloadData" id="downloadData" class="input">
                <option value="html">

                  HTML
                </option>
                <option value="csv">

                  CSV
                </option>
              </select>
            </TD>
            <TD class="default" valign="middle">
              <input type="submit" value="Download" class="submit">
                <xsl:attribute name="onclick">
                  fillDownloadPeriod();
                </xsl:attribute>
              </input>
            </TD>
          </TR>
          <xsl:if test="contains($isPreview, 'true')">
            <input type="hidden" name="preview" value="1"/>
          </xsl:if>

          <input type="hidden" name="KeyInstn">
            <xsl:attribute name="value">
              <xsl:value-of select="$keyInstn"/>
            </xsl:attribute>
          </input>

          <input type="hidden" name="KeyFndg">
            <xsl:attribute name="value">
              <xsl:value-of select="$keyFndg"/>
            </xsl:attribute>
          </input>

          <input type="hidden" name="__period" id="__period" value=""/>

        </form>
      </TABLE>
    </xsl:if>
  </xsl:template>
  <!-- End Download Historical Data Dropdown -->

  <!-- REMOVE SELECTOR_STOCK FOR SL -->
  <xsl:template name="Descriptor_stock2">
    <xsl:if test="TradedIssues/RowCount > 1">
      <TR align="left">
        <TD valign="TOP" CLASS="default">
          <a>
            <xsl:attribute name="href">#</xsl:attribute>
            <xsl:attribute name="OnClick">
              window.open('fndgdesc.aspx?IID=<xsl:value-of select="Company/KeyInstn"/>', 'SecurityDescriptions', 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=400,height=300');
            </xsl:attribute>
            Select link to view descriptions of our traded funding issues.
          </a>
        </TD>
      </TR>
    </xsl:if>

    <xsl:if test="Company/ShowEmailNotificationLink = 'true'">
      <xsl:for-each select="CLIENTEMAILPREFS[NotificationType = '1' and NotificationEnabled = 'True']">
        <TR align="left">
          <TD valign="TOP" CLASS="default">
            <a>
              <xsl:attribute name="href">
                email.aspx?IID=<xsl:value-of select="../Company/KeyInstn"/>
              </xsl:attribute>
              Notify me with an end-of-day stock quote.
            </a>
          </TD>
        </TR>
      </xsl:for-each>
    </xsl:if>
  </xsl:template>
  <!-- REMOVE DESCRIPTOR_STOCK FOR SL -->

  <xsl:template name="Chart_Drop_Image_stock2">
    <xsl:variable name="isPreview" select="Company/IsPreview" />
    <TR align="left">
      <TD CLASS="defaultbold" valign="TOP">Current Chart</TD>
    </TR>
    <TR align="center">
      <TD CLASS="default" valign="TOP" style="padding-top: 10px;">
        <div id="graphContainer"></div>
          
          <xsl:choose>
          <xsl:when test="contains(Company/URL, 'print=1') or contains(Company/URL, 'Print=1')">
            <xsl:variable name="KeyInstn" select="Company/KeyInstn" />
            <xsl:variable name="KeyFndg" select="Company/KeyFndg" />
            <xsl:variable name="StartDate" select="PeriodDates[KeyPeriod = ../GraphOptions/Period]/StartDate" />
            <xsl:variable name="EndDate" select="PeriodDates[KeyPeriod = ../GraphOptions/Period]/EndDate" />
            <IMG>
              <xsl:attribute name="src">
                StockChartImage.aspx?iid=<xsl:value-of select="$KeyInstn"/>&#38;KeyFndg=<xsl:value-of select="$KeyFndg"/>&#38;StartDate=<xsl:value-of select="$StartDate"/>&#38;EndDate=<xsl:value-of select="$EndDate"/>
              </xsl:attribute>
            </img>
          </xsl:when>
          <xsl:otherwise>
            <IMG id="graph" style="display:none;" alt=""></img>
            <SPAN id="errorMsg" style="display:none;">The requested graph could not be constructed. Some data may not be available.  Please try changing the period or deselecting options.</SPAN>
            <embed id="LoadAnim" src="images/graphing/loading.swf" wmode="transparent" width="75" height="75" />
          </xsl:otherwise>
        </xsl:choose>
          
      </TD>
    </TR>
  </xsl:template>



  <!-- Template 2 -->
  <xsl:template name="UpdateChart2">
    <xsl:variable name="peerCount" select="PeerCount/Count"/>
    <xsl:variable name="indexCount" select="IndexCount/Count"/>
    <xsl:variable name="showPeers" select="GraphComponents/Peers"/>
    <xsl:variable name="keyInstn" select="Company/KeyInstn"/>
    <xsl:variable name="keyFndg" select="Company/KeyFndg"/>
    <xsl:variable name="showIndexes" select="GraphComponents/Indexes"/>
    <xsl:variable name="showSplits" select="GraphComponents/Splits"/>
    <xsl:variable name="showDivs" select="GraphComponents/Dividends"/>
    <xsl:variable name="showNews" select="GraphComponents/News"/>
    <xsl:variable name="showPRs" select="GraphComponents/PressReleases"/>
    <xsl:variable name="showFilings" select="GraphComponents/Filings"/>
    <xsl:variable name="showDocs" select="GraphComponents/Documents"/>
    <xsl:variable name="showDivYield" select="GraphComponents/DividendYield"/>
    <xsl:variable name="showReturn" select="GraphComponents/TotalReturn"/>
    <xsl:variable name="isPreview" select="Company/IsPreview" />
    <xsl:variable name="defaultPeriod" select="GraphOptions/Period" />
    <xsl:variable name="endDate" select="PeriodDates/EndDate" />
    <TABLE CELLPADDING="0" CELLSPACING="0" BORDER="0" WIDTH="100%">
      <form name="chartOptions" id="chartOptions"  method="POST">
        <!--action="StockInfoIFrame.aspx" target="theIFrame">-->
        <xsl:attribute name="onsubmit">
          expandIFrame(<xsl:value-of select="GraphSize/Width"/>,<xsl:value-of select="GraphSize/Height + 85"/>,<xsl:value-of select="$peerCount"/>,<xsl:value-of select="$indexCount"/>),document.getElementById("submitChart").disabled=true;
        </xsl:attribute>


        <TR>
          <TD>
            <TABLE ID="PeriodSelect" cellpadding="3" cellspacing="6" border="0" width="100%">
              <TR>
                <TD align="left" class="data" colspan="2">
                  <SPAN CLASS="defaultbold">Period: </SPAN>
					<label class="visuallyhidden" for="period">period</label>
                  <select id="period" name="period" class="input" >
                    <xsl:for-each select="PeriodDates">
                      <option>
                        <xsl:if test="$defaultPeriod = KeyPeriod">
                          <xsl:attribute name="Selected">1</xsl:attribute>
                        </xsl:if>
                        <xsl:attribute name="Value">
                          <xsl:value-of select="StartDate"/>
                        </xsl:attribute>
                        <xsl:value-of select="Desc"/>
                      </option>
                    </xsl:for-each>
                  </select><SPAN style="padding-left: 25px" class="defaultbold">Select Chart Type:</SPAN>&#32;<label class="visuallyhidden" for="TypeSelect">TypeSelect</label><select class="input" id="TypeSelect">
                    <option selected="selected" value="Area">Area</option>
                    <option value="Line">Line</option>
                    <option value="Column">Column</option>
                    <!--<option value="CandleStick">Candlestick</option>-->
                  </select>
                </TD>


                <xsl:if test="contains($isPreview, 'true')">
                  <input type="hidden" name="preview" value="1"/>
                </xsl:if>

                <input type="hidden" name="KeyInstn">
                  <xsl:attribute name="value">
                    <xsl:value-of select="$keyInstn"/>
                  </xsl:attribute>
                </input>

                <input type="hidden" name="KeyFndg">
                  <xsl:attribute name="value">
                    <xsl:value-of select="$keyFndg"/>
                  </xsl:attribute>
                </input>

                <input type="hidden" name="Peers">
                  <xsl:attribute name="value">
                    <xsl:value-of select="$peerCount"/>
                  </xsl:attribute>
                </input>

                <input type="hidden" name="Indexes">
                  <xsl:attribute name="value">
                    <xsl:value-of select="$indexCount"/>
                  </xsl:attribute>
                </input>

                <input type="hidden" id="EndDate1" name="EndDate1">
                  <xsl:attribute name="value">
                    <xsl:value-of select="$endDate"/>
                  </xsl:attribute>
                </input>
              </TR>
              <TR>
                <TD align="left" class="data" colspan="2">
                  <SPAN class="defaultbold">Start Date:&#32;</SPAN>
                  <input size="10" id="StartDate" type="text" class="input"/>
                  <SPAN style="padding-left: 10px" class="defaultbold">End Date:&#32;</SPAN>
                  <input size="10" id="EndDate2" type="text" class="input"/>
                </TD>
              </TR>
              <TR>
                <TD align="left" class="data" height="10px"></TD>
              </TR>
              <TR>
                <TD align="left" class="data">
                  <button class="submit" type="button" NAME="submitChart" ID="submitChart">Update Chart</button>
                </TD>
                <TD align="right" class="data">
                  <button class="submit" type="button" NAME="clearSelections" ID="clearSelections" onclick="this.form.reset()">Clear Selections</button>
                </TD>
              </TR>
            </TABLE>
          </TD>
        </TR>
        <TR>
          <TD>
            <TABLE border="0" width="100%" cellpadding="0" cellspacing="2">
              <TR>
                <!-- Peer checkboxes For Future release  -->
                <xsl:if test="contains($showPeers,'True')">
                  <TD valign="top">
                    <TABLE ID="PeerSelect" border="0" cellpadding="3" cellspacing="0" width="100%" class="colorlight">
                      <TR align="left">
                        <TD valign="middle" class="surr datashade" colspan="2">
                          <SPAN class="defaultbold">Select Peers</SPAN>
                        </TD>
                      </TR>
                      <TR align="left">
                        <xsl:for-each select="Peers">
                          <xsl:variable name="instn" select="KeyInstnPeer"/>
                          <xsl:variable name="ticker" select="Ticker"/>
                          <xsl:variable name="name" select="FormName"/>
                          <xsl:variable name="fullPeerName" select="InstnName"/>
                          <xsl:choose>
                            <xsl:when test="position() mod 2 = 0">
                              <TD class="data" valign="middle">
                                <input type="checkbox">
                                  <xsl:attribute name="name">
                                    <xsl:value-of select="$name"/>
                                  </xsl:attribute>
                                  <xsl:attribute name="id">
                                    <xsl:value-of select="$name"/>
                                  </xsl:attribute>
                                  <xsl:attribute name="value">
                                    <xsl:value-of select="$instn"/>
                                  </xsl:attribute>
                                  <xsl:attribute name="onclick">
                                    clickCounter(<xsl:value-of select="$peerCount"/>,<xsl:value-of select="$indexCount"/>,this)
                                  </xsl:attribute>
                                </input>
                                <acronym>
                                  <xsl:attribute name="title">
                                    <xsl:value-of select="$fullPeerName"/>
                                  </xsl:attribute>
                                  &#160;<xsl:value-of select="Ticker"/>&#160;
                                </acronym>
                              </TD>
                              <xsl:call-template name="CloseTR"/>
                            </xsl:when>

                            <xsl:when test="position() mod 2 = 1">
                              <xsl:call-template name="OpenTR"/>

                              <TD class="data" valign="middle">
                                <input type="checkbox">
                                  <xsl:attribute name="name">
                                    <xsl:value-of select="$name"/>
                                  </xsl:attribute>
                                  <xsl:attribute name="id">
                                    <xsl:value-of select="$name"/>
                                  </xsl:attribute>
                                  <xsl:attribute name="value">
                                    <xsl:value-of select="$instn"/>
                                  </xsl:attribute>
                                  <xsl:attribute name="onclick">
                                    clickCounter(<xsl:value-of select="$peerCount"/>,<xsl:value-of select="$indexCount"/>,this)
                                  </xsl:attribute>
                                </input>
                                <acronym>
                                  <xsl:attribute name="title">
                                    <xsl:value-of select="$fullPeerName"/>
                                  </xsl:attribute>
                                  &#160;<xsl:value-of select="Ticker"/>&#160;
                                </acronym>
                              </TD>
                            </xsl:when>
                            <xsl:otherwise>
                              <TD class="data" valign="middle">
                                <input type="checkbox">
                                  <xsl:attribute name="name">
                                    <xsl:value-of select="$name"/>
                                  </xsl:attribute>
                                  <xsl:attribute name="id">
                                    <xsl:value-of select="$name"/>
                                  </xsl:attribute>
                                  <xsl:attribute name="value">
                                    <xsl:value-of select="$instn"/>
                                  </xsl:attribute>
                                  <xsl:attribute name="onclick">
                                    clickCounter(<xsl:value-of select="$peerCount"/>,<xsl:value-of select="$indexCount"/>,this)
                                  </xsl:attribute>
                                </input>
                                <acronym>
                                  <xsl:attribute name="title">
                                    <xsl:value-of select="$fullPeerName"/>
                                  </xsl:attribute>
                                  &#160;<xsl:value-of select="Ticker"/>&#160;
                                </acronym>
                              </TD>
                            </xsl:otherwise>
                          </xsl:choose>
                        </xsl:for-each>

                        <xsl:if test="not(peerCount mod 2 = 0)">

                          <xsl:call-template name="CloseTR"/>

                        </xsl:if>
                      </TR>
                    </TABLE>
                  </TD>
                </xsl:if>

                <!-- Index Check boxes -->
                <xsl:if test="contains($showIndexes,'True')">

                  <xsl:if test="$indexCount &gt; 0">
                    <TD valign="top">
                      <TABLE ID="IndexSelect" border="0" cellpadding="3" cellspacing="0" width="100%">
                        <TR align="left">
                          <TD valign="middle" class="surr datashade" >
                            <SPAN class="defaultbold">Select Index</SPAN>
                          </TD>
                        </TR>

                        <xsl:for-each select="CompIndex">
                          <xsl:variable name="index" select="KeyIndex"/>
                          <xsl:variable name="name" select="FormName"/>

                          <TR align="left">
                            <TD class="data" valign="middle">
                              <input type="checkbox">
                                <xsl:attribute name="name">
                                  <xsl:value-of select="$name"/>
                                </xsl:attribute>
                                <xsl:attribute name="id">
                                  <xsl:value-of select="$name"/>
                                </xsl:attribute>
                                <xsl:attribute name="value">
                                  <xsl:value-of select="$index"/>
                                </xsl:attribute>
                                <xsl:attribute name="onclick">
                                  clickCounter(<xsl:value-of select="$peerCount"/>,<xsl:value-of select="$indexCount"/>,this)
                                </xsl:attribute>
                              </input>
                              <xsl:value-of select="IndexShortName"/>
                            </TD>
                          </TR>
                        </xsl:for-each>
                      </TABLE>
                    </TD>
                  </xsl:if>
                </xsl:if>
                <!-- End Index Check boxes -->


                <!-- Splits Divs Filings Documents -->
                <xsl:if test="contains($showSplits,'True') or contains($showDivs,'True') or contains($showNews,'True') or contains($showPRs,'True') or contains($showFilings,'True') or contains($showDocs,'True')">
                  <TD valign="top">
                    <TABLE ID="EventSelector" border="0" cellpadding="3" cellspacing="0" width="100%">
                      <TR align="left">
                        <TD valign="middle" class="surr datashade">
                          <SPAN class="defaultbold">Select Events</SPAN>
                        </TD>
                      </TR>

                      <xsl:if test="contains($showSplits,'True')">
                        <TR align="left">
                          <TD class="data" valign="middle">
                            <input type="checkbox" name="splits" value="Splits"/>
                            <IMG src="Images/graphing/leg_splits.gif" alt="Splits"/> Splits
                          </TD>
                        </TR>
                      </xsl:if>

                      <xsl:if test="contains($showDivs,'True')">
                        <TR align="left">
                          <TD class="data" valign="middle">
                            <input type="checkbox" name="divs" value="Divs"/>
                            <IMG src="Images/graphing/leg_dividends.gif" alt="Dividends"/> Dividends
                          </TD>
                        </TR>
                      </xsl:if>

                      <xsl:if test="contains($showNews,'True')">
                        <TR align="left">
                          <TD class="data" valign="middle">
                            <input type="checkbox" name="news" value="1"/>
                            <IMG src="Images/graphing/leg_newssummaries.gif" alt="News Summaries"/> News Summaries
                          </TD>
                        </TR>
                      </xsl:if>

                      <xsl:if test="contains($showPRs,'True')">
                        <TR align="left">
                          <TD class="data" valign="middle">
                            <input type="checkbox" name="prs" value="PRs"/>
                            <IMG src="Images/graphing/leg_pressreleases.gif" alt="Press Releases"/> Press Releases
                          </TD>
                        </TR>
                      </xsl:if>

                      <xsl:if test="contains($showFilings,'True')">
                        <TR align="left">
                          <TD class="data" valign="middle">
                            <input type="checkbox" name="filings" value="Filings"/>
                            <IMG src="Images/graphing/leg_filings.gif" alt="Filings"/> Filings
                          </TD>
                        </TR>
                      </xsl:if>

                      <xsl:if test="contains($showDocs,'True')">
                        <TR align="left">
                          <TD class="data" valign="middle">
                            <input type="checkbox" name="docs" value="Docs"/>
                            <IMG src="Images/graphing/leg_documents.gif" alt="Documents"/> Documents
                          </TD>
                        </TR>
                      </xsl:if>

                      <xsl:if test="contains($showSplits,'True') or contains($showDivs,'True') or contains($showNews,'True') or contains($showPRs,'True') or contains($showFilings,'True') or contains($showDocs,'True')">
                        <TR align="left">
                          <TD class="data" valign="middle">
                            <IMG src="Images/graphing/leg_multipleevents.gif" alt="Multiple Events"/> - Indicates Multiple Events
                          </TD>
                        </TR>
                      </xsl:if>
                    </TABLE>
                  </TD>
                </xsl:if>

                <!-- Performance Measures -->
                <xsl:if test="contains($showDivYield,'True') or contains($showReturn,'True')">
                  <TD valign="top">
                    <TABLE border="0" cellpadding="3" cellspacing="0" width="100%">
                      <TR align="left">
                        <TD valign="middle" class="surr datashade">
                          <SPAN class="defaultbold">Select Performance Measure</SPAN>
                        </TD>
                      </TR>

                      <xsl:if test="contains($showDivYield,'True')">
                        <TR align="left">
                          <TD class="data" valign="middle">
                            <input id="DivYield" type="checkbox" name="divyield" value="1">
                              <xsl:attribute name="onclick">
                                clickCounter(<xsl:value-of select="$peerCount"/>,<xsl:value-of select="$indexCount"/>,this)
                              </xsl:attribute>
                            </input>
                            Dividend Yield (%)
                          </TD>
                        </TR>
                      </xsl:if>

                      <xsl:if test="contains($showReturn,'True')">
                        <TR align="left">
                          <TD class="data" valign="middle">
                            <input id="TotalReturn" type="checkbox" name="treturn" value="1">
                              <xsl:attribute name="onclick">
                                clickCounter(<xsl:value-of select="$peerCount"/>,<xsl:value-of select="$indexCount"/>,this)
                              </xsl:attribute>
                            </input>
                            Total Return (%)
                          </TD>
                        </TR>
                      </xsl:if>

                    </TABLE>
                  </TD>
                </xsl:if>
              </TR>

            </TABLE>
          </TD>
        </TR>
      </form>
    </TABLE>
    <br />
  </xsl:template>
  <!-- End Template 2 -->

  <xsl:template name="FinancialDataTables2">
    <xsl:variable name="mytradingsymbol" select="/irw:IRW/irw:StockInformation/TradedIssues/TradingSymbol[../KeyFndg = /irw:IRW/irw:StockInformation/Company/KeyFndg]"/>

    <xsl:if test="Company/IsCommonStock='True'">
      <TR>
        <TD valign="top">
          <xsl:call-template name="Vol_Highlights_stock2"/>
        </TD>
      </TR>
      <TR>
        <TD valign="top" class="botbord">
          <IMG SRC="/images/Interactive/blank.gif" WIDTH="100%" HEIGHT="1" alt=""/>
        </TD>
      </TR>

      <xsl:if test="not(Company/GAAP=0)">
        <TR>
          <TD valign="top">
            <br />
            <xsl:call-template name="Fin_Data_stock2"/>
          </TD>
        </TR>
        <TR>
          <TD valign="top" class="botbord">
            <IMG SRC="/images/Interactive/blank.gif" WIDTH="100%" HEIGHT="1" alt=""/>
          </TD>
        </TR>
      </xsl:if>

      <TR>
        <TD valign="top">
          <br />
          <xsl:call-template name="Stock_Price_stock2"/>
        </TD>
      </TR>
      <TR>
        <TD valign="top" class="botbord">
          <IMG SRC="/images/Interactive/blank.gif" WIDTH="100%" HEIGHT="1" alt=""/>
        </TD>
      </TR>

      <xsl:if test="not(Company/GAAP=0)">
        <TR>
          <TD valign="top">
            <br />
            <xsl:call-template name="Price_Ratios_stock2"/>
          </TD>
        </TR>
      <TR>
        <TD valign="top" class="botbord">
          <IMG SRC="/images/Interactive/blank.gif" WIDTH="100%" HEIGHT="1" alt=""/>
        </TD>
      </TR>
      </xsl:if>
      <TR>
        <TD valign="top">
          <br />
          <xsl:call-template name="Price_Change_stock2"/>
        </TD>
      </TR>
      <TR>
        <TD valign="top" class="botbord">
          <IMG SRC="/images/Interactive/blank.gif" WIDTH="100%" HEIGHT="1" alt=""/>
        </TD>
      </TR>
      <TR>
        <TD valign="top">
          <br />
          <xsl:call-template name="Dividends_stock2"/>
        </TD>
      </TR>
      <TR>
        <TD valign="top" class="botbord">
          <IMG SRC="/images/Interactive/blank.gif" WIDTH="100%" HEIGHT="1" alt=""/>
        </TD>
      </TR>
    </xsl:if>

    <xsl:if test="Company/IsCommonStock='False'">
      <TR>
        <TD valign="top">
          <xsl:call-template name="Vol_Highlights_stock2"/>
        </TD>
      </TR>
      <TR>
        <TD valign="top" class="botbord">
          <IMG SRC="/images/Interactive/blank.gif" WIDTH="100%" HEIGHT="1" alt=""/>
        </TD>
      </TR>
      <TR>
        <TD valign="top">
          <br />
          <TABLE CELLSPACING="0" CELLPADDING="3" CLASS="" WIDTH="100%" BORDER="0" ID="TABLE10">
            <TR VALIGN="bottom">
              <TD CLASS="surrleft datashade defaultbold" WIDTH="40%">Price Change&#160;(%)</TD>
              <TD CLASS="surrright datashade defaultbold" align="right">
                <xsl:value-of select="$mytradingsymbol"/>
              </TD>
            </TR>
            <TR >
              <TD CLASS="data" NOWRAP="" align="LEFT" WIDTH="40%">
                <SPAN CLASS="defaultbold">One Day</SPAN>
              </TD>
              <TD CLASS="data" WIDTH="28%" align="right">
                <xsl:value-of select="format-number(PriceChange/OneDay,'#,##0.00 ;(#,##0.00)','data')"/>
                <SPAN CLASS="dataalign">&#160;</SPAN>
              </TD>
            </TR>
            <TR>
              <TD CLASS="data" NOWRAP="" align="LEFT" WIDTH="40%">
                <SPAN CLASS="defaultbold">One Month</SPAN>
              </TD>
              <TD CLASS="data" WIDTH="28%" align="right">
                <xsl:value-of select="format-number(PriceChange/OneMonth,'#,##0.00 ;(#,##0.00)','data')"/>
                <SPAN CLASS="dataalign">&#160;</SPAN>
              </TD>
            </TR>
            <TR >
              <TD CLASS="data" NOWRAP="" align="LEFT" WIDTH="40%">
                <SPAN CLASS="defaultbold">Three Month</SPAN>
              </TD>
              <TD CLASS="data" WIDTH="28%" align="right">
                <xsl:value-of select="format-number(PriceChange/ThreeMonth,'#,##0.00 ;(#,##0.00)','data')"/>
                <SPAN CLASS="dataalign">&#160;</SPAN>
              </TD>
            </TR>
            <TR >
              <TD CLASS="data" NOWRAP="" align="LEFT" WIDTH="40%">
                <SPAN CLASS="defaultbold">YTD</SPAN>
              </TD>
              <TD CLASS="data" WIDTH="28%" align="right">
                <xsl:value-of select="format-number(PriceChange/YTD,'#,##0.00 ;(#,##0.00)','data')"/>
                <SPAN CLASS="dataalign">&#160;</SPAN>
              </TD>
            </TR>
            <TR >
              <TD CLASS="data" NOWRAP="" align="LEFT" WIDTH="40%">
                <SPAN CLASS="defaultbold">One Year</SPAN>
              </TD>
              <TD CLASS="data" WIDTH="28%" align="right">
                <xsl:value-of select="format-number(PriceChange/OneYear,'#,##0.00 ;(#,##0.00)','data')"/>
                <SPAN CLASS="dataalign">&#160;</SPAN>
              </TD>
            </TR>
          </TABLE>
        </TD>
      </TR>
      <TR>
        <TD valign="top" class="botbord">
          <IMG SRC="/images/Interactive/blank.gif" WIDTH="100%" HEIGHT="1" alt=""/>
        </TD>
      </TR>
      <TR>
        <TD valign="top">
          <br />
          <xsl:call-template name="Stock_Price_stock2"/>
        </TD>
      </TR>
      <TR>
        <TD valign="top" class="botbord">
          <IMG SRC="/images/Interactive/blank.gif" WIDTH="100%" HEIGHT="1" alt=""/>
        </TD>
      </TR>
      <TR>
        <TD valign="top">
          <br />
          <xsl:call-template name="Dividends_stock2"/>
        </TD>
      </TR>
      <TR>
        <TD valign="top" class="botbord">
          <IMG SRC="/images/Interactive/blank.gif" WIDTH="100%" HEIGHT="1" alt=""/>
        </TD>
      </TR>
    </xsl:if>
  </xsl:template>



  <xsl:template name="Quote_Table_stock2">
    <xsl:variable name="keyinstn" select="Company/KeyInstn"/>
    <TABLE BORDER="0" CELLSPACING="1" CELLPADDING="2" WIDTH="100%">
      <TR VALIGN="BOTTOM">
        <TD colspan="3" CLASS="defaultbold">Current Quote</TD>
      </TR>
      <TR VALIGN="BOTTOM">
        <TD CLASS="" NOWRAP="" WIDTH="20%" valign="top">
          <SPAN CLASS="defaultbold">Last Sale</SPAN>
          <br />
          <SPAN class="default">
            &#36;&#160;<xsl:value-of select="format-number(StockQuote/CurrentPrice, '###,##0.00')"/>
          </SPAN>
        </TD>
        <TD CLASS="" NOWRAP="" WIDTH="20%" valign="top">
          <SPAN CLASS="defaultbold">Last Close</SPAN>
          <br />
          <SPAN class="default">
            <xsl:choose>
              <xsl:when test="StockQuote/LastPrice > 0">
                &#36;&#160;<xsl:value-of select="format-number(StockQuote/LastPrice, '###,##0.00')"/>
              </xsl:when>
              <xsl:otherwise>
                NA
              </xsl:otherwise>
            </xsl:choose>
          </SPAN>
        </TD>
        <TD WIDTH="60%" valign="top" rowspan="3">
          <TABLE width="100%" cellpadding="2" cellspacing="1" border="0">
            <TR>
              <TD CLASS="" valign="top" align="right">
                <SPAN class="default">
                  Enter a date below to request the
                  <br />closing price for that specific day:
                </SPAN>
              </TD>
            </TR>
            <TR>
              <TD valign="top" align="right">
                <input class="input" type="text" NAME="date" size="12" ID="snlstock">
                  <xsl:attribute name="onkeydown">
                    KeydownStock(<xsl:value-of select="Company/KeyInstn"/>,<xsl:value-of select="Company/KeyFndg"/>, event);
                  </xsl:attribute>
                </input>&#160;
                <input type="button" value="Enter" id="Button1"  class="submit" name="submit1">
                  <xsl:attribute name="OnClick">
                    window.open('quotepop.aspx?IID=<xsl:value-of select="Company/KeyInstn"/>&amp;KeyFndg=<xsl:value-of select="Company/KeyFndg"/>&amp;date=' + document.getElementById('snlstock').value, 'ClosingPrice', 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=60')
                  </xsl:attribute>
                </input>
                <br />
                <SPAN class="default">
                  Format: MM/DD/YY
                </SPAN>
              </TD>
            </TR>
          </TABLE>
        </TD>
      </TR>
      <TR VALIGN="BOTTOM">
        <TD CLASS="" NOWRAP="" valign="top">
          <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1850, 0); return false" title="Today's High">
            <SPAN CLASS="defaultbold">Today's High</SPAN>
          </a>
          <br />
          <SPAN class="default">
            &#36;&#160;
            <xsl:choose>
              <xsl:when test="StockQuote/HighToday= 0">NA</xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="format-number(StockQuote/HighToday, '###,##0.00')"/>
              </xsl:otherwise>
            </xsl:choose>
          </SPAN>
        </TD>
        <TD CLASS="" NOWRAP="" valign="top">
          <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1851, 0); return false" title="Today's Low">
            <SPAN CLASS="defaultbold">Today's Low</SPAN>
          </a>
          <br />
          <SPAN class="default">
            &#36;&#160;
            <xsl:choose>
              <xsl:when test="StockQuote/LowToday= 0">NA</xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="format-number(StockQuote/LowToday, '###,##0.00')"/>
              </xsl:otherwise>
            </xsl:choose>
          </SPAN>
        </TD>
      </TR>

      <TR align="CENTER" VALIGN="TOP">
        <TD CLASS="" NOWRAP="" align="left">
          <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1855, 0); return false" title="Net Change">
            <SPAN CLASS="defaultbold">Net Change</SPAN>
          </a>
          <br />
          <SPAN class="default">
            <xsl:variable name="NetChangeFromPreviousClose" select="StockQuote/NetChangeFromPreviousClose"/>
            <xsl:choose>
              <xsl:when test="$NetChangeFromPreviousClose != 0">
                <xsl:choose>
                  <xsl:when test="$NetChangeFromPreviousClose &gt; 0">
                    &#36;&#160;<xsl:value-of select="format-number($NetChangeFromPreviousClose,'###,##0.00','data')"/>&#160;
                  </xsl:when>
                  <xsl:otherwise>
                    &#36;&#160;<xsl:value-of select="$NetChangeFromPreviousClose"/>&#160;
                  </xsl:otherwise>
                </xsl:choose>
                <xsl:if test="StockQuote/NetChangeDirection = 'positive'">
                  <SPAN class="default">
                    <IMG SRC="/images/Interactive/IR/uptriangle.gif" WIDTH="11" HEIGHT="10" BORDER="0" ALT="Up" />&#160;
                  </SPAN>
                </xsl:if>
                <xsl:if test="StockQuote/NetChangeDirection = 'negative'">
                  <SPAN class="default">
                    <IMG SRC="/images/Interactive/IR/downtriangle.gif" WIDTH="11" HEIGHT="10" BORDER="0" ALT="Down" />&#160;
                  </SPAN>
                </xsl:if>
                <strong>
                  <xsl:if test="StockQuote/NetChangeDirection = 'negative'">(</xsl:if><xsl:value-of select="format-number(StockQuote/PercentChangeFromPreviousClose, '###,##0.00')"/><xsl:if test="StockQuote/NetChangeDirection = 'negative'">)</xsl:if>&#37;
                </strong>
              </xsl:when>
              <xsl:otherwise>
                NA
              </xsl:otherwise>
            </xsl:choose>
          </SPAN>
        </TD>

        <TD CLASS="" NOWRAP="" valign="top" align="left">
          <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 2454, 0); return false" title="Today's Volume">
            <SPAN CLASS="defaultbold">Today's Volume</SPAN>
          </a>
          <br />
          <SPAN class="default">
            <xsl:value-of select="StockQuote/VolumeToday"/>
          </SPAN>
        </TD>
      </TR>

      <TR>
        <TD height="1" class="" colspan="3" valign="middle">
          <TABLE width="100%" border="0" cellpadding="0" cellspacing="0">
            <TR>
              <TD class="colordark" height="1"></TD>
            </TR>
          </TABLE>
        </TD>
      </TR>

      <TR>
        <TD colspan="3" class="default">
          <SPAN class="defaultbold">Data as of</SPAN>&#160;
          <xsl:value-of select="StockQuote/QuoteTimeStamp"/>&#160;
          Minimum 20 minute delay
        </TD>
      </TR>
    </TABLE>
  </xsl:template>


  <xsl:template name="Vol_Highlights_stock2">
    <TABLE CELLSPACING="0" CELLPADDING="3" CLASS="" WIDTH="100%" BORDER="0" ID="TABLE10">
      <TR>
        <TD CLASS="surrleft datashade" WIDTH="40%" align="left">
          <SPAN class="titletest2">Volume Highlights</SPAN>
        </TD>
        <TD CLASS="surr_topbot datashade" align="right" WIDTH="28%" nowrap="">
          <SPAN class="titletest2">
            Avg Daily<br />Volume
          </SPAN>
        </TD>
        <TD CLASS="surrright datashade" align="right" WIDTH="32%">
          <SPAN class="titletest2">
            %&#160;of&#160;Shares<br />Outstanding
          </SPAN>
        </TD>
      </TR>
      <TR>
        <TD CLASS="data" NOWRAP="" align="LEFT" width="40%">
          <SPAN class="defaultbold">One Day</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="VolumeHighlights/OneDayAvgDailyVolume"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="32%" align="right">
          <xsl:value-of select="format-number(VolumeHighlights/OneDayPercentSharesOutstanding,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
      </TR>
      <TR>
        <TD CLASS="data" NOWRAP="" align="LEFT" width="40%">
          <SPAN class="defaultbold">One Month</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="VolumeHighlights/OneMonthAvgDailyVolume"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="32%" align="right">
          <xsl:value-of select="format-number(VolumeHighlights/OneMonthPercentSharesOutstanding,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
      </TR>
      <TR>
        <TD CLASS="data" NOWRAP="" align="LEFT" width="40%">
          <SPAN class="defaultbold">Three Months</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="VolumeHighlights/ThreeMonthsAvgDailyVolume"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="32%" align="right">
          <xsl:value-of select="format-number(VolumeHighlights/ThreeMonthsPercentSharesOutstanding,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
      </TR>
      <TR>
        <TD CLASS="data" NOWRAP="" align="LEFT" width="40%">
          <SPAN class="defaultbold"> YTD</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="VolumeHighlights/YTDAvgDailyVolume"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="32%" align="right">
          <xsl:value-of select="format-number(VolumeHighlights/YTDPercentSharesOutstanding,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
      </TR>
      <TR>
        <TD CLASS="data" NOWRAP="" align="LEFT" width="40%">
          <SPAN class="defaultbold">One Year</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="VolumeHighlights/OneYearAvgDailyVolume"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="32%" align="right">
          <xsl:value-of select="format-number(VolumeHighlights/OneYearPercentSharesOutstanding,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
      </TR>
    </TABLE>
  </xsl:template>

  <xsl:template name="Fin_Data_stock2">
    <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" CLASS="" WIDTH="100%" ID="TABLE11">
      <xsl:variable name="GAAPDomain" select="Company/GAAP"/>
      <xsl:variable name="INSURANCE_UNDERWRITER" select="8"/>
      <xsl:variable name="INSURANCE_BROKER" select="9"/>
      <xsl:variable name="THRIFT" select="2"/>
      <xsl:variable name="INVESTMENT_COMPANY" select="7"/>
      <xsl:variable name="REIT" select="6"/>
      <xsl:variable name="BANK" select="1"/>
      <xsl:variable name="ENERGY" select="10"/>
      <xsl:variable name="GAS" select="18"/>
      <xsl:variable name="FINTECH" select="15"/>
      <xsl:variable name="HOMEBUILDER" select="16"/>

      <xsl:variable name="COMMUNICATIONS" select="14"/>
      <xsl:variable name="MEDIA_ENT" select="20"/>
      <xsl:variable name="NEWMEDIA" select="21"/>
      <xsl:variable name="keyinstn" select="Company/KeyInstn"/>

      <xsl:choose>
        <xsl:when test="$GAAPDomain = $BANK or $GAAPDomain = $THRIFT">
          <TR VALIGN="TOP">
            <TD CLASS="surrleft datashade" NOWRAP="" WIDTH="60%" align="left">
              <SPAN class="titletest2">Financial Data</SPAN>
            </TD>
            <TD CLASS="surrright datashade" WIDTH="40%" align="right">
              <SPAN class="titletest2">
                <xsl:value-of select="FinancialData/DateEnded"/>
              </SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EquityPerShareForCalcs', 1); return false" title="Book Value ($)">
                <SPAN CLASS="defaultbold">Book Value ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/BookValue,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'TangibleEquityPerShare', 1); return false" title="Tangible Book ($)">
                <SPAN CLASS="defaultbold">Tangible Book ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/TangibleBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSAfterExtraForCalcs', 0); return false" title="Quarter EPS ($)">
                <SPAN CLASS="defaultbold">Quarter EPS ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/QuarterEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSAfterExtraForCalcs', 0); return false" title="EPS ($)">
                <SPAN CLASS="defaultbold">
                  <xsl:value-of select="Company/FiscalPeriodDisplay"/> EPS ($)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/EPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'CoreEPS', 0); return false" title="Core EPS ($)">
                <SPAN CLASS="defaultbold">
                  <xsl:value-of select="Company/FiscalPeriodDisplay"/> Core EPS ($)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/CoreEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $INVESTMENT_COMPANY">
          <TR VALIGN="TOP">
            <TD CLASS="surrleft datashade" NOWRAP="" WIDTH="60%" align="left">
              <SPAN class="titletest2">Financial Data</SPAN>
            </TD>
            <TD CLASS="surrright datashade" WIDTH="40%" align="right">
              <SPAN class="titletest2">
                <xsl:value-of select="FinancialData/DateEnded"/>
              </SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EquityPerShareForCalcs', 1); return false" title="Book Value ($)">
                <SPAN CLASS="defaultbold">Book Value ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/BookValue,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'QuarterEPS', 1); return false" title="Quarter EPS ($)">
                <SPAN CLASS="defaultbold">Quarter EPS ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/QuarterEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSAfterExtraForCalcs', 0); return false" title="EPS ($)">
                <SPAN CLASS="defaultbold">
                  <xsl:value-of select="Company/FiscalPeriodDisplay"/> EPS ($)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/EPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $ENERGY or $GAAPDomain = $GAS">
          <TR VALIGN="TOP">
            <TD CLASS="surrleft datashade" NOWRAP="" WIDTH="60%" align="left">
              <SPAN class="titletest2">Financial Data</SPAN>
            </TD>
            <TD CLASS="surrright datashade" WIDTH="40%" align="right">
              <SPAN class="titletest2">
                <xsl:value-of select="FinancialData/DateEnded"/>
              </SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'NetIncomeGrowth', 1); return false" title="Net Income Growth (%)">
                <SPAN CLASS="defaultbold">Net Income Growth (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/NetIncomeGrowth,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSBasic', 1); return false" title="Basic EPS ($)">
                <SPAN CLASS="defaultbold">Basic EPS ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/EPSBasic,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'DebtToTotalCapBookValue', 0); return false" title="Debt / Book Capitalization (%)">
                <SPAN CLASS="defaultbold">Debt / Book Capitalization (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/DebtToTotalCap,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'OperatingIncomeUtilityRpGrowth', 0); return false" title="Reported Net Operating Income Growth (%)">
                <SPAN CLASS="defaultbold">Reported Net Operating Income Growth (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/OperatingIncomeUtilityRpGrowth,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'OperatingAssetsEnergyGrowth', 0); return false" title="Operating Asset Growth (%)">
                <SPAN CLASS="defaultbold">Operating Asset Growth (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/OperatingAssetsEnergyGrowth,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $FINTECH">
          <TR VALIGN="TOP">
            <TD CLASS="surrleft datashade" NOWRAP="" WIDTH="60%" align="left">
              <SPAN class="titletest2">Financial Data</SPAN>
            </TD>
            <TD CLASS="surrright datashade" WIDTH="40%" align="right">
              <SPAN class="titletest2">
                <xsl:value-of select="FinancialData/DateEnded"/>
              </SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},988, 1); return false" title="Diluted EPS after Extra ($)">
                <SPAN CLASS="defaultbold">Diluted EPS after Extra ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/EPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},10180, 1); return false" title="LTM EPS after Extra ($)">
                <SPAN CLASS="defaultbold">LTM EPS after Extra ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/LTMEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},9827, 0); return false" title="Book Value ($)">
                <SPAN CLASS="defaultbold">Book Value ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/BookValue,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},13487, 0); return false" title="Tangible Book Value per Share ($)">
                <SPAN CLASS="defaultbold">Tangible Book Value per Share ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/TangibleBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $REIT">
          <TR VALIGN="TOP">
            <TD CLASS="surrleft datashade" NOWRAP="" WIDTH="60%" align="left">
              <SPAN class="titletest2">Quarterly Financial Data</SPAN>
            </TD>
            <TD CLASS="surrright datashade" WIDTH="40%" align="right">
              <SPAN class="titletest2">
                <xsl:value-of select="FinancialData/DateEnded"/>
              </SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'FFOPerShareForCalcs', 1); return false" title="FFO / Share ($)">
                <SPAN CLASS="defaultbold">FFO / Share ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/FFOPerShare,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'LTMFFOPerShare', 1); return false" title="FFO / Share ($)">
                <SPAN CLASS="defaultbold">
                  <xsl:value-of select="Company/FiscalPeriodDisplay"/> FFO / Share ($)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/LTMFFOPerShare,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'FADPerShare', 0); return false" title="FAD / Share ($)">
                <SPAN CLASS="defaultbold">FAD / Share ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/FADPerShare,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'FFOOperatingReported', 0); return false" title="Reported Operating FFO ($000)">
                <SPAN CLASS="defaultbold">Reported Operating FFO ($000)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="FinancialData/FFOOperatingReported"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'FFOOperatingReportedShare', 0); return false" title="Reported Operating FFO / Share ($)">
                <SPAN CLASS="defaultbold">Reported Operating FFO / Share ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/FFOOperatingReportedShare,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'SameStoreRevenueChange', 0); return false" title="Same-store NOI Change (%)">
                <SPAN CLASS="defaultbold">Same-store NOI Change (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/SameStoreRevenueChange,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'DebtToTotalCapFinl', 0); return false" title="Debt / Total Cap (%)">
                <SPAN CLASS="defaultbold">Debt / Total Cap (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/DebtToTotalCap,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $INSURANCE_BROKER or $GAAPDomain = $INSURANCE_UNDERWRITER">
          <TR VALIGN="TOP">
            <TD CLASS="surrleft datashade" NOWRAP="" WIDTH="60%" align="left">
              <SPAN class="titletest2">Financial Data</SPAN>
            </TD>
            <TD CLASS="surrright datashade" WIDTH="40%" align="right">
              <SPAN class="titletest2">
                <xsl:value-of select="FinancialData/DateEnded"/>
              </SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EquityPerShareForCalcs', 1); return false" title="Book Value ($)">
                <SPAN CLASS="defaultbold">Book Value ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/BookValue,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'TangibleEquityPerShare', 1); return false" title="Tangible Book ($)">
                <SPAN CLASS="defaultbold">Tangible Book ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/TangibleBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSAfterExtraForCalcs', 0); return false" title="Quarter EPS ($)">
                <SPAN CLASS="defaultbold">Quarter EPS ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/QuarterEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSAfterExtraForCalcs', 0); return false" title="EPS ($)">
                <SPAN CLASS="defaultbold">
                  <xsl:value-of select="Company/FiscalPeriodDisplay"/> EPS ($)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/EPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'CoreEPS', 0); return false" title="Operating EPS ($)">
                <SPAN CLASS="defaultbold">
                  <xsl:value-of select="Company/FiscalPeriodDisplay"/> Operating EPS ($)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/OperatingEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $HOMEBUILDER">
          <TR VALIGN="TOP">
            <TD CLASS="surrleft datashade" NOWRAP="" WIDTH="60%" align="left">
              <SPAN class="titletest2">Financial Data</SPAN>
            </TD>
            <TD CLASS="surrright datashade" WIDTH="40%" align="right">
              <SPAN class="titletest2">
                <xsl:value-of select="FinancialData/DateEnded"/>
              </SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EquityPerShareForCalcs', 1); return false" title="Book Value ($)">
                <SPAN CLASS="defaultbold">Book Value ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/BookValue,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSAfterExtraForCalcs', 0); return false" title="Quarter EPS ($)">
                <SPAN CLASS="defaultbold">Quarter EPS ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/QuarterEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSAfterExtraForCalcs', 0); return false" title="EPS ($)">
                <SPAN CLASS="defaultbold">
                  <xsl:value-of select="Company/FiscalPeriodDisplay"/> EPS ($)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/EPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'OperatingMarginBuilder', 0); return false" title="Operating Margin ($)">
                <SPAN CLASS="defaultbold">Operating Margin ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/OperatingMarginBuilder,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $MEDIA_ENT or $GAAPDomain = $COMMUNICATIONS or $GAAPDomain = $NEWMEDIA">
          <TR VALIGN="TOP">
            <TD CLASS="surrleft datashade" NOWRAP="" WIDTH="60%" align="left">
              <SPAN class="titletest2">Financial Data</SPAN>
            </TD>
            <TD CLASS="surrright datashade" WIDTH="40%" valign="bottom" align="right">
              <SPAN class="titletest2">
                <xsl:value-of select="FinancialData/DateEnded"/>
              </SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'TotalCapBookValue', 0); return false" title="Total Capitalization ($M)">
                <SPAN CLASS="defaultbold">Total Capitalization ($M)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="FinancialData/BookValue"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSAfterExtraForCalcs', 0); return false" title="Quarter EPS ($)">
                <SPAN CLASS="defaultbold">Quarter EPS ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/QuarterEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSAfterExtraForCalcs', 0); return false" title="EPS ($)">
                <SPAN CLASS="defaultbold">
                  <xsl:value-of select="Company/FiscalPeriodDisplay"/> EPS ($)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/EPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'NetIncomeMargin', 0); return false" title="Net Income Margin (%)">
                <SPAN CLASS="defaultbold">Net Income Margin (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/NetIncome,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:otherwise>
          <TR VALIGN="TOP">
            <TD CLASS="surrleft datashade" NOWRAP="" WIDTH="60%" align="left">
              <SPAN class="titletest2">Financial Data</SPAN>
            </TD>
            <TD CLASS="surrright datashade" WIDTH="40%" align="right">
              <SPAN class="titletest2">
                <xsl:value-of select="FinancialData/DateEnded"/>
              </SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EquityPerShareForCalcs', 1); return false" title="Book Value ($)">
                <SPAN CLASS="defaultbold">Book Value ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/BookValue,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'TangibleEquityPerShare', 1); return false" title="Tangible Book ($)">
                <SPAN CLASS="defaultbold">Tangible Book ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/TangibleBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSAfterExtraForCalcs', 0); return false" title="Quarter EPS ($)">
                <SPAN CLASS="defaultbold">Quarter EPS ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/QuarterEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSAfterExtraForCalcs', 0); return false" title="EPS ($)">
                <xsl:value-of select="Company/FiscalPeriodDisplay"/>
                <SPAN CLASS="defaultbold"> EPS ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/EPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:otherwise>
      </xsl:choose>
    </TABLE>
  </xsl:template>

  <xsl:template name="Stock_Price_stock2">
    <TABLE CELLPADDING="3" CELLSPACING="0" CLASS="" WIDTH="100%" BORDER="0" ID="TABLE13">
      <TR>
        <TD CLASS="surrleft datashade" WIDTH="40%">
          <SPAN class="titletest2">Stock Price History</SPAN>
        </TD>
        <TD CLASS="surr_topbot datashade" align="RIGHT" valign="MIDDLE" WIDTH="28%" nowrap="">
          <SPAN class="titletest2">High ($)</SPAN>
        </TD>
        <TD CLASS="surrright datashade" align="RIGHT" valign="MIDDLE" WIDTH="32%" nowrap="">
          <SPAN class="titletest2">Low ($)</SPAN>
        </TD>
      </TR>
      <TR>
        <TD CLASS="data" NOWRAP="" WIDTH="40%">
          <SPAN CLASS="defaultbold">One Month</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="format-number(StockPriceHistory/OneMonthHigh,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
        <TD class="data" WIDTH="32%" align="right">
          <xsl:value-of select="format-number(StockPriceHistory/OneMonthLow,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
      </TR>
      <TR>
        <TD CLASS="data" NOWRAP="" WIDTH="40%">
          <SPAN CLASS="defaultbold">Three Months</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="format-number(StockPriceHistory/ThreeMonthsHigh,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS=  "dataalign">&#160;</SPAN>
        </TD>
        <TD class="data" WIDTH="32%" align="right">
          <xsl:value-of select="format-number(StockPriceHistory/ThreeMonthsLow,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
      </TR>
      <TR>
        <TD CLASS="data" NOWRAP="" WIDTH="40%">
          <SPAN CLASS="defaultbold">Year-to-Date</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="format-number(StockPriceHistory/YTDHigh,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="32%" align="right">
          <xsl:value-of select="format-number(StockPriceHistory/YTDLow,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
      </TR>
      <TR>
        <TD CLASS="data" NOWRAP="" WIDTH="40%">
          <SPAN CLASS="defaultbold">One Year</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="format-number(StockPriceHistory/OneYearHigh,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="32%" align="right">
          <xsl:value-of select="format-number(StockPriceHistory/OneYearLow,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
      </TR>
      <TR>
        <TD CLASS="data" NOWRAP="" WIDTH="40%">
          <SPAN CLASS="defaultbold">Three Year</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="format-number(StockPriceHistory/ThreeYearsHigh,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="32%" align="right">
          <xsl:value-of select="format-number(StockPriceHistory/ThreeYearsLow,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
      </TR>
      <TR>
        <TD CLASS="data" NOWRAP="" WIDTH="40%">
          <SPAN CLASS="defaultbold">Three Year Date</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="StockPriceHistory/ThreeYearDateHigh"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="32%" align="right">
          <xsl:value-of select="StockPriceHistory/ThreeYearDateLow"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
      </TR>
    </TABLE>
  </xsl:template>

  <xsl:template name="Price_Ratios_stock2">
    <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" CLASS="" WIDTH="100%" ID="TABLE14">

      <xsl:variable name="GAAPDomain" select="Company/GAAP"/>
      <xsl:variable name="INSURANCE_BROKER" select="9"/>
      <xsl:variable name="INVESTMENT_COMPANY" select="7"/>
      <xsl:variable name="THRIFT" select="2"/>
      <xsl:variable name="INSURANCE" select="8"/>
      <xsl:variable name="REIT" select="6"/>
      <xsl:variable name="BANK" select="1"/>
      <xsl:variable name="ENERGY" select="10"/>
      <xsl:variable name="GAS" select="18"/>
      <xsl:variable name="FINTECH" select="15"/>
      <xsl:variable name="HOMEBUILDER" select="16"/>
      <xsl:variable name="COMMUNICATIONS" select="14"/>
      <xsl:variable name="MEDIA_ENT" select="20"/>
      <xsl:variable name="NEWMEDIA" select="21"/>
      <xsl:variable name="keyinstn" select="Company/KeyInstn" />
      <xsl:variable name="keyspecialtaxstatus" select="Company/KeySpecialTaxStatus" />
      <xsl:choose>
        <xsl:when test="$GAAPDomain = $BANK or $GAAPDomain = $THRIFT">
          <TR VALIGN="TOP">
            <TD CLASS="surr datashade" NOWRAP="" colspan="2">
              <SPAN class="titletest2">Current Pricing Ratios</SPAN>
            </TD>
          </TR>
          <TR valign="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 610, 0); return false" title="Price / Book (%)">
                <SPAN CLASS="defaultbold">Price / Book (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1843, 0); return false" title="Price / Tangible Book (%)">
                <SPAN CLASS="defaultbold">Price / Tangible Book (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToTangibleBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 609, 0); return false" title="Price / Earnings (x)">
                <SPAN CLASS="defaultbold">Price / Earnings (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1842, 0); return false" title="Price / EPS (x)">
                <SPAN CLASS="defaultbold">
                  Price / <xsl:value-of select="Company/FiscalPeriodDisplay"/> EPS (x)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9637, 0); return false" title="Price / Core EPS (x)">
                <SPAN CLASS="defaultbold">
                  Price / <xsl:value-of select="Company/FiscalPeriodDisplay"/> Core EPS (x)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToCoreEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $HOMEBUILDER">
          <TR VALIGN="TOP">
            <TD CLASS="surr datashade" NOWRAP="" colspan="2">
              <SPAN class="titletest2">Current Pricing Ratios</SPAN>
            </TD>
          </TR>
          <TR valign="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 610, 0); return false" title="Price / Book (%)">
                <SPAN CLASS="defaultbold">Price / Book (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9632, 0); return false" title="Price / Earnings (x)">
                <SPAN CLASS="defaultbold">Price / Earnings (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEarningsBeforeExtra,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9631, 0); return false" title="Price/ LTM EPS (x)">
                <SPAN CLASS="defaultbold">Price/ LTM EPS (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEarningsLTMBeforeExtra,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $INVESTMENT_COMPANY">
          <TR VALIGN="TOP">
            <TD CLASS="surr datashade" NOWRAP="" colspan="2">
              <SPAN class="titletest2">Current Pricing Ratios</SPAN>
            </TD>
          </TR>
          <TR valign="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 610, 0); return false" title="Price / Book (%)">
                <SPAN CLASS="defaultbold">Price / Book (%) </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1843, 0); return false" title="Price / Earnings (x)">
                <SPAN CLASS="defaultbold">Price / Earnings (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 609, 0); return false" title="Price / EPS (x)">
                <SPAN CLASS="defaultbold">
                  Price / <xsl:value-of select="Company/FiscalPeriodDisplay"/> EPS (x)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1842, 0); return false" title="Price / EBITDA (x)">
                <SPAN CLASS="defaultbold">Price / EBITDA (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEBITDA,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $REIT">
          <TR VALIGN="TOP">
            <TD CLASS="surr datashade" NOWRAP="" colspan="2">
              <SPAN class="titletest2">Current Pricing Ratios</SPAN>
            </TD>
          </TR>
          <xsl:choose>
            <xsl:when test="$keyspecialtaxstatus = '0'">
              <TR valign="TOP">
                <TD CLASS="data" NOWRAP="" WIDTH="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 610, 0); return false" title="Price / FFO (x)">
                    <SPAN CLASS="defaultbold">Price / FFO (x)</SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(CurrentPricingRatios/PriceToFFO,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
              <TR VALIGN="TOP">
                <TD CLASS="data" NOWRAP="" WIDTH="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1843, 0); return false" title="FFO Yield (%)">
                    <SPAN CLASS="defaultbold">FFO Yield (%)</SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(CurrentPricingRatios/FFOYield,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
              <TR VALIGN="TOP">
                <TD CLASS="data" NOWRAP="" WIDTH="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 609, 0); return false" title="Price / FFO (x)">
                    <SPAN CLASS="defaultbold">
                      Price / <xsl:value-of select="Company/FiscalPeriodDisplay"/> FFO (x)
                    </SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(CurrentPricingRatios/PriceToLTMFFO,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
              <TR VALIGN="TOP">
                <TD CLASS="data" NOWRAP="" WIDTH="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1842, 0); return false" title="Price / EBITDA (x)">
                    <SPAN CLASS="defaultbold">Price / EBITDA (x)</SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEBITDA,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
              <TR VALIGN="TOP">
                <TD CLASS="data" NOWRAP="" WIDTH="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9637, 0); return false" title="Price / Earnings (x)">
                    <SPAN CLASS="defaultbold">Price / Earnings (x)</SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
              <TR VALIGN="TOP">
                <TD CLASS="data" NOWRAP="" WIDTH="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9637, 0); return false" title="Price / Earnings (x)">
                    <SPAN CLASS="defaultbold">
                      Price / <xsl:value-of select="Company/FiscalPeriodDisplay"/> Earnings (x)
                    </SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(CurrentPricingRatios/PriceToLTMEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
            </xsl:when>
            <xsl:otherwise>
              <TR valign="TOP">
                <TD CLASS="data" NOWRAP="" WIDTH="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9633, 0); return false" title="Price / FFO (x)">
                    <SPAN CLASS="defaultbold">Price / FFO (x)</SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(CurrentPricingRatios/PriceToFFO,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
              <TR VALIGN="TOP">
                <TD CLASS="data" NOWRAP="" WIDTH="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1843, 0); return false" title="FFO Yield (%)">
                    <SPAN CLASS="defaultbold">FFO Yield (%)</SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(CurrentPricingRatios/FFOYield,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
              <TR VALIGN="TOP">
                <TD CLASS="data" NOWRAP="" WIDTH="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 609, 0); return false" title="Price / FFO (x)">
                    <SPAN CLASS="defaultbold">
                      Price / <xsl:value-of select="Company/FiscalPeriodDisplay"/> FFO (x)
                    </SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(CurrentPricingRatios/PriceToLTMFFO,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
              <TR VALIGN="TOP">
                <TD CLASS="data" NOWRAP="" WIDTH="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1842, 0); return false" title="Price / EBITDA (x)">
                    <SPAN CLASS="defaultbold">Price / EBITDA (x)</SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEBITDA,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
              <TR VALIGN="TOP">
                <TD CLASS="data" NOWRAP="" WIDTH="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9637, 0); return false" title="Price / Earnings (x)">
                    <SPAN CLASS="defaultbold">Price / Earnings (x)</SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
              <TR VALIGN="TOP">
                <TD CLASS="data" NOWRAP="" WIDTH="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9637, 0); return false" title="Price / Earnings (x)">
                    <SPAN CLASS="defaultbold">
                      Price / <xsl:value-of select="Company/FiscalPeriodDisplay"/> Earnings (x)
                    </SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(CurrentPricingRatios/PriceToLTMEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $INSURANCE_BROKER or $GAAPDomain = $INSURANCE">
          <TR VALIGN="TOP">
            <TD CLASS="surr datashade" NOWRAP="" colspan="2">
              <SPAN class="titletest2">Current Pricing Ratios</SPAN>
            </TD>
          </TR>
          <TR valign="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 610, 0); return false" title="Price / Book (%)">
                <SPAN CLASS="defaultbold">Price / Book (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1843, 0); return false" title="Price / Tangible Book (%)">
                <SPAN CLASS="defaultbold">Price / Tangible Book (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToTangibleBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1843, 0); return false" title="Price / Book, excl. 115 (%)">
                <SPAN CLASS="defaultbold">Price / Book, excl. 115 (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToBookExcluding115,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 609, 0); return false" title="Price / Earnings (x)">
                <SPAN CLASS="defaultbold">Price / Earnings (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1842, 0); return false" title="Price / EPS (x)">
                <SPAN CLASS="defaultbold">
                  Price / <xsl:value-of select="Company/FiscalPeriodDisplay"/> EPS (x)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9637, 0); return false" title="Price / Operating EPS (x)">
                <SPAN CLASS="defaultbold">
                  Price / <xsl:value-of select="Company/FiscalPeriodDisplay"/> Operating EPS (x)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToOperatingEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $ENERGY or $GAAPDomain = $GAS">
          <TR VALIGN="TOP">
            <TD CLASS="surr datashade" NOWRAP="" colspan="2">
              <SPAN class="titletest2">Current Pricing Ratios</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1842, 0); return false" title="Price / EBITDA (x)">
                <SPAN CLASS="defaultbold">Price / EBITDA (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEBITDA,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9632, 0); return false" title="Price / Earnings (x)">
                <SPAN CLASS="defaultbold">Price / Earnings (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEarningsBeforeExtra,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR valign="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9631, 0); return false" title="Price / LTM Earnings (x)">
                <SPAN CLASS="defaultbold">Price / LTM Earnings (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToLTMEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $FINTECH">
          <TR VALIGN="TOP">
            <TD CLASS="surr datashade" NOWRAP="" colspan="2">
              <SPAN class="titletest2">Current Pricing Ratios</SPAN>
            </TD>
          </TR>
          <TR valign="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 610, 0); return false" title="Price / Book (%)">
                <SPAN CLASS="defaultbold">Price / Book (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1843, 0); return false" title="Price / Tangible Book (%)">
                <SPAN CLASS="defaultbold">Price / Tangible Book (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToTangibleBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9632, 0); return false" title="Price / Earnings (x)">
                <SPAN CLASS="defaultbold">Price / Earnings (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR valign="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9631, 0); return false" title="Price / LTM Earnings (x)">
                <SPAN CLASS="defaultbold">Price / LTM Earnings (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToLTMEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9639, 0); return false" title="Price / EBITDA (x)">
                <SPAN CLASS="defaultbold">Price / EBITDA (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEBITDA,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $MEDIA_ENT or $GAAPDomain = $COMMUNICATIONS or $GAAPDomain = $NEWMEDIA">
          <TR VALIGN="TOP">
            <TD CLASS="surr datashade" NOWRAP="" colspan="2">
              <SPAN class="titletest2">Current Pricing</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 609, 0); return false" title="Price / Earnings (x)">
                <SPAN CLASS="defaultbold">Price / Earnings (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1842, 0); return false" title="Price / EPS (x)">
                <SPAN CLASS="defaultbold">
                  Price / <xsl:value-of select="Company/FiscalPeriodDisplay"/> EPS (x)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:otherwise>
          <TR VALIGN="TOP">
            <TD CLASS="surr datashade" NOWRAP="" colspan="2">
              <SPAN class="titletest2">Current Pricing Ratios</SPAN>
            </TD>
          </TR>
          <TR valign="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 610, 0); return false" title="Price / Book (%)">
                <SPAN CLASS="defaultbold">Price / Book (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1843, 0); return false" title="Price / Tangible Book (%)">
                <SPAN CLASS="defaultbold">Price / Tangible Book (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToTangibleBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 609, 0); return false" title="Price / Earnings (x)">
                <SPAN CLASS="defaultbold">Price / Earnings (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1842, 0); return false" title="Price / EPS (x)">
                <SPAN CLASS="defaultbold">
                  Price /<xsl:value-of select="Company/FiscalPeriodDisplay"/> EPS (x)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:otherwise>
      </xsl:choose>
    </TABLE>
  </xsl:template>

  <xsl:template name="Price_Change_stock2">
    <xsl:variable name="mytradingsymbol" select="/irw:IRW/irw:StockInformation/TradedIssues/TradingSymbol[../KeyFndg = /irw:IRW/irw:StockInformation/Company/KeyFndg]"/>
    <TABLE CELLPADDING="3" CELLSPACING="0" CLASS="" WIDTH="100%" BORDER="0" ID="TABLE16">
      <TR>
        <TD CLASS="surrleft datashade" WIDTH="40%">
          <SPAN class="titletest2">Price Change&#160;(%)</SPAN>
        </TD>
        <TD class="surrright datashade" align="right" width="60%">
        <xsl:if test="Company/ComparativeIndexName != ''">
          <xsl:attribute name="class">surr_topbot datashade</xsl:attribute>
          <xsl:attribute name="width">28%</xsl:attribute>
        </xsl:if>
          <SPAN class="titletest2">
            <xsl:value-of select="$mytradingsymbol"/>
          </SPAN>&#160;&#160;
        </TD>
        <xsl:if test="Company/ComparativeIndexName != ''">
          <TD CLASS="surrright datashade" align="right" WIDTH="32%">
            <SPAN class="titletest2">
              <xsl:value-of select="Company/ComparativeIndexName"/> Index
            </SPAN>
          </TD>
        </xsl:if>
      </TR>
      <TR >
        <TD CLASS="data" NOWRAP="" align="LEFT" WIDTH="40%">
          <SPAN CLASS="defaultbold">One Day</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="format-number(PriceChange/OneDay,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
          <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
        </TD>
        <xsl:if test="Company/ComparativeIndexName != ''">
          <TD CLASS="data" WIDTH="28%" align="right">
            <xsl:value-of select="format-number(PriceChange/OneDayIndex,'#,##0.00 ;(#,##0.00)','data')"/>
            <SPAN CLASS="dataalign">&#160;</SPAN>
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </TD>
        </xsl:if>
      </TR>
      <TR >
        <TD CLASS="data" NOWRAP="" align="LEFT" WIDTH="40%">
          <SPAN CLASS="defaultbold">One Month</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="format-number(PriceChange/OneMonth,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
          <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
        </TD>
        <xsl:if test="Company/ComparativeIndexName != ''">
          <TD CLASS="data" WIDTH="28%" align="right">
            <xsl:value-of select="format-number(PriceChange/OneMonthIndex,'#,##0.00 ;(#,##0.00)','data')"/>
            <SPAN CLASS="dataalign">&#160;</SPAN>
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </TD>
        </xsl:if>
      </TR>
      <TR >
        <TD CLASS="data" NOWRAP="" align="LEFT" WIDTH="40%">
          <SPAN CLASS="defaultbold">Three Month</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="format-number(PriceChange/ThreeMonth,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
          <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
        </TD>
        <xsl:if test="Company/ComparativeIndexName != ''">
          <TD CLASS="data" WIDTH="28%" align="right">
            <xsl:value-of select="format-number(PriceChange/ThreeMonthIndex,'#,##0.00 ;(#,##0.00)','data')"/>
            <SPAN CLASS="dataalign">&#160;</SPAN>
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </TD>
        </xsl:if>
      </TR>
      <TR >
        <TD CLASS="data" NOWRAP="" align="LEFT" WIDTH="40%">
          <SPAN CLASS="defaultbold">YTD</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="format-number(PriceChange/YTD,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
          <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
        </TD>
        <xsl:if test="Company/ComparativeIndexName != ''">
          <TD CLASS="data" WIDTH="28%" align="right">
            <xsl:value-of select="format-number(PriceChange/YTDIndex,'#,##0.00 ;(#,##0.00)','data')"/>
            <SPAN CLASS="dataalign">&#160;</SPAN>
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </TD>
        </xsl:if>
      </TR>
      <TR >
        <TD CLASS="data" NOWRAP="" align="LEFT" WIDTH="40%">
          <SPAN CLASS="defaultbold">One Year</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="format-number(PriceChange/OneYear,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
          <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
        </TD>
        <xsl:if test="Company/ComparativeIndexName != ''">
          <TD CLASS="data" WIDTH="28%" align="right">
            <xsl:value-of select="format-number(PriceChange/OneYearIndex,'#,##0.00 ;(#,##0.00)','data')"/>
            <SPAN CLASS="dataalign">&#160;</SPAN>
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </TD>
        </xsl:if>
      </TR>
    </TABLE>
  </xsl:template>

  <xsl:template name="Dividends_stock2">
    <xsl:variable name="divkey">
      <xsl:choose>
        <xsl:when test="Dividends/IsDivAggregate = 'True'">46843</xsl:when>
        <xsl:otherwise>9640</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="divyieldkey">
      <xsl:choose>
        <xsl:when test="Dividends/IsDivAggregate = 'True'">46844</xsl:when>
        <xsl:otherwise>2425</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" CLASS="" WIDTH="100%" ID="TABLE17">
      <xsl:variable name="GAAPDomain" select="Company/GAAP"/>
      <xsl:variable name="REIT" select="6"/>
      <xsl:variable name="ENERGY" select="10"/>
      <xsl:variable name="GAS" select="18"/>
      <xsl:variable name="FINTECH" select="15"/>
      <xsl:variable name="keyinstn" select="Company/KeyInstn" />
      <xsl:variable name="keyspecialtaxstatus" select="Company/KeySpecialTaxStatus" />
      <xsl:choose>
        <xsl:when test="$GAAPDomain = $REIT">
          <TR VALIGN="TOP">
            <TD colspan="2" CLASS="surr datashade">
              <SPAN class="titletest2">Dividends</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, {$divkey}, 0); return false" title="Quarterly Dividend ($)"> 
                <SPAN CLASS="defaultbold">Quarterly Dividend ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" width="40%" align="right">
              <xsl:value-of select="format-number(Dividends/QuarterlyDividend,'#,##0.0000 ;(#,##0.0000)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, {$divyieldkey}, 0); return false" title="Dividend Yield (%)"> 
                <SPAN CLASS="defaultbold">Dividend Yield (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" width="40%" align="right">
              <xsl:value-of select="format-number(Dividends/DividendYield,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <xsl:if test="Company/IsCommonStock='True'">
            <TR VALIGN="TOP">
              <!--### Change decimal places to 4 as per "Quarterly Dividend" above ###-->
              <TD CLASS="data" width="60%">
                <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 'Dividend', 0); return false" title="Dividends Announced ($)">
                  <SPAN CLASS="defaultbold">
                    <xsl:value-of select="Company/FiscalPeriodDisplay"/> Dividends Announced ($)
                  </SPAN>
                </a>
              </TD>
              <TD CLASS="data" width="40%" align="right">
                <xsl:value-of select="format-number(Dividends/Dividend,'#,##0.0000 ;(#,##0.0000)','data')"/>
                <SPAN CLASS="dataalign">&#160;</SPAN>
              </TD>
            </TR>
            <TR VALIGN="TOP">
              <xsl:choose>
                <xsl:when test="$keyspecialtaxstatus = '0'">
                  <TD CLASS="data" width="60%">
                    <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 'DividendPayout', 0); return false" title="Payout Ratio (%)">
                      <SPAN CLASS="defaultbold">
                        <xsl:value-of select="Company/FiscalPeriodDisplay"/> Payout Ratio (%)
                      </SPAN>
                    </a>
                  </TD>
                </xsl:when>
                <xsl:otherwise>
                  <TD CLASS="data" width="60%">
                    <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 'DividendPayoutFFO', 0); return false" title="Payout Ratio (%)">
                      <SPAN CLASS="defaultbold">
                        <xsl:value-of select="Company/FiscalPeriodDisplay"/> Payout Ratio (%)
                      </SPAN>
                    </a>
                  </TD>
                </xsl:otherwise>
              </xsl:choose>
              <TD CLASS="data" width="40%" align="right">
                <xsl:value-of select="format-number(Dividends/LTMPayoutRatio,'#,##0.00 ;(#,##0.00)','data')"/>
                <SPAN CLASS="dataalign">&#160;</SPAN>
              </TD>
            </TR>
          </xsl:if>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $ENERGY or $GAAPDomain = $GAS">
          <TR VALIGN="TOP">
            <TD colspan="2" CLASS="surr datashade">
              <SPAN class="titletest2">Dividends</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 13498, 0); return false" title="Dividend Paid ($)">
                <SPAN CLASS="defaultbold">Dividend Paid ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" width="40%" align="right">
              <xsl:value-of select="format-number(Dividends/DividendPaid,'#,##0.0000 ;(#,##0.0000)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 3249, 0); return false" title="Dividend Payout (%)">
                <SPAN CLASS="defaultbold">Dividend Payout (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" width="40%" align="right">
              <xsl:value-of select="format-number(Dividends/DividendPayout,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9935, 0); return false" title="Dividend / Book Per Share (x)">
                <SPAN CLASS="defaultbold">Dividend / Book Per Share (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" width="40%" align="right">
              <xsl:value-of select="format-number(Dividends/DividendToBookPerShare,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $FINTECH">
          <TR VALIGN="TOP">
            <TD colspan="2" CLASS="surr datashade">
              <SPAN class="titletest2">Dividends</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, {$divkey}, 0); return false" title="Quarterly Dividend ($)">
                <SPAN CLASS="defaultbold">Quarterly Dividend ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" width="40%" align="right">
              <xsl:value-of select="format-number(Dividends/QuarterlyDividend,'#,##0.0000 ;(#,##0.0000)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, {$divyieldkey}, 0); return false" title="Dividend Yield (%)">
                <SPAN CLASS="defaultbold">Dividend Yield (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" width="40%" align="right">
              <xsl:value-of select="format-number(Dividends/DividendYield,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <!--### Change decimal places to 4 as per "Quarterly Dividend" above ###-->
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 20350, 0); return false" title="LTM Dividends Announced ($)">
                <SPAN CLASS="defaultbold">LTM Dividends Announced ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" width="40%" align="right">
              <xsl:value-of select="format-number(Dividends/DivAnnouncedLTM,'#,##0.0000 ;(#,##0.0000)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 3249, 0); return false" title="Dividend Payout Ratio (%)">
                <SPAN CLASS="defaultbold">Dividend Payout Ratio (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" width="40%" align="right">
              <xsl:value-of select="format-number(Dividends/LTMPayoutRatio,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:otherwise>
          <TR VALIGN="TOP">
            <TD colspan="2" CLASS="surr datashade">
              <SPAN class="titletest2">Dividends</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, {$divkey}, 0); return false" title="Quarterly Dividend ($)">
                <SPAN CLASS="defaultbold">Quarterly Dividend ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" width="40%" align="right">
              <xsl:value-of select="format-number(Dividends/QuarterlyDividend,'#,##0.0000 ;(#,##0.0000)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, {$divyieldkey}, 0); return false" title="Dividend Yield (%)">
                <SPAN CLASS="defaultbold">Dividend Yield (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" width="40%" align="right">
              <xsl:value-of select="format-number(Dividends/DividendYield,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <xsl:if test="Company/IsCommonStock='True'">
            <TR VALIGN="TOP">
              <!--### Change decimal places to 4 as per "Quarterly Dividend" above ###-->
              <TD CLASS="data" width="60%">
                <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 'Dividend', 0); return false" title="Dividend ($)">
                  <SPAN CLASS="defaultbold">
                    <xsl:value-of select="Company/FiscalPeriodDisplay"/> Dividend ($)
                  </SPAN>
                </a>
              </TD>
              <TD CLASS="data" width="40%" align="right">
                <xsl:value-of select="format-number(Dividends/Dividend,'#,##0.0000 ;(#,##0.0000)','data')"/>
                <SPAN CLASS="dataalign">&#160;</SPAN>
              </TD>
            </TR>
            <xsl:if test="Company/GAAP != 0">
            <TR VALIGN="TOP">
              <TD CLASS="data" width="60%">
                <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 'DividendPayout', 0); return false" title="Payout Ratio (%)">
                  <SPAN CLASS="defaultbold">
                    <xsl:value-of select="Company/FiscalPeriodDisplay"/> Payout Ratio (%)
                  </SPAN>
                </a>
              </TD>
              <TD CLASS="data" width="40%" align="right">
                <xsl:value-of select="format-number(Dividends/PayoutRatio,'#,##0.00 ;(#,##0.00)','data')"/>
                <SPAN CLASS="dataalign">&#160;</SPAN>
              </TD>
            </TR>
            </xsl:if>
          </xsl:if>
        </xsl:otherwise>
      </xsl:choose>
    </TABLE>
  </xsl:template>
  <!-- Template ONE Ends here. -->



  <!-- This is Template THREE option in the console under 'Style Template' -->
  <xsl:template name="Selector_Stock3">
    <xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
    <xsl:variable name="TradedIssuesCount" select="TradedIssues/RowCount"/>
    <xsl:choose>
      <xsl:when test="$TradedIssuesCount > 1">
        <TR>
          <TD CLASS="" nowrap="" align="left" valign="top">
            <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" class="table2 datashade">
              <TR>
                <TD CLASS="defaultbold">
                  <SPAN class=" defaultbold">Security:</SPAN>
                </TD>
                <TD CLASS="default" align="left">
                  <xsl:variable name="CurrentKeyFndg" select="Company/KeyFndg"/>
					<label class="visuallyhidden" for="KeyFndg">KeyFndg</label>
                  <select NAME="KeyFndg" ID="KeyFndg" size="1" class="input">
                    <xsl:for-each select="TradedIssues">
                      <xsl:value-of select="KeyFndg"/>
                      <option>

                        <xsl:attribute name="value">
                          <xsl:value-of select="KeyFndg"/>
                        </xsl:attribute>
                        <xsl:if test="$CurrentKeyFndg=KeyFndg">
                          <xsl:attribute name="SELECTED">1</xsl:attribute>
                        </xsl:if>
                        <xsl:value-of select="TradingSymbol"/>
                      </option>
                    </xsl:for-each>
                  </select>
                </TD>
                <TD CLASS="default" align="left">
                  <input class="submit" type="submit" NAME="submit" value="Apply" align="left" ID="Submit1"/>
                </TD>
              </TR>
            </TABLE>
          </TD>
        </TR>
      </xsl:when>
    </xsl:choose>

  </xsl:template>

  <xsl:template name="Chart_Drop_stock3">
    <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%">
      <xsl:call-template name="Chart_Drop_Image_stock3"/>
    </TABLE>
    <br />
  </xsl:template>

  <xsl:template name="ChartOptions3">
    <TABLE border="0" width="100%" cellpadding="3" cellspacing="3" class="data">
      <TR align="left">
        <TD valign="top">
          <xsl:call-template name="UpdateChart3"/>
        </TD>
      </TR>

      <xsl:if test="contains(GraphComponents/AllowDataDownload,'True')">
        <TR align="left">
          <TD valign="top" class="table2 datashade">
            <SPAN class="defaultbold">Downloads</SPAN>
          </TD>
        </TR>
        <TR align="left" class="data">
          <TD valign="middle">

            <xsl:call-template name="DownloadHistData3"/>
            <xsl:if test="contains(GraphComponents/AllowDataDownload,'False')">&#160;</xsl:if>

          </TD>
        </TR>
        <TR align="left" class="data">
          <TD valign="middle">

            <xsl:call-template name="DownloadChart3"/>
            <xsl:if test="contains(GraphOptions/Small,'False') or contains(GraphOptions/Medium,'False') or contains(GraphOptions/Large,'False')">&#160;</xsl:if>

          </TD>
        </TR>
      </xsl:if>
    </TABLE>
  </xsl:template>

  <!--New Code add 'DownloadHistData3 and DownloadChart3' Template. -->

  <xsl:template name="DownloadChart3">
    <xsl:variable name="peerCount" select="PeerCount/Count"/>
    <xsl:variable name="indexCount" select="IndexCount/Count"/>
    <xsl:variable name="keyInstn" select="Company/KeyInstn"/>
    <xsl:variable name="keyFndg" select="Company/KeyFndg"/>
    <xsl:variable name="isPreview" select="Company/IsPreview" />

    <xsl:if test="contains(GraphOptions/Small,'True') or contains(GraphOptions/Medium,'True') or contains(GraphOptions/Large,'True')">

      <TABLE border="0" cellpadding="3" cellspacing="0">

        <form name="downloadChartOptions" method="POST" action="StockInfoIFrame.aspx" target="_blank">
          <TR align="left">
            <TD class="defaultbold" valign="middle" nowrap="">
              Chart Size:
			  <label class="visuallyhidden" for="downloadSize">downloadSize</label>
              <select name="downloadSize" id="downloadSize" class="input">
                <xsl:if test="contains(GraphOptions/Small,'True')">
                  <option value="1">

                    Small
                  </option>
                </xsl:if>
                <xsl:if test="contains(GraphOptions/Medium,'True')">
                  <option value="2">

                    Medium
                  </option>
                </xsl:if>
                <xsl:if test="contains(GraphOptions/Large,'True')">
                  <option value="3">

                    Large
                  </option>
                </xsl:if>
              </select>
            </TD>
            <TD class="defaultbold" valign="middle">
              <button id="ChartDownload" type="button">Download</button>
            </TD>
          </TR>

          <xsl:if test="contains($isPreview, 'true')">
            <input type="hidden" name="preview" value="1"/>
          </xsl:if>

          <input type="hidden" name="KeyInstn">
            <xsl:attribute name="value">
              <xsl:value-of select="$keyInstn"/>
            </xsl:attribute>
          </input>

          <input type="hidden" name="KeyFndg">
            <xsl:attribute name="value">
              <xsl:value-of select="$keyFndg"/>
            </xsl:attribute>
          </input>

          <xsl:for-each select="Peers">
            <xsl:variable name="name" select="FormName"/>

            <input type="hidden" value="">
              <xsl:attribute name="name">
                <xsl:value-of select="concat('_','',$name)"/>
              </xsl:attribute>

              <xsl:attribute name="id">
                <xsl:value-of select="concat('_','',$name)"/>
              </xsl:attribute>
            </input>
          </xsl:for-each>

          <xsl:for-each select="CompIndex">
            <xsl:variable name="name" select="FormName"/>

            <input type="hidden" value="">
              <xsl:attribute name="name">
                <xsl:value-of select="concat('_','',$name)"/>
              </xsl:attribute>

              <xsl:attribute name="id">
                <xsl:value-of select="concat('_','',$name)"/>
              </xsl:attribute>
            </input>
          </xsl:for-each>

          <input type="hidden" name="Peers">
            <xsl:attribute name="value">
              <xsl:value-of select="$peerCount"/>
            </xsl:attribute>
          </input>

          <input type="hidden" name="Indexes">
            <xsl:attribute name="value">
              <xsl:value-of select="$indexCount"/>
            </xsl:attribute>
          </input>

          <input type="hidden" name="_period" id="_period" value=""/>
        </form>
      </TABLE>
    </xsl:if>
  </xsl:template>
  <!-- End Download Chart Dropdown -->


  <!-- Download Historical Data Dropdown -->
  <xsl:template name="DownloadHistData3">
    <xsl:variable name="keyInstn" select="Company/KeyInstn"/>
    <xsl:variable name="keyFndg" select="Company/KeyFndg"/>
    <xsl:variable name="isPreview" select="Company/IsPreview" />

    <xsl:if test="contains(GraphComponents/AllowDataDownload,'True')">
      <TABLE border="0" cellpadding="3" cellspacing="0">
        <form name="downloadDataOptions" method="POST" action="StockInfoIFrame.aspx" target="_blank">
          <TR align="left">
            <TD class="defaultbold" valign="middle" nowrap="">
              Historical Data Format:
			  <label class="visuallyhidden" for="downloadData">downloadData</label>
              <select name="downloadData" id="downloadData" class="input">
                <option value="html">

                  HTML
                </option>
                <option value="csv">

                  CSV
                </option>
              </select>
            </TD>
            <TD class="default" valign="middle">
              <input type="submit" value="Download" class="submit">
                <xsl:attribute name="onclick">
                  fillDownloadPeriod();
                </xsl:attribute>
              </input>
            </TD>
          </TR>
          <xsl:if test="contains($isPreview, 'true')">
            <input type="hidden" name="preview" value="1"/>
          </xsl:if>

          <input type="hidden" name="KeyInstn">
            <xsl:attribute name="value">
              <xsl:value-of select="$keyInstn"/>
            </xsl:attribute>
          </input>

          <input type="hidden" name="KeyFndg">
            <xsl:attribute name="value">
              <xsl:value-of select="$keyFndg"/>
            </xsl:attribute>
          </input>

          <input type="hidden" name="__period" id="__period" value=""/>

        </form>
      </TABLE>
    </xsl:if>
  </xsl:template>
  <!-- End Download Historical Data Dropdown -->


  <!-- Template 3 -->
  <xsl:template name="UpdateChart3">
    <xsl:variable name="peerCount" select="PeerCount/Count"/>
    <xsl:variable name="indexCount" select="IndexCount/Count"/>
    <xsl:variable name="showPeers" select="GraphComponents/Peers"/>
    <xsl:variable name="keyInstn" select="Company/KeyInstn"/>
    <xsl:variable name="keyFndg" select="Company/KeyFndg"/>
    <xsl:variable name="showIndexes" select="GraphComponents/Indexes"/>
    <xsl:variable name="showSplits" select="GraphComponents/Splits"/>
    <xsl:variable name="showDivs" select="GraphComponents/Dividends"/>
    <xsl:variable name="showNews" select="GraphComponents/News"/>
    <xsl:variable name="showPRs" select="GraphComponents/PressReleases"/>
    <xsl:variable name="showFilings" select="GraphComponents/Filings"/>
    <xsl:variable name="showDocs" select="GraphComponents/Documents"/>
    <xsl:variable name="showDivYield" select="GraphComponents/DividendYield"/>
    <xsl:variable name="showReturn" select="GraphComponents/TotalReturn"/>
    <xsl:variable name="isPreview" select="Company/IsPreview" />
    <xsl:variable name="defaultPeriod" select="GraphOptions/Period" />
    <xsl:variable name="endDate" select="PeriodDates/EndDate" />

    <TABLE cellpadding="3" cellspacing="0" border="0" width="100%">
      <form name="chartOptions" id="chartOptions"  method="POST">
        <!--action="StockInfoIFrame.aspx" target="theIFrame">-->
        <xsl:attribute name="onsubmit">
          expandIFrame(<xsl:value-of select="GraphSize/Width"/>,<xsl:value-of select="GraphSize/Height + 85"/>,<xsl:value-of select="$peerCount"/>,<xsl:value-of select="$indexCount"/>),document.getElementById("submitChart").disabled=true;
        </xsl:attribute>
        <TR>
          <TD>
            <TABLE ID="PeriodSelect" cellpadding="3" cellspacing="6" border="0" width="100%">
              <TR>
                <TD align="left" class="data" colspan="2">
                  <SPAN CLASS="Defaultbold">Period: </SPAN>
					<label class="visuallyhidden" for="period">period</label>
                  <select id="period" name="period" class="input" >
                    <xsl:for-each select="PeriodDates">
                      <option>
                        <xsl:if test="$defaultPeriod = KeyPeriod">
                          <xsl:attribute name="Selected">1</xsl:attribute>
                        </xsl:if>
                        <xsl:attribute name="Value">
                          <xsl:value-of select="StartDate"/>
                        </xsl:attribute>
                        <xsl:value-of select="Desc"/>
                      </option>
                    </xsl:for-each>
                  </select><SPAN style="padding-left: 25px" class="defaultbold">Select Chart Type:</SPAN>&#32;
					  <label class="visuallyhidden" for="TypeSelect">TypeSelect</label>
					  <select class="input" id="TypeSelect">
                    <option selected="selected" value="Area">Area</option>
                    <option value="Line">Line</option>
                    <option value="Column">Column</option>
                    <!--<option value="CandleStick">Candlestick</option>-->
                  </select>
                </TD>

                <xsl:if test="contains($isPreview, 'true')">
                  <input type="hidden" name="preview" value="1"/>
                </xsl:if>

                <input type="hidden" name="KeyInstn">
                  <xsl:attribute name="value">
                    <xsl:value-of select="$keyInstn"/>
                  </xsl:attribute>
                </input>

                <input type="hidden" name="KeyFndg">
                  <xsl:attribute name="value">
                    <xsl:value-of select="$keyFndg"/>
                  </xsl:attribute>
                </input>

                <input type="hidden" name="Peers">
                  <xsl:attribute name="value">
                    <xsl:value-of select="$peerCount"/>
                  </xsl:attribute>
                </input>

                <input type="hidden" name="Indexes">
                  <xsl:attribute name="value">
                    <xsl:value-of select="$indexCount"/>
                  </xsl:attribute>
                </input>

                <input type="hidden" id="EndDate1" name="EndDate1">
                  <xsl:attribute name="value">
                    <xsl:value-of select="$endDate"/>
                  </xsl:attribute>
                </input>
              </TR>
              <TR>
                <TD align="left" class="data" colspan="2">
                  <SPAN class="defaultbold">Start Date:&#32;</SPAN>
                  <input size="10" id="StartDate" type="text" class="input"/>
                  <SPAN style="padding-left: 10px" class="defaultbold">End Date:&#32;</SPAN>
                  <input size="10" id="EndDate2" type="text" class="input"/>
                </TD>
              </TR>
              <TR>
                <TD align="left" class="data" height="10px"></TD>
              </TR>
              <TR>
                <TD align="left" class="data">
                  <button class="submit" type="button" NAME="submitChart" ID="submitChart">Update Chart</button>
                </TD>
                <TD align="right" class="data">
                  <button class="submit" type="button" NAME="clearSelections" ID="clearSelections" onclick="this.form.reset()">Clear Selections</button>
                </TD>
              </TR>
            </TABLE>
          </TD>
        </TR>
        <TR>
          <TD>

            <TABLE border="0" width="100%" cellpadding="0"  class="colorlight">
              <TR>
                <!-- Peer checkboxes For Future release  -->
                <xsl:if test="contains($showPeers,'True')">
                  <TD valign="top" colspan="3" >
                    <TABLE ID="PeerSelect" border="0" cellpadding="3" cellspacing="0" width="100%">
                      <TR align="left">
                        <TD class="table2 datashade" colspan="2">
                          <SPAN class="defaultbold">Select Peers</SPAN>
                        </TD>
                      </TR>
                      <TR align="left">
                        <xsl:for-each select="Peers">
                          <xsl:variable name="instn" select="KeyInstnPeer"/>
                          <xsl:variable name="ticker" select="Ticker"/>
                          <xsl:variable name="name" select="FormName"/>
                          <xsl:variable name="fullPeerName" select="InstnName"/>

                          <xsl:choose>
                            <xsl:when test="position() mod 2 = 0">
                              <TD class="data" valign="middle">
                                <input type="checkbox">
                                  <xsl:attribute name="name">
                                    <xsl:value-of select="$name"/>
                                  </xsl:attribute>
                                  <xsl:attribute name="id">
                                    <xsl:value-of select="$name"/>
                                  </xsl:attribute>
                                  <xsl:attribute name="value">
                                    <xsl:value-of select="$instn"/>
                                  </xsl:attribute>
                                  <xsl:attribute name="onclick">
                                    clickCounter(<xsl:value-of select="$peerCount"/>,<xsl:value-of select="$indexCount"/>,this)
                                  </xsl:attribute>
                                </input>
                                <acronym>
                                  <xsl:attribute name="title">
                                    <xsl:value-of select="$fullPeerName"/>
                                  </xsl:attribute>
                                  &#160;<xsl:value-of select="Ticker"/>&#160;
                                </acronym>
                              </TD>
                              <xsl:call-template name="CloseTR"/>
                            </xsl:when>
                            <xsl:when test="position() mod 2 = 1">
                              <xsl:call-template name="OpenTR"/>
                              <TD class="data" valign="middle">
                                <input type="checkbox">
                                  <xsl:attribute name="name">
                                    <xsl:value-of select="$name"/>
                                  </xsl:attribute>
                                  <xsl:attribute name="id">
                                    <xsl:value-of select="$name"/>
                                  </xsl:attribute>
                                  <xsl:attribute name="value">
                                    <xsl:value-of select="$instn"/>
                                  </xsl:attribute>
                                  <xsl:attribute name="onclick">
                                    clickCounter(<xsl:value-of select="$peerCount"/>,<xsl:value-of select="$indexCount"/>,this)
                                  </xsl:attribute>
                                </input>
                                <acronym>
                                  <xsl:attribute name="title">
                                    <xsl:value-of select="$fullPeerName"/>
                                  </xsl:attribute>
                                  &#160;<xsl:value-of select="Ticker"/>&#160;
                                </acronym>
                              </TD>
                            </xsl:when>
                            <xsl:otherwise>
                              <TD class="data" valign="middle">
                                <input type="checkbox">
                                  <xsl:attribute name="name">
                                    <xsl:value-of select="$name"/>
                                  </xsl:attribute>
                                  <xsl:attribute name="id">
                                    <xsl:value-of select="$name"/>
                                  </xsl:attribute>
                                  <xsl:attribute name="value">
                                    <xsl:value-of select="$instn"/>
                                  </xsl:attribute>
                                  <xsl:attribute name="onclick">
                                    clickCounter(<xsl:value-of select="$peerCount"/>,<xsl:value-of select="$indexCount"/>,this)
                                  </xsl:attribute>
                                </input>
                                <acronym>
                                  <xsl:attribute name="title">
                                    <xsl:value-of select="$fullPeerName"/>
                                  </xsl:attribute>
                                  &#160;<xsl:value-of select="Ticker"/>&#160;
                                </acronym>
                              </TD>
                            </xsl:otherwise>
                          </xsl:choose>
                        </xsl:for-each>
                        <xsl:if test="not(peerCount mod 2 = 0)">
                          <xsl:call-template name="CloseTR"/>
                        </xsl:if>
                      </TR>
                    </TABLE>
                  </TD>
                </xsl:if>

                <!-- Index Check boxes -->
                <xsl:if test="contains($showIndexes,'True')">
                  <xsl:if test="$indexCount &gt; 0">
                    <TD valign="top" colspan="3">
                      <TABLE ID="IndexSelect" border="0" cellpadding="3" cellspacing="0" width="100%">
                        <TR align="left">
                          <TD valign="middle" class="table2 datashade" >
                            <SPAN class="defaultbold">Select Index</SPAN>
                          </TD>
                        </TR>
                        <xsl:for-each select="CompIndex">
                          <xsl:variable name="index" select="KeyIndex"/>
                          <xsl:variable name="name" select="FormName"/>

                          <TR align="left">
                            <TD valign="middle" class="data">
                              <input type="checkbox">
                                <xsl:attribute name="name">
                                  <xsl:value-of select="$name"/>
                                </xsl:attribute>
                                <xsl:attribute name="id">
                                  <xsl:value-of select="$name"/>
                                </xsl:attribute>
                                <xsl:attribute name="value">
                                  <xsl:value-of select="$index"/>
                                </xsl:attribute>
                                <xsl:attribute name="onclick">
                                  clickCounter(<xsl:value-of select="$peerCount"/>,<xsl:value-of select="$indexCount"/>,this)
                                </xsl:attribute>
                              </input>
                              <xsl:value-of select="IndexShortName"/>
                            </TD>
                          </TR>
                        </xsl:for-each>
                      </TABLE>
                    </TD>
                  </xsl:if>
                </xsl:if>
                <!-- End Index Check boxes -->

                <!-- Splits Divs Filings Documents -->
                <xsl:if test="contains($showSplits,'True') or contains($showDivs,'True') or contains($showNews,'True') or contains($showPRs,'True') or contains($showFilings,'True') or contains($showDocs,'True')">
                  <TD valign="top" colspan="3">
                    <TABLE ID="EventSelector" border="0" cellpadding="3" cellspacing="0" width="100%">
                      <TR align="left">
                        <TD valign="middle" class="table2 datashade">
                          <SPAN class="defaultbold">Select Events</SPAN>
                        </TD>
                      </TR>
                      <xsl:if test="contains($showSplits,'True')">
                        <TR align="left">
                          <TD class="data" valign="middle">
                            <input type="checkbox" name="splits" value="Splits"/>
                            <IMG src="Images/graphing/leg_splits.gif" alt="Splits"/> Splits
                          </TD>
                        </TR>
                      </xsl:if>

                      <xsl:if test="contains($showDivs,'True')">
                        <TR align="left">
                          <TD class="data" valign="middle">
                            <input type="checkbox" name="divs" value="Divs"/>
                            <IMG src="Images/graphing/leg_dividends.gif" alt="Dividends"/> Dividends
                          </TD>
                        </TR>
                      </xsl:if>

                      <xsl:if test="contains($showNews,'True')">
                        <TR align="left">
                          <TD class="data" valign="middle">
                            <input type="checkbox" name="news" value="1"/>
                            <IMG src="Images/graphing/leg_newssummaries.gif" alt="News Summaries"/> News Summaries
                          </TD>
                        </TR>
                      </xsl:if>

                      <xsl:if test="contains($showPRs,'True')">
                        <TR align="left">
                          <TD class="data" valign="middle">
                            <input type="checkbox" name="prs" value="PRs"/>
                            <IMG src="Images/graphing/leg_pressreleases.gif" alt="Press Releases"/> Press Releases
                          </TD>
                        </TR>
                      </xsl:if>

                      <xsl:if test="contains($showFilings,'True')">
                        <TR align="left">
                          <TD class="data" valign="middle">
                            <input type="checkbox" name="filings" value="Filings"/>
                            <IMG src="Images/graphing/leg_filings.gif" alt="Filings"/> Filings
                          </TD>
                        </TR>
                      </xsl:if>

                      <xsl:if test="contains($showDocs,'True')">
                        <TR align="left">
                          <TD class="data" valign="middle">
                            <input type="checkbox" name="docs" value="Docs"/>
                            <IMG src="Images/graphing/leg_documents.gif" alt="Documents"/> Documents
                          </TD>
                        </TR>
                      </xsl:if>

                      <xsl:if test="contains($showSplits,'True') or contains($showDivs,'True') or contains($showNews,'True') or contains($showPRs,'True') or contains($showFilings,'True') or contains($showDocs,'True')">
                        <TR align="left">
                          <TD class="data" valign="middle">
                            <IMG src="Images/graphing/leg_multipleevents.gif" alt="Multiple Events"/> - Indicates Multiple Events
                          </TD>
                        </TR>
                      </xsl:if>
                    </TABLE>
                  </TD>
                </xsl:if>

                <!-- Performance Measures -->
                <xsl:if test="contains($showDivYield,'True') or contains($showReturn,'True')">
                  <TD valign="top">
                    <TABLE border="0" cellpadding="3" cellspacing="0" width="100%">
                      <TR align="left">
                        <TD valign="middle" class="table2 datashade">
                          <SPAN class="defaultbold">Select Performance Measure</SPAN>
                        </TD>
                      </TR>

                      <xsl:if test="contains($showDivYield,'True')">
                        <TR align="left">
                          <TD class="data" valign="middle">
                            <input id="DivYield" type="checkbox" name="divyield" value="1">
                              <xsl:attribute name="onclick">
                                clickCounter(<xsl:value-of select="$peerCount"/>,<xsl:value-of select="$indexCount"/>,this)
                              </xsl:attribute>
                            </input>
                            Dividend Yield (%)
                          </TD>
                        </TR>
                      </xsl:if>

                      <xsl:if test="contains($showReturn,'True')">
                        <TR align="left">
                          <TD class="data" valign="middle">
                            <input id="TotalReturn" type="checkbox" name="treturn" value="1">
                              <xsl:attribute name="onclick">
                                clickCounter(<xsl:value-of select="$peerCount"/>,<xsl:value-of select="$indexCount"/>,this)
                              </xsl:attribute>
                            </input>
                            Total Return (%)
                          </TD>
                        </TR>
                      </xsl:if>

                    </TABLE>
                  </TD>
                </xsl:if>
              </TR>
            </TABLE>
          </TD>
        </TR>
      </form>
    </TABLE>
    <br />
  </xsl:template>

  <!-- End Template3 -->


  <xsl:template name="Chart_Drop_Image_stock3">
    <xsl:variable name="isPreview" select="Company/IsPreview" />
    <TR align="left">
      <TD class="report_top table1_item datashade" valign="TOP">
        <SPAN CLASS="defaultbold">Current Chart</SPAN>
      </TD>
    </TR>

    <TR align="left">
      <TD CLASS="" valign="TOP" align="center" style="padding-top: 10px;">
        <div id="graphContainer"></div>
          
          <xsl:choose>
          <xsl:when test="contains(Company/URL, 'print=1') or contains(Company/URL, 'Print=1')">
            <xsl:variable name="KeyInstn" select="Company/KeyInstn" />
            <xsl:variable name="KeyFndg" select="Company/KeyFndg" />
            <xsl:variable name="StartDate" select="PeriodDates[KeyPeriod = ../GraphOptions/Period]/StartDate" />
            <xsl:variable name="EndDate" select="PeriodDates[KeyPeriod = ../GraphOptions/Period]/EndDate" />
            <IMG>
              <xsl:attribute name="src">
                StockChartImage.aspx?iid=<xsl:value-of select="$KeyInstn"/>&#38;KeyFndg=<xsl:value-of select="$KeyFndg"/>&#38;StartDate=<xsl:value-of select="$StartDate"/>&#38;EndDate=<xsl:value-of select="$EndDate"/>
              </xsl:attribute>
            </img>
          </xsl:when>
          <xsl:otherwise>
            <IMG id="graph" style="display:none;" alt=""></img>
            <SPAN id="errorMsg" style="display:none;">The requested graph could not be constructed. Some data may not be available.  Please try changing the period or deselecting options.</SPAN>
            <embed id="LoadAnim" src="images/graphing/loading.swf" wmode="transparent" width="75" height="75" />
          </xsl:otherwise>
        </xsl:choose>
          
      </TD>
    </TR>
  </xsl:template>

  <xsl:template name="FinancialDataTables3">
    <xsl:variable name="mytradingsymbol" select="/irw:IRW/irw:StockInformation/TradedIssues/TradingSymbol[../KeyFndg = /irw:IRW/irw:StockInformation/Company/KeyFndg]"/>
    <xsl:if test="Company/IsCommonStock='True'">
      <TR>
        <TD valign="top" colspan="3">
          <xsl:call-template name="Vol_Highlights_stock3"/>
        </TD>
      </TR>
      <TR>
        <TD valign="top" class="botbord" colspan="3">
          <IMG SRC="/images/Interactive/blank.gif" WIDTH="1" HEIGHT="1" alt=""/>
        </TD>
      </TR>
      <xsl:if test="not(Company/GAAP=0)">
        <TR>
          <TD valign="top" colspan="3">
            <br />
            <xsl:call-template name="Fin_Data_stock3"/>
          </TD>
        </TR>
        <TR>
          <TD valign="top" class="botbord" colspan="3">
            <IMG SRC="/images/Interactive/blank.gif" WIDTH="1" HEIGHT="1" alt=""/>
          </TD>
        </TR>
      </xsl:if>
      <TR>
        <TD valign="top" colspan="3">
          <br />
          <xsl:call-template name="Stock_Price_stock3"/>
        </TD>
      </TR>
      <TR>
        <TD valign="top" class="botbord" colspan="3">
          <IMG SRC="/images/Interactive/blank.gif" WIDTH="1" HEIGHT="1" alt=""/>
        </TD>
      </TR>
      <xsl:if test="not(Company/GAAP=0)">
        <TR>
          <TD valign="top" colspan="3">
            <br />
            <xsl:call-template name="Price_Ratios_stock3"/>
          </TD>
        </TR>
        <TR>
          <TD valign="top" class="botbord" colspan="3">
            <IMG SRC="/images/Interactive/blank.gif" WIDTH="1" HEIGHT="1" alt=""/>
          </TD>
        </TR>
      </xsl:if>
      <TR>
        <TD valign="top" colspan="3">
          <br />
          <xsl:call-template name="Price_Change_stock3"/>
        </TD>
      </TR>
      <TR>
        <TD valign="top" class="botbord" colspan="3">
          <IMG SRC="/images/Interactive/blank.gif" WIDTH="1" HEIGHT="1" alt=""/>
        </TD>
      </TR>
      <TR>
        <TD valign="top" colspan="3">
          <br />
          <xsl:call-template name="Dividends_stock3"/>
        </TD>
      </TR>
      <TR>
        <TD valign="top" class="botbord" colspan="3">
          <IMG SRC="/images/Interactive/blank.gif" WIDTH="1" HEIGHT="1"alt=""/>
        </TD>
      </TR>
    </xsl:if>
    <xsl:if test="Company/IsCommonStock='False'">
      <TR>
        <TD valign="top" colspan="3">
          <xsl:call-template name="Vol_Highlights_stock3"/>
        </TD>
      </TR>
      <TR>
        <TD valign="top" class="botbord" colspan="3">
          <IMG SRC="/images/Interactive/blank.gif" WIDTH="1" HEIGHT="1" alt=""/>
        </TD>
      </TR>
      <TR>
        <TD valign="top" colspan="3">
          <br />
          <TABLE CELLPADDING="3" CELLSPACING="0" CLASS="table2" WIDTH="100%" BORDER="0" ID="TABLE16">
            <TR VALIGN="bottom">
              <TD CLASS="table1_item datashade defaultbold" WIDTH="40%">Price Change&#160;(%)</TD>
              <TD CLASS="table1_item datashade defaultbold" align="right">
                <xsl:value-of select="$mytradingsymbol"/>
              </TD>
            </TR>
            <TR >
              <TD CLASS="data" NOWRAP="" align="LEFT" WIDTH="40%">
                <SPAN CLASS="defaultbold">One Day</SPAN>
              </TD>
              <TD CLASS="data" WIDTH="28%" align="right">
                <xsl:value-of select="format-number(PriceChange/OneDay,'#,##0.00 ;(#,##0.00)','data')"/>
                <SPAN CLASS="dataalign">&#160;</SPAN>
              </TD>
            </TR>
            <TR>
              <TD CLASS="data" NOWRAP="" align="LEFT" WIDTH="40%">
                <SPAN CLASS="defaultbold">One Month</SPAN>
              </TD>
              <TD CLASS="data" WIDTH="28%" align="right">
                <xsl:value-of select="format-number(PriceChange/OneMonth,'#,##0.00 ;(#,##0.00)','data')"/>
                <SPAN CLASS="dataalign">&#160;</SPAN>
              </TD>
            </TR>
            <TR >
              <TD CLASS="data" NOWRAP="" align="LEFT" WIDTH="40%">
                <SPAN CLASS="defaultbold">Three Month</SPAN>
              </TD>
              <TD CLASS="data" WIDTH="28%" align="right">
                <xsl:value-of select="format-number(PriceChange/ThreeMonth,'#,##0.00 ;(#,##0.00)','data')"/>
                <SPAN CLASS="dataalign">&#160;</SPAN>
              </TD>
            </TR>
            <TR >
              <TD CLASS="data" NOWRAP="" align="LEFT" WIDTH="40%">
                <SPAN CLASS="defaultbold">YTD</SPAN>
              </TD>
              <TD CLASS="data" WIDTH="28%" align="right">
                <xsl:value-of select="format-number(PriceChange/YTD,'#,##0.00 ;(#,##0.00)','data')"/>
                <SPAN CLASS="dataalign">&#160;</SPAN>
              </TD>
            </TR>
            <TR >
              <TD CLASS="data" NOWRAP="" align="LEFT" WIDTH="40%">
                <SPAN CLASS="defaultbold">One Year</SPAN>
              </TD>
              <TD CLASS="data" WIDTH="28%" align="right">
                <xsl:value-of select="format-number(PriceChange/OneYear,'#,##0.00 ;(#,##0.00)','data')"/>
                <SPAN CLASS="dataalign">&#160;</SPAN>
              </TD>
            </TR>
          </TABLE>
        </TD>
      </TR>
      <TR>
        <TD valign="top" class="botbord" colspan="3">
          <IMG SRC="/images/Interactive/blank.gif" WIDTH="1" HEIGHT="1" alt=""/>
        </TD>
      </TR>
      <TR>
        <TD valign="top" colspan="3">
          <br />
          <xsl:call-template name="Stock_Price_stock3"/>
        </TD>
      </TR>
      <TR>
        <TD valign="top" class="botbord" colspan="3">
          <IMG SRC="/images/Interactive/blank.gif" WIDTH="1" HEIGHT="1" alt=""/>
        </TD>
      </TR>
        <TR>
          <TD valign="top" colspan="3">
            <br />
            <xsl:call-template name="Dividends_stock3"/>
          </TD>
        </TR>
      <TR>
        <TD valign="top" class="botbord" colspan="3">
          <IMG SRC="/images/Interactive/blank.gif" WIDTH="1" HEIGHT="1" alt=""/>
        </TD>
      </TR>
    </xsl:if>
  </xsl:template>

  <xsl:template name="Quote_Table_stock3">
    <xsl:variable name="keyinstn" select="Company/KeyInstn"/>

    <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" class="bottom_cap" >
      <TR VALIGN="BOTTOM">
        <TD colspan="3" class="report_top table1_item datashade">
          <SPAN class="defaultbold">Current Quote</SPAN>
        </TD>
      </TR>

      <TR VALIGN="BOTTOM">
        <TD CLASS="" NOWRAP="" WIDTH="20%">
          <SPAN CLASS="defaultbold">Last Sale</SPAN>
          <br />
          <SPAN class="default">
            &#36;&#160;<xsl:value-of select="format-number(StockQuote/CurrentPrice, '###,##0.00')"/>
          </SPAN>
        </TD>
        <TD CLASS="" NOWRAP="" WIDTH="20%">
          <SPAN CLASS="defaultbold">Last Close</SPAN>
          <br />
          <SPAN class="default">
            <xsl:choose>
              <xsl:when test="StockQuote/LastPrice > 0">
                &#36;&#160;<xsl:value-of select="format-number(StockQuote/LastPrice, '###,##0.00')"/>
              </xsl:when>
              <xsl:otherwise>
                NA
              </xsl:otherwise>
            </xsl:choose>
          </SPAN>
        </TD>
        <TD rowspan="3" valign="top">
          <TABLE width="100%" cellpadding="2" cellspacing="1" border="0">
            <TR>
              <TD CLASS="" valign="top" align="right">
                <SPAN class="default">
                  Enter a date below to request the<br />
                  closing price for that specific day:
                </SPAN>
              </TD>
            </TR>
            <TR>
              <TD CLASS="" valign="top" align="right">
                <input class="input" type="text" NAME="date" size="12" ID="snlstock">
                  <xsl:attribute name="onkeydown">
                    KeydownStock(<xsl:value-of select="Company/KeyInstn"/>,<xsl:value-of select="Company/KeyFndg"/>, event);
                  </xsl:attribute>
                </input>&#160;
                <input type="button" value="Enter" id="Button1"  class="submit" name="submit1">
                  <xsl:attribute name="OnClick">
                    window.open('quotepop.aspx?IID=<xsl:value-of select="Company/KeyInstn"/>&amp;KeyFndg=<xsl:value-of select="Company/KeyFndg"/>&amp;date=' + document.getElementById('snlstock').value, 'ClosingPrice', 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=60')
                  </xsl:attribute>
                </input>
                <br /><SPAN class="default">Format: MM/DD/YY</SPAN>
              </TD>
            </TR>
          </TABLE>
        </TD>
      </TR>

      <TR VALIGN="BOTTOM">
        <TD CLASS="" NOWRAP="" WIDTH="20%">
          <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1850, 0); return false" title="Today's High">
            <SPAN CLASS="defaultbold">Today's High</SPAN>
          </a>
          <br />
          <SPAN class="default">
            &#36;&#160;
            <xsl:choose>
              <xsl:when test="StockQuote/HighToday= 0">NA</xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="format-number(StockQuote/HighToday, '###,##0.00')"/>
              </xsl:otherwise>
            </xsl:choose>
          </SPAN>
        </TD>
        <TD CLASS="" NOWRAP="" WIDTH="20%">
          <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1851, 0); return false" title="Today's Low">
            <SPAN CLASS="defaultbold">Today's Low</SPAN>
          </a>
          <br />
          <SPAN class="default">
            &#36;&#160;
            <xsl:choose>
              <xsl:when test="StockQuote/LowToday= 0">NA</xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="format-number(StockQuote/LowToday, '###,##0.00')"/>
              </xsl:otherwise>
            </xsl:choose>
          </SPAN>
        </TD>
      </TR>
      <TR align="CENTER" VALIGN="TOP">
        <TD CLASS="" NOWRAP="" align="left">
          <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1855, 0); return false" title="Net Change">
            <SPAN CLASS="defaultbold">Net Change</SPAN>
          </a>
          <br />
          <SPAN class="default">
            <xsl:variable name="NetChangeFromPreviousClose" select="StockQuote/NetChangeFromPreviousClose"/>
            <xsl:choose>
              <xsl:when test="$NetChangeFromPreviousClose != 0">
                <xsl:choose>
                  <xsl:when test="$NetChangeFromPreviousClose &gt; 0">
                    &#36;&#160;<xsl:value-of select="format-number($NetChangeFromPreviousClose,'###,##0.00','data')"/>&#160;
                  </xsl:when>
                  <xsl:otherwise>
                    &#36;&#160;<xsl:value-of select="$NetChangeFromPreviousClose"/>&#160;
                  </xsl:otherwise>
                </xsl:choose>
                <xsl:if test="StockQuote/NetChangeDirection = 'positive'">
                  <SPAN class="default">
                    <IMG SRC="/images/Interactive/IR/uptriangle.gif" WIDTH="11" HEIGHT="10" BORDER="0" ALT="Up" />&#160;
                  </SPAN>
                </xsl:if>
                <xsl:if test="StockQuote/NetChangeDirection = 'negative'">
                  <SPAN class="default">
                    <IMG SRC="/images/Interactive/IR/downtriangle.gif" WIDTH="11" HEIGHT="10" BORDER="0" ALT="Down" />&#160;
                  </SPAN>
                </xsl:if>
                <strong>
                  <xsl:if test="StockQuote/NetChangeDirection = 'negative'">(</xsl:if><xsl:value-of select="format-number(StockQuote/PercentChangeFromPreviousClose, '###,##0.00')"/><xsl:if test="StockQuote/NetChangeDirection = 'negative'">)</xsl:if>&#37;
                </strong>
              </xsl:when>
              <xsl:otherwise>
                NA
              </xsl:otherwise>
            </xsl:choose>
          </SPAN>
        </TD>
        <TD CLASS="" NOWRAP="" align="left">
          <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 2454, 0); return false" title="Today's Volume">
            <SPAN CLASS="defaultbold">Today's Volume</SPAN>
          </a>
          <br />
          <SPAN class="default">
            <xsl:value-of select="StockQuote/VolumeToday"/>
          </SPAN>
        </TD>
      </TR>
      <TR align="left" VALIGN="TOP">
        <TD  class="default" NOWRAP="1" colspan="3">
          <SPAN class="defaultbold">Data as of</SPAN>&#160;
          <xsl:value-of select="StockQuote/QuoteTimeStamp"/>&#160;
          Minimum 20 minute delay
        </TD>
      </TR>
    </TABLE>
    <br />
  </xsl:template>

  <xsl:template name="Vol_Highlights_stock3">
    <TABLE CELLSPACING="0" CELLPADDING="3" CLASS="table2" WIDTH="100%" BORDER="0" ID="TABLE10">
      <TR>
        <TD CLASS="table1_item datashade" WIDTH="40%" align="left">
          <SPAN class="defaultbold">Volume Highlights</SPAN>
        </TD>
        <TD CLASS="table1_item datashade" align="right" WIDTH="28%" nowrap="">
          <SPAN class="defaultbold">
            Avg Daily<br />Volume
          </SPAN>
        </TD>
        <TD CLASS="table1_item datashade" align="right" WIDTH="32%">
          <SPAN class="defaultbold">
            %&#160;of&#160;Shares<br />Outstanding
          </SPAN>
        </TD>
      </TR>
      <TR>
        <TD CLASS="data" NOWRAP="" align="LEFT" width="40%">
          <SPAN class="defaultbold">One Day</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="VolumeHighlights/OneDayAvgDailyVolume"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="32%" align="right">
          <xsl:value-of select="format-number(VolumeHighlights/OneDayPercentSharesOutstanding,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
      </TR>
      <TR>
        <TD CLASS="data" NOWRAP="" align="LEFT" width="40%">
          <SPAN class="defaultbold">One Month</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="VolumeHighlights/OneMonthAvgDailyVolume"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="32%" align="right">
          <xsl:value-of select="format-number(VolumeHighlights/OneMonthPercentSharesOutstanding,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
      </TR>
      <TR>
        <TD CLASS="data" NOWRAP="" align="LEFT" width="40%">
          <SPAN class="defaultbold">Three Months</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="VolumeHighlights/ThreeMonthsAvgDailyVolume"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="32%" align="right">
          <xsl:value-of select="format-number(VolumeHighlights/ThreeMonthsPercentSharesOutstanding,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
      </TR>
      <TR>
        <TD CLASS="data" NOWRAP="" align="LEFT" width="40%">
          <SPAN class="defaultbold"> YTD</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="VolumeHighlights/YTDAvgDailyVolume"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="32%" align="right">
          <xsl:value-of select="format-number(VolumeHighlights/YTDPercentSharesOutstanding,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
      </TR>
      <TR>
        <TD CLASS="data" NOWRAP="" align="LEFT" width="40%">
          <SPAN class="defaultbold">One Year</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="VolumeHighlights/OneYearAvgDailyVolume"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="32%" align="right">
          <xsl:value-of select="format-number(VolumeHighlights/OneYearPercentSharesOutstanding,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
      </TR>
    </TABLE>
  </xsl:template>

  <xsl:template name="Fin_Data_stock3">
    <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" CLASS="table2" WIDTH="100%" ID="TABLE11">
      <xsl:variable name="GAAPDomain" select="Company/GAAP"/>
      <xsl:variable name="INSURANCE_UNDERWRITER" select="8"/>
      <xsl:variable name="INSURANCE_BROKER" select="9"/>
      <xsl:variable name="THRIFT" select="2"/>
      <xsl:variable name="INVESTMENT_COMPANY" select="7"/>
      <xsl:variable name="REIT" select="6"/>
      <xsl:variable name="BANK" select="1"/>
      <xsl:variable name="ENERGY" select="10"/>
      <xsl:variable name="GAS" select="18"/>
      <xsl:variable name="FINTECH" select="15"/>
      <xsl:variable name="HOMEBUILDER" select="16"/>
      <xsl:variable name="COMMUNICATIONS" select="14"/>
      <xsl:variable name="MEDIA_ENT" select="20"/>
      <xsl:variable name="NEWMEDIA" select="21"/>
      <xsl:variable name="keyinstn" select="Company/KeyInstn"/>
      <xsl:choose>
        <xsl:when test="$GAAPDomain = $BANK or $GAAPDomain = $THRIFT">
          <TR VALIGN="TOP">
            <TD CLASS="surrleft table1_item datashade" NOWRAP="" WIDTH="60%" align="left">
              <SPAN class="defaultbold">Financial Data</SPAN>
            </TD>
            <TD CLASS="surrright table1_item datashade" WIDTH="40%" align="right">
              <SPAN class="defaultbold">
                <xsl:value-of select="FinancialData/DateEnded"/>
              </SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EquityPerShareForCalcs', 1); return false" title="Book Value ($)">
                <SPAN CLASS="defaultbold">Book Value ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/BookValue,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'TangibleEquityPerShare', 1); return false" title="Tangible Book ($)">
                <SPAN CLASS="defaultbold">Tangible Book ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/TangibleBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSAfterExtraForCalcs', 0); return false" title="Quarter EPS ($)">
                <SPAN CLASS="defaultbold">Quarter EPS ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/QuarterEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSAfterExtraForCalcs', 0); return false" title="EPS ($)">
                <SPAN CLASS="defaultbold">
                  <xsl:value-of select="Company/FiscalPeriodDisplay"/> EPS ($)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/EPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'CoreEPS', 0); return false">
                <SPAN CLASS="defaultbold">
                  <xsl:value-of select="Company/FiscalPeriodDisplay"/> Core EPS ($)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/CoreEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $INVESTMENT_COMPANY">
          <TR VALIGN="TOP">
            <TD CLASS="surrleft table1_item datashade" NOWRAP="" WIDTH="60%" align="left">
              <SPAN class="defaultbold">Financial Data</SPAN>
            </TD>
            <TD CLASS="surrright table1_item datashade" WIDTH="40%" align="right">
              <SPAN class="defaultbold">
                <xsl:value-of select="FinancialData/DateEnded"/>
              </SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EquityPerShareForCalcs', 1); return false">
                <SPAN CLASS="defaultbold">Book Value ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/BookValue,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'QuarterEPS', 1); return false" title="Quarter EPS ($)">
                <SPAN CLASS="defaultbold">Quarter EPS ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/QuarterEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSAfterExtraForCalcs', 0); return false" title="EPS ($)">
                <SPAN CLASS="defaultbold">
                  <xsl:value-of select="Company/FiscalPeriodDisplay"/> EPS ($)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/EPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $ENERGY or $GAAPDomain = $GAS">
          <TR VALIGN="TOP">
            <TD CLASS="surrleft table1_item datashade" NOWRAP="" WIDTH="60%" align="left">
              <SPAN class="defaultbold">Financial Data</SPAN>
            </TD>
            <TD CLASS="surrright table1_item datashade" WIDTH="40%" align="right">
              <SPAN class="defaultbold">
                <xsl:value-of select="FinancialData/DateEnded"/>
              </SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'NetIncomeGrowth', 1); return false" title="Net Income Growth (%)">
                <SPAN CLASS="defaultbold">Net Income Growth (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/NetIncomeGrowth,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSBasic', 1); return false" title="Basic EPS ($)">
                <SPAN CLASS="defaultbold">Basic EPS ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/EPSBasic,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'DebtToTotalCapBookValue', 0); return false" title="Debt / Book Capitalization (%)">
                <SPAN CLASS="defaultbold">Debt / Book Capitalization (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/DebtToTotalCap,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'OperatingIncomeUtilityRpGrowth', 0); return false" title="Reported Net Operating Income Growth (%)">
                <SPAN CLASS="defaultbold">Reported Net Operating Income Growth (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/OperatingIncomeUtilityRpGrowth,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'OperatingAssetsEnergyGrowth', 0); return false" title="Operating Asset Growth (%)">
                <SPAN CLASS="defaultbold">Operating Asset Growth (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/OperatingAssetsEnergyGrowth,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $FINTECH">
          <TR VALIGN="TOP">
            <TD CLASS="surrleft table1_item datashade" NOWRAP="" WIDTH="60%" align="left">
              <SPAN class="defaultbold">Financial Data</SPAN>
            </TD>
            <TD CLASS="surrright table1_item datashade" WIDTH="40%" align="right">
              <SPAN class="defaultbold">
                <xsl:value-of select="FinancialData/DateEnded"/>
              </SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},988, 1); return false" title="Diluted EPS after Extra ($)">
                <SPAN CLASS="defaultbold">Diluted EPS after Extra ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/EPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},10180, 1); return false" title="LTM EPS after Extra ($)">
                <SPAN CLASS="defaultbold">LTM EPS after Extra ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/LTMEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},9827, 0); return false" title="Book Value ($)">
                <SPAN CLASS="defaultbold">Book Value ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/BookValue,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},13487, 0); return false" title="Tangible Book Value per Share ($)">
                <SPAN CLASS="defaultbold">Tangible Book Value per Share ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/TangibleBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $REIT">
          <TR VALIGN="TOP">
            <TD CLASS="surrleft table1_item datashade" NOWRAP="" WIDTH="60%" align="left">
              <SPAN class="defaultbold">Quarterly Financial Data</SPAN>
            </TD>
            <TD CLASS="surrright table1_item datashade" WIDTH="40%" align="right">
              <SPAN class="defaultbold">
                <xsl:value-of select="FinancialData/DateEnded"/>
              </SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'FFOPerShareForCalcs', 1); return false" title="FFO / Share ($)">
                <SPAN CLASS="defaultbold">FFO / Share ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/FFOPerShare,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'LTMFFOPerShare', 1); return false" title="FFO / Share ($)">
                <SPAN CLASS="defaultbold">
                  <xsl:value-of select="Company/FiscalPeriodDisplay"/> FFO / Share ($)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/LTMFFOPerShare,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'FADPerShare', 0); return false" title="FAD / Share ($)">
                <SPAN CLASS="defaultbold">FAD / Share ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/FADPerShare,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'FFOOperatingReported', 0); return false" title="Reported Operating FFO ($000)">
                <SPAN CLASS="defaultbold">Reported Operating FFO ($000)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="FinancialData/FFOOperatingReported"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'FFOOperatingReportedShare', 0); return false" title="Reported Operating FFO / Share ($)">
                <SPAN CLASS="defaultbold">Reported Operating FFO / Share ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/FFOOperatingReportedShare,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'SameStoreRevenueChange', 0); return false" title="Same-store NOI Change (%">
                <SPAN CLASS="defaultbold">Same-store NOI Change (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/SameStoreRevenueChange,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'DebtToTotalCapFinl', 0); return false" title="Debt / Total Cap (%)">
                <SPAN CLASS="defaultbold">Debt / Total Cap (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/DebtToTotalCap,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $INSURANCE_BROKER or $GAAPDomain = $INSURANCE_UNDERWRITER">
          <TR VALIGN="TOP">
            <TD CLASS="surrleft table1_item datashade" NOWRAP="" WIDTH="60%" align="left">
              <SPAN class="defaultbold">Financial Data</SPAN>
            </TD>
            <TD CLASS="surrright table1_item datashade" WIDTH="40%" align="right">
              <SPAN class="defaultbold">
                <xsl:value-of select="FinancialData/DateEnded"/>
              </SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EquityPerShareForCalcs', 1); return false" title="Book Value ($)">
                <SPAN CLASS="defaultbold">Book Value ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/BookValue,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'TangibleEquityPerShare', 1); return false" title="Tangible Book ($)">
                <SPAN CLASS="defaultbold">Tangible Book ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/TangibleBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSAfterExtraForCalcs', 0); return false" title="Quarter EPS ($)">
                <SPAN CLASS="defaultbold">Quarter EPS ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/QuarterEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSAfterExtraForCalcs', 0); return false" title="EPS ($)">
                <SPAN CLASS="defaultbold">
                  <xsl:value-of select="Company/FiscalPeriodDisplay"/> EPS ($)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/EPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'CoreEPS', 0); return false" title="Operating EPS ($)">
                <SPAN CLASS="defaultbold">
                  <xsl:value-of select="Company/FiscalPeriodDisplay"/> Operating EPS ($)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/OperatingEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $HOMEBUILDER">
          <TR VALIGN="TOP">
            <TD CLASS="surrleft table1_item datashade" NOWRAP="" WIDTH="60%" align="left">
              <SPAN class="defaultbold">Financial Data</SPAN>
            </TD>
            <TD CLASS="surrright table1_item datashade" WIDTH="40%" align="right">
              <SPAN class="defaultbold">
                <xsl:value-of select="FinancialData/DateEnded"/>
              </SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EquityPerShareForCalcs', 1); return false" title="Book Value ($)">
                <SPAN CLASS="defaultbold">Book Value ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/BookValue,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSAfterExtraForCalcs', 0); return false" title="Quarter EPS ($)">
                <SPAN CLASS="defaultbold">Quarter EPS ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/QuarterEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSAfterExtraForCalcs', 0); return false" title="EPS ($)">
                <SPAN CLASS="defaultbold">
                  <xsl:value-of select="Company/FiscalPeriodDisplay"/> EPS ($)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/EPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'OperatingMarginBuilder', 0); return false" title="Operating Margin ($)">
                <SPAN CLASS="defaultbold">Operating Margin ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/OperatingMarginBuilder,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $MEDIA_ENT or $GAAPDomain = $COMMUNICATIONS or $GAAPDomain = $NEWMEDIA">
          <TR VALIGN="TOP">
            <TD CLASS="surrleft table1_item datashade defaultbold" NOWRAP="" WIDTH="60%" align="left" title="Financial Data">
              <SPAN class="defaultbold">Financial Data</SPAN>
            </TD>
            <TD CLASS="surrright table1_item datashade defaultbold" WIDTH="40%" valign="bottom" align="right">
              <xsl:value-of select="FinancialData/DateEnded"/>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'TotalCapBookValue', 0); return false" title="Total Capitalization ($M)">
                <SPAN CLASS="defaultbold">Total Capitalization ($M)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="FinancialData/BookValue"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSAfterExtraForCalcs', 0); return false" title="Quarter EPS ($)">
                <SPAN CLASS="defaultbold">Quarter EPS ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/QuarterEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSAfterExtraForCalcs', 0); return false" title="EPS ($)">
                <SPAN CLASS="defaultbold">
                  <xsl:value-of select="Company/FiscalPeriodDisplay"/> EPS ($)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/EPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'NetIncomeMargin', 0); return false" title="Net Income Margin (%)">
                <SPAN CLASS="defaultbold">Net Income Margin (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/NetIncome,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:otherwise>
          <TR VALIGN="TOP">
            <TD CLASS="surrleft table1_item datashade" NOWRAP="" WIDTH="60%" align="left">
              <SPAN class="defaultbold">Financial Data</SPAN>
            </TD>
            <TD CLASS="surrright table1_item datashade" WIDTH="40%" align="right">
              <SPAN class="defaultbold">
                <xsl:value-of select="FinancialData/DateEnded"/>
              </SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EquityPerShareForCalcs', 1); return false" title="Book Value ($)">
                <SPAN CLASS="defaultbold">Book Value ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/BookValue,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'TangibleEquityPerShare', 1); return false" title="Tangible Book ($)">
                <SPAN CLASS="defaultbold">Tangible Book ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/TangibleBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSAfterExtraForCalcs', 0); return false" title="Quarter EPS ($)">
                <SPAN CLASS="defaultbold">Quarter EPS ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/QuarterEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSAfterExtraForCalcs', 0); return false" title="EPS ($)">
                <SPAN CLASS="defaultbold">
                  <xsl:value-of select="Company/FiscalPeriodDisplay"/> EPS ($)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(FinancialData/EPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:otherwise>
      </xsl:choose>
    </TABLE>
  </xsl:template>

  <xsl:template name="Stock_Price_stock3">
    <TABLE CELLPADDING="3" CELLSPACING="0" CLASS="table2" WIDTH="100%" BORDER="0" ID="TABLE13">
      <TR>
        <TD CLASS="table1_item datashade" WIDTH="40%">
          <SPAN class="defaultbold">Stock Price History</SPAN>
        </TD>
        <TD CLASS="table1_item datashade" align="RIGHT" WIDTH="28%" nowrap="">
          <SPAN class="defaultbold">High ($)</SPAN>
        </TD>
        <TD CLASS="table1_item datashade" align="RIGHT" WIDTH="32%" nowrap="">
          <SPAN class="defaultbold">Low ($)</SPAN>
        </TD>
      </TR>
      <TR>
        <TD CLASS="data" NOWRAP="" WIDTH="40%">
          <SPAN CLASS="defaultbold">One Month</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="format-number(StockPriceHistory/OneMonthHigh,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
        <TD class="data" WIDTH="32%" align="right">
          <xsl:value-of select="format-number(StockPriceHistory/OneMonthLow,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
      </TR>
      <TR>
        <TD CLASS="data" NOWRAP="" WIDTH="40%">
          <SPAN CLASS="defaultbold">Three Months</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="format-number(StockPriceHistory/ThreeMonthsHigh,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS=  "dataalign">&#160;</SPAN>
        </TD>
        <TD class="data" WIDTH="32%" align="right">
          <xsl:value-of select="format-number(StockPriceHistory/ThreeMonthsLow,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
      </TR>
      <TR>
        <TD CLASS="data" NOWRAP="" WIDTH="40%">
          <SPAN CLASS="defaultbold">Year-to-Date</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="format-number(StockPriceHistory/YTDHigh,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="32%" align="right">
          <xsl:value-of select="format-number(StockPriceHistory/YTDLow,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
      </TR>
      <TR>
        <TD CLASS="data" NOWRAP="" WIDTH="40%">
          <SPAN CLASS="defaultbold">One Year</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="format-number(StockPriceHistory/OneYearHigh,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="32%" align="right">
          <xsl:value-of select="format-number(StockPriceHistory/OneYearLow,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
      </TR>
      <TR>
        <TD CLASS="data" NOWRAP="" WIDTH="40%">
          <SPAN CLASS="defaultbold">Three Year</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="format-number(StockPriceHistory/ThreeYearsHigh,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="32%" align="right">
          <xsl:value-of select="format-number(StockPriceHistory/ThreeYearsLow,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
      </TR>
      <TR>
        <TD CLASS="data" NOWRAP="" WIDTH="40%">
          <SPAN CLASS="defaultbold">Three Year Date</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="StockPriceHistory/ThreeYearDateHigh"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="32%" align="right">
          <xsl:value-of select="StockPriceHistory/ThreeYearDateLow"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
      </TR>
    </TABLE>
  </xsl:template>

  <xsl:template name="Price_Ratios_stock3">
    <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" CLASS="table2" WIDTH="100%" ID="TABLE14">

      <xsl:variable name="GAAPDomain" select="Company/GAAP"/>
      <xsl:variable name="INSURANCE_BROKER" select="9"/>
      <xsl:variable name="INVESTMENT_COMPANY" select="7"/>
      <xsl:variable name="THRIFT" select="2"/>
      <xsl:variable name="INSURANCE" select="8"/>
      <xsl:variable name="REIT" select="6"/>
      <xsl:variable name="BANK" select="1"/>
      <xsl:variable name="ENERGY" select="10"/>
      <xsl:variable name="GAS" select="18"/>
      <xsl:variable name="FINTECH" select="15"/>
      <xsl:variable name="HOMEBUILDER" select="16"/>
      <xsl:variable name="COMMUNICATIONS" select="14"/>
      <xsl:variable name="MEDIA_ENT" select="20"/>
      <xsl:variable name="NEWMEDIA" select="21"/>
      <xsl:variable name="keyinstn" select="Company/KeyInstn" />
      <xsl:variable name="keyspecialtaxstatus" select="Company/KeySpecialTaxStatus" />
      <xsl:choose>
        <xsl:when test="$GAAPDomain = $BANK or $GAAPDomain = $THRIFT">
          <TR VALIGN="TOP">
            <TD CLASS="table1_item datashade" NOWRAP="" colspan="2">
              <SPAN class="defaultbold">Current Pricing Ratios</SPAN>
            </TD>
          </TR>
          <TR valign="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 610, 0); return false" title="Price / Book (%)">
                <SPAN CLASS="defaultbold">Price / Book (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1843, 0); return false" title="Price / Tangible Book (%)">
                <SPAN CLASS="defaultbold">Price / Tangible Book (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToTangibleBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 609, 0); return false" title="Price / Earnings (x)">
                <SPAN CLASS="defaultbold">Price / Earnings (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1842, 0); return false" title="Price / EPS (x)">
                <SPAN CLASS="defaultbold">
                  Price / <xsl:value-of select="Company/FiscalPeriodDisplay"/> EPS (x)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9637, 0); return false" title="Price / Core EPS (x)">
                <SPAN CLASS="defaultbold">
                  Price / <xsl:value-of select="Company/FiscalPeriodDisplay"/> Core EPS (x)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToCoreEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $HOMEBUILDER">
          <TR VALIGN="TOP">
            <TD CLASS="table1_item datashade" NOWRAP="" colspan="2">
              <SPAN class="defaultbold">Current Pricing Ratios</SPAN>
            </TD>
          </TR>
          <TR valign="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 610, 0); return false" title="Price / Book (%)">
                <SPAN CLASS="defaultbold">Price / Book (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9632, 0); return false" title="Price / Earnings (x)">
                <SPAN CLASS="defaultbold">Price / Earnings (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEarningsBeforeExtra,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9631, 0); return false" title="Price/ LTM EPS (x)">
                <SPAN CLASS="defaultbold">Price/ LTM EPS (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEarningsLTMBeforeExtra,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $INVESTMENT_COMPANY">
          <TR VALIGN="TOP">
            <TD CLASS="table1_item datashade" NOWRAP="" colspan="2">
              <SPAN class="defaultbold">Current Pricing Ratios</SPAN>
            </TD>
          </TR>
          <TR valign="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 610, 0); return false" title="Price / Book (%)">
                <SPAN CLASS="defaultbold">Price / Book (%) </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1843, 0); return false" title="Price / Earnings (x)">
                <SPAN CLASS="defaultbold">Price / Earnings (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 609, 0); return false" title="Price / EPS (x)">
                <SPAN CLASS="defaultbold">
                  Price / <xsl:value-of select="Company/FiscalPeriodDisplay"/> EPS (x)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1842, 0); return false" title="Price / EBITDA (x)">
                <SPAN CLASS="defaultbold">Price / EBITDA (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEBITDA,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $REIT">
          <TR VALIGN="TOP">
            <TD CLASS="table1_item datashade" NOWRAP="" colspan="2">
              <SPAN class="defaultbold">Current Pricing Ratios</SPAN>
            </TD>
          </TR>
          <xsl:choose>
            <xsl:when test="$keyspecialtaxstatus = '0'">
              <TR valign="TOP">
                <TD CLASS="data" NOWRAP="" width="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 610, 0); return false" title="Price / FFO (x)">
                    <SPAN CLASS="defaultbold">Price / FFO (x)</SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(CurrentPricingRatios/PriceToFFO,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
              <TR VALIGN="TOP">
                <TD CLASS="data" NOWRAP="" width="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1843, 0); return false" title="FFO Yield (%)">
                    <SPAN CLASS="defaultbold">FFO Yield (%)</SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(CurrentPricingRatios/FFOYield,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
              <TR VALIGN="TOP">
                <TD CLASS="data" NOWRAP="" width="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 609, 0); return false">
                    <SPAN CLASS="defaultbold">
                      Price / <xsl:value-of select="Company/FiscalPeriodDisplay"/> FFO (x)
                    </SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(CurrentPricingRatios/PriceToLTMFFO,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
              <TR VALIGN="TOP">
                <TD CLASS="data" NOWRAP="" width="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1842, 0); return false" title="Price / EBITDA (x)">
                    <SPAN CLASS="defaultbold">Price / EBITDA (x)</SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEBITDA,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
              <TR VALIGN="TOP">
                <TD CLASS="data" NOWRAP="" width="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9637, 0); return false" title="Price / Earnings (x)">
                    <SPAN CLASS="defaultbold">Price / Earnings (x)</SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
              <TR VALIGN="TOP">
                <TD CLASS="data" NOWRAP="" width="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9637, 0); return false" title="Price / Earnings (x)">
                    <SPAN CLASS="defaultbold">
                      Price / <xsl:value-of select="Company/FiscalPeriodDisplay"/> Earnings (x)
                    </SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(CurrentPricingRatios/PriceToLTMEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
            </xsl:when>
            <xsl:otherwise>
              <TR valign="TOP">
                <TD CLASS="data" NOWRAP="" width="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9633, 0); return false" title="Price / FFO (x)">
                    <SPAN CLASS="defaultbold">Price / FFO (x)</SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(CurrentPricingRatios/PriceToFFO,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
              <TR VALIGN="TOP">
                <TD CLASS="data" NOWRAP="" width="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1843, 0); return false" title="FFO Yield (%)">
                    <SPAN CLASS="defaultbold">FFO Yield (%)</SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(CurrentPricingRatios/FFOYield,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
              <TR VALIGN="TOP">
                <TD CLASS="data" NOWRAP="" width="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 609, 0); return false" title="Price / FFO (x)">
                    <SPAN CLASS="defaultbold">
                      Price / <xsl:value-of select="Company/FiscalPeriodDisplay"/> FFO (x)
                    </SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(CurrentPricingRatios/PriceToLTMFFO,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
              <TR VALIGN="TOP">
                <TD CLASS="data" NOWRAP="" width="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1842, 0); return false" title="Price / EBITDA (x)">
                    <SPAN CLASS="defaultbold">Price / EBITDA (x)</SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEBITDA,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
              <TR VALIGN="TOP">
                <TD CLASS="data" NOWRAP="" width="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9637, 0); return false" title="Price / Earnings (x)">
                    <SPAN CLASS="defaultbold">Price / Earnings (x)</SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
              <TR VALIGN="TOP">
                <TD CLASS="data" NOWRAP="" width="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9637, 0); return false" title="Price / Earnings (x)">
                    <SPAN CLASS="defaultbold">
                      Price / <xsl:value-of select="Company/FiscalPeriodDisplay"/> Earnings (x)
                    </SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(CurrentPricingRatios/PriceToLTMEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $INSURANCE_BROKER or $GAAPDomain = $INSURANCE">
          <TR VALIGN="TOP">
            <TD CLASS="table1_item datashade" NOWRAP="" colspan="2">
              <SPAN class="defaultbold">Current Pricing Ratios</SPAN>
            </TD>
          </TR>
          <TR valign="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 610, 0); return false" title="Price / Book (%)">
                <SPAN CLASS="defaultbold">Price / Book (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1843, 0); return false" title="Price / Tangible Book (%)">
                <SPAN CLASS="defaultbold">Price / Tangible Book (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToTangibleBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1843, 0); return false" title="Price / Book, excl. 115 (%)">
                <SPAN CLASS="defaultbold">Price / Book, excl. 115 (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToBookExcluding115,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 609, 0); return false" title="Price / Earnings (x)">
                <SPAN CLASS="defaultbold">Price / Earnings (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1842, 0); return false" title="Price / EPS (x)">
                <SPAN CLASS="defaultbold">
                  Price / <xsl:value-of select="Company/FiscalPeriodDisplay"/> EPS (x)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9637, 0); return false" title="Price / Operating EPS (x)">
                <SPAN CLASS="defaultbold">
                  Price / <xsl:value-of select="Company/FiscalPeriodDisplay"/> Operating EPS (x)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToOperatingEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $ENERGY or $GAAPDomain = $GAS">
          <TR VALIGN="TOP">
            <TD CLASS="table1_item datashade" NOWRAP="" colspan="2">
              <SPAN class="defaultbold">Current Pricing Ratios</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1842, 0); return false" title="Price / EBITDA (x)">
                <SPAN CLASS="defaultbold">Price / EBITDA (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEBITDA,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9632, 0); return false" title="Price / Earnings (x)">
                <SPAN CLASS="defaultbold">Price / Earnings (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEarningsBeforeExtra,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR valign="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9631, 0); return false" title="Price / LTM Earnings (x)">
                <SPAN CLASS="defaultbold">Price / LTM Earnings (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToLTMEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $FINTECH">
          <TR VALIGN="TOP">
            <TD CLASS="table1_item datashade" NOWRAP="" colspan="2">
              <SPAN class="defaultbold">Current Pricing Ratios</SPAN>
            </TD>
          </TR>
          <TR valign="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 610, 0); return false">
                <SPAN CLASS="defaultbold">Price / Book (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1843, 0); return false" title="Price / Tangible Book (%)">
                <SPAN CLASS="defaultbold">Price / Tangible Book (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToTangibleBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9632, 0); return false" title="Price / Earnings (x)">
                <SPAN CLASS="defaultbold">Price / Earnings (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR valign="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9631, 0); return false" title="Price / LTM Earnings (x)">
                <SPAN CLASS="defaultbold">Price / LTM Earnings (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToLTMEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9639, 0); return false" title="Price / EBITDA (x)">
                <SPAN CLASS="defaultbold">Price / EBITDA (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEBITDA,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $MEDIA_ENT or $GAAPDomain = $COMMUNICATIONS or $GAAPDomain = $NEWMEDIA">
          <TR VALIGN="TOP">
            <TD CLASS="table1_item datashade" NOWRAP="" colspan="2">
              <SPAN class="defaultbold">Current Pricing</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 609, 0); return false" title="Price / Earnings (x)">
                <SPAN CLASS="defaultbold">Price / Earnings (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1842, 0); return false" title="Price / EPS (x)">
                <SPAN CLASS="defaultbold">
                  Price / <xsl:value-of select="Company/FiscalPeriodDisplay"/> EPS (x)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:otherwise>
          <TR VALIGN="TOP">
            <TD CLASS="table1_item datashade" NOWRAP="" colspan="2">
              <SPAN class="defaultbold">Current Pricing Ratios</SPAN>
            </TD>
          </TR>
          <TR valign="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 610, 0); return false" title="Price / Book (%)">
                <SPAN CLASS="defaultbold">Price / Book (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1843, 0); return false" title="Price / Tangible Book (%)">
                <SPAN CLASS="defaultbold">Price / Tangible Book (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToTangibleBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 609, 0); return false" title="Price / Earnings (x)">
                <SPAN CLASS="defaultbold">Price / Earnings (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1842, 0); return false" title="Price / EPS (x)">
                <SPAN CLASS="defaultbold">
                  Price /<xsl:value-of select="Company/FiscalPeriodDisplay"/> EPS (x)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(CurrentPricingRatios/PriceToEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:otherwise>
      </xsl:choose>
    </TABLE>
  </xsl:template>

  <xsl:template name="Price_Change_stock3">
    <xsl:variable name="mytradingsymbol" select="/irw:IRW/irw:StockInformation/TradedIssues/TradingSymbol[../KeyFndg = /irw:IRW/irw:StockInformation/Company/KeyFndg]"/>
    <TABLE CELLPADDING="3" CELLSPACING="0" CLASS="table2" WIDTH="100%" BORDER="0" ID="TABLE16">
      <TR>
        <TD CLASS="table1_item datashade" WIDTH="40%">
          <SPAN class="defaultbold">Price Change&#160;(%)</SPAN>
        </TD>
        <TD CLASS="table1_item datashade" align="right" WIDTH="28%">
          <SPAN class="defaultbold">
            <xsl:value-of select="$mytradingsymbol"/>
          </SPAN>&#160;&#160;&#160;&#160;
        </TD>
        <xsl:if test="Company/ComparativeIndexName != ''">
          <TD CLASS="table1_item datashade" align="right" WIDTH="32%">
            <SPAN class="defaultbold">
              <xsl:value-of select="Company/ComparativeIndexName"/> Index
            </SPAN>
          </TD>
        </xsl:if>
      </TR>
      <TR >
        <TD CLASS="data" NOWRAP="" align="LEFT" WIDTH="40%">
          <SPAN CLASS="defaultbold">One Day</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="format-number(PriceChange/OneDay,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
          <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
        </TD>
        <xsl:if test="Company/ComparativeIndexName != ''">
          <TD CLASS="data" WIDTH="28%" align="right">
            <xsl:value-of select="format-number(PriceChange/OneDayIndex,'#,##0.00 ;(#,##0.00)','data')"/>
            <SPAN CLASS="dataalign">&#160;</SPAN>
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </TD>
        </xsl:if>
      </TR>
      <TR >
        <TD CLASS="data" NOWRAP="" align="LEFT" WIDTH="40%">
          <SPAN CLASS="defaultbold">One Month</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="format-number(PriceChange/OneMonth,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
          <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
        </TD>
        <xsl:if test="Company/ComparativeIndexName != ''">
          <TD CLASS="data" WIDTH="28%" align="right">
            <xsl:value-of select="format-number(PriceChange/OneMonthIndex,'#,##0.00 ;(#,##0.00)','data')"/>
            <SPAN CLASS="dataalign">&#160;</SPAN>
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </TD>
        </xsl:if>
      </TR>
      <TR >
        <TD CLASS="data" NOWRAP="" align="LEFT" WIDTH="40%">
          <SPAN CLASS="defaultbold">Three Month</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="format-number(PriceChange/ThreeMonth,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
          <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
        </TD>
        <xsl:if test="Company/ComparativeIndexName != ''">
          <TD CLASS="data" WIDTH="28%" align="right">
            <xsl:value-of select="format-number(PriceChange/ThreeMonthIndex,'#,##0.00 ;(#,##0.00)','data')"/>
            <SPAN CLASS="dataalign">&#160;</SPAN>
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </TD>
        </xsl:if>
      </TR>
      <TR >
        <TD CLASS="data" NOWRAP="" align="LEFT" WIDTH="40%">
          <SPAN CLASS="defaultbold">YTD</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="format-number(PriceChange/YTD,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
          <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
        </TD>
        <xsl:if test="Company/ComparativeIndexName != ''">
          <TD CLASS="data" WIDTH="28%" align="right">
            <xsl:value-of select="format-number(PriceChange/YTDIndex,'#,##0.00 ;(#,##0.00)','data')"/>
            <SPAN CLASS="dataalign">&#160;</SPAN>
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </TD>
        </xsl:if>
      </TR>
      <TR >
        <TD CLASS="data" NOWRAP="" align="LEFT" WIDTH="40%">
          <SPAN CLASS="defaultbold">One Year</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="format-number(PriceChange/OneYear,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
          <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
        </TD>
        <xsl:if test="Company/ComparativeIndexName != ''">
          <TD CLASS="data" WIDTH="28%" align="right">
            <xsl:value-of select="format-number(PriceChange/OneYearIndex,'#,##0.00 ;(#,##0.00)','data')"/>
            <SPAN CLASS="dataalign">&#160;</SPAN>
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </TD>
        </xsl:if>
      </TR>
    </TABLE>
  </xsl:template>

  <xsl:template name="Dividends_stock3">
    <xsl:variable name="divkey">
      <xsl:choose>
        <xsl:when test="Dividends/IsDivAggregate = 'True'">46843</xsl:when>
        <xsl:otherwise>9640</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="divyieldkey">
      <xsl:choose>
        <xsl:when test="Dividends/IsDivAggregate = 'True'">46844</xsl:when>
        <xsl:otherwise>2425</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" CLASS="table2" WIDTH="100%" ID="TABLE17">
      <xsl:variable name="GAAPDomain" select="Company/GAAP"/>
      <xsl:variable name="REIT" select="6"/>
      <xsl:variable name="ENERGY" select="10"/>
      <xsl:variable name="GAS" select="18"/>
      <xsl:variable name="FINTECH" select="15"/>
      <xsl:variable name="keyinstn" select="Company/KeyInstn" />
      <xsl:variable name="keyspecialtaxstatus" select="Company/KeySpecialTaxStatus" />
      <xsl:choose>
        <xsl:when test="$GAAPDomain = $REIT">
          <TR VALIGN="TOP">
            <TD colspan="2" CLASS="table1_item datashade">
              <SPAN class="defaultbold">Dividends</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, {$divkey}, 0); return false" title="Quarterly Dividend ($)">
                <SPAN CLASS="defaultbold">Quarterly Dividend ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" width="40%" align="right">
              <xsl:value-of select="format-number(Dividends/QuarterlyDividend,'#,##0.0000 ;(#,##0.0000)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, {$divyieldkey}, 0); return false" title="Dividend Yield (%)">
                <SPAN CLASS="defaultbold">Dividend Yield (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" width="40%" align="right">
              <xsl:value-of select="format-number(Dividends/DividendYield,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <xsl:if test="Company/IsCommonStock='True'">
            <TR VALIGN="TOP">
              <!--### Change decimal places to 4 as per "Quarterly Dividend" above ###-->
              <TD CLASS="data" width="60%">
                <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 'Dividend', 0); return false" title="Dividends Announced ($)">
                  <SPAN CLASS="defaultbold">
                    <xsl:value-of select="Company/FiscalPeriodDisplay"/> Dividends Announced ($)
                  </SPAN>
                </a>
              </TD>
              <TD CLASS="data" width="40%" align="right">
                <xsl:value-of select="format-number(Dividends/Dividend,'#,##0.0000 ;(#,##0.0000)','data')"/>
                <SPAN CLASS="dataalign">&#160;</SPAN>
              </TD>
            </TR>
            <TR VALIGN="TOP">
              <xsl:choose>
                <xsl:when test="$keyspecialtaxstatus = '0'">
                  <TD CLASS="data" width="60%">
                    <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 'DividendPayout', 0); return false" title="Payout Ratio (%)">
                      <SPAN CLASS="defaultbold">
                        <xsl:value-of select="Company/FiscalPeriodDisplay"/> Payout Ratio (%)
                      </SPAN>
                    </a>
                  </TD>
                </xsl:when>
                <xsl:otherwise>
                  <TD CLASS="data" width="60%">
                    <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 'DividendPayoutFFO', 0); return false" title="Payout Ratio (%)">
                      <SPAN CLASS="defaultbold">
                        <xsl:value-of select="Company/FiscalPeriodDisplay"/> Payout Ratio (%)
                      </SPAN>
                    </a>
                  </TD>
                </xsl:otherwise>
              </xsl:choose>
              <TD CLASS="data" width="40%" align="right">
                <xsl:value-of select="format-number(Dividends/LTMPayoutRatio,'#,##0.00 ;(#,##0.00)','data')"/>
                <SPAN CLASS="dataalign">&#160;</SPAN>
              </TD>
            </TR>
          </xsl:if>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $ENERGY or $GAAPDomain = $GAS">
          <TR VALIGN="TOP">
            <TD colspan="2" CLASS="table1_item datashade">
              <SPAN class="defaultbold">Dividends</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 13498, 0); return false" title="Dividend Paid ($)">
                <SPAN CLASS="defaultbold">Dividend Paid ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" width="40%" align="right">
              <xsl:value-of select="format-number(Dividends/DividendPaid,'#,##0.0000 ;(#,##0.0000)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 3249, 0); return false" title="Dividend Payout (%)">
                <SPAN CLASS="defaultbold">Dividend Payout (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" width="40%" align="right">
              <xsl:value-of select="format-number(Dividends/DividendPayout,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9935, 0); return false" title="Dividend / Book Per Share (x)">
                <SPAN CLASS="defaultbold">Dividend / Book Per Share (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" width="40%" align="right">
              <xsl:value-of select="format-number(Dividends/DividendToBookPerShare,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $FINTECH">
          <TR VALIGN="TOP">
            <TD colspan="2" CLASS="table1_item datashade">
              <SPAN class="defaultbold">Dividends</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, {$divkey}, 0); return false" title="Quarterly Dividend ($)">
                <SPAN CLASS="defaultbold">Quarterly Dividend ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" width="40%" align="right">
              <xsl:value-of select="format-number(Dividends/QuarterlyDividend,'#,##0.0000 ;(#,##0.0000)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, {$divyieldkey}, 0); return false" title="Dividend Yield (%)">
                <SPAN CLASS="defaultbold">Dividend Yield (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" width="40%" align="right">
              <xsl:value-of select="format-number(Dividends/DividendYield,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <!--### Change decimal places to 4 as per "Quarterly Dividend" above ###-->
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 20350, 0); return false" title="LTM Dividends Announced ($)">
                <SPAN CLASS="defaultbold">LTM Dividends Announced ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" width="40%" align="right">
              <xsl:value-of select="format-number(Dividends/DivAnnouncedLTM,'#,##0.0000 ;(#,##0.0000)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 3249, 0); return false" title="Dividend Payout Ratio (%)">
                <SPAN CLASS="defaultbold">Dividend Payout Ratio (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" width="40%" align="right">
              <xsl:value-of select="format-number(Dividends/LTMPayoutRatio,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:otherwise>
          <TR VALIGN="TOP">
            <TD colspan="2" CLASS="table1_item datashade">
              <SPAN class="defaultbold">Dividends</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, {$divkey}, 0); return false" title="Quarterly Dividend ($)">
                <SPAN CLASS="defaultbold">Quarterly Dividend ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" width="40%" align="right">
              <xsl:value-of select="format-number(Dividends/QuarterlyDividend,'#,##0.0000 ;(#,##0.0000)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, {$divyieldkey}, 0); return false" title="Dividend Yield (%)">
                <SPAN CLASS="defaultbold">Dividend Yield (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" width="40%" align="right">
              <xsl:value-of select="format-number(Dividends/DividendYield,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <xsl:if test="Company/IsCommonStock='True'">
            <TR VALIGN="TOP">
              <!--### Change decimal places to 4 as per "Quarterly Dividend" above ###-->
              <TD CLASS="data" width="60%">
                <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 'Dividend', 0); return false" title="Dividend ($)">
                  <SPAN CLASS="defaultbold">
                    <xsl:value-of select="Company/FiscalPeriodDisplay"/> Dividend ($)
                  </SPAN>
                </a>
              </TD>
              <TD CLASS="data" width="40%" align="right">
                <xsl:value-of select="format-number(Dividends/Dividend,'#,##0.0000 ;(#,##0.0000)','data')"/>
                <SPAN CLASS="dataalign">&#160;</SPAN>
              </TD>
            </TR>
            <xsl:if test="Company/GAAP != 0">
            <TR VALIGN="TOP">
              <TD CLASS="data" width="60%">
                <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 'DividendPayout', 0); return false" title="Payout Ratio (%)">
                  <SPAN CLASS="defaultbold">
                    <xsl:value-of select="Company/FiscalPeriodDisplay"/> Payout Ratio (%)
                  </SPAN>
                </a>
              </TD>
              <TD CLASS="data" width="40%" align="right">
                <xsl:value-of select="format-number(Dividends/PayoutRatio,'#,##0.00 ;(#,##0.00)','data')"/>
                <SPAN CLASS="dataalign">&#160;</SPAN>
              </TD>
            </TR>
          </xsl:if>
          </xsl:if>
        </xsl:otherwise>
      </xsl:choose>
    </TABLE>
  </xsl:template>

  <!-- Template THREE Ends here. -->


  <!-- Start Template 4 -->
  <xsl:template name="Quote_Table_stock4">
    <style>
      .stockdata-header-border{
      border-right:1px solid #CCCCCC;
      border-top:1px solid #CCCCCC;
      border-bottom:1px solid #CCCCCC;
      }

      .stockdata-header-border-first{
      border-top:1px solid #CCCCCC;
      border-right:1px solid #CCCCCC;
      border-bottom:1px solid #CCCCCC;
      border-left:1px solid #CCCCCC;
      }

      .stockdata-border-first{
      border-right:1px solid #CCCCCC;
      border-left:1px solid #CCCCCC;
      border-bottom:1px solid #CCCCCC;
      }
      .stockdata-border{
      border-right:1px solid #CCCCCC;
      border-bottom:1px solid #CCCCCC;
      }
      .uptriangle, .downtriangle { margin-left: 2px; }
    </style>
    <xsl:variable name="keyinstn" select="Company/KeyInstn"/>
    <br />
    <TABLE border="0" cellspacing="0" cellpadding="3" width="100%" >
      <TR>
        <TD colspan="8" align="right">
          <xsl:choose>
            <xsl:when test="StockQuote/Time = '12:00 AM'" >
              <xsl:value-of select="StockQuote/QuoteTimeStamp"/>&#160;Close
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="StockQuote/QuoteTimeStamp"/>&#160;ET
            </xsl:otherwise>
          </xsl:choose> 
        </TD>
      </TR>
      <TR>
        <TD align="center" class="datashade stockdata-header-border-first" >Last</TD>
        <TD align="center" class="datashade stockdata-header-border" >Change</TD>
        <TD align="center" class="datashade stockdata-header-border" >&#37;Change</TD>
        <TD align="center" class="datashade stockdata-header-border" >Volume</TD>
        <TD align="center" class="datashade stockdata-header-border" >Today's High</TD>
        <TD align="center" class="datashade stockdata-header-border" >Today's Low</TD>
        <TD align="center" class="datashade stockdata-header-border" >Open</TD>
        <!--<td align="center" class="datashade stockdata-header-border" >Close</td>-->
      </TR>

      <TR>
        <TD align="center" class="stockdata-border-first">
          <SPAN class="default">
            <xsl:variable name="CurrentPrice" select="StockQuote/CurrentPrice"/>
            <xsl:choose>
              <xsl:when test="$CurrentPrice &gt; 0">
                &#36;<xsl:value-of select="format-number($CurrentPrice, '###,##0.00')"/>
              </xsl:when>
              <xsl:otherwise>NA</xsl:otherwise>
            </xsl:choose>
          </SPAN>
        </TD>
        <TD align="center" class="stockdata-border">
          <SPAN class="default">
            <xsl:variable name="NetChangeFromPreviousClose" select="StockQuote/NetChangeFromPreviousClose"/>
            <xsl:choose>
              <xsl:when test="$NetChangeFromPreviousClose != 0">
                <xsl:choose>
                  <xsl:when test="$NetChangeFromPreviousClose &gt; 0">
                    &#36;<xsl:value-of select="format-number($NetChangeFromPreviousClose,'###,##0.00','data')"/>&#160;
                  </xsl:when>
                  <xsl:otherwise>
                    &#36;<xsl:value-of select="$NetChangeFromPreviousClose"/>&#160;
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:when>
              <xsl:otherwise>
                NA
              </xsl:otherwise>
            </xsl:choose>
          </SPAN>
        </TD>
        <TD align="center" class="stockdata-border">
          <SPAN class="default">
            <xsl:variable name="PercentChangeFromPreviousClose" select="StockQuote/PercentChangeFromPreviousClose"/>
            <xsl:choose>
              <xsl:when test="$PercentChangeFromPreviousClose != 0">
                <xsl:choose>
                  <xsl:when test="$PercentChangeFromPreviousClose &gt; 0">
                    <xsl:if test="StockQuote/NetChangeDirection = 'negative'">(</xsl:if><xsl:value-of select="format-number($PercentChangeFromPreviousClose, '###,##0.00')"/> <xsl:if test="StockQuote/NetChangeDirection = 'negative'">)</xsl:if>
                  </xsl:when>
                  <xsl:otherwise><xsl:value-of select="format-number($PercentChangeFromPreviousClose, '###,##0.00')"/></xsl:otherwise>
                </xsl:choose>
                    <xsl:if test="StockQuote/NetChangeDirection = 'positive'">
                      <SPAN class="default"><IMG class="uptriangle" SRC="/images/Interactive/IR/uptriangle.gif" WIDTH="11" HEIGHT="10" BORDER="0" ALT="Up" /></SPAN>
                    </xsl:if>
                    <xsl:if test="StockQuote/NetChangeDirection = 'negative'">
                      <SPAN class="default"><IMG class="downtriangle" SRC="/images/Interactive/IR/downtriangle.gif" WIDTH="11" HEIGHT="10" BORDER="0" ALT="Down" /></SPAN>
                    </xsl:if>
              </xsl:when>
              <xsl:otherwise>NA</xsl:otherwise>
            </xsl:choose>
          </SPAN>
        </TD>
        <TD align="center" class="stockdata-border">
          <SPAN class="default">
            <xsl:value-of select="StockQuote/VolumeToday"/>
          </SPAN>
        </TD>
        <TD align="center" class="stockdata-border">
          <SPAN class="default">
            <xsl:choose>
              <xsl:when test="StockQuote/HighToday &gt; 0">
                &#36;<xsl:value-of select="format-number(StockQuote/HighToday, '###,##0.00')"/>
              </xsl:when>
              <xsl:otherwise>NA</xsl:otherwise>
            </xsl:choose>
          </SPAN>
        </TD>
        <TD align="center" class="stockdata-border">
          <SPAN class="default">
            <xsl:choose>
              <xsl:when test="StockQuote/LowToday &gt; 0">
                &#36;<xsl:value-of select="format-number(StockQuote/LowToday, '###,##0.00')"/>
              </xsl:when>
              <xsl:otherwise>NA</xsl:otherwise>
            </xsl:choose>
          </SPAN>
        </TD>
        <TD align="center" class="stockdata-border">
          <SPAN class="default">
            <xsl:choose>
              <xsl:when test="StockQuote/FndgPriceOpen &gt; 0">
                &#36;<xsl:value-of select="format-number(StockQuote/FndgPriceOpen, '###,##0.00')"/>
              </xsl:when>
              <xsl:otherwise>NA</xsl:otherwise>
            </xsl:choose>
          </SPAN>
        </TD>
        <!--<td align="center" class="stockdata-border">
          <span class="default">
            <xsl:choose>
              <xsl:when test="StockQuote/LastPrice &gt; 0">
                &#36;<xsl:value-of select="format-number(StockQuote/LastPrice, '###,##0.00')"/>
              </xsl:when>
              <xsl:otherwise>NA</xsl:otherwise>
            </xsl:choose>
          </span>
        </td>-->
      </TR>
    </TABLE>
    <br />
  </xsl:template>

  <xsl:template name="Descriptor_stock4">
    <style type="text/css">
      .ir_chartOption_Wrapper { text-align: right; padding: 20px 0 0 0; border-bottom: 1px solid #CCCCCC; }
      .ChartOptions { text-align: right; float: right; position: relative; margin:0 10px 0 0; }
      .ChartOptions div { margin: 0;}
      span.LeftNavfly {position:relative; background: url(images/ArrowDown.gif) no-repeat 95% 50%; padding: 0px 25px 0px 10px; display: block; height: 20px; line-height: 20px; cursor: pointer; }
      a.menu-open { background-color:#ddeef6!important; color:#666!important; outline:none;}
      span.LeftNavfly a, span.LeftNavfly a img { border: 0; padding:2px}
      span.LeftNavfly a { padding: 5px 3px; }
      .ChartOptions div.closeprice { display:none; background:#e9e9e9; position:absolute; z-index:100; text-align:left; padding:10px; top: 21px; right: 0px; margin-right: 0px; *margin-right: -1px;font-size:12px; border: 1px solid #CCC; white-space: nowrap;}
      #ChartDataHoverTable { margin: 0 auto; font-size: 11px; }
      #simpleTooltip {font-family:Arial, Helvetica, sans-serif; padding: 5px; border: 1px solid #cccccc; background: #EEE; font-size:11px; height:14px; line-height:14px; color:#666; font-weight: bold;}
      .closeprice{padding:5px;}
      .enterprice{ padding-top:5px;}
      .l_section{ width: 70%; float:left }
      .r_section {width: 30%; float: right}
    </style>
    <div class="l_section">
      <xsl:if test="TradedIssues/RowCount > 1">
        <a>
          <xsl:attribute name="href">#</xsl:attribute>
          <xsl:attribute name="OnClick">
            window.open('fndgdesc.aspx?IID=<xsl:value-of select="Company/KeyInstn"/>', 'SecurityDescriptions', 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=400,height=300');
          </xsl:attribute>
          Select link to view descriptions of our traded funding issues.
        </a>
        <br />
      </xsl:if>

      <xsl:if test="Company/ShowEmailNotificationLink = 'true'">
        <xsl:for-each select="CLIENTEMAILPREFS[NotificationType = '1' and NotificationEnabled = 'True']">
          <a>
            <xsl:attribute name="href">
              email.aspx?IID=<xsl:value-of select="../Company/KeyInstn"/>
            </xsl:attribute>
            Notify me with an end-of-day stock quote.
          </a>
          <br />
        </xsl:for-each>
      </xsl:if>
    </div>
    <div class="r_section">
      <div class="ChartOptions">
        <SPAN class="LeftNavfly">Stock Price Lookup</SPAN>
        <div class="closeprice">
          Enter a date below to request the <br/>closing price for that specific day:
          <div class="enterprice">
            <input size="10" id="closeprice" type="text" class="input"/>&#160;
            <input type="button" value="Enter" id="Button1"  class="submit" name="submit1">
              <xsl:attribute name="OnClick">
                window.open('quotepop.aspx?IID=<xsl:value-of select="Company/KeyInstn"/>&amp;KeyFndg=<xsl:value-of select="Company/KeyFndg"/>&amp;date=' + document.getElementById('closeprice').value, 'ClosingPrice', 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=60')
              </xsl:attribute>
            </input>
          </div>
        </div>
      </div>
    </div>
  </xsl:template>

  <xsl:template name="Chart_Drop_Image_stock4">
    <xsl:variable name="isPreview" select="Company/IsPreview" />
    <div id="graphContainer"></div>
    <xsl:variable name="KeyInstn" select="Company/KeyInstn" />
    <xsl:variable name="KeyFndg" select="Company/KeyFndg" />        
    <xsl:choose >
      <xsl:when test="contains(Company/URL, 'print=1') or contains(Company/URL, 'Print=1')">        
        <xsl:variable name="cHeight" select="GraphSize/Height" /> 
        <iframe id="onlyImageFrame" name="onlyImageFrame" width="100%" scrolling="no" frameborder="0">          
          <xsl:attribute name="height">
            <xsl:choose>
              <xsl:when test="$cHeight = 325">
                <xsl:value-of select="$cHeight + 170"/>  
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="$cHeight + 90"/>  
              </xsl:otherwise>              
            </xsl:choose>        
          </xsl:attribute>
          <xsl:attribute name="src">
            StockChartIFrame.aspx?pdf=1&amp;KeyInstn=<xsl:value-of select="$KeyInstn"/>            
          </xsl:attribute>
        </iframe>
      </xsl:when>
      <xsl:otherwise>
        <iframe id="myStockframe" name="myStockframe" class="autoHeight" width= "100%" scrolling="no" frameborder="0">
        </iframe>
      </xsl:otherwise>
    </xsl:choose>
    <div align="right">Minimum 20 minute delay</div>
  </xsl:template>

  <xsl:template name="FinancialDataTables4">
    <xsl:variable name="colspan" select="count(TableOptions)"/>
    <xsl:if test="$colspan &gt; 0">
      <div style="padding-bottom: 5px; height: 15px;" class="defaultbold">
        <SPAN id="showdata" class="defaultbold" style="display: none;">For the definition of a financial field, select the field name link.</SPAN>
      </div>
      <xsl:if test="not(contains(translate(Company/URL,$ucletters,$lcletters), 'print=1'))">
      <ul id="stock_tabs">
        <xsl:for-each select="TableOptions">
          <li class="stock_tab table1">
            <xsl:choose>
              <xsl:when test="ID = 1 or ID = 3 or ID = 5">
                <xsl:attribute name="onclick">document.getElementById('showdata').style.display = 'block'</xsl:attribute>
              </xsl:when>
              <xsl:otherwise>
                <xsl:attribute name="onclick">document.getElementById('showdata').style.display = 'none'</xsl:attribute>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:attribute name="id">simpleTab<xsl:value-of select="position()" disable-output-escaping="yes"/></xsl:attribute>
            <xsl:value-of select="Name" disable-output-escaping="yes"/>
          </li>
        </xsl:for-each>
      </ul>
      </xsl:if>
      <div id="stock_TabData" class="table2">
        <xsl:variable name="mytradingsymbol" select="/irw:IRW/irw:StockInformation/TradedIssues/TradingSymbol[../KeyFndg = /irw:IRW/irw:StockInformation/Company/KeyFndg]"/>

        <xsl:if test="Company/IsCommonStock='True'">
          <xsl:for-each select="TableOptions">
            <xsl:if test="ID = 0">
              <div class="panel">
                <xsl:attribute name="id">simpleContent<xsl:value-of select="position()" disable-output-escaping="yes"/></xsl:attribute>
                <xsl:attribute name="colspan"><xsl:value-of select="$colspan" disable-output-escaping="yes"/></xsl:attribute>
                <xsl:call-template name="Vol_Highlights_stock4"/>
              </div>
            </xsl:if>
            <xsl:if test="ID = 1">
              <xsl:if test="not(../Company/GAAP=0)">
                <div class="panel">
                <xsl:attribute name="id">simpleContent<xsl:value-of select="position()" disable-output-escaping="yes"/></xsl:attribute>
                <xsl:attribute name="colspan"><xsl:value-of select="$colspan" disable-output-escaping="yes"/></xsl:attribute>
                  <xsl:call-template name="Fin_Data_stock4"/>
                </div>
              </xsl:if>
            </xsl:if>
            <xsl:if test="ID = 2">
              <div class="panel">
                <xsl:attribute name="id">simpleContent<xsl:value-of select="position()" disable-output-escaping="yes"/></xsl:attribute>
                <xsl:attribute name="colspan"><xsl:value-of select="$colspan" disable-output-escaping="yes"/></xsl:attribute>
                <xsl:call-template name="Stock_Price_stock4"/>
              </div>
            </xsl:if>
            <xsl:if test="ID = 3">
              <xsl:if test="not(../Company/GAAP=0)">
                <div class="panel">
                <xsl:attribute name="id">simpleContent<xsl:value-of select="position()" disable-output-escaping="yes"/></xsl:attribute>
                <xsl:attribute name="colspan"><xsl:value-of select="$colspan" disable-output-escaping="yes"/></xsl:attribute>
                  <xsl:call-template name="Price_Ratios_stock4"/>
                </div>
              </xsl:if>
            </xsl:if>
            <xsl:if test="ID = 4">
              <div class="panel">
                <xsl:attribute name="id">simpleContent<xsl:value-of select="position()" disable-output-escaping="yes"/></xsl:attribute>
                <xsl:attribute name="colspan"><xsl:value-of select="$colspan" disable-output-escaping="yes"/></xsl:attribute>
                <xsl:call-template name="Price_Change_stock4"/>
              </div>
            </xsl:if>
            <xsl:if test="ID = 5">              
              <div class="panel">
              <xsl:attribute name="id">simpleContent<xsl:value-of select="position()" disable-output-escaping="yes"/></xsl:attribute>
              <xsl:attribute name="colspan"><xsl:value-of select="$colspan" disable-output-escaping="yes"/></xsl:attribute>
                <xsl:call-template name="Dividends_stock4"/>
              </div>              
            </xsl:if>
          </xsl:for-each>
        </xsl:if>

        <xsl:if test="Company/IsCommonStock='False'">
          <xsl:for-each select="TableOptions">
            <xsl:if test="ID = 0">
              <div class="panel">
                <xsl:attribute name="id">simpleContent<xsl:value-of select="position()" disable-output-escaping="yes"/></xsl:attribute>
                <xsl:attribute name="colspan"><xsl:value-of select="$colspan" disable-output-escaping="yes"/></xsl:attribute>
                <xsl:call-template name="Vol_Highlights_stock4"/>
              </div>
            </xsl:if>

            <xsl:if test="ID = 2">
              <div class="panel">
                <xsl:attribute name="id">simpleContent<xsl:value-of select="position()" disable-output-escaping="yes"/></xsl:attribute>
                <xsl:attribute name="colspan"><xsl:value-of select="$colspan" disable-output-escaping="yes"/></xsl:attribute>
                <xsl:call-template name="Stock_Price_stock4"/>
              </div>
            </xsl:if>
            <xsl:if test="ID = 3"></xsl:if>
            <xsl:if test="ID = 4">
              <div class="panel">
                <xsl:attribute name="id">simpleContent<xsl:value-of select="position()" disable-output-escaping="yes"/></xsl:attribute>
                <xsl:attribute name="colspan"><xsl:value-of select="$colspan" disable-output-escaping="yes"/></xsl:attribute>
                <TABLE CELLPADDING="3" CELLSPACING="0" CLASS="table2" WIDTH="100%" BORDER="0" ID="TABLE16">
                  <TR VALIGN="bottom">
                    <TD CLASS="table1_item datashade defaultbold" WIDTH="40%">Price Change&#160;(%)</TD>
                    <TD CLASS="table1_item datashade defaultbold" align="right">
                      <xsl:value-of select="$mytradingsymbol"/>
                    </TD>
                  </TR>
                  <TR>
                    <TD CLASS="data" NOWRAP="" align="LEFT" WIDTH="40%">
                      <SPAN CLASS="defaultbold">One Day</SPAN>
                    </TD>
                    <TD CLASS="data" WIDTH="28%" align="right">
                      <xsl:value-of select="format-number(../PriceChange/OneDay,'#,##0.00 ;(#,##0.00)','data')"/>
                      <SPAN CLASS="dataalign">&#160;</SPAN>
                    </TD>
                  </TR>
                  <TR>
                    <TD CLASS="data" NOWRAP="" align="LEFT" WIDTH="40%">
                      <SPAN CLASS="defaultbold">One Month</SPAN>
                    </TD>
                    <TD CLASS="data" WIDTH="28%" align="right">
                      <xsl:value-of select="format-number(../PriceChange/OneMonth,'#,##0.00 ;(#,##0.00)','data')"/>
                      <SPAN CLASS="dataalign">&#160;</SPAN>
                    </TD>
                  </TR>
                  <TR>
                    <TD CLASS="data" NOWRAP="" align="LEFT" WIDTH="40%">
                      <SPAN CLASS="defaultbold">Three Month</SPAN>
                    </TD>
                    <TD CLASS="data" WIDTH="28%" align="right">
                      <xsl:value-of select="format-number(../PriceChange/ThreeMonth,'#,##0.00 ;(#,##0.00)','data')"/>
                      <SPAN CLASS="dataalign">&#160;</SPAN>
                    </TD>
                  </TR>
                  <TR>
                    <TD CLASS="data" NOWRAP="" align="LEFT" WIDTH="40%">
                      <SPAN CLASS="defaultbold">YTD</SPAN>
                    </TD>
                    <TD CLASS="data" WIDTH="28%" align="right">
                      <xsl:value-of select="format-number(../PriceChange/YTD,'#,##0.00 ;(#,##0.00)','data')"/>
                      <SPAN CLASS="dataalign">&#160;</SPAN>
                    </TD>
                  </TR>
                  <TR>
                    <TD CLASS="data" NOWRAP="" align="LEFT" WIDTH="40%">
                      <SPAN CLASS="defaultbold">One Year</SPAN>
                    </TD>
                    <TD CLASS="data" WIDTH="28%" align="right">
                      <xsl:value-of select="format-number(../PriceChange/OneYear,'#,##0.00 ;(#,##0.00)','data')"/>
                      <SPAN CLASS="dataalign">&#160;</SPAN>
                    </TD>
                  </TR>
                </TABLE>
              </div>
            </xsl:if>

            <xsl:if test="ID = 5">              
              <div class="panel">
              <xsl:attribute name="id">simpleContent<xsl:value-of select="position()" disable-output-escaping="yes"/></xsl:attribute>
              <xsl:attribute name="colspan"><xsl:value-of select="$colspan" disable-output-escaping="yes"/></xsl:attribute>
                <xsl:call-template name="Dividends_stock4"/>
              </div>
            </xsl:if>
          </xsl:for-each>
        </xsl:if>
      </div>
    </xsl:if>
  </xsl:template>



  <xsl:template name="Vol_Highlights_stock4">
    <TABLE CELLSPACING="0" CELLPADDING="3" CLASS="table2" WIDTH="100%" BORDER="0" ID="TABLE10">
      <TR>
        <TD CLASS="table1_item datashade" WIDTH="40%" align="left">
          <SPAN class="defaultbold">Volume Highlights</SPAN>
        </TD>
        <TD CLASS="table1_item datashade" align="right" WIDTH="28%" nowrap="">
          <SPAN class="defaultbold">
            Avg Daily<br />Volume
          </SPAN>
        </TD>
        <TD CLASS="table1_item datashade" align="right" WIDTH="32%">
          <SPAN class="defaultbold">
            %&#160;of&#160;Shares<br />Outstanding
          </SPAN>
        </TD>
      </TR>
      <TR>
        <TD CLASS="data" NOWRAP="" align="LEFT" width="40%">
          <SPAN class="defaultbold">One Day</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="../VolumeHighlights/OneDayAvgDailyVolume"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="32%" align="right">
          <xsl:value-of select="format-number(../VolumeHighlights/OneDayPercentSharesOutstanding,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
      </TR>
      <TR>
        <TD CLASS="data" NOWRAP="" align="LEFT" width="40%">
          <SPAN class="defaultbold">One Month</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="../VolumeHighlights/OneMonthAvgDailyVolume"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="32%" align="right">
          <xsl:value-of select="format-number(../VolumeHighlights/OneMonthPercentSharesOutstanding,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
      </TR>
      <TR>
        <TD CLASS="data" NOWRAP="" align="LEFT" width="40%">
          <SPAN class="defaultbold">Three Months</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="../VolumeHighlights/ThreeMonthsAvgDailyVolume"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="32%" align="right">
          <xsl:value-of select="format-number(../VolumeHighlights/ThreeMonthsPercentSharesOutstanding,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
      </TR>
      <TR>
        <TD CLASS="data" NOWRAP="" align="LEFT" width="40%">
          <SPAN class="defaultbold"> YTD</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="../VolumeHighlights/YTDAvgDailyVolume"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="32%" align="right">
          <xsl:value-of select="format-number(../VolumeHighlights/YTDPercentSharesOutstanding,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
      </TR>
      <TR>
        <TD CLASS="data" NOWRAP="" align="LEFT" width="40%">
          <SPAN class="defaultbold">One Year</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="../VolumeHighlights/OneYearAvgDailyVolume"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="32%" align="right">
          <xsl:value-of select="format-number(../VolumeHighlights/OneYearPercentSharesOutstanding,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
      </TR>
    </TABLE>
  </xsl:template>

  <xsl:template name="Fin_Data_stock4">
    <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" CLASS="table2" WIDTH="100%" ID="TABLE11">
      <xsl:variable name="GAAPDomain" select="../Company/GAAP"/>
      <xsl:variable name="INSURANCE_UNDERWRITER" select="8"/>
      <xsl:variable name="INSURANCE_BROKER" select="9"/>
      <xsl:variable name="THRIFT" select="2"/>
      <xsl:variable name="INVESTMENT_COMPANY" select="7"/>
      <xsl:variable name="REIT" select="6"/>
      <xsl:variable name="BANK" select="1"/>
      <xsl:variable name="ENERGY" select="10"/>
      <xsl:variable name="GAS" select="18"/>
      <xsl:variable name="FINTECH" select="15"/>
      <xsl:variable name="HOMEBUILDER" select="16"/>
      <xsl:variable name="COMMUNICATIONS" select="14"/>
      <xsl:variable name="MEDIA_ENT" select="20"/>
      <xsl:variable name="NEWMEDIA" select="21"/>
      <xsl:variable name="keyinstn" select="../Company/KeyInstn"/>
      <xsl:choose>
        <xsl:when test="$GAAPDomain = $BANK or $GAAPDomain = $THRIFT">
          <TR VALIGN="TOP">
            <TD CLASS="surrleft table1_item datashade" NOWRAP="" WIDTH="60%" align="left">
              <SPAN class="defaultbold">Financial Data</SPAN>
            </TD>
            <TD CLASS="surrright table1_item datashade" WIDTH="40%" align="right">
              <SPAN class="defaultbold">
                <xsl:value-of select="../FinancialData/DateEnded"/>
              </SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EquityPerShareForCalcs', 1); return false" title="Book Value ($)">
                <SPAN CLASS="defaultbold">Book Value ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../FinancialData/BookValue,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'TangibleEquityPerShare', 1); return false" title="Tangible Book ($)">
                <SPAN CLASS="defaultbold">Tangible Book ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../FinancialData/TangibleBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSAfterExtraForCalcs', 0); return false" title="Quarter EPS ($)">
                <SPAN CLASS="defaultbold">Quarter EPS ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../FinancialData/QuarterEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSAfterExtraForCalcs', 0); return false">
                <SPAN CLASS="defaultbold">
                  <xsl:value-of select="../Company/FiscalPeriodDisplay"/> EPS ($)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../FinancialData/EPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'CoreEPS', 0); return false" title="Core EPS ($)">
                <SPAN CLASS="defaultbold">
                  <xsl:value-of select="../Company/FiscalPeriodDisplay"/> Core EPS ($)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../FinancialData/CoreEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $INVESTMENT_COMPANY">
          <TR VALIGN="TOP">
            <TD CLASS="surrleft table1_item datashade" NOWRAP="" WIDTH="60%" align="left">
              <SPAN class="defaultbold">Financial Data</SPAN>
            </TD>
            <TD CLASS="surrright table1_item datashade" WIDTH="40%" align="right">
              <SPAN class="defaultbold">
                <xsl:value-of select="../FinancialData/DateEnded"/>
              </SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EquityPerShareForCalcs', 1); return false" title="Book Value ($)">
                <SPAN CLASS="defaultbold">Book Value ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../FinancialData/BookValue,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'QuarterEPS', 1); return false" title="Quarter EPS ($)">
                <SPAN CLASS="defaultbold">Quarter EPS ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../FinancialData/QuarterEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSAfterExtraForCalcs', 0); return false" title="EPS ($)">
                <SPAN CLASS="defaultbold">
                  <xsl:value-of select="../Company/FiscalPeriodDisplay"/> EPS ($)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../FinancialData/EPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $ENERGY or $GAAPDomain = $GAS">
          <TR VALIGN="TOP">
            <TD CLASS="surrleft table1_item datashade" NOWRAP="" WIDTH="60%" align="left">
              <SPAN class="defaultbold">Financial Data</SPAN>
            </TD>
            <TD CLASS="surrright table1_item datashade" WIDTH="40%" align="right">
              <SPAN class="defaultbold">
                <xsl:value-of select="../FinancialData/DateEnded"/>
              </SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'NetIncomeGrowth', 1); return false" title="Net Income Growth (%)">
                <SPAN CLASS="defaultbold">Net Income Growth (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../FinancialData/NetIncomeGrowth,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSBasic', 1); return false" title="Basic EPS ($)">
                <SPAN CLASS="defaultbold">Basic EPS ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../FinancialData/EPSBasic,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'DebtToTotalCapBookValue', 0); return false" title="Debt / Book Capitalization (%)">
                <SPAN CLASS="defaultbold">Debt / Book Capitalization (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../FinancialData/DebtToTotalCap,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'OperatingIncomeUtilityRpGrowth', 0); return false" title="Reported Net Operating Income Growth (%)">
                <SPAN CLASS="defaultbold">Reported Net Operating Income Growth (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../FinancialData/OperatingIncomeUtilityRpGrowth,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'OperatingAssetsEnergyGrowth', 0); return false" title="Operating Asset Growth (%)">
                <SPAN CLASS="defaultbold">Operating Asset Growth (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../FinancialData/OperatingAssetsEnergyGrowth,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $FINTECH">
          <TR VALIGN="TOP">
            <TD CLASS="surrleft table1_item datashade" NOWRAP="" WIDTH="60%" align="left">
              <SPAN class="defaultbold">Financial Data</SPAN>
            </TD>
            <TD CLASS="surrright table1_item datashade" WIDTH="40%" align="right">
              <SPAN class="defaultbold">
                <xsl:value-of select="../FinancialData/DateEnded"/>
              </SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},988, 1); return false" title="Diluted EPS after Extra ($)">
                <SPAN CLASS="defaultbold">Diluted EPS after Extra ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../FinancialData/EPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},10180, 1); return false" title="LTM EPS after Extra ($)">
                <SPAN CLASS="defaultbold">LTM EPS after Extra ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../FinancialData/LTMEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},9827, 0); return false" title="Book Value ($)">
                <SPAN CLASS="defaultbold">Book Value ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../FinancialData/BookValue,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},13487, 0); return false" title="Tangible Book Value per Share ($)">
                <SPAN CLASS="defaultbold">Tangible Book Value per Share ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../FinancialData/TangibleBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $REIT">
          <TR VALIGN="TOP">
            <TD CLASS="surrleft table1_item datashade" NOWRAP="" WIDTH="60%" align="left">
              <SPAN class="defaultbold">Quarterly Financial Data</SPAN>
            </TD>
            <TD CLASS="surrright table1_item datashade" WIDTH="40%" align="right">
              <SPAN class="defaultbold">
                <xsl:value-of select="../FinancialData/DateEnded"/>
              </SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'FFOPerShareForCalcs', 1); return false" title="FFO / Share ($)">
                <SPAN CLASS="defaultbold">FFO / Share ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../FinancialData/FFOPerShare,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'LTMFFOPerShare', 1); return false" title="FFO / Share ($)">
                <SPAN CLASS="defaultbold">
                  <xsl:value-of select="../Company/FiscalPeriodDisplay"/> FFO / Share ($)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../FinancialData/LTMFFOPerShare,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'FADPerShare', 0); return false" title="FAD / Share ($)">
                <SPAN CLASS="defaultbold">FAD / Share ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../FinancialData/FADPerShare,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'FFOOperatingReported', 0); return false" title="Reported Operating FFO ($000)">
                <SPAN CLASS="defaultbold">Reported Operating FFO ($000)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="../FinancialData/FFOOperatingReported"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'FFOOperatingReportedShare', 0); return false" title="Reported Operating FFO / Share ($)">
                <SPAN CLASS="defaultbold">Reported Operating FFO / Share ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../FinancialData/FFOOperatingReportedShare,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'SameStoreRevenueChange', 0); return false" title="Same-store NOI Change (%)">
                <SPAN CLASS="defaultbold">Same-store NOI Change (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../FinancialData/SameStoreRevenueChange,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'DebtToTotalCapFinl', 0); return false" title="Debt / Total Cap (%)">
                <SPAN CLASS="defaultbold">Debt / Total Cap (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../FinancialData/DebtToTotalCap,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $INSURANCE_BROKER or $GAAPDomain = $INSURANCE_UNDERWRITER">
          <TR VALIGN="TOP">
            <TD CLASS="surrleft table1_item datashade" NOWRAP="" WIDTH="60%" align="left">
              <SPAN class="defaultbold">Financial Data</SPAN>
            </TD>
            <TD CLASS="surrright table1_item datashade" WIDTH="40%" align="right">
              <SPAN class="defaultbold">
                <xsl:value-of select="../FinancialData/DateEnded"/>
              </SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EquityPerShareForCalcs', 1); return false" title="Book Value ($)">
                <SPAN CLASS="defaultbold">Book Value ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../FinancialData/BookValue,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'TangibleEquityPerShare', 1); return false" title="Tangible Book ($)">
                <SPAN CLASS="defaultbold">Tangible Book ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../FinancialData/TangibleBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSAfterExtraForCalcs', 0); return false" title="Quarter EPS ($)">
                <SPAN CLASS="defaultbold">Quarter EPS ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../FinancialData/QuarterEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSAfterExtraForCalcs', 0); return false" title="EPS ($)">
                <SPAN CLASS="defaultbold">
                  <xsl:value-of select="../Company/FiscalPeriodDisplay"/> EPS ($)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../FinancialData/EPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'CoreEPS', 0); return false" title="Operating EPS ($)">
                <SPAN CLASS="defaultbold">
                  <xsl:value-of select="../Company/FiscalPeriodDisplay"/> Operating EPS ($)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../FinancialData/OperatingEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $HOMEBUILDER">
          <TR VALIGN="TOP">
            <TD CLASS="surrleft table1_item datashade" NOWRAP="" WIDTH="60%" align="left">
              <SPAN class="defaultbold">Financial Data</SPAN>
            </TD>
            <TD CLASS="surrright table1_item datashade" WIDTH="40%" align="right">
              <SPAN class="defaultbold">
                <xsl:value-of select="../FinancialData/DateEnded"/>
              </SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EquityPerShareForCalcs', 1); return false" title="Book Value ($)">
                <SPAN CLASS="defaultbold">Book Value ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../FinancialData/BookValue,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSAfterExtraForCalcs', 0); return false" title="Quarter EPS ($)">
                <SPAN CLASS="defaultbold">Quarter EPS ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../FinancialData/QuarterEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSAfterExtraForCalcs', 0); return false" title="EPS ($)">
                <SPAN CLASS="defaultbold">
                  <xsl:value-of select="../Company/FiscalPeriodDisplay"/> EPS ($)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../FinancialData/EPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'OperatingMarginBuilder', 0); return false" title="Operating Margin ($)">
                <SPAN CLASS="defaultbold">Operating Margin ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../FinancialData/OperatingMarginBuilder,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $MEDIA_ENT or $GAAPDomain = $COMMUNICATIONS or $GAAPDomain = $NEWMEDIA">
          <TR VALIGN="TOP">
            <TD CLASS="surrleft table1_item datashade defaultbold" NOWRAP="" WIDTH="60%" align="left">
              <SPAN class="defaultbold">Financial Data</SPAN>
            </TD>
            <TD CLASS="surrright table1_item datashade defaultbold" WIDTH="40%" valign="bottom" align="right">
              <xsl:value-of select="../FinancialData/DateEnded"/>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'TotalCapBookValue', 0); return false" title="Total Capitalization ($M)">
                <SPAN CLASS="defaultbold">Total Capitalization ($M)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="../FinancialData/BookValue"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSAfterExtraForCalcs', 0); return false" title="Quarter EPS ($)">
                <SPAN CLASS="defaultbold">Quarter EPS ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../FinancialData/QuarterEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSAfterExtraForCalcs', 0); return false" title="EPS ($)">
                <SPAN CLASS="defaultbold">
                  <xsl:value-of select="../Company/FiscalPeriodDisplay"/> EPS ($)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../FinancialData/EPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'NetIncomeMargin', 0); return false" title="Net Income Margin (%)">
                <SPAN CLASS="defaultbold">Net Income Margin (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../FinancialData/NetIncome,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:otherwise>
          <TR VALIGN="TOP">
            <TD CLASS="surrleft table1_item datashade" NOWRAP="" WIDTH="60%" align="left" title="Financial Data">
              <SPAN class="defaultbold">Financial Data</SPAN>
            </TD>
            <TD CLASS="surrright table1_item datashade" WIDTH="40%" align="right">
              <SPAN class="defaultbold">
                <xsl:value-of select="../FinancialData/DateEnded"/>
              </SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EquityPerShareForCalcs', 1); return false" title="Book Value ($)">
                <SPAN CLASS="defaultbold">Book Value ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../FinancialData/BookValue,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'TangibleEquityPerShare', 1); return false" title="Tangible Book ($)">
                <SPAN CLASS="defaultbold">Tangible Book ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../FinancialData/TangibleBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSAfterExtraForCalcs', 0); return false" title="Quarter EPS ($)">
                <SPAN CLASS="defaultbold">Quarter EPS ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../FinancialData/QuarterEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn},'EPSAfterExtraForCalcs', 0); return false" title="EPS ($)">
                <SPAN CLASS="defaultbold">
                  <xsl:value-of select="../Company/FiscalPeriodDisplay"/> EPS ($)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../FinancialData/EPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:otherwise>
      </xsl:choose>
    </TABLE>
  </xsl:template>

  <xsl:template name="Stock_Price_stock4">
    <TABLE CELLPADDING="3" CELLSPACING="0" CLASS="table2" WIDTH="100%" BORDER="0" ID="TABLE13">
      <TR>
        <TD CLASS="table1_item datashade" WIDTH="40%">
          <SPAN class="defaultbold">Stock Price History</SPAN>
        </TD>
        <TD CLASS="table1_item datashade" align="RIGHT" WIDTH="28%" nowrap="">
          <SPAN class="defaultbold">High ($)</SPAN>
        </TD>
        <TD CLASS="table1_item datashade" align="RIGHT" WIDTH="32%" nowrap="">
          <SPAN class="defaultbold">Low ($)</SPAN>
        </TD>
      </TR>
      <TR>
        <TD CLASS="data" NOWRAP="" WIDTH="40%">
          <SPAN CLASS="defaultbold">One Month</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="format-number(../StockPriceHistory/OneMonthHigh,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
        <TD class="data" WIDTH="32%" align="right">
          <xsl:value-of select="format-number(../StockPriceHistory/OneMonthLow,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
      </TR>
      <TR>
        <TD CLASS="data" NOWRAP="" WIDTH="40%">
          <SPAN CLASS="defaultbold">Three Months</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="format-number(../StockPriceHistory/ThreeMonthsHigh,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS=  "dataalign">&#160;</SPAN>
        </TD>
        <TD class="data" WIDTH="32%" align="right">
          <xsl:value-of select="format-number(../StockPriceHistory/ThreeMonthsLow,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
      </TR>
      <TR>
        <TD CLASS="data" NOWRAP="" WIDTH="40%">
          <SPAN CLASS="defaultbold">Year-to-Date</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="format-number(../StockPriceHistory/YTDHigh,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="32%" align="right">
          <xsl:value-of select="format-number(../StockPriceHistory/YTDLow,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
      </TR>
      <TR>
        <TD CLASS="data" NOWRAP="" WIDTH="40%">
          <SPAN CLASS="defaultbold">One Year</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="format-number(../StockPriceHistory/OneYearHigh,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="32%" align="right">
          <xsl:value-of select="format-number(../StockPriceHistory/OneYearLow,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
      </TR>
      <TR>
        <TD CLASS="data" NOWRAP="" WIDTH="40%">
          <SPAN CLASS="defaultbold">Three Year</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="format-number(../StockPriceHistory/ThreeYearsHigh,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="32%" align="right">
          <xsl:value-of select="format-number(../StockPriceHistory/ThreeYearsLow,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
      </TR>
      <TR>
        <TD CLASS="data" NOWRAP="" WIDTH="40%">
          <SPAN CLASS="defaultbold">Three Year Date</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="../StockPriceHistory/ThreeYearDateHigh"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="32%" align="right">
          <xsl:value-of select="../StockPriceHistory/ThreeYearDateLow"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
        </TD>
      </TR>
    </TABLE>
  </xsl:template>

  <xsl:template name="Price_Ratios_stock4">
    <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" CLASS="table2" WIDTH="100%" ID="TABLE14">

      <xsl:variable name="GAAPDomain" select="../Company/GAAP"/>
      <xsl:variable name="INSURANCE_BROKER" select="9"/>
      <xsl:variable name="INVESTMENT_COMPANY" select="7"/>
      <xsl:variable name="THRIFT" select="2"/>
      <xsl:variable name="INSURANCE" select="8"/>
      <xsl:variable name="REIT" select="6"/>
      <xsl:variable name="BANK" select="1"/>
      <xsl:variable name="ENERGY" select="10"/>
      <xsl:variable name="GAS" select="18"/>
      <xsl:variable name="FINTECH" select="15"/>
      <xsl:variable name="HOMEBUILDER" select="16"/>
      <xsl:variable name="COMMUNICATIONS" select="14"/>
      <xsl:variable name="MEDIA_ENT" select="20"/>
      <xsl:variable name="NEWMEDIA" select="21"/>
      <xsl:variable name="keyinstn" select="../Company/KeyInstn" />
      <xsl:variable name="keyspecialtaxstatus" select="../Company/KeySpecialTaxStatus" />
      <xsl:choose>
        <xsl:when test="$GAAPDomain = $BANK or $GAAPDomain = $THRIFT">
          <TR VALIGN="TOP">
            <TD CLASS="table1_item datashade" NOWRAP="" colspan="2">
              <SPAN class="defaultbold">Current Pricing Ratios</SPAN>
            </TD>
          </TR>
          <TR valign="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 610, 0); return false" title="Price / Book (%)">
                <SPAN CLASS="defaultbold">Price / Book (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../CurrentPricingRatios/PriceToBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1843, 0); return false" title="Price / Tangible Book (%)">
                <SPAN CLASS="defaultbold">Price / Tangible Book (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../CurrentPricingRatios/PriceToTangibleBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 609, 0); return false" title="Price / Earnings (x)">
                <SPAN CLASS="defaultbold">Price / Earnings (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../CurrentPricingRatios/PriceToEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1842, 0); return false" title="EPS (x)">
                <SPAN CLASS="defaultbold">
                  Price / <xsl:value-of select="../Company/FiscalPeriodDisplay"/> EPS (x)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../CurrentPricingRatios/PriceToEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9637, 0); return false" title="Core EPS (x)">
                <SPAN CLASS="defaultbold">
                  Price / <xsl:value-of select="../Company/FiscalPeriodDisplay"/> Core EPS (x)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../CurrentPricingRatios/PriceToCoreEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $HOMEBUILDER">
          <TR VALIGN="TOP">
            <TD CLASS="table1_item datashade" NOWRAP="" colspan="2">
              <SPAN class="defaultbold">Current Pricing Ratios</SPAN>
            </TD>
          </TR>
          <TR valign="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 610, 0); return false" title="Price / Book (%)">
                <SPAN CLASS="defaultbold">Price / Book (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../CurrentPricingRatios/PriceToBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9632, 0); return false" title="Price / Earnings (x)">
                <SPAN CLASS="defaultbold">Price / Earnings (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../CurrentPricingRatios/PriceToEarningsBeforeExtra,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9631, 0); return false" title="Price/ LTM EPS (x)">
                <SPAN CLASS="defaultbold">Price/ LTM EPS (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../CurrentPricingRatios/PriceToEarningsLTMBeforeExtra,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $INVESTMENT_COMPANY">
          <TR VALIGN="TOP">
            <TD CLASS="table1_item datashade" NOWRAP="" colspan="2">
              <SPAN class="defaultbold">Current Pricing Ratios</SPAN>
            </TD>
          </TR>
          <TR valign="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 610, 0); return false" title="Price / Book (%)">
                <SPAN CLASS="defaultbold">Price / Book (%) </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../CurrentPricingRatios/PriceToBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1843, 0); return false" title="Price / Earnings (x)">
                <SPAN CLASS="defaultbold">Price / Earnings (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../CurrentPricingRatios/PriceToEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 609, 0); return false" title="EPS (x)">
                <SPAN CLASS="defaultbold">
                  Price / <xsl:value-of select="../Company/FiscalPeriodDisplay"/> EPS (x)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../CurrentPricingRatios/PriceToEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1842, 0); return false" title="Price / EBITDA (x)">
                <SPAN CLASS="defaultbold">Price / EBITDA (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../CurrentPricingRatios/PriceToEBITDA,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $REIT">
          <TR VALIGN="TOP">
            <TD CLASS="table1_item datashade" NOWRAP="" colspan="2">
              <SPAN class="defaultbold">Current Pricing Ratios</SPAN>
            </TD>
          </TR>
          <xsl:choose>
            <xsl:when test="$keyspecialtaxstatus = '0'">
              <TR valign="TOP">
                <TD CLASS="data" NOWRAP="" width="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 610, 0); return false">
                    <SPAN CLASS="defaultbold">Price / FFO (x)</SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(../CurrentPricingRatios/PriceToFFO,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
              <TR VALIGN="TOP">
                <TD CLASS="data" NOWRAP="" width="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1843, 0); return false">
                    <SPAN CLASS="defaultbold">FFO Yield (%)</SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(../CurrentPricingRatios/FFOYield,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
              <TR VALIGN="TOP">
                <TD CLASS="data" NOWRAP="" width="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 609, 0); return false">
                    <SPAN CLASS="defaultbold">
                      Price / <xsl:value-of select="../Company/FiscalPeriodDisplay"/> FFO (x)
                    </SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(../CurrentPricingRatios/PriceToLTMFFO,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
              <TR VALIGN="TOP">
                <TD CLASS="data" NOWRAP="" width="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1842, 0); return false">
                    <SPAN CLASS="defaultbold">Price / EBITDA (x)</SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(../CurrentPricingRatios/PriceToEBITDA,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
              <TR VALIGN="TOP">
                <TD CLASS="data" NOWRAP="" width="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9637, 0); return false">
                    <SPAN CLASS="defaultbold">Price / Earnings (x)</SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(../CurrentPricingRatios/PriceToEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
              <TR VALIGN="TOP">
                <TD CLASS="data" NOWRAP="" width="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9637, 0); return false">
                    <SPAN CLASS="defaultbold">
                      Price / <xsl:value-of select="../Company/FiscalPeriodDisplay"/> Earnings (x)
                    </SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(../CurrentPricingRatios/PriceToLTMEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
            </xsl:when>
            <xsl:otherwise>
              <TR valign="TOP">
                <TD CLASS="data" NOWRAP="" width="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9633, 0); return false">
                    <SPAN CLASS="defaultbold">Price / FFO (x)</SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(../CurrentPricingRatios/PriceToFFO,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
              <TR VALIGN="TOP">
                <TD CLASS="data" NOWRAP="" width="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1843, 0); return false">
                    <SPAN CLASS="defaultbold">FFO Yield (%)</SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(../CurrentPricingRatios/FFOYield,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
              <TR VALIGN="TOP">
                <TD CLASS="data" NOWRAP="" width="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 609, 0); return false">
                    <SPAN CLASS="defaultbold">
                      Price / <xsl:value-of select="../Company/FiscalPeriodDisplay"/> FFO (x)
                    </SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(../CurrentPricingRatios/PriceToLTMFFO,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
              <TR VALIGN="TOP">
                <TD CLASS="data" NOWRAP="" width="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1842, 0); return false">
                    <SPAN CLASS="defaultbold">Price / EBITDA (x)</SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(../CurrentPricingRatios/PriceToEBITDA,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
              <TR VALIGN="TOP">
                <TD CLASS="data" NOWRAP="" width="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9637, 0); return false">
                    <SPAN CLASS="defaultbold">Price / Earnings (x)</SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(../CurrentPricingRatios/PriceToEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
              <TR VALIGN="TOP">
                <TD CLASS="data" NOWRAP="" width="60%">
                  <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9637, 0); return false">
                    <SPAN CLASS="defaultbold">
                      Price / <xsl:value-of select="../Company/FiscalPeriodDisplay"/> Earnings (x)
                    </SPAN>
                  </a>
                </TD>
                <TD CLASS="data" WIDTH="40%" align="right">
                  <xsl:value-of select="format-number(../CurrentPricingRatios/PriceToLTMEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
                  <SPAN CLASS="dataalign">&#160;</SPAN>
                </TD>
              </TR>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $INSURANCE_BROKER or $GAAPDomain = $INSURANCE">
          <TR VALIGN="TOP">
            <TD CLASS="table1_item datashade" NOWRAP="" colspan="2">
              <SPAN class="defaultbold">Current Pricing Ratios</SPAN>
            </TD>
          </TR>
          <TR valign="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 610, 0); return false">
                <SPAN CLASS="defaultbold">Price / Book (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../CurrentPricingRatios/PriceToBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1843, 0); return false" title="Price / Tangible Book (%)">
                <SPAN CLASS="defaultbold">Price / Tangible Book (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../CurrentPricingRatios/PriceToTangibleBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1843, 0); return false" title="Price / Book, excl. 115 (%)">
                <SPAN CLASS="defaultbold">Price / Book, excl. 115 (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../CurrentPricingRatios/PriceToBookExcluding115,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 609, 0); return false" title="Price / Earnings (x)">
                <SPAN CLASS="defaultbold">Price / Earnings (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../CurrentPricingRatios/PriceToEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1842, 0); return false" title="Price / EPS (x)">
                <SPAN CLASS="defaultbold">
                  Price / <xsl:value-of select="../Company/FiscalPeriodDisplay"/> EPS (x)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../CurrentPricingRatios/PriceToEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9637, 0); return false" title="Price /  Operating EPS (x)">
                <SPAN CLASS="defaultbold">
                  Price / <xsl:value-of select="../Company/FiscalPeriodDisplay"/> Operating EPS (x)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../CurrentPricingRatios/PriceToOperatingEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $ENERGY or $GAAPDomain = $GAS">
          <TR VALIGN="TOP">
            <TD CLASS="table1_item datashade" NOWRAP="" colspan="2">
              <SPAN class="defaultbold">Current Pricing Ratios</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1842, 0); return false" title="Price / EBITDA (x)">
                <SPAN CLASS="defaultbold">Price / EBITDA (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../CurrentPricingRatios/PriceToEBITDA,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9632, 0); return false" title="Price / Earnings (x)">
                <SPAN CLASS="defaultbold">Price / Earnings (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../CurrentPricingRatios/PriceToEarningsBeforeExtra,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR valign="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9631, 0); return false" title="Price / LTM Earnings (x)">
                <SPAN CLASS="defaultbold">Price / LTM Earnings (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../CurrentPricingRatios/PriceToLTMEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $FINTECH">
          <TR VALIGN="TOP">
            <TD CLASS="table1_item datashade" NOWRAP="" colspan="2">
              <SPAN class="defaultbold">Current Pricing Ratios</SPAN>
            </TD>
          </TR>
          <TR valign="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 610, 0); return false" title="Price / Book (%)">
                <SPAN CLASS="defaultbold">Price / Book (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../CurrentPricingRatios/PriceToBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1843, 0); return false" title="Price / Tangible Book (%)">
                <SPAN CLASS="defaultbold">Price / Tangible Book (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../CurrentPricingRatios/PriceToTangibleBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9632, 0); return false" title="Price / Earnings (x)">
                <SPAN CLASS="defaultbold">Price / Earnings (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../CurrentPricingRatios/PriceToEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR valign="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9631, 0); return false" title="Price / LTM Earnings (x)">
                <SPAN CLASS="defaultbold">Price / LTM Earnings (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../CurrentPricingRatios/PriceToLTMEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9639, 0); return false" title="Price / EBITDA (x)">
                <SPAN CLASS="defaultbold">Price / EBITDA (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../CurrentPricingRatios/PriceToEBITDA,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $MEDIA_ENT or $GAAPDomain = $COMMUNICATIONS or $GAAPDomain = $NEWMEDIA">
          <TR VALIGN="TOP">
            <TD CLASS="table1_item datashade" NOWRAP="" colspan="2">
              <SPAN class="defaultbold">Current Pricing</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 609, 0); return false" title="Price / Earnings (x)">
                <SPAN CLASS="defaultbold">Price / Earnings (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../CurrentPricingRatios/PriceToEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" WIDTH="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1842, 0); return false" title="Price / EPS (x)">
                <SPAN CLASS="defaultbold">
                  Price / <xsl:value-of select="../Company/FiscalPeriodDisplay"/> EPS (x)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../CurrentPricingRatios/PriceToEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:otherwise>
          <TR VALIGN="TOP">
            <TD CLASS="table1_item datashade" NOWRAP="" colspan="2">
              <SPAN class="defaultbold">Current Pricing Ratios</SPAN>
            </TD>
          </TR>
          <TR valign="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 610, 0); return false" title="Price / Book (%)">
                <SPAN CLASS="defaultbold">Price / Book (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../CurrentPricingRatios/PriceToBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1843, 0); return false" title="Price / Tangible Book (%)">
                <SPAN CLASS="defaultbold">Price / Tangible Book (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../CurrentPricingRatios/PriceToTangibleBook,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 609, 0); return false" title="Price / Earnings (x)">
                <SPAN CLASS="defaultbold">Price / Earnings (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../CurrentPricingRatios/PriceToEarnings,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" NOWRAP="" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 1842, 0); return false" title="Price / EPS (x)">
                <SPAN CLASS="defaultbold">
                  Price /<xsl:value-of select="../Company/FiscalPeriodDisplay"/> EPS (x)
                </SPAN>
              </a>
            </TD>
            <TD CLASS="data" WIDTH="40%" align="right">
              <xsl:value-of select="format-number(../CurrentPricingRatios/PriceToEPS,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:otherwise>
      </xsl:choose>
    </TABLE>
  </xsl:template>

  <xsl:template name="Price_Change_stock4">
    <xsl:variable name="mytradingsymbol" select="/irw:IRW/irw:StockInformation/TradedIssues/TradingSymbol[../KeyFndg = /irw:IRW/irw:StockInformation/Company/KeyFndg]"/>
    <TABLE CELLPADDING="3" CELLSPACING="0" CLASS="table2" WIDTH="100%" BORDER="0" ID="TABLE16">
      <TR>
        <TD CLASS="table1_item datashade" WIDTH="40%">
          <SPAN class="defaultbold">Price Change&#160;(%)</SPAN>
        </TD>
        <TD CLASS="table1_item datashade" align="right" WIDTH="28%">
          <SPAN class="defaultbold">
            <xsl:value-of select="$mytradingsymbol"/>
          </SPAN>&#160;&#160;&#160;&#160;
        </TD>
        <xsl:if test="../Company/ComparativeIndexName != ''">
          <TD CLASS="table1_item datashade" align="right" WIDTH="32%">
            <SPAN class="defaultbold">
              <xsl:value-of select="../Company/ComparativeIndexName"/> Index
            </SPAN>
          </TD>
        </xsl:if>
      </TR>
      <TR >
        <TD CLASS="data" NOWRAP="" align="LEFT" WIDTH="40%">
          <SPAN CLASS="defaultbold">One Day</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="format-number(../PriceChange/OneDay,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
          <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
        </TD>
        <xsl:if test="../Company/ComparativeIndexName != ''">
          <TD CLASS="data" WIDTH="28%" align="right">
            <xsl:value-of select="format-number(../PriceChange/OneDayIndex,'#,##0.00 ;(#,##0.00)','data')"/>
            <SPAN CLASS="dataalign">&#160;</SPAN>
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </TD>
        </xsl:if>
      </TR>
      <TR >
        <TD CLASS="data" NOWRAP="" align="LEFT" WIDTH="40%">
          <SPAN CLASS="defaultbold">One Month</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="format-number(../PriceChange/OneMonth,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
          <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
        </TD>
        <xsl:if test="../Company/ComparativeIndexName != ''">
          <TD CLASS="data" WIDTH="28%" align="right">
            <xsl:value-of select="format-number(../PriceChange/OneMonthIndex,'#,##0.00 ;(#,##0.00)','data')"/>
            <SPAN CLASS="dataalign">&#160;</SPAN>
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </TD>
        </xsl:if>
      </TR>
      <TR >
        <TD CLASS="data" NOWRAP="" align="LEFT" WIDTH="40%">
          <SPAN CLASS="defaultbold">Three Month</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="format-number(../PriceChange/ThreeMonth,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
          <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
        </TD>
        <xsl:if test="../Company/ComparativeIndexName != ''">
          <TD CLASS="data" WIDTH="28%" align="right">
            <xsl:value-of select="format-number(../PriceChange/ThreeMonthIndex,'#,##0.00 ;(#,##0.00)','data')"/>
            <SPAN CLASS="dataalign">&#160;</SPAN>
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </TD>
        </xsl:if>
      </TR>
      <TR >
        <TD CLASS="data" NOWRAP="" align="LEFT" WIDTH="40%">
          <SPAN CLASS="defaultbold">YTD</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="format-number(../PriceChange/YTD,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
          <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
        </TD>
        <xsl:if test="../Company/ComparativeIndexName != ''">
          <TD CLASS="data" WIDTH="28%" align="right">
            <xsl:value-of select="format-number(../PriceChange/YTDIndex,'#,##0.00 ;(#,##0.00)','data')"/>
            <SPAN CLASS="dataalign">&#160;</SPAN>
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </TD>
        </xsl:if>
      </TR>
      <TR >
        <TD CLASS="data" NOWRAP="" align="LEFT" WIDTH="40%">
          <SPAN CLASS="defaultbold">One Year</SPAN>
        </TD>
        <TD CLASS="data" WIDTH="28%" align="right">
          <xsl:value-of select="format-number(../PriceChange/OneYear,'#,##0.00 ;(#,##0.00)','data')"/>
          <SPAN CLASS="dataalign">&#160;</SPAN>
          <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
        </TD>
        <xsl:if test="../Company/ComparativeIndexName != ''">
          <TD CLASS="data" WIDTH="28%" align="right">
            <xsl:value-of select="format-number(../PriceChange/OneYearIndex,'#,##0.00 ;(#,##0.00)','data')"/>
            <SPAN CLASS="dataalign">&#160;</SPAN>
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </TD>
        </xsl:if>
      </TR>
    </TABLE>
  </xsl:template>

  <xsl:template name="Dividends_stock4">
    <xsl:variable name="divkey">
      <xsl:choose>
        <xsl:when test="../Dividends/IsDivAggregate = 'True'">46843</xsl:when>
        <xsl:otherwise>9640</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="divyieldkey">
      <xsl:choose>
        <xsl:when test="../Dividends/IsDivAggregate = 'True'">46844</xsl:when>
        <xsl:otherwise>2425</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" CLASS="table2" WIDTH="100%" ID="TABLE17">
      <xsl:variable name="GAAPDomain" select="../Company/GAAP"/>
      <xsl:variable name="REIT" select="6"/>
      <xsl:variable name="ENERGY" select="10"/>
      <xsl:variable name="GAS" select="18"/>
      <xsl:variable name="FINTECH" select="15"/>
      <xsl:variable name="keyinstn" select="../Company/KeyInstn" />
      <xsl:variable name="keyspecialtaxstatus" select="../Company/KeySpecialTaxStatus" />
      <xsl:choose>
        <xsl:when test="$GAAPDomain = $REIT">
          <TR VALIGN="TOP">
            <TD colspan="2" CLASS="table1_item datashade">
              <SPAN class="defaultbold">Dividends</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, {$divkey}, 0); return false" title="Quarterly Dividend ($)">
                <SPAN CLASS="defaultbold">Quarterly Dividend ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" width="40%" align="right">
              <xsl:value-of select="format-number(../Dividends/QuarterlyDividend,'#,##0.0000 ;(#,##0.0000)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, {$divyieldkey}, 0); return false" title="Dividend Yield (%)">
                <SPAN CLASS="defaultbold">Dividend Yield (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" width="40%" align="right">
              <xsl:value-of select="format-number(../Dividends/DividendYield,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <xsl:if test="../Company/IsCommonStock='True'">
            <TR VALIGN="TOP">
              <!--### Change decimal places to 4 as per "Quarterly Dividend" above ###-->
              <TD CLASS="data" width="60%">
                <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 'Dividend', 0); return false" title="Dividends Announced ($)">
                  <SPAN CLASS="defaultbold">
                    <xsl:value-of select="../Company/FiscalPeriodDisplay"/> Dividends Announced ($)
                  </SPAN>
                </a>
              </TD>
              <TD CLASS="data" width="40%" align="right">
                <xsl:value-of select="format-number(../Dividends/Dividend,'#,##0.0000 ;(#,##0.0000)','data')"/>
                <SPAN CLASS="dataalign">&#160;</SPAN>
              </TD>
            </TR>
            <TR VALIGN="TOP">
              <xsl:choose>
                <xsl:when test="$keyspecialtaxstatus = '0'">
                  <TD CLASS="data" width="60%">
                    <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 'DividendPayout', 0); return false" title="Payout Ratio (%)">
                      <SPAN CLASS="defaultbold">
                        <xsl:value-of select="../Company/FiscalPeriodDisplay"/> Payout Ratio (%)
                      </SPAN>
                    </a>
                  </TD>
                </xsl:when>
                <xsl:otherwise>
                  <TD CLASS="data" width="60%">
                    <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 'DividendPayoutFFO', 0); return false" title="Payout Ratio (%)">
                      <SPAN CLASS="defaultbold">
                        <xsl:value-of select="../Company/FiscalPeriodDisplay"/> Payout Ratio (%)
                      </SPAN>
                    </a>
                  </TD>
                </xsl:otherwise>
              </xsl:choose>
              <TD CLASS="data" width="40%" align="right">
                <xsl:value-of select="format-number(../Dividends/LTMPayoutRatio,'#,##0.00 ;(#,##0.00)','data')"/>
                <SPAN CLASS="dataalign">&#160;</SPAN>
              </TD>
            </TR>
          </xsl:if>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $ENERGY or $GAAPDomain = $GAS">
          <TR VALIGN="TOP">
            <TD colspan="2" CLASS="table1_item datashade">
              <SPAN class="defaultbold">Dividends</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 13498, 0); return false" title="Dividend Paid ($)">
                <SPAN CLASS="defaultbold">Dividend Paid ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" width="40%" align="right">
              <xsl:value-of select="format-number(../Dividends/DividendPaid,'#,##0.0000 ;(#,##0.0000)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 3249, 0); return false" title="Dividend Payout (%)">
                <SPAN CLASS="defaultbold">Dividend Payout (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" width="40%" align="right">
              <xsl:value-of select="format-number(../Dividends/DividendPayout,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 9935, 0); return false" title="Dividend / Book Per Share (x)">
                <SPAN CLASS="defaultbold">Dividend / Book Per Share (x)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" width="40%" align="right">
              <xsl:value-of select="format-number(../Dividends/DividendToBookPerShare,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="$GAAPDomain = $FINTECH">
          <TR VALIGN="TOP">
            <TD colspan="2" CLASS="table1_item datashade">
              <SPAN class="defaultbold">Dividends</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, {$divkey}, 0); return false" title="Quarterly Dividend ($)">
                <SPAN CLASS="defaultbold">Quarterly Dividend ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" width="40%" align="right">
              <xsl:value-of select="format-number(../Dividends/QuarterlyDividend,'#,##0.0000 ;(#,##0.0000)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, {$divyieldkey}, 0); return false" title="Dividend Yield (%)">
                <SPAN CLASS="defaultbold">Dividend Yield (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" width="40%" align="right">
              <xsl:value-of select="format-number(../Dividends/DividendYield,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <!--### Change decimal places to 4 as per "Quarterly Dividend" above ###-->
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 20350, 0); return false" title="LTM Dividends Announced ($)">
                <SPAN CLASS="defaultbold">LTM Dividends Announced ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" width="40%" align="right">
              <xsl:value-of select="format-number(../Dividends/DivAnnouncedLTM,'#,##0.0000 ;(#,##0.0000)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 3249, 0); return false" title="Dividend Payout Ratio (%)">
                <SPAN CLASS="defaultbold">Dividend Payout Ratio (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" width="40%" align="right">
              <xsl:value-of select="format-number(../Dividends/LTMPayoutRatio,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
        </xsl:when>
        <xsl:otherwise>
          <TR VALIGN="TOP">
            <TD colspan="2" CLASS="table1_item datashade">
              <SPAN class="defaultbold">Dividends</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, {$divkey}, 0); return false" title="Quarterly Dividend ($)">
                <SPAN CLASS="defaultbold">Quarterly Dividend ($)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" width="40%" align="right">
              <xsl:value-of select="format-number(../Dividends/QuarterlyDividend,'#,##0.0000 ;(#,##0.0000)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <TR VALIGN="TOP">
            <TD CLASS="data" width="60%">
              <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, {$divyieldkey}, 0); return false" title="Dividend Yield (%)">
                <SPAN CLASS="defaultbold">Dividend Yield (%)</SPAN>
              </a>
            </TD>
            <TD CLASS="data" width="40%" align="right">
              <xsl:value-of select="format-number(../Dividends/DividendYield,'#,##0.00 ;(#,##0.00)','data')"/>
              <SPAN CLASS="dataalign">&#160;</SPAN>
            </TD>
          </TR>
          <xsl:if test="../Company/IsCommonStock='True'">
            <TR VALIGN="TOP">
              <!--### Change decimal places to 4 as per "Quarterly Dividend" above ###-->
              <TD CLASS="data" width="60%">
                <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 'Dividend', 0); return false" title="Dividend ($)">
                  <SPAN CLASS="defaultbold">
                    <xsl:value-of select="../Company/FiscalPeriodDisplay"/> Dividend ($)
                  </SPAN>
                </a>
              </TD>
              <TD CLASS="data" width="40%" align="right">
                <xsl:value-of select="format-number(../Dividends/Dividend,'#,##0.0000 ;(#,##0.0000)','data')"/>
                <SPAN CLASS="dataalign">&#160;</SPAN>
              </TD>
            </TR>
            <xsl:if test="../Company/GAAP != 0">
            <TR VALIGN="TOP">
              <TD CLASS="data" width="60%">
                <a href="#" class="boldfielddef" onclick="DefWindow({$keyinstn}, 'DividendPayout', 0); return false" title="Payout Ratio (%)">
                  <SPAN CLASS="defaultbold">
                    <xsl:value-of select="../Company/FiscalPeriodDisplay"/> Payout Ratio (%)
                  </SPAN>
                </a>
              </TD>
              <TD CLASS="data" width="40%" align="right">
                <xsl:value-of select="format-number(../Dividends/PayoutRatio,'#,##0.00 ;(#,##0.00)','data')"/>
                <SPAN CLASS="dataalign">&#160;</SPAN>
              </TD>
            </TR>
            </xsl:if>
          </xsl:if>
        </xsl:otherwise>
      </xsl:choose>
    </TABLE>
  </xsl:template>


  <!-- Template FOUR Ends here. -->

  <xsl:template name="Script_template"> 
    <xsl:if test="Company/TemplateName != 'AltStock4'">
      <SCRIPT language="javascript" type="text/javascript" src="javascript/jquery-1.3.1.min.js"></SCRIPT>
    </xsl:if>
    <xsl:if test="not(contains(translate(Company/URL,$ucletters,$lcletters), 'print=1'))">
    <SCRIPT type="text/javascript">
      <![CDATA[            
        (function( $ ){ 
          $(function() {        
            //SetIframeSrc
            var KeyInstn = ]]><xsl:value-of select="Company/KeyInstn"/><![CDATA[;
            var KeyFndg = ]]><xsl:value-of select="Company/KeyFndg"/><![CDATA[;
            var Preview = "]]><xsl:value-of select="Company/IsPreview"/><![CDATA[" == "true";
            $("#myStockframe").attr("src","StockChartIFrame.aspx?KeyInstn=" + KeyInstn +"&KeyFndg=" + KeyFndg +"&Preview=" + Preview +"&Width=" + $("#graphContainer").width());
          });
        })( jQuery );
      ]]>
      </SCRIPT>
    </xsl:if>
    <xsl:if test="Company/TemplateName != 'AltStock4'">
      <SCRIPT language="javascript" type="text/javascript" src="javascript/jqueryIFrame.js"></SCRIPT>
    </xsl:if>
    <link rel="stylesheet" href="javascript/calendar/ui.datepicker.css" type="text/css"/>

    <style>
      ul#stock_tabs { list-style: none; margin: 0 !important; padding: 0;}
      ul#stock_tabs li { float: left; margin: 0 !important; padding: 0; overflow: hidden; position: relative; }
      ul#stock_tabs li.stock_tab, ul#stock_tabs li.tabSelected {text-decoration: none; display: block; outline: none; /*-moz-border-radius: 5px 5px 0 0; -webkit-border-radius: 5px 5px 0 0;*/ margin-right: 2px !important; padding: 5px 2px; text-align: center; cursor: pointer; font-size: 12px; font-weight: bold; }
      ul#stock_tabs li.stock_tabSelected { } ul#stock_tabs li.stock_tabSelected:hover {} ul#stock_tabs li.stock_tab:hover { background-color:#efefef; }
      #stock_TabData {  min-height: 105px; overflow: hidden; padding: 10px; /*-moz-border-radius: 0 5px 5px 5px; -webkit-border-radius: 0 5px 5px 5px;*/ clear:left;}
      .EventImage {display: none;}
      .panel { margin-bottom:10px; }
    </style>      
    <xsl:if test="not(contains(translate(Company/URL,$ucletters,$lcletters), 'print=1'))">       
    <SCRIPT language="javascript" type="text/javascript" src="javascript/irjson.js"></SCRIPT>
    <SCRIPT language="javascript" type="text/javascript" src="javascript/jquery.autoheight.js"></SCRIPT>
    <SCRIPT language="javascript" type="text/javascript" src="javascript/ui.datepicker.js"></SCRIPT>
    <SCRIPT language="javascript" type="text/javascript" src="javascript/IRGraph.js"></SCRIPT>

    <!-- Financial Data Tables Tab code  -->
    <SCRIPT language="javascript" type="text/javascript" src="javascript/jTabPanel.js"></SCRIPT>
    <xsl:variable name="TableOptions" select="count(TableOptions)"/>
    <SCRIPT language="javascript" type="text/javascript">
      <![CDATA[
		jQuery(function() {
      jQuery("#stock_TabData .panel").hide();
      jQuery("#simpleContent1").show();
			var simpleTabPanel = new TabPanel({
				tabPrefix: 'simpleTab',
				panelPrefix: 'simpleContent',
				tabCount: ]]><xsl:value-of select="$TableOptions" /><![CDATA[,
				onSelectedIndexChanged: function(lastSelectedIndex, selectedIndex) {
					if (lastSelectedIndex != 0) {
						jQuery('#simpleTab' + lastSelectedIndex.toString()).attr("class", "stock_tab table1");
					}
					jQuery('#simpleTab' + selectedIndex.toString()).attr("class", "tabSelected header table1");
				}
			});
			simpleTabPanel.initialize();
		});
	  ]]>
    </SCRIPT>
    <!-- End -->

    <SCRIPT language="javascript" type="text/javascript">
      <![CDATA[
      $(document).ready(function() {
          $("span.LeftNavfly").click(function() {
			      $(this).next().slideToggle(100);
			      $(this).toggleClass("menu-open");
		      });
		      $("div.ir_chartOption_Dropdowns").mouseup(function() {
			      return false
		      });
		      $(document).mouseup(function(e) {
			      if($(e.target).parent("span.LeftNavfly").length==0) {
				      $("span.LeftNavfly").removeClass("menu-open");
				      $("div.ir_chartOption_Dropdowns").hide();
			      }
		      });

        var keyInstn = ]]><xsl:value-of select="Company/KeyInstn"/><![CDATA[;
        var keyFndg = ]]><xsl:value-of select="Company/KeyFndg"/><![CDATA[;
        var preview = "]]><xsl:value-of select="Company/IsPreview"/><![CDATA[" == "true";

        //CSS wasn't working well in its own file, so we'll apply it via jQuery
        $("#TrackLine").css({
          "background-color": "Red",
          "position": "absolute",
          "display": "none",
          "width": "1px",
          "z-index": 10000
        });

        $("#ToolTip").css({
            "background-color": "#ffffe0",
            "color": "#000",
            "border": "solid 1px #222",
            "padding": "10px",
            "display": "none",
            "position": "absolute",
            "z-index": 10001
        });

        $("img.EventImage").css({
            "position": "absolute",
            "display": "none"
        });

        var updateButton = {
          "selector": "#submitChart",
          "event": "click",
          "peers": "#PeerSelect input:checked",
          "indexes": "#IndexSelect input:checked",
          "showDivYield": function(){return $("#DivYield:checked").size() > 0;},
          "showTotalReturn": function(){return $("#TotalReturn:checked").size() > 0;},
          "startDate": "#StartDate",
          "endDate": "#EndDate2",
          "graphType": "#TypeSelect"
        };

        var clearSelectionsButton = {
          "selector": "#clearSelections",
          "event": "click",
          "peers": "#PeerSelect input:checked",
          "indexes": "#IndexSelect input:checked",
          "showDivYield": function(){return $("#DivYield:checked").size() > 0;},
          "showTotalReturn": function(){return $("#TotalReturn:checked").size() > 0;},
          "startDate": "#StartDate",
          "endDate": "#EndDate2",
          "graphType": "#TypeSelect"
        };

        var updateSelect = {
          "selector": "#PeriodSelect",
          "event": "change",
          "peers": "#PeerSelect input:checked",
          "indexes": "#IndexSelect input:checked",
          "showDivYield": function(){return $("#DivYield:checked").size() > 0;},
          "showTotalReturn": function(){return $("#TotalReturn:checked").size() > 0;},
          "graphType": "#TypeSelect"
        };

        var eventSelect = {
          "control": "#EventSelector input",
          "event": "click",
          "selected": "#EventSelector input:checked"
        };

        $("#graph").stockGraph({
          "keyInstn" : keyInstn,
          "keyFndg" : keyFndg,
          "tracklineControl" : "#TrackLine",
          "tooltipControl" : "#ToolTip",
          "refreshControl": [updateButton,clearSelectionsButton, updateSelect],
          "startDate": "#PeriodSelect select",
          "endDate": "#EndDate1",
          "onerror": function(){ $("#LoadAnim").hide(); $("#graph").hide(); $("#titleContainer").hide(); $("#errorMsg").show(); },
          "onsuccess": function(){$("#errorMsg").hide(); $("#graph").show(); $("#titleContainer").show();},
          "preview": preview,
          "events": eventSelect,
          "loadAnim": "#LoadAnim"
        });

        var updateButton2 = {
          "selector": "#ChartDownload",
          "event": "click",
          "peers": "#PeerSelect input:checked",
          "indexes": "#IndexSelect input:checked",
          "showDivYield": function(){return $("#DivYield:checked").size() > 0;},
          "showTotalReturn": function(){return $("#TotalReturn:checked").size() > 0;},
          "startDate": ["#StartDate", "#PeriodSelect select"],
          "endDate": ["#EndDate2", "#EndDate1"],
          "graphType": "#TypeSelect"
        };

        $("#graph2").stockGraph({
          "keyInstn" : keyInstn,
          "keyFndg" : keyFndg,
          "refreshControl": updateButton2,
          "onsuccess": function(){downloadChart($("#graph2").attr("src"));},
          "preview": preview,
          "size": "#downloadSize"
        });

        $("#StartDate").datepicker({
		      dateFormat: 'mm/dd/yy',
		      buttonImage: 'javascript/calendar/calendar.gif',
		      buttonImageOnly: true,
		      showOn: 'both',
		      closeAtTop: false,
		      showStatus: true
	      });
        $("#EndDate2").datepicker({
		      dateFormat: 'mm/dd/yy',
		      buttonImage: 'javascript/calendar/calendar.gif',
		      buttonImageOnly: true,
		      showOn: 'both',
		      closeAtTop: false,
		      showStatus: true
	      });

        //SetIframeSrc
        //$("#myStockframe").attr("src","StockChartIFrame.aspx?KeyInstn=" + keyInstn +"&KeyFndg=" + keyFndg +"&Preview=" + preview +"&Width=" + $("#graphContainer").width());


        $("#closeprice").datepicker({
		      dateFormat: 'mm/dd/yy',
		      buttonImage: 'javascript/calendar/calendar.gif',
		      buttonImageOnly: true,
		      showOn: 'both',
		      closeAtTop: false,
		      showStatus: true
	      });
    });

    function downloadChart(imageLink){
      window.open(imageLink, "chartDownload");
      //window.open("", "StockChart");
      /*var appendText = "<html><head></head><body>";
      appendText += "To save the image right click and select 'Save Image As'<br/>";
      if(imageLink) appendText += "<img id='graph' src='" + imageLink + "'>";
      else appendText += $("#errorMsg").text();
      appendText += "</body></html>";
      win.document.write(appendText);*/
      //$(win.document).html(appendText);
    }
    ]]>
    </SCRIPT>
    </xsl:if>
    <SCRIPT language="javascript" type="text/javascript">
      <![CDATA[
		function DefWindow(KeyInstn, ItemName, EOP) {
			var page = "definitions.aspx?IID=" + KeyInstn + "&Item=" + ItemName + "&EOP=" + EOP;
			var winprops = "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=300,height=130";
			popup = window.open(page, "Definitions", winprops);
		}
		]]>
    </SCRIPT>

    <!-- Add date value input text box afert "ENTER" Key then Popup related windows. RC-->
    <SCRIPT language="javascript" type="text/javascript">
      <![CDATA[
		function KeydownStock(KeyInstn,KeyFndg, event){
			if ((event.which && event.which == 13) || (event.keyCode && event.keyCode == 13)) {
				window.open('quotepop.aspx?IID='+KeyInstn+'&KeyFndg='+KeyFndg+'&date='+ document.getElementById('snlstock').value, 'ClosingPrice', 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=60');
				//return false;
			}
			else
			return true;
		}
	  ]]>
    </SCRIPT>
    <!-- iframe auto resize -->
    <SCRIPT language="javascript" type="text/javascript">
      <![CDATA[
	  function calcHeight()
	  {
	    //find the height of the internal page
	    var the_height= document.getElementById('theIFrame').contentWindow.document.body.scrollHeight;

	    //change the height of the iframe
	    document.getElementById('theIFrame').height = the_height;
	  }
	  ]]>
    </SCRIPT>

    <div id="TrackLine"></div>
    <div id="ToolTip"></div>
    <IMG id="graph2" style="display:none" alt="Graph" />
    <IMG id="Divs" class="EventImage" src="Images/graphing/dividends2.gif" alt="Dividends" />
    <IMG id="Docs" class="EventImage" src="Images/graphing/documents.gif" alt="Documents"/>
    <IMG id="Splits" class="EventImage" src="Images/graphing/splits.gif" alt="Splits"/>
    <IMG id="PRs" class="EventImage" src="Images/graphing/pressreleases.gif" alt="pressreleases" />
    <IMG id="Filings" class="EventImage" src="Images/graphing/filings.gif" alt="Filings" />
    <IMG id="All" class="EventImage" src="Images/graphing/multipleevents.gif" alt="Multiple events" />
    
    <SCRIPT language="javascript" type="text/javascript" src="chart.js"></SCRIPT>
    <style type="text/css">
      acronym { border-bottom:none; }
    </style>
  </xsl:template>

  <xsl:template match="irw:StockInformation">
    <xsl:variable name="TemplateName" select="Company/TemplateName"/>
    <xsl:choose>
      <!-- Template 1 -->
      <xsl:when test="$TemplateName = 'AltStock1'">
        <xsl:call-template name="Script_template"/>
        <xsl:call-template name="TemplateONEstylesheet" />
        <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" CLASS="">
          <xsl:call-template name="Title_S2"/>
          <TR align="">
            <TD CLASS="leftTOPbord" colspan="2" valign="top">
              <TABLE BORDER="0" WIDTH="100%" CELLPADDING="3" cellspacing="0" class="data">
                <xsl:call-template name="Descriptor_stock2"/>
                <xsl:variable name="keyinstn" select="Company/KeyInstn"/>
                <xsl:variable name="formaction" select="concat('stockinfo.aspx?IID=', $keyinstn)"></xsl:variable>
                <form method="POST" id="stockform2" name="stockform2">
                  <xsl:attribute name="action">
                    <xsl:value-of select="$formaction"/>
                  </xsl:attribute>
                  <xsl:call-template name="Selector_Stock2"/>
                </form>
                <TR>
                  <TD valign="top" CLASS="default" >
                    <TABLE class="surr datashade" cellpadding="0" cellspacing="0" width="100%">
                      <TR>
                        <TD valign="top" CLASS="default">
                          <xsl:call-template name="Quote_Table_stock2"/>
                        </TD>
                      </TR>
                      <TR align="left">
                        <TD valign="top" class="default">
                          <xsl:call-template name="Chart_Drop_stock2"/>
                          <xsl:call-template name="ChartOptions2"/>
                        </TD>
                      </TR>
                    </TABLE>
                  </TD>
                </TR>
                <TR>
                  <TD valign="TOP" CLASS="default">
                    <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="2" width="100%" ID="TABLE2">
                      <TR align="left">
                        <TD CLASS="default" valign="top">
                          <IMG SRC="/images/Interactive/blank.gif" WIDTH="100%" HEIGHT="4" alt=""/>
                        </TD>
                      </TR>
                      <TR align="left">
                        <TD valign="top" CLASS="default">
                          <SPAN class="defaultbold">For the definition of a financial field, select the field name link.</SPAN>
                        </TD>
                      </TR>
                      <xsl:call-template name="FinancialDataTables2"/>
                    </TABLE>
                  </TD>
                </TR>
              </TABLE>
            </TD>
          </TR>
          <xsl:if test="Company/Footer != ''">
            <TR>
              <TD CLASS="default" colspan="2" valign="top">
                <SPAN class="default">
                  <xsl:value-of select="Company/Footer" disable-output-escaping="yes"/>
                </SPAN>
              </TD>
            </TR>
          </xsl:if>
        </TABLE>
        <xsl:call-template name="Copyright"/>
      </xsl:when>
      <!-- End Template 1 -->

      <!-- Template 3 -->
      <xsl:when test="$TemplateName = 'AltStock3'">
        <xsl:call-template name="Script_template"/>
        <xsl:call-template name="TemplateTHREEstylesheet" />
        <xsl:call-template name="Title_T3"/>

        <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" CLASS="table2">
          <TR align="">
            <TD CLASS="data" valign="top">
              <TABLE BORDER="0" WIDTH="100%" CELLPADDING="3" cellspacing="0" class="data">
                <xsl:call-template name="Descriptor_stock2"/>
                <xsl:variable name="keyinstn" select="Company/KeyInstn"/>
                <xsl:variable name="formaction" select="concat('stockinfo.aspx?IID=', $keyinstn)"></xsl:variable>
                <form method="POST" id="Stockform2" name="Stockform2">
                  <xsl:attribute name="action">
                    <xsl:value-of select="$formaction"/>
                  </xsl:attribute>
                  <xsl:call-template name="Selector_Stock3"/>
                </form>
                <TR>
                  <TD valign="top" CLASS="default" >
                    <TABLE class="" cellpadding="0" cellspacing="0" width="100%">
                      <TR>
                        <TD valign="top" CLASS="default">
                          <xsl:call-template name="Quote_Table_stock3"/>
                        </TD>
                      </TR>
                      <TR align="left">
                        <TD valign="top" class="default bottom_cap">
                          <xsl:call-template name="Chart_Drop_stock3"/>
                          <xsl:call-template name="ChartOptions3"/>
                        </TD>
                      </TR>
                    </TABLE>
                  </TD>
                </TR>
                <TR>
                  <TD valign="TOP" CLASS="default">
                    <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="2" width="100%" ID="TABLE2">
                      <TR align="left">
                        <TD CLASS="default" valign="top">
                          <IMG SRC="/images/Interactive/blank.gif" WIDTH="100%" HEIGHT="4" alt=""/>
                        </TD>
                      </TR>
                      <TR align="left">
                        <TD valign="top" CLASS="default">
                          <SPAN class="defaultbold">For the definition of a financial field, select the field name link.</SPAN>
                        </TD>
                      </TR>
                      <xsl:call-template name="FinancialDataTables3"/>
                    </TABLE>
                  </TD>
                </TR>
              </TABLE>
            </TD>
          </TR>

          <xsl:if test="Company/Footer != ''">
            <TR>
              <TD CLASS="default" valign="top">
                <SPAN class="default">
                  <xsl:value-of select="Company/Footer" disable-output-escaping="yes"/>
                </SPAN>
              </TD>
            </TR>
          </xsl:if>
        </TABLE>
        <xsl:call-template name="Copyright"/>
      </xsl:when>
      <!-- End Template Three -->

      <!-- Template 4 -->
      <xsl:when test="$TemplateName = 'AltStock4'">
        <xsl:call-template name="TemplateTHREEstylesheet" />
        <xsl:call-template name="StockInformation_4" />
      </xsl:when>
      <!-- End Template 4 -->
      
      <!-- Template 5 -->
      <xsl:when test="$TemplateName = 'AltStock5'">
        <xsl:call-template name="Script_template"/>
        <xsl:call-template name="TemplateTHREEstylesheet" />
        <xsl:call-template name="Title_T3"/>

        <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" CLASS="table2">
          <TR align="">
            <TD CLASS="data" valign="top">
              <TABLE BORDER="0" WIDTH="100%" CELLPADDING="3" cellspacing="0" class="data">
                <xsl:call-template name="Descriptor_stock4"/>
                <TR>
                  <TD valign="top" CLASS="default" >
                    <TABLE class="" cellpadding="0" cellspacing="0" width="100%">
                      <TR>
                        <TD valign="top" CLASS="default">
                          <xsl:call-template name="Quote_Table_stock4"/>
                        </TD>
                      </TR>
                      <TR align="left">
                        <TD valign="top" class="default">
                          <xsl:call-template name="Chart_Drop_Image_stock4"/>
                        </TD>
                      </TR>
                    </TABLE>
                  </TD>
                </TR>
                <TR>
                  <TD valign="TOP" CLASS="default">
                    <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="2" width="100%" ID="TABLE2">
                      <TR align="left">
                        <TD CLASS="default" valign="top">
                          <IMG SRC="/images/Interactive/blank.gif" WIDTH="100%" HEIGHT="4" alt=""/>
                        </TD>
                      </TR>
                      <TR align="left">
                        <TD valign="top" CLASS="default">
                          <xsl:call-template name="FinancialDataTables4"/>
                        </TD>
                      </TR>
                    </TABLE>
                  </TD>
                </TR>
              </TABLE>
            </TD>
          </TR>

          <xsl:if test="Company/Footer != ''">
            <TR>
              <TD CLASS="default" valign="top">
                <SPAN class="default">
                  <xsl:value-of select="Company/Footer" disable-output-escaping="yes"/>
                </SPAN>
              </TD>
            </TR>
          </xsl:if>
        </TABLE>
        <xsl:call-template name="Copyright"/>
      </xsl:when>
      <!-- End Template Four -->

      <!-- Template Default -->
      <xsl:otherwise>
        <xsl:call-template name="Script_template"/>
        <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" CLASS="colordark">
          <xsl:call-template name="Title_S1"/>
          <TR align="">
            <TD CLASS="colordark" colspan="2" valign="top">
              <TABLE BORDER="0" WIDTH="100%" CELLPADDING="3" cellspacing="0" class="colorlight">
                <TR>
                  <xsl:variable name="keyinstn" select="Company/KeyInstn"/>
                  <xsl:variable name="formaction" select="concat('stockinfo.aspx?IID=', $keyinstn)"></xsl:variable>
                  <TD CLASS="default" valign="TOP">
                    <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="2" width="100%" ID="TABLE2">
                      <form method="POST" id="Stockform2" name="Stockform2">
                        <xsl:attribute name="action">
                          <xsl:value-of select="$formaction"/>
                        </xsl:attribute>
                        <TR>
                          <TD valign="top" class="colorlight" colspan="3">
                            <xsl:call-template name="Selector_stock"/>
                          </TD>
                        </TR>
                      </form>
                      <xsl:call-template name="Descriptor_stock"/>

                      <xsl:if test="Company/ShowEmailNotificationLink = 'true'">
                        <xsl:for-each select="CLIENTEMAILPREFS[NotificationType = '1' and NotificationEnabled = 'True']">
                          <TR>
                            <TD CLASS="default" nowrap="1" colspan="3">
                              <a>
                                <xsl:attribute name="href">
                                  email.aspx?IID=<xsl:value-of select="../Company/KeyInstn"/>
                                </xsl:attribute>Notify me with an end-of-day stock quote.
                              </a>
                            </TD>
                          </TR>
                        </xsl:for-each>
                      </xsl:if>
                      <TR>
                        <TD valign="top">
                          <xsl:call-template name="Quote_Table_stock"/>
                        </TD>
                        <TD CLASS="colorlight">
                          <IMG SRC="/images/Interactive/blank.gif" WIDTH="2" HEIGHT="8" alt="" />
                        </TD>
                        <TD valign="TOP">
                          <xsl:call-template name="Date_table_stock"/>
                        </TD>
                      </TR>
                      <TR>
                        <TD>
                          <IMG SRC="/images/Interactive/blank.gif" WIDTH="8" HEIGHT="4" alt=""/>
                        </TD>
                        <TD>
                          <IMG SRC="/images/Interactive/blank.gif" WIDTH="2" HEIGHT="8" alt=""/>
                        </TD>
                        <TD>
                          <IMG SRC="/images/Interactive/blank.gif" WIDTH="8" HEIGHT="4" alt=""/>
                        </TD>
                      </TR>
                      <xsl:call-template name="Chart_Drop_stock"/>
                      <TR>
                        <TD valign="top" class="colorlight" colspan="3">
                          <SPAN class="default">For the definition of a financial field, select the field name link.</SPAN>
                        </TD>
                      </TR>
                      <xsl:call-template name="FinancialDataTables"/>
                    </TABLE>
                  </TD>
                </TR>
              </TABLE>
            </TD>
          </TR>
          <xsl:if test="Company/Footer != ''">
            <TR class="colorlight" align="left">
              <TD colspan="2">
                <SPAN class="default">
                  <xsl:value-of select="Company/Footer" disable-output-escaping="yes"/>
                </SPAN>
              </TD>
            </TR>
          </xsl:if>
        </TABLE>
        <xsl:call-template name="Copyright"/>
      </xsl:otherwise>

    </xsl:choose>

  </xsl:template>

<!-- StockInformation Template Style 4 -->  
  <xsl:template name="StockScriptResource_4">
    <xsl:variable name="KeyInstn" select="Company/KeyInstn"/>	
    <xsl:call-template name="CommonScript_4" /> 
    <xsl:if test="not(contains(translate(Company/URL,$ucletters,$lcletters), 'print=1'))">
      <SCRIPT language="javascript" type="text/javascript" src="{$ScriptPath}stock_temp1.js"></SCRIPT>
      <xsl:if test="$devPath != ''">
        <SCRIPT language="javascript" type="text/javascript" src="/Interactive/LookAndFeel/{$KeyInstn}/{$devPath}stock_temp1.js"></SCRIPT>	
      </xsl:if>
      <!--<SCRIPT language="javascript" type="text/javascript">
        <![CDATA[
          var KeyInstn ="]]><xsl:value-of select="$KeyInstn"/><![CDATA[";
          var devPath ="]]><xsl:value-of select="$devPath"/><![CDATA[";
          var num = "";
          st1=new String(window.location);
          i1=st1.indexOf('&style=');		
          for(j=i1 + 7;j<=i1 + 7;j++) { i3 = st1.charAt(j); num = num + i3; }		
          if(num == '/' || num == ''){ num = ''; }
          if(num == '1'){
            document.write('<link href="/Interactive/LookAndFeel/'+KeyInstn+'/'+devPath+'stockinfo_c.css" rel="stylesheet" type="text/css" />');
            document.write('<script language="javascript" type="text/javascript" src="/Interactive/LookAndFeel/'+KeyInstn+'/'+devPath+'stockinfo_c.js"><\/script>');
          } else if(num == '0') {
            document.write('<link href="/Interactive/LookAndFeel/'+KeyInstn+'/'+devPath+'stockinfo_ir.css" rel="stylesheet" type="text/css" />');
            document.write('<script language="javascript" type="text/javascript" src="/Interactive/LookAndFeel/'+KeyInstn+'/'+devPath+'stockinfo_ir.js"><\/script>');
          }
        ]]>
      </SCRIPT>-->
      </xsl:if>
  </xsl:template>  
  
  <xsl:template name="DateTime_4">
    <div class="date_time">
    <xsl:choose>
      <xsl:when test="StockQuote/Time = '12:00 AM'" >
        <xsl:value-of select="StockQuote/QuoteTimeStamp"/>&#160;Close
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="StockQuote/QuoteTimeStamp"/>&#160;ET
      </xsl:otherwise>
    </xsl:choose> 
    </div>
  </xsl:template>
  <xsl:template name="DescriptorStock_4">
    <div id="stock_top_text">
      <div class="stock_top_text">
        <div class="l_section">
          <xsl:if test="TradedIssues/RowCount > 1">
            <a href="#">
              <xsl:attribute name="OnClick">window.open('fndgdesc.aspx?IID=<xsl:value-of select="Company/KeyInstn"/>', 'SecurityDescriptions', 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=400,height=300');</xsl:attribute>Select link to view descriptions of our traded funding issues.</a><br />
          </xsl:if>
          <xsl:if test="Company/ShowEmailNotificationLink = 'true'">
            <xsl:for-each select="CLIENTEMAILPREFS[NotificationType = '1' and NotificationEnabled = 'True']">
              <a><xsl:attribute name="href">email.aspx?IID=<xsl:value-of select="../Company/KeyInstn"/></xsl:attribute>Notify me with an end-of-day stock quote.</a><br />
            </xsl:for-each>
          </xsl:if>
        </div>
        <div class="r_section"><xsl:call-template name="StockPriceLookup_4" /></div>
      </div>
    </div>
  </xsl:template>
  
  <xsl:template name="StockPriceLookup_4">
    <div id="stc_pricelst">
      <div class="stc_pricelst">			
        <div class="ChartOptions">
          <SPAN class="LeftNavfly">Stock Price Lookup</SPAN>
          <div class="closeprice">
            Enter a date below to request the <br/>closing price for that specific day:
            <div class="enterprice">
				<label class="visuallyhidden" for="closeprice">closeprice</label>
              <input size="10" id="closeprice" type="text" class="input"/>&#160;<input type="button" value="Enter" id="Button1"  class="submit" name="submit1">
                <xsl:attribute name="OnClick">window.open('quotepop.aspx?IID=<xsl:value-of select="Company/KeyInstn"/>&amp;KeyFndg=<xsl:value-of select="Company/KeyFndg"/>&amp;date=' + document.getElementById('closeprice').value, 'ClosingPrice', 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=60')</xsl:attribute>
              </input>
            </div>
          </div>
        </div>
      </div>
    </div>
  </xsl:template>

  <xsl:template name="QuoteTableStock_4">
    <xsl:variable name="keyinstn" select="Company/KeyInstn"/>
    <div id="msum_top_horz">
    	<div class="msum_top_horz">
        <ul>
          <li>
            <SPAN class="bold">Last (&#36;)</SPAN>
            <SPAN>
              <xsl:variable name="CurrentPrice" select="StockQuote/CurrentPrice"/>
              <xsl:choose>
                <xsl:when test="$CurrentPrice &gt; 0"><xsl:value-of select="format-number($CurrentPrice, '###,##0.00')"/></xsl:when>
                <xsl:otherwise>NA</xsl:otherwise>
              </xsl:choose>
            </SPAN>
          </li>
          <li>
            <SPAN class="bold">Change (&#36;)</SPAN>
            <SPAN>
              <xsl:variable name="NetChangeFromPreviousClose" select="StockQuote/NetChangeFromPreviousClose"/>
              <xsl:choose>
                <xsl:when test="$NetChangeFromPreviousClose != 0">
                  <xsl:choose>
                    <xsl:when test="$NetChangeFromPreviousClose &gt; 0"><xsl:value-of select="format-number($NetChangeFromPreviousClose,'###,##0.00','data')"/>&#160;</xsl:when>
                    <xsl:otherwise><xsl:value-of select="$NetChangeFromPreviousClose"/>&#160;</xsl:otherwise>
                  </xsl:choose>
                </xsl:when>
                <xsl:otherwise>NA</xsl:otherwise>
              </xsl:choose>
            </SPAN>
          </li>
          <li>
            <SPAN class="bold">Change (&#37;)</SPAN>
            <SPAN>
			      <xsl:if test="StockQuote/NetChangeDirection = 'positive'"><xsl:attribute name="class">up_arrw_txt</xsl:attribute></xsl:if>
			      <xsl:if test="StockQuote/NetChangeDirection = 'negative'"><xsl:attribute name="class">down_arrw_txt</xsl:attribute></xsl:if>
                    <xsl:variable name="PercentChangeFromPreviousClose" select="StockQuote/PercentChangeFromPreviousClose"/>
                    <xsl:choose>                      
                      <xsl:when test="$PercentChangeFromPreviousClose != 0">
                         <xsl:choose>
                           <xsl:when test="$PercentChangeFromPreviousClose &gt; 0">
                             <xsl:if test="StockQuote/NetChangeDirection = 'negative'">(</xsl:if><xsl:value-of select="format-number($PercentChangeFromPreviousClose, '###,##0.00')"/> <xsl:if test="StockQuote/NetChangeDirection = 'negative'">)</xsl:if>
                           </xsl:when>
                           <xsl:otherwise><xsl:value-of select="format-number($PercentChangeFromPreviousClose, '###,##0.00')"/></xsl:otherwise>
                         </xsl:choose>
                        <IMG src="javascript/Template4/images/common/spacer.gif"  alt="">
                          <xsl:if test="StockQuote/NetChangeDirection = 'positive'"><xsl:attribute name="class">up_arrw</xsl:attribute></xsl:if>
                          <xsl:if test="StockQuote/NetChangeDirection = 'negative'"><xsl:attribute name="class">down_arrw</xsl:attribute></xsl:if></img>				  
                </xsl:when>
                <xsl:otherwise>NA</xsl:otherwise>
              </xsl:choose>
            </SPAN>
          </li>
          <li>
            <SPAN class="bold">Volume</SPAN>
            <SPAN><xsl:value-of select="StockQuote/VolumeToday"/></SPAN>
          </li>
          <li>
            <SPAN class="bold">Today's high (&#36;)</SPAN>
            <SPAN>
              <xsl:choose>
                <xsl:when test="StockQuote/HighToday &gt; 0"><xsl:value-of select="format-number(StockQuote/HighToday, '###,##0.00')"/></xsl:when>
                <xsl:otherwise>NA</xsl:otherwise>
              </xsl:choose>
            </SPAN>
          </li>
          <li>
            <SPAN class="bold">Today's Low (&#36;)</SPAN>
            <SPAN>
              <xsl:choose>
                <xsl:when test="StockQuote/LowToday &gt; 0"><xsl:value-of select="format-number(StockQuote/LowToday, '###,##0.00')"/></xsl:when>
                <xsl:otherwise>NA</xsl:otherwise>
              </xsl:choose>
            </SPAN>
          </li>
          <li>
            <SPAN class="bold">Open (&#36;)</SPAN>
            <SPAN>
              <xsl:choose>
                <xsl:when test="StockQuote/FndgPriceOpen &gt; 0"><xsl:value-of select="format-number(StockQuote/FndgPriceOpen, '###,##0.00')"/></xsl:when>
                <xsl:otherwise>NA</xsl:otherwise>
              </xsl:choose>
            </SPAN>
          </li>
          <!--<li>
            <span class="bold">Close</span>
            <span>
              <xsl:choose>
                <xsl:when test="StockQuote/LastPrice &gt; 0">&#36;<xsl:value-of select="format-number(StockQuote/LastPrice, '###,##0.00')"/></xsl:when>
                <xsl:otherwise>NA</xsl:otherwise>
              </xsl:choose>
            </span>
          </li>-->
        </ul>
      </div>
    </div>
  </xsl:template>
  
  <xsl:template name="FinancialDataTables_4">
    <xsl:variable name="colspan" select="count(TableOptions)"/>
    <xsl:if test="$colspan &gt; 0">
      <div id="FinancialData">
        <div style="padding-bottom: 5px; height: 15px;" class="defaultbold">
          <SPAN id="showdata" class="defaultbold" style="display: none;">For the definition of a financial field, select the field name link.</SPAN>
        </div>
        <xsl:if test="not(contains(translate(Company/URL,$ucletters,$lcletters), 'print=1'))">
        <ul id="stock_tabs">
          <xsl:for-each select="TableOptions">
            <li class="stock_tab table1">
              <xsl:choose>
                <xsl:when test="ID = 1 or ID = 3 or ID = 5"><xsl:attribute name="onclick">document.getElementById('showdata').style.display = 'block'</xsl:attribute></xsl:when>
                <xsl:otherwise><xsl:attribute name="onclick">document.getElementById('showdata').style.display = 'none'</xsl:attribute>
              </xsl:otherwise>
              </xsl:choose>
              <xsl:attribute name="id">simpleTab<xsl:value-of select="position()" disable-output-escaping="yes"/></xsl:attribute>
                <xsl:value-of select="Name" disable-output-escaping="yes"/>
            </li>
          </xsl:for-each>
        </ul>
        </xsl:if>
        <div id="stock_TabData" class="table2">
          <xsl:variable name="mytradingsymbol" select="/irw:IRW/irw:StockInformation/TradedIssues/TradingSymbol[../KeyFndg = /irw:IRW/irw:StockInformation/Company/KeyFndg]"/>
          <xsl:if test="Company/IsCommonStock='True'">
            <xsl:for-each select="TableOptions">
              <xsl:if test="ID = 0">
                <div class="panel">
                  <xsl:attribute name="id">simpleContent<xsl:value-of select="position()" disable-output-escaping="yes"/></xsl:attribute>
                  <xsl:attribute name="colspan"><xsl:value-of select="$colspan" disable-output-escaping="yes"/></xsl:attribute>
                    <xsl:call-template name="Vol_Highlights_stock4"/>
                </div>
              </xsl:if>
              <xsl:if test="ID = 1">
                <xsl:if test="not(../Company/GAAP=0)">
                  <div class="panel">
                    <xsl:attribute name="id">simpleContent<xsl:value-of select="position()" disable-output-escaping="yes"/></xsl:attribute>
                    <xsl:attribute name="colspan"><xsl:value-of select="$colspan" disable-output-escaping="yes"/></xsl:attribute>
                    <xsl:call-template name="Fin_Data_stock4"/>
                  </div>
                </xsl:if>
              </xsl:if>
              <xsl:if test="ID = 2">
                <div class="panel">
                  <xsl:attribute name="id">simpleContent<xsl:value-of select="position()" disable-output-escaping="yes"/></xsl:attribute>
                  <xsl:attribute name="colspan"><xsl:value-of select="$colspan" disable-output-escaping="yes"/></xsl:attribute>
                    <xsl:call-template name="Stock_Price_stock4"/>
                </div>
              </xsl:if>
              <xsl:if test="ID = 3">
                <xsl:if test="not(../Company/GAAP=0)">
                  <div class="panel">
                    <xsl:attribute name="id">simpleContent<xsl:value-of select="position()" disable-output-escaping="yes"/></xsl:attribute>
                    <xsl:attribute name="colspan"><xsl:value-of select="$colspan" disable-output-escaping="yes"/></xsl:attribute>
                      <xsl:call-template name="Price_Ratios_stock4"/>
                  </div>
                </xsl:if>
              </xsl:if>
              
              <xsl:if test="ID = 4">
                <div class="panel">
                  <xsl:attribute name="id">simpleContent<xsl:value-of select="position()" disable-output-escaping="yes"/></xsl:attribute>
                  <xsl:attribute name="colspan"><xsl:value-of select="$colspan" disable-output-escaping="yes"/></xsl:attribute>
                    <xsl:call-template name="Price_Change_stock4"/>
                </div>
              </xsl:if>
              <xsl:if test="ID = 5">
                <div class="panel">
                  <xsl:attribute name="id">simpleContent<xsl:value-of select="position()" disable-output-escaping="yes"/></xsl:attribute>
                  <xsl:attribute name="colspan"><xsl:value-of select="$colspan" disable-output-escaping="yes"/></xsl:attribute>
                    <xsl:call-template name="Dividends_stock4"/>
                </div>
              </xsl:if>
            </xsl:for-each>
          </xsl:if>
          
          <xsl:if test="Company/IsCommonStock='False'">
            <xsl:for-each select="TableOptions">
              <xsl:if test="ID = 0">
                <div class="panel">
                  <xsl:attribute name="id">simpleContent<xsl:value-of select="position()" disable-output-escaping="yes"/></xsl:attribute>
                  <xsl:attribute name="colspan"><xsl:value-of select="$colspan" disable-output-escaping="yes"/></xsl:attribute>
                    <xsl:call-template name="Vol_Highlights_stock4"/>
                </div>
              </xsl:if>              
              <xsl:if test="ID = 2">
                <div class="panel">
                  <xsl:attribute name="id">simpleContent<xsl:value-of select="position()" disable-output-escaping="yes"/></xsl:attribute>
                  <xsl:attribute name="colspan"><xsl:value-of select="$colspan" disable-output-escaping="yes"/></xsl:attribute>
                    <xsl:call-template name="Stock_Price_stock4"/>
                </div>
              </xsl:if>
              <xsl:if test="ID = 3"></xsl:if>              
              <xsl:if test="ID = 4">
                <div class="panel">
                  <xsl:attribute name="id">simpleContent<xsl:value-of select="position()" disable-output-escaping="yes"/></xsl:attribute>
                  <xsl:attribute name="colspan"><xsl:value-of select="$colspan" disable-output-escaping="yes"/></xsl:attribute>
                  <TABLE CELLPADDING="3" CELLSPACING="0" CLASS="table2" WIDTH="100%" BORDER="0" ID="TABLE16">
                    <TR VALIGN="bottom">
                      <TD CLASS="table1_item datashade defaultbold" WIDTH="40%">Price Change&#160;(%)</TD>
                      <TD CLASS="table1_item datashade defaultbold" align="right"><xsl:value-of select="$mytradingsymbol"/></TD>
                    </TR>
                    <TR>
                      <TD CLASS="data" NOWRAP="" align="LEFT" WIDTH="40%"><SPAN CLASS="defaultbold">One Day</SPAN></TD>
                      <TD CLASS="data" WIDTH="28%" align="right">
                        <xsl:value-of select="format-number(../PriceChange/OneDay,'#,##0.00 ;(#,##0.00)','data')"/><SPAN CLASS="dataalign">&#160;</SPAN>
                      </TD>
                    </TR>
                    <TR>
                      <TD CLASS="data" NOWRAP="" align="LEFT" WIDTH="40%"><SPAN CLASS="defaultbold">One Month</SPAN></TD>
                      <TD CLASS="data" WIDTH="28%" align="right">
                        <xsl:value-of select="format-number(../PriceChange/OneMonth,'#,##0.00 ;(#,##0.00)','data')"/><SPAN CLASS="dataalign">&#160;</SPAN>
                      </TD>
                    </TR>
                    <TR>
                      <TD CLASS="data" NOWRAP="" align="LEFT" WIDTH="40%"><SPAN CLASS="defaultbold">Three Month</SPAN></TD>
                      <TD CLASS="data" WIDTH="28%" align="right">
                        <xsl:value-of select="format-number(../PriceChange/ThreeMonth,'#,##0.00 ;(#,##0.00)','data')"/><SPAN CLASS="dataalign">&#160;</SPAN>
                      </TD>
                    </TR>
                    <TR>
                      <TD CLASS="data" NOWRAP="" align="LEFT" WIDTH="40%"><SPAN CLASS="defaultbold">YTD</SPAN></TD>
                      <TD CLASS="data" WIDTH="28%" align="right">
                        <xsl:value-of select="format-number(../PriceChange/YTD,'#,##0.00 ;(#,##0.00)','data')"/><SPAN CLASS="dataalign">&#160;</SPAN>
                      </TD>
                    </TR>
                    <TR>
                      <TD CLASS="data" NOWRAP="" align="LEFT" WIDTH="40%"><SPAN CLASS="defaultbold">One Year</SPAN></TD>
                      <TD CLASS="data" WIDTH="28%" align="right">
                        <xsl:value-of select="format-number(../PriceChange/OneYear,'#,##0.00 ;(#,##0.00)','data')"/><SPAN CLASS="dataalign">&#160;</SPAN>
                      </TD>
                    </TR>
                  </TABLE>
                </div>
              </xsl:if>
              
              <xsl:if test="ID = 5">               
                <div class="panel">
                  <xsl:attribute name="id">simpleContent<xsl:value-of select="position()" disable-output-escaping="yes"/></xsl:attribute>
                  <xsl:attribute name="colspan"><xsl:value-of select="$colspan" disable-output-escaping="yes"/></xsl:attribute>
                    <xsl:call-template name="Dividends_stock4"/>
                </div>
              </xsl:if>
            </xsl:for-each>
          </xsl:if>
        </div>
      </div>
    </xsl:if>
  </xsl:template>  

  <xsl:template name="ChartDropImageStock_4">
	<div id="StockChartDropImage">
		<xsl:call-template name="Chart_Drop_Image_stock4"/>
	</div>
  </xsl:template>
  
  <xsl:template name="StockInformation_4">
    <xsl:variable name="TemplateName" select="Company/TemplateName"/>
    <xsl:call-template name="StockScriptResource_4"/> 
    <xsl:call-template name="Script_template"/>   
    <div id="outer">
      <div id="Stock_Info">
        <xsl:call-template name="Toolkit_Section_4"/>        
		    <div id="StockInfo">			
			    <xsl:call-template name="Title_Head_4"/>
          <xsl:call-template name="Header_4"/>
			    <xsl:call-template name="DescriptorStock_4"/>
			    <xsl:call-template name="DateTime_4"/>			
			    <xsl:call-template name="QuoteTableStock_4"/>
			    <xsl:call-template name="ChartDropImageStock_4"/>
			    <xsl:call-template name="FinancialDataTables_4"/>
          <xsl:call-template name="Footer_4"/>
          <xsl:call-template name="Copyright_4"/>
		    </div>        
      </div>		
    </div>    
  </xsl:template>
  <!-- End StockInformation Template Style 4 -->
  
</xsl:stylesheet>
