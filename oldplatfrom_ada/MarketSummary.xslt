<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.ord/TR/REC-html40"
	xmlns:irw="http://www.snl.com/xml/irw">

<xsl:output method="html" media-type="text/html" indent="no"/>

<xsl:template name="Market_Alacarte">	
<TABLE width="100%" border="0" cellspacing="0" cellpadding="2">
	<TR>
		<TD class="small" nowrap="" valign="bottom"><span class="defaultbold">Trading Symbol</span></TD>
		<TD class="small" align="right" valign="bottom">
		<xsl:if test="MarketSummary/NetChangeFromPreviousClose != 0">
			<xsl:attribute name="colspan">2</xsl:attribute>
		</xsl:if><span class="default"><xsl:value-of select="Company/Ticker"/></span></TD>
		<TD>&#160;</TD>
	</TR>
	<TR class="small">
		<TD class="small" nowrap=""><span class="defaultbold">Exchange</span></TD>
		<TD class="small" align="right">
		<xsl:if test="MarketSummary/NetChangeFromPreviousClose != 0">
			<xsl:attribute name="colspan">2</xsl:attribute>
		</xsl:if><span class="default"><xsl:value-of select="Company/Exchange"/></span></TD>
		<TD>&#160;</TD>
	</TR>
	<TR class="small">
		<TD nowrap=""><span class="defaultbold">Market Value ($M)</span></TD>
		<TD nowrap="" align="right">
		<xsl:if test="MarketSummary/NetChangeFromPreviousClose != 0">
			<xsl:attribute name="colspan">2</xsl:attribute>
		</xsl:if><span class="default"><nobr>
      <xsl:variable name="MarketValueAtCurrentPrice" select="format-number(MarketSummary/MarketValueAtCurrentPrice, '###,##0.00')" />
      <xsl:choose>
        <xsl:when test="$MarketValueAtCurrentPrice = 'NaN'"><xsl:value-of select="MarketSummary/MarketValueAtCurrentPrice"/></xsl:when>
        <xsl:otherwise><xsl:value-of select="format-number(MarketSummary/MarketValueAtCurrentPrice, '###,##0.00')"/></xsl:otherwise>
      </xsl:choose></nobr></span></TD>
		<TD>&#160;</TD>
	</TR>
	<TR class="small">
		<TD class="small" nowrap=""><span class="defaultbold">Stock Quote</span></TD>
		<TD class="small" nowrap="" align="right">
		<xsl:if test="MarketSummary/NetChangeFromPreviousClose != 0">
			<xsl:attribute name="colspan">2</xsl:attribute>
		</xsl:if><span class="default"><nobr>$&#160;<xsl:value-of select="format-number(MarketSummary/CurrentPrice, '###,##0.00')"/></nobr></span></TD>
		<TD>&#160;</TD>
	</TR>
	<TR class="small">
		<TD class="small" nowrap=""><span class="defaultbold">Volume</span></TD>
		<TD class="small" nowrap="" align="right">
		<xsl:if test="MarketSummary/NetChangeFromPreviousClose != 0">
			<xsl:attribute name="colspan">2</xsl:attribute>
		</xsl:if>
		<span class="default"><nobr>&#160;<xsl:value-of select="MarketSummary/VolumeToday"/></nobr></span></TD>
		<TD>&#160;</TD>
	</TR>
	<TR class="small">
		<TD class="small" nowrap="" valign="top">
			<span class="defaultbold">Change</span>
		</TD>
		<xsl:choose>
		<xsl:when test="MarketSummary/NetChangeFromPreviousClose != 0">
			<TD class="small" nowrap="" valign="top" align="right" >
				<xsl:if test="MarketSummary/NetChangeDirection = 'positive'">
				<span class="default" align="right">
					<IMG SRC="/images/Interactive/IR/uptriangle.gif" WIDTH="11" HEIGHT="10" BORDER="0" ALT="Up" />
				</span>
				</xsl:if>
				<xsl:if test="MarketSummary/NetChangeDirection = 'negative'">
					<span class="default"><IMG SRC="/images/Interactive/IR/downtriangle.gif" WIDTH="11" HEIGHT="10" BORDER="0" ALT="Down" /></span>
				</xsl:if>
			</TD>
			<TD class="small" nowrap="" align="right">
				<span class="default">
          &#36;&#160;<xsl:value-of select="MarketSummary/NetChangeFromPreviousClose"/></span>
			</TD>
			<TD>&#160;</TD>
		</xsl:when>
		<xsl:otherwise>
			<TD class="small" nowrap="" valign="top" align="right">NA</TD>
      <TD>&#160;</TD>
		</xsl:otherwise>
		</xsl:choose>
	</TR>
	<TR align="center">
  <xsl:variable name="PercentChangeFromPreviousClose" select="MarketSummary/PercentChangeFromPreviousClose"/>
	 <xsl:choose>
    <xsl:when test="$PercentChangeFromPreviousClose != 0">    
			<TD>&#160;</TD>
			<TD class="small" align="right" valign="top">
		  <xsl:if test="MarketSummary/NetChangeFromPreviousClose != 0">
			  <xsl:attribute name="colspan">2</xsl:attribute>
		  </xsl:if>
			  <span class="default">
          <xsl:choose>
            <xsl:when test="$PercentChangeFromPreviousClose &gt; 0">
              <xsl:if test="MarketSummary/NetChangeDirection = 'negative'">(</xsl:if><xsl:value-of select="format-number($PercentChangeFromPreviousClose, '###,##0.00')"/><xsl:if test="MarketSummary/NetChangeDirection = 'negative'">)</xsl:if>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="format-number($PercentChangeFromPreviousClose, '###,##0.00')"/>
            </xsl:otherwise>
          </xsl:choose>
			  </span>
			</TD>
			<TD class="small" valign="top"><span class="default" >&#37;</span></TD>
    </xsl:when>
    <xsl:otherwise>  
			<TD>&#160;</TD>
			<TD class="small" nowrap="" valign="top" align="right">NA</TD>
      <TD>&#160;</TD>
    </xsl:otherwise>
  </xsl:choose>        
	</TR>
	<TR align="center">
		<TD colspan="3" class="biggysmall" nowrap="">
		<xsl:if test="MarketSummary/NetChangeFromPreviousClose != 0">
			<xsl:attribute name="colspan">4</xsl:attribute>
		</xsl:if>As of <xsl:value-of select="MarketSummary/QuoteDateTimeStamp"/><BR />Minimum 20 minute delay.</TD>
	</TR>
</TABLE>
</xsl:template>

<xsl:template match="MarketSummary1">
<HTML>
	<head>
		<title><xsl:value-of select="Company/Ticker" />&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</title>
		<xsl:call-template name="Template2stylesheet" />
		<style type="text/css">
		  body { margin: 0px; }
		  #MarketAlacarte {
			margin: 0 auto;
		  }
		</style>
	
	</head>
<body>
  <div id="MarketAlacarte">
	  <table cellpadding="0" cellspacing="3" border="0" width="175px" align="center">
		  <tr>
			  <td valign="top">
				  <xsl:call-template name="Market_Alacarte"/>
			  </td>
		  </tr>
	  </table>
  </div>
</body>
</HTML>
</xsl:template>

</xsl:stylesheet>
