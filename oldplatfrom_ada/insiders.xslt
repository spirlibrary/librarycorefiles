<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.ord/TR/REC-html40"
	xmlns:irw="http://www.snl.com/xml/irw">

<xsl:output method="html" media-type="text/html" indent="no"/>

<!-- Start Default Template here-->

<!--
//This is the default template. Most changes are to either remove the company name, ticker, or exchange. you can also change the name of the page or remove this row all together. 
//These templates below are broken up how the page is displayed.
 
-->

  <xsl:template name="Insiders_Title">
    <xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>

    <tr>
      <td CLASS="colordark" NOWRAP="NOWRAP">
        <SPAN CLASS="title1light">
          <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          <xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/>
        </SPAN>
      </td>
      <td CLASS="colordark" NOWRAP="NOWRAP" align="right" valign="top">
        <span class="subtitlelight">
          <xsl:value-of select="$CompanyNameCaps"/> (<xsl:value-of select="Company/Exchange"/> - <xsl:value-of select="Company/Ticker"/>)
        </span>
        <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
      </td>
    </tr>
    <tr class="default" align="left">
      <td class="colorlight" colspan="2">
        <span class="default">
          <xsl:value-of select="Company/Header" disable-output-escaping="yes"/>
        </span>
      </td>
    </tr>
  </xsl:template>
  <xsl:template name="Insiders_header">
    <tr>

      <td COLSPAN="4" CLASS="colorlight">
        <span class="default">If you wish to be notified of new Regulatory Filings and Reports posted to this site, <a title="click here">
            <xsl:attribute name="href">email.aspx?IID=<xsl:value-of select="Company/KeyInstn"/>
            </xsl:attribute>click here</a>.
        </span>
      </td>


    </tr>

  </xsl:template>





  <xsl:template name="Insiders_footer">
	<tr class="default" align="left">
		<td class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span></td>
	</tr>

</xsl:template>

<xsl:template name="Insiders_data">
	<tr>
		<td WIDTH="40%" CLASS="header">Filing Description</td>
		<td WIDTH="20%" CLASS="header" NOWRAP="1">Filing Type</td>
		<td ALIGN="RIGHT" CLASS="header" WIDTH="20%" NOWRAP="1">Filing Date</td>
		<td ALIGN="RIGHT" CLASS="header" WIDTH="20%" NOWRAP="1">Event Date</td>
	</tr>

	<xsl:if test="count(OtherFiling) = 0">
	<tr VALIGN="TOP">
		<td align="center" colspan="4" CLASS="data">
			<br/>No insiders filings available.<br/>
		</td>
	</tr>
	</xsl:if>

	<xsl:for-each select="OtherFiling">
								
		<tr VALIGN="TOP">
			<td WIDTH="40%" CLASS="data">
				<xsl:if test="position() mod 2 = 0">
					<xsl:attribute name="CLASS">datashade</xsl:attribute>
				</xsl:if>
				<a title="{FilingDescription}">
				<xsl:attribute name="href">
					<xsl:value-of select="DocLink"/>
				</xsl:attribute>
				<xsl:value-of select="FilingDescription"/></a>
			</td>
			<td WIDTH="20%" CLASS="data">
				<xsl:if test="position() mod 2 = 0">
					<xsl:attribute name="CLASS">datashade</xsl:attribute>
				</xsl:if>
				<A CLASS="fielddef" title="{FilingType}">
				<xsl:attribute name="href">
					<xsl:value-of select="FilingTypeLink"/>
				</xsl:attribute>
				<xsl:value-of select="FilingType"/></A>
			</td>
			<td ALIGN="RIGHT" WIDTH="20%" CLASS="data">
				<xsl:if test="position() mod 2 = 0">
					<xsl:attribute name="CLASS">datashade</xsl:attribute>
				</xsl:if>
				<xsl:value-of select="FilingDate"/>
			</td>
			<td ALIGN="RIGHT" WIDTH="20%" CLASS="data">
				<xsl:if test="position() mod 2 = 0">
					<xsl:attribute name="CLASS">datashade</xsl:attribute>
				</xsl:if>			
				<xsl:value-of select="EventDate"/>
			</td>
		</tr>
		<xsl:if test="Abstract != ''">
		<tr VALIGN="TOP" class="data">
			<td COLSPAN="4" CLASS="data">
				<xsl:if test="position() mod 2 = 0">
					<xsl:attribute name="CLASS">datashade</xsl:attribute>
				</xsl:if>			
				Abstract: <xsl:value-of select="Abstract"/>
			</td>
		</tr>			
		</xsl:if>
	</xsl:for-each>
		
		<xsl:if test="count(OtherFiling) != 0">
		<xsl:variable name="EndPage" select="Company/Start + Company/RowsPerPage - 1"/>

		<tr VALIGN="TOP" class="data">
			<td COLSPAN="2" class="defaultbold">Rows <xsl:value-of select="Company/Start"/> through 
      			<xsl:choose>
					<xsl:when test="$EndPage &gt; Company/Total">
						<xsl:value-of select="Company/Total"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$EndPage"/>
					</xsl:otherwise>
				</xsl:choose>
				of <xsl:value-of select="Company/Total"/>
			</td>
			<td COLSPAN="2" ALIGN="RIGHT">
				<xsl:for-each select="Pagination">
					<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
      				<xsl:choose>
						<xsl:when test="CurrentPage = 'false'">
							<a target="_self" title="{PageLabel}">
								<xsl:attribute name="href">
									<xsl:value-of select="PageLink"/>
								</xsl:attribute>
								<xsl:value-of select="PageLabel"/>
							</a>
						</xsl:when>
						<xsl:otherwise>
								<strong><xsl:value-of select="PageLabel"/></strong>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</td>
		</tr>	
		</xsl:if>	
</xsl:template>

<xsl:template name="Insiders_header_a">
	<tr>
	
			<td COLSPAN="4" CLASS="colorlight"><span class="title2">Forms 3, 4 &amp; 5</span></td>


	</tr>

</xsl:template>
<!-- Default Template Ends here. all templates below are ONLY if you are using the 'Style Template' section in the console. -->























<xsl:template name="Insiders_title_2">
	<tr>
		<td COLSPAN="4" CLASS="colorlight"><span class="title2">Forms 3, 4 &amp; 5</span></td>
	</tr>
	<xsl:if test="Company/ShowEmailNotificationLink = 'true'">
		<xsl:if test="CLIENTEMAILPREFS/NotificationType = '2' or CLIENTEMAILPREFS/NotificationType = '30'">
				<TR>
					<TD COLSPAN="4" CLASS="colorlight" nowrap="1">
            <span class="default">If you wish to be notified of new Regulatory Filings and Reports posted to this site, <a title="click here">
                <xsl:attribute name="href">email.aspx?IID=<xsl:value-of select="Company/KeyInstn"/></xsl:attribute>click here</a>.
            </span>
					</TD>
				</TR>
			</xsl:if>
	</xsl:if>

</xsl:template>


<xsl:template name="Insiders_data2">
	<tr>
		<td  CLASS="surrleft datashade"><span class="titletest2 ">Filing Description</span></td>
		<td  CLASS="surr_topbot datashade" NOWRAP="1" ALIGN="center"><span class="titletest2 ">Filing Type</span></td>
		<td  ALIGN="center" CLASS="surr_topbot datashade"  NOWRAP="1"><span class="titletest2 ">Filing Date</span></td>
		<td  ALIGN="center" CLASS="surrright datashade"  NOWRAP="1"><span class="titletest2 ">Event Date</span></td>
	</tr>

	<xsl:if test="count(OtherFiling) = 0">
	<tr VALIGN="TOP">
		<td align="center" colspan="4" CLASS="data">
			<br/><span class=" titletest2">No insiders filings available.</span><br/>
		</td>
	</tr>
	</xsl:if>

	<xsl:for-each select="OtherFiling">
								
		<tr VALIGN="TOP">
			<td  CLASS="data">
				<xsl:if test="position() mod 2 = 0">
					<xsl:attribute name="CLASS">datashade</xsl:attribute>
				</xsl:if>
				<a title="{FilingDescription}">
				<xsl:attribute name="href">
					<xsl:value-of select="DocLink"/>
				</xsl:attribute>
				<xsl:value-of select="FilingDescription"/></a>
			</td>
			<td  CLASS="data" ALIGN="center">
				<xsl:if test="position() mod 2 = 0">
					<xsl:attribute name="CLASS">datashade</xsl:attribute>
				</xsl:if>
				<A CLASS="fielddef" title="{FilingType}">
				<xsl:attribute name="href">
					<xsl:value-of select="FilingTypeLink"/>
				</xsl:attribute>
				<xsl:value-of select="FilingType"/></A>
			</td>
			<td ALIGN="center"  CLASS="data">
				<xsl:if test="position() mod 2 = 0">
					<xsl:attribute name="CLASS">datashade</xsl:attribute>
				</xsl:if>
				<xsl:value-of select="FilingDate"/>
			</td>
			<td ALIGN="center"  CLASS="data">
				<xsl:if test="position() mod 2 = 0">
					<xsl:attribute name="CLASS">datashade</xsl:attribute>
				</xsl:if>			
				<xsl:value-of select="EventDate"/>
			</td>
		</tr>
		<xsl:if test="Abstract != ''">
		<tr VALIGN="TOP" class="data">
			<td COLSPAN="4">
				<xsl:if test="position() mod 2 = 0">
					<xsl:attribute name="CLASS">datashade</xsl:attribute>
				</xsl:if>			
				<span class=" titletest2">Abstract:</span>&#160; <xsl:value-of select="Abstract"/>
			</td>
		</tr>			
		</xsl:if>
	</xsl:for-each>
		
		<xsl:if test="count(OtherFiling) != 0">
		<xsl:variable name="EndPage" select="Company/Start + Company/RowsPerPage - 1"/>

		<tr VALIGN="TOP" class="data">
			<td COLSPAN="2" class="defaultbold">Rows <xsl:value-of select="Company/Start"/> through 
      			<xsl:choose>
					<xsl:when test="$EndPage &gt; Company/Total">
						<span class=" titletest2"><xsl:value-of select="Company/Total"/></span>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$EndPage"/>
					</xsl:otherwise>
				</xsl:choose>
				of <span class=" titletest2"><xsl:value-of select="Company/Total"/></span>
			</td>
			<td COLSPAN="2" ALIGN="RIGHT">
				<xsl:for-each select="Pagination">
					<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
      				<xsl:choose>
						<xsl:when test="CurrentPage = 'false'">
							<a target="_self" title="{PageLink}">
								<xsl:attribute name="href">
									<xsl:value-of select="PageLink"/>
								</xsl:attribute>
								<span class="i"><xsl:value-of select="PageLabel"/></span>
							</a>
						</xsl:when>
						<xsl:otherwise>
						<span class=" titletest2"><xsl:value-of select="PageLabel"/></span>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</td>
		</tr>	
		</xsl:if>	
</xsl:template>

<xsl:template name="Insiders_footer2">
	<tr class="default" align="left">
		<td class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span></td>
	</tr>

</xsl:template>

  <!-- Template ONE Ends here. -->
  
  
  
  
  
  
  <!-- This is Template 3 -->
  <xsl:template name="Insiders_Title3">
  <xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
  <TR>
  <TD CLASS=" titletest" nowrap="" align="left" valign="top"><xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/><br /><IMG SRC="" WIDTH="2" HEIGHT="1" alt="" /><span class=" titletest2"><xsl:value-of select="$CompanyNameCaps"/>&#160;(<xsl:value-of select="Company/Exchange"/>&#160;-&#160;<xsl:value-of select="Company/Ticker"/>)&#160;</span></TD>
  <TD align="right"><table cellpadding="2" cellspacing="0" width="190"><tr>	
  		<td  class="surr datashade"><span class="default">If you wish to be notified of new Regulatory Filings and Reports posted to this site, <a title="click here"><xsl:attribute name="href">email.aspx?IID=<xsl:value-of select="Company/KeyInstn"/></xsl:attribute>click here</a>.</span></td>
  	</tr></table></TD>
  </TR>
  	<TR class="default" align="left">
  		<TD class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Header" disable-output-escaping="yes"/></span></TD>
  	</TR>
  </xsl:template>
  
  
  
  <xsl:template name="Insiders_title_3">
  	<tr>
  		<td COLSPAN="4" CLASS="colorlight"><span class="defaultbold">Forms 3, 4 &amp; 5</span></td>
  	</tr>
	<xsl:if test="Company/ShowEmailNotificationLink = 'true'">
		<xsl:if test="CLIENTEMAILPREFS/NotificationType = '2' or CLIENTEMAILPREFS/NotificationType = '30'">
				<TR>
					<TD COLSPAN="4" CLASS="colorlight" nowrap="1">
            <span class="default">
              If you wish to be notified of new Regulatory Filings and Reports posted to this site, <a>
                <xsl:attribute name="href">
                  email.aspx?IID=<xsl:value-of select="Company/KeyInstn"/>
                </xsl:attribute>click here</a>.
            </span>
					</TD>
				</TR>
		</xsl:if>
	</xsl:if>  
  </xsl:template>
  
  
  <xsl:template name="Insiders_data3">
  	<tr>
  		<td  CLASS=" datashade"><span class="titletest2 ">Filing Description</span></td>
  		<td  CLASS=" datashade" NOWRAP="1" ALIGN="center"><span class="titletest2 ">Filing Type</span></td>
  		<td  ALIGN="center" CLASS=" datashade"  NOWRAP="1"><span class="titletest2 ">Filing Date</span></td>
  		<td  ALIGN="center" CLASS=" datashade"  NOWRAP="1"><span class="titletest2 ">Event Date</span></td>
  	</tr>
  
  	<xsl:if test="count(OtherFiling) = 0">
  	<tr VALIGN="TOP">
  		<td align="center" colspan="4" CLASS="data">
  			<br/><span class=" titletest2">No insiders filings available.</span><br/>
  		</td>
  	</tr>
  	</xsl:if>
  
  	<xsl:for-each select="OtherFiling">
  								
  		<tr VALIGN="TOP">
  			<td  CLASS="data">
  				<xsl:if test="position() mod 2 = 0">
  					<xsl:attribute name="CLASS">datashade</xsl:attribute>
  				</xsl:if>
  				<a title="{FilingDescription}">
  				<xsl:attribute name="href">
  					<xsl:value-of select="DocLink"/>
  				</xsl:attribute>
  				<xsl:value-of select="FilingDescription"/></a>
  			</td>
  			<td  CLASS="data" ALIGN="center">
  				<xsl:if test="position() mod 2 = 0">
  					<xsl:attribute name="CLASS">datashade</xsl:attribute>
  				</xsl:if>
  				<A CLASS="fielddef" title="{FilingType}">>
  				<xsl:attribute name="href">
  					<xsl:value-of select="FilingTypeLink"/>
  				</xsl:attribute>
  				<xsl:value-of select="FilingType"/></A>
  			</td>
  			<td ALIGN="center"  CLASS="data">
  				<xsl:if test="position() mod 2 = 0">
  					<xsl:attribute name="CLASS">datashade</xsl:attribute>
  				</xsl:if>
  				<xsl:value-of select="FilingDate"/>
  			</td>
  			<td ALIGN="center"  CLASS="data">
  				<xsl:if test="position() mod 2 = 0">
  					<xsl:attribute name="CLASS">datashade</xsl:attribute>
  				</xsl:if>			
  				<xsl:value-of select="EventDate"/>
  			</td>
  		</tr>
  		<xsl:if test="Abstract != ''">
  		<tr VALIGN="TOP" class="data">
  			<td COLSPAN="4">
  				<xsl:if test="position() mod 2 = 0">
  					<xsl:attribute name="CLASS">datashade</xsl:attribute>
  				</xsl:if>			
  				<span class="default">Abstract:</span>&#160; <xsl:value-of select="Abstract"/>
  			</td>
  		</tr>			
  		</xsl:if>
  	</xsl:for-each>
  		
  		<xsl:if test="count(OtherFiling) != 0">
  		<xsl:variable name="EndPage" select="Company/Start + Company/RowsPerPage - 1"/>
  
  		<tr VALIGN="TOP" class="data">
  			<td COLSPAN="2" class="defaultbold">Rows <xsl:value-of select="Company/Start"/> through 
        			<xsl:choose>
  					<xsl:when test="$EndPage &gt; Company/Total">
  						<span class="defaultbold"><xsl:value-of select="Company/Total"/></span>
  					</xsl:when>
  					<xsl:otherwise>
  						<xsl:value-of select="$EndPage"/>
  					</xsl:otherwise>
  				</xsl:choose>
  				of <span class="defaultbold"><xsl:value-of select="Company/Total"/></span>
  			</td>
  			<td COLSPAN="2" ALIGN="RIGHT">
  				<xsl:for-each select="Pagination">
  					<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
        				<xsl:choose>
  						<xsl:when test="CurrentPage = 'false'">
  							<a target="_self"  title="{PageLabel}">>
  								<xsl:attribute name="href">
  									<xsl:value-of select="PageLink"/>
  								</xsl:attribute>
  								<span class="i"><xsl:value-of select="PageLabel"/></span>
  							</a>
  						</xsl:when>
  						<xsl:otherwise>
  										<span class="default"><xsl:value-of select="PageLabel"/></span>
  						</xsl:otherwise>
  					</xsl:choose>
  				</xsl:for-each>
  			</td>
  		</tr>	
  		</xsl:if>	
  </xsl:template>
  
  <xsl:template name="Insiders_footer3">
  	<tr class="default" align="left">
  		<td class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span></td>
  	</tr>
  </xsl:template>
  
  <!-- Template 3 -->
     
	   
	   
	   
	   
	   <!-- Main XSLT templates are here. these are the templates which are actually being displayed in the page. this is for TOP LEVEL editing. if you dont need to use the individual templates above you can do your main editing here 
	   
	   when pulling THESE templates below you must carry the top comment with them and uncomment them when you drop it into the company specific xslt.
	   
	   
	   
	   
-->


<xsl:template match="irw:Insiders">
<xsl:variable name="TemplateName" select="Company/TemplateName"/>
<xsl:choose>
<!-- Template 2 -->
<xsl:when test="$TemplateName = 'AltInsiders1'">
<xsl:call-template name="TemplateONEstylesheet" />
<SCRIPT LANGUAGE="JavaScript">
	function openWindow (url,name,widgets) {
		popupWin = window.open (url,name,widgets);
		popupWin.opener.top.name="opener";
		popupWin.focus();
	}
</SCRIPT>
<table BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" CLASS="">
<xsl:call-template name="Title_S2"/>
	<tr	ALIGN="CENTER">
		<td	CLASS="leftTOPbord" colspan="2"> 
			<table BORDER="0" CELLSPACING="0" CELLPADDING="4" WIDTH="100%">
						
<xsl:call-template name="Insiders_title_2"/>
<xsl:call-template name="Insiders_data2"/>	
						
			</table>
		</td>
	</tr>
<xsl:call-template name="Insiders_footer2"/>	
</table>
<xsl:call-template name="Copyright"/>
</xsl:when>
<!-- End Template 2 -->


<!-- Template 3 -->
<xsl:when test="$TemplateName = 'AltInsiders3'">
<xsl:call-template name="TemplateTHREEstylesheet" />
<SCRIPT LANGUAGE="JavaScript">
	function openWindow (url,name,widgets) {
		popupWin = window.open (url,name,widgets);
		popupWin.opener.top.name="opener";
		popupWin.focus();
	}
</SCRIPT>
<xsl:call-template name="Title_T3"/>
<table BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%" CLASS="Table2">
	<tr ALIGN="CENTER">
		<td colspan="2"> 
			<table BORDER="0" CELLSPACING="0" CELLPADDING="4" WIDTH="100%" >			
				<xsl:call-template name="Insiders_title_3"/>
				<xsl:call-template name="Insiders_data3"/>	
			</table>
		</td>
	</tr>
	<xsl:call-template name="Insiders_footer3"/>	
</table>
<xsl:call-template name="Copyright"/>
</xsl:when>
<!-- End Template 3 -->




<!-- Template Default -->
<xsl:otherwise>

<SCRIPT LANGUAGE="JavaScript">
	function openWindow (url,name,widgets) {
		popupWin = window.open (url,name,widgets);
		popupWin.opener.top.name="opener";
		popupWin.focus();
	}
</SCRIPT>
<table BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" CLASS="colordark">
<xsl:call-template name="Title_S1"/>
	<tr	ALIGN="CENTER">
		<td	CLASS="colordark" colspan="2"> 
			<table BORDER="0" CELLSPACING="0" CELLPADDING="4" WIDTH="100%">
				<tr>
					<td	class="colorlight">
						<table border="0" cellspacing="0" cellpadding="4" width="100%">							
							<xsl:if test="Company/ShowEmailNotificationLink = 'true'">
								<xsl:if test="CLIENTEMAILPREFS/NotificationType = '2' or CLIENTEMAILPREFS/NotificationType = '30'">
										<xsl:call-template name="Insiders_header"/>
									</xsl:if>
							</xsl:if>

<xsl:call-template name="Insiders_header_a"/>
<xsl:call-template name="Insiders_data"/>	
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
<xsl:call-template name="Insiders_footer"/>	
</table>
<xsl:call-template name="Copyright"/>

</xsl:otherwise>


</xsl:choose>

</xsl:template>



</xsl:stylesheet>


  