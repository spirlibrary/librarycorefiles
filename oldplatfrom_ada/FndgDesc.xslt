<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.ord/TR/REC-html40"
	xmlns:irw="http://www.snl.com/xml/irw">

<xsl:output method="html" media-type="text/html" indent="no"/>

<xsl:template match="FndgDesc">
<HTML>
<HEAD>
<TITLE>Funding Description</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
</HEAD>
<xsl:variable name="KeyInstn" select="Company/KeyInstn"/>
<xsl:variable name="anchortag1" select="concat('/interactive/lookandfeel/', $KeyInstn, '/style.css')"/>
<xsl:variable name="anchortag2" select="concat('/Interactive/LookAndFeel/IRStyles/live_', $KeyInstn, '.css')"/>
<xsl:variable name="anchortag3" select="concat('/Interactive/LookAndFeel/IRStyles/live_', $KeyInstn, '_suppl.css')"/>
<link REL="stylesheet" TYPE="text/css"><xsl:attribute name="href"><xsl:value-of select="$anchortag1"/></xsl:attribute></link>
<link REL="stylesheet" TYPE="text/css"><xsl:attribute name="href"><xsl:value-of select="$anchortag2"/></xsl:attribute></link>
<link REL="stylesheet" TYPE="text/css"><xsl:attribute name="href"><xsl:value-of select="$anchortag3"/></xsl:attribute></link>

<BODY CLASS="colorlight">

<table WIDTH="100%" CELLPADDING="2" CELLSPACING="0" BORDER="0">
  <tr>
	<td CLASS="colorlight"><span CLASS="title2">Funding Description</span><BR/></td>
  </tr>
  <tr>
     <td>
        <table BORDER="0" CELLSPACING="1" CELLPADDING="3" CLASS="header">
		  <tr>
		    <td CLASS="header" valign="top">Security&#160;&#160;</td>
		    <td CLASS="header" valign="top">Description</td>
		  </tr> 
		<xsl:if test="count(TradedIssues) = 0">
		  <tr>
			<td CLASS="data" colspan="2" valign="top">Data is currently unavailable.</td>
		  </tr>
		</xsl:if>
		<xsl:if test="count(TradedIssues) &gt; 0">
			<xsl:for-each select="TradedIssues">
				<tr> 
					<td CLASS="data" valign="top"><span class="default"><xsl:value-of select="TradingSymbol"/></span></td>
					<td CLASS="data" valign="top"><span class="default"><xsl:value-of select="FndgName" disable-output-escaping="yes"/></span></td>
				</tr>
			</xsl:for-each>
		</xsl:if>
		</table>
	</td>
  </tr>
</table>
</BODY>
</HTML>
</xsl:template>

</xsl:stylesheet>

