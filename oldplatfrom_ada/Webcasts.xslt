<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.ord/TR/REC-html40"
	xmlns:irw="http://www.snl.com/xml/irw">

  <xsl:output method="html" media-type="text/html" indent="no"/>

<!-- Start Default Template here-->

  <!--start xml break Present_Title -->

  <xsl:template name="Present_Title">
    <xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
    <TR>
      <TD CLASS="colordark" nowrap="1">
        <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
        <SPAN CLASS="title1light">
          <xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/>
        </SPAN>
      </TD>
      <TD CLASS="colordark" nowrap="1" align="right" valign="top">
        <span class="subtitlelight">
          <xsl:value-of select="$CompanyNameCaps"/> (<xsl:value-of select="Company/Exchange"/> - <xsl:value-of select="Company/Ticker"/>)
        </span>
        <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
      </TD>
    </TR>
    <TR class="default" align="left">
      <TD class="colorlight" colspan="2">
        <span class="default">
          <xsl:value-of select="Company/Header" disable-output-escaping="yes"/>
        </span>
      </TD>
    </TR>
  </xsl:template>

  <!-- start xml break Present_Data -->

  <xsl:template name="Present_Data">
    <xsl:for-each select="Presentation">
        <TR>
          <TD CLASS="data">
            <A TARGET="_new" TITLE="Select to view the presentation" title="{Title}">
              <xsl:attribute name="href">
                <xsl:value-of select="Link" disable-output-escaping="yes"/>
              </xsl:attribute>
              <xsl:value-of select="Title" disable-output-escaping="yes" />
            </A>
          </TD>
          <TD CLASS="data">
            <xsl:value-of select="Date"/>
          </TD>
          <xsl:if test="IconLink != ''">
            <TD CLASS="data" ALIGN="center">
              <A TARGET="_new">
                <xsl:attribute name="HREF">
                  <xsl:value-of select="IconLink" />
                </xsl:attribute>
                <IMG  BORDER="0" alt="">
                  <xsl:attribute name="SRC">
                    <xsl:value-of select="IconImage" />
                  </xsl:attribute>
                </IMG>
              </A>
            </TD>
          </xsl:if>
          <xsl:if test="IconLink = ''">
            <TD CLASS="data" ALIGN="center">
              <IMG  BORDER="0" alt="">
                <xsl:attribute name="SRC">
                  <xsl:value-of select="IconImage" disable-output-escaping="yes"/>
                </xsl:attribute>
              </IMG>
            </TD>
          </xsl:if>
        </TR>
    </xsl:for-each>
  </xsl:template>

  <!--begin WriteSoftware Links-->

  <xsl:template name="PSW_Data">
    <xsl:choose>
    	<xsl:when test="Company/DocDownloadCount = 0"></xsl:when>
      <xsl:when test="Company/DocDownloadCount &gt; 1">
        <TR>
          <TD CLASS="data" NOWRAP="NOWRAP" colspan="4">
          	<img src="/images/blank.gif" class="datashade" width="100%" height="1" border="0" alt="" />
          </TD>
        </TR>
        <TR>
          <TD CLASS="data" NOWRAP="NOWRAP" colspan="4">
           <SPAN CLASS="defaultbold">
              Click the link to the right to download :
            </SPAN>
            <xsl:for-each select="Presentation[string-length(SoftwareLink) &gt; 0]">

              <xsl:if test="normalize-space(SoftwareLink)">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                <xsl:choose>
                  <xsl:when test="../UISettings/ShowDisclaimer = 'true'">
                    <A TARGET="_new">
                      <xsl:variable name="keyinstn" select="../Company/KeyInstn" />
                      <xsl:variable name="urljump" select="SoftwareLink"/>
                      <xsl:variable name="url" select="concat('presentationsdisclaimer.aspx?IID=', $keyinstn, '&amp;website=', $urljump)" />
                      <xsl:attribute name="HREF">
                        <xsl:value-of select="$url" />
                      </xsl:attribute>
                      <xsl:value-of select="SoftwareName"/>
                    </A>
                  </xsl:when>
                  <xsl:otherwise>
                    <A TARGET="_new" title="{SoftwareName}">
                      <xsl:attribute name="HREF">
                        <xsl:value-of disable-output-escaping="yes" select="SoftwareLink"/>
                      </xsl:attribute>
                      <xsl:value-of select="SoftwareName"/>
                    </A>
                  </xsl:otherwise>
                </xsl:choose>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                <xsl:if test="not(position() = last())">
                  <xsl:text disable-output-escaping="yes">|</xsl:text>
                </xsl:if>
              </xsl:if>
            </xsl:for-each>
          </TD>
        </TR>
      </xsl:when>
      <xsl:otherwise>
        <TR>
          <TD CLASS="data" NOWRAP="NOWRAP" colspan="4">
          	<img src="/images/blank.gif" class="datashade" width="100%" height="1" border="0" />
          </TD>
        </TR>
        <TR>
          <TD CLASS="data" NOWRAP="NOWRAP" colspan="4">
            <SPAN CLASS="defaultbold">
              Click the link to the right to download :
            </SPAN>
            <xsl:for-each select="Presentation">
              <xsl:if test="normalize-space(SoftwareLink)">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                <xsl:choose>
                  <xsl:when test="../UISettings/ShowDisclaimer = 'true'">
                    <A TARGET="_new" title="{SoftwareName}">
                      <xsl:variable name="keyinstn" select="../Company/KeyInstn" />
                      <xsl:variable name="urljump" select="SoftwareLink"/>
                      <xsl:variable name="url" select="concat('presentationsdisclaimer.aspx?IID=', $keyinstn, '&amp;website=', $urljump)" />
                      <xsl:attribute name="HREF">
                        <xsl:value-of select="$url" />
                      </xsl:attribute>
                      <xsl:value-of select="SoftwareName"/>
                    </A>
                  </xsl:when>
                  <xsl:otherwise>
                    <A TARGET="_new" title="{SoftwareName}">
                      <xsl:attribute name="HREF">
                        <xsl:value-of disable-output-escaping="yes" select="SoftwareLink"/>
                      </xsl:attribute>
                      <xsl:value-of select="SoftwareName"/>
                    </A>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:if>
            </xsl:for-each>
          </TD>
        </TR>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <!--end WriteSoftware Links-->

  <xsl:template name="Present_Footer">
  <xsl:if test="Company/Footer != ''">
	<TR class="default" align="left">
		<TD class="colorlight" colspan="2">
			<span class="default">
				<xsl:value-of select="Company/Footer" disable-output-escaping="yes"/>
			</span>
		</TD>
	</TR>
  </xsl:if>
  </xsl:template>

<!-- END Default Template here-->


<!-- Start Template ONE -->

  <!-- start xml break Present_Data -->

  <xsl:template name="Present_Data2">
    <xsl:for-each select="Presentation">
        <TR>
          <TD CLASS="data">
            <A TARGET="_new" TITLE="Select to view the presentation">
              <xsl:attribute name="href">
                <xsl:value-of select="Link" disable-output-escaping="yes"/>
              </xsl:attribute>
              <xsl:value-of select="Title" disable-output-escaping="yes" />
            </A>
          </TD>
          <TD CLASS="data">
            <xsl:value-of select="Date"/>
          </TD>
          <xsl:if test="IconLink != ''">
            <TD CLASS="data" ALIGN="center">
              <A TARGET="_new">
                <xsl:attribute name="HREF">
                  <xsl:value-of select="IconLink" />
                </xsl:attribute>
                <IMG  BORDER="0" alt="">
                  <xsl:attribute name="SRC">
                    <xsl:value-of select="IconImage" />
                  </xsl:attribute>
                </IMG>
              </A>
            </TD>
          </xsl:if>
          <xsl:if test="IconLink = ''">
            <TD CLASS="data" ALIGN="center">
              <IMG  BORDER="0" alt="">
                <xsl:attribute name="SRC">
                  <xsl:value-of select="IconImage" disable-output-escaping="yes"/>
                </xsl:attribute>
              </IMG>
            </TD>
          </xsl:if>
        </TR>
    </xsl:for-each>
  </xsl:template>

  <!--begin WriteSoftware Links-->

  <xsl:template name="PSW_Data2">
    <xsl:choose>
    	<xsl:when test="Company/DocDownloadCount = 0"></xsl:when>
      <xsl:when test="Company/DocDownloadCount &gt; 1">
     	<td valign="top">
			<table border="0" cellspacing="0" cellpadding="3" width="150">
			 <TR>
				 <TD CLASS="surr datashade defaultbold">
					  Click the link to the right to download :
				 </TD>
			  </TR>
			 <xsl:for-each select="Presentation[string-length(SoftwareLink) &gt; 0]">
			 <xsl:if test="normalize-space(SoftwareLink)">
			 <TR>
				 <TD CLASS="data">
						 <xsl:choose>
							<xsl:when test="../UISettings/ShowDisclaimer = 'true'">
							  <A TARGET="_new" title="{SoftwareName}">
								 <xsl:variable name="keyinstn" select="../Company/KeyInstn" />
								 <xsl:variable name="urljump" select="SoftwareLink"/>
								 <xsl:variable name="url" select="concat('presentationsdisclaimer.aspx?IID=', $keyinstn, '&amp;website=', $urljump)" />
								 <xsl:attribute name="HREF">
									<xsl:value-of select="$url" />
								 </xsl:attribute>
								 <xsl:value-of select="SoftwareName"/>
							  </A>
							</xsl:when>
							<xsl:otherwise>
							  <A TARGET="_new" title="{SoftwareName}">
								 <xsl:attribute name="HREF">
									<xsl:value-of disable-output-escaping="yes" select="SoftwareLink"/>
								 </xsl:attribute>
								 <xsl:value-of select="SoftwareName"/>
							  </A>
							</xsl:otherwise>
						 </xsl:choose>
				 </TD>
			  </TR>
			  </xsl:if>
			</xsl:for-each>
			</table>
		</td>
      </xsl:when>
      <xsl:otherwise>
      <td valign="top">
			<table border="0" cellspacing="0" cellpadding="3" width="150">
			 <TR>
				 <TD CLASS="surr datashade defaultbold">
					  Click the link to the right to download :
				 </TD>
			  </TR>
			<xsl:for-each select="Presentation">
			 <xsl:if test="normalize-space(SoftwareLink)">
			 <TR>
				 <TD CLASS="data">
					 <xsl:choose>
						<xsl:when test="../UISettings/ShowDisclaimer = 'true'">
						  <A TARGET="_new" title="{SoftwareName}">
							 <xsl:variable name="keyinstn" select="../Company/KeyInstn" />
							 <xsl:variable name="urljump" select="SoftwareLink"/>
							 <xsl:variable name="url" select="concat('presentationsdisclaimer.aspx?IID=', $keyinstn, '&amp;website=', $urljump)" />
							 <xsl:attribute name="HREF">
								<xsl:value-of select="$url" />
							 </xsl:attribute>
							 <xsl:value-of select="SoftwareName"/>
						  </A>
						</xsl:when>
						<xsl:otherwise>
						  <A TARGET="_new" title="{SoftwareName}">
							 <xsl:attribute name="HREF">
								<xsl:value-of disable-output-escaping="yes" select="SoftwareLink"/>
							 </xsl:attribute>
							 <xsl:value-of select="SoftwareName"/>
						  </A>
						</xsl:otherwise>
					 </xsl:choose>
				 </TD>
			  </TR>
			  </xsl:if>
			</xsl:for-each>
			</table>
		</td>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <!--end WriteSoftware Links-->

  <xsl:template name="Present_Footer2">
  <xsl:if test="Company/Footer != ''">
	<TR class="default" align="left">
		<TD class="data" colspan="2">
			<span class="default">
				<xsl:value-of select="Company/Footer" disable-output-escaping="yes"/>
			</span>
		</TD>
	</TR>
  </xsl:if>
  </xsl:template>
<!-- End Template ONE -->


<!-- Start Template THREE -->

  <!-- start xml break Present_Data -->

  <xsl:template name="Present_Data3">
    <xsl:for-each select="Presentation">
        <TR>
          <TD CLASS="data">
            <A TARGET="_new" TITLE="Select to view the presentation">
              <xsl:attribute name="href">
                <xsl:value-of select="Link" disable-output-escaping="yes"/>
              </xsl:attribute>
              <xsl:value-of select="Title" disable-output-escaping="yes" />
            </A>
          </TD>
          <TD CLASS="data">
            <xsl:value-of select="Date"/>
          </TD>
          <xsl:if test="IconLink != ''">
            <TD CLASS="data" ALIGN="center">
              <A TARGET="_new">
                <xsl:attribute name="HREF">
                  <xsl:value-of select="IconLink" />
                </xsl:attribute>
                <IMG  BORDER="0" alt="">
                  <xsl:attribute name="SRC">
                    <xsl:value-of select="IconImage" />
                  </xsl:attribute>
                </IMG></A>
            </TD>
          </xsl:if>
          <xsl:if test="IconLink = ''">
            <TD CLASS="data" ALIGN="center">
              <IMG  BORDER="0" alt="">
                <xsl:attribute name="SRC">
                  <xsl:value-of select="IconImage" disable-output-escaping="yes"/>
                </xsl:attribute>
              </IMG>
            </TD>
          </xsl:if>
        </TR>
    </xsl:for-each>
  </xsl:template>

  <!--begin WriteSoftware Links-->

  <xsl:template name="PSW_Data3">
    <xsl:choose>
    	<xsl:when test="Company/DocDownloadCount = 0"></xsl:when>
      <xsl:when test="Company/DocDownloadCount &gt; 1">
      <td valign="top">
			<table border="0" cellspacing="0" cellpadding="3" width="150" id="Table2" class="table2">
			 <TR>
				 <TD CLASS="datashade defaultbold table1_item">
					  Click the link to the right to download :
				 </TD>
			  </TR>
			 <xsl:for-each select="Presentation[string-length(SoftwareLink) &gt; 0]">
			 <xsl:if test="normalize-space(SoftwareLink)">
			 <TR>
				 <TD CLASS="data">
						 <xsl:choose>
							<xsl:when test="../UISettings/ShowDisclaimer = 'true'">
							  <A TARGET="_new" title="{SoftwareName}">
								 <xsl:variable name="keyinstn" select="../Company/KeyInstn" />
								 <xsl:variable name="urljump" select="SoftwareLink"/>
								 <xsl:variable name="url" select="concat('presentationsdisclaimer.aspx?IID=', $keyinstn, '&amp;website=', $urljump)" />
								 <xsl:attribute name="HREF">
									<xsl:value-of select="$url" />
								 </xsl:attribute>
								 <xsl:value-of select="SoftwareName"/>
							  </A>
							</xsl:when>
							<xsl:otherwise>
							  <A TARGET="_new" title="{SoftwareName}">
								 <xsl:attribute name="HREF">
									<xsl:value-of disable-output-escaping="yes" select="SoftwareLink"/>
								 </xsl:attribute>
								 <xsl:value-of select="SoftwareName"/>
							  </A>
							</xsl:otherwise>
						 </xsl:choose>
				 </TD>
			  </TR>
			  </xsl:if>
			</xsl:for-each>
			</table>
		</td>
      </xsl:when>
      <xsl:otherwise>
      <td valign="top">
			<table border="0" cellspacing="0" cellpadding="3" width="150" id="Table2" class="table2">
			 <TR>
				 <TD CLASS="datashade defaultbold table1_item">
					  Click the link to the right to download :
				 </TD>
			  </TR>
			<xsl:for-each select="Presentation">
			 <xsl:if test="normalize-space(SoftwareLink)">
			 <TR>
				 <TD CLASS="data">
					 <xsl:choose>
						<xsl:when test="../UISettings/ShowDisclaimer = 'true'">
						  <A TARGET="_new" title="{SoftwareName}">
							 <xsl:variable name="keyinstn" select="../Company/KeyInstn" />
							 <xsl:variable name="urljump" select="SoftwareLink"/>
							 <xsl:variable name="url" select="concat('presentationsdisclaimer.aspx?IID=', $keyinstn, '&amp;website=', $urljump)" />
							 <xsl:attribute name="HREF">
								<xsl:value-of select="$url" />
							 </xsl:attribute>
							 <xsl:value-of select="SoftwareName"/>
						  </A>
						</xsl:when>
						<xsl:otherwise>
						  <A TARGET="_new" title="{SoftwareName}">
							 <xsl:attribute name="HREF">
								<xsl:value-of disable-output-escaping="yes" select="SoftwareLink"/>
							 </xsl:attribute>
							 <xsl:value-of select="SoftwareName"/>
						  </A>
						</xsl:otherwise>
					 </xsl:choose>
				 </TD>
			  </TR>
			  </xsl:if>
			</xsl:for-each>
			</table>
		</td>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <!--end WriteSoftware Links-->

  <xsl:template name="Present_Footer3">
  <xsl:if test="Company/Footer != ''">
	<TR class="default" align="left">
		<TD class="data" colspan="2">
			<span class="default">
				<xsl:value-of select="Company/Footer" disable-output-escaping="yes"/>
			</span>
		</TD>
	</TR>
  </xsl:if>
  </xsl:template>
<!-- End Template THREE -->



<!-- Main XSLT templates are here. -->


<xsl:template match="irw:Presentations">
<xsl:variable name="TemplateName" select="Company/TemplateName"/>

<xsl:choose>
<!-- Template ONE -->
<xsl:when test="$TemplateName = 'AltWebcast1'">
<!-- Copy from here down

<xsl:template match="irw:Presents">
<xsl:variable name="TemplateName" select="Company/TemplateName"/>
-->
<xsl:call-template name="TemplateONEstylesheet" />
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" CLASS="table2" valign="top">
<xsl:call-template name="Title_S2"/>
<TR ALIGN="CENTER">
	<TD CLASS="leftTOPbord data" colspan="2" align="left">
		<table border="0" cellspacing="0" cellpadding="4" width="100%" id="Table1" >
			<xsl:if test="count(YearLinks/Year) &gt; 0">
			<tr class="data">
				<td align="left" valign="top" colspan="2">
					<table border="0" cellspacing="3" cellpadding="1" width="100%" valign="top">
					<tr class="data">
						<xsl:for-each select="YearLinks">
						<td nowrap="1">
							<a>
							<xsl:attribute name="href">
								<xsl:value-of select="Link"/>
							</xsl:attribute>
								<strong><xsl:value-of select="Year"/></strong>
							</a>
						</td>
						</xsl:for-each>
					</tr>
					</table>
				</td>
			</tr>
			</xsl:if>
			<xsl:choose>
			<xsl:when test="count(Presentation) &gt; 0">
			<tr class="data">
				<td valign="top" width="100%">
					<table border="0" cellspacing="0" cellpadding="3" width="100%">
						<tr>
							<td class="datashade surrleft defaultbold" NOWRAP="NOWRAP">Title</td>
							<td class="datashade surr_topbot defaultbold" align="left">Date</td>
							<td class="datashade surrright defaultbold" align="center">Format</td>
						</tr>
						<xsl:call-template name="Present_Data2"/>
					</table>
				</td>
				<xsl:call-template name="PSW_Data2"/>
			</tr>
			</xsl:when>
			<xsl:otherwise>
				<tr class="data">
					<td valign="top" colspan="2">
						<span class="default">There are no documents available for the current year.</span>
					</td>
				</tr>
			</xsl:otherwise>
			</xsl:choose>
		</table>
	</TD>
</TR>
<xsl:call-template name="Present_Footer2"/>
</TABLE>
<xsl:call-template name="Copyright"/>
<!--
</xsl:template>

End Copy Here for Template ONE
-->
</xsl:when>
<!-- End Template ONE -->

<!-- Start Template THREE -->

<xsl:when test="$TemplateName = 'AltWebcast3'">
<!-- Copy from here down

<xsl:template match="irw:Presents">
<xsl:variable name="TemplateName" select="Company/TemplateName"/>
-->
<xsl:call-template name="TemplateTHREEstylesheet" />
<xsl:call-template name="Title_T3"/>
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" CLASS="table2" valign="top">
<TR ALIGN="CENTER">
	<TD CLASS="data" valign="top">
		<table border="0" cellspacing="0" cellpadding="4" width="100%" id="Table1" >
			<xsl:if test="count(YearLinks/Year) &gt; 0">
			<tr class="data">
				<td align="left" valign="top" colspan="2">
					<table border="0" cellspacing="3" cellpadding="1" width="100%" valign="top" id="Table2">
					<tr class="data">
						<xsl:for-each select="YearLinks">
						<td nowrap="1">
							<a>
							<xsl:attribute name="href">
								<xsl:value-of select="Link"/>
							</xsl:attribute>
								<strong><xsl:value-of select="Year"/></strong>
							</a>
						</td>
						</xsl:for-each>
					</tr>
					</table>
				</td>
			</tr>
			</xsl:if>
			<xsl:choose>
			<xsl:when test="count(Presentation) &gt; 0">
			<tr class="data">
				<td valign="top" width="100%">
					<table border="0" cellspacing="0" cellpadding="3" width="100%" id="Table2" class="table2">
						<tr>
							<td class="datashade table1_item defaultbold" NOWRAP="NOWRAP">Title</td>
							<td class="datashade table1_item defaultbold" align="left">Date</td>
							<td class="datashade table1_item defaultbold" align="center">Format</td>
						</tr>
						<xsl:call-template name="Present_Data3"/>
					</table>
				</td>
				<xsl:call-template name="PSW_Data3"/>
			</tr>
			</xsl:when>
			<xsl:otherwise>
				<tr class="data">
					<td valign="top" colspan="2">
						<span class="default">There are no documents available for the current year.</span>
					</td>
				</tr>
			</xsl:otherwise>
			</xsl:choose>
		</table>
	</TD>
</TR>
<xsl:call-template name="Present_Footer3"/>
</TABLE>
<xsl:call-template name="Copyright"/>
<!--
</xsl:template>

End Copy Here for Template THREE
-->
</xsl:when>
<!-- End Template THREE -->


<!-- Default Template  -->
<xsl:otherwise>
<!-- Copy from here down

<xsl:template match="irw:Presents">
<xsl:variable name="TemplateName" select="Company/TemplateName"/>
-->
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" CLASS="colordark" valign="top">
<xsl:call-template name="Title_S1"/>

<TR ALIGN="CENTER">
	<TD CLASS="colordark" colspan="2">
		<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="4" WIDTH="100%" valign="top">
		<tr>
			<td class="colorlight">
				<table border="0" cellspacing="0" cellpadding="3" width="100%" valign="top">
				<xsl:if test="count(YearLinks/Year) &gt; 0">
				<tr class="colorlight">
					<td align="left" valign="top" colspan="4">
						<table border="0" cellspacing="3" cellpadding="1" width="100%" valign="top">
						<tr class="colorlight">
							<xsl:for-each select="YearLinks">
							<td nowrap="1">
								<a>
								<xsl:attribute name="href">
									<xsl:value-of select="Link"/>
								</xsl:attribute>
									<strong><xsl:value-of select="Year"/></strong>
								</a>
							</td>
							</xsl:for-each>
						</tr>
						</table>
					</td>
				</tr>
				</xsl:if>
				<xsl:choose>
				<xsl:when test="count(Presentation) &gt; 0">
					<tr>
						<td class="header" NOWRAP="NOWRAP">Title</td>
						<td class="header" align="left">Date</td>
						<td class="header" align="center">Format</td>
					</tr>
					<xsl:call-template name="Present_Data"/>
					<xsl:call-template name="PSW_Data"/>
				</xsl:when>
				<xsl:otherwise>
					<tr class="colorlight">
						<td valign="top">
							<span class="default">There are no documents available for the current year.</span>
						</td>
					</tr>
				</xsl:otherwise>
				</xsl:choose>
				</table>
			</td>
		</tr>
		</TABLE>
	</TD>
</TR>
<xsl:call-template name="Present_Footer"/>
</TABLE>
<xsl:call-template name="Copyright"/>
<!--
</xsl:template>

End Copy Here
-->
</xsl:otherwise>

<!--End  Default Template -->

</xsl:choose>
</xsl:template>
</xsl:stylesheet>
