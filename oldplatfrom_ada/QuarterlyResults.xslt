<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.ord/TR/REC-html40"
	xmlns:irw="http://www.snl.com/xml/irw">

<!-- <xsl:output method="html" media-type="text/html" indent="no"/>
<xsl:output method="html"  doctype-system="http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd" doctype-public="-//W3C//Dtd XHTML 1.0 Transitional//EN"/> -->

<xsl:output method="html" doctype-public="-//W3C//Dtd XHTML 1.0 Transitional//EN" indent="no"/>

<!-- TEMPLATE ONE -->


<xsl:template name="QR_Title">
<xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
	<TR>
		<TD CLASS="colordark" nowrap="1">
			<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
			<SPAN CLASS="title1light"><xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/></SPAN>
	    </TD>
		<TD CLASS="colordark" nowrap="1" align="right" valign="top">
			<span class="subtitlelight"><xsl:value-of select="$CompanyNameCaps"/> (<xsl:value-of select="Company/Exchange"/> - <xsl:value-of select="Company/Ticker"/>)</span>
			<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
		</TD>
	</TR>
		<TR class="default" align="left">
			<TD class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Header" disable-output-escaping="yes"/></span></TD>
	</TR>
</xsl:template>


<xsl:template name="QR_Items">
  <table border="0" cellspacing="0" cellpadding="3" width="100%">
    <xsl:for-each select="Headers">
      <TR>
        <td valign="top">
          <DIV class="header" style="padding:3px;"><xsl:value-of select="IRCompanyLinksHeader" disable-output-escaping="yes"/></DIV>
          <ul>
            <xsl:for-each select="Items">
              <li>
                <a class="fielddef" href="{WebSiteURL}" target="_blank" title="{WebSiteURLText}">
                  <xsl:value-of select="WebSiteURLText" disable-output-escaping="yes"/>
                </a>
              </li>
            </xsl:for-each>
          </ul>
        </td>
      </TR>
    </xsl:for-each>
  </table>
</xsl:template>
<!-- END TEMPLATE ONE -->







<!-- TEMPLATE ONE -->

<xsl:template name="QR_Title2">
<xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
<TR>
<TD CLASS="title2colordark titletest" nowrap="" colspan="2"><xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/><br /><IMG SRC="" WIDTH="2" HEIGHT="1" alt="" /><span class="title2colordark titletest2"><xsl:value-of select="$CompanyNameCaps"/>&#160;(<xsl:value-of select="Company/Exchange"/>&#160;-&#160;<xsl:value-of select="Company/Ticker"/>)&#160;</span></TD>

</TR>
	<TR class="default" align="left">
		<TD class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Header" disable-output-escaping="yes"/></span></TD>
	</TR>
</xsl:template>



<xsl:template name="QR_Items2">
  <table border="0" cellspacing="0" cellpadding="3" width="100%">							
    <tr>
      <td valign="top">
        <div class="data" align="right" id="toggleShowHideAll"><a class="ResultsExpandAll" href="javascript:;" title="Expand All">Expand All</a> | <a class="ResultsCollapseAll" href="javascript:;" title="Collapse All">Collapse All</a></div>
        <xsl:for-each select="Headers">
          <div class="technology"><a class="toggle_line" href="javascript:;" title="Click to view Quarterly Result"><img src="images/arrow-rt.gif" border="0" class="toggleImage" alt="Right Arrow"/></a><a class="toggle_line" href="javascript:;"><xsl:value-of select="IRCompanyLinksHeader" disable-output-escaping="yes"/></a></div>
          <div class="thelanguage" style="display:none">
            <ul style="margin-top: 0px; padding-top: 0px;">
              <xsl:for-each select="Items">
                <li><a class="fielddef" href="{WebSiteURL}" target="_blank" title="{WebSiteURLText}"><xsl:value-of select="WebSiteURLText" disable-output-escaping="yes"/></a></li>
              </xsl:for-each>
            </ul>
          </div>
        </xsl:for-each>
      </td>
    </tr>
  </table>
</xsl:template>
<!-- END TEMPLATE ONE -->


<!-- TEMPLATE 3 -->

<xsl:template name="QR_Title3">
<xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
<TR><TD CLASS="title2colordark titletest" nowrap="" colspan="2"><xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/><br /><IMG SRC="" WIDTH="2" HEIGHT="1" alt="" /><span class="title2colordark titletest2"><xsl:value-of select="$CompanyNameCaps"/>&#160;(<xsl:value-of select="Company/Exchange"/>&#160;-&#160;<xsl:value-of select="Company/Ticker"/>)&#160;</span></TD></TR>
<TR class="default" align="left"><TD class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Header" disable-output-escaping="yes"/></span></TD></TR>
</xsl:template>

<xsl:template name="QR_Items3">
  <table border="0" cellspacing="0" cellpadding="3" width="100%">
    <tr>
      <td valign="top">
        <div class="data" align="right" id="toggleShowHideAll"><a class="ResultsExpandAll" href="javascript:;" title="Expand All">Expand All</a> | <a class="ResultsCollapseAll" href="javascript:;" title="Collapse All">Collapse All</a></div>
        <xsl:for-each select="Headers">
          <div class="technology"><a class="toggle_line" href="javascript:;" title="Click to view Quarterly Result"><img src="images/arrow-rt.gif" border="0" class="toggleImage" alt="Right Arrow" /></a><a class="toggle_line" href="javascript:;"><xsl:value-of select="IRCompanyLinksHeader" disable-output-escaping="yes"/></a></div>
          <div class="thelanguage" style="display:none">
            <ul style="margin-top: 0px; padding-top: 0px;">
              <xsl:for-each select="Items">
                <li><a class="fielddef" href="{WebSiteURL}" target="_blank" title="{WebSiteURLText}"><xsl:value-of select="WebSiteURLText" disable-output-escaping="yes"/></a></li>
              </xsl:for-each>
            </ul>
          </div>
        </xsl:for-each>
      </td>
    </tr>
  </table>
</xsl:template>

<!-- END TEMPLATE 3 -->

<xsl:template name="QR_Items_source">
  <script src="javascript/jquery-1.4.1.min.js"></script>
  <script src="javascript/menuToggler.js"></script>
  <style>
    .technology { border: 1px solid #ffffff; line-height: 25px; padding-left: 3px; }
    a.toggle_line{ text-decoration:none; font-weight:bold; }
    a.toggle_line:visited { text-decoration:none; font-weight:bold; }
    img.toggleImage{ padding-top:3px; z-index:999999; text-decoration:none !important; padding-right:8px;}
  </style>
</xsl:template>



<xsl:template match="irw:QuarterlyResults">
<xsl:variable name="TemplateName" select="Company/TemplateName"/>
  <xsl:choose>
    <xsl:when test="$TemplateName = 'AltQuartRe1' or $TemplateName = 'AltQuartRe3'">
      <xsl:call-template name="QR_Items_source"/>
    </xsl:when>
  </xsl:choose>
   
<xsl:choose>
<!-- Template ONE -->
<xsl:when test="$TemplateName = 'AltQuartRe1'">
	<xsl:call-template name="TemplateONEstylesheet"/> 
	<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" CLASS="">
	<xsl:call-template name="Title_S2"/>
		<TR>
			<TD CLASS="leftTOPbord" colspan="2" valign="top"> 
				<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="4" WIDTH="100%">
					<tr>
						<td	class="data" valign="top">
              <xsl:choose>
                <xsl:when test="count(Headers) &gt; 0"><xsl:call-template name="QR_Items2"/></xsl:when>
                <xsl:otherwise>
                  <table border="0" cellspacing="0" cellpadding="3" width="100%">
                    <tr>
                      <td valign="top">
                        <span class="default">Quarterly Results Documents currently unavailable. </span>
                      </td>
                    </tr>
                  </table>
                </xsl:otherwise>
              </xsl:choose>
						</td>
					</tr>
				</TABLE>
			</TD>
		</TR>
		<TR class="default" align="left">
			<TD class="data" colspan="2"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span></TD>
		</TR>
	</TABLE>	
	<xsl:call-template name="Copyright"/>	
</xsl:when>
<!-- End Template ONE -->

<!-- Template 3 -->
<xsl:when test="$TemplateName = 'AltQuartRe3'">
<xsl:call-template name="TemplateTHREEstylesheet"/>
<xsl:call-template name="Title_T3"/>
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" CLASS="table2">
	<TR>
		<TD CLASS="data" valign="top"> 
			<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="4" WIDTH="100%">
				<tr>
					<td	class="data" valign="top">			
            <xsl:choose>
              <xsl:when test="count(Headers) &gt; 0"><xsl:call-template name="QR_Items3"/></xsl:when>
              <xsl:otherwise>
                <table border="0" cellspacing="0" cellpadding="3" width="100%">
                  <tr>
                    <td valign="top">
                      <span class="default">Quarterly Results Documents currently unavailable. </span>
                    </td>
                  </tr>
                </table>
              </xsl:otherwise>
            </xsl:choose>
					</td>
				</tr>
			</TABLE>
		</TD>
	</TR>
  <xsl:if test="Company/Footer != ''">
    <TR class="default" align="left">
      <TD class="data"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span></TD>
    </TR>
  </xsl:if>
</TABLE>
<xsl:call-template name="Copyright"/>	
</xsl:when>
<!-- End Template 3 -->


<!-- Template Default -->
<xsl:otherwise>

<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" CLASS="colordark">
	<xsl:call-template name="Title_S1"/>
	<TR>
		<TD CLASS="colordark" colspan="2" valign="top"> 
			<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="4" WIDTH="100%">
				<tr>
					<td	class="colorlight" valign="top">
            <xsl:choose>
              <xsl:when test="count(Headers) &gt; 0"><xsl:call-template name="QR_Items"/></xsl:when>
              <xsl:otherwise>
                <table border="0" cellspacing="0" cellpadding="3" width="100%">
                  <tr>
                    <td valign="top">
                      <span class="default">Quarterly Results Documents currently unavailable. </span>
                    </td>
                  </tr>
                </table>
              </xsl:otherwise>
            </xsl:choose>				
					</td>
				</tr>
			</TABLE>
		</TD>
	</TR>
  <xsl:if test="Company/Footer != ''">
    <TR class="default" align="left">
      <TD class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span></TD>
    </TR>  
  </xsl:if>	
</TABLE>
<xsl:call-template name="Copyright"/>	
</xsl:otherwise>


</xsl:choose>
</xsl:template>
</xsl:stylesheet>
  
