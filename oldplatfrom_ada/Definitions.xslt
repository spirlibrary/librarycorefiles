<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.ord/TR/REC-html40"
	xmlns:irw="http://www.snl.com/xml/irw">

<xsl:output method="html" media-type="text/html" indent="no"/>

<xsl:template match="Definitions/Definitions">
<HTML>
<head><title><xsl:value-of select="Caption" disable-output-escaping="yes"/></title></head>
<xsl:variable name="KeyInstn" select="KeyInstn"/>
<xsl:variable name="anchortag1" select="concat('/interactive/lookandfeel/', $KeyInstn, '/style.css')"/>
<xsl:variable name="anchortag2" select="concat('/Interactive/LookAndFeel/IRStyles/live_', $KeyInstn, '.css')"/>
<xsl:variable name="anchortag3" select="concat('/Interactive/LookAndFeel/IRStyles/live_', $KeyInstn, '_suppl.css')"/>
<link REL="stylesheet" TYPE="text/css"><xsl:attribute name="href"><xsl:value-of select="$anchortag1"/></xsl:attribute></link>
<link REL="stylesheet" TYPE="text/css"><xsl:attribute name="href"><xsl:value-of select="$anchortag2"/></xsl:attribute></link>
<link REL="stylesheet" TYPE="text/css"><xsl:attribute name="href"><xsl:value-of select="$anchortag3"/></xsl:attribute></link>
<body CLASS="data">
<table BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" CLASS="data">

	<TR ALIGN="left">
		<TD CLASS=""> 
			<span class=""><xsl:value-of select="Definition" disable-output-escaping="yes"/></span>
		</TD>
	</TR>
</table>
</body>
</HTML>
</xsl:template>



</xsl:stylesheet>


  