<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns="http://www.w3.ord/TR/REC-html40"
  xmlns:irw="http://www.snl.com/xml/irw">

<xsl:output method="html" media-type="text/html" indent="no"/>
<xsl:decimal-format name="data" NaN="NA"/>

<!-- Start Default Template here-->

<!--
//This is the default template. Most changes are to either remove the company name, ticker, or exchange. you can also change the name of the page or remove this row all together.
//These templates below are broken up how the page is displayed.
-->

<xsl:template name="HEADER_PEER">
  <xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
  <TR>
    <TD CLASS="colordark" nowrap="0"><SPAN CLASS="title1light"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text><xsl:value-of disable-output-escaping="yes" select="TitleInfo/TitleName"/></SPAN></TD>
    <TD CLASS="colordark" nowrap="0" align="right" valign="top"><span class="subtitlelight"><xsl:value-of select="$CompanyNameCaps"/>&#160;(<xsl:value-of select="Company/Exchange"/>&#160;-&#160;<xsl:value-of select="Company/Ticker"/>)&#160;</span></TD>
  </TR>
  <TR class="default" align="left">
      <TD class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Header" disable-output-escaping="yes"/></span></TD>
  </TR>
</xsl:template>

<xsl:template name="peerdatarow">
<xsl:param name="KeyInstn"/>
<xsl:param name="SNLItemName"/>
<xsl:param name="EOP"/>
<xsl:param name="DisplayName"/>
<xsl:param name="ItemName"/>
<xsl:param name="NumberFormatString"/>
<xsl:variable name="quote">'</xsl:variable>
<xsl:variable name="itemname" select="concat($quote, $SNLItemName, $quote)"/>
<xsl:variable name="CFCount" select="count(CompanyFinancials)"/>
          <tr valign="top" class="data">
        <td class="data" width="1%"><img src="/images/interactive/blank.gif" width="5" height="12" alt=""/></td>
    <td class="data" width="54%" align="left">
      <a href="#" class="fielddef">
      <xsl:attribute name="onClick">DefWindow(<xsl:value-of select="$KeyInstn"/>,
      <xsl:value-of select="$itemname"/>,
      <xsl:value-of select="$EOP"/>); return false
      </xsl:attribute>
      <xsl:value-of select="$DisplayName"/>
      </a>
    </td>



    <xsl:for-each select="CompanyFinancials">
    <xsl:variable name="Value" select="*[name(.)=$ItemName]"/>
    <td class="data" align="right" width="15%">
    <xsl:value-of select="format-number($Value, $NumberFormatString, 'data')"/>
    <SPAN CLASS="dataalign">&#160;</SPAN></td>
    </xsl:for-each>



    <xsl:for-each select="Median">
    <xsl:variable name="Value" select="*[name(.)=$ItemName]"/>

    <xsl:if test="$CFCount = 0">
    <td class="data" align="right" width="15%">NA</td>
    </xsl:if>
    <td class="data" align="right" width="15%">
    <xsl:value-of select="format-number($Value, $NumberFormatString, 'data')"/>
    <SPAN CLASS="dataalign">&#160;</SPAN></td>
    </xsl:for-each>

    <xsl:for-each select="Average">
    <xsl:variable name="Value" select="*[name(.)=$ItemName]"/>
    <td class="data" align="right" width="15%">
    <xsl:value-of select="format-number($Value, $NumberFormatString, 'data')"/>
    <SPAN CLASS="dataalign">&#160;</SPAN></td>
    </xsl:for-each>
         </tr>
</xsl:template>


<!--start xml break PEERDATA-->
<xsl:template name="PEERDATA">
    <xsl:variable name="GAAPDomain" select="Company/GAAP"/>
    <xsl:variable name="INSURANCE_UNDERWRITER" select="8"/>
    <xsl:variable name="INSURANCE_BROKER" select="9"/>
    <xsl:variable name="THRIFT" select="2"/>
    <xsl:variable name="INVESTMENT_COMPANY" select="7"/>
    <xsl:variable name="BROKER_DEALER" select="5"/>
    <xsl:variable name="INVESTMENT_ADVISOR" select="4"/>
    <xsl:variable name="REIT" select="6"/>
    <xsl:variable name="BANK" select="1"/>
    <xsl:variable name="SPECIALTY_LENDER" select="3"/>
    <xsl:variable name="ENERGY" select="10"/>
    <xsl:variable name="COMMUNICATIONS" select="14"/>
    <xsl:variable name="FINTECH" select="15"/>
    <xsl:variable name="HOMEBUILDER" select="16"/>
    <xsl:variable name="COAL" select="17"/>
    <xsl:variable name="GAS" select="18"/>
    <xsl:variable name="GAMING" select="19"/>
    <xsl:variable name="MEDIA_ENT" select="20"/>
    <xsl:variable name="NEWMEDIA" select="21"/>
<xsl:choose>
<xsl:when test="$GAAPDomain = $BANK or $GAAPDomain = $THRIFT">
  <tr class="colorlight">
    <td class="colorlight" COLSPAN="3">
      <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2">
        <tr valign="BOTTOM" class="header">
          <td colspan="2" NOWRAP="1" class="header">Performance Ratios (%)</td>
          <td align="RIGHT" class="header">
             <xsl:if test="not(normalize-space(Company/Ticker))"><xsl:value-of select="Company/InstnName"/></xsl:if>
              <xsl:value-of select="Company/Ticker"/></td>
          <td align="RIGHT" class="header" nowrap="1">Peer<br/>Median</td>
          <td align="RIGHT" class="header" nowrap="1">Peer<br/>Average</td>
        </tr>
          <xsl:call-template name="peerdatarow">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">ROAA</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName">ROAA</xsl:with-param>
          <xsl:with-param name="ItemName">ROAA</xsl:with-param>
          <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

          <xsl:call-template name="peerdatarow">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">ROAE</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName">ROAE</xsl:with-param>
          <xsl:with-param name="ItemName">ROAE</xsl:with-param>
          <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

          <xsl:call-template name="peerdatarow">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">NetInterestMargin</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName">Net Interest Margin</xsl:with-param>
          <xsl:with-param name="ItemName">NetInterestMargin</xsl:with-param>
          <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

          <xsl:call-template name="peerdatarow">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">EfficiencyRatio</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName">Efficiency Ratio</xsl:with-param>
          <xsl:with-param name="ItemName">EfficiencyRatio</xsl:with-param>
          <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

          <xsl:call-template name="peerdatarow">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">InvLoansToDeposits</xsl:with-param>
          <xsl:with-param name="EOP">1</xsl:with-param>
          <xsl:with-param name="DisplayName">Loans / Deposits</xsl:with-param>
          <xsl:with-param name="ItemName">InvLoansToDeposits</xsl:with-param>
          <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      </table>
    </td>
  </tr>
  <tr class="colorlight">
    <td class="colorlight" COLSPAN="3">
      <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2">
        <tr valign="BOTTOM" class="header">
          <td colspan="2" NOWRAP="1" class="header">Asset Quality Ratios (%)</td>
          <td align="RIGHT" class="header">&#160;</td>
          <td align="RIGHT" class="header" nowrap="1">&#160;</td>
          <td align="RIGHT" class="header" nowrap="1">&#160;</td>
        </tr>

        <xsl:call-template name="peerdatarow">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">NPAsToAssets</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">NPAs / Assets</xsl:with-param>
        <xsl:with-param name="ItemName">NPAsToAssets</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param>
        </xsl:call-template>

        <xsl:call-template name="peerdatarow">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">NetChargeoffsToLoans</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">NCOs / Avg Loans</xsl:with-param>
        <xsl:with-param name="ItemName">NetChargeoffsToLoans</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param>
        </xsl:call-template>

        <xsl:call-template name="peerdatarow">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">ReservesToLoans</xsl:with-param>
        <xsl:with-param name="EOP">1</xsl:with-param>
        <xsl:with-param name="DisplayName">Reserves / Loans</xsl:with-param>
        <xsl:with-param name="ItemName">ReservesToLoans</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param>
        </xsl:call-template>

        <xsl:call-template name="peerdatarow">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">ReservesToNPAs</xsl:with-param>
        <xsl:with-param name="EOP">1</xsl:with-param>
        <xsl:with-param name="DisplayName">Reserves / NPAs</xsl:with-param>
        <xsl:with-param name="ItemName">ReservesToNPAs</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param>
        </xsl:call-template>
      </table>
    </td>
  </tr>
  <tr class="colorlight">
    <td class="colorlight" COLSPAN="3">
      <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2">
        <tr valign="BOTTOM" class="header">
          <td colspan="2" NOWRAP="1" class="header">Capital Ratios (%)</td>
          <td align="RIGHT" class="header">&#160;</td>
          <td align="RIGHT" class="header" nowrap="1">&#160;</td>
          <td align="RIGHT" class="header" nowrap="1">&#160;</td>
        </tr>
        <xsl:call-template name="peerdatarow">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">T1RatioAsRegBOCalc</xsl:with-param>
            <xsl:with-param name="EOP">1</xsl:with-param>
            <xsl:with-param name="DisplayName">Tier 1 Capital</xsl:with-param>
            <xsl:with-param name="ItemName">T1RatioAsRegBOCalc</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">TangibleEquityToAssets</xsl:with-param>
            <xsl:with-param name="EOP">1</xsl:with-param>
            <xsl:with-param name="DisplayName">Tangible Equity / Tangible Assets</xsl:with-param>
            <xsl:with-param name="ItemName">TangibleEquityToAssets</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">EquityToAssets</xsl:with-param>
            <xsl:with-param name="EOP">1</xsl:with-param>
            <xsl:with-param name="DisplayName">Total Equity / Total Assets</xsl:with-param>
            <xsl:with-param name="ItemName">EquityToAssets</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      </table>
    </td>
  </tr>
  <tr class="colorlight">
    <td class="colorlight" COLSPAN="3">
      <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2">
        <tr valign="BOTTOM" class="header">
          <td colspan="2" NOWRAP="1" class="header">Market Ratios</td>
          <td align="RIGHT" class="header">&#160;</td>
          <td align="RIGHT" class="header" nowrap="1">&#160;</td>
          <td align="RIGHT" class="header" nowrap="1">&#160;</td>
        </tr>
        <xsl:call-template name="peerdatarow">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">PriceToEarnings</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">Price / Earnings (x)</xsl:with-param>
            <xsl:with-param name="ItemName">PriceToEarnings</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">PriceToBook</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">Price / Book (%)</xsl:with-param>
            <xsl:with-param name="ItemName">PriceToBook</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">DivYield</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">Dividend Yield (%)</xsl:with-param>
            <xsl:with-param name="ItemName">DividendYield</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

      </table>
    </td>
  </tr>
</xsl:when>





<xsl:when test="$GAAPDomain = $INSURANCE_UNDERWRITER or $GAAPDomain = $INSURANCE_BROKER">
  <tr class="colorlight">
    <td class="colorlight" COLSPAN="3">
      <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2">
        <tr valign="BOTTOM" class="header">
          <td colspan="2" NOWRAP="1" class="header">Performance Ratios (%)</td>
          <td align="RIGHT" class="header">
          <xsl:if test="not(normalize-space(Company/Ticker))">
    <xsl:value-of select="Company/InstnName"/>
          </xsl:if>
          <xsl:value-of select="Company/Ticker"/></td>
          <td align="RIGHT" class="header" nowrap="1">Peer<br/>Median</td>
          <td align="RIGHT" class="header" nowrap="1">Peer<br/>Average</td>
        </tr>
      <xsl:call-template name="peerdatarow">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">ROAA</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName">ROAA</xsl:with-param>
          <xsl:with-param name="ItemName">ROAA</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">ROAAInsuranceOperating</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName">Operating ROAA</xsl:with-param>
          <xsl:with-param name="ItemName">ROAAInsuranceOperating</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

          <xsl:call-template name="peerdatarow">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">ROAE</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName">ROAE</xsl:with-param>
          <xsl:with-param name="ItemName">ROAE</xsl:with-param>
          <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

      <xsl:call-template name="peerdatarow">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">ROAEInsuranceOperating</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName">Operating ROAE</xsl:with-param>
          <xsl:with-param name="ItemName">ROAEInsuranceOperating</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">InvestmentPortfolioYield</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName">Investment Yield</xsl:with-param>
          <xsl:with-param name="ItemName">InvestmentPortfolioYield</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">OperatingMarginInsurance</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName">Operating Margin</xsl:with-param>
          <xsl:with-param name="ItemName">OperatingMarginInsurance</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">EPSAfterExtraGrowth</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName">EPS Growth</xsl:with-param>
          <xsl:with-param name="ItemName">EPSAfterExtraGrowth</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">EPSOperatingGrowth</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName">Operating EPS Growth</xsl:with-param>
          <xsl:with-param name="ItemName">EPSOperatingGrowth</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

      </table>
    </td>
  </tr>
  <tr class="colorlight">
      <td class="colorlight" COLSPAN="3">
        <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2">
          <tr valign="BOTTOM" class="header">
            <td colspan="2" NOWRAP="1" class="header">Balance Sheet Ratios (%)</td>
          <td align="RIGHT" class="header">&#160;</td>
          <td align="RIGHT" class="header" nowrap="1">&#160;</td>
          <td align="RIGHT" class="header" nowrap="1">&#160;</td>
        </tr>
  <xsl:call-template name="peerdatarow">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">CashAndInvestmentsToAssets</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName">Cash &amp; Investments / Assets</xsl:with-param>
      <xsl:with-param name="ItemName">CashAndInvestmentsToAssets</xsl:with-param>
  <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
  <xsl:call-template name="peerdatarow">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">PolicyReservesToEquity</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName">Policy Reserves / Equity (x)</xsl:with-param>
      <xsl:with-param name="ItemName">PolicyReservesToEquity</xsl:with-param>
  <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
  <xsl:call-template name="peerdatarow">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">DebtAndRedeemablePfdToEquity</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName">Debt plus Redeemable Preferred / Equity (x)</xsl:with-param>
      <xsl:with-param name="ItemName">DebtAndRedeemablePfdToEquity</xsl:with-param>
  <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
  <xsl:call-template name="peerdatarow">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">DebtToTotalCapBookValue</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName">Debt / Total Cap, at Book</xsl:with-param>
      <xsl:with-param name="ItemName">DebtToTotalCapBookValue</xsl:with-param>
  <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
  <xsl:call-template name="peerdatarow">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">TangibleEquityToAssets</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName">Tangible Equity / Tangible Assets</xsl:with-param>
      <xsl:with-param name="ItemName">TangibleEquityToAssets</xsl:with-param>
  <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
  <xsl:call-template name="peerdatarow">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">EquityToAssets</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName">Total Equity / Total Assets</xsl:with-param>
      <xsl:with-param name="ItemName">EquityToAssets</xsl:with-param>
  <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
  <xsl:call-template name="peerdatarow">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">TangibleCommonEquityToAssets</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName">Tangible Common Equity / Tangible Assets</xsl:with-param>
      <xsl:with-param name="ItemName">TangibleCommonEquityToAssets</xsl:with-param>
  <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        </table>
      </td>
  </tr>
    <tr class="colorlight">
        <td class="colorlight" COLSPAN="3">
          <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2">
            <tr valign="BOTTOM" class="header">
              <td colspan="2" NOWRAP="1" class="header">Market Ratios</td>
          <td align="RIGHT" class="header">&#160;</td>
          <td align="RIGHT" class="header" nowrap="1">&#160;</td>
          <td align="RIGHT" class="header" nowrap="1">&#160;</td>
        </tr>
  <xsl:call-template name="peerdatarow">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">PriceToEarnings</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName">Price / Earnings (x)</xsl:with-param>
      <xsl:with-param name="ItemName">PriceToEarnings</xsl:with-param>
  <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
  <xsl:call-template name="peerdatarow">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">PriceToOperatingEPS</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName">Price / Operating EPS (x)</xsl:with-param>
      <xsl:with-param name="ItemName">PriceToOperatingEPS</xsl:with-param>
  <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
  <xsl:call-template name="peerdatarow">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">PriceToBook</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName">Price / Book (%)</xsl:with-param>
      <xsl:with-param name="ItemName">PriceToBook</xsl:with-param>
  <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
  <xsl:call-template name="peerdatarow">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">DivYield</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName">Dividend Yield (%)</xsl:with-param>
      <xsl:with-param name="ItemName">DividendYield</xsl:with-param>
  <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
          </table>
        </td>
  </tr>

</xsl:when>







<xsl:when test="$GAAPDomain = $REIT">
 <tr class="colorlight">
    <td class="colorlight" COLSPAN="3">
      <table border="0" cellspacing="0" cellpadding="2" width="100%">
        <tr valign="BOTTOM" class="header">
          <td colspan="2" NOWRAP="1" class="header">Financial Performance (%)</td>
          <td align="RIGHT" class="header">
<xsl:if test="not(normalize-space(Company/Ticker))">
<xsl:value-of select="Company/InstnName"/>
</xsl:if>

          <xsl:value-of select="Company/Ticker"/></td>
          <td align="RIGHT" class="header" nowrap="1">Peer<br/>Median</td>
          <td align="RIGHT" class="header" nowrap="1">Peer<br/>Average</td>
        </tr>



    <xsl:call-template name="peerdatarow">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">EBITDACoverageRecurringRealEst</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Rec. EBITDA / Int. Exp. (x)</xsl:with-param>
        <xsl:with-param name="ItemName">EBITDACoverageRecurringRealEst</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>


    <xsl:call-template name="peerdatarow">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">EBITDACoverageRecurringPFDReal</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Rec. EBITDA / Int. Exp. + Pref. Divs. (x)</xsl:with-param>
        <xsl:with-param name="ItemName">EBITDACoverageRecurringPFDReal</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

    <xsl:call-template name="peerdatarow">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">GAndANominalToRevenueRE</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">G&amp;A / Total Revenue</xsl:with-param>
        <xsl:with-param name="ItemName">GAndANominalToRevenueRE</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

      <xsl:call-template name="peerdatarow">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">FFOPerShareGrowth</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">FFO / Share Growth</xsl:with-param>
        <xsl:with-param name="ItemName">FFOPerShareGrowth</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

        <xsl:call-template name="peerdatarow">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">ROAE</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">ROAE</xsl:with-param>
        <xsl:with-param name="ItemName">ROAE</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

        <xsl:call-template name="peerdatarow">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">ROAA</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">ROAA</xsl:with-param>
        <xsl:with-param name="ItemName">ROAA</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

    <xsl:call-template name="peerdatarow">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">DividendPayoutFFO</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Payout / FFO</xsl:with-param>
        <xsl:with-param name="ItemName">DividendPayoutFFO</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
           </table>
    </td>
  </tr>
  <tr class="colorlight">
      <td class="colorlight" COLSPAN="3">
        <table border="0" cellspacing="0" cellpadding="2" width="100%">
          <tr valign="BOTTOM" class="header">
            <td colspan="2" NOWRAP="1" class="header">Market Performance</td>
          <td align="RIGHT" class="header">&#160;</td>
          <td align="RIGHT" class="header" nowrap="1">&#160;</td>
          <td align="RIGHT" class="header" nowrap="1">&#160;</td>
        </tr>
                <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">PriceToFFO</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Price / FFO (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">PriceToFFO</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">DivYield</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Dividend Yield (%)</xsl:with-param>
                  <xsl:with-param name="ItemName">DividendYield</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">UPREITMarketCap</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">UPREIT Market Cap ($M)</xsl:with-param>
                  <xsl:with-param name="ItemName">UPREITMarketCap</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">TotalCap</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Total Capitalization ($M)</xsl:with-param>
                  <xsl:with-param name="ItemName">TotalCap</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
          </table>
      </td>
  </tr>
</xsl:when>





<xsl:when test="$GAAPDomain = $ENERGY or $GAAPDomain = $GAS">
 <tr class="colorlight">
     <td class="colorlight" COLSPAN="3">
       <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2">
         <tr valign="BOTTOM" class="header">
           <td colspan="2" NOWRAP="1" class="header">Financial Performance</td>
           <td align="RIGHT" class="header">
           <xsl:if test="not(normalize-space(Company/Ticker))">
     <xsl:value-of select="Company/InstnName"/>
</xsl:if>

           <xsl:value-of select="Company/Ticker"/></td>
           <td align="RIGHT" class="header" nowrap="1">Peer<br/>Median</td>
           <td align="RIGHT" class="header" nowrap="1">Peer<br/>Average</td>
         </tr>
              <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">EBITDAToIncome</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">EBITDA / Income (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">EBITDAToIncome</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">EBITDAAdjEnergyToInterest</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">EBITDA / Interest Exp (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">EBITDAAdjEnergyToInterest</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                  <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">DebtToEquity</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Debt / Equity (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">DebtToEquity</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                  <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">AFUDCToNetIncomeBasic</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">AFUDC / Basic Net Income (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">AFUDCToNetIncomeBasic</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                  <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">ROAE</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">ROAE (%)</xsl:with-param>
                  <xsl:with-param name="ItemName">ROAE</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                  <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">ROAA</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">ROAA (%)</xsl:with-param>
                  <xsl:with-param name="ItemName">ROAA</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
       </table>
     </td>
   </tr>
   <tr class="colorlight">
       <td class="colorlight" COLSPAN="3">
         <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2">
           <tr valign="BOTTOM" class="header">
             <td colspan="2" NOWRAP="1" class="header">Operating Performance</td>
           <td align="RIGHT" class="header">&#160;</td>
           <td align="RIGHT" class="header" nowrap="1">&#160;</td>
           <td align="RIGHT" class="header" nowrap="1">&#160;</td>
         </tr>
                <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">CustomersElectric</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">No. Electric Customers</xsl:with-param>
                  <xsl:with-param name="ItemName">CustomersElectric</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0</xsl:with-param></xsl:call-template>
                  <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">CustomersGas</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">No. Gas Customers</xsl:with-param>
                  <xsl:with-param name="ItemName">CustomersGas</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0</xsl:with-param></xsl:call-template>
                  <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">GasThroughput</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Gas Throughput (MMcf)</xsl:with-param>
                  <xsl:with-param name="ItemName">GasThroughput</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0</xsl:with-param></xsl:call-template>

        </table>
      </td>
   </tr>

</xsl:when>

<xsl:when test="$GAAPDomain = $INVESTMENT_COMPANY">
  <tr class="colorlight">
      <td class="colorlight" COLSPAN="3">
        <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2">
          <tr valign="BOTTOM" class="header">
            <td colspan="2" NOWRAP="1" class="header">Performance Ratios (%)</td>
            <td align="RIGHT" class="header">
            <xsl:if test="not(normalize-space(Company/Ticker))"><xsl:value-of select="Company/InstnName"/></xsl:if>
            <xsl:value-of select="Company/Ticker"/></td>
            <td align="RIGHT" class="header" nowrap="1">Peer<br/>Median</td>
            <td align="RIGHT" class="header" nowrap="1">Peer<br/>Average</td>
          </tr>

                    <xsl:call-template name="peerdatarow">
                    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                    <xsl:with-param name="SNLItemName">ROAA</xsl:with-param>
                    <xsl:with-param name="EOP">0</xsl:with-param>
                    <xsl:with-param name="DisplayName">ROAA</xsl:with-param>
                    <xsl:with-param name="ItemName">ROAA</xsl:with-param>
                    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

                    <xsl:call-template name="peerdatarow">
                    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                    <xsl:with-param name="SNLItemName">ROAE</xsl:with-param>
                    <xsl:with-param name="EOP">0</xsl:with-param>
                    <xsl:with-param name="DisplayName">ROAE</xsl:with-param>
                    <xsl:with-param name="ItemName">ROAE</xsl:with-param>
                    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

                    <xsl:call-template name="peerdatarow">
                    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                    <xsl:with-param name="SNLItemName">NetInterestMargin</xsl:with-param>
                    <xsl:with-param name="EOP">0</xsl:with-param>
                    <xsl:with-param name="DisplayName">Net Interest Margin</xsl:with-param>
                    <xsl:with-param name="ItemName">NetInterestMargin</xsl:with-param>
                    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

                <xsl:call-template name="peerdatarow">
                    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                    <xsl:with-param name="SNLItemName">EBITDACoverage</xsl:with-param>
                    <xsl:with-param name="EOP">0</xsl:with-param>
                    <xsl:with-param name="DisplayName">EBITDA / Interest Expense (x)</xsl:with-param>
                    <xsl:with-param name="ItemName">EBITDACoverage</xsl:with-param>
                <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                <xsl:call-template name="peerdatarow">
                    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                    <xsl:with-param name="SNLItemName">AssetGrowth</xsl:with-param>
                    <xsl:with-param name="EOP">0</xsl:with-param>
                    <xsl:with-param name="DisplayName">Asset Growth</xsl:with-param>
                    <xsl:with-param name="ItemName">AssetGrowth</xsl:with-param>
                <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                <xsl:call-template name="peerdatarow">
                    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                    <xsl:with-param name="SNLItemName">NetIncomeGrowth</xsl:with-param>
                    <xsl:with-param name="EOP">0</xsl:with-param>
                    <xsl:with-param name="DisplayName">Net Income Growth</xsl:with-param>
                    <xsl:with-param name="ItemName">NetIncomeGrowth</xsl:with-param>
                <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        </table>
      </td>
    </tr>
    <tr class="colorlight">
        <td class="colorlight" COLSPAN="3">
          <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2">
            <tr valign="BOTTOM" class="header">
              <td colspan="2" NOWRAP="1" class="header">Balance Sheet Ratios (%)</td>
            <td align="RIGHT" class="header">&#160;</td>
            <td align="RIGHT" class="header" nowrap="1">&#160;</td>
            <td align="RIGHT" class="header" nowrap="1">&#160;</td>
          </tr>
                <xsl:call-template name="peerdatarow">
                    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                    <xsl:with-param name="SNLItemName">EquityToAssets</xsl:with-param>
                    <xsl:with-param name="EOP">0</xsl:with-param>
                    <xsl:with-param name="DisplayName">Total Equity / Total Assets</xsl:with-param>
                    <xsl:with-param name="ItemName">EquityToAssets</xsl:with-param>
                <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                <xsl:call-template name="peerdatarow">
                    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                    <xsl:with-param name="SNLItemName">DebtToEquity</xsl:with-param>
                    <xsl:with-param name="EOP">0</xsl:with-param>
                    <xsl:with-param name="DisplayName">Total Debt / Total Equity (x)</xsl:with-param>
                    <xsl:with-param name="ItemName">DebtToEquity</xsl:with-param>
                <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                <xsl:call-template name="peerdatarow">
                    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                    <xsl:with-param name="SNLItemName">DebtToTotalCapBookValue</xsl:with-param>
                    <xsl:with-param name="EOP">0</xsl:with-param>
                    <xsl:with-param name="DisplayName">Debt / Total Cap, at Book</xsl:with-param>
                    <xsl:with-param name="ItemName">DebtToTotalCapBookValue</xsl:with-param>
                <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                <xsl:call-template name="peerdatarow">
                    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                    <xsl:with-param name="SNLItemName">InvestmentsInvestmentCoToAsset</xsl:with-param>
                    <xsl:with-param name="EOP">0</xsl:with-param>
                    <xsl:with-param name="DisplayName">Investments / Assets</xsl:with-param>
                    <xsl:with-param name="ItemName">InvestmentsInvestmentCoToAsset</xsl:with-param>
                <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

          </table>
        </td>
    </tr>
        <tr class="colorlight">
            <td class="colorlight" COLSPAN="3">
              <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2">
                <tr valign="BOTTOM" class="header">
                  <td colspan="2" NOWRAP="1" class="header">Market Ratios</td>
            <td align="RIGHT" class="header">&#160;</td>
            <td align="RIGHT" class="header" nowrap="1">&#160;</td>
            <td align="RIGHT" class="header" nowrap="1">&#160;</td>
          </tr>
                <xsl:call-template name="peerdatarow">
                    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                    <xsl:with-param name="SNLItemName">PriceToBook</xsl:with-param>
                    <xsl:with-param name="EOP">0</xsl:with-param>
                    <xsl:with-param name="DisplayName">Price / Book (%)</xsl:with-param>
                    <xsl:with-param name="ItemName">PriceToBook</xsl:with-param>
                <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                <xsl:call-template name="peerdatarow">
                    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                    <xsl:with-param name="SNLItemName">PriceToEarnings</xsl:with-param>
                    <xsl:with-param name="EOP">0</xsl:with-param>
                    <xsl:with-param name="DisplayName">Price / Earnings (x)</xsl:with-param>
                    <xsl:with-param name="ItemName">PriceToEarnings</xsl:with-param>
                <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                <xsl:call-template name="peerdatarow">
                    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                    <xsl:with-param name="SNLItemName">DivYield</xsl:with-param>
                    <xsl:with-param name="EOP">0</xsl:with-param>
                    <xsl:with-param name="DisplayName">Dividend Yield (%)</xsl:with-param>
                    <xsl:with-param name="ItemName">DividendYield</xsl:with-param>
                <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              </table>
            </td>
  </tr>
</xsl:when>

<xsl:when test="$GAAPDomain = $SPECIALTY_LENDER">
<tr class="colorlight">
    <td class="colorlight" COLSPAN="3">
      <table border="0" cellspacing="0" cellpadding="2" width="100%">
        <tr valign="BOTTOM" class="header">
          <td colspan="2" NOWRAP="1" class="header">Performance Ratios (%)</td>
          <td align="RIGHT" class="header">
            <xsl:if test="not(normalize-space(Company/Ticker))">
              <xsl:value-of select="Company/InstnName"/>
            </xsl:if>
            <xsl:value-of select="Company/Ticker"/>
          </td>
          <td align="RIGHT" class="header" nowrap="1">Peer<br/>Median</td>
          <td align="RIGHT" class="header" nowrap="1">Peer<br/>Average</td>
        </tr>


                  <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">ROAA</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">ROAA</xsl:with-param>
                  <xsl:with-param name="ItemName">ROAA</xsl:with-param>
                  <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

                  <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">ROAE</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">ROAE</xsl:with-param>
                  <xsl:with-param name="ItemName">ROAE</xsl:with-param>
                  <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

              <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">ROAUM</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Return on Avg. AUM</xsl:with-param>
                  <xsl:with-param name="ItemName">ROAUM</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">AssetGrowth</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Asset Growth</xsl:with-param>
                  <xsl:with-param name="ItemName">AssetGrowth</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">NetIncomeGrowth</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Net Income Growth</xsl:with-param>
                  <xsl:with-param name="ItemName">NetIncomeGrowth</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">AssetsUnderMgmtGrowth</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Assets Managed Growth</xsl:with-param>
                  <xsl:with-param name="ItemName">AssetsUnderMgmtGrowth</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">EPSGrowth</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">EPS Growth</xsl:with-param>
                  <xsl:with-param name="ItemName">EPSGrowth</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      </table>
    </td>
  </tr>
  <tr class="colorlight">
      <td class="colorlight" COLSPAN="3">
        <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2">
          <tr valign="BOTTOM" class="header">
            <td colspan="2" NOWRAP="1" class="header">Balance Sheet Ratios (%)</td>
          <td align="RIGHT" class="header">&#160;</td>
          <td align="RIGHT" class="header" nowrap="1">&#160;</td>
          <td align="RIGHT" class="header" nowrap="1">&#160;</td>
        </tr>
              <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">EquityToAssets</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Total Equity / Total Assets</xsl:with-param>
                  <xsl:with-param name="ItemName">EquityToAssets</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">TangibleEquityToAssets</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Tangible Equity / Total Assets</xsl:with-param>
                  <xsl:with-param name="ItemName">TangibleEquityToAssets</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">DebtToEquity</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Debt / Equity (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">DebtToEquity</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">CashAndInvestmentsToAssetsSI</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Cash &amp; Investments / Assets</xsl:with-param>
                  <xsl:with-param name="ItemName">CashAndInvestmentsToAssetsSI</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">CashAndInvestmentsToAssetsSI</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Clearing Fees &amp; Commissions / Revenue</xsl:with-param>
                  <xsl:with-param name="ItemName">ClearingRevenueToRevenue</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

        </table>
      </td>
  </tr>
  <tr class="colorlight">
      <td class="colorlight" COLSPAN="3">
        <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2">
          <tr valign="BOTTOM" class="header">
            <td colspan="2" NOWRAP="1" class="header">Income Statement Ratios (%)</td>
          <td align="RIGHT" class="header">&#160;</td>
          <td align="RIGHT" class="header" nowrap="1">&#160;</td>
          <td align="RIGHT" class="header" nowrap="1">&#160;</td>
        </tr>


              <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">CashAndInvestmentsToAssetsSI</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Clearing Fees &amp; Commissions / Revenue</xsl:with-param>
                  <xsl:with-param name="ItemName">ClearingRevenueToRevenue</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">InvestmentBankingToRevenue</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Investment Bank Fees / Total Revenue</xsl:with-param>
                  <xsl:with-param name="ItemName">InvestmentBankingToRevenue</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">AssetMgmtFeeToRevenueSI</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Total Asset Mgmt Fees / Total Revenue</xsl:with-param>
                  <xsl:with-param name="ItemName">AssetMgmtFeeToRevenueSI</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">PortfolioRevenueToRevenueSI</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Portfolio Revenue / Total Revenue</xsl:with-param>
                  <xsl:with-param name="ItemName">PortfolioRevenueToRevenueSI</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">NetIncomeToRevenueSAndI</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Net Income / Total Revenue</xsl:with-param>
                  <xsl:with-param name="ItemName">NetIncomeToRevenueSAndI</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">EBITDAToIncome</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">EBITDA / Pre-Tax Income (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">EBITDAToIncome</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        </table>
      </td>
  </tr>
  <tr class="colorlight">
      <td class="colorlight" COLSPAN="3">
        <table border="0" cellspacing="0" cellpadding="2" width="100%">
          <tr valign="BOTTOM" class="header">
            <td colspan="2" NOWRAP="1" class="header">Market Performance</td>
          <td align="RIGHT" class="header">&#160;</td>
          <td align="RIGHT" class="header" nowrap="1">&#160;</td>
          <td align="RIGHT" class="header" nowrap="1">&#160;</td>
        </tr>
              <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">PriceToBook</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Price / Book (%)</xsl:with-param>
                  <xsl:with-param name="ItemName">PriceToBook</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">PriceToEarnings</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Price / Earnings (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">PriceToEarnings</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">DivYield</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Dividend Yield (%)</xsl:with-param>
                  <xsl:with-param name="ItemName">DividendYield</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
          </table>
      </td>
  </tr>

</xsl:when>




<xsl:when test="$GAAPDomain = $BROKER_DEALER or $GAAPDomain = $INVESTMENT_ADVISOR">
  <tr class="colorlight">
    <td class="colorlight" COLSPAN="3">
      <table border="0" cellspacing="0" cellpadding="2" width="100%">
        <tr valign="BOTTOM" class="header">
          <td colspan="2" NOWRAP="1" class="header">Performance Ratios (%)</td>
          <td align="RIGHT" class="header">
            <xsl:if test="not(normalize-space(Company/Ticker))">
              <xsl:value-of select="Company/InstnName"/>
            </xsl:if>
            <xsl:value-of select="Company/Ticker"/>
          </td>
          <td align="RIGHT" class="header" nowrap="1">Peer<br/>Median</td>
          <td align="RIGHT" class="header" nowrap="1">Peer<br/>Average</td>
        </tr>

        <xsl:call-template name="peerdatarow">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">ROAA</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">ROAA</xsl:with-param>
        <xsl:with-param name="ItemName">ROAA</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

        <xsl:call-template name="peerdatarow">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">ROAE</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">ROAE</xsl:with-param>
        <xsl:with-param name="ItemName">ROAE</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

        <xsl:call-template name="peerdatarow">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">ROAUM</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Return on Avg. AUM</xsl:with-param>
        <xsl:with-param name="ItemName">ROAUM</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <!--<xsl:call-template name="peerdatarow">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">ROAUM</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Return on Avg. AUM</xsl:with-param>
        <xsl:with-param name="ItemName">ROAUM</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>-->
        <xsl:call-template name="peerdatarow">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">AssetGrowth</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Asset Growth</xsl:with-param>
        <xsl:with-param name="ItemName">AssetGrowth</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">NetIncomeGrowth</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Net Income Growth</xsl:with-param>
        <xsl:with-param name="ItemName">NetIncomeGrowth</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">AssetsUnderMgmtGrowth</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Assets Managed Growth</xsl:with-param>
        <xsl:with-param name="ItemName">AssetsUnderMgmtGrowth</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">EPSGrowth</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">EPS Growth</xsl:with-param>
        <xsl:with-param name="ItemName">EPSGrowth</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
            </table>
        </td>
    </tr>
  <tr class="colorlight">
    <td class="colorlight" COLSPAN="3">
      <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2">
        <tr valign="BOTTOM" class="header">
          <td colspan="2" NOWRAP="1" class="header">Balance Sheet Ratios (%)</td>
          <td align="RIGHT" class="header">&#160;</td>
          <td align="RIGHT" class="header" nowrap="1">&#160;</td>
          <td align="RIGHT" class="header" nowrap="1">&#160;</td>
        </tr>
        <xsl:call-template name="peerdatarow">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">EquityToAssets</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">Total Equity / Total Assets</xsl:with-param>
            <xsl:with-param name="ItemName">EquityToAssets</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">TangibleEquityToAssets</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">Tangible Equity / Total Assets</xsl:with-param>
            <xsl:with-param name="ItemName">TangibleEquityToAssets</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">DebtToEquity</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">Debt / Equity (x)</xsl:with-param>
            <xsl:with-param name="ItemName">DebtToEquity</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">CashAndInvestmentsToAssetsSI</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">Cash &amp; Investments / Assets</xsl:with-param>
            <xsl:with-param name="ItemName">CashAndInvestmentsToAssetsSI</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">ClearingRevenueToRevenue</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">Clearing Fees &amp; Commissions / Revenue</xsl:with-param>
            <xsl:with-param name="ItemName">ClearingRevenueToRevenue</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        </table>
      </td>
  </tr>
  <tr class="colorlight">
      <td class="colorlight" COLSPAN="3">
        <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2">
          <tr valign="BOTTOM" class="header">
            <td colspan="2" NOWRAP="1" class="header">Income Statement Ratios (%)</td>
          <td align="RIGHT" class="header">&#160;</td>
          <td align="RIGHT" class="header" nowrap="1">&#160;</td>
          <td align="RIGHT" class="header" nowrap="1">&#160;</td>
        </tr>
                    <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">ClearingRevenueToRevenue</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Clearing Fees &amp; Commissions / Revenue</xsl:with-param>
                  <xsl:with-param name="ItemName">ClearingRevenueToRevenue</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">InvestmentBankingToRevenue</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Investment Bank Fees / Total Revenue</xsl:with-param>
                  <xsl:with-param name="ItemName">InvestmentBankingToRevenue</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">AssetMgmtFeeToRevenueSI</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Total Asset Mgmt Fees / Total Revenue</xsl:with-param>
                  <xsl:with-param name="ItemName">AssetMgmtFeeToRevenueSI</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">PortfolioRevenueToRevenueSI</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Portfolio Revenue / Total Revenue</xsl:with-param>
                  <xsl:with-param name="ItemName">PortfolioRevenueToRevenueSI</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">NetIncomeToRevenueSAndI</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Net Income / Total Revenue</xsl:with-param>
                  <xsl:with-param name="ItemName">NetIncomeToRevenueSAndI</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">EBITDAToIncome</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">EBITDA / Pre-Tax Income (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">EBITDAToIncome</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        </table>
      </td>
  </tr>
  <tr class="colorlight">
      <td class="colorlight" COLSPAN="3">
        <table border="0" cellspacing="0" cellpadding="2" width="100%">
          <tr valign="BOTTOM" class="header">
            <td colspan="2" NOWRAP="1" class="header">Market Performance</td>
          <td align="RIGHT" class="header">&#160;</td>
          <td align="RIGHT" class="header" nowrap="1">&#160;</td>
          <td align="RIGHT" class="header" nowrap="1">&#160;</td>
        </tr>
                    <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">PriceToBook</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Price / Book (%)</xsl:with-param>
                  <xsl:with-param name="ItemName">PriceToBook</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">PriceToEarnings</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Price / Earnings (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">PriceToEarnings</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">DivYield</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Dividend Yield (%)</xsl:with-param>
                  <xsl:with-param name="ItemName">DividendYield</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
          </table>
      </td>
  </tr>
</xsl:when>





<xsl:when test="$GAAPDomain = $FINTECH">
  <TR class="colorlight">
    <TD class="colorlight" COLSPAN="3">
      <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2">
        <tr valign="BOTTOM" class="header">
          <td colspan="2" NOWRAP="1" class="header">Financial Performance (%)</td>
          <td align="RIGHT" class="header">
            <xsl:if test="not(normalize-space(Company/Ticker))">
              <xsl:value-of select="Company/InstnName"/>
            </xsl:if>
            <xsl:value-of select="Company/Ticker"/>
          </td>
          <td align="RIGHT" class="header" nowrap="1">
            Peer<br/>Median
          </td>
          <td align="RIGHT" class="header" nowrap="1">Peer<br/>Average</td>
        </tr>
                  <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">ROAA</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">ROAA</xsl:with-param>
                  <xsl:with-param name="ItemName">ROAA</xsl:with-param>
                  <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                  <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">ROAE</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">ROAE</xsl:with-param>
                  <xsl:with-param name="ItemName">ROAE</xsl:with-param>
                  <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">GrossMarginTech</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Gross Sales Margin</xsl:with-param>
                  <xsl:with-param name="ItemName">GrossMarginTech</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">OperatingMarginTech</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Operating Sales Margin</xsl:with-param>
                  <xsl:with-param name="ItemName">OperatingMarginTech</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">SalesTechPerEmployee</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Sales/ Employee ($000)</xsl:with-param>
                  <xsl:with-param name="ItemName">SalesTechPerEmployee</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">EBITDACoverage</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">EBITDA/ Interest Expense (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">EBITDACoverage</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">EBITDAToIncome</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">EBITDA/ Pre-tax Earnings (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">EBITDAToIncome</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

      </table>
    </TD>
  </TR>
  <TR class="colorlight">
      <TD class="colorlight" COLSPAN="3">
        <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2">
          <tr valign="BOTTOM" class="header">
            <td colspan="2" NOWRAP="1" class="header">Balance Sheet Ratios (%)</td>
          <td align="RIGHT" class="header">&#160;</td>
          <td align="RIGHT" class="header" nowrap="1">&#160;</td>
          <td align="RIGHT" class="header" nowrap="1">&#160;</td>
        </tr>

                    <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">EquityToAssets</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Total Equity / Total Assets</xsl:with-param>
                  <xsl:with-param name="ItemName">EquityToAssets</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">DebtToEquity</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Debt / Equity (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">DebtToEquity</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">CurrentRatio</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Current Ratio (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">CurrentRatio</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">IntangiblesToEquity</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Intangibles/ Equity</xsl:with-param>
                  <xsl:with-param name="ItemName">IntangiblesToEquity</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        </table>
      </TD>
  </TR>
    <TR class="colorlight">
        <TD class="colorlight" COLSPAN="3">
          <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2">
            <tr valign="BOTTOM" class="header">
              <td colspan="2" NOWRAP="1" class="header">Market Ratios</td>
          <td align="RIGHT" class="header">&#160;</td>
          <td align="RIGHT" class="header" nowrap="1">&#160;</td>
          <td align="RIGHT" class="header" nowrap="1">&#160;</td>
        </tr>
                    <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">PriceToEarningsLTMAfterExtra</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Price / Earnings (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">PriceToEarningsLTMAfterExtra</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">PriceToBook</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Price / Book (%)</xsl:with-param>
                  <xsl:with-param name="ItemName">PriceToBook</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">DivYield</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Dividend Yield (%)</xsl:with-param>
                  <xsl:with-param name="ItemName">DivYield</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
          </table>
        </TD>
  </TR>

</xsl:when>




<xsl:when test="$GAAPDomain = $HOMEBUILDER">
  <TR class="colorlight">
    <TD class="colorlight" COLSPAN="3">
      <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2">
        <tr valign="BOTTOM" class="header">
          <td colspan="2" NOWRAP="1" class="header">Financial Performance</td>
          <td align="RIGHT" class="header">
            <xsl:if test="not(normalize-space(Company/Ticker))">
              <xsl:value-of select="Company/InstnName"/>
            </xsl:if>
            <xsl:value-of select="Company/Ticker"/>
          </td>
          <td align="RIGHT" class="header" nowrap="1">Peer<br/>Median</td>
          <td align="RIGHT" class="header" nowrap="1">Peer<br/>Average</td>
        </tr>
      <xsl:call-template name="peerdatarow">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">ROAA</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName">ROAA</xsl:with-param>
      <xsl:with-param name="ItemName">ROAA</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

      <xsl:call-template name="peerdatarow">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">ROAE</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName">ROAE</xsl:with-param>
      <xsl:with-param name="ItemName">ROAE</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

      <xsl:call-template name="peerdatarow">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">GrossMarginBuilder</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName">Gross Margin</xsl:with-param>
      <xsl:with-param name="ItemName">GrossMarginBuilder</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

      <xsl:call-template name="peerdatarow">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">OperatingMarginBuilderBldg</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName">Construction Margin</xsl:with-param>
      <xsl:with-param name="ItemName">OperatingMarginBuilderBldg</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

      <xsl:call-template name="peerdatarow">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">OperatingMarginBuilderFinl</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName">Financial Services Margin</xsl:with-param>
      <xsl:with-param name="ItemName">OperatingMarginBuilderFinl</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

      <xsl:call-template name="peerdatarow">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">OperatingMarginBuilder</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName">Total Operating Margin</xsl:with-param>
      <xsl:with-param name="ItemName">OperatingMarginBuilder</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

      </table>
    </TD>
  </TR>
  <TR class="colorlight">
      <TD class="colorlight" COLSPAN="3">
        <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2">
          <tr valign="BOTTOM" class="header">
            <td colspan="2" NOWRAP="1" class="header">Homebuilding Growth Ratios (%)</td>
          <td align="RIGHT" class="header">&#160;</td>
          <td align="RIGHT" class="header" nowrap="1">&#160;</td>
          <td align="RIGHT" class="header" nowrap="1">&#160;</td>
        </tr>

      <xsl:call-template name="peerdatarow">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">SalesBuilderGrowth</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName">Construction Sales Growth</xsl:with-param>
      <xsl:with-param name="ItemName">SalesBuilderGrowth</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

      <xsl:call-template name="peerdatarow">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">SalesBuilderNetGrowth</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName">Construction Gross Profit Growth</xsl:with-param>
      <xsl:with-param name="ItemName">SalesBuilderNetGrowth</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">ConstructionOpEarningsGrowth</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName">Construction Operating Earnings Growth</xsl:with-param>
      <xsl:with-param name="ItemName">ConstructionOpEarningsGrowth</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

      <xsl:call-template name="peerdatarow">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">BuilderHomesGrowth</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName"># of Homes Sold Growth</xsl:with-param>
      <xsl:with-param name="ItemName">BuilderHomesGrowth</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

      <xsl:call-template name="peerdatarow">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">BuilderHomeSalePriceGrowth</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName">Change in Average Sale Price</xsl:with-param>
      <xsl:with-param name="ItemName">BuilderHomeSalePriceGrowth</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

      <xsl:call-template name="peerdatarow">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">BuilderLotsGrowth</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName">Total Lots Growth</xsl:with-param>
      <xsl:with-param name="ItemName">BuilderLotsGrowth</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        </table>
      </TD>
  </TR>
    <TR class="colorlight">
        <TD class="colorlight" COLSPAN="3">
          <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2">
            <tr valign="BOTTOM" class="header">
              <td colspan="2" NOWRAP="1" class="header">Capitalization</td>
          <td align="RIGHT" class="header">&#160;</td>
          <td align="RIGHT" class="header" nowrap="1">&#160;</td>
          <td align="RIGHT" class="header" nowrap="1">&#160;</td>
        </tr>
      <xsl:call-template name="peerdatarow">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">DebtToTotalCapBookValue</xsl:with-param>
        <xsl:with-param name="EOP">1</xsl:with-param>
        <xsl:with-param name="DisplayName">Debt/ Book Cap</xsl:with-param>
        <xsl:with-param name="ItemName">DebtToTotalCapBookValue</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">DebtTototalCapAdjLessCash</xsl:with-param>
        <xsl:with-param name="EOP">1</xsl:with-param>
        <xsl:with-param name="DisplayName">Net/Debt/ Net Cap</xsl:with-param>
        <xsl:with-param name="ItemName">DebtTototalCapAdjLessCash</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">DebtSeniorToDebt</xsl:with-param>
        <xsl:with-param name="EOP">1</xsl:with-param>
        <xsl:with-param name="DisplayName">Senior Debt/ Debt</xsl:with-param>
        <xsl:with-param name="ItemName">DebtSeniorToDebt</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">DebtAndSubDebtBldgBldgToDebt</xsl:with-param>
        <xsl:with-param name="EOP">1</xsl:with-param>
        <xsl:with-param name="DisplayName">Homebuilding Debt/ Debt</xsl:with-param>
        <xsl:with-param name="ItemName">DebtAndSubDebtBldgBldgToDebt</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">DebtAndSubDebtBldgFinlToDebt</xsl:with-param>
        <xsl:with-param name="EOP">1</xsl:with-param>
        <xsl:with-param name="DisplayName">Financial Services Debt/ Debt</xsl:with-param>
        <xsl:with-param name="ItemName">DebtAndSubDebtBldgFinlToDebt</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">EquityToAssets</xsl:with-param>
        <xsl:with-param name="EOP">1</xsl:with-param>
        <xsl:with-param name="DisplayName">Equity/ Assets</xsl:with-param>
        <xsl:with-param name="ItemName">EquityToAssets</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
          </table>
        </TD>
  </TR>

    <TR class="colorlight">
        <TD class="colorlight" COLSPAN="3">
          <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2">
            <tr valign="BOTTOM" class="header">
              <td colspan="2" NOWRAP="1" class="header">Market Ratios</td>
          <td align="RIGHT" class="header">&#160;</td>
          <td align="RIGHT" class="header" nowrap="1">&#160;</td>
          <td align="RIGHT" class="header" nowrap="1">&#160;</td>
        </tr>
      <xsl:call-template name="peerdatarow">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">PriceToEarningsBeforeExtra</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Price/ Earnings</xsl:with-param>
        <xsl:with-param name="ItemName">PriceToEarningsBeforeExtra</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">PriceToEarningsLTMBeforeExtra</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Price/ LTM Earnings</xsl:with-param>
        <xsl:with-param name="ItemName">PriceToEarningsLTMBeforeExtra</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">PriceToBook</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Price/ Book</xsl:with-param>
        <xsl:with-param name="ItemName">PriceToBook</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">DivYield</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Dividend Yield</xsl:with-param>
        <xsl:with-param name="ItemName">DivYield</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
          </table>
        </TD>
  </TR>

</xsl:when>


<xsl:when test="$GAAPDomain = $COAL">
 <tr class="colorlight">
     <td class="colorlight" COLSPAN="3">
       <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2">
         <tr valign="BOTTOM" class="header">
           <td colspan="2" NOWRAP="1" class="header">Financial Performance</td>
           <td align="RIGHT" class="header">
             <xsl:if test="not(normalize-space(Company/Ticker))">
               <xsl:value-of select="Company/InstnName"/>
             </xsl:if>
             <xsl:value-of select="Company/Ticker"/>
           </td>
           <td align="RIGHT" class="header" nowrap="1">Peer<br/>Median</td>
           <td align="RIGHT" class="header" nowrap="1">Peer<br/>Average</td>
         </tr>
      <xsl:call-template name="peerdatarow">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">EBITDAToIncomeRealEstate</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">EBITDA / Income (x)</xsl:with-param>
        <xsl:with-param name="ItemName">EBITDAToIncomeRealEstate</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">DebtToEquity</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Debt / Equity (x)</xsl:with-param>
        <xsl:with-param name="ItemName">DebtToEquity</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">ROAE</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">ROAE (%)</xsl:with-param>
        <xsl:with-param name="ItemName">ROAE</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">ROAA</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">ROAA (%)</xsl:with-param>
        <xsl:with-param name="ItemName">ROAA</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
       </table>
     </td>
   </tr>
   <tr class="colorlight">
       <td class="colorlight" COLSPAN="3">
         <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2">
           <tr valign="BOTTOM" class="header">
             <td colspan="2" NOWRAP="1" class="header">Operating Performance</td>
           <td align="RIGHT" class="header">&#160;</td>
           <td align="RIGHT" class="header" nowrap="1">&#160;</td>
           <td align="RIGHT" class="header" nowrap="1">&#160;</td>
         </tr>

          <xsl:call-template name="peerdatarow">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">CoalSoldHeatContent</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName">Coal Sold</xsl:with-param>
          <xsl:with-param name="ItemName">CoalSoldHeatContent</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
          <xsl:call-template name="peerdatarow">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">CoalReserves</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName">Coal Reserves</xsl:with-param>
          <xsl:with-param name="ItemName">CoalReserves</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

        </table>
      </td>
   </tr>
</xsl:when>

<xsl:when test="$GAAPDomain = $GAMING">
 <tr class="colorlight">
     <td class="colorlight" COLSPAN="3">
       <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2">
         <tr valign="BOTTOM" class="header">
           <td colspan="2" NOWRAP="1" class="header">Financial Performance</td>
           <td align="RIGHT" class="header"><xsl:if test="not(normalize-space(Company/Ticker))"><xsl:value-of select="Company/InstnName"/></xsl:if><xsl:value-of select="Company/Ticker"/></td>
           <td align="RIGHT" class="header" nowrap="1">Peer<br/>Median</td>
           <td align="RIGHT" class="header" nowrap="1">Peer<br/>Average</td>
         </tr>
        <xsl:call-template name="peerdatarow">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">ROAA</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">ROAA</xsl:with-param>
        <xsl:with-param name="ItemName">ROAA</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">ROAE</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">ROAE</xsl:with-param>
        <xsl:with-param name="ItemName">ROAE</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">GrossMarginDirectGaming</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Gaming Margin</xsl:with-param>
        <xsl:with-param name="ItemName">GrossMarginDirectGaming</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">GrossMarginHotel</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Hotel Margin</xsl:with-param>
        <xsl:with-param name="ItemName">GrossMarginHotel</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">GrossMarginOperatingGaming</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Total Operating Margin</xsl:with-param>
        <xsl:with-param name="ItemName">GrossMarginOperatingGaming</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">EBITDACoverageRealEstate</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">EBITDA / Interest Expense</xsl:with-param>
        <xsl:with-param name="ItemName">EBITDACoverageRealEstate</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
       </table>
     </td>
   </tr>
   <tr class="colorlight">
       <td class="colorlight" COLSPAN="3">
         <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2">
           <tr valign="BOTTOM" class="header">
             <td colspan="2" NOWRAP="1" class="header">Gaming Growth Ratios (%)</td>
           <td align="RIGHT" class="header">&#160;</td>
           <td align="RIGHT" class="header" nowrap="1">&#160;</td>
           <td align="RIGHT" class="header" nowrap="1">&#160;</td>
         </tr>

    <xsl:call-template name="peerdatarow">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">NetIncomeGrowth</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Net Income Growth</xsl:with-param>
        <xsl:with-param name="ItemName">NetIncomeGrowth</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
    <xsl:call-template name="peerdatarow">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">NOIGrowth</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">NOI Growth</xsl:with-param>
        <xsl:with-param name="ItemName">NOIGrowth</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
    <xsl:call-template name="peerdatarow">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">NetRevenueGrowth</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Net Revenue Growth</xsl:with-param>
        <xsl:with-param name="ItemName">NetRevenueGrowth</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
    <xsl:call-template name="peerdatarow">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">RevenueGrowthGaming</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Gaming Revenue Growth</xsl:with-param>
        <xsl:with-param name="ItemName">RevenueGrowthGaming</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
    <xsl:call-template name="peerdatarow">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">HotelRevenueGrowth</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Hotel Revenue Growth</xsl:with-param>
        <xsl:with-param name="ItemName">HotelRevenueGrowth</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

        </table>
      </td>
   </tr>
   <tr class="colorlight">
          <td class="colorlight" COLSPAN="3">
            <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2">
              <tr valign="BOTTOM" class="header">
                <td colspan="2" NOWRAP="1" class="header">Gaming Ratios (%)</td>
              <td align="RIGHT" class="header">&#160;</td>
              <td align="RIGHT" class="header" nowrap="1">&#160;</td>
              <td align="RIGHT" class="header" nowrap="1">&#160;</td>
            </tr>

      <xsl:call-template name="peerdatarow">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">GamingRevenueToRevenue</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName">Gaming Revs/Total Revenues Gaming</xsl:with-param>
          <xsl:with-param name="ItemName">GamingRevenueToRevenue</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">HotelRevenueToRevenue</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName">Hotel Revs/Total Revenues Gaming</xsl:with-param>
          <xsl:with-param name="ItemName">HotelRevenueToRevenue</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">GamingExpenseToExpense</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName">Gaming Exp/Total Expenses Gaming</xsl:with-param>
          <xsl:with-param name="ItemName">GamingExpenseToExpense</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">HotelExpenseToExpense</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName">Hotel Exp/Total Expenses Gaming</xsl:with-param>
          <xsl:with-param name="ItemName">HotelExpenseToExpense</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

            </table>
          </td>
   </tr>
   <tr class="colorlight">
          <td class="colorlight" COLSPAN="3">
            <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2">
              <tr valign="BOTTOM" class="header">
                <td colspan="2" NOWRAP="1" class="header">Capitalization - Debt Ratios</td>
              <td align="RIGHT" class="header">&#160;</td>
              <td align="RIGHT" class="header" nowrap="1">&#160;</td>
              <td align="RIGHT" class="header" nowrap="1">&#160;</td>
            </tr>

      <xsl:call-template name="peerdatarow">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">DebtToTotalCapBookValue</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName">Debt/ Book Cap</xsl:with-param>
          <xsl:with-param name="ItemName">DebtToTotalCapBookValue</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">EquityToAssets</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName">Equity/ Assets</xsl:with-param>
          <xsl:with-param name="ItemName">EquityToAssets</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">DebtAndSubDebtToAssets</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName">Debt/Assets</xsl:with-param>
          <xsl:with-param name="ItemName">DebtAndSubDebtToAssets</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">DebtLongTermToDebt</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName">Long-term Debt/ Debt</xsl:with-param>
          <xsl:with-param name="ItemName">DebtLongTermToDebt</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">DebtSeniorToDebt</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName">Senior Debt/Total Debt</xsl:with-param>
          <xsl:with-param name="ItemName">DebtSeniorToDebt</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

            </table>
          </td>
   </tr>
   <tr class="colorlight">
          <td class="colorlight" COLSPAN="3">
            <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2">
              <tr valign="BOTTOM" class="header">
                <td colspan="2" NOWRAP="1" class="header">Market Ratios</td>
              <td align="RIGHT" class="header">&#160;</td>
              <td align="RIGHT" class="header" nowrap="1">&#160;</td>
              <td align="RIGHT" class="header" nowrap="1">&#160;</td>
            </tr>

      <xsl:call-template name="peerdatarow">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">PriceToEarnings</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName">Price/ Earnings</xsl:with-param>
          <xsl:with-param name="ItemName">PriceToEarnings</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">PriceToEarningsLTMAfterExtra</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName">Price/ LTM Earnings</xsl:with-param>
          <xsl:with-param name="ItemName">PriceToEarningsLTMAfterExtra</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">PriceToBook</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName">Price/ Book</xsl:with-param>
          <xsl:with-param name="ItemName">PriceToBook</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">DivYield</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName">Dividend Yield</xsl:with-param>
          <xsl:with-param name="ItemName">DivYield</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

            </table>
          </td>
   </tr>
   <tr class="colorlight">
          <td class="colorlight" COLSPAN="3">
            <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2">
              <tr valign="BOTTOM" class="header">
                <td colspan="2" NOWRAP="1" class="header">Miscellaneous</td>
              <td align="RIGHT" class="header">&#160;</td>
              <td align="RIGHT" class="header" nowrap="1">&#160;</td>
              <td align="RIGHT" class="header" nowrap="1">&#160;</td>
            </tr>

      <xsl:call-template name="peerdatarow">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">PropertyCountCasino</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName"># of Casino Properties</xsl:with-param>
          <xsl:with-param name="ItemName">PropertyCountCasino</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0;(#,##0)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">GamingSlotCount</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName"># of Slots</xsl:with-param>
          <xsl:with-param name="ItemName">GamingSlotCount</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0;(#,##0)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">GamingTableCount</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName"># of Tables</xsl:with-param>
          <xsl:with-param name="ItemName">GamingTableCount</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0;(#,##0)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">PropertySizeRooms</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName"># of Hotel Rooms</xsl:with-param>
          <xsl:with-param name="ItemName">PropertySizeRooms</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0;(#,##0)</xsl:with-param></xsl:call-template>

            </table>
          </td>
   </tr>
</xsl:when>


  <xsl:when test="$GAAPDomain = $MEDIA_ENT or $GAAPDomain = $COMMUNICATIONS or $GAAPDomain = $NEWMEDIA">
    <tr class="colorlight">
      <td class="colorlight" COLSPAN="3">
        <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2">
          <tr valign="BOTTOM" class="header">
            <td colspan="2" NOWRAP="1" class="header">Performance Ratios (%)</td>
            <td align="RIGHT" class="header">
              <xsl:if test="not(normalize-space(Company/Ticker))">
                <xsl:value-of select="Company/InstnName"/>
              </xsl:if>
              <xsl:value-of select="Company/Ticker"/>
            </td>
            <td align="RIGHT" class="header" nowrap="1">
              Peer<br/>Median
            </td>
            <td align="RIGHT" class="header" nowrap="1">
              Peer<br/>Average
            </td>
          </tr>
          <xsl:call-template name="peerdatarow">
            <xsl:with-param name="KeyInstn">
              <xsl:value-of select="Company/KeyInstn"/>
            </xsl:with-param>
            <xsl:with-param name="SNLItemName">ROAE</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">ROAE</xsl:with-param>
            <xsl:with-param name="ItemName">ROAE</xsl:with-param>
            <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param>
          </xsl:call-template>

          <xsl:call-template name="peerdatarow">
            <xsl:with-param name="KeyInstn">
              <xsl:value-of select="Company/KeyInstn"/>
            </xsl:with-param>
            <xsl:with-param name="SNLItemName">RecurringEBITDAMarginComm</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">Recurring EBITDA Margin</xsl:with-param>
            <xsl:with-param name="ItemName">RecurringEBITDAMarginComm</xsl:with-param>
            <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param>
          </xsl:call-template>

          <xsl:call-template name="peerdatarow">
            <xsl:with-param name="KeyInstn">
              <xsl:value-of select="Company/KeyInstn"/>
            </xsl:with-param>
            <xsl:with-param name="SNLItemName">RecurringLeveredFCFMarginComm</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">Recurring Levered FCF Margin</xsl:with-param>
            <xsl:with-param name="ItemName">RecurringLeveredFCFMarginComm</xsl:with-param>
            <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param>
          </xsl:call-template>

          <xsl:call-template name="peerdatarow">
            <xsl:with-param name="KeyInstn">
              <xsl:value-of select="Company/KeyInstn"/>
            </xsl:with-param>
            <xsl:with-param name="SNLItemName">NetIncomeMargin</xsl:with-param>
            <xsl:with-param name="EOP">1</xsl:with-param>
            <xsl:with-param name="DisplayName">Net Income Margin</xsl:with-param>
            <xsl:with-param name="ItemName">NetIncomeMargin</xsl:with-param>
            <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param>
          </xsl:call-template>
        </table>
      </td>
    </tr>
    <tr class="colorlight">
      <td class="colorlight" COLSPAN="3">
        <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2">
          <tr valign="BOTTOM" class="header">
            <td colspan="2" NOWRAP="1" class="header">Leverage Ratios (%)</td>
            <td align="RIGHT" class="header">&#160;</td>
            <td align="RIGHT" class="header" nowrap="1">&#160;</td>
            <td align="RIGHT" class="header" nowrap="1">&#160;</td>
          </tr>

          <xsl:call-template name="peerdatarow">
            <xsl:with-param name="KeyInstn">
              <xsl:value-of select="Company/KeyInstn"/>
            </xsl:with-param>
            <xsl:with-param name="SNLItemName">DebtToEBITDARecurring</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">Debt/ Recurring EBITDA</xsl:with-param>
            <xsl:with-param name="ItemName">DebtToEBITDARecurring</xsl:with-param>
            <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param>
          </xsl:call-template>

          <xsl:call-template name="peerdatarow">
            <xsl:with-param name="KeyInstn">
              <xsl:value-of select="Company/KeyInstn"/>
            </xsl:with-param>
            <xsl:with-param name="SNLItemName">NetDebtPrefToEBITDARecurring</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">Net Debt and Preferred/ Recurring EBITDA</xsl:with-param>
            <xsl:with-param name="ItemName">NetDebtPrefToEBITDARecurring</xsl:with-param>
            <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param>
          </xsl:call-template>
        </table>
      </td>
    </tr>
    <tr class="colorlight">
      <td class="colorlight" COLSPAN="3">
        <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2">
          <tr valign="BOTTOM" class="header">
            <td colspan="2" NOWRAP="1" class="header">Coverage Ratios (%)</td>
            <td align="RIGHT" class="header">&#160;</td>
            <td align="RIGHT" class="header" nowrap="1">&#160;</td>
            <td align="RIGHT" class="header" nowrap="1">&#160;</td>
          </tr>
          <xsl:call-template name="peerdatarow">
            <xsl:with-param name="KeyInstn">
              <xsl:value-of select="Company/KeyInstn"/>
            </xsl:with-param>
            <xsl:with-param name="SNLItemName">EBITDACoverageRecurring</xsl:with-param>
            <xsl:with-param name="EOP">1</xsl:with-param>
            <xsl:with-param name="DisplayName">Recurring EBITDA/ Interest Expense</xsl:with-param>
            <xsl:with-param name="ItemName">EBITDACoverageRecurring</xsl:with-param>
            <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="peerdatarow">
            <xsl:with-param name="KeyInstn">
              <xsl:value-of select="Company/KeyInstn"/>
            </xsl:with-param>
            <xsl:with-param name="SNLItemName">EBITDACoverageRecurringPfd</xsl:with-param>
            <xsl:with-param name="EOP">1</xsl:with-param>
            <xsl:with-param name="DisplayName">Recurring EBITDA/ Interest Expense plus Pfd</xsl:with-param>
            <xsl:with-param name="ItemName">EBITDACoverageRecurringPfd</xsl:with-param>
            <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param>
          </xsl:call-template>
        </table>
      </td>
    </tr>
    <tr class="colorlight">
      <td class="colorlight" COLSPAN="3">
        <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2">
          <tr valign="BOTTOM" class="header">
            <td colspan="2" NOWRAP="1" class="header">Market Ratios (x)</td>
            <td align="RIGHT" class="header">&#160;</td>
            <td align="RIGHT" class="header" nowrap="1">&#160;</td>
            <td align="RIGHT" class="header" nowrap="1">&#160;</td>
          </tr>
          <xsl:call-template name="peerdatarow">
            <xsl:with-param name="KeyInstn">
              <xsl:value-of select="Company/KeyInstn"/>
            </xsl:with-param>
            <xsl:with-param name="SNLItemName">PriceToEarningsLTMAfterExtra</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">Price/LTM EPS</xsl:with-param>
            <xsl:with-param name="ItemName">PriceToEarningsLTMAfterExtra</xsl:with-param>
            <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="peerdatarow">
            <xsl:with-param name="KeyInstn">
              <xsl:value-of select="Company/KeyInstn"/>
            </xsl:with-param>
            <xsl:with-param name="SNLItemName">TEVToRecurringEBITDALTM</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">TEV / Recurring EBITDA (LTM)</xsl:with-param>
            <xsl:with-param name="ItemName">TEVToRecurringEBITDALTM</xsl:with-param>
            <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="peerdatarow">
            <xsl:with-param name="KeyInstn">
              <xsl:value-of select="Company/KeyInstn"/>
            </xsl:with-param>
            <xsl:with-param name="SNLItemName">PriceToRecurringFCFLTM</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">Price / Recurring Levered FCF (LTM)</xsl:with-param>
            <xsl:with-param name="ItemName">PriceToRecurringFCFLTM</xsl:with-param>
            <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="peerdatarow">
            <xsl:with-param name="KeyInstn">
              <xsl:value-of select="Company/KeyInstn"/>
            </xsl:with-param>
            <xsl:with-param name="SNLItemName">DivYield</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">Dividend Yield (%)</xsl:with-param>
            <xsl:with-param name="ItemName">DividendYield</xsl:with-param>
            <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param>
          </xsl:call-template>

        </table>
      </td>
    </tr>
  </xsl:when>


  <xsl:otherwise>
  <TR class="colorlight">
    <TD class="colorlight" COLSPAN="3">
      <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2">
        <tr valign="BOTTOM" class="header">
          <td colspan="2" NOWRAP="1" class="header">Financial Performance</td>
          <td align="RIGHT" class="header">
            <xsl:if test="not(normalize-space(Company/Ticker))">
              <xsl:value-of select="Company/InstnName"/>
          </xsl:if>

          <xsl:value-of select="Company/Ticker"/></td>
          <td align="RIGHT" class="header" nowrap="1">Peer<br/>Median</td>
          <td align="RIGHT" class="header" nowrap="1">Peer<br/>Average</td>
        </tr>

    <xsl:call-template name="peerdatarow">
    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
    <xsl:with-param name="SNLItemName">ROAA</xsl:with-param>
    <xsl:with-param name="EOP">0</xsl:with-param>
    <xsl:with-param name="DisplayName">ROAA</xsl:with-param>
    <xsl:with-param name="ItemName">ROAA</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

    <xsl:call-template name="peerdatarow">
    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
    <xsl:with-param name="SNLItemName">ROAE</xsl:with-param>
    <xsl:with-param name="EOP">0</xsl:with-param>
    <xsl:with-param name="DisplayName">ROAE</xsl:with-param>
    <xsl:with-param name="ItemName">ROAE</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

    <xsl:call-template name="peerdatarow">
    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
    <xsl:with-param name="SNLItemName">GrossMarginTech</xsl:with-param>
    <xsl:with-param name="EOP">0</xsl:with-param>
    <xsl:with-param name="DisplayName">Gross Sales Margin</xsl:with-param>
    <xsl:with-param name="ItemName">GrossMarginTech</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
    <xsl:call-template name="peerdatarow">
    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
    <xsl:with-param name="SNLItemName">OperatingMarginTech</xsl:with-param>
    <xsl:with-param name="EOP">0</xsl:with-param>
    <xsl:with-param name="DisplayName">Operating Sales Margin</xsl:with-param>
    <xsl:with-param name="ItemName">OperatingMarginTech</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
    <xsl:call-template name="peerdatarow">
    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
    <xsl:with-param name="SNLItemName">SalesTechPerEmployee</xsl:with-param>
    <xsl:with-param name="EOP">0</xsl:with-param>
    <xsl:with-param name="DisplayName">Sales/ Employee ($000)</xsl:with-param>
    <xsl:with-param name="ItemName">SalesTechPerEmployee</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
    <xsl:call-template name="peerdatarow">
    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
    <xsl:with-param name="SNLItemName">EBITDACoverage</xsl:with-param>
    <xsl:with-param name="EOP">0</xsl:with-param>
    <xsl:with-param name="DisplayName">EBITDA/ Interest Expense (x)</xsl:with-param>
    <xsl:with-param name="ItemName">EBITDACoverage</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
    <xsl:call-template name="peerdatarow">
    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
    <xsl:with-param name="SNLItemName">EBITDAToIncome</xsl:with-param>
    <xsl:with-param name="EOP">0</xsl:with-param>
    <xsl:with-param name="DisplayName">EBITDA/ Pre-tax Earnings (x)</xsl:with-param>
    <xsl:with-param name="ItemName">EBITDAToIncome</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      </table>
    </TD>
  </TR>
  <TR class="colorlight">
      <TD class="colorlight" COLSPAN="3">
        <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2">
          <tr valign="BOTTOM" class="header">
            <td colspan="2" NOWRAP="1" class="header">Balance Sheet Ratios (%)</td>
          <td align="RIGHT" class="header">&#160;</td>
          <td align="RIGHT" class="header" nowrap="1">&#160;</td>
          <td align="RIGHT" class="header" nowrap="1">&#160;</td>
        </tr>

                    <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">EquityToAssets</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Total Equity / Total Assets</xsl:with-param>
                  <xsl:with-param name="ItemName">EquityToAssets</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">DebtToEquity</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Debt / Equity (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">DebtToEquity</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">CurrentRatio</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Current Ratio (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">CurrentRatio</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">IntangiblesToEquity</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Intangibles/ Equity</xsl:with-param>
                  <xsl:with-param name="ItemName">IntangiblesToEquity</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

        </table>
      </TD>
  </TR>
    <TR class="colorlight">
        <TD class="colorlight" COLSPAN="3">
          <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2">
            <tr valign="BOTTOM" class="header">
              <td colspan="2" NOWRAP="1" class="header">Market Ratios</td>
          <td align="RIGHT" class="header">&#160;</td>
          <td align="RIGHT" class="header" nowrap="1">&#160;</td>
          <td align="RIGHT" class="header" nowrap="1">&#160;</td>
        </tr>
                    <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">PriceToEarningsLTMAfterExtra</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Price / Earnings (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">PriceToEarningsLTMAfterExtra</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">PriceToBook</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Price / Book (%)</xsl:with-param>
                  <xsl:with-param name="ItemName">PriceToBook</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">DivYield</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Dividend Yield (%)</xsl:with-param>
                  <xsl:with-param name="ItemName">DividendYield</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
          </table>
        </TD>
  </TR>
  </xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template name="PEER_HEAD">
<TR class="colorlight">
  <TD class="colorlight" colspan="3" valign="top"><img src="/images/Interactive/blank.gif" width="220" height="8" alt=""/><BR/><span class="default"><i>Data for trailing four quarters</i></span><BR/> <IMG SRC="/images/interactive/blank.gif" WIDTH="150" HEIGHT="1"/><BR/></TD>
</TR>
<tr class="colorlight">
  <td class="colorlight" colspan="3"><span class="default">For the definition of a financial field, select the field name below.</span></td>
</tr>
</xsl:template>

<!-- Default Template Ends here. all templates below are ONLY if you are using the 'Style Template' section in the console. -->







<!-- This is Template ONE option in the console under 'Style Template'
//This template is a striped down version of the main template. Everything (data) is displayed in single rows.
//The sections will be commented on where the rows are. you should be able to just move rows (entire chunks of data) where you need too. Edits to this section are rare, mostly due to size (width of the page).
-->
<xsl:template name="HEADER_PEER2">
 <xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
<TR>
<TD CLASS="titletest" nowrap=""><xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/><br /><IMG SRC="" WIDTH="2" HEIGHT="1" alt="" /><span class="titletest2"><xsl:value-of select="$CompanyNameCaps"/>&#160;(<xsl:value-of select="Company/Exchange"/>&#160;-&#160;<xsl:value-of select="Company/Ticker"/>)&#160;</span></TD>
                        <TD nowrap="0" align="right" valign="top"> <TABLE><TR><TD CLASS="SURR DATASHADE"><span class="default"><i>Data for trailing four quarters</i></span><BR/><span class="default">For the definition of a financial field,select the field name below.</span></TD></TR></TABLE></TD>
</TR>
  <TR class="default" align="left">
    <TD class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Header" disable-output-escaping="yes"/></span></TD>
  </TR>
</xsl:template>

  <xsl:template name="peerdatarow_2">
  <xsl:param name="KeyInstn"/>
  <xsl:param name="SNLItemName"/>
  <xsl:param name="EOP"/>
  <xsl:param name="DisplayName"/>
  <xsl:param name="ItemName"/>
  <xsl:param name="NumberFormatString"/>
    <xsl:variable name="quote">'</xsl:variable>
    <xsl:variable name="itemname" select="concat($quote, $SNLItemName, $quote)"/>
	<tr valign="top" class="data">
        <td class="data" width="1%"><img src="/images/interactive/blank.gif" width="5" height="12" alt=""/></td>
        <td class="data" width="54%" align="left"><a href="#" class="fielddef" title="{$DisplayName}"><xsl:attribute name="onClick">DefWindow(<xsl:value-of select="$KeyInstn"/>, <xsl:value-of select="$itemname"/>, <xsl:value-of select="$EOP"/>); return false</xsl:attribute><xsl:value-of select="$DisplayName"/></a></td>
        <xsl:for-each select="CompanyFinancials">
        <xsl:variable name="Value" select="*[name(.)=$ItemName]"/>
        <td class="data" align="right" width="15%"><xsl:value-of select="format-number($Value, $NumberFormatString, 'data')"/><SPAN CLASS="dataalign">&#160;</SPAN></td>
        </xsl:for-each>
        <xsl:for-each select="Median">
        <xsl:variable name="Value" select="*[name(.)=$ItemName]"/>
        <td class="data" align="right" width="15%"><xsl:value-of select="format-number($Value, $NumberFormatString, 'data')"/><SPAN CLASS="dataalign">&#160;</SPAN></td>
        </xsl:for-each>
        <xsl:for-each select="Average">
        <xsl:variable name="Value" select="*[name(.)=$ItemName]"/>
        <td class="data" align="right" width="15%"><xsl:value-of select="format-number($Value, $NumberFormatString, 'data')"/><SPAN CLASS="dataalign">&#160;</SPAN></td>
        </xsl:for-each>
    </tr>
 </xsl:template>

 <xsl:template name="PEERDATA_2">
    <xsl:variable name="GAAPDomain" select="Company/GAAP"/>
    <xsl:variable name="INSURANCE_UNDERWRITER" select="8"/>
    <xsl:variable name="INSURANCE_BROKER" select="9"/>
    <xsl:variable name="THRIFT" select="2"/>
    <xsl:variable name="INVESTMENT_COMPANY" select="7"/>
    <xsl:variable name="BROKER_DEALER" select="5"/>
    <xsl:variable name="INVESTMENT_ADVISOR" select="4"/>
    <xsl:variable name="REIT" select="6"/>
    <xsl:variable name="BANK" select="1"/>
    <xsl:variable name="SPECIALTY_LENDER" select="3"/>
    <xsl:variable name="ENERGY" select="10"/>
    <xsl:variable name="COMMUNICATIONS" select="14"/>
    <xsl:variable name="FINTECH" select="15"/>
    <xsl:variable name="HOMEBUILDER" select="16"/>
    <xsl:variable name="COAL" select="17"/>
    <xsl:variable name="GAS" select="18"/>
    <xsl:variable name="GAMING" select="19"/>
    <xsl:variable name="MEDIA_ENT" select="20"/>
    <xsl:variable name="NEWMEDIA" select="21"/>

 <xsl:choose>
 <xsl:when test="$GAAPDomain = $BANK or $GAAPDomain = $THRIFT">
  <tr class="data">
    <td class="data" COLSPAN="3">
      <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
        <tr valign="bottom" class="header">
          <td colspan="2" NOWRAP="1" class="surrleft datashade"><span class="titletest2">Performance Ratios (%)</span></td>
          <td align="right" class="surr_topbot datashade">
           <xsl:if test="not(normalize-space(Company/Ticker))">
               <span class="titletest2"><xsl:value-of select="Company/InstnName"/></span>
                    </xsl:if>

           <span class="titletest2"><xsl:value-of select="Company/Ticker"/></span>

          </td>
          <td align="right" class="surr_topbot datashade" nowrap="1"><span class="titletest2">Peer<br/>Median</span></td>
          <td align="right" class="surrright datashade" nowrap="1"><span class="titletest2">Peer<br/>Average</span></td>
        </tr>
            <xsl:call-template name="peerdatarow_2">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">ROAA</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">ROAA</xsl:with-param>
            <xsl:with-param name="ItemName">ROAA</xsl:with-param>
            <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

            <xsl:call-template name="peerdatarow_2">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">ROAE</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">ROAE</xsl:with-param>
            <xsl:with-param name="ItemName">ROAE</xsl:with-param>
            <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

            <xsl:call-template name="peerdatarow_2">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">NetInterestMargin</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">Net Interest Margin</xsl:with-param>
            <xsl:with-param name="ItemName">NetInterestMargin</xsl:with-param>
            <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

            <xsl:call-template name="peerdatarow_2">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">EfficiencyRatio</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">Efficiency Ratio</xsl:with-param>
            <xsl:with-param name="ItemName">EfficiencyRatio</xsl:with-param>
            <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

        <xsl:call-template name="peerdatarow_2">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">InvLoansToDeposits</xsl:with-param>
            <xsl:with-param name="EOP">1</xsl:with-param>
            <xsl:with-param name="DisplayName">Loans / Deposits</xsl:with-param>
            <xsl:with-param name="ItemName">InvLoansToDeposits</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      </table> <br />
    </td>
  </tr>
  <tr class="data">
    <td class="data" COLSPAN="3">
      <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
        <tr valign="bottom" class="header">
          <td colspan="2" NOWRAP="1" class="surrleft datashade"><span class="titletest2">Asset Quality Ratios (%)</span></td>
          <td align="right" class="surr_topbot datashade">&#160;</td>
          <td align="right" class="surr_topbot datashade" nowrap="1">&#160;</td>
          <td align="right" class="surrright datashade" nowrap="1">&#160;</td>
        </tr>

            <xsl:call-template name="peerdatarow_2">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">NPAsToAssets</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">NPAs / Assets</xsl:with-param>
            <xsl:with-param name="ItemName">NPAsToAssets</xsl:with-param>
            <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

        <xsl:call-template name="peerdatarow_2">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">NetChargeoffsToLoans</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">NCOs / Avg Loans</xsl:with-param>
            <xsl:with-param name="ItemName">NetChargeoffsToLoans</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow_2">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">ReservesToLoans</xsl:with-param>
            <xsl:with-param name="EOP">1</xsl:with-param>
            <xsl:with-param name="DisplayName">Reserves / Loans</xsl:with-param>
            <xsl:with-param name="ItemName">ReservesToLoans</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow_2">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">ReservesToNPAs</xsl:with-param>
            <xsl:with-param name="EOP">1</xsl:with-param>
            <xsl:with-param name="DisplayName">Reserves / NPAs</xsl:with-param>
            <xsl:with-param name="ItemName">ReservesToNPAs</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      </table><br />
    </td>
  </tr>
  <tr class="data">
    <td class="data" COLSPAN="3">
      <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
        <tr valign="bottom" class="header">
            <td colspan="2" NOWRAP="1" class="surrleft datashade"><span class="titletest2">Capital Ratios (%)</span></td>
            <td align="right" class="surr_topbot datashade">&#160;</td>
            <td align="right" class="surr_topbot datashade" nowrap="1">&#160;</td>
            <td align="right" class="surrright datashade" nowrap="1">&#160;</td>
        </tr>
        <xsl:call-template name="peerdatarow_2">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">T1RatioAsRegBOCalc</xsl:with-param>
            <xsl:with-param name="EOP">1</xsl:with-param>
            <xsl:with-param name="DisplayName">Tier 1 Capital</xsl:with-param>
            <xsl:with-param name="ItemName">T1RatioAsRegBOCalc</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow_2">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">TangibleEquityToAssets</xsl:with-param>
            <xsl:with-param name="EOP">1</xsl:with-param>
            <xsl:with-param name="DisplayName">Tangible Equity / Tangible Assets</xsl:with-param>
            <xsl:with-param name="ItemName">TangibleEquityToAssets</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow_2">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">EquityToAssets</xsl:with-param>
            <xsl:with-param name="EOP">1</xsl:with-param>
            <xsl:with-param name="DisplayName">Total Equity / Total Assets</xsl:with-param>
            <xsl:with-param name="ItemName">EquityToAssets</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      </table> <br />
    </td>
  </tr>
  <tr class="data">
    <td class="data" COLSPAN="3">
      <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
        <tr valign="bottom" class="header">
          <td colspan="2" NOWRAP="1" class="surrleft datashade"><span class="titletest2">Market Ratios</span></td>
          <td align="right" class="surr_topbot datashade">&#160;</td>
          <td align="right" class="surr_topbot datashade" nowrap="1">&#160;</td>
          <td align="right" class="surrright datashade" nowrap="1">&#160;</td>
        </tr>
        <xsl:call-template name="peerdatarow_2">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">PriceToEarnings</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">Price / Earnings (x)</xsl:with-param>
            <xsl:with-param name="ItemName">PriceToEarnings</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow_2">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">PriceToBook</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">Price / Book (%)</xsl:with-param>
            <xsl:with-param name="ItemName">PriceToBook</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow_2">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">DivYield</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">Dividend Yield (%)</xsl:with-param>
            <xsl:with-param name="ItemName">DividendYield</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

      </table>
    </td>
  </tr>
 </xsl:when>





 <xsl:when test="$GAAPDomain = $INSURANCE_UNDERWRITER or $GAAPDomain = $INSURANCE_BROKER">
   <tr class="data">
     <td class="data" COLSPAN="3">
       <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
          <tr valign="bottom" class="header">
      <td colspan="2" NOWRAP="1" class="surrleft datashade"><span class="titletest2">Performance Ratios (%)</span></td>
      <td align="right" class="surr_topbot datashade"><xsl:if test="not(normalize-space(Company/Ticker))"><span class="titletest2"><xsl:value-of select="Company/InstnName"/></span></xsl:if><span class="titletest2"><xsl:value-of select="Company/Ticker"/></span></td>
      <td align="right" class="surr_topbot datashade" nowrap="1"><span class="titletest2">Peer<br/>Median</span></td>
      <td align="right" class="surrright datashade" nowrap="1"><span class="titletest2">Peer<br/>Average</span></td>
        </tr>
                      <xsl:call-template name="peerdatarow_2">
                    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                    <xsl:with-param name="SNLItemName">ROAA</xsl:with-param>
                    <xsl:with-param name="EOP">0</xsl:with-param>
                    <xsl:with-param name="DisplayName">ROAA</xsl:with-param>
                    <xsl:with-param name="ItemName">ROAA</xsl:with-param>
                <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                <xsl:call-template name="peerdatarow_2">
                    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                    <xsl:with-param name="SNLItemName">ROAAInsuranceOperating</xsl:with-param>
                    <xsl:with-param name="EOP">0</xsl:with-param>
                    <xsl:with-param name="DisplayName">Operating ROAA</xsl:with-param>
                    <xsl:with-param name="ItemName">ROAAInsuranceOperating</xsl:with-param>
                <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

                    <xsl:call-template name="peerdatarow_2">
                    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                    <xsl:with-param name="SNLItemName">ROAE</xsl:with-param>
                    <xsl:with-param name="EOP">0</xsl:with-param>
                    <xsl:with-param name="DisplayName">ROAE</xsl:with-param>
                    <xsl:with-param name="ItemName">ROAE</xsl:with-param>
                    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

                <xsl:call-template name="peerdatarow_2">
                    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                    <xsl:with-param name="SNLItemName">ROAEInsuranceOperating</xsl:with-param>
                    <xsl:with-param name="EOP">0</xsl:with-param>
                    <xsl:with-param name="DisplayName">Operating ROAE</xsl:with-param>
                    <xsl:with-param name="ItemName">ROAEInsuranceOperating</xsl:with-param>
                <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                <xsl:call-template name="peerdatarow_2">
                    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                    <xsl:with-param name="SNLItemName">InvestmentPortfolioYield</xsl:with-param>
                    <xsl:with-param name="EOP">0</xsl:with-param>
                    <xsl:with-param name="DisplayName">Investment Yield</xsl:with-param>
                    <xsl:with-param name="ItemName">InvestmentPortfolioYield</xsl:with-param>
                <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                <xsl:call-template name="peerdatarow_2">
                    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                    <xsl:with-param name="SNLItemName">OperatingMarginInsurance</xsl:with-param>
                    <xsl:with-param name="EOP">0</xsl:with-param>
                    <xsl:with-param name="DisplayName">Operating Margin</xsl:with-param>
                    <xsl:with-param name="ItemName">OperatingMarginInsurance</xsl:with-param>
                <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                <xsl:call-template name="peerdatarow_2">
                    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                    <xsl:with-param name="SNLItemName">EPSAfterExtraGrowth</xsl:with-param>
                    <xsl:with-param name="EOP">0</xsl:with-param>
                    <xsl:with-param name="DisplayName">EPS Growth</xsl:with-param>
                    <xsl:with-param name="ItemName">EPSAfterExtraGrowth</xsl:with-param>
                <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                <xsl:call-template name="peerdatarow_2">
                    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                    <xsl:with-param name="SNLItemName">EPSOperatingGrowth</xsl:with-param>
                    <xsl:with-param name="EOP">0</xsl:with-param>
                    <xsl:with-param name="DisplayName">Operating EPS Growth</xsl:with-param>
                    <xsl:with-param name="ItemName">EPSOperatingGrowth</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

       </table> <br />
     </td>
   </tr>
   <tr class="data">
       <td class="data" COLSPAN="3">
         <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
          <tr valign="bottom" class="header">
                  <td colspan="2" NOWRAP="1" class="surrleft datashade"><span class="titletest2">Balance Sheet Ratios (%)</span></td>
                <td align="right" class="surr_topbot datashade">&#160;</td>
                <td align="right" class="surr_topbot datashade" nowrap="1">&#160;</td>
                <td align="right" class="surrright datashade" nowrap="1">&#160;</td>
          </tr>
              <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">CashAndInvestmentsToAssets</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Cash &amp; Investments / Assets</xsl:with-param>
                  <xsl:with-param name="ItemName">CashAndInvestmentsToAssets</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">PolicyReservesToEquity</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Policy Reserves / Equity (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">PolicyReservesToEquity</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">DebtAndRedeemablePfdToEquity</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Debt plus Redeemable Preferred / Equity (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">DebtAndRedeemablePfdToEquity</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">DebtToTotalCapBookValue</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Debt / Total Cap, at Book</xsl:with-param>
                  <xsl:with-param name="ItemName">DebtToTotalCapBookValue</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">TangibleEquityToAssets</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Tangible Equity / Tangible Assets</xsl:with-param>
                  <xsl:with-param name="ItemName">TangibleEquityToAssets</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">EquityToAssets</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Total Equity / Total Assets</xsl:with-param>
                  <xsl:with-param name="ItemName">EquityToAssets</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">TangibleCommonEquityToAssets</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Tangible Common Equity / Tangible Assets</xsl:with-param>
                  <xsl:with-param name="ItemName">TangibleCommonEquityToAssets</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        </table> <br />
      </td>
   </tr>
     <tr class="data">
         <td class="data" COLSPAN="3">
           <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
          <tr valign="bottom" class="header">
      <td colspan="2" NOWRAP="1" class="surrleft datashade"><span class="titletest2">Market Ratios</span></td>
      <td align="right" class="surr_topbot datashade">&#160;</td>
      <td align="right" class="surr_topbot datashade" nowrap="1">&#160;</td>
      <td align="right" class="surrright datashade" nowrap="1">&#160;</td>
        </tr>
            <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">PriceToEarnings</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Price / Earnings (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">PriceToEarnings</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
            <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">PriceToOperatingEPS</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Price / Operating EPS (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">PriceToOperatingEPS</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">PriceToBook</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Price / Book (%)</xsl:with-param>
                  <xsl:with-param name="ItemName">PriceToBook</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">DivYield</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Dividend Yield (%)</xsl:with-param>
                  <xsl:with-param name="ItemName">DividendYield</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
          </table>
        </td>
   </tr>

 </xsl:when>







 <xsl:when test="$GAAPDomain = $REIT">
  <tr class="data">
     <td class="data" COLSPAN="3">
       <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
         <tr valign="bottom" class="header">
       <td colspan="2" NOWRAP="1" class="surrleft datashade"><span class="titletest2">Financial Performance (%)</span></td>
       <td align="right" class="surr_topbot datashade"><xsl:if test="not(normalize-space(Company/Ticker))"><span class="titletest2"><xsl:value-of select="Company/InstnName"/></span></xsl:if><span class="titletest2"><xsl:value-of select="Company/Ticker"/></span></td>
       <td align="right" class="surr_topbot datashade" nowrap="1"><span class="titletest2">Peer<br/>Median</span></td>
       <td align="right" class="surrright datashade" nowrap="1"><span class="titletest2">Peer<br/>Average</span></td>
        </tr>
              <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">EBITDACoverageRecurringRealEst</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Rec. EBITDA / Int. Exp. (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">EBITDACoverageRecurringRealEst</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">EBITDACoverageRecurringPFDReal</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Rec. EBITDA / Int. Exp. + Pref. Divs. (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">EBITDACoverageRecurringPFDReal</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">GAndANominalToRevenueRE</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">G&amp;A / Total Revenue</xsl:with-param>
                  <xsl:with-param name="ItemName">GAndANominalToRevenueRE</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                  <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">FFOPerShareGrowth</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">FFO / Share Growth</xsl:with-param>
                  <xsl:with-param name="ItemName">FFOPerShareGrowth</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

                  <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">ROAE</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">ROAE</xsl:with-param>
                  <xsl:with-param name="ItemName">ROAE</xsl:with-param>
                  <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

                  <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">ROAA</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">ROAA</xsl:with-param>
                  <xsl:with-param name="ItemName">ROAA</xsl:with-param>
                  <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

                  <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">DividendPayoutFFO</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Payout / FFO</xsl:with-param>
                  <xsl:with-param name="ItemName">DividendPayoutFFO</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
            </table><br />
     </td>
   </tr>
   <tr class="data">
       <td class="data" COLSPAN="3">
         <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
          <tr valign="bottom" class="header">
      <td colspan="2" NOWRAP="1" class="surrleft datashade"><span class="titletest2">Market Performance</span></td>
      <td align="right" class="surr_topbot datashade">&#160;</td>
      <td align="right" class="surr_topbot datashade" nowrap="1">&#160;</td>
      <td align="right" class="surrright datashade" nowrap="1">&#160;</td>
        </tr>
                  <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">PriceToFFO</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Price / FFO (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">PriceToFFO</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                  <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">DivYield</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Dividend Yield (%)</xsl:with-param>
                  <xsl:with-param name="ItemName">DividendYield</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                  <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">UPREITMarketCap</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">UPREIT Market Cap ($M)</xsl:with-param>
                  <xsl:with-param name="ItemName">UPREITMarketCap</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                  <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">TotalCap</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Total Capitalization ($M)</xsl:with-param>
                  <xsl:with-param name="ItemName">TotalCap</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
          </table>
      </td>
   </tr>
 </xsl:when>





 <xsl:when test="$GAAPDomain = $ENERGY or $GAAPDomain = $GAS">
  <tr class="data">
      <td class="data" COLSPAN="3">
        <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
          <tr valign="bottom" class="header">
       <td colspan="2" NOWRAP="1" class="surrleft datashade"><span class="titletest2">Financial Performance</span></td>
       <td align="right" class="surr_topbot datashade"><xsl:if test="not(normalize-space(Company/Ticker))"><span class="titletest2"><xsl:value-of select="Company/InstnName"/></span></xsl:if><span class="titletest2"><xsl:value-of select="Company/Ticker"/></span></td>
       <td align="right" class="surr_topbot datashade" nowrap="1"><span class="titletest2">Peer<br/>Median</span></td>
       <td align="right" class="surrright datashade" nowrap="1"><span class="titletest2">Peer<br/>Average</span></td>
         </tr>
              <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">EBITDAToIncome</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">EBITDA / Income (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">EBITDAToIncome</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">EBITDAAdjEnergyToInterest</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">EBITDA / Interest Exp (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">EBITDAAdjEnergyToInterest</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                  <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">DebtToEquity</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Debt / Equity (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">DebtToEquity</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                  <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">AFUDCToNetIncomeBasic</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">AFUDC / Basic Net Income (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">AFUDCToNetIncomeBasic</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                  <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">ROAE</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">ROAE (%)</xsl:with-param>
                  <xsl:with-param name="ItemName">ROAE</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                  <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">ROAA</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">ROAA (%)</xsl:with-param>
                  <xsl:with-param name="ItemName">ROAA</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        </table><br/>
      </td>
    </tr>
    <tr class="data">
        <td class="data" COLSPAN="3">
          <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
            <tr valign="bottom" class="header">
        <td colspan="2" NOWRAP="1" class="surrleft datashade"><span class="titletest2">Operating Performance</span></td>
         <td align="right" class="surr_topbot datashade">&#160;</td>
         <td align="right" class="surr_topbot datashade" nowrap="1">&#160;</td>
         <td align="right" class="surrright datashade" nowrap="1">&#160;</td>
          </tr>
                  <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">CustomersElectric</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">No. Electric Customers</xsl:with-param>
                  <xsl:with-param name="ItemName">CustomersElectric</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0</xsl:with-param></xsl:call-template>
                  <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">CustomersGas</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">No. Gas Customers</xsl:with-param>
                  <xsl:with-param name="ItemName">CustomersGas</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0</xsl:with-param></xsl:call-template>
                  <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">GasThroughput</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Gas Throughput (MMcf)</xsl:with-param>
                  <xsl:with-param name="ItemName">GasThroughput</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0</xsl:with-param></xsl:call-template>

          </table>
        </td>
    </tr>

 </xsl:when>

 <xsl:when test="$GAAPDomain = $INVESTMENT_COMPANY">
   <tr class="data">
       <td class="data" COLSPAN="3">
         <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
          <tr valign="bottom" class="header">
        <td colspan="2" NOWRAP="1" class="surrleft datashade"><span class="titletest2">Performance Ratios (%)</span></td>
        <td align="right" class="surr_topbot datashade"><xsl:if test="not(normalize-space(Company/Ticker))"><span class="titletest2"><xsl:value-of select="Company/InstnName"/></span></xsl:if><span class="titletest2"><xsl:value-of select="Company/Ticker"/></span></td>
        <td align="right" class="surr_topbot datashade" nowrap="1"><span class="titletest2">Peer<br/>Median</span></td>
        <td align="right" class="surrright datashade" nowrap="1"><span class="titletest2">Peer<br/>Average</span></td>
          </tr>

              <xsl:call-template name="peerdatarow_2">
              <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
              <xsl:with-param name="SNLItemName">ROAA</xsl:with-param>
              <xsl:with-param name="EOP">0</xsl:with-param>
              <xsl:with-param name="DisplayName">ROAA</xsl:with-param>
              <xsl:with-param name="ItemName">ROAA</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

              <xsl:call-template name="peerdatarow_2">
              <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
              <xsl:with-param name="SNLItemName">ROAE</xsl:with-param>
              <xsl:with-param name="EOP">0</xsl:with-param>
              <xsl:with-param name="DisplayName">ROAE</xsl:with-param>
              <xsl:with-param name="ItemName">ROAE</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

              <xsl:call-template name="peerdatarow_2">
              <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
              <xsl:with-param name="SNLItemName">NetInterestMargin</xsl:with-param>
              <xsl:with-param name="EOP">0</xsl:with-param>
              <xsl:with-param name="DisplayName">Net Interest Margin</xsl:with-param>
              <xsl:with-param name="ItemName">NetInterestMargin</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

          <xsl:call-template name="peerdatarow_2">
              <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
              <xsl:with-param name="SNLItemName">EBITDACoverage</xsl:with-param>
              <xsl:with-param name="EOP">0</xsl:with-param>
              <xsl:with-param name="DisplayName">EBITDA / Interest Expense (x)</xsl:with-param>
              <xsl:with-param name="ItemName">EBITDACoverage</xsl:with-param>
          <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
          <xsl:call-template name="peerdatarow_2">
              <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
              <xsl:with-param name="SNLItemName">AssetGrowth</xsl:with-param>
              <xsl:with-param name="EOP">0</xsl:with-param>
              <xsl:with-param name="DisplayName">Asset Growth</xsl:with-param>
              <xsl:with-param name="ItemName">AssetGrowth</xsl:with-param>
          <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
          <xsl:call-template name="peerdatarow_2">
              <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
              <xsl:with-param name="SNLItemName">NetIncomeGrowth</xsl:with-param>
              <xsl:with-param name="EOP">0</xsl:with-param>
              <xsl:with-param name="DisplayName">Net Income Growth</xsl:with-param>
              <xsl:with-param name="ItemName">NetIncomeGrowth</xsl:with-param>
          <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
         </table><br />
       </td>
     </tr>
     <tr class="data">
         <td class="data" COLSPAN="3">
           <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
          <tr valign="bottom" class="header">
        <td colspan="2" NOWRAP="1" class="surrleft datashade"><span class="titletest2">Balance Sheet Ratios (%)</span></td>
        <td align="right" class="surr_topbot datashade">&#160;</td>
        <td align="right" class="surr_topbot datashade" nowrap="1">&#160;</td>
        <td align="right" class="surrright datashade" nowrap="1">&#160;</td>
          </tr>
                <xsl:call-template name="peerdatarow_2">
                    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                    <xsl:with-param name="SNLItemName">EquityToAssets</xsl:with-param>
                    <xsl:with-param name="EOP">0</xsl:with-param>
                    <xsl:with-param name="DisplayName">Total Equity / Total Assets</xsl:with-param>
                    <xsl:with-param name="ItemName">EquityToAssets</xsl:with-param>
                <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                <xsl:call-template name="peerdatarow_2">
                    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                    <xsl:with-param name="SNLItemName">DebtToEquity</xsl:with-param>
                    <xsl:with-param name="EOP">0</xsl:with-param>
                    <xsl:with-param name="DisplayName">Total Debt / Total Equity (x)</xsl:with-param>
                    <xsl:with-param name="ItemName">DebtToEquity</xsl:with-param>
                <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                <xsl:call-template name="peerdatarow_2">
                    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                    <xsl:with-param name="SNLItemName">DebtToTotalCapBookValue</xsl:with-param>
                    <xsl:with-param name="EOP">0</xsl:with-param>
                    <xsl:with-param name="DisplayName">Debt / Total Cap, at Book</xsl:with-param>
                    <xsl:with-param name="ItemName">DebtToTotalCapBookValue</xsl:with-param>
                <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                <xsl:call-template name="peerdatarow_2">
                    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                    <xsl:with-param name="SNLItemName">InvestmentsInvestmentCoToAsset</xsl:with-param>
                    <xsl:with-param name="EOP">0</xsl:with-param>
                    <xsl:with-param name="DisplayName">Investments / Assets</xsl:with-param>
                    <xsl:with-param name="ItemName">InvestmentsInvestmentCoToAsset</xsl:with-param>
                <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

          </table><br />
        </td>
     </tr>
         <tr class="data">
             <td class="data" COLSPAN="3">
               <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
          <tr valign="bottom" class="header">
        <td colspan="2" NOWRAP="1" class="surrleft datashade"><span class="titletest2">Market Ratios</span></td>
        <td align="right" class="surr_topbot datashade">&#160;</td>
        <td align="right" class="surr_topbot datashade" nowrap="1">&#160;</td>
        <td align="right" class="surrright datashade" nowrap="1">&#160;</td>
          </tr>
                <xsl:call-template name="peerdatarow_2">
                    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                    <xsl:with-param name="SNLItemName">PriceToBook</xsl:with-param>
                    <xsl:with-param name="EOP">0</xsl:with-param>
                    <xsl:with-param name="DisplayName">Price / Book (%)</xsl:with-param>
                    <xsl:with-param name="ItemName">PriceToBook</xsl:with-param>
                <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                <xsl:call-template name="peerdatarow_2">
                    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                    <xsl:with-param name="SNLItemName">PriceToEarnings</xsl:with-param>
                    <xsl:with-param name="EOP">0</xsl:with-param>
                    <xsl:with-param name="DisplayName">Price / Earnings (x)</xsl:with-param>
                    <xsl:with-param name="ItemName">PriceToEarnings</xsl:with-param>
                <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                <xsl:call-template name="peerdatarow_2">
                    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                    <xsl:with-param name="SNLItemName">DivYield</xsl:with-param>
                    <xsl:with-param name="EOP">0</xsl:with-param>
                    <xsl:with-param name="DisplayName">Dividend Yield (%)</xsl:with-param>
                    <xsl:with-param name="ItemName">DividendYield</xsl:with-param>
                <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              </table>
            </td>
   </tr>
 </xsl:when>

 <xsl:when test="$GAAPDomain = $SPECIALTY_LENDER">
 <tr class="data">
     <td class="data" COLSPAN="3">
       <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
          <tr valign="bottom" class="header">
      <td colspan="2" NOWRAP="1" class="surrleft datashade"><span class="titletest2">Performance Ratios (%)</span></td>
      <td align="right" class="surr_topbot datashade"><xsl:if test="not(normalize-space(Company/Ticker))"><span class="titletest2"><xsl:value-of select="Company/InstnName"/></span></xsl:if><span class="titletest2"><xsl:value-of select="Company/Ticker"/></span></td>
      <td align="right" class="surr_topbot datashade" nowrap="1"><span class="titletest2">Peer<br/>Median</span></td>
      <td align="right" class="surrright datashade" nowrap="1"><span class="titletest2">Peer<br/>Average</span></td>
        </tr>


                  <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">ROAA</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">ROAA</xsl:with-param>
                  <xsl:with-param name="ItemName">ROAA</xsl:with-param>
                  <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

                  <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">ROAE</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">ROAE</xsl:with-param>
                  <xsl:with-param name="ItemName">ROAE</xsl:with-param>
                  <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

              <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">ROAUM</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Return on Avg. AUM</xsl:with-param>
                  <xsl:with-param name="ItemName">ROAUM</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">AssetGrowth</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Asset Growth</xsl:with-param>
                  <xsl:with-param name="ItemName">AssetGrowth</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">NetIncomeGrowth</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Net Income Growth</xsl:with-param>
                  <xsl:with-param name="ItemName">NetIncomeGrowth</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">AssetsUnderMgmtGrowth</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Assets Managed Growth</xsl:with-param>
                  <xsl:with-param name="ItemName">AssetsUnderMgmtGrowth</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">EPSGrowth</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">EPS Growth</xsl:with-param>
                  <xsl:with-param name="ItemName">EPSGrowth</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
       </table><br />
     </td>
   </tr>
   <tr class="data">
       <td class="data" COLSPAN="3">
         <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
         <tr valign="bottom" class="header">
    <td colspan="2" NOWRAP="1" class="surrleft datashade"><span class="titletest2">Balance Sheet Ratios (%)</span></td>
       <td align="right" class="surr_topbot datashade">&#160;</td>
       <td align="right" class="surr_topbot datashade" nowrap="1">&#160;</td>
       <td align="right" class="surrright datashade" nowrap="1">&#160;</td>
        </tr>
              <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">EquityToAssets</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Total Equity / Total Assets</xsl:with-param>
                  <xsl:with-param name="ItemName">EquityToAssets</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">TangibleEquityToAssets</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Tangible Equity / Total Assets</xsl:with-param>
                  <xsl:with-param name="ItemName">TangibleEquityToAssets</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">DebtToEquity</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Debt / Equity (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">DebtToEquity</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">CashAndInvestmentsToAssetsSI</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Cash &amp; Investments / Assets</xsl:with-param>
                  <xsl:with-param name="ItemName">CashAndInvestmentsToAssetsSI</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">CashAndInvestmentsToAssetsSI</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Clearing Fees &amp; Commissions / Revenue</xsl:with-param>
                  <xsl:with-param name="ItemName">ClearingRevenueToRevenue</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

        </table><br />
      </td>
   </tr>
   <tr class="data">
       <td class="data" COLSPAN="3">
         <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
         <tr valign="bottom" class="header">
       <td colspan="2" NOWRAP="1" class="surrleft datashade"><span class="titletest2">Income Statement Ratios (%)</span></td>
       <td align="right" class="surr_topbot datashade">&#160;</td>
       <td align="right" class="surr_topbot datashade" nowrap="1">&#160;</td>
       <td align="right" class="surrright datashade" nowrap="1">&#160;</td>
        </tr>


              <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">CashAndInvestmentsToAssetsSI</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Clearing Fees &amp; Commissions / Revenue</xsl:with-param>
                  <xsl:with-param name="ItemName">ClearingRevenueToRevenue</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">InvestmentBankingToRevenue</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Investment Bank Fees / Total Revenue</xsl:with-param>
                  <xsl:with-param name="ItemName">InvestmentBankingToRevenue</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">AssetMgmtFeeToRevenueSI</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Total Asset Mgmt Fees / Total Revenue</xsl:with-param>
                  <xsl:with-param name="ItemName">AssetMgmtFeeToRevenueSI</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">PortfolioRevenueToRevenueSI</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Portfolio Revenue / Total Revenue</xsl:with-param>
                  <xsl:with-param name="ItemName">PortfolioRevenueToRevenueSI</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">NetIncomeToRevenueSAndI</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Net Income / Total Revenue</xsl:with-param>
                  <xsl:with-param name="ItemName">NetIncomeToRevenueSAndI</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">EBITDAToIncome</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">EBITDA / Pre-Tax Income (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">EBITDAToIncome</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        </table><br />
      </td>
   </tr>
   <tr class="data">
       <td class="data" COLSPAN="3">
         <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
          <tr valign="bottom" class="header">
        <td colspan="2" NOWRAP="1" class="surrleft datashade"><span class="titletest2">Market Performance</span></td>
      <td align="right" class="surr_topbot datashade">&#160;</td>
      <td align="right" class="surr_topbot datashade" nowrap="1">&#160;</td>
      <td align="right" class="surrright datashade" nowrap="1">&#160;</td>
          </tr>
              <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">PriceToBook</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Price / Book (%)</xsl:with-param>
                  <xsl:with-param name="ItemName">PriceToBook</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">PriceToEarnings</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Price / Earnings (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">PriceToEarnings</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">DivYield</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Dividend Yield (%)</xsl:with-param>
                  <xsl:with-param name="ItemName">DividendYield</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
          </table>
      </td>
   </tr>

 </xsl:when>




 <xsl:when test="$GAAPDomain = $BROKER_DEALER or $GAAPDomain = $INVESTMENT_ADVISOR">
   <tr class="data">
     <td class="data" COLSPAN="3">
      <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
          <tr valign="bottom" class="header">
      <td colspan="2" NOWRAP="1" class="surrleft datashade"><span class="titletest2">Performance Ratios (%)</span></td>
      <td align="right" class="surr_topbot datashade"><xsl:if test="not(normalize-space(Company/Ticker))"><span class="titletest2"><xsl:value-of select="Company/InstnName"/></span></xsl:if><span class="titletest2"><xsl:value-of select="Company/Ticker"/></span></td>
      <td align="right" class="surr_topbot datashade" nowrap="1"><span class="titletest2">Peer<br/>Median</span></td>
      <td align="right" class="surrright datashade" nowrap="1"><span class="titletest2">Peer<br/>Average</span></td>
          </tr>

                  <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">ROAA</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">ROAA</xsl:with-param>
                  <xsl:with-param name="ItemName">ROAA</xsl:with-param>
                  <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

                  <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">ROAE</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">ROAE</xsl:with-param>
                  <xsl:with-param name="ItemName">ROAE</xsl:with-param>
                  <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

                    <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">ROAUM</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Return on Avg. AUM</xsl:with-param>
                  <xsl:with-param name="ItemName">ROAUM</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                      <!--<xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">ROAUM</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Return on Avg. AUM</xsl:with-param>
                  <xsl:with-param name="ItemName">ROAUM</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>-->
                      <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">AssetGrowth</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Asset Growth</xsl:with-param>
                  <xsl:with-param name="ItemName">AssetGrowth</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                      <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">NetIncomeGrowth</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Net Income Growth</xsl:with-param>
                  <xsl:with-param name="ItemName">NetIncomeGrowth</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                      <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">AssetsUnderMgmtGrowth</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Assets Managed Growth</xsl:with-param>
                  <xsl:with-param name="ItemName">AssetsUnderMgmtGrowth</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                      <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">EPSGrowth</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">EPS Growth</xsl:with-param>
                  <xsl:with-param name="ItemName">EPSGrowth</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
       </table><br />
     </td>
   </tr>
   <tr class="data">
       <td class="data" COLSPAN="3">
         <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
          <tr valign="bottom" class="header">
      <td colspan="2" NOWRAP="1" class="surrleft datashade"><span class="titletest2">Balance Sheet Ratios (%)</span></td>
      <td align="right" class="surr_topbot datashade">&#160;</td>
      <td align="right" class="surr_topbot datashade" nowrap="1">&#160;</td>
      <td align="right" class="surrright datashade" nowrap="1">&#160;</td>
          </tr>
                      <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">EquityToAssets</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Total Equity / Total Assets</xsl:with-param>
                  <xsl:with-param name="ItemName">EquityToAssets</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                      <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">TangibleEquityToAssets</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Tangible Equity / Total Assets</xsl:with-param>
                  <xsl:with-param name="ItemName">TangibleEquityToAssets</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                      <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">DebtToEquity</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Debt / Equity (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">DebtToEquity</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                      <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">CashAndInvestmentsToAssetsSI</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Cash &amp; Investments / Assets</xsl:with-param>
                  <xsl:with-param name="ItemName">CashAndInvestmentsToAssetsSI</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                      <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">ClearingRevenueToRevenue</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Clearing Fees &amp; Commissions / Revenue</xsl:with-param>
                  <xsl:with-param name="ItemName">ClearingRevenueToRevenue</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        </table><br />
      </td>
   </tr>
   <tr class="data">
       <td class="data" COLSPAN="3">
         <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
          <tr valign="bottom" class="header">
      <td colspan="2" NOWRAP="1" class="surrleft datashade"><span class="titletest2">Income Statement Ratios (%)</span></td>
      <td align="right" class="surr_topbot datashade">&#160;</td>
      <td align="right" class="surr_topbot datashade" nowrap="1">&#160;</td>
      <td align="right" class="surrright datashade" nowrap="1">&#160;</td>
          </tr>
                      <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">ClearingRevenueToRevenue</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Clearing Fees &amp; Commissions / Revenue</xsl:with-param>
                  <xsl:with-param name="ItemName">ClearingRevenueToRevenue</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                      <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">InvestmentBankingToRevenue</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Investment Bank Fees / Total Revenue</xsl:with-param>
                  <xsl:with-param name="ItemName">InvestmentBankingToRevenue</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                      <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">AssetMgmtFeeToRevenueSI</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Total Asset Mgmt Fees / Total Revenue</xsl:with-param>
                  <xsl:with-param name="ItemName">AssetMgmtFeeToRevenueSI</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                      <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">PortfolioRevenueToRevenueSI</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Portfolio Revenue / Total Revenue</xsl:with-param>
                  <xsl:with-param name="ItemName">PortfolioRevenueToRevenueSI</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                      <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">NetIncomeToRevenueSAndI</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Net Income / Total Revenue</xsl:with-param>
                  <xsl:with-param name="ItemName">NetIncomeToRevenueSAndI</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                      <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">EBITDAToIncome</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">EBITDA / Pre-Tax Income (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">EBITDAToIncome</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        </table><br />
      </td>
   </tr>
   <tr class="data">
       <td class="data" COLSPAN="3">
         <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
          <tr valign="bottom" class="header">
        <td colspan="2" NOWRAP="1" class="surrleft datashade"><span class="titletest2">Market Performance</span></td>
      <td align="right" class="surr_topbot datashade">&#160;</td>
      <td align="right" class="surr_topbot datashade" nowrap="1">&#160;</td>
      <td align="right" class="surrright datashade" nowrap="1">&#160;</td>
         </tr>
                      <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">PriceToBook</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Price / Book (%)</xsl:with-param>
                  <xsl:with-param name="ItemName">PriceToBook</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                      <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">PriceToEarnings</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Price / Earnings (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">PriceToEarnings</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                      <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">DivYield</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Dividend Yield (%)</xsl:with-param>
                  <xsl:with-param name="ItemName">DividendYield</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
          </table>
      </td>
   </tr>
 </xsl:when>





 <xsl:when test="$GAAPDomain = $FINTECH">
   <TR class="data">
     <TD class="data" COLSPAN="3">
       <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
          <tr valign="bottom" class="header">
      <td colspan="2" NOWRAP="1" class="surrleft datashade"><span class="titletest2">Financial Performance (%)</span></td>
      <td align="right" class="surr_topbot datashade"><xsl:if test="not(normalize-space(Company/Ticker))"><span class="titletest2"><xsl:value-of select="Company/InstnName"/></span></xsl:if><span class="titletest2"><xsl:value-of select="Company/Ticker"/></span></td>
      <td align="right" class="surr_topbot datashade" nowrap="1"><span class="titletest2">Peer<br/>Median</span></td>
      <td align="right" class="surrright datashade" nowrap="1"><span class="titletest2">Peer<br/>Average</span></td>
          </tr>
                  <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">ROAA</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">ROAA</xsl:with-param>
                  <xsl:with-param name="ItemName">ROAA</xsl:with-param>
                  <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                  <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">ROAE</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">ROAE</xsl:with-param>
                  <xsl:with-param name="ItemName">ROAE</xsl:with-param>
                  <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                      <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">GrossMarginTech</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Gross Sales Margin</xsl:with-param>
                  <xsl:with-param name="ItemName">GrossMarginTech</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                      <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">OperatingMarginTech</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Operating Sales Margin</xsl:with-param>
                  <xsl:with-param name="ItemName">OperatingMarginTech</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                      <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">SalesTechPerEmployee</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Sales/ Employee ($000)</xsl:with-param>
                  <xsl:with-param name="ItemName">SalesTechPerEmployee</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                      <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">EBITDACoverage</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">EBITDA/ Interest Expense (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">EBITDACoverage</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                      <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">EBITDAToIncome</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">EBITDA/ Pre-tax Earnings (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">EBITDAToIncome</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

       </table><br />
     </TD>
   </TR>
   <TR class="data">
       <TD class="data" COLSPAN="3">
          <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
          <tr valign="bottom" class="header">
                  <td colspan="2" NOWRAP="1" class="surrleft datashade"><span class="titletest2">Balance Sheet Ratios (%)</span></td>
                <td align="right" class="surr_topbot datashade">&#160;</td>
                <td align="right" class="surr_topbot datashade" nowrap="1">&#160;</td>
                <td align="right" class="surrright datashade" nowrap="1">&#160;</td>
        </tr>

                      <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">EquityToAssets</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Total Equity / Total Assets</xsl:with-param>
                  <xsl:with-param name="ItemName">EquityToAssets</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                      <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">DebtToEquity</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Debt / Equity (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">DebtToEquity</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                      <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">CurrentRatio</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Current Ratio (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">CurrentRatio</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                      <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">IntangiblesToEquity</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Intangibles/ Equity</xsl:with-param>
                  <xsl:with-param name="ItemName">IntangiblesToEquity</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        </table><br />
      </TD>
   </TR>
     <TR class="data">
         <TD class="data" COLSPAN="3">
            <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
          <tr valign="bottom" class="header">
                    <td colspan="2" NOWRAP="1" class="surrleft datashade"><span class="titletest2">Market Ratios</span></td>
                <td align="right" class="surr_topbot datashade">&#160;</td>
                <td align="right" class="surr_topbot datashade" nowrap="1">&#160;</td>
                <td align="right" class="surrright datashade" nowrap="1">&#160;</td>
          </tr>
                      <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">PriceToEarningsLTMAfterExtra</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Price / Earnings (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">PriceToEarningsLTMAfterExtra</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                      <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">PriceToBook</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Price / Book (%)</xsl:with-param>
                  <xsl:with-param name="ItemName">PriceToBook</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                      <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">DivYield</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Dividend Yield (%)</xsl:with-param>
                  <xsl:with-param name="ItemName">DivYield</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
          </table>
        </TD>
   </TR>

 </xsl:when>




 <xsl:when test="$GAAPDomain = $HOMEBUILDER">
   <TR class="data">
     <TD class="data" COLSPAN="3">
       <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
          <tr valign="bottom" class="header">
                <td colspan="2" NOWRAP="1" class="surrleft datashade"><span class="titletest2">Financial Performance</span></td>
                <td align="right" class="surr_topbot datashade"><xsl:if test="not(normalize-space(Company/Ticker))"><span class="titletest2"><xsl:value-of select="Company/InstnName"/></span></xsl:if><span class="titletest2"><xsl:value-of select="Company/Ticker"/></span></td>
                <td align="right" class="surr_topbot datashade" nowrap="1"><span class="titletest2">Peer<br/>Median</span></td>
                <td align="right" class="surrright datashade" nowrap="1"><span class="titletest2">Peer<br/>Average</span></td>
        </tr>
      <xsl:call-template name="peerdatarow_2">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">ROAA</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName">ROAA</xsl:with-param>
      <xsl:with-param name="ItemName">ROAA</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

      <xsl:call-template name="peerdatarow_2">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">ROAE</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName">ROAE</xsl:with-param>
      <xsl:with-param name="ItemName">ROAE</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

      <xsl:call-template name="peerdatarow_2">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">GrossMarginBuilder</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName">Gross Margin</xsl:with-param>
      <xsl:with-param name="ItemName">GrossMarginBuilder</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

      <xsl:call-template name="peerdatarow_2">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">OperatingMarginBuilderBldg</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName">Construction Margin</xsl:with-param>
      <xsl:with-param name="ItemName">OperatingMarginBuilderBldg</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

      <xsl:call-template name="peerdatarow_2">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">OperatingMarginBuilderFinl</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName">Financial Services Margin</xsl:with-param>
      <xsl:with-param name="ItemName">OperatingMarginBuilderFinl</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

      <xsl:call-template name="peerdatarow_2">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">OperatingMarginBuilder</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName">Total Operating Margin</xsl:with-param>
      <xsl:with-param name="ItemName">OperatingMarginBuilder</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
       </table><br />
     </TD>
   </TR>
   <TR class="data">
       <TD class="data" COLSPAN="3">
         <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
          <tr valign="bottom" class="header">
                  <td colspan="2" NOWRAP="1" class="surrleft datashade"><span class="titletest2">Homebuilding Growth Ratios (%)</span></td>
                <td align="right" class="surr_topbot datashade">&#160;</td>
                <td align="right" class="surr_topbot datashade" nowrap="1">&#160;</td>
                <td align="right" class="surrright datashade" nowrap="1">&#160;</td>
        </tr>

      <xsl:call-template name="peerdatarow_2">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">SalesBuilderGrowth</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName">Construction Sales Growth</xsl:with-param>
      <xsl:with-param name="ItemName">SalesBuilderGrowth</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

      <xsl:call-template name="peerdatarow_2">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">SalesBuilderNetGrowth</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName">Construction Gross Profit Growth</xsl:with-param>
      <xsl:with-param name="ItemName">SalesBuilderNetGrowth</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">ConstructionOpEarningsGrowth</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName">Construction Operating Earnings Growth</xsl:with-param>
      <xsl:with-param name="ItemName">ConstructionOpEarningsGrowth</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

      <xsl:call-template name="peerdatarow_2">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">BuilderHomesGrowth</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName"># of Homes Sold Growth</xsl:with-param>
      <xsl:with-param name="ItemName">BuilderHomesGrowth</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

      <xsl:call-template name="peerdatarow_2">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">BuilderHomeSalePriceGrowth</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName">Change in Average Sale Price</xsl:with-param>
      <xsl:with-param name="ItemName">BuilderHomeSalePriceGrowth</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

      <xsl:call-template name="peerdatarow_2">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">BuilderLotsGrowth</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName">Total Lots Growth</xsl:with-param>
      <xsl:with-param name="ItemName">BuilderLotsGrowth</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        </table><br />
      </TD>
   </TR>
     <TR class="data">
         <TD class="data" COLSPAN="3">
           <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
          <tr valign="bottom" class="header">
      <td colspan="2" NOWRAP="1" class="surrleft datashade"><span class="titletest2">Capitalization</span></td>
      <td align="right" class="surr_topbot datashade">&#160;</td>
      <td align="right" class="surr_topbot datashade" nowrap="1">&#160;</td>
      <td align="right" class="surrright datashade" nowrap="1">&#160;</td>
          </tr>
      <xsl:call-template name="peerdatarow_2">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">DebtToTotalCapBookValue</xsl:with-param>
        <xsl:with-param name="EOP">1</xsl:with-param>
        <xsl:with-param name="DisplayName">Debt/ Book Cap</xsl:with-param>
        <xsl:with-param name="ItemName">DebtToTotalCapBookValue</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow_2">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">DebtTototalCapAdjLessCash</xsl:with-param>
        <xsl:with-param name="EOP">1</xsl:with-param>
        <xsl:with-param name="DisplayName">Net/Debt/ Net Cap</xsl:with-param>
        <xsl:with-param name="ItemName">DebtTototalCapAdjLessCash</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow_2">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">DebtSeniorToDebt</xsl:with-param>
        <xsl:with-param name="EOP">1</xsl:with-param>
        <xsl:with-param name="DisplayName">Senior Debt/ Debt</xsl:with-param>
        <xsl:with-param name="ItemName">DebtSeniorToDebt</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow_2">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">DebtAndSubDebtBldgBldgToDebt</xsl:with-param>
        <xsl:with-param name="EOP">1</xsl:with-param>
        <xsl:with-param name="DisplayName">Homebuilding Debt/ Debt</xsl:with-param>
        <xsl:with-param name="ItemName">DebtAndSubDebtBldgBldgToDebt</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow_2">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">DebtAndSubDebtBldgFinlToDebt</xsl:with-param>
        <xsl:with-param name="EOP">1</xsl:with-param>
        <xsl:with-param name="DisplayName">Financial Services Debt/ Debt</xsl:with-param>
        <xsl:with-param name="ItemName">DebtAndSubDebtBldgFinlToDebt</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow_2">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">EquityToAssets</xsl:with-param>
        <xsl:with-param name="EOP">1</xsl:with-param>
        <xsl:with-param name="DisplayName">Equity/ Assets</xsl:with-param>
        <xsl:with-param name="ItemName">EquityToAssets</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
          </table><br />
        </TD>
   </TR>

     <TR class="data">
         <TD class="data" COLSPAN="3">
           <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
          <tr valign="bottom" class="header">
      <td colspan="2" NOWRAP="1" class="surrleft datashade"><span class="titletest2">Market Ratios</span></td>
      <td align="right" class="surr_topbot datashade">&#160;</td>
      <td align="right" class="surr_topbot datashade" nowrap="1">&#160;</td>
      <td align="right" class="surrright datashade" nowrap="1">&#160;</td>
          </tr>
      <xsl:call-template name="peerdatarow_2">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">PriceToEarningsBeforeExtra</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Price/ Earnings</xsl:with-param>
        <xsl:with-param name="ItemName">PriceToEarningsBeforeExtra</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow_2">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">PriceToEarningsLTMBeforeExtra</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Price/ LTM Earnings</xsl:with-param>
        <xsl:with-param name="ItemName">PriceToEarningsLTMBeforeExtra</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow_2">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">PriceToBook</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Price/ Book</xsl:with-param>
        <xsl:with-param name="ItemName">PriceToBook</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow_2">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">DivYield</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Dividend Yield</xsl:with-param>
        <xsl:with-param name="ItemName">DivYield</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
          </table>
        </TD>
   </TR>

 </xsl:when>


 <xsl:when test="$GAAPDomain = $COAL">
  <tr class="data">
      <td class="data" COLSPAN="3">
        <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
          <tr valign="bottom" class="">
            <td colspan="2" NOWRAP="1" class="surrleft datashade"><span class="titletest2">Financial Performance</span></td>
            <td align="right" class="surr_topbot datashade"><span class="titletest2"><xsl:value-of select="Company/Ticker"/></span></td>
            <td align="right" class="surr_topbot datashade" nowrap="1"><span class="titletest2">Peer<br/>Median</span></td>
            <td align="right" class="surrright datashade" nowrap="1"><span class="titletest2">Peer<br/>Average</span></td>
          </tr>
      <xsl:call-template name="peerdatarow_2">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">EBITDAToIncomeRealEstate</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">EBITDA / Income (x)</xsl:with-param>
        <xsl:with-param name="ItemName">EBITDAToIncomeRealEstate</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow_2">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">DebtToEquity</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Debt / Equity (x)</xsl:with-param>
        <xsl:with-param name="ItemName">DebtToEquity</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow_2">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">ROAE</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">ROAE (%)</xsl:with-param>
        <xsl:with-param name="ItemName">ROAE</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow_2">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">ROAA</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">ROAA (%)</xsl:with-param>
        <xsl:with-param name="ItemName">ROAA</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        </table><br />
      </td>
    </tr>
    <tr class="data">
        <td class="data" COLSPAN="3">
          <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
          <tr valign="bottom" class="">
              <td colspan="2" NOWRAP="1" class="surrleft datashade"><span class="titletest2">Operating Performance</span></td>
            <td align="right" class="surr_topbot datashade">&#160;</td>
            <td align="right" class="surr_topbot datashade" nowrap="1">&#160;</td>
            <td align="right" class="surrright datashade" nowrap="1">&#160;</td>
          </tr>

          <xsl:call-template name="peerdatarow_2">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">CoalSoldHeatContent</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName">Coal Sold</xsl:with-param>
          <xsl:with-param name="ItemName">CoalSoldHeatContent</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
          <xsl:call-template name="peerdatarow_2">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">CoalReserves</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName">Coal Reserves</xsl:with-param>
          <xsl:with-param name="ItemName">CoalReserves</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

          </table>
        </td>
    </tr>
 </xsl:when>

 <xsl:when test="$GAAPDomain = $GAMING">
  <tr class="data">
      <td class="data" COLSPAN="3">
        <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
          <tr valign="bottom" class="">
            <td colspan="2" NOWRAP="1" class="surrleft datashade"><span class="titletest2">Financial Performance</span></td>
            <td align="right" class="surr_topbot datashade"><xsl:if test="not(normalize-space(Company/Ticker))"><span class="titletest2"><xsl:value-of select="Company/InstnName"/></span></xsl:if><span class="titletest2"><xsl:value-of select="Company/Ticker"/></span></td>
            <td align="right" class="surr_topbot datashade" nowrap="1"><span class="titletest2">Peer<br/>Median</span></td>
            <td align="right" class="surrright datashade" nowrap="1"><span class="titletest2">Peer<br/>Average</span></td>
          </tr>
        <xsl:call-template name="peerdatarow_2">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">ROAA</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">ROAA</xsl:with-param>
        <xsl:with-param name="ItemName">ROAA</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow_2">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">ROAE</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">ROAE</xsl:with-param>
        <xsl:with-param name="ItemName">ROAE</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow_2">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">GrossMarginDirectGaming</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Gaming Margin</xsl:with-param>
        <xsl:with-param name="ItemName">GrossMarginDirectGaming</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow_2">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">GrossMarginHotel</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Hotel Margin</xsl:with-param>
        <xsl:with-param name="ItemName">GrossMarginHotel</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow_2">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">GrossMarginOperatingGaming</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Total Operating Margin</xsl:with-param>
        <xsl:with-param name="ItemName">GrossMarginOperatingGaming</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow_2">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">EBITDACoverageRealEstate</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">EBITDA / Interest Expense</xsl:with-param>
        <xsl:with-param name="ItemName">EBITDACoverageRealEstate</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        </table><br />
      </td>
    </tr>
    <tr class="data">
        <td class="data" COLSPAN="3">
          <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
          <tr valign="bottom" class="">
              <td colspan="2" NOWRAP="1" class="surrleft datashade"><span class="titletest2">Gaming Growth Ratios (%)</span></td>
            <td align="right" class="surr_topbot datashade">&#160;</td>
            <td align="right" class="surr_topbot datashade" nowrap="1">&#160;</td>
            <td align="right" class="surrright datashade" nowrap="1">&#160;</td>
          </tr>

    <xsl:call-template name="peerdatarow_2">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">NetIncomeGrowth</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Net Income Growth</xsl:with-param>
        <xsl:with-param name="ItemName">NetIncomeGrowth</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
    <xsl:call-template name="peerdatarow_2">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">NOIGrowth</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">NOI Growth</xsl:with-param>
        <xsl:with-param name="ItemName">NOIGrowth</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
    <xsl:call-template name="peerdatarow_2">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">NetRevenueGrowth</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Net Revenue Growth</xsl:with-param>
        <xsl:with-param name="ItemName">NetRevenueGrowth</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
    <xsl:call-template name="peerdatarow_2">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">RevenueGrowthGaming</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Gaming Revenue Growth</xsl:with-param>
        <xsl:with-param name="ItemName">RevenueGrowthGaming</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
    <xsl:call-template name="peerdatarow_2">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">HotelRevenueGrowth</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Hotel Revenue Growth</xsl:with-param>
        <xsl:with-param name="ItemName">HotelRevenueGrowth</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

          </table><br />
        </td>
    </tr>
    <tr class="data">
           <td class="data" COLSPAN="3">
             <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
          <tr valign="bottom" class="">
                 <td colspan="2" NOWRAP="1" class="surrleft datashade"><span class="titletest2">Gaming Ratios (%)</span></td>
               <td align="right" class="surr_topbot datashade">&#160;</td>
               <td align="right" class="surr_topbot datashade" nowrap="1">&#160;</td>
               <td align="right" class="surrright datashade" nowrap="1">&#160;</td>
             </tr>

        <xsl:call-template name="peerdatarow_2">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">GamingRevenueToRevenue</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">Gaming Revs/Total Revenues Gaming</xsl:with-param>
            <xsl:with-param name="ItemName">GamingRevenueToRevenue</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow_2">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">HotelRevenueToRevenue</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">Hotel Revs/Total Revenues Gaming</xsl:with-param>
            <xsl:with-param name="ItemName">HotelRevenueToRevenue</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow_2">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">GamingExpenseToExpense</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">Gaming Exp/Total Expenses Gaming</xsl:with-param>
            <xsl:with-param name="ItemName">GamingExpenseToExpense</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow_2">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">HotelExpenseToExpense</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">Hotel Exp/Total Expenses Gaming</xsl:with-param>
            <xsl:with-param name="ItemName">HotelExpenseToExpense</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

            </table><br />
          </td>
    </tr>
    <tr class="data">
           <td class="data" COLSPAN="3">
             <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
          <tr valign="bottom" class="">
                 <td colspan="2" NOWRAP="1" class="surrleft datashade"><span class="titletest2">Capitalization - Debt Ratios</span></td>
               <td align="right" class="surr_topbot datashade">&#160;</td>
               <td align="right" class="surr_topbot datashade" nowrap="1">&#160;</td>
               <td align="right" class="surrright datashade" nowrap="1">&#160;</td>
             </tr>

        <xsl:call-template name="peerdatarow_3">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">DebtToTotalCapBookValue</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">Debt/ Book Cap</xsl:with-param>
            <xsl:with-param name="ItemName">DebtToTotalCapBookValue</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow_2">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">EquityToAssets</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">Equity/ Assets</xsl:with-param>
            <xsl:with-param name="ItemName">EquityToAssets</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow_2">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">DebtAndSubDebtToAssets</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">Debt/Assets</xsl:with-param>
            <xsl:with-param name="ItemName">DebtAndSubDebtToAssets</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow_2">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">DebtLongTermToDebt</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">Long-term Debt/ Debt</xsl:with-param>
            <xsl:with-param name="ItemName">DebtLongTermToDebt</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow_2">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">DebtSeniorToDebt</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">Senior Debt/Total Debt</xsl:with-param>
            <xsl:with-param name="ItemName">DebtSeniorToDebt</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

            </table><br />
          </td>
    </tr>
    <tr class="data">
           <td class="data" COLSPAN="3">
            <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
          <tr valign="bottom" class="">
                 <td colspan="2" NOWRAP="1" class="surrleft datashade"><span class="titletest2">Market Ratios</span></td>
               <td align="right" class="surr_topbot datashade">&#160;</td>
               <td align="right" class="surr_topbot datashade" nowrap="1">&#160;</td>
               <td align="right" class="surrright datashade" nowrap="1">&#160;</td>
             </tr>

        <xsl:call-template name="peerdatarow_2">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">PriceToEarnings</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">Price/ Earnings</xsl:with-param>
            <xsl:with-param name="ItemName">PriceToEarnings</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow_2">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">PriceToEarningsLTMAfterExtra</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">Price/ LTM Earnings</xsl:with-param>
            <xsl:with-param name="ItemName">PriceToEarningsLTMAfterExtra</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow_2">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">PriceToBook</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">Price/ Book</xsl:with-param>
            <xsl:with-param name="ItemName">PriceToBook</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow_2">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">DivYield</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">Dividend Yield</xsl:with-param>
            <xsl:with-param name="ItemName">DivYield</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

            </table><br />
          </td>
    </tr>
    <tr class="data">
           <td class="data" COLSPAN="3">
             <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
          <tr valign="bottom" class="">
                 <td colspan="2" NOWRAP="1" class="surrleft datashade"><span class="titletest2">Miscellaneous</span></td>
               <td align="right" class="surr_topbot datashade">&#160;</td>
               <td align="right" class="surr_topbot datashade" nowrap="1">&#160;</td>
               <td align="right" class="surrright datashade" nowrap="1">&#160;</td>
             </tr>

        <xsl:call-template name="peerdatarow_2">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">PropertyCountCasino</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName"># of Casino Properties</xsl:with-param>
            <xsl:with-param name="ItemName">PropertyCountCasino</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0;(#,##0)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow_2">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">GamingSlotCount</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName"># of Slots</xsl:with-param>
            <xsl:with-param name="ItemName">GamingSlotCount</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0;(#,##0)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow_2">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">GamingTableCount</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName"># of Tables</xsl:with-param>
            <xsl:with-param name="ItemName">GamingTableCount</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0;(#,##0)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow_2">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">PropertySizeRooms</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName"># of Hotel Rooms</xsl:with-param>
            <xsl:with-param name="ItemName">PropertySizeRooms</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0;(#,##0)</xsl:with-param></xsl:call-template>

            </table><br />
          </td>
    </tr>
 </xsl:when>

 <xsl:when test="$GAAPDomain = $MEDIA_ENT or $GAAPDomain = $COMMUNICATIONS or $GAAPDomain = $NEWMEDIA">
     <tr class="data">
       <td class="data" COLSPAN="3">
         <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
           <tr valign="bottom" class="header">
             <td colspan="2" NOWRAP="1" class="surrleft datashade">
               <span class="titletest2">Performance Ratios (%)</span>
             </td>
             <td align="right" class="surr_topbot datashade">
               <xsl:if test="not(normalize-space(Company/Ticker))">
                 <span class="titletest2">
                   <xsl:value-of select="Company/InstnName"/>
                 </span>
               </xsl:if>

               <span class="titletest2">
                 <xsl:value-of select="Company/Ticker"/>
               </span>

             </td>
             <td align="right" class="surr_topbot datashade" nowrap="1">
               <span class="titletest2">
                 Peer<br/>Median
               </span>
             </td>
             <td align="right" class="surrright datashade" nowrap="1">
               <span class="titletest2">
                 Peer<br/>Average
               </span>
             </td>
           </tr>
           <xsl:call-template name="peerdatarow_2">
             <xsl:with-param name="KeyInstn">
               <xsl:value-of select="Company/KeyInstn"/>
             </xsl:with-param>
             <xsl:with-param name="SNLItemName">ROAE</xsl:with-param>
             <xsl:with-param name="EOP">0</xsl:with-param>
             <xsl:with-param name="DisplayName">ROAE</xsl:with-param>
             <xsl:with-param name="ItemName">ROAE</xsl:with-param>
             <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param>
           </xsl:call-template>

           <xsl:call-template name="peerdatarow_2">
             <xsl:with-param name="KeyInstn">
               <xsl:value-of select="Company/KeyInstn"/>
             </xsl:with-param>
             <xsl:with-param name="SNLItemName">RecurringEBITDAMarginComm</xsl:with-param>
             <xsl:with-param name="EOP">0</xsl:with-param>
             <xsl:with-param name="DisplayName">Recurring EBITDA Margin</xsl:with-param>
             <xsl:with-param name="ItemName">RecurringEBITDAMarginComm</xsl:with-param>
             <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param>
           </xsl:call-template>

           <xsl:call-template name="peerdatarow_2">
             <xsl:with-param name="KeyInstn">
               <xsl:value-of select="Company/KeyInstn"/>
             </xsl:with-param>
             <xsl:with-param name="SNLItemName">RecurringLeveredFCFMarginComm</xsl:with-param>
             <xsl:with-param name="EOP">0</xsl:with-param>
             <xsl:with-param name="DisplayName">Recurring Levered FCF Margin</xsl:with-param>
             <xsl:with-param name="ItemName">RecurringLeveredFCFMarginComm</xsl:with-param>
             <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param>
           </xsl:call-template>

           <xsl:call-template name="peerdatarow_2">
             <xsl:with-param name="KeyInstn">
               <xsl:value-of select="Company/KeyInstn"/>
             </xsl:with-param>
             <xsl:with-param name="SNLItemName">NetIncomeMargin</xsl:with-param>
             <xsl:with-param name="EOP">1</xsl:with-param>
             <xsl:with-param name="DisplayName">Net Income Margin</xsl:with-param>
             <xsl:with-param name="ItemName">NetIncomeMargin</xsl:with-param>
             <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param>
           </xsl:call-template>
         </table>
         <br />
       </td>
     </tr>
     <tr class="data">
       <td class="data" COLSPAN="3">
         <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
           <tr valign="bottom" class="header">
             <td colspan="2" NOWRAP="1" class="surrleft datashade">
               <span class="titletest2">Leverage Ratios (%)</span>
             </td>
             <td align="right" class="surr_topbot datashade">&#160;</td>
             <td align="right" class="surr_topbot datashade" nowrap="1">&#160;</td>
             <td align="right" class="surrright datashade" nowrap="1">&#160;</td>
           </tr>

           <xsl:call-template name="peerdatarow_2">
             <xsl:with-param name="KeyInstn">
               <xsl:value-of select="Company/KeyInstn"/>
             </xsl:with-param>
             <xsl:with-param name="SNLItemName">DebtToEBITDARecurring</xsl:with-param>
             <xsl:with-param name="EOP">0</xsl:with-param>
             <xsl:with-param name="DisplayName">Debt/ Recurring EBITDA</xsl:with-param>
             <xsl:with-param name="ItemName">DebtToEBITDARecurring</xsl:with-param>
             <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param>
           </xsl:call-template>

           <xsl:call-template name="peerdatarow_2">
             <xsl:with-param name="KeyInstn">
               <xsl:value-of select="Company/KeyInstn"/>
             </xsl:with-param>
             <xsl:with-param name="SNLItemName">NetDebtPrefToEBITDARecurring</xsl:with-param>
             <xsl:with-param name="EOP">0</xsl:with-param>
             <xsl:with-param name="DisplayName">Net Debt and Preferred/ Recurring EBITDA</xsl:with-param>
             <xsl:with-param name="ItemName">NetDebtPrefToEBITDARecurring</xsl:with-param>
             <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param>
           </xsl:call-template>

         </table>
         <br />
       </td>
     </tr>
     <tr class="data">
       <td class="data" COLSPAN="3">
         <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
           <tr valign="bottom" class="header">
             <td colspan="2" NOWRAP="1" class="surrleft datashade">
               <span class="titletest2">Coverage Ratios (%)</span>
             </td>
             <td align="right" class="surr_topbot datashade">&#160;</td>
             <td align="right" class="surr_topbot datashade" nowrap="1">&#160;</td>
             <td align="right" class="surrright datashade" nowrap="1">&#160;</td>
           </tr>
           <xsl:call-template name="peerdatarow_2">
             <xsl:with-param name="KeyInstn">
               <xsl:value-of select="Company/KeyInstn"/>
             </xsl:with-param>
             <xsl:with-param name="SNLItemName">EBITDACoverageRecurring</xsl:with-param>
             <xsl:with-param name="EOP">1</xsl:with-param>
             <xsl:with-param name="DisplayName">Recurring EBITDA/ Interest Expense</xsl:with-param>
             <xsl:with-param name="ItemName">EBITDACoverageRecurring</xsl:with-param>
             <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param>
           </xsl:call-template>
           <xsl:call-template name="peerdatarow_2">
             <xsl:with-param name="KeyInstn">
               <xsl:value-of select="Company/KeyInstn"/>
             </xsl:with-param>
             <xsl:with-param name="SNLItemName">EBITDACoverageRecurringPfd</xsl:with-param>
             <xsl:with-param name="EOP">1</xsl:with-param>
             <xsl:with-param name="DisplayName">Recurring EBITDA/ Interest Expense plus Pfd</xsl:with-param>
             <xsl:with-param name="ItemName">EBITDACoverageRecurringPfd</xsl:with-param>
             <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param>
           </xsl:call-template>
         </table>
         <br />
       </td>
     </tr>
     <tr class="data">
       <td class="data" COLSPAN="3">
         <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
           <tr valign="bottom" class="header">
             <td colspan="2" NOWRAP="1" class="surrleft datashade">
               <span class="titletest2">Market Ratios (x)</span>
             </td>
             <td align="right" class="surr_topbot datashade">&#160;</td>
             <td align="right" class="surr_topbot datashade" nowrap="1">&#160;</td>
             <td align="right" class="surrright datashade" nowrap="1">&#160;</td>
           </tr>
           <xsl:call-template name="peerdatarow_2">
             <xsl:with-param name="KeyInstn">
               <xsl:value-of select="Company/KeyInstn"/>
             </xsl:with-param>
             <xsl:with-param name="SNLItemName">PriceToEarningsLTMAfterExtra</xsl:with-param>
             <xsl:with-param name="EOP">0</xsl:with-param>
             <xsl:with-param name="DisplayName">Price/LTM EPS</xsl:with-param>
             <xsl:with-param name="ItemName">PriceToEarningsLTMAfterExtra</xsl:with-param>
             <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param>
           </xsl:call-template>
           <xsl:call-template name="peerdatarow_2">
             <xsl:with-param name="KeyInstn">
               <xsl:value-of select="Company/KeyInstn"/>
             </xsl:with-param>
             <xsl:with-param name="SNLItemName">TEVToRecurringEBITDALTM</xsl:with-param>
             <xsl:with-param name="EOP">0</xsl:with-param>
             <xsl:with-param name="DisplayName">TEV / Recurring EBITDA (LTM)</xsl:with-param>
             <xsl:with-param name="ItemName">TEVToRecurringEBITDALTM</xsl:with-param>
             <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param>
           </xsl:call-template>
           <xsl:call-template name="peerdatarow_2">
             <xsl:with-param name="KeyInstn">
               <xsl:value-of select="Company/KeyInstn"/>
             </xsl:with-param>
             <xsl:with-param name="SNLItemName">PriceToRecurringFCFLTM</xsl:with-param>
             <xsl:with-param name="EOP">0</xsl:with-param>
             <xsl:with-param name="DisplayName">Price / Recurring Levered FCF (LTM)</xsl:with-param>
             <xsl:with-param name="ItemName">PriceToRecurringFCFLTM</xsl:with-param>
             <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param>
           </xsl:call-template>
           <xsl:call-template name="peerdatarow_2">
             <xsl:with-param name="KeyInstn">
               <xsl:value-of select="Company/KeyInstn"/>
             </xsl:with-param>
             <xsl:with-param name="SNLItemName">DivYield</xsl:with-param>
             <xsl:with-param name="EOP">0</xsl:with-param>
             <xsl:with-param name="DisplayName">Dividend Yield (%)</xsl:with-param>
             <xsl:with-param name="ItemName">DividendYield</xsl:with-param>
             <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param>
           </xsl:call-template>

         </table>
       </td>
     </tr>
   </xsl:when>


   <xsl:otherwise>
   <TR class="data">
     <TD class="data" COLSPAN="3">
       <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
          <tr valign="bottom" class="">
           <td colspan="2" NOWRAP="1" class="surrleft datashade"><span class="titletest2">Financial Performance</span></td>
           <td align="right" class="surr_topbot datashade">
           <xsl:if test="not(normalize-space(Company/Ticker))">
     <span class="titletest2"><xsl:value-of select="Company/InstnName"/></span>
          </xsl:if>

           <span class="titletest2"><xsl:value-of select="Company/Ticker"/></span>

           </td>
           <td align="right" class="surr_topbot datashade" nowrap="1">Peer<br/>Median</td>
           <td align="right" class="surrright datashade" nowrap="1">Peer<br/>Average</td>
         </tr>

                  <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">ROAA</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">ROAA</xsl:with-param>
                  <xsl:with-param name="ItemName">ROAA</xsl:with-param>
                  <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

                  <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">ROAE</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">ROAE</xsl:with-param>
                  <xsl:with-param name="ItemName">ROAE</xsl:with-param>
                  <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

                      <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">GrossMarginTech</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Gross Sales Margin</xsl:with-param>
                  <xsl:with-param name="ItemName">GrossMarginTech</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                      <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">OperatingMarginTech</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Operating Sales Margin</xsl:with-param>
                  <xsl:with-param name="ItemName">OperatingMarginTech</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">SalesTechPerEmployee</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Sales/ Employee ($000)</xsl:with-param>
                  <xsl:with-param name="ItemName">SalesTechPerEmployee</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">EBITDACoverage</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">EBITDA/ Interest Expense (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">EBITDACoverage</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">EBITDAToIncome</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">EBITDA/ Pre-tax Earnings (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">EBITDAToIncome</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
       </table><br />
     </TD>
   </TR>
   <TR class="data">
       <TD class="data" COLSPAN="3">
         <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
          <tr valign="bottom" class="">
             <td colspan="2" NOWRAP="1" class="surrleft datashade"><span class="titletest2">Balance Sheet Ratios (%)</span></td>
           <td align="right" class="surr_topbot datashade">&#160;</td>
           <td align="right" class="surr_topbot datashade" nowrap="1">&#160;</td>
           <td align="right" class="surrright datashade" nowrap="1">&#160;</td>
         </tr>

                    <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">EquityToAssets</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Total Equity / Total Assets</xsl:with-param>
                  <xsl:with-param name="ItemName">EquityToAssets</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">DebtToEquity</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Debt / Equity (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">DebtToEquity</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">CurrentRatio</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Current Ratio (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">CurrentRatio</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">IntangiblesToEquity</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Intangibles/ Equity</xsl:with-param>
                  <xsl:with-param name="ItemName">IntangiblesToEquity</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

        </table><br />
      </TD>
   </TR>
     <TR class="data">
         <TD class="data" COLSPAN="3">
           <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
          <tr valign="bottom" class="">
               <td colspan="2" NOWRAP="1" class="surrleft datashade"><span class="titletest2">Market Ratios</span></td>
           <td align="right" class="surr_topbot datashade">&#160;</td>
           <td align="right" class="surr_topbot datashade" nowrap="1">&#160;</td>
           <td align="right" class="surrright datashade" nowrap="1">&#160;</td>
         </tr>
                    <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">PriceToEarningsLTMAfterExtra</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Price / Earnings (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">PriceToEarningsLTMAfterExtra</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">PriceToBook</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Price / Book (%)</xsl:with-param>
                  <xsl:with-param name="ItemName">PriceToBook</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow_2">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">DivYield</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Dividend Yield (%)</xsl:with-param>
                  <xsl:with-param name="ItemName">DividendYield</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
          </table>
        </TD>
   </TR>
   </xsl:otherwise>
 </xsl:choose>
</xsl:template>

<xsl:template name="PEER_HEAD_2">
<TR class="data">
  <TD class="data" colspan="3" valign="top"><img src="/images/Interactive/blank.gif" width="220" height="8" alt=""/><BR/><span class="default"><i>Data for trailing four quarters</i></span><BR/> <IMG SRC="/images/interactive/blank.gif" WIDTH="150" HEIGHT="1"/><BR/></TD>
</TR>
<tr class="data">
  <td class="data" colspan="3"><span class="default">For the definition of a financial field, select the field name below.</span></td>
</tr>
</xsl:template>
  <!-- Template ONE Ends here. -->






 <!-- Template 3 -->
 <xsl:template name="peerdatarow_3">
 <xsl:param name="KeyInstn"/>
 <xsl:param name="SNLItemName"/>
 <xsl:param name="EOP"/>
 <xsl:param name="DisplayName"/>
 <xsl:param name="ItemName"/>
 <xsl:param name="NumberFormatString"/>
  <xsl:variable name="quote">'</xsl:variable>
  <xsl:variable name="itemname" select="concat($quote, $SNLItemName, $quote)"/>
            <tr valign="top" class="data">
      <td class="data" width="1%"><img src="/images/interactive/blank.gif" width="5" height="12" alt=""/></td>
      <td class="data" width="54%" align="left"><a href="#" class="fielddef" title="{$DisplayName}"><xsl:attribute name="onClick">DefWindow(<xsl:value-of select="$KeyInstn"/>, <xsl:value-of select="$itemname"/>, <xsl:value-of select="$EOP"/>); return false</xsl:attribute><xsl:value-of select="$DisplayName"/></a></td>
      <xsl:for-each select="CompanyFinancials">
      <xsl:variable name="Value" select="*[name(.)=$ItemName]"/>
      <td class="data" align="right" width="15%"><xsl:value-of select="format-number($Value, $NumberFormatString, 'data')"/><SPAN CLASS="dataalign">&#160;</SPAN></td>
      </xsl:for-each>
      <xsl:for-each select="Median">
      <xsl:variable name="Value" select="*[name(.)=$ItemName]"/>
      <td class="data" align="right" width="15%"><xsl:value-of select="format-number($Value, $NumberFormatString, 'data')"/><SPAN CLASS="dataalign">&#160;</SPAN></td>
      </xsl:for-each>
      <xsl:for-each select="Average">
      <xsl:variable name="Value" select="*[name(.)=$ItemName]"/>
      <td class="data" align="right" width="15%"><xsl:value-of select="format-number($Value, $NumberFormatString, 'data')"/><SPAN CLASS="dataalign">&#160;</SPAN></td>
      </xsl:for-each>
               </tr>
</xsl:template>

<xsl:template name="PEERDATA_3">
<table BORDER="0" CELLSPACING="0" CELLPADDING="3" CLASS="data" width="100%">
<xsl:call-template name="PEER_HEAD_3"/>
    <xsl:variable name="GAAPDomain" select="Company/GAAP"/>
    <xsl:variable name="INSURANCE_UNDERWRITER" select="8"/>
    <xsl:variable name="INSURANCE_BROKER" select="9"/>
    <xsl:variable name="THRIFT" select="2"/>
    <xsl:variable name="INVESTMENT_COMPANY" select="7"/>
    <xsl:variable name="BROKER_DEALER" select="5"/>
    <xsl:variable name="INVESTMENT_ADVISOR" select="4"/>
    <xsl:variable name="REIT" select="6"/>
    <xsl:variable name="BANK" select="1"/>
    <xsl:variable name="SPECIALTY_LENDER" select="3"/>
    <xsl:variable name="ENERGY" select="10"/>
    <xsl:variable name="COMMUNICATIONS" select="14"/>
    <xsl:variable name="FINTECH" select="15"/>
    <xsl:variable name="HOMEBUILDER" select="16"/>
    <xsl:variable name="COAL" select="17"/>
    <xsl:variable name="GAS" select="18"/>
    <xsl:variable name="GAMING" select="19"/>
    <xsl:variable name="MEDIA_ENT" select="20"/>
    <xsl:variable name="NEWMEDIA" select="21"/>
<xsl:choose>
<xsl:when test="$GAAPDomain = $BANK or $GAAPDomain = $THRIFT">
  <tr class="data">
    <td class="data" COLSPAN="3">
      <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
        <tr valign="bottom" class="">
          <td colspan="2" NOWRAP="1" class="datashade table1_item"><span class="defaultbold">Performance Ratios (%)</span>
          </td>
            <td align="right" class="datashade table1_item">
                <span class="defaultbold"><xsl:if test="not(normalize-space(Company/Ticker))"><xsl:value-of select="Company/InstnName"/></xsl:if><xsl:value-of select="Company/Ticker"/></span>
            </td>
          <td align="right" class="datashade table1_item" nowrap="1">
              <span class="defaultbold">
                  Peer<br/>Median
              </span>
          </td>
          <td align="right" class="datashade table1_item" nowrap="1">
              <span class="defaultbold">
                  Peer<br/>Average
              </span>
          </td>
        </tr>
            <xsl:call-template name="peerdatarow_3">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">ROAA</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">ROAA</xsl:with-param>
            <xsl:with-param name="ItemName">ROAA</xsl:with-param>
            <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

            <xsl:call-template name="peerdatarow_3">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">ROAE</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">ROAE</xsl:with-param>
            <xsl:with-param name="ItemName">ROAE</xsl:with-param>
            <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

            <xsl:call-template name="peerdatarow_3">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">NetInterestMargin</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">Net Interest Margin</xsl:with-param>
            <xsl:with-param name="ItemName">NetInterestMargin</xsl:with-param>
            <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

            <xsl:call-template name="peerdatarow_3">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">EfficiencyRatio</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">Efficiency Ratio</xsl:with-param>
            <xsl:with-param name="ItemName">EfficiencyRatio</xsl:with-param>
            <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

        <xsl:call-template name="peerdatarow_3">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">InvLoansToDeposits</xsl:with-param>
            <xsl:with-param name="EOP">1</xsl:with-param>
            <xsl:with-param name="DisplayName">Loans / Deposits</xsl:with-param>
            <xsl:with-param name="ItemName">InvLoansToDeposits</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      </table> <br />
    </td>
  </tr>
  <tr class="data">
    <td class="data" COLSPAN="3">
      <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
        <tr valign="bottom" class="">
          <td align="left" colspan="2" NOWRAP="1" class="datashade table1_item">
              <span class="defaultbold">Asset Quality Ratios (%)</span>
          </td>
          <td align="right" class="datashade table1_item">
              <span class="defaultbold">&#160;</span>
          </td>
          <td align="right" class="datashade table1_item" nowrap="1">
              <span class="defaultbold">&#160;</span>
          </td>
          <td align="right" class="datashade table1_item" nowrap="1">
              <span class="defaultbold">&#160;</span>
          </td>
        </tr>

            <xsl:call-template name="peerdatarow_3">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">NPAsToAssets</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">NPAs / Assets</xsl:with-param>
            <xsl:with-param name="ItemName">NPAsToAssets</xsl:with-param>
            <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

        <xsl:call-template name="peerdatarow_3">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">NetChargeoffsToLoans</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">NCOs / Avg Loans</xsl:with-param>
            <xsl:with-param name="ItemName">NetChargeoffsToLoans</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow_3">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">ReservesToLoans</xsl:with-param>
            <xsl:with-param name="EOP">1</xsl:with-param>
            <xsl:with-param name="DisplayName">Reserves / Loans</xsl:with-param>
            <xsl:with-param name="ItemName">ReservesToLoans</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow_3">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">ReservesToNPAs</xsl:with-param>
            <xsl:with-param name="EOP">1</xsl:with-param>
            <xsl:with-param name="DisplayName">Reserves / NPAs</xsl:with-param>
            <xsl:with-param name="ItemName">ReservesToNPAs</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      </table><br />
    </td>
  </tr>
  <tr class="data">
    <td class="data" COLSPAN="3">
      <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
        <tr valign="bottom" class="">
          <td align="left" colspan="2" NOWRAP="1" class="datashade table1_item">
              <span class="defaultbold">Capital Ratios (%)</span>
          </td>
          <td align="right" class="datashade table1_item">
              <span class="defaultbold">&#160;</span>
          </td>
          <td align="right" class="datashade table1_item" nowrap="1">
              <span class="defaultbold">&#160;</span>
          </td>
          <td align="right" class="datashade table1_item" nowrap="1">
              <span class="defaultbold">&#160;</span>
          </td>
        </tr>
        <xsl:call-template name="peerdatarow_3">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">T1RatioAsRegBOCalc</xsl:with-param>
            <xsl:with-param name="EOP">1</xsl:with-param>
            <xsl:with-param name="DisplayName">Tier 1 Capital</xsl:with-param>
            <xsl:with-param name="ItemName">T1RatioAsRegBOCalc</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow_3">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">TangibleEquityToAssets</xsl:with-param>
            <xsl:with-param name="EOP">1</xsl:with-param>
            <xsl:with-param name="DisplayName">Tangible Equity / Tangible Assets</xsl:with-param>
            <xsl:with-param name="ItemName">TangibleEquityToAssets</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow_3">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">EquityToAssets</xsl:with-param>
            <xsl:with-param name="EOP">1</xsl:with-param>
            <xsl:with-param name="DisplayName">Total Equity / Total Assets</xsl:with-param>
            <xsl:with-param name="ItemName">EquityToAssets</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      </table> <br />
    </td>
  </tr>
  <tr class="data">
    <td class="data" COLSPAN="3">
      <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
        <tr valign="bottom" class="">
          <td align="left" colspan="2" NOWRAP="1" class="datashade table1_item">
              <span class="defaultbold">Market Ratios</span>
          </td>
          <td align="right" class="datashade table1_item">
              <span class="defaultbold">&#160;</span>
          </td>
          <td align="right" class="datashade table1_item" nowrap="1">
              <span class="defaultbold">&#160;</span>
          </td>
          <td align="right" class="datashade table1_item" nowrap="1">
              <span class="defaultbold">&#160;</span>
          </td>
        </tr>
        <xsl:call-template name="peerdatarow_3">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">PriceToEarnings</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">Price / Earnings (x)</xsl:with-param>
            <xsl:with-param name="ItemName">PriceToEarnings</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow_3">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">PriceToBook</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">Price / Book (%)</xsl:with-param>
            <xsl:with-param name="ItemName">PriceToBook</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow_3">
            <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
            <xsl:with-param name="SNLItemName">DivYield</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">Dividend Yield (%)</xsl:with-param>
            <xsl:with-param name="ItemName">DividendYield</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

      </table>
    </td>
  </tr>
</xsl:when>





<xsl:when test="$GAAPDomain = $INSURANCE_UNDERWRITER or $GAAPDomain = $INSURANCE_BROKER">
  <tr class="data">
    <td class="data" COLSPAN="3">
      <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
         <tr valign="bottom" class="">
          <td colspan="2" NOWRAP="1" class="datashade table1_item">
              <span class="defaultbold">Performance Ratios (%)</span>
          </td>
          <td align="right" class="datashade table1_item">
              <span class="defaultbold">
                  <xsl:if test="not(normalize-space(Company/Ticker))">
                      <xsl:value-of select="Company/InstnName"/>
                  </xsl:if>
                  <xsl:value-of select="Company/Ticker"/>
              </span>
          </td>
          <td align="right" class="datashade table1_item" nowrap="1">
              <span class="defaultbold">
                  Peer<br/>Median
              </span>
          </td>
          <td align="right" class="datashade table1_item" nowrap="1">
              <span class="defaultbold">
                  Peer<br/>Average
              </span>
          </td>
        </tr>
                      <xsl:call-template name="peerdatarow_3">
                    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                    <xsl:with-param name="SNLItemName">ROAA</xsl:with-param>
                    <xsl:with-param name="EOP">0</xsl:with-param>
                    <xsl:with-param name="DisplayName">ROAA</xsl:with-param>
                    <xsl:with-param name="ItemName">ROAA</xsl:with-param>
                <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                <xsl:call-template name="peerdatarow_3">
                    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                    <xsl:with-param name="SNLItemName">ROAAInsuranceOperating</xsl:with-param>
                    <xsl:with-param name="EOP">0</xsl:with-param>
                    <xsl:with-param name="DisplayName">Operating ROAA</xsl:with-param>
                    <xsl:with-param name="ItemName">ROAAInsuranceOperating</xsl:with-param>
                <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

                    <xsl:call-template name="peerdatarow_3">
                    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                    <xsl:with-param name="SNLItemName">ROAE</xsl:with-param>
                    <xsl:with-param name="EOP">0</xsl:with-param>
                    <xsl:with-param name="DisplayName">ROAE</xsl:with-param>
                    <xsl:with-param name="ItemName">ROAE</xsl:with-param>
                    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

                <xsl:call-template name="peerdatarow_3">
                    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                    <xsl:with-param name="SNLItemName">ROAEInsuranceOperating</xsl:with-param>
                    <xsl:with-param name="EOP">0</xsl:with-param>
                    <xsl:with-param name="DisplayName">Operating ROAE</xsl:with-param>
                    <xsl:with-param name="ItemName">ROAEInsuranceOperating</xsl:with-param>
                <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                <xsl:call-template name="peerdatarow_3">
                    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                    <xsl:with-param name="SNLItemName">InvestmentPortfolioYield</xsl:with-param>
                    <xsl:with-param name="EOP">0</xsl:with-param>
                    <xsl:with-param name="DisplayName">Investment Yield</xsl:with-param>
                    <xsl:with-param name="ItemName">InvestmentPortfolioYield</xsl:with-param>
                <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                <xsl:call-template name="peerdatarow_3">
                    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                    <xsl:with-param name="SNLItemName">OperatingMarginInsurance</xsl:with-param>
                    <xsl:with-param name="EOP">0</xsl:with-param>
                    <xsl:with-param name="DisplayName">Operating Margin</xsl:with-param>
                    <xsl:with-param name="ItemName">OperatingMarginInsurance</xsl:with-param>
                <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                <xsl:call-template name="peerdatarow_3">
                    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                    <xsl:with-param name="SNLItemName">EPSAfterExtraGrowth</xsl:with-param>
                    <xsl:with-param name="EOP">0</xsl:with-param>
                    <xsl:with-param name="DisplayName">EPS Growth</xsl:with-param>
                    <xsl:with-param name="ItemName">EPSAfterExtraGrowth</xsl:with-param>
                <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                <xsl:call-template name="peerdatarow_3">
                    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                    <xsl:with-param name="SNLItemName">EPSOperatingGrowth</xsl:with-param>
                    <xsl:with-param name="EOP">0</xsl:with-param>
                    <xsl:with-param name="DisplayName">Operating EPS Growth</xsl:with-param>
                    <xsl:with-param name="ItemName">EPSOperatingGrowth</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

      </table> <br />
    </td>
  </tr>
  <tr class="data">
      <td class="data" COLSPAN="3">
        <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
         <tr valign="bottom" class="">
            <td align="left" colspan="2" NOWRAP="1" class="datashade table1_item">
                <span class="defaultbold">Balance Sheet Ratios (%)</span>
            </td>
          <td align="right" class="datashade table1_item">
              <span class="defaultbold">&#160;</span>
          </td>
          <td align="right" class="datashade table1_item" nowrap="1">
              <span class="defaultbold">&#160;</span>
          </td>
          <td align="right" class="datashade table1_item" nowrap="1">
              <span class="defaultbold">&#160;</span>
          </td>
        </tr>
              <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">CashAndInvestmentsToAssets</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Cash &amp; Investments / Assets</xsl:with-param>
                  <xsl:with-param name="ItemName">CashAndInvestmentsToAssets</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">PolicyReservesToEquity</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Policy Reserves / Equity (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">PolicyReservesToEquity</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">DebtAndRedeemablePfdToEquity</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Debt plus Redeemable Preferred / Equity (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">DebtAndRedeemablePfdToEquity</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">DebtToTotalCapBookValue</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Debt / Total Cap, at Book</xsl:with-param>
                  <xsl:with-param name="ItemName">DebtToTotalCapBookValue</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">TangibleEquityToAssets</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Tangible Equity / Tangible Assets</xsl:with-param>
                  <xsl:with-param name="ItemName">TangibleEquityToAssets</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">EquityToAssets</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Total Equity / Total Assets</xsl:with-param>
                  <xsl:with-param name="ItemName">EquityToAssets</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">TangibleCommonEquityToAssets</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Tangible Common Equity / Tangible Assets</xsl:with-param>
                  <xsl:with-param name="ItemName">TangibleCommonEquityToAssets</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        </table> <br />
      </td>
  </tr>
    <tr class="data">
        <td class="data" COLSPAN="3">
          <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
         <tr valign="bottom" class="">
              <td align="left" colspan="2" NOWRAP="1" class="datashade table1_item">
                  <span class="defaultbold">Market Ratios</span>
              </td>
          <td align="right" class="datashade table1_item">&#160;</td>
          <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
          <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
        </tr>
            <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">PriceToEarnings</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Price / Earnings (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">PriceToEarnings</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
            <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">PriceToOperatingEPS</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Price / Operating EPS (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">PriceToOperatingEPS</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">PriceToBook</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Price / Book (%)</xsl:with-param>
                  <xsl:with-param name="ItemName">PriceToBook</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">DivYield</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Dividend Yield (%)</xsl:with-param>
                  <xsl:with-param name="ItemName">DividendYield</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
          </table>
        </td>
  </tr>

</xsl:when>







<xsl:when test="$GAAPDomain = $REIT">
 <tr class="data">
    <td class="data" COLSPAN="3">
      <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
         <tr valign="bottom" class="">
          <td colspan="2" NOWRAP="1" class="datashade table1_item">
              <span class="defaultbold">Financial Performance (%)</span>
          </td>
          <td align="right" class="datashade table1_item">
              <span class="defaultbold">
                  <xsl:if test="not(normalize-space(Company/Ticker))">
                      <xsl:value-of select="Company/InstnName"/>
                  </xsl:if>
                  <xsl:value-of select="Company/Ticker"/>
              </span>
          </td>
          <td align="right" class="datashade table1_item" nowrap="1">
              <span class="defaultbold">
                  Peer<br/>Median
              </span>
          </td>
          <td align="right" class="datashade table1_item" nowrap="1">
              <span class="defaultbold">
                  Peer<br/>Average
              </span>
          </td>
        </tr>
              <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">EBITDACoverageRecurringRealEst</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Rec. EBITDA / Int. Exp. (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">EBITDACoverageRecurringRealEst</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">EBITDACoverageRecurringPFDReal</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Rec. EBITDA / Int. Exp. + Pref. Divs. (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">EBITDACoverageRecurringPFDReal</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">GAndANominalToRevenueRE</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">G&amp;A / Total Revenue</xsl:with-param>
                  <xsl:with-param name="ItemName">GAndANominalToRevenueRE</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">FFOPerShareGrowth</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">FFO / Share Growth</xsl:with-param>
                  <xsl:with-param name="ItemName">FFOPerShareGrowth</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

                  <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">ROAE</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">ROAE</xsl:with-param>
                  <xsl:with-param name="ItemName">ROAE</xsl:with-param>
                  <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

                  <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">ROAA</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">ROAA</xsl:with-param>
                  <xsl:with-param name="ItemName">ROAA</xsl:with-param>
                  <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

                <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">DividendPayoutFFO</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Payout / FFO</xsl:with-param>
                  <xsl:with-param name="ItemName">DividendPayoutFFO</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
           </table><br />
    </td>
  </tr>
  <tr class="data">
      <td class="data" COLSPAN="3">
        <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
         <tr valign="bottom" class="">
            <td align="left" colspan="2" NOWRAP="1" class="datashade table1_item">
                <span class="defaultbold">Market Performance</span>
            </td>
          <td align="right" class="datashade table1_item">&#160;</td>
          <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
          <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
        </tr>
                <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">PriceToFFO</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Price / FFO (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">PriceToFFO</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">DivYield</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Dividend Yield (%)</xsl:with-param>
                  <xsl:with-param name="ItemName">DividendYield</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">UPREITMarketCap</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">UPREIT Market Cap ($M)</xsl:with-param>
                  <xsl:with-param name="ItemName">UPREITMarketCap</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">TotalCap</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Total Capitalization ($M)</xsl:with-param>
                  <xsl:with-param name="ItemName">TotalCap</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
          </table>
      </td>
  </tr>
</xsl:when>





<xsl:when test="$GAAPDomain = $ENERGY or $GAAPDomain = $GAS">
 <tr class="data">
     <td class="data" COLSPAN="3">
       <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
         <tr valign="bottom" class="">
           <td colspan="2" NOWRAP="1" class="datashade table1_item">
               <span class="defaultbold">Financial Performance</span>
           </td>
           <td align="right" class="datashade table1_item">
               <span class="defaultbold">
                   <xsl:if test="not(normalize-space(Company/Ticker))">
                       <xsl:value-of select="Company/InstnName"/>
                   </xsl:if>
                   <xsl:value-of select="Company/Ticker"/>
               </span>
           </td>
           <td align="right" class="datashade table1_item" nowrap="1">
               <span class="defaultbold">
                   Peer<br/>Median
               </span>
           </td>
           <td align="right" class="datashade table1_item" nowrap="1">
               <span class="defaultbold">
                   Peer<br/>Average
               </span>
           </td>
         </tr>
              <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">EBITDAToIncome</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">EBITDA / Income (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">EBITDAToIncome</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">EBITDAAdjEnergyToInterest</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">EBITDA / Interest Exp (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">EBITDAAdjEnergyToInterest</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                  <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">DebtToEquity</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Debt / Equity (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">DebtToEquity</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                  <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">AFUDCToNetIncomeBasic</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">AFUDC / Basic Net Income (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">AFUDCToNetIncomeBasic</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                  <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">ROAE</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">ROAE (%)</xsl:with-param>
                  <xsl:with-param name="ItemName">ROAE</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                  <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">ROAA</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">ROAA (%)</xsl:with-param>
                  <xsl:with-param name="ItemName">ROAA</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
       </table><br/>
     </td>
   </tr>
   <tr class="data">
       <td class="data" COLSPAN="3">
         <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
           <tr valign="bottom">
             <td align="left" colspan="2" NOWRAP="1" class="datashade table1_item">
                 <span class="defaultbold">Operating Performance</span>
             </td>
           <td align="right" class="datashade table1_item">&#160;</td>
           <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
           <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
         </tr>
                <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">CustomersElectric</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">No. Electric Customers</xsl:with-param>
                  <xsl:with-param name="ItemName">CustomersElectric</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0</xsl:with-param></xsl:call-template>
                  <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">CustomersGas</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">No. Gas Customers</xsl:with-param>
                  <xsl:with-param name="ItemName">CustomersGas</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0</xsl:with-param></xsl:call-template>
                  <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">GasThroughput</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Gas Throughput (MMcf)</xsl:with-param>
                  <xsl:with-param name="ItemName">GasThroughput</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0</xsl:with-param></xsl:call-template>

        </table>
      </td>
   </tr>

</xsl:when>

<xsl:when test="$GAAPDomain = $INVESTMENT_COMPANY">
  <tr class="data">
    <td class="data" COLSPAN="3">
      <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
        <tr valign="bottom" class="">
          <td colspan="2" NOWRAP="1" class="datashade table1_item">
              <span class="defaultbold">Performance Ratios (%)</span>
          </td>
          <td align="right" class="datashade table1_item">
              <span class="defaultbold">
                  <xsl:if test="not(normalize-space(Company/Ticker))">
                      <xsl:value-of select="Company/InstnName"/>
                  </xsl:if>
                  <xsl:value-of select="Company/Ticker"/>
              </span>
          </td>
          <td align="right" class="datashade table1_item" nowrap="1">
              <span class="defaultbold">
                  Peer<br/>Median
              </span>
          </td>
          <td align="right" class="datashade table1_item" nowrap="1">
              <span class="defaultbold">
                  Peer<br/>Average
              </span>
          </td>
        </tr>

        <xsl:call-template name="peerdatarow_3">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">ROAA</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">ROAA</xsl:with-param>
        <xsl:with-param name="ItemName">ROAA</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

        <xsl:call-template name="peerdatarow_3">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">ROAE</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">ROAE</xsl:with-param>
        <xsl:with-param name="ItemName">ROAE</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

        <xsl:call-template name="peerdatarow_3">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">NetInterestMargin</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Net Interest Margin</xsl:with-param>
        <xsl:with-param name="ItemName">NetInterestMargin</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

        <xsl:call-template name="peerdatarow_3">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">EBITDACoverage</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">EBITDA / Interest Expense (x)</xsl:with-param>
        <xsl:with-param name="ItemName">EBITDACoverage</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

        <xsl:call-template name="peerdatarow_3">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">AssetGrowth</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Asset Growth</xsl:with-param>
        <xsl:with-param name="ItemName">AssetGrowth</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

        <xsl:call-template name="peerdatarow_3">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">NetIncomeGrowth</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Net Income Growth</xsl:with-param>
        <xsl:with-param name="ItemName">NetIncomeGrowth</xsl:with-param>
        <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        </table><br />
      </td>
    </tr>
    <tr class="data">
        <td class="data" COLSPAN="3">
          <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
         <tr valign="bottom" class="">
              <td align="left" colspan="2" NOWRAP="1" class="datashade table1_item">
                  <span class="defaultbold">Balance Sheet Ratios (%)</span>
              </td>
            <td align="right" class="datashade table1_item">&#160;</td>
            <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
            <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
          </tr>
                <xsl:call-template name="peerdatarow_3">
                    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                    <xsl:with-param name="SNLItemName">EquityToAssets</xsl:with-param>
                    <xsl:with-param name="EOP">0</xsl:with-param>
                    <xsl:with-param name="DisplayName">Total Equity / Total Assets</xsl:with-param>
                    <xsl:with-param name="ItemName">EquityToAssets</xsl:with-param>
                <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                <xsl:call-template name="peerdatarow_3">
                    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                    <xsl:with-param name="SNLItemName">DebtToEquity</xsl:with-param>
                    <xsl:with-param name="EOP">0</xsl:with-param>
                    <xsl:with-param name="DisplayName">Total Debt / Total Equity (x)</xsl:with-param>
                    <xsl:with-param name="ItemName">DebtToEquity</xsl:with-param>
                <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                <xsl:call-template name="peerdatarow_3">
                    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                    <xsl:with-param name="SNLItemName">DebtToTotalCapBookValue</xsl:with-param>
                    <xsl:with-param name="EOP">0</xsl:with-param>
                    <xsl:with-param name="DisplayName">Debt / Total Cap, at Book</xsl:with-param>
                    <xsl:with-param name="ItemName">DebtToTotalCapBookValue</xsl:with-param>
                <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                <xsl:call-template name="peerdatarow_3">
                    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                    <xsl:with-param name="SNLItemName">InvestmentsInvestmentCoToAsset</xsl:with-param>
                    <xsl:with-param name="EOP">0</xsl:with-param>
                    <xsl:with-param name="DisplayName">Investments / Assets</xsl:with-param>
                    <xsl:with-param name="ItemName">InvestmentsInvestmentCoToAsset</xsl:with-param>
                <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

          </table><br />
        </td>
    </tr>
        <tr class="data">
            <td class="data" COLSPAN="3">
              <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
         <tr valign="bottom" class="">
             <td align="left" colspan="2" NOWRAP="1" class="datashade table1_item">
                 <span class="defaultbold">Market Ratios</span>
             </td>
            <td align="right" class="datashade table1_item">&#160;</td>
            <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
            <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
          </tr>
                <xsl:call-template name="peerdatarow_3">
                    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                    <xsl:with-param name="SNLItemName">PriceToBook</xsl:with-param>
                    <xsl:with-param name="EOP">0</xsl:with-param>
                    <xsl:with-param name="DisplayName">Price / Book (%)</xsl:with-param>
                    <xsl:with-param name="ItemName">PriceToBook</xsl:with-param>
                <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                <xsl:call-template name="peerdatarow_3">
                    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                    <xsl:with-param name="SNLItemName">PriceToEarnings</xsl:with-param>
                    <xsl:with-param name="EOP">0</xsl:with-param>
                    <xsl:with-param name="DisplayName">Price / Earnings (x)</xsl:with-param>
                    <xsl:with-param name="ItemName">PriceToEarnings</xsl:with-param>
                <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                <xsl:call-template name="peerdatarow_3">
                    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                    <xsl:with-param name="SNLItemName">DivYield</xsl:with-param>
                    <xsl:with-param name="EOP">0</xsl:with-param>
                    <xsl:with-param name="DisplayName">Dividend Yield (%)</xsl:with-param>
                    <xsl:with-param name="ItemName">DividendYield</xsl:with-param>
                <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              </table>
            </td>
  </tr>
</xsl:when>

<xsl:when test="$GAAPDomain = $SPECIALTY_LENDER">
<tr class="data">
    <td class="data" COLSPAN="3">
      <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
         <tr valign="bottom" class="">
          <td colspan="2" NOWRAP="1" class="datashade table1_item">
              <span class="defaultbold">Performance Ratios (%)</span>
          </td>
          <td align="right" class="datashade table1_item">
              <span class="defaultbold">
                  <xsl:if test="not(normalize-space(Company/Ticker))">
                      <xsl:value-of select="Company/InstnName"/>
                  </xsl:if>
                  <xsl:value-of select="Company/Ticker"/>
              </span>
          </td>
          <td align="right" class="datashade table1_item" nowrap="1">
              <span class="defaultbold">
                  Peer<br/>Median
              </span>
          </td>
          <td align="right" class="datashade table1_item" nowrap="1">
              <span class="defaultbold">
                  Peer<br/>Average
              </span>
          </td>
        </tr>
              <xsl:call-template name="peerdatarow_3">
              <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
              <xsl:with-param name="SNLItemName">ROAA</xsl:with-param>
              <xsl:with-param name="EOP">0</xsl:with-param>
              <xsl:with-param name="DisplayName">ROAA</xsl:with-param>
              <xsl:with-param name="ItemName">ROAA</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

              <xsl:call-template name="peerdatarow_3">
              <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
              <xsl:with-param name="SNLItemName">ROAE</xsl:with-param>
              <xsl:with-param name="EOP">0</xsl:with-param>
              <xsl:with-param name="DisplayName">ROAE</xsl:with-param>
              <xsl:with-param name="ItemName">ROAE</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

              <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">ROAUM</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Return on Avg. AUM</xsl:with-param>
                  <xsl:with-param name="ItemName">ROAUM</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">AssetGrowth</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Asset Growth</xsl:with-param>
                  <xsl:with-param name="ItemName">AssetGrowth</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">NetIncomeGrowth</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Net Income Growth</xsl:with-param>
                  <xsl:with-param name="ItemName">NetIncomeGrowth</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">AssetsUnderMgmtGrowth</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Assets Managed Growth</xsl:with-param>
                  <xsl:with-param name="ItemName">AssetsUnderMgmtGrowth</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">EPSGrowth</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">EPS Growth</xsl:with-param>
                  <xsl:with-param name="ItemName">EPSGrowth</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      </table><br />
    </td>
  </tr>
  <tr class="data">
      <td class="data" COLSPAN="3">
        <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
         <tr valign="bottom" class="">
            <td align="left" colspan="2" NOWRAP="1" class="datashade table1_item">
                <span class="defaultbold">Balance Sheet Ratios (%)</span>
            </td>
          <td align="right" class="datashade table1_item">&#160;</td>
          <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
          <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
        </tr>
              <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">EquityToAssets</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Total Equity / Total Assets</xsl:with-param>
                  <xsl:with-param name="ItemName">EquityToAssets</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">TangibleEquityToAssets</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Tangible Equity / Total Assets</xsl:with-param>
                  <xsl:with-param name="ItemName">TangibleEquityToAssets</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">DebtToEquity</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Debt / Equity (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">DebtToEquity</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">CashAndInvestmentsToAssetsSI</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Cash &amp; Investments / Assets</xsl:with-param>
                  <xsl:with-param name="ItemName">CashAndInvestmentsToAssetsSI</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">CashAndInvestmentsToAssetsSI</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Clearing Fees &amp; Commissions / Revenue</xsl:with-param>
                  <xsl:with-param name="ItemName">ClearingRevenueToRevenue</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

        </table><br />
      </td>
  </tr>
  <tr class="data">
      <td class="data" COLSPAN="3">
        <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
         <tr valign="bottom" class="">
            <td align="left" colspan="2" NOWRAP="1" class="datashade table1_item">
                <span class="defaultbold">Income Statement Ratios (%)</span>
            </td>
          <td align="right" class="datashade table1_item">&#160;</td>
          <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
          <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
        </tr>


              <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">CashAndInvestmentsToAssetsSI</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Clearing Fees &amp; Commissions / Revenue</xsl:with-param>
                  <xsl:with-param name="ItemName">ClearingRevenueToRevenue</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">InvestmentBankingToRevenue</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Investment Bank Fees / Total Revenue</xsl:with-param>
                  <xsl:with-param name="ItemName">InvestmentBankingToRevenue</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">AssetMgmtFeeToRevenueSI</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Total Asset Mgmt Fees / Total Revenue</xsl:with-param>
                  <xsl:with-param name="ItemName">AssetMgmtFeeToRevenueSI</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">PortfolioRevenueToRevenueSI</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Portfolio Revenue / Total Revenue</xsl:with-param>
                  <xsl:with-param name="ItemName">PortfolioRevenueToRevenueSI</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">NetIncomeToRevenueSAndI</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Net Income / Total Revenue</xsl:with-param>
                  <xsl:with-param name="ItemName">NetIncomeToRevenueSAndI</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">EBITDAToIncome</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">EBITDA / Pre-Tax Income (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">EBITDAToIncome</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        </table><br />
      </td>
  </tr>
  <tr class="data">
      <td class="data" COLSPAN="3">
        <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
         <tr valign="bottom" class="">
            <td align="left" colspan="2" NOWRAP="1" class="datashade table1_item">
                <span class="defaultbold">Market Performance</span>
            </td>
          <td align="right" class="datashade table1_item">&#160;</td>
          <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
          <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
        </tr>
              <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">PriceToBook</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Price / Book (%)</xsl:with-param>
                  <xsl:with-param name="ItemName">PriceToBook</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">PriceToEarnings</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Price / Earnings (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">PriceToEarnings</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
              <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">DivYield</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Dividend Yield (%)</xsl:with-param>
                  <xsl:with-param name="ItemName">DividendYield</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
          </table>
      </td>
  </tr>

</xsl:when>




<xsl:when test="$GAAPDomain = $BROKER_DEALER or $GAAPDomain = $INVESTMENT_ADVISOR">
  <tr class="data">
    <td class="data" COLSPAN="3">
     <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
         <tr valign="bottom" class="">
          <td colspan="2" NOWRAP="1" class="datashade table1_item">
              <span class="defaultbold">Performance Ratios (%)</span>
          </td>
          <td align="right" class="datashade table1_item">
              <span class="defaultbold">
                  <xsl:if test="not(normalize-space(Company/Ticker))">
                      <xsl:value-of select="Company/InstnName"/>
                  </xsl:if>
                  <xsl:value-of select="Company/Ticker"/>
              </span>
          </td>
          <td align="right" class="datashade table1_item" nowrap="1">
              <span class="defaultbold">
                  Peer<br/>Median
              </span>
          </td>
          <td align="right" class="datashade table1_item" nowrap="1">
              <span class="defaultbold">
                  Peer<br/>Average
              </span>
          </td>
        </tr>

                  <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">ROAA</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">ROAA</xsl:with-param>
                  <xsl:with-param name="ItemName">ROAA</xsl:with-param>
                  <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

                  <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">ROAE</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">ROAE</xsl:with-param>
                  <xsl:with-param name="ItemName">ROAE</xsl:with-param>
                  <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

                    <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">ROAUM</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Return on Avg. AUM</xsl:with-param>
                  <xsl:with-param name="ItemName">ROAUM</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <!--<xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">ROAUM</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Return on Avg. AUM</xsl:with-param>
                  <xsl:with-param name="ItemName">ROAUM</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>-->
                    <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">AssetGrowth</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Asset Growth</xsl:with-param>
                  <xsl:with-param name="ItemName">AssetGrowth</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">NetIncomeGrowth</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Net Income Growth</xsl:with-param>
                  <xsl:with-param name="ItemName">NetIncomeGrowth</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">AssetsUnderMgmtGrowth</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Assets Managed Growth</xsl:with-param>
                  <xsl:with-param name="ItemName">AssetsUnderMgmtGrowth</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">EPSGrowth</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">EPS Growth</xsl:with-param>
                  <xsl:with-param name="ItemName">EPSGrowth</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      </table><br />
    </td>
  </tr>
  <tr class="data">
      <td class="data" COLSPAN="3">
        <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
         <tr valign="bottom" class="">
            <td align="left" colspan="2" NOWRAP="1" class="datashade table1_item">
                <span class="defaultbold">Balance Sheet Ratios (%)</span>
            </td>
          <td align="right" class="datashade table1_item">&#160;</td>
          <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
          <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
        </tr>
                    <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">EquityToAssets</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Total Equity / Total Assets</xsl:with-param>
                  <xsl:with-param name="ItemName">EquityToAssets</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">TangibleEquityToAssets</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Tangible Equity / Total Assets</xsl:with-param>
                  <xsl:with-param name="ItemName">TangibleEquityToAssets</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">DebtToEquity</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Debt / Equity (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">DebtToEquity</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">CashAndInvestmentsToAssetsSI</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Cash &amp; Investments / Assets</xsl:with-param>
                  <xsl:with-param name="ItemName">CashAndInvestmentsToAssetsSI</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">ClearingRevenueToRevenue</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Clearing Fees &amp; Commissions / Revenue</xsl:with-param>
                  <xsl:with-param name="ItemName">ClearingRevenueToRevenue</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        </table><br />
      </td>
  </tr>
  <tr class="data">
      <td class="data" COLSPAN="3">
        <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
         <tr valign="bottom" class="">
            <td align="left" colspan="2" NOWRAP="1" class="datashade table1_item">
                <span class="defaultbold">Income Statement Ratios (%)</span>
            </td>
          <td align="right" class="datashade table1_item">&#160;</td>
          <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
          <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
        </tr>
                    <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">ClearingRevenueToRevenue</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Clearing Fees &amp; Commissions / Revenue</xsl:with-param>
                  <xsl:with-param name="ItemName">ClearingRevenueToRevenue</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">InvestmentBankingToRevenue</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Investment Bank Fees / Total Revenue</xsl:with-param>
                  <xsl:with-param name="ItemName">InvestmentBankingToRevenue</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">AssetMgmtFeeToRevenueSI</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Total Asset Mgmt Fees / Total Revenue</xsl:with-param>
                  <xsl:with-param name="ItemName">AssetMgmtFeeToRevenueSI</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">PortfolioRevenueToRevenueSI</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Portfolio Revenue / Total Revenue</xsl:with-param>
                  <xsl:with-param name="ItemName">PortfolioRevenueToRevenueSI</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">NetIncomeToRevenueSAndI</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Net Income / Total Revenue</xsl:with-param>
                  <xsl:with-param name="ItemName">NetIncomeToRevenueSAndI</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">EBITDAToIncome</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">EBITDA / Pre-Tax Income (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">EBITDAToIncome</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        </table><br />
      </td>
  </tr>
  <tr class="data">
      <td class="data" COLSPAN="3">
        <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
         <tr valign="bottom" class="">
            <td align="left" colspan="2" NOWRAP="1" class="datashade table1_item">
                <span class="defaultbold">Market Performance</span>
            </td>
          <td align="right" class="datashade table1_item">&#160;</td>
          <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
          <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
        </tr>
                    <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">PriceToBook</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Price / Book (%)</xsl:with-param>
                  <xsl:with-param name="ItemName">PriceToBook</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">PriceToEarnings</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Price / Earnings (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">PriceToEarnings</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">DivYield</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Dividend Yield (%)</xsl:with-param>
                  <xsl:with-param name="ItemName">DividendYield</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
          </table>
      </td>
  </tr>
</xsl:when>





<xsl:when test="$GAAPDomain = $FINTECH">
  <TR class="data">
    <TD class="data" COLSPAN="3">
      <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
         <tr valign="bottom" class="">
          <td colspan="2" NOWRAP="1" class="datashade table1_item">
              <span class="defaultbold">Financial Performance (%)</span>
          </td>
          <td align="right" class="datashade table1_item">
              <span class="defaultbold">
                  <xsl:if test="not(normalize-space(Company/Ticker))">
                      <xsl:value-of select="Company/InstnName"/>
                  </xsl:if>
                  <xsl:value-of select="Company/Ticker"/>
              </span>
          </td>
          <td align="right" class="datashade table1_item" nowrap="1">
              <span class="defaultbold">
                  Peer<br/>Median
              </span>
          </td>
          <td align="right" class="datashade table1_item" nowrap="1">
              <span class="defaultbold">
                  Peer<br/>Average
              </span>
          </td>
        </tr>
                  <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">ROAA</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">ROAA</xsl:with-param>
                  <xsl:with-param name="ItemName">ROAA</xsl:with-param>
                  <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                  <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">ROAE</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">ROAE</xsl:with-param>
                  <xsl:with-param name="ItemName">ROAE</xsl:with-param>
                  <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">GrossMarginTech</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Gross Sales Margin</xsl:with-param>
                  <xsl:with-param name="ItemName">GrossMarginTech</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">OperatingMarginTech</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Operating Sales Margin</xsl:with-param>
                  <xsl:with-param name="ItemName">OperatingMarginTech</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">SalesTechPerEmployee</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Sales/ Employee ($000)</xsl:with-param>
                  <xsl:with-param name="ItemName">SalesTechPerEmployee</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">EBITDACoverage</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">EBITDA/ Interest Expense (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">EBITDACoverage</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">EBITDAToIncome</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">EBITDA/ Pre-tax Earnings (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">EBITDAToIncome</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

      </table><br />
    </TD>
  </TR>
  <TR class="data">
      <TD class="data" COLSPAN="3">
         <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
         <tr valign="bottom" class="">
            <td align="left" colspan="2" NOWRAP="1" class="datashade table1_item">
                <span class="defaultbold">Balance Sheet Ratios (%)</span>
            </td>
          <td align="right" class="datashade table1_item">&#160;</td>
          <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
          <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
        </tr>

                    <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">EquityToAssets</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Total Equity / Total Assets</xsl:with-param>
                  <xsl:with-param name="ItemName">EquityToAssets</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">DebtToEquity</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Debt / Equity (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">DebtToEquity</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">CurrentRatio</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Current Ratio (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">CurrentRatio</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">IntangiblesToEquity</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Intangibles/ Equity</xsl:with-param>
                  <xsl:with-param name="ItemName">IntangiblesToEquity</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        </table><br />
      </TD>
  </TR>
    <TR class="data">
        <TD class="data" COLSPAN="3">
           <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
         <tr valign="bottom" class="">
              <td align="letf" colspan="2" NOWRAP="1" class="datashade table1_item">
                  <span class="defaultbold">Market Ratios</span>
              </td>
          <td align="right" class="datashade table1_item">&#160;</td>
          <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
          <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
        </tr>
                    <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">PriceToEarningsLTMAfterExtra</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Price / Earnings (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">PriceToEarningsLTMAfterExtra</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">PriceToBook</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Price / Book (%)</xsl:with-param>
                  <xsl:with-param name="ItemName">PriceToBook</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">DivYield</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Dividend Yield (%)</xsl:with-param>
                  <xsl:with-param name="ItemName">DivYield</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
          </table>
        </TD>
  </TR>

</xsl:when>




<xsl:when test="$GAAPDomain = $HOMEBUILDER">
  <TR class="data">
    <TD class="data" COLSPAN="3">
      <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
         <tr valign="bottom" class="">
          <td colspan="2" NOWRAP="1" class="datashade table1_item">
              <span class="defaultbold">Financial Performance</span>
          </td>
          <td align="right" class="datashade table1_item">
              <span class="defaultbold">
                  <xsl:if test="not(normalize-space(Company/Ticker))">
                      <xsl:value-of select="Company/InstnName"/>
                  </xsl:if>
                  <xsl:value-of select="Company/Ticker"/>
              </span>
          </td>
          <td align="right" class="datashade table1_item" nowrap="1">
              <span class="defaultbold">
                  Peer<br/>Median
              </span>
          </td>
          <td align="right" class="datashade table1_item" nowrap="1">
              <span class="defaultbold">
                  Peer<br/>Average
              </span>
          </td>
        </tr>
      <xsl:call-template name="peerdatarow_3">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">ROAA</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName">ROAA</xsl:with-param>
      <xsl:with-param name="ItemName">ROAA</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

      <xsl:call-template name="peerdatarow_3">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">ROAE</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName">ROAE</xsl:with-param>
      <xsl:with-param name="ItemName">ROAE</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

      <xsl:call-template name="peerdatarow_3">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">GrossMarginBuilder</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName">Gross Margin</xsl:with-param>
      <xsl:with-param name="ItemName">GrossMarginBuilder</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

      <xsl:call-template name="peerdatarow_3">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">OperatingMarginBuilderBldg</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName">Construction Margin</xsl:with-param>
      <xsl:with-param name="ItemName">OperatingMarginBuilderBldg</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

      <xsl:call-template name="peerdatarow_3">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">OperatingMarginBuilderFinl</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName">Financial Services Margin</xsl:with-param>
      <xsl:with-param name="ItemName">OperatingMarginBuilderFinl</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

      <xsl:call-template name="peerdatarow_3">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">OperatingMarginBuilder</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName">Total Operating Margin</xsl:with-param>
      <xsl:with-param name="ItemName">OperatingMarginBuilder</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      </table><br />
    </TD>
  </TR>
  <TR class="data">
      <TD class="data" COLSPAN="3">
        <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
         <tr valign="bottom" class="">
            <td colspan="2" NOWRAP="1" class="datashade table1_item">
                <span class="defaultbold">Homebuilding Growth Ratios (%)</span>
            </td>
          <td align="right" class="datashade table1_item">&#160;</td>
          <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
          <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
        </tr>

      <xsl:call-template name="peerdatarow_3">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">SalesBuilderGrowth</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName">Construction Sales Growth</xsl:with-param>
      <xsl:with-param name="ItemName">SalesBuilderGrowth</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

      <xsl:call-template name="peerdatarow_3">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">SalesBuilderNetGrowth</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName">Construction Gross Profit Growth</xsl:with-param>
      <xsl:with-param name="ItemName">SalesBuilderNetGrowth</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">ConstructionOpEarningsGrowth</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName">Construction Operating Earnings Growth</xsl:with-param>
      <xsl:with-param name="ItemName">ConstructionOpEarningsGrowth</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

      <xsl:call-template name="peerdatarow_3">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">BuilderHomesGrowth</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName"># of Homes Sold Growth</xsl:with-param>
      <xsl:with-param name="ItemName">BuilderHomesGrowth</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

      <xsl:call-template name="peerdatarow">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">BuilderHomeSalePriceGrowth</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName">Change in Average Sale Price</xsl:with-param>
      <xsl:with-param name="ItemName">BuilderHomeSalePriceGrowth</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

      <xsl:call-template name="peerdatarow_3">
      <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
      <xsl:with-param name="SNLItemName">BuilderLotsGrowth</xsl:with-param>
      <xsl:with-param name="EOP">0</xsl:with-param>
      <xsl:with-param name="DisplayName">Total Lots Growth</xsl:with-param>
      <xsl:with-param name="ItemName">BuilderLotsGrowth</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        </table><br />
      </TD>
  </TR>
    <TR class="data">
        <TD class="data" COLSPAN="3">
          <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
         <tr valign="bottom" class="">
              <td align="left" colspan="2" NOWRAP="1" class="datashade table1_item">
                  <span class="defaultbold">Capitalization</span>
              </td>
          <td align="right" class="datashade table1_item">&#160;</td>
          <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
          <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
        </tr>
      <xsl:call-template name="peerdatarow_3">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">DebtToTotalCapBookValue</xsl:with-param>
        <xsl:with-param name="EOP">1</xsl:with-param>
        <xsl:with-param name="DisplayName">Debt/ Book Cap</xsl:with-param>
        <xsl:with-param name="ItemName">DebtToTotalCapBookValue</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow_3">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">DebtTototalCapAdjLessCash</xsl:with-param>
        <xsl:with-param name="EOP">1</xsl:with-param>
        <xsl:with-param name="DisplayName">Net/Debt/ Net Cap</xsl:with-param>
        <xsl:with-param name="ItemName">DebtTototalCapAdjLessCash</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow_3">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">DebtSeniorToDebt</xsl:with-param>
        <xsl:with-param name="EOP">1</xsl:with-param>
        <xsl:with-param name="DisplayName">Senior Debt/ Debt</xsl:with-param>
        <xsl:with-param name="ItemName">DebtSeniorToDebt</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow_3">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">DebtAndSubDebtBldgBldgToDebt</xsl:with-param>
        <xsl:with-param name="EOP">1</xsl:with-param>
        <xsl:with-param name="DisplayName">Homebuilding Debt/ Debt</xsl:with-param>
        <xsl:with-param name="ItemName">DebtAndSubDebtBldgBldgToDebt</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow_3">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">DebtAndSubDebtBldgFinlToDebt</xsl:with-param>
        <xsl:with-param name="EOP">1</xsl:with-param>
        <xsl:with-param name="DisplayName">Financial Services Debt/ Debt</xsl:with-param>
        <xsl:with-param name="ItemName">DebtAndSubDebtBldgFinlToDebt</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow_3">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">EquityToAssets</xsl:with-param>
        <xsl:with-param name="EOP">1</xsl:with-param>
        <xsl:with-param name="DisplayName">Equity/ Assets</xsl:with-param>
        <xsl:with-param name="ItemName">EquityToAssets</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
          </table><br />
        </TD>
  </TR>

    <TR class="data">
        <TD class="data" COLSPAN="3">
          <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
         <tr valign="bottom" class="">
              <td align="left" colspan="2" NOWRAP="1" class="datashade table1_item">
                  <span class="defaultbold">Market Ratios</span>
              </td>
          <td align="right" class="datashade table1_item">&#160;</td>
          <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
          <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
        </tr>
      <xsl:call-template name="peerdatarow_3">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">PriceToEarningsBeforeExtra</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Price/ Earnings</xsl:with-param>
        <xsl:with-param name="ItemName">PriceToEarningsBeforeExtra</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow_3">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">PriceToEarningsLTMBeforeExtra</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Price/ LTM Earnings</xsl:with-param>
        <xsl:with-param name="ItemName">PriceToEarningsLTMBeforeExtra</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow_3">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">PriceToBook</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Price/ Book</xsl:with-param>
        <xsl:with-param name="ItemName">PriceToBook</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow_3">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">DivYield</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Dividend Yield</xsl:with-param>
        <xsl:with-param name="ItemName">DivYield</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
          </table>
        </TD>
  </TR>

</xsl:when>


<xsl:when test="$GAAPDomain = $COAL">
 <tr class="data">
     <td class="data" COLSPAN="3">
       <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
         <tr valign="bottom" class="">
           <td colspan="2" NOWRAP="1" class="datashade table1_item">
               <span class="defaultbold">Financial Performance</span>
           </td>
           <td align="right" class="datashade table1_item">
               <span class="defaultbold">
                   <xsl:if test="not(normalize-space(Company/Ticker))">
                       <xsl:value-of select="Company/InstnName"/>
                   </xsl:if>
                   <xsl:value-of select="Company/Ticker"/>
               </span>
           </td>
           <td align="right" class="datashade table1_item" nowrap="1">
               <span class="defaultbold">
                   Peer<br/>Median
               </span>
           </td>
           <td align="right" class="datashade table1_item" nowrap="1">
               <span class="defaultbold">
                   Peer<br/>Average
               </span>
           </td>
         </tr>
      <xsl:call-template name="peerdatarow_3">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">EBITDAToIncomeRealEstate</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">EBITDA / Income (x)</xsl:with-param>
        <xsl:with-param name="ItemName">EBITDAToIncomeRealEstate</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow_3">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">DebtToEquity</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Debt / Equity (x)</xsl:with-param>
        <xsl:with-param name="ItemName">DebtToEquity</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow_3">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">ROAE</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">ROAE (%)</xsl:with-param>
        <xsl:with-param name="ItemName">ROAE</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow_3">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">ROAA</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">ROAA (%)</xsl:with-param>
        <xsl:with-param name="ItemName">ROAA</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
       </table><br />
     </td>
   </tr>
   <tr class="data">
       <td class="data" COLSPAN="3">
         <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
         <tr valign="bottom" class="">
             <td align="left" colspan="2" NOWRAP="1" class="datashade table1_item">
                 <span class="defaultbold">Operating Performance</span>
             </td>
           <td align="right" class="datashade table1_item">&#160;</td>
           <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
           <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
         </tr>

          <xsl:call-template name="peerdatarow_3">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">CoalSoldHeatContent</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName">Coal Sold</xsl:with-param>
          <xsl:with-param name="ItemName">CoalSoldHeatContent</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
          <xsl:call-template name="peerdatarow_3">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">CoalReserves</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName">Coal Reserves</xsl:with-param>
          <xsl:with-param name="ItemName">CoalReserves</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

        </table>
      </td>
   </tr>
</xsl:when>

<xsl:when test="$GAAPDomain = $GAMING">
 <tr class="data">
     <td class="data" COLSPAN="3">
       <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
         <tr valign="bottom" class="">
           <td colspan="2" NOWRAP="1" class="datashade table1_item">
               <span class="defaultbold">Financial Performance</span>
           </td>
           <td align="right" class="datashade table1_item">
               <span class="defaultbold">
                   <xsl:if test="not(normalize-space(Company/Ticker))">
                       <xsl:value-of select="Company/InstnName"/>
                   </xsl:if>
                   <xsl:value-of select="Company/Ticker"/>
               </span>
           </td>
           <td align="right" class="datashade table1_item" nowrap="1">
               <span class="defaultbold">
                   Peer<br/>Median
               </span>
           </td>
           <td align="right" class="datashade table1_item" nowrap="1">
               <span class="defaultbold">
                   Peer<br/>Average
               </span>
           </td>
         </tr>
        <xsl:call-template name="peerdatarow_3">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">ROAA</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">ROAA</xsl:with-param>
        <xsl:with-param name="ItemName">ROAA</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow_3">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">ROAE</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">ROAE</xsl:with-param>
        <xsl:with-param name="ItemName">ROAE</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow_3">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">GrossMarginDirectGaming</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Gaming Margin</xsl:with-param>
        <xsl:with-param name="ItemName">GrossMarginDirectGaming</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow_3">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">GrossMarginHotel</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Hotel Margin</xsl:with-param>
        <xsl:with-param name="ItemName">GrossMarginHotel</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
        <xsl:call-template name="peerdatarow_3">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">GrossMarginOperatingGaming</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Total Operating Margin</xsl:with-param>
        <xsl:with-param name="ItemName">GrossMarginOperatingGaming</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow_3">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">EBITDACoverageRealEstate</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">EBITDA / Interest Expense</xsl:with-param>
        <xsl:with-param name="ItemName">EBITDACoverageRealEstate</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
       </table><br />
     </td>
   </tr>
   <tr class="data">
       <td class="data" COLSPAN="3">
         <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
         <tr valign="bottom" class="">
             <td align="left" colspan="2" NOWRAP="1" class="datashade table1_item">
                 <span class="defaultbold">Gaming Growth Ratios (%)</span>
             </td>
           <td align="right" class="datashade table1_item">&#160;</td>
           <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
           <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
         </tr>

    <xsl:call-template name="peerdatarow_3">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">NetIncomeGrowth</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Net Income Growth</xsl:with-param>
        <xsl:with-param name="ItemName">NetIncomeGrowth</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
    <xsl:call-template name="peerdatarow_3">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">NOIGrowth</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">NOI Growth</xsl:with-param>
        <xsl:with-param name="ItemName">NOIGrowth</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
    <xsl:call-template name="peerdatarow_3">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">NetRevenueGrowth</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Net Revenue Growth</xsl:with-param>
        <xsl:with-param name="ItemName">NetRevenueGrowth</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
    <xsl:call-template name="peerdatarow_3">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">RevenueGrowthGaming</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Gaming Revenue Growth</xsl:with-param>
        <xsl:with-param name="ItemName">RevenueGrowthGaming</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
    <xsl:call-template name="peerdatarow_3">
        <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
        <xsl:with-param name="SNLItemName">HotelRevenueGrowth</xsl:with-param>
        <xsl:with-param name="EOP">0</xsl:with-param>
        <xsl:with-param name="DisplayName">Hotel Revenue Growth</xsl:with-param>
        <xsl:with-param name="ItemName">HotelRevenueGrowth</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

        </table><br />
      </td>
   </tr>
   <tr class="data">
          <td class="data" COLSPAN="3">
            <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
         <tr valign="bottom" class="">
                <td align="left" colspan="2" NOWRAP="1" class="datashade table1_item">
                    <span class="defaultbold">Gaming Ratios (%)</span>
                </td>
              <td align="right" class="datashade table1_item">&#160;</td>
              <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
              <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
            </tr>

      <xsl:call-template name="peerdatarow_3">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">GamingRevenueToRevenue</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName">Gaming Revs/Total Revenues Gaming</xsl:with-param>
          <xsl:with-param name="ItemName">GamingRevenueToRevenue</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow_3">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">HotelRevenueToRevenue</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName">Hotel Revs/Total Revenues Gaming</xsl:with-param>
          <xsl:with-param name="ItemName">HotelRevenueToRevenue</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow_3">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">GamingExpenseToExpense</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName">Gaming Exp/Total Expenses Gaming</xsl:with-param>
          <xsl:with-param name="ItemName">GamingExpenseToExpense</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow_3">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">HotelExpenseToExpense</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName">Hotel Exp/Total Expenses Gaming</xsl:with-param>
          <xsl:with-param name="ItemName">HotelExpenseToExpense</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

            </table><br />
          </td>
   </tr>
   <tr class="data">
          <td class="data" COLSPAN="3">
            <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
         <tr valign="bottom" class="">
                <td align="left" colspan="2" NOWRAP="1" class="datashade table1_item">
                    <span class="defaultbold">Capitalization - Debt Ratios</span>
                </td>
              <td align="right" class="datashade table1_item">&#160;</td>
              <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
              <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
            </tr>

      <xsl:call-template name="peerdatarow_3">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">DebtToTotalCapBookValue</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName">Debt/ Book Cap</xsl:with-param>
          <xsl:with-param name="ItemName">DebtToTotalCapBookValue</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow_3">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">EquityToAssets</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName">Equity/ Assets</xsl:with-param>
          <xsl:with-param name="ItemName">EquityToAssets</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow_3">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">DebtAndSubDebtToAssets</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName">Debt/Assets</xsl:with-param>
          <xsl:with-param name="ItemName">DebtAndSubDebtToAssets</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow_3">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">DebtLongTermToDebt</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName">Long-term Debt/ Debt</xsl:with-param>
          <xsl:with-param name="ItemName">DebtLongTermToDebt</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow_3">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">DebtSeniorToDebt</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName">Senior Debt/Total Debt</xsl:with-param>
          <xsl:with-param name="ItemName">DebtSeniorToDebt</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

            </table><br />
          </td>
   </tr>
   <tr class="data">
          <td class="data" COLSPAN="3">
           <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
         <tr valign="bottom" class="">
                <td align="left" colspan="2" NOWRAP="1" class="datashade table1_item">
                    <span class="defaultbold">Market Ratios</span>
                </td>
              <td align="right" class="datashade table1_item">&#160;</td>
              <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
              <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
            </tr>

      <xsl:call-template name="peerdatarow_3">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">PriceToEarnings</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName">Price/ Earnings</xsl:with-param>
          <xsl:with-param name="ItemName">PriceToEarnings</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow_3">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">PriceToEarningsLTMAfterExtra</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName">Price/ LTM Earnings</xsl:with-param>
          <xsl:with-param name="ItemName">PriceToEarningsLTMAfterExtra</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow_3">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">PriceToBook</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName">Price/ Book</xsl:with-param>
          <xsl:with-param name="ItemName">PriceToBook</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      <xsl:call-template name="peerdatarow_3">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">DivYield</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName">Dividend Yield</xsl:with-param>
          <xsl:with-param name="ItemName">DivYield</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

            </table><br />
          </td>
   </tr>
   <tr class="data">
          <td class="data" COLSPAN="3">
            <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
         <tr valign="bottom" class="">
                <td align="left" colspan="2" NOWRAP="1" class="datashade table1_item">
                    <span class="defaultbold">Miscellaneous</span>
                </td>
              <td align="right" class="datashade table1_item">&#160;</td>
              <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
              <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
            </tr>

      <xsl:call-template name="peerdatarow_3">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">PropertyCountCasino</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName"># of Casino Properties</xsl:with-param>
          <xsl:with-param name="ItemName">PropertyCountCasino</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0;(#,##0)</xsl:with-param></xsl:call-template>

      <xsl:call-template name="peerdatarow_3">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">GamingSlotCount</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName"># of Slots</xsl:with-param>
          <xsl:with-param name="ItemName">GamingSlotCount</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0;(#,##0)</xsl:with-param></xsl:call-template>

      <xsl:call-template name="peerdatarow_3">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">GamingTableCount</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName"># of Tables</xsl:with-param>
          <xsl:with-param name="ItemName">GamingTableCount</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0;(#,##0)</xsl:with-param></xsl:call-template>

      <xsl:call-template name="peerdatarow_3">
          <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
          <xsl:with-param name="SNLItemName">PropertySizeRooms</xsl:with-param>
          <xsl:with-param name="EOP">0</xsl:with-param>
          <xsl:with-param name="DisplayName"># of Hotel Rooms</xsl:with-param>
          <xsl:with-param name="ItemName">PropertySizeRooms</xsl:with-param>
      <xsl:with-param name="NumberFormatString">#,##0;(#,##0)</xsl:with-param></xsl:call-template>

            </table><br />
          </td>
   </tr>
</xsl:when>

  <xsl:when test="$GAAPDomain = $MEDIA_ENT or $GAAPDomain = $COMMUNICATIONS or $GAAPDomain = $NEWMEDIA">
    <tr class="data">
      <td class="data" COLSPAN="3">
        <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
          <tr valign="bottom" class="">
            <td colspan="2" NOWRAP="1" class="datashade table1_item">
                <span class="defaultbold">Performance Ratios (%)</span>
            </td>
            <td align="right" class="datashade table1_item">
                <span class="defaultbold">
                    <xsl:if test="not(normalize-space(Company/Ticker))">
                        <xsl:value-of select="Company/InstnName"/>
                    </xsl:if>
                    <xsl:value-of select="Company/Ticker"/>
                </span>
            </td>
            <td align="right" class="datashade table1_item" nowrap="1">
                <span class="defaultbold">
                    Peer<br/>Median
                </span>
            </td>
            <td align="right" class="datashade table1_item" nowrap="1">
                <span class="defaultbold">
                    Peer<br/>Average
                </span>
            </td>
          </tr>
          <xsl:call-template name="peerdatarow_3">
            <xsl:with-param name="KeyInstn">
              <xsl:value-of select="Company/KeyInstn"/>
            </xsl:with-param>
            <xsl:with-param name="SNLItemName">ROAE</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">ROAE</xsl:with-param>
            <xsl:with-param name="ItemName">ROAE</xsl:with-param>
            <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param>
          </xsl:call-template>

          <xsl:call-template name="peerdatarow_3">
            <xsl:with-param name="KeyInstn">
              <xsl:value-of select="Company/KeyInstn"/>
            </xsl:with-param>
            <xsl:with-param name="SNLItemName">RecurringEBITDAMarginComm</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">Recurring EBITDA Margin</xsl:with-param>
            <xsl:with-param name="ItemName">RecurringEBITDAMarginComm</xsl:with-param>
            <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param>
          </xsl:call-template>

          <xsl:call-template name="peerdatarow_3">
            <xsl:with-param name="KeyInstn">
              <xsl:value-of select="Company/KeyInstn"/>
            </xsl:with-param>
            <xsl:with-param name="SNLItemName">EfficiencyRatio</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">Efficiency Ratio</xsl:with-param>
            <xsl:with-param name="ItemName">EfficiencyRatio</xsl:with-param>
            <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param>
          </xsl:call-template>

          <xsl:call-template name="peerdatarow_3">
            <xsl:with-param name="KeyInstn">
              <xsl:value-of select="Company/KeyInstn"/>
            </xsl:with-param>
            <xsl:with-param name="SNLItemName">RecurringLeveredFCFMarginComm</xsl:with-param>
            <xsl:with-param name="EOP">1</xsl:with-param>
            <xsl:with-param name="DisplayName">Recurring Levered FCF Margin</xsl:with-param>
            <xsl:with-param name="ItemName">RecurringLeveredFCFMarginComm</xsl:with-param>
            <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param>
          </xsl:call-template>

          <xsl:call-template name="peerdatarow_3">
            <xsl:with-param name="KeyInstn">
              <xsl:value-of select="Company/KeyInstn"/>
            </xsl:with-param>
            <xsl:with-param name="SNLItemName">NetIncomeMargin</xsl:with-param>
            <xsl:with-param name="EOP">1</xsl:with-param>
            <xsl:with-param name="DisplayName">Net Income Margin</xsl:with-param>
            <xsl:with-param name="ItemName">NetIncomeMargin</xsl:with-param>
            <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param>
          </xsl:call-template>

        </table>
        <br />
      </td>
    </tr>
    <tr class="data">
      <td class="data" COLSPAN="3">
        <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
          <tr valign="bottom" class="">
            <td align="left" colspan="2" NOWRAP="1" class="datashade table1_item">
                <span class="defaultbold">Leverage Ratios (%)</span>
            </td>
            <td align="right" class="datashade table1_item">&#160;</td>
            <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
            <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
          </tr>

          <xsl:call-template name="peerdatarow_3">
            <xsl:with-param name="KeyInstn">
              <xsl:value-of select="Company/KeyInstn"/>
            </xsl:with-param>
            <xsl:with-param name="SNLItemName">DebtToEBITDARecurring</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">Debt/ Recurring EBITDA</xsl:with-param>
            <xsl:with-param name="ItemName">DebtToEBITDARecurring</xsl:with-param>
            <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param>
          </xsl:call-template>

          <xsl:call-template name="peerdatarow_3">
            <xsl:with-param name="KeyInstn">
              <xsl:value-of select="Company/KeyInstn"/>
            </xsl:with-param>
            <xsl:with-param name="SNLItemName">NetDebtPrefToEBITDARecurring</xsl:with-param>
            <xsl:with-param name="EOP">1</xsl:with-param>
            <xsl:with-param name="DisplayName">Net Debt and Preferred/ Recurring EBITDA</xsl:with-param>
            <xsl:with-param name="ItemName">NetDebtPrefToEBITDARecurring</xsl:with-param>
            <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param>
          </xsl:call-template>
        </table>
        <br />
      </td>
    </tr>
    <tr class="data">
      <td class="data" COLSPAN="3">
        <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
          <tr valign="bottom" class="">
            <td align="left" colspan="2" NOWRAP="1" class="datashade table1_item">
                <span class="defaultbold">Coverage Ratios (%)</span>
            </td>
            <td align="right" class="datashade table1_item">&#160;</td>
            <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
            <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
          </tr>
          <xsl:call-template name="peerdatarow_3">
            <xsl:with-param name="KeyInstn">
              <xsl:value-of select="Company/KeyInstn"/>
            </xsl:with-param>
            <xsl:with-param name="SNLItemName">EBITDACoverageRecurring</xsl:with-param>
            <xsl:with-param name="EOP">1</xsl:with-param>
            <xsl:with-param name="DisplayName">Recurring EBITDA/ Interest Expense</xsl:with-param>
            <xsl:with-param name="ItemName">EBITDACoverageRecurring</xsl:with-param>
            <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="peerdatarow_3">
            <xsl:with-param name="KeyInstn">
              <xsl:value-of select="Company/KeyInstn"/>
            </xsl:with-param>
            <xsl:with-param name="SNLItemName">EBITDACoverageRecurringPfd</xsl:with-param>
            <xsl:with-param name="EOP">1</xsl:with-param>
            <xsl:with-param name="DisplayName">Recurring EBITDA/ Interest Expense plus Pfd</xsl:with-param>
            <xsl:with-param name="ItemName">EBITDACoverageRecurringPfd</xsl:with-param>
            <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param>
          </xsl:call-template>
        </table>
        <br />
      </td>
    </tr>
    <tr class="data">
      <td class="data" COLSPAN="3">
        <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
          <tr valign="bottom" class="">
            <td align="left" colspan="2" NOWRAP="1" class="datashade table1_item">
                <span class="defaultbold">Market Ratios (x)</span>
            </td>
            <td align="right" class="datashade table1_item">&#160;</td>
            <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
            <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
          </tr>
          <xsl:call-template name="peerdatarow_3">
            <xsl:with-param name="KeyInstn">
              <xsl:value-of select="Company/KeyInstn"/>
            </xsl:with-param>
            <xsl:with-param name="SNLItemName">PriceToEarningsLTMAfterExtra</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">Price/LTM EPS</xsl:with-param>
            <xsl:with-param name="ItemName">PriceToEarningsLTMAfterExtra</xsl:with-param>
            <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="peerdatarow_3">
            <xsl:with-param name="KeyInstn">
              <xsl:value-of select="Company/KeyInstn"/>
            </xsl:with-param>
            <xsl:with-param name="SNLItemName">TEVToRecurringEBITDALTM</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">TEV / Recurring EBITDA (LTM)</xsl:with-param>
            <xsl:with-param name="ItemName">TEVToRecurringEBITDALTM</xsl:with-param>
            <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="peerdatarow_3">
            <xsl:with-param name="KeyInstn">
              <xsl:value-of select="Company/KeyInstn"/>
            </xsl:with-param>
            <xsl:with-param name="SNLItemName">PriceToRecurringFCFLTM</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">Price / Recurring Levered FCF (LTM)</xsl:with-param>
            <xsl:with-param name="ItemName">PriceToRecurringFCFLTM</xsl:with-param>
            <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="peerdatarow_3">
            <xsl:with-param name="KeyInstn">
              <xsl:value-of select="Company/KeyInstn"/>
            </xsl:with-param>
            <xsl:with-param name="SNLItemName">DivYield</xsl:with-param>
            <xsl:with-param name="EOP">0</xsl:with-param>
            <xsl:with-param name="DisplayName">Dividend Yield (%)</xsl:with-param>
            <xsl:with-param name="ItemName">DividendYield</xsl:with-param>
            <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param>
          </xsl:call-template>

        </table>
      </td>
    </tr>
  </xsl:when>


  <xsl:otherwise>
  <TR class="data">
    <TD class="data" COLSPAN="3">
      <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
         <tr valign="bottom" class="">
          <td colspan="2" NOWRAP="1" class="datashade table1_item">
              <span class="defaultbold">Financial Performance</span>
          </td>
          <td align="right" class="datashade table1_item">
              <span class="defaultbold">
                  <xsl:if test="not(normalize-space(Company/Ticker))">
                      <xsl:value-of select="Company/InstnName"/>
                  </xsl:if>
                  <xsl:value-of select="Company/Ticker"/>
              </span>
          </td>
          <td align="right" class="datashade table1_item" nowrap="1">
              <span class="defaultbold">
                  Peer<br/>Median
              </span>
          </td>
          <td align="right" class="datashade table1_item" nowrap="1">
              <span class="defaultbold">
                  Peer<br/>Average
              </span>
          </td>
        </tr>

    <xsl:call-template name="peerdatarow_3">
    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
    <xsl:with-param name="SNLItemName">ROAA</xsl:with-param>
    <xsl:with-param name="EOP">0</xsl:with-param>
    <xsl:with-param name="DisplayName">ROAA</xsl:with-param>
    <xsl:with-param name="ItemName">ROAA</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

    <xsl:call-template name="peerdatarow_3">
    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
    <xsl:with-param name="SNLItemName">ROAE</xsl:with-param>
    <xsl:with-param name="EOP">0</xsl:with-param>
    <xsl:with-param name="DisplayName">ROAE</xsl:with-param>
    <xsl:with-param name="ItemName">ROAE</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

    <xsl:call-template name="peerdatarow_3">
    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
    <xsl:with-param name="SNLItemName">GrossMarginTech</xsl:with-param>
    <xsl:with-param name="EOP">0</xsl:with-param>
    <xsl:with-param name="DisplayName">Gross Sales Margin</xsl:with-param>
    <xsl:with-param name="ItemName">GrossMarginTech</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

    <xsl:call-template name="peerdatarow_3">
    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
    <xsl:with-param name="SNLItemName">OperatingMarginTech</xsl:with-param>
    <xsl:with-param name="EOP">0</xsl:with-param>
    <xsl:with-param name="DisplayName">Operating Sales Margin</xsl:with-param>
    <xsl:with-param name="ItemName">OperatingMarginTech</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

    <xsl:call-template name="peerdatarow_3">
    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
    <xsl:with-param name="SNLItemName">SalesTechPerEmployee</xsl:with-param>
    <xsl:with-param name="EOP">0</xsl:with-param>
    <xsl:with-param name="DisplayName">Sales/ Employee ($000)</xsl:with-param>
    <xsl:with-param name="ItemName">SalesTechPerEmployee</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

    <xsl:call-template name="peerdatarow_3">
    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
    <xsl:with-param name="SNLItemName">EBITDACoverage</xsl:with-param>
    <xsl:with-param name="EOP">0</xsl:with-param>
    <xsl:with-param name="DisplayName">EBITDA/ Interest Expense (x)</xsl:with-param>
    <xsl:with-param name="ItemName">EBITDACoverage</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

    <xsl:call-template name="peerdatarow_3">
    <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
    <xsl:with-param name="SNLItemName">EBITDAToIncome</xsl:with-param>
    <xsl:with-param name="EOP">0</xsl:with-param>
    <xsl:with-param name="DisplayName">EBITDA/ Pre-tax Earnings (x)</xsl:with-param>
    <xsl:with-param name="ItemName">EBITDAToIncome</xsl:with-param>
    <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
      </table><br />
    </TD>
  </TR>
  <TR class="data">
      <TD class="data" COLSPAN="3">
        <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
         <tr valign="bottom" class="">
            <td align="left" colspan="2" NOWRAP="1" class="datashade table1_item">
                <span class="defaultbold">Balance Sheet Ratios (%)</span>
            </td>
          <td align="right" class="datashade table1_item">&#160;</td>
          <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
          <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
        </tr>

                    <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">EquityToAssets</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Total Equity / Total Assets</xsl:with-param>
                  <xsl:with-param name="ItemName">EquityToAssets</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">DebtToEquity</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Debt / Equity (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">DebtToEquity</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">CurrentRatio</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Current Ratio (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">CurrentRatio</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">IntangiblesToEquity</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Intangibles/ Equity</xsl:with-param>
                  <xsl:with-param name="ItemName">IntangiblesToEquity</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>

        </table><br />
      </TD>
  </TR>
    <TR class="data">
        <TD class="data" COLSPAN="3">
          <table border="0" cellspacing="0" cellpadding="2" width="100%" ID="Table2" class="table2">
         <tr valign="bottom" class="">
              <td align="left" colspan="2" NOWRAP="1" class="datashade table1_item">
                  <span class="defaultbold">Market Ratios</span>
              </td>
          <td align="right" class="datashade table1_item">&#160;</td>
          <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
          <td align="right" class="datashade table1_item" nowrap="1">&#160;</td>
        </tr>
                    <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">PriceToEarningsLTMAfterExtra</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Price / Earnings (x)</xsl:with-param>
                  <xsl:with-param name="ItemName">PriceToEarningsLTMAfterExtra</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">PriceToBook</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Price / Book (%)</xsl:with-param>
                  <xsl:with-param name="ItemName">PriceToBook</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
                    <xsl:call-template name="peerdatarow_3">
                  <xsl:with-param name="KeyInstn"><xsl:value-of select="Company/KeyInstn"/></xsl:with-param>
                  <xsl:with-param name="SNLItemName">DivYield</xsl:with-param>
                  <xsl:with-param name="EOP">0</xsl:with-param>
                  <xsl:with-param name="DisplayName">Dividend Yield (%)</xsl:with-param>
                  <xsl:with-param name="ItemName">DividendYield</xsl:with-param>
              <xsl:with-param name="NumberFormatString">#,##0.00;(#,##0.00)</xsl:with-param></xsl:call-template>
          </table>
        </TD>
  </TR>
  </xsl:otherwise>
</xsl:choose>
<tr>
	<td class="data" align="left" valign="top">
		<span class="default"><i>
		<xsl:variable name="MyDES" select="Median/DateEndedStandard"/>
		<xsl:value-of select="Company/InstnName"/>'s peer group consists of the following:
		<xsl:for-each select="PeerData">
		<xsl:value-of select="InstnName"/>
		<xsl:if test="Ticker != ''"> (<xsl:value-of select="Ticker"/>)</xsl:if>
		<xsl:if test="position() &lt; count(../PeerData)">, </xsl:if>
		</xsl:for-each>
		<br/><br/><xsl:value-of select="Company/InstnName"/>'s financial data is as of <xsl:value-of select="substring-before(CompanyFinancials/DateEndedStandard, ' ')"/>.
		<br/><br/>Peer financial data is as of <xsl:value-of select="substring-before($MyDES, ' ')"/>
		<xsl:if test="count(PeerData[DateEndedStandard != $MyDES]) &gt; 0">
        &#160;except for these companies:<br/><br/>
        <xsl:for-each select="PeerData"><xsl:if test="substring-before(DateEndedStandard, ' ') != substring-before($MyDES, ' ')"><xsl:value-of select="InstnName"/>'s financial data is as of <xsl:value-of select="substring-before(DateEndedStandard, ' ')"/>.<br/></xsl:if></xsl:for-each></xsl:if></i></span>
	</td>
</tr>
</table>
</xsl:template>
<xsl:template name="PEER_HEAD_3">
<TR class="data">
  <TD class="data" colspan="3" valign="top"><img src="/images/Interactive/blank.gif" width="220" height="8" alt=""/><BR/><span class="default"><i>Data for trailing four quarters</i></span><BR/> <IMG SRC="/images/interactive/blank.gif" WIDTH="150" HEIGHT="1"/><BR/></TD>
</TR>
<tr class="data">
  <td class="data" colspan="3"><span class="default">For the definition of a financial field, select the field name below.</span></td>
</tr>
</xsl:template>
<!-- END TEMPLATE THREE ends here. -->












<!-- Main XSLT templates are here. these are the templates which are actually being displayed in the page. this is for TOP LEVEL editing. if you dont need to use the individual templates above you can do your main editing here when pulling THESE templates below you must carry the top comment with them and uncomment them when you drop it into the company specific xslt.
-->




<xsl:template match="irw:Peer">
<xsl:variable name="TemplateName" select="Company/TemplateName"/>

<!-- old code -->
<!--<SCRIPT LANGUAGE="JavaScript">
function DefWindow(KeyInstn, ItemName, EOP) {
  var page = "definitions.aspx?IID=" + KeyInstn + "&amp;Item=" + ItemName + "&amp;EOP=" + EOP;
  var winprops = "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=300,height=100";
  popup = window.open(page, "Definitions", winprops);
}
//  End -->
<!--</SCRIPT>-->

<!-- Updated code for UPREIT MARKET and TOTAL CAPITALIZATION'S definition 12/28/07 (RC) -->
<script language="javascript">
<![CDATA[
function DefWindow(KeyInstn, ItemName, EOP) {
var popup;
var winprops = "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=300,height=100";
var page = "definitions.aspx?IID=" + KeyInstn + "&Item=" + ItemName + "&EOP=" + EOP;
popup = window.open(page, "Definitions", winprops);
	if(ItemName == 'UPREITMarketCap' || ItemName == 'TotalCap'){
		popup.document.open();
			popup.document.write ('<html><head><title>');

			if(ItemName == 'UPREITMarketCap'){
				popup.document.write ('UPREIT Market Capitalization');
			} else if(ItemName == 'TotalCap'){
				popup.document.write ('Total Capitalization');
			}

			popup.document.write ('</title><link href="/Interactive/LookAndFeel/IRStyles/live_'+KeyInstn+'.css" REL="stylesheet" TYPE="text/css">');
			popup.document.write ('</head><body onBlur=\'window.close()\' class=\'data\'><table border="0" cellspacing="0" cellpadding="3" width="100%" class="data"><tr align="left"><td class=\'data\'>');

			if(ItemName == 'UPREITMarketCap'){
				popup.document.write ('Market capitalization of common equity, assuming the conversion of all convertible subsidiary equity into common');
			} else if(ItemName == 'TotalCap'){
				popup.document.write ('Total capitalization of the company, including debt, book value of any preferred issued by the company or subsidiaries, and the market value of common stock including the effect of any convertible subsidiary equity');
			}
			popup.document.write ('</td></tr></table></body></html>');
		popup.document.close();
	}
	popup.focus();
}
]]>
</script>

<xsl:choose>
<!-- Template 1 -->
<xsl:when test="$TemplateName = 'AltPeer1'">

<!-- Below script is repeating -->
<!--<SCRIPT LANGUAGE="JavaScript">
function DefWindow(KeyInstn, ItemName, EOP) {
  var page = "definitions.aspx?IID=" + KeyInstn + "&amp;Item=" + ItemName + "&amp;EOP=" + EOP;
  var winprops = "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=300,height=100";
  popup = window.open(page, "Definitions", winprops);
}
//  End -->
<!--</SCRIPT>-->

  <xsl:call-template name="TemplateONEstylesheet" />
  <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" CLASS="">
    <xsl:call-template name="Title_S2"/>
    <TR ALIGN="CENTER">
      <TD CLASS="leftTOPbord" colspan="2">
        <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="4" CLASS="data" width="100%" ID="Table1">
          <TR>
            <TD class="">
              <table BORDER="0" CELLSPACING="0" CELLPADDING="4" CLASS="data" width="100%">
                <xsl:call-template name="PEER_HEAD_2"/>
                <xsl:call-template name="PEERDATA_2"/>
                <TR class="data" ALIGN="left">
                  <TD colspan="2">
                    <span class="default">
                      <i>
                        <xsl:variable name="MyDES" select="Median/DateEndedStandard"/>
                        <xsl:value-of select="Company/InstnName"/>'s peer group consists of the following:
                        <xsl:for-each select="PeerData">
                          <xsl:value-of select="InstnName"/>
                          <xsl:if test="Ticker != ''">
                            (<xsl:value-of select="Ticker"/>)
                          </xsl:if>
                          <xsl:if test="position() &lt; count(../PeerData)">, </xsl:if>
                        </xsl:for-each>
                        <br/><br/>
                        <xsl:value-of select="Company/InstnName"/>'s financial data is as of <xsl:value-of select="substring-before(CompanyFinancials/DateEndedStandard, ' ')"/>.
                        <br/><br/>
                        Peer financial data is as of <xsl:value-of select="substring-before($MyDES, ' ')"/>
                        <xsl:if test="count(PeerData[DateEndedStandard != $MyDES]) &gt; 0">
                          &#160;except for these companies:<br/><br/>
                          <xsl:for-each select="PeerData">
                            <xsl:if test="substring-before(DateEndedStandard, ' ') != substring-before($MyDES, ' ')">
                              <xsl:value-of select="InstnName"/>'s financial data is as of <xsl:value-of select="substring-before(DateEndedStandard, ' ')"/>.<br/>
                            </xsl:if>
                          </xsl:for-each>
                        </xsl:if>
                      </i>
                    </span>
                  </TD>
                </TR>
              </table>
            </TD>
          </TR>
          <TR class="data" ALIGN="left">
            <TD colspan="2">
              <span class="default">
                <xsl:value-of select="Company/Footer" disable-output-escaping="yes"/>
              </span>
            </TD>
          </TR>
        </TABLE>
      </TD>
    </TR>
  </TABLE>
  <xsl:call-template name="Copyright"/>
</xsl:when>
<!-- End Template 1 -->

<!-- Template 3 -->
<xsl:when test="$TemplateName = 'AltPeer3'">
<!-- Below script is repeating -->
<!--<SCRIPT LANGUAGE="JavaScript">
function DefWindow(KeyInstn, ItemName, EOP) {
  var page = "definitions.aspx?IID=" + KeyInstn + "&amp;Item=" + ItemName + "&amp;EOP=" + EOP;
  var winprops = "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=300,height=100";
  popup = window.open(page, "Definitions", winprops);
}
//  End -->
<!--</SCRIPT>-->

<xsl:call-template name="TemplateTHREEstylesheet" />
<xsl:call-template name="Title_T3"/>
<table BORDER="0" CELLSPACING="0" CELLPADDING="3" CLASS="table2" width="100%">
<tr>
	<td class="data" align="left" valign="top">
		<xsl:call-template name="PEERDATA_3"/>
	</td>
</tr>
<xsl:if test="Company/Footer != ''">
<tr>
	<td class="data" align="left" valign="top"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span></td>
</tr>
</xsl:if>
</table>

<xsl:call-template name="Copyright"/>
</xsl:when>
<!-- End Template 3 -->


<xsl:otherwise>
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" CLASS="colordark">
<xsl:call-template name="Title_S1"/>
  <TR ALIGN="CENTER">
    <TD CLASS="colordark" colspan="2">
      <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="4" CLASS="data" width="100%" ID="Table1">
	    <xsl:call-template name="PEER_HEAD"/>
        <xsl:call-template name="PEERDATA"/>
        <TR class="colorlight" ALIGN="left">
          <TD colspan="2">
            <span class="default"><i>
            <xsl:variable name="MyDES" select="Median/DateEndedStandard"/>
            <xsl:value-of select="Company/InstnName"/>'s peer group consists of the following:
            <xsl:for-each select="PeerData">
            <xsl:value-of select="InstnName"/>
            <xsl:if test="Ticker != ''"> (<xsl:value-of select="Ticker"/>)</xsl:if><xsl:if test="position() &lt; count(../PeerData)">, </xsl:if>
            </xsl:for-each>
            <br/><br/>
            <xsl:value-of select="Company/InstnName"/>'s financial data is as of <xsl:value-of select="substring-before(CompanyFinancials/DateEndedStandard, ' ')"/>.
            <br/><br/>Peer financial data is as of <xsl:value-of select="substring-before($MyDES, ' ')"/>
            <xsl:if test="count(PeerData[DateEndedStandard != $MyDES]) &gt; 0">
              &#160;except for these companies:<br/><br/>
            <xsl:for-each select="PeerData">
            <xsl:if test="substring-before(DateEndedStandard, ' ') != substring-before($MyDES, ' ')">
            <xsl:value-of select="InstnName"/>'s financial data is as of <xsl:value-of select="substring-before(DateEndedStandard, ' ')"/>.<br/>
            </xsl:if>
            </xsl:for-each>
            </xsl:if>
            </i></span>
          </TD>
        </TR>
        <xsl:if test="Company/Footer != ''">
        <TR class="colorlight" ALIGN="left">
          <TD colspan="2"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span></TD>
        </TR>
        </xsl:if>
      </TABLE>
    </TD>
  </TR>
</TABLE>
<xsl:call-template name="Copyright"/>
</xsl:otherwise>
</xsl:choose>
</xsl:template>
</xsl:stylesheet>