﻿<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
]>

<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.ord/TR/REC-html40"
    xmlns:v="urn:schemas-microsoft-com:vml"
	xmlns:irw="http://www.snl.com/xml/irw"
  xmlns:ms="urn:schemas-microsoft-com:xslt"
  xmlns:dt="urn:schemas-microsoft-com:datatypes">

<xsl:output method="html" media-type="text/html" indent="no"/>
  <xsl:variable name="lower" select="'abcdefghijklmnopqrstuvwxyz'"/>
  <xsl:variable name="upper" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>

  <xsl:variable name="show_logo">0</xsl:variable>
  <xsl:variable name="logo_url">images/Title_Left.gif</xsl:variable>
  <xsl:variable name="logo_width">200</xsl:variable>
  <xsl:variable name="logo_height">78</xsl:variable>

  <!-- **********************************************************************
 Global Style variables (can be customized): '' for using browser's default
     ********************************************************************** -->

  <xsl:variable name="global_font">arial,sans-serif</xsl:variable>
  <xsl:variable name="global_font_size"></xsl:variable>
  <xsl:variable name="global_bg_color">#ffffff</xsl:variable>
  <xsl:variable name="global_text_color">#000000</xsl:variable>
  <xsl:variable name="global_link_color">#0000cc</xsl:variable>
  <xsl:variable name="global_vlink_color">#551a8b</xsl:variable>
  <xsl:variable name="global_alink_color">#ff0000</xsl:variable>


  <!-- **********************************************************************
 Result page components (can be customized)
     - whether to show a component: 0 for FALSE, non-zero (e.g., 1) for TRUE
     - text and style
     ********************************************************************** -->

  <!-- *** choose result page header: '', 'provided', 'mine', or 'both' *** -->
  <xsl:variable name="choose_result_page_header">both</xsl:variable>

  <!-- *** customize provided result page header *** -->
  <xsl:variable name="show_swr_link">1</xsl:variable>
  <xsl:variable name="swr_search_anchor_text">Search Within Results</xsl:variable>
  <xsl:variable name="show_result_page_adv_link">0</xsl:variable>
  <xsl:variable name="adv_search_anchor_text">Advanced Search</xsl:variable>
  <xsl:variable name="show_result_page_help_link">0</xsl:variable>
  <xsl:variable name="search_help_anchor_text">Search Tips</xsl:variable>

  <!-- *** search boxes *** -->
  <xsl:variable name="show_top_search_box">0</xsl:variable>
  <xsl:variable name="show_bottom_search_box">0</xsl:variable>
  <xsl:variable name="search_box_size">32</xsl:variable>

  <!-- *** choose search button type: 'text' or 'image' *** -->
  <xsl:variable name="choose_search_button">text</xsl:variable>
  <xsl:variable name="search_button_text">Google Search</xsl:variable>
  <xsl:variable name="search_button_image_url"></xsl:variable>
  <xsl:variable name="search_collections_xslt"></xsl:variable>

  <!-- *** search info bars *** -->
  <xsl:variable name="show_search_info">1</xsl:variable>

  <!-- *** choose separation bar: 'ltblue', 'blue', 'line', 'nothing' *** -->
  <xsl:variable name="choose_sep_bar">nothing</xsl:variable>
  <xsl:variable name="sep_bar_std_text">Search</xsl:variable>
  <xsl:variable name="sep_bar_adv_text">Advanced Search</xsl:variable>
  <xsl:variable name="sep_bar_error_text">Error</xsl:variable>

  <!-- *** navigation bars: '', 'google', 'link', or 'simple'*** -->
  <xsl:variable name="show_top_navigation">0</xsl:variable>
  <xsl:variable name="choose_bottom_navigation">link</xsl:variable>
  <xsl:variable name="my_nav_align">right</xsl:variable>
  <xsl:variable name="my_nav_size">-1</xsl:variable>
  <xsl:variable name="my_nav_color">#6f6f6f</xsl:variable>

  <!-- *** sort by date/relevance *** -->
  <xsl:variable name="show_sort_by">0</xsl:variable>

  <!-- *** spelling suggestions *** -->
  <xsl:variable name="show_spelling">1</xsl:variable>
  <xsl:variable name="spelling_text">Did you mean:</xsl:variable>
  <xsl:variable name="spelling_text_color">#cc0000</xsl:variable>

  <!-- *** synonyms suggestions *** -->
  <xsl:variable name="show_synonyms">1</xsl:variable>
  <xsl:variable name="synonyms_text">You could also try:</xsl:variable>
  <xsl:variable name="synonyms_text_color">#cc0000</xsl:variable>

  <!-- *** keymatch suggestions *** -->
  <xsl:variable name="show_keymatch">1</xsl:variable>
  <xsl:variable name="keymatch_text">KeyMatch</xsl:variable>
  <xsl:variable name="keymatch_text_color">#2255aa</xsl:variable>
  <xsl:variable name="keymatch_bg_color">#e8e8ff</xsl:variable>

  <!-- *** Google Desktop integration *** -->
  <xsl:variable name="egds_show_search_tabs">1</xsl:variable>
  <xsl:variable name="egds_appliance_tab_label">Appliance</xsl:variable>
  <xsl:variable name="egds_show_desktop_results">1</xsl:variable>

  <!-- *** onebox information *** -->
  <xsl:variable name="show_onebox">1</xsl:variable>

  <!-- *** analytics information *** -->
  <xsl:variable name="analytics_account"></xsl:variable>

  <!-- *** ASR enabling *** -->
  <xsl:variable name="show_asr">0</xsl:variable>

  <!-- **********************************************************************
 Result elements (can be customized)
     - whether to show an element ('1' for yes, '0' for no)
     - font/size/color ('' for using style of the context)
     ********************************************************************** -->

  <!-- *** result title and snippet *** -->
  <xsl:variable name="show_res_title">1</xsl:variable>
  <xsl:variable name="res_title_color">#0000cc</xsl:variable>
  <xsl:variable name="res_title_size"></xsl:variable>
  <xsl:variable name="show_res_snippet">1</xsl:variable>
  <xsl:variable name="res_snippet_size">80%</xsl:variable>

  <!-- *** keyword match (in title or snippet) *** -->
  <xsl:variable name="res_keyword_color"></xsl:variable>
  <xsl:variable name="res_keyword_size"></xsl:variable>
  <xsl:variable name="res_keyword_format">b</xsl:variable>
  <!-- 'b' for bold -->

  <!-- *** link URL *** -->
  <xsl:variable name="show_res_url">1</xsl:variable>
  <xsl:variable name="res_url_color">#008000</xsl:variable>
  <xsl:variable name="res_url_size">-1</xsl:variable>
  <xsl:variable name="truncate_result_urls">1</xsl:variable>
  <xsl:variable name="truncate_result_url_length">100</xsl:variable>

  <!-- *** misc elements *** -->
  <xsl:variable name="show_meta_tags">0</xsl:variable>
  <xsl:variable name="show_res_size">0</xsl:variable>
  <xsl:variable name="show_res_date">0</xsl:variable>
  <xsl:variable name="show_res_cache">0</xsl:variable>

  <!-- *** used in result cache link, similar pages link, and description *** -->
  <xsl:variable name="faint_color">#7777cc</xsl:variable>

  <!-- *** show secure results radio button *** -->
  <xsl:variable name="show_secure_radio">1</xsl:variable>

  <!-- **********************************************************************
 Other variables (can be customized)
     ********************************************************************** -->

  <!-- *** page title *** -->
  <xsl:variable name="front_page_title">Search Home</xsl:variable>
  <xsl:variable name="result_page_title">Search Results</xsl:variable>
  <xsl:variable name="adv_page_title">Advanced Search</xsl:variable>
  <xsl:variable name="error_page_title">Error</xsl:variable>
  <xsl:variable name="swr_page_title">Search Within Results</xsl:variable>

  <!-- *** choose adv_search page header: '', 'provided', 'mine', or 'both' *** -->
  <xsl:variable name="choose_adv_search_page_header">both</xsl:variable>

  <!-- *** cached page header text *** -->
  <xsl:variable name="cached_page_header_text">This is the cached copy of</xsl:variable>

  <!-- *** error message text *** -->
  <xsl:variable name="server_error_msg_text">A server error has occurred.</xsl:variable>
  <xsl:variable name="server_error_des_text">Check server response code in details.</xsl:variable>
  <xsl:variable name="xml_error_msg_text">Unknown XML result type.</xsl:variable>
  <xsl:variable name="xml_error_des_text">View page source to see the offending XML.</xsl:variable>

  <!-- *** advanced search page panel background color *** -->
  <xsl:variable name="adv_search_panel_bgcolor">#cbdced</xsl:variable>

  <!-- *** dynamic result cluster options *** -->
  <xsl:variable name="show_res_clusters">0</xsl:variable>
  <xsl:variable name="res_cluster_position">right</xsl:variable>

  <!-- *** alerts2 options *** -->
  <xsl:variable name="show_alerts2">0</xsl:variable>

  <xsl:variable name="googleconnector_protocol">googleconnector://</xsl:variable>
  <!-- *** db_url_protocol: googledb:// *** -->
  <xsl:variable name="db_url_protocol">googledb://</xsl:variable>

  <!-- *** num_results: actual num_results per page *** -->
  <xsl:variable name="num_results">
    <xsl:choose>
      <xsl:when test="irw:IRW/irw:SearchResults/GSP/PARAM[(@name='num') and (@value!='')]">
        <xsl:value-of select="irw:IRW/irw:SearchResults/GSP/PARAM[@name='num']/@value"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="10"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <!-- *** search_url *** -->
  <xsl:variable name="search_url">
    SearchResults.aspx?iid=<xsl:value-of select="irw:IRW/irw:SearchResults/Company/KeyInstn"/>&amp;q=<xsl:value-of select="irw:IRW/irw:SearchResults/GSP/PARAM[@name = 'q']/@original_value"/>&amp;site=<xsl:value-of select="irw:IRW/irw:SearchResults/GSP/PARAM[@name = 'site']/@value"/>
    <xsl:if test="contains(translate(irw:IRW/irw:SearchResults/Company/URL,$upper,$lower), 'search=1')">&amp;search=1</xsl:if>
    <!--<xsl:for-each select="irw:IRW/irw:SearchResults/GSP/PARAM[(@name != 'start') and
                                   (@name != 'swrnum') and
                     (@name != 'epoch') and
                     not(starts-with(@name, 'metabased_'))]">
      <xsl:value-of select="@name"/>
      <xsl:text>=</xsl:text>
      <xsl:value-of select="@original_value"/>
      <xsl:if test="position() != last()">
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>
      </xsl:if>
    </xsl:for-each>-->
  </xsl:variable>

  <!-- *** synonym_url: does not include q, as_q, and start elements *** -->
  <xsl:variable name="synonym_url">
    SearchResults.aspx?iid=<xsl:value-of select="irw:IRW/irw:SearchResults/Company/KeyInstn"/>&amp;site=<xsl:value-of select="irw:IRW/irw:SearchResults/GSP/PARAM[@name = 'site']/@value"/>
    <!--<xsl:for-each
  select="irw:IRW/irw:SearchResults/GSP/PARAM[(@name != 'q') and
                     (@name != 'as_q') and
                     (@name != 'swrnum') and

		     (@name != 'ie') and
                     (@name != 'start') and
                     (@name != 'epoch') and
                     not(starts-with(@name, 'metabased_'))]">
      <xsl:value-of select="@name"/>
      <xsl:text>=</xsl:text>
      <xsl:value-of select="@original_value"/>
      <xsl:if test="position() != last()">
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>
      </xsl:if>
    </xsl:for-each>-->
  </xsl:variable>

  <!-- *** search_url_escaped: safe for inclusion in javascript *** -->
  <!--<xsl:variable name="search_url_escaped">
    <xsl:call-template name="replace_string">
      <xsl:with-param name="find" select='"&apos;"'/>
      <xsl:with-param name="replace" select='"%27"'/>
      <xsl:with-param name="string" select="$search_url"/>
    </xsl:call-template>
  </xsl:variable>-->

  <!-- *** filter_url: everything except resetting "filter=" *** -->
  <xsl:variable name="filter_url">
    search?<xsl:for-each
  select="irw:IRW/irw:SearchResults/GSP/PARAM[(@name != 'filter') and
                     (@name != 'epoch' or '0' != '') and
                     not(starts-with(@name, 'metabased_'))]">
      <xsl:value-of select="@name"/>
      <xsl:text>=</xsl:text>
      <xsl:value-of select="@original_value"/>
      <xsl:if test="position() != last()">
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>
      </xsl:if>
    </xsl:for-each>
    <xsl:text disable-output-escaping='yes'>&amp;filter=</xsl:text>
  </xsl:variable>

  <xsl:variable name="base_url">
    <xsl:for-each
      select="irw:IRW/irw:SearchResults/GSP/PARAM[@name = 'client' or

                     @name = 'site' or
                     @name = 'num' or
                     @name = 'output' or
                     @name = 'proxystylesheet' or
                     @name = 'access' or
                     @name = 'lr' or
                     @name = 'ie']">
      <xsl:value-of select="@name"/>=<xsl:value-of select="@original_value"/>
      <xsl:if test="position() != last()">&amp;</xsl:if>
    </xsl:for-each>
  </xsl:variable>


  <!-- **********************************************************************
 Utility functions for generating html entities
     ********************************************************************** -->
  <xsl:template name="nbsp">
    <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
  </xsl:template>
  <xsl:template name="nbsp3">
    <xsl:call-template name="nbsp"/>
    <xsl:call-template name="nbsp"/>
    <xsl:call-template name="nbsp"/>
  </xsl:template>
  <xsl:template name="nbsp4">
    <xsl:call-template name="nbsp3"/>
    <xsl:call-template name="nbsp"/>
  </xsl:template>
  <xsl:template name="quot">
    <xsl:text disable-output-escaping="yes">&amp;quot;</xsl:text>
  </xsl:template>
  <xsl:template name="copy">
    <xsl:text disable-output-escaping="yes">&amp;copy;</xsl:text>
  </xsl:template>

  <!-- *** Find the substring after the last occurence of a separator *** -->
  <xsl:template name="last_substring_after">

    <xsl:param name="string"/>
    <xsl:param name="separator"/>
    <xsl:param name="fallback"/>

    <xsl:variable name="newString"
      select="substring-after($string, $separator)"/>

    <xsl:choose>
      <xsl:when test="$newString!=''">
        <xsl:call-template name="last_substring_after">
          <xsl:with-param name="string" select="$newString"/>
          <xsl:with-param name="separator" select="$separator"/>
          <xsl:with-param name="fallback" select="$newString"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$fallback"/>
      </xsl:otherwise>
    </xsl:choose>

  </xsl:template>

  <!-- **********************************************************************
 Sort-by criteria: sort by date/relevance
     ********************************************************************** -->
  <xsl:template name="sort_by">
    <xsl:variable name="sort_by_relevance_url">
      <xsl:value-of select="$search_url"/>&amp;sort=date%3AD%3AL%3Ad1
    </xsl:variable>

    <xsl:variable name="sort_by_date_url">
      <xsl:value-of select="$search_url"/>&amp;sort=date%3AD%3AS%3Ad1
    </xsl:variable>

    <table>
      <tr valign='top'>
        <td class="data">
          <span class="s">
            <xsl:choose>
              <xsl:when test="GSP/PARAM[@name = 'sort' and starts-with(@value,'date:D:S')]">
                <font color="{$global_text_color}">
                  <xsl:text>Sort by date / </xsl:text>
                </font>
                <a ctype="sort" href="{$sort_by_relevance_url}" title="Sort by relevance">Sort by relevance</a>
              </xsl:when>
              <xsl:when test="GSP/PARAM[@name = 'sort' and starts-with(@value,'date:A:S')]">
                <font color="{$global_text_color}">
                  <xsl:text>Sort by date / </xsl:text>
                </font>
                <a ctype="sort" href="{$sort_by_relevance_url}" title="Sort by relevance">Sort by relevance</a>
              </xsl:when>
              <xsl:otherwise>
                <a ctype="sort" href="{$sort_by_date_url}"  title="Sort by date">Sort by date</a>
                <font color="{$global_text_color}">
                  <xsl:text> / Sort by relevance</xsl:text>
                </font>
              </xsl:otherwise>
            </xsl:choose>
          </span>
        </td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template name="results">
    <xsl:param name="query"/>
    <xsl:param name="time"/>

    <xsl:if test="GSP/RES">
      <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
          <td width="100%" align="right" class="data">
            Results
            <strong>
              <xsl:value-of select="GSP/RES/@SN" /> - <xsl:value-of select="GSP/RES/@EN" />
            </strong>
            of about
            <strong>
              <xsl:value-of select="GSP/RES/M" />
            </strong>
            for
            <strong>
              <xsl:value-of select="$query" />
            </strong>.
            Search took
            <strong>
              <xsl:value-of select="format-number($time,'0.00')"/>
            </strong> seconds.
          </td>
        </tr>
        <xsl:if test="not(contains(translate(Company/URL,$upper,$lower), 'print=1'))">
        <xsl:if test="GSP/PARAM[@name='site']/@value = 'Filings' or GSP/PARAM[@name='site']/@value = 'PRs'">
        <tr>
          <td width="100%" align="right" class="data">
            <xsl:call-template name="sort_by"/>
          </td>
        </tr>
        </xsl:if>
        </xsl:if>
      </table>    
    </xsl:if>

    <xsl:call-template name="spelling"/>

    <xsl:call-template name="synonyms"/>
  

      <!-- for real results -->
      <xsl:apply-templates select="GSP/RES/R">
        <xsl:with-param name="query" select="$query"/>
      </xsl:apply-templates>

      <!-- *** Filter note (if needed) *** -->
      <xsl:if test="(GSP/RES/FI) and (not(GSP/RES/NB/NU))">
        <p>
          <i>
            In order to show you the most relevant results, we have omitted some entries very similar to the <xsl:value-of select="GSP/RES/@EN"/> already displayed.<br/>If you like, you can <a href="{$filter_url}0" title="repeat the search with the omitted results included">repeat the search with the omitted results included</a>.
          </i>
        </p>
      </xsl:if>

    <!-- *** Add bottom navigation *** -->
    <xsl:variable name="nav_style">link</xsl:variable>
    <xsl:if test="not(contains(translate(Company/URL,$upper,$lower), 'print=1'))">
      <xsl:call-template name="google_navigation">
        <xsl:with-param name="prev" select="GSP/RES/NB/PU"/>
        <xsl:with-param name="next" select="GSP/RES/NB/NU"/>
        <xsl:with-param name="view_begin" select="GSP/RES/@SN"/>
        <xsl:with-param name="view_end" select="GSP/RES/@EN"/>
        <xsl:with-param name="guess" select="GSP/RES/M"/>
        <xsl:with-param name="navigation_style" select="$nav_style"/>
      </xsl:call-template>
    </xsl:if>

  </xsl:template>

  <xsl:template name="no_RES">
    <xsl:param name="query"/>

    <span class="p">
      <br/>
      Your search - <strong>
        <xsl:value-of select="$query"/>
      </strong> - did not match any documents.
      <br/>
      No pages were found containing <strong>
        "<xsl:value-of select="$query"/>"
      </strong>.
      <br/>
      <br/>
      Suggestions:
      <ul>
        <li>Make sure all words are spelled correctly.</li>
        <li>Try different keywords.</li>
        <xsl:if test="GSP/PARAM[(@name='access') and(@value='a')]">
          <li>Make sure your security credentials are correct.</li>
        </xsl:if>
        <li>Try more general keywords.</li>
      </ul>
    </span>

  </xsl:template>

  <xsl:template name="search_unavailable">
    <span class="p">
      Site search is not available.
    </span>
  </xsl:template>

  <xsl:template name="spelling">
    <xsl:if test="GSP/Spelling/Suggestion">
      <p>
        <span class="p">
            <xsl:value-of select="$spelling_text"/>
            <xsl:call-template name="nbsp"/>
        </span>
        <xsl:choose>
          <xsl:when test="contains(Company/URL, 'search=1')">
            <a ctype="spell" href="SearchResults.aspx?iid={Company/KeyInstn}&amp;q={GSP/Spelling/Suggestion[1]/@q}&amp;spell=1&amp;search=1&amp;site={GSP/PARAM[@name='site']/@value}" title="{GSP/Spelling/Suggestion[1]}">
              <xsl:value-of disable-output-escaping="yes" select="GSP/Spelling/Suggestion[1]"/>
            </a>
          </xsl:when>
          <xsl:otherwise>
            <a ctype="spell" href="SearchResults.aspx?iid={Company/KeyInstn}&amp;q={GSP/Spelling/Suggestion[1]/@q}&amp;spell=1&amp;site={GSP/PARAM[@name='site']/@value}" title="{GSP/Spelling/Suggestion[1]}">
              <xsl:value-of disable-output-escaping="yes" select="GSP/Spelling/Suggestion[1]"/>
            </a>
          </xsl:otherwise>
        </xsl:choose>
      </p>
    </xsl:if>
  </xsl:template>

  <xsl:template name="synonyms">
    <xsl:if test="GSP/Synonyms/OneSynonym">
      <p>
        <span class="p">
          <font color="{$synonyms_text_color}">
            <xsl:value-of select="$synonyms_text"/>
            <xsl:call-template name="nbsp"/>
          </font>
        </span>
        <xsl:for-each select="GSP/Synonyms/OneSynonym">
          <a ctype="synonym" href="{$synonym_url}&amp;q={@q}">
            <xsl:value-of disable-output-escaping="yes" select="."/>
          </a>
          <xsl:text> </xsl:text>
        </xsl:for-each>
      </p>
    </xsl:if>
  </xsl:template>

  <xsl:template match="R">
    <xsl:param name="query"/>

    <xsl:variable name="protocol"     select="substring-before(U, '://')"/>
    <xsl:variable name="temp_url"     select="substring-after(U, '://')"/>
    <xsl:variable name="display_url1" select="substring-after(UD, '://')"/>
    <xsl:variable name="escaped_url"  select="substring-after(UE, '://')"/>    

    <xsl:variable name="display_url2">
      <xsl:choose>
        <xsl:when test="$display_url1">
          <xsl:value-of select="$display_url1"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$temp_url"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="display_url">
          <xsl:value-of select="$display_url2"/>
    </xsl:variable>

    <xsl:variable name="stripped_url">
          <xsl:value-of select="$display_url"/>
    </xsl:variable>

    <xsl:variable name="crowded_url" select="HN/@U"/>
    <xsl:variable name="crowded_display_url1" select="HN"/>
    <xsl:variable name="crowded_display_url">

          <xsl:value-of select="$crowded_display_url1"/>
    </xsl:variable>

    <xsl:variable name="url_indexed" select="not(starts-with($temp_url, 'noindex!/'))"/>

    <table cellpadding="0" cellspacing="5" border="0" width="100%" style="margin-top: 5px;">
      <tr>
        <td class="data">
          <a>
            <xsl:attribute name="href">
              <xsl:value-of select="MT[@N='URL']/@V" disable-output-escaping="yes"/>
            </xsl:attribute>
            <xsl:value-of select="MT[@N='PageTitle']/@V" disable-output-escaping="yes"/>
          </a> 
        </td>
      </tr>
      <tr>
        <td class="data">
          <xsl:value-of select="S" disable-output-escaping="yes"/>          
        </td>
      </tr>
      <tr>
        <td class="data">
          <xsl:value-of select="MT[@N='URL']/@V" disable-output-escaping="yes"/>
          <xsl:if test="HAS/C/@SZ != ''">
            -&nbsp;<xsl:value-of select="HAS/C/@SZ" disable-output-escaping="yes"/>
          </xsl:if>
          <xsl:if test="MT[@N='DocumentDate']/@V != ''">
            <xsl:variable name="year" select="substring-before(MT[@N='DocumentDate']/@V, '-')"></xsl:variable>
            <xsl:variable name="monthday" select="substring-before(substring-after(MT[@N='DocumentDate']/@V, '-'), ' ')"></xsl:variable>
            <xsl:variable name="month" select="substring-before($monthday, '-')"></xsl:variable>
            <xsl:variable name="day" select="substring-after($monthday, '-')"></xsl:variable>
            -&nbsp;<xsl:value-of select="format-number($month, '#')" />/<xsl:value-of select="format-number($day, '#')" />/<xsl:value-of select="$year" />
          </xsl:if>
        </td>
      </tr>
    </table>
    
  </xsl:template>

  <xsl:template name="google_navigation">
    <xsl:param name="prev"/>
    <xsl:param name="next"/>
    <xsl:param name="view_begin"/>
    <xsl:param name="view_end"/>
    <xsl:param name="guess"/>
    <xsl:param name="navigation_style"/>

    <xsl:variable name="fontclass">
      <xsl:choose>
        <xsl:when test="$navigation_style = 'top'">s</xsl:when>
        <xsl:otherwise>b</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- *** Test to see if we should even show navigation *** -->
    <xsl:if test="($prev) or ($next)">

      <!-- *** Start Google result navigation bar *** -->

      <xsl:if test="$navigation_style != 'top'">
        <xsl:text disable-output-escaping="yes">&lt;center&gt;
        &lt;div class=&quot;n&quot;&gt;</xsl:text>
      </xsl:if>

      <table border="0" cellpadding="0" width="1%" cellspacing="0" style="margin-top: 10px;">
        <tr align="center" valign="top" class="data">
          <xsl:if test="$navigation_style != 'top'">
            <td valign="bottom" nowrap="1" class="data">
                Result Page<xsl:call-template name="nbsp"/>
            </td>
          </xsl:if>


          <!-- *** Show previous navigation, if available *** -->
          <xsl:choose>
            <xsl:when test="$prev">
              <td nowrap="1">
                <span class="{$fontclass}">
                  <a ctype="nav.prev" href="{$search_url}&amp;start={$view_begin - $num_results - 1}" title="Previous">
                    <xsl:if test="$navigation_style = 'google'">

                      <img src="/nav_previous.gif" width="68" height="26"
                        alt="Previous" border="0"/>
                      <br/>
                    </xsl:if>
                    <xsl:if test="$navigation_style = 'top'">
                      <xsl:text>&lt;</xsl:text>
                    </xsl:if>
                    <xsl:text>Previous</xsl:text>
                  </a>
                </span>
                <xsl:if test="$navigation_style != 'google'">
                  <xsl:call-template name="nbsp"/>
                </xsl:if>
              </td>
            </xsl:when>
            <xsl:otherwise>
              <td nowrap="1">
                <xsl:if test="$navigation_style = 'google'">
                  <img src="/nav_first.gif" width="18" height="26"
                    alt="First" border="0"/>
                  <br/>
                </xsl:if>
              </td>
            </xsl:otherwise>
          </xsl:choose>

          <xsl:if test="($navigation_style = 'google') or
                      ($navigation_style = 'link')">
            <!-- *** Google result set navigation *** -->
            <xsl:variable name="mod_end">
              <xsl:choose>
                <xsl:when test="$next">
                  <xsl:value-of select="$guess"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="$view_end"/>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:variable>

            <xsl:call-template name="result_nav">
              <xsl:with-param name="start" select="0"/>
              <xsl:with-param name="end" select="$mod_end"/>
              <xsl:with-param name="current_view" select="($view_begin)-1"/>
              <xsl:with-param name="navigation_style" select="$navigation_style"/>
            </xsl:call-template>
          </xsl:if>

          <!-- *** Show next navigation, if available *** -->
          <xsl:choose>
            <xsl:when test="$next">
              <td nowrap="1">
                <xsl:if test="$navigation_style != 'google'">
                  <xsl:call-template name="nbsp"/>
                </xsl:if>
                <span class="{$fontclass}">
                  <a ctype="nav.next" href="{$search_url}&amp;start={$view_begin + $num_results - 1}" title="Next">
                    <xsl:if test="$navigation_style = 'google'">

                      <img src="/nav_next.gif" width="100" height="26"

                        alt="Next" border="0"/>
                      <br/>
                    </xsl:if>
                    <xsl:text>Next</xsl:text>
                    <xsl:if test="$navigation_style = 'top'">
                      <xsl:text>&gt;</xsl:text>
                    </xsl:if>
                  </a>
                </span>
              </td>
            </xsl:when>
            <xsl:otherwise>
              <td nowrap="1">
                <xsl:if test="$navigation_style != 'google'">
                  <xsl:call-template name="nbsp"/>
                </xsl:if>
                <xsl:if test="$navigation_style = 'google'">
                  <img src="/nav_last.gif" width="46" height="26"

                    alt="Last" border="0"/>
                  <br/>
                </xsl:if>
              </td>
            </xsl:otherwise>
          </xsl:choose>

          <!-- *** End Google result bar *** -->
        </tr>
      </table>

      <xsl:if test="$navigation_style != 'top'">
        <xsl:text disable-output-escaping="yes">&lt;/div&gt;
        &lt;/center&gt;</xsl:text>
      </xsl:if>
    </xsl:if>
  </xsl:template>

  <xsl:template name="result_nav">
    <xsl:param name="start"/>
    <xsl:param name="end"/>
    <xsl:param name="current_view"/>
    <xsl:param name="navigation_style"/>

    <!-- *** Choose how to show this result set *** -->
    <xsl:choose>
      <xsl:when test="($start)&lt;(($current_view)-(10*($num_results)))">
      </xsl:when>
      <xsl:when test="(($current_view)&gt;=($start)) and
                    (($current_view)&lt;(($start)+($num_results)))">
        <td class="data">
          <xsl:if test="$navigation_style = 'google'">
            <img src="/nav_current.gif" width="16" height="26" alt="Current"/>
            <br/>
          </xsl:if>
          <xsl:if test="$navigation_style = 'link'">
            <xsl:call-template name="nbsp"/>
          </xsl:if>
          <span class="i">
            <xsl:value-of
          select="(($start)div($num_results))+1"/>
          </span>
          <xsl:if test="$navigation_style = 'link'">
            <xsl:call-template name="nbsp"/>
          </xsl:if>
        </td>
      </xsl:when>
      <xsl:otherwise>
        <td class="data">
          <xsl:if test="$navigation_style = 'link'">
            <xsl:call-template name="nbsp"/>
          </xsl:if>
          <a ctype="nav.page" href="{$search_url}&amp;start={$start}" title="{(($start)div($num_results))+1}">
            <xsl:if test="$navigation_style = 'google'">
              <img src="/nav_page.gif" width="16" height="26" alt="Navigation"
                   border="0"/>
              <br/>
            </xsl:if>
            <xsl:value-of select="(($start)div($num_results))+1"/>
          </a>
          <xsl:if test="$navigation_style = 'link'">
            <xsl:call-template name="nbsp"/>
          </xsl:if>
        </td>
      </xsl:otherwise>
    </xsl:choose>

    <!-- *** Recursively iterate through result sets to display *** -->
    <xsl:if test="((($start)+($num_results))&lt;($end)) and
                ((($start)+($num_results))&lt;(($current_view)+
                (10*($num_results))))">
      <xsl:call-template name="result_nav">
        <xsl:with-param name="start" select="$start+$num_results"/>
        <xsl:with-param name="end" select="$end"/>
        <xsl:with-param name="current_view" select="$current_view"/>
        <xsl:with-param name="navigation_style" select="$navigation_style"/>
      </xsl:call-template>
    </xsl:if>

  </xsl:template>

  <xsl:template match="irw:SearchResults">

    <table border="0" cellspacing="0" cellpadding="3" width="100%" class="colordark">
      <xsl:call-template name="Title_S1"/>
      <tr>
        <td class="colordark" colspan="2">
          <a name="Top" id="Top" title="top"></a>
          <table border="0" cellspacing="0" cellpadding="4" width="100%">
            <tr>
              <td class="colorlight" width="100%">
                <xsl:choose>
                  <xsl:when test="GSP/RES or GSP/GM or GSP/Spelling or GSP/Synonyms or GSP/CT or GSP/ENTOBRESULTS">
                    <xsl:call-template name="results">
                      <xsl:with-param name="query" select="GSP/Q"/>
                      <xsl:with-param name="time" select="GSP/TM"/>
                    </xsl:call-template>
                  </xsl:when>
                  <xsl:when test="GSP/Q=''">
                  </xsl:when>
                  <xsl:when test="GSP">
                    <xsl:call-template name="no_RES">
                      <xsl:with-param name="query" select="GSP/Q"/>
                    </xsl:call-template>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:call-template name="search_unavailable" />
                  </xsl:otherwise>
                </xsl:choose>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>

    <xsl:call-template name="Copyright"/>

    

      
  </xsl:template>
  
</xsl:stylesheet>
