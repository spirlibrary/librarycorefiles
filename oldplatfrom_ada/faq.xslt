<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.ord/TR/REC-html40"
	xmlns:irw="http://www.snl.com/xml/irw">
	
<!-- <xsl:output method="xhtml" media-type="text/html" indent="no"/>
<xsl:output method="html"  doctype-system="http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd" doctype-public="-//W3C//Dtd XHTML 1.0 Transitional//EN"/> -->

<xsl:output method="html" doctype-public="-//W3C//Dtd XHTML 1.0 Transitional//EN" indent="no"/>

  <!-- SP 72522 RC: 05/22/2009
  CSS problem on Committee chart page. Please fix that.
  -->
<!-- Start Default Template here-->
<!-- This is the default template. Most changes are to either remove the company name, ticker, or exchange. you can also change the name of the page or remove this row all together. These templates below are broken up how the page is displayed. -->

<xsl:template name="Faq_Title">
<xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
	<tr>
		<td class="colordark" nowrap="nowrap">
			<span class="title1light"><xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/></span>
	    </td>
		<td class="colordark" nowrap="nowrap" align="right" valign="top"><span class="subtitlelight"><xsl:value-of select="$CompanyNameCaps"/> (<xsl:value-of select="Company/Exchange"/> - <xsl:value-of select="Company/Ticker"/>)</span><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
	</tr>
		<tr class="default" align="left">
			<td class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Header" disable-output-escaping="yes"/></span></td>
	</tr>
</xsl:template>

<xsl:template name="Faq_Data">
  <xsl:variable name="keyinstn" select="Company/KeyInstn" />
  <a name="top" id="top"></a>
  <table border="0" cellspacing="0" cellpadding="4" width="100%">
    <tr>
      <td class="colorlight">
        
        <table border="0" cellspacing="0" cellpadding="4" width="100%">
          <xsl:if test="FAQ/FAQCount != 0">
            <xsl:for-each select="FAQ">
              <tr>
                <td valign="top" align="left" class="data">
                    <a><xsl:attribute name="href">faq.aspx?iid=<xsl:value-of select="$keyinstn"/>#Q<xsl:value-of select="position()" disable-output-escaping="yes"/></xsl:attribute><span class="defaultbold"><xsl:value-of select="FAQQuestion" disable-output-escaping="yes"/></span></a><br/>
                </td>
              </tr>
            </xsl:for-each>
          </xsl:if>
          <xsl:if test="FAQ/FAQCount = 0">
            <tr><td valign="top" align="left" class="data">No questions listed at this time.</td></tr>
          </xsl:if>
          <tr>
            <td valign="top" align="left" class="data"><br /><a href="#top" title="top">Top</a><br /><br /><br /></td>
					</tr>
				</table>

				<table border="0" cellspacing="0" cellpadding="4" width="100%">
          <xsl:if test="FAQ/FAQCount != 0">
            <xsl:for-each select="FAQ">
              <tr>
								<td valign="top" align="left">
									<a><xsl:attribute name="name">#Q<xsl:value-of select="position()" disable-output-escaping="yes"/></xsl:attribute><xsl:attribute name="ID">Q<xsl:value-of select="position()" /></xsl:attribute></a>
                  <span class="defaultbold">Q: <xsl:value-of select="FAQQuestion" disable-output-escaping="yes"/> </span>
								</td>
							</tr>
							<tr>
								<td valign="top" align="left" class="data"><span class="defaultbold">A: </span><xsl:value-of select="FAQAnswer" disable-output-escaping="yes"/><br/></td>
							</tr>
							<tr>
								<td valign="top" align="left" class="data"><a href="#top" title="Top">Top</a><br /><hr size="1" /></td>
							</tr>
            </xsl:for-each>
          </xsl:if>
          <xsl:if test="FAQ/FAQCount = 0">
						<tr>
							<td valign="top" align="left" class="data">No questions listed at this time.<br/></td>
						</tr>
					</xsl:if>
        </table>
        
      </td>
    </tr>
  </table>
</xsl:template>

<xsl:template name="Faq_Footer">  		
  <xsl:if test="Company/Footer != ''">
  <tr class="default" align="left">
	  <td class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span></td>
  </tr>
  </xsl:if>
</xsl:template>
  
<!-- Default Template Ends here. all templates below are ONLY if you are using the 'Style Template' section in the console. -->



<!-- This is Template ONE option in the console under 'Style Template'
//This template is a striped down version of the main template. Everything (data) is displayed in single rows.
//The sections will be commented on where the rows are. you should be able to just move rows (entire chunks of data) where you need too. Edits to this section are rare, mostly due to size (width of the page).
-->

<xsl:template name="Faq_Title2">
<xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
<tr>
<td class="title2colordark titletest" nowrap="" colspan="2" align="left"><xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/><br /><IMG SRC="" width="2" HEIGHT="1" alt="{$CompanyNameCaps}" /><span class="title2colordark titletest2"><xsl:value-of select="$CompanyNameCaps"/> (<xsl:value-of select="Company/Exchange"/> - <xsl:value-of select="Company/Ticker"/>)</span><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
</tr>
<tr class="default" align="left">
<td class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Header" disable-output-escaping="yes"/></span></td>
</tr>

</xsl:template>

<xsl:template name="Faq_Data2">
  <xsl:variable name="CompanyURL" select="Company/URL" />
  <xsl:if test="FAQ/FAQCount != 0">
    <div class="Faqcontent">
      <xsl:if test="not(contains(translate($CompanyURL,$ucletters,$lcletters), 'print=1'))">
        <div class="data" align="right" id="toggleShowHideAll">
            <a class="ResultsExpandAll" href="javascript:;" title="Expand All">Expand All</a> | <a class="ResultsCollapseAll" href="javascript:;" title="Collapse All">Collapse All</a>
        </div>
      </xsl:if>
        <xsl:for-each select="FAQ">
            <div class="technology">
            <a class="toggle_line" href="javascript:;" title="Toggle Line">
              <xsl:if test="contains(translate($CompanyURL,$ucletters,$lcletters), 'print=1')"><xsl:attribute name="style">text-decoration: none</xsl:attribute></xsl:if>
              <img src="images/arrow-rt.gif" border="0" style="padding-top:3px; padding-right:5px" class="toggleImage" alt="Right Arrow">
                <xsl:if test="contains(translate($CompanyURL,$ucletters,$lcletters), 'print=1')"><xsl:attribute name="src">images/arrow-down.gif</xsl:attribute></xsl:if></img>
              <div class="faqQ"><xsl:value-of select="FAQQuestion" disable-output-escaping="yes"/></div>
                </a>
            </div>
            <div class="thelanguage" style="display:none;">
              <xsl:if test="contains(translate($CompanyURL,$ucletters,$lcletters), 'print=1')"><xsl:attribute name="style">display:block;</xsl:attribute></xsl:if>
                <div class="faqA data"><xsl:value-of select="FAQAnswer" disable-output-escaping="yes"/><br/><hr width="100%" style="color:#999999" align="center" size="1"/></div>
            </div>
        </xsl:for-each>
    </div>
</xsl:if>
<xsl:if test="FAQ/FAQCount = 0">
	<span class="default">No questions listed at this time.</span><br/>
</xsl:if>
</xsl:template>

<xsl:template name="Faq_Footer2">  		
  <xsl:if test="Company/Footer != ''">
    <tr class="default" align="left">
      <td class="data" colspan="2"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span></td>
    </tr>
  </xsl:if>
</xsl:template>

  <!-- Template ONE Ends here. --> 


  <!-- Template THREE Starts -->
<xsl:template name="Faq_Data3">
  <xsl:variable name="CompanyURL" select="Company/URL" />
  <xsl:if test="FAQ/FAQCount != 0">
    <div class="Faqcontent">
      <xsl:if test="not(contains(translate($CompanyURL,$ucletters,$lcletters), 'print=1'))">
        <div class="data" align="right" id="toggleShowHideAll">
            <a class="ResultsExpandAll" href="javascript:;" title="Expand All">Expand All</a> | <a class="ResultsCollapseAll" href="javascript:;" title="Collapse All">Collapse All</a>
        </div>
      </xsl:if>
        <xsl:for-each select="FAQ">
            <div class="technology">
            <a class="toggle_line" href="javascript:;" title="Toggle Line">
              <xsl:if test="contains(translate($CompanyURL,$ucletters,$lcletters), 'print=1')"><xsl:attribute name="style">text-decoration: none</xsl:attribute></xsl:if>
              <img src="images/arrow-rt.gif" border="0" style="padding-top:3px; padding-right:5px" class="toggleImage" alt="Right Arrow">
                <xsl:if test="contains(translate($CompanyURL,$ucletters,$lcletters), 'print=1')"><xsl:attribute name="src">images/arrow-down.gif</xsl:attribute></xsl:if></img>
              <div class="faqQ"><xsl:value-of select="FAQQuestion" disable-output-escaping="yes"/></div>
                </a>
            </div>
            <div class="thelanguage" style="display:none;">
              <xsl:if test="contains(translate($CompanyURL,$ucletters,$lcletters), 'print=1')"><xsl:attribute name="style">display:block;</xsl:attribute></xsl:if>
                <div class="faqA data"><xsl:value-of select="FAQAnswer" disable-output-escaping="yes"/><br/><hr width="100%" style="color:#999999" align="center" size="1"/></div>
            </div>
        </xsl:for-each>
    </div>
</xsl:if>
<xsl:if test="FAQ/FAQCount = 0">
	<span class="default">No questions listed at this time.</span><br/>
</xsl:if>
</xsl:template>
  
<xsl:template name="Faq_Footer3">  		
  <xsl:if test="Company/Footer != ''">
    <tr class="default" align="left">
      <td class="data"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span></td>
    </tr>
  </xsl:if>
</xsl:template>

  <!-- Template THREE Ends -->

<xsl:template name="Faq_Resources">   
  <xsl:if test="not(contains(translate(Company/URL,$ucletters,$lcletters), 'print=1'))">
    <script src="javascript/jquery-1.4.1.min.js"></script>
    <script src="javascript/menuToggler.js"></script>  
  </xsl:if>
</xsl:template>


<!-- Main XSLT templates are here. these are the templates which are actually being displayed in the page. this is for TOP LEVEL editing. if you dont need to use the individual templates above you can do your main editing here when pulling THESE templates below you must carry the top comment with them and uncomment them when you drop it into the company specific xslt. -->

<xsl:template match="irw:Faqs">
<xsl:variable name="TemplateName" select="Company/TemplateName"/>
  <xsl:choose>
    <!-- Template 1 -->
    <xsl:when test="$TemplateName = 'AltFaq1'">
      <xsl:call-template name="Faq_Resources" />    
      <xsl:call-template name="TemplateONEstylesheet" />    
      <table border="0" cellspacing="0" cellpadding="3" width="100%" class="data">
        <xsl:call-template name="Title_S2"/>
        <tr><td class="leftTOPbord" colspan="2" valign="top" align="left"><xsl:call-template name="Faq_Data2"/></td></tr>
        <xsl:call-template name="Faq_Footer2"/>
      </table>
      <xsl:call-template name="Copyright"/>
    </xsl:when>
    <!-- End Template 1 -->
    
    <!-- Template 3 -->
    <xsl:when test="$TemplateName = 'AltFaq3'">
      <xsl:call-template name="Faq_Resources" />    
      <xsl:call-template name="TemplateTHREEstylesheet" />
      <xsl:call-template name="Title_T3"/>
      <table border="0" cellspacing="0" cellpadding="3" width="100%" class="table2">
        <tr><td class="data" valign="top" align="left"><xsl:call-template name="Faq_Data3"/></td></tr>        
        <xsl:call-template name="Faq_Footer3"/>
      </table>
      <xsl:call-template name="Copyright"/>
    </xsl:when>
    <!-- End Template 3 -->
    
    <!-- Template Default -->
    <xsl:otherwise>
      <table border="0" cellspacing="0" cellpadding="3" width="100%" class="colordark">
        <xsl:call-template name="Title_S1"/>
        <tr><td class="colordark" colspan="2"><xsl:call-template name="Faq_Data"/></td></tr>
        <xsl:call-template name="Faq_Footer"/>
      </table>
      <xsl:call-template name="Copyright"/>
</xsl:otherwise>


</xsl:choose>


</xsl:template>


</xsl:stylesheet>