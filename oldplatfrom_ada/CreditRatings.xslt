<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns="http://www.w3.ord/TR/REC-html40"
  xmlns:irw="http://www.snl.com/xml/irw">

<xsl:output method="html" media-type="text/html" indent="no"/>

<!--
Last Change 2: Dt. 29/06/2009 JR [SP: 75024]
=============================================
Changes are Below : Fixed now
Broken background in Mozilla above "View Ratings Scale" Link (Template 1/3).
A blank row was printing with datashade without any data which was now fixed.
-->


<!--
Last Change : Dt. 07/01/2008 JR
======================================
In footer, please left-align "Ratings Watch Action Legend" and definitions, Currently this is right-aligned.
-->

<!-- Start Default Template here-->

<!--
//This is the default template. Most changes are to either remove the company name, ticker, or exchange. you can also change the name of the page or remove this row all together.
//These templates below are broken up how the page is displayed.

-->

<xsl:template name="CreditRatings_Data">
<table border="0" cellspacing="0" cellpadding="3" width="100%">

<xsl:if test="Company/DisplayingCompanysName = ''">
	<tr class="colorlight">
		<td class="colorlight" align="left" valign="top" colspan="2" nowrap="nowrap">
			<span class="title2">
				<xsl:value-of select="Company/InstnName" disable-output-escaping="yes"/>
			</span>
		</td>
	</tr>			
</xsl:if>
<xsl:if test="Company/DisplayingCompanysName != ''">
	<tr class="colorlight">
		<td class="colorlight" align="left" valign="top" colspan="2" nowrap="nowrap">
			<span class="title2">			
				<xsl:value-of select="Company/DisplayingCompanysName" disable-output-escaping="yes"/>
			</span>
		</td>
	</tr>
</xsl:if>

<xsl:variable name="colCount" select="count(Agencies)+1"/>
<tr>
	<td class="header" align="left" valign="bottom">
		<xsl:choose>
		<xsl:when test="$colCount>0"><xsl:attribute name="colspan"><xsl:value-of select="$colCount"/></xsl:attribute></xsl:when>
		<xsl:otherwise><xsl:attribute name="colspan"><xsl:value-of select="2"/></xsl:attribute>
		</xsl:otherwise>
		</xsl:choose>
		<table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<td class="header" align="left" valign="bottom">Credit Ratings</td>
			<td align="right" class="header" nowrap="">
				<a class="header" title="View Ratings Scale">
				<xsl:variable name="keyinstn" select="Company/KeyInstn"/>
				<xsl:variable name="url1" select="concat('creditratingsscale.aspx?iid=', $keyinstn)"/>
				<xsl:attribute name="href"><xsl:value-of select="$url1"/></xsl:attribute>View Ratings Scale</a>
			</td>
		</tr>
		</table>
	</td>
</tr>

<xsl:if test="count(CreditRatings) = 0">
<tr>
	<td class="data">
		<xsl:choose>
		<xsl:when test="$colCount>0"><xsl:attribute name="colspan"><xsl:value-of select="$colCount"/></xsl:attribute></xsl:when>
		<xsl:otherwise><xsl:attribute name="colspan"><xsl:value-of select="5"/></xsl:attribute></xsl:otherwise>
		</xsl:choose>
		Credit ratings are currently unavailable.
	</td>
</tr>
<tr>
	<td class="data" align="left" valign="top" colspan="6" nowrap="nowrap">
		<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
	</td>
</tr>
</xsl:if>

<xsl:if test="count(CreditRatings) > 0">
<tr>
	<td class="header" align="left" valign="bottom">&#160;</td>
	<xsl:for-each select="CreditRatings[not(RatingAgency = preceding-sibling::CreditRatings/RatingAgency) and not(contains(RatingAgency, 'DBRS'))]">

		<td class="header" align="right" valign="bottom"><xsl:value-of select="RatingAgency" disable-output-escaping="yes"/></td>

	</xsl:for-each>
</tr>
</xsl:if>

<xsl:for-each select="CreditRatings[not(CreditRatingTranche = preceding-sibling::CreditRatings/CreditRatingTranche)]">
<xsl:sort select="FormOrder" order="ascending" data-type="number"/>
<xsl:sort select="CreditRatingTranche" order="ascending"/>
<xsl:variable name="CreditRatingTrancheKey" select="KeyCreditRatingTranche"/>
<xsl:variable name="position" select="position()"/>
<xsl:if test="FormOrder > '0'">
<tr>
	<td align="left" valign="top" class="data">
		<xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">datashade</xsl:attribute></xsl:if>
		<xsl:variable name="url2" select="concat('creditratingsdetail.aspx?IID=', KeyInstn, '&amp;Key=', KeyInstn, '&amp;Sub=0')"/>
		<a><xsl:attribute name="href"><xsl:value-of select="$url2" /></xsl:attribute>
			<xsl:value-of select="CreditRatingTranche" disable-output-escaping="yes"/>
		</a>
	</td>
	<xsl:for-each select="../CreditRatings[not(contains(RatingAgency, 'DBRS'))]">
	<xsl:if test="$CreditRatingTrancheKey = KeyCreditRatingTranche">

		<td align="right" valign="top" class="data">
		<xsl:if test="$position mod 2 = 0"><xsl:attribute name="class">datashade</xsl:attribute></xsl:if>
			<xsl:value-of select="CreditRating"/><br/><xsl:value-of select="RatingsAsOf"/>
		</td>

	</xsl:if>
	</xsl:for-each>
</tr>
</xsl:if>
</xsl:for-each>

<tr>
	<td class="header" align="left" valign="bottom">
		<xsl:choose>
		<xsl:when test="$colCount>0"><xsl:attribute name="colspan"><xsl:value-of select="$colCount"/></xsl:attribute></xsl:when>
		<xsl:otherwise><xsl:attribute name="colspan"><xsl:value-of select="5"/></xsl:attribute></xsl:otherwise>
		</xsl:choose>
		Credit Ratings (Subsidiaries)
	</td>
</tr>

<xsl:if test="count(Subsidiaries[KeyCreditRatingTranche != 1]) = 0">
<tr>
	<td class="data">
		<xsl:choose>
		<xsl:when test="$colCount>0"><xsl:attribute name="colspan"><xsl:value-of select="$colCount"/></xsl:attribute></xsl:when>
		<xsl:otherwise><xsl:attribute name="colspan"><xsl:value-of select="5"/></xsl:attribute></xsl:otherwise>
		</xsl:choose>
		Credit ratings for subsidiaries are currently unavailable.
	</td>
</tr>
<tr>
	<td class="data" align="left" valign="top" nowrap="nowrap">
		<xsl:choose>
		<xsl:when test="$colCount>0"><xsl:attribute name="colspan"><xsl:value-of select="$colCount"/></xsl:attribute></xsl:when>
		<xsl:otherwise><xsl:attribute name="colspan"><xsl:value-of select="5"/></xsl:attribute></xsl:otherwise>
		</xsl:choose>
		<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
	</td>
</tr>
</xsl:if>

<xsl:if test="count(Subsidiaries[KeyCreditRatingTranche != 1]) > 0">
<tr>
	<td class="header" align="left" valign="bottom">&#160;</td>
	<xsl:for-each select="Subsidiaries[not(RatingAgency = preceding-sibling::Subsidiaries/RatingAgency) and not(contains(RatingAgency, 'DBRS'))]">

		<td class="header" align="right" valign="bottom">
			<xsl:value-of select="RatingAgency" disable-output-escaping="yes"/>
		</td>

	</xsl:for-each>
	</tr>

	<xsl:for-each select="Subsidiaries[not(KeyInstn = preceding-sibling::Subsidiaries/KeyInstn) and KeyCreditRatingTranche != 1]">
	<xsl:sort select="StandardizedName" order="ascending"/>
	<xsl:variable name="KeyInstnSubsidiary" select="KeyInstn"/>
	<tr>
		<td class="data" align="left" valign="bottom" colspan="6">
			<xsl:variable name="url3" select="concat('creditratingsdetail.aspx?IID=', ../Company/KeyInstn, '&amp;Key=', KeyInstn, '&amp;Sub=1')"/>
			<a>
			<xsl:attribute name="href"><xsl:value-of select="$url3" /></xsl:attribute>
				<xsl:value-of select="StandardizedName" disable-output-escaping="yes"/>
			</a>
		</td>
	</tr>

		<xsl:for-each select="../Subsidiaries[not(CreditRatingTranche = preceding-sibling::Subsidiaries/CreditRatingTranche) and KeyCreditRatingTranche != 1]">
      <!--<xsl:sort select="preceding-sibling::FormOrder" order="ascending"/>
      <xsl:sort select="preceding-sibling::StandardizedName" order="ascending"/>
		<xsl:sort select="preceding-sibling::CreditRatingTranche" order="ascending"/>-->
      <xsl:sort select="FormOrder" order="ascending" data-type="number"/>
      <xsl:variable name="CreditRatingTrancheKeySub" select="KeyCreditRatingTranche"/>
      <xsl:variable name="position" select="position()"/>

		<xsl:if test="FormOrder != '0' and count(../Subsidiaries[KeyInstn = $KeyInstnSubsidiary and KeyCreditRatingTranche = $CreditRatingTrancheKeySub]) > 0">
		<tr>
			<td align="left" valign="top" nowrap="" class="data">
			<xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">datashade</xsl:attribute></xsl:if>
				<img SRC="/images/Interactive/blank.gif" WIDTH="16" HEIGHT="8" alt=""/><xsl:value-of select="CreditRatingTranche" disable-output-escaping="yes"/>
			</td>
			<xsl:for-each select="../Subsidiaries[not(contains(RatingAgency, 'DBRS'))]">
			<xsl:if test="$CreditRatingTrancheKeySub = KeyCreditRatingTranche and KeyInstn = $KeyInstnSubsidiary">

				<td align="right" valign="top" class="data">
					<xsl:if test="$position mod 2 = 0"><xsl:attribute name="class">datashade</xsl:attribute></xsl:if>
					<xsl:value-of select="CreditRating"/><br/><xsl:value-of select="RatingsAsOf"/>
				</td>

			</xsl:if>
			</xsl:for-each>
		</tr>
		</xsl:if>
		</xsl:for-each>

	</xsl:for-each>

	<xsl:if test="Subsidiaries/RatingsWatchLegend != ''">
	<tr><td class="data"><xsl:choose>
		<xsl:when test="$colCount>0"><xsl:attribute name="colspan"><xsl:value-of select="$colCount"/></xsl:attribute></xsl:when>
		<xsl:otherwise><xsl:attribute name="colspan"><xsl:value-of select="5"/></xsl:attribute></xsl:otherwise>
		</xsl:choose>&#160;</td></tr>
	<tr>
		<td class="data" align="left" valign="top"><!-- "RatingsWatchLegend"(given in the footer) is aligned to left Dt.07/01/2008 JR -->
		<xsl:choose>
		<xsl:when test="$colCount>0"><xsl:attribute name="colspan"><xsl:value-of select="$colCount"/></xsl:attribute></xsl:when>
		<xsl:otherwise><xsl:attribute name="colspan"><xsl:value-of select="5"/></xsl:attribute></xsl:otherwise>
		</xsl:choose>
			<xsl:value-of disable-output-escaping="yes" select="Subsidiaries/RatingsWatchLegend"/>
		</td>
	</tr>
	</xsl:if>

</xsl:if>

<!--start script for financial strength ratings -->
<xsl:if test="count(Subsidiaries[KeyCreditRatingTranche = 1]) > 0">
<tr>
	<td class="header" align="left" valign="bottom">
	<xsl:choose>
	<xsl:when test="$colCount>0"><xsl:attribute name="colspan"><xsl:value-of select="$colCount"/></xsl:attribute></xsl:when>
	<xsl:otherwise><xsl:attribute name="colspan"><xsl:value-of select="5"/></xsl:attribute></xsl:otherwise>
	</xsl:choose>
		Financial Strength Ratings (Subsidiaries)
	</td>
</tr>
</xsl:if>

<xsl:if test="count(Subsidiaries[KeyCreditRatingTranche = 1]) > 0">
	<tr>
		<td class="header" align="left" valign="bottom">&#160;</td>
		<xsl:for-each select="Subsidiaries[not(RatingAgency = preceding-sibling::Subsidiaries/RatingAgency) and not(contains(RatingAgency, 'DBRS'))]">

		<td class="header" align="right" valign="bottom">
			<xsl:value-of select="RatingAgency" disable-output-escaping="yes"/>
		</td>

		</xsl:for-each>
	</tr>

	<xsl:for-each select="Subsidiaries[not(KeyInstn = preceding-sibling::Subsidiaries/KeyInstn)]">
	<xsl:sort select="StandardizedName" order="ascending"/>
	<xsl:variable name="KeyInstnSubsidiary" select="KeyInstn"/>
	<xsl:variable name="position" select="position()"/>
	<tr>
		<td class="data" align="left" valign="top">
			<xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">datashade</xsl:attribute></xsl:if>
			<xsl:variable name="url4" select="concat('creditratingsdetail.aspx?IID=', ../Company/KeyInstn, '&amp;Key=', KeyInstn, '&amp;Sub=1')"/>
			<a>
			<xsl:attribute name="href"><xsl:value-of select="$url4" /></xsl:attribute>
				<xsl:value-of select="StandardizedName" disable-output-escaping="yes"/>
			</a>
		</td>
		<xsl:for-each select="../Subsidiaries[KeyCreditRatingTranche = 1]">
		<xsl:if test="KeyInstn = $KeyInstnSubsidiary">

			<td class="data" align="right" valign="top">
				<xsl:if test="$position mod 2 = 0"><xsl:attribute name="class">datashade</xsl:attribute></xsl:if>
				<xsl:value-of select="CreditRating"/><br/><xsl:value-of select="RatingsAsOf"/>
			</td>

		</xsl:if>
		</xsl:for-each>
	</tr>
	</xsl:for-each>
</xsl:if>
<xsl:if test="Company/Footer != ''">
<tr class="default" align="left">
	<td class="data" colspan="6"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span></td>
</tr>
</xsl:if>
</table>
</xsl:template>


<!-- Default Template Ends here. all templates below are ONLY if you are using the 'Style Template' section in the console. -->



<!-- This is Template ONE option in the console under 'Style Template'
//This template is a striped down version of the main template. Everything (data) is displayed in single rows.
//The sections will be commented on where the rows are. you should be able to just move rows (entire chunks of data) where you need too. Edits to this section are rare, mostly due to size (width of the page).
-->
<xsl:template name="CreditRatings_Data2">
<table border="0" cellspacing="0" cellpadding="3" width="100%" class="data">

<xsl:if test="Company/DisplayingCompanysName = ''">
	<tr>
		<td class="surrleft datashade" align="left" valign="top" colspan="2" nowrap="nowrap">
			<span class="defaultbold">
				<xsl:value-of select="Company/InstnName" disable-output-escaping="yes"/>
			</span>
		</td>
	</tr>
</xsl:if>
<xsl:if test="Company/DisplayingCompanysName != ''">
	<tr>
		<td class="surrleft datashade" align="left" valign="top" colspan="2" nowrap="nowrap">
			<span class="defaultbold">
				<xsl:value-of select="Company/DisplayingCompanysName" disable-output-escaping="yes"/>
			</span>
		</td>
	</tr>
</xsl:if>

<xsl:variable name="colCount" select="count(Agencies)+1"/>
<tr>
	<td class="surr datashade" align="left" valign="bottom">
		<xsl:choose>
		<xsl:when test="$colCount>0"><xsl:attribute name="colspan"><xsl:value-of select="$colCount"/></xsl:attribute></xsl:when>
		<xsl:otherwise><xsl:attribute name="colspan"><xsl:value-of select="2"/></xsl:attribute>
		</xsl:otherwise>
		</xsl:choose>
		<table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<td class="datashade" align="left" valign="bottom"><span class="defaultbold">Credit Ratings</span></td>
			<td align="right" class="datashade" nowrap="">
				<a class="defaultbold" title="View Ratings Scale">
				<xsl:variable name="keyinstn" select="Company/KeyInstn"/>
				<xsl:variable name="url1" select="concat('creditratingsscale.aspx?iid=', $keyinstn)"/>
				<xsl:attribute name="href"><xsl:value-of select="$url1"/></xsl:attribute>View Ratings Scale</a>
			</td>
		</tr>
		</table>
	</td>
</tr>
<xsl:if test="count(CreditRatings) = 0">
<tr>
	<td class="data">
	<xsl:if test="$colCount>0"><xsl:attribute name="colspan"><xsl:value-of select="$colCount"/></xsl:attribute></xsl:if>
		Credit ratings are currently unavailable.
	</td>
</tr>
<tr>
	<td class="data" align="left" valign="top" nowrap="nowrap">
	<xsl:if test="$colCount>0"><xsl:attribute name="colspan"><xsl:value-of select="$colCount"/></xsl:attribute></xsl:if>
		<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
	</td>
</tr>
</xsl:if>

<xsl:if test="count(CreditRatings) > 0">
<tr>
	<td class="botbord data" align="left" valign="bottom" height="25px">&#160;</td>
	<xsl:for-each select="CreditRatings[not(RatingAgency = preceding-sibling::CreditRatings/RatingAgency) and not(contains(RatingAgency, 'DBRS'))]">
	<td class="botbord data" align="right" valign="bottom"><span class="defaultbold"><xsl:value-of select="RatingAgency" disable-output-escaping="yes"/></span></td>

	</xsl:for-each>
</tr>
</xsl:if>

<xsl:for-each select="CreditRatings[not(CreditRatingTranche = preceding-sibling::CreditRatings/CreditRatingTranche)]">
<xsl:sort select="FormOrder" order="ascending" data-type="number"/>
<xsl:sort select="CreditRatingTranche" order="ascending"/>
<xsl:variable name="CreditRatingTrancheKey" select="KeyCreditRatingTranche"/>
<xsl:variable name="position" select="position()"/>
<xsl:if test="FormOrder > '0'">
<tr>
	<td align="left" valign="top" class="data">
		<xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">datashade</xsl:attribute></xsl:if>
		<xsl:variable name="url2" select="concat('creditratingsdetail.aspx?IID=', KeyInstn, '&amp;Key=', KeyInstn, '&amp;Sub=0')"/>
		<a><xsl:attribute name="href"><xsl:value-of select="$url2" /></xsl:attribute>
			<xsl:value-of select="CreditRatingTranche" disable-output-escaping="yes"/>
		</a>
	</td>
	<xsl:for-each select="../CreditRatings[not(contains(RatingAgency, 'DBRS'))]">
	<xsl:if test="$CreditRatingTrancheKey = KeyCreditRatingTranche">

		<td align="right" valign="top" class="data">
		<xsl:if test="$position mod 2 = 0"><xsl:attribute name="class">datashade</xsl:attribute></xsl:if>
			<xsl:value-of select="CreditRating"/><br/><xsl:value-of select="RatingsAsOf"/>
		</td>

	</xsl:if>
	</xsl:for-each>
</tr>
</xsl:if>
</xsl:for-each>

<tr>
	<td class="surr datashade" align="left" valign="bottom">
		<xsl:choose>
		<xsl:when test="$colCount>0"><xsl:attribute name="colspan"><xsl:value-of select="$colCount"/></xsl:attribute></xsl:when>
		<xsl:otherwise><xsl:attribute name="colspan"><xsl:value-of select="5"/></xsl:attribute></xsl:otherwise>
		</xsl:choose>
		<span class="defaultbold">Credit Ratings (Subsidiaries)</span>
	</td>
</tr>

<xsl:if test="count(Subsidiaries[KeyCreditRatingTranche != 1]) = 0">
<tr>
	<td class="data">
		<xsl:choose>
		<xsl:when test="$colCount>0"><xsl:attribute name="colspan"><xsl:value-of select="$colCount"/></xsl:attribute></xsl:when>
		<xsl:otherwise><xsl:attribute name="colspan"><xsl:value-of select="5"/></xsl:attribute></xsl:otherwise>
		</xsl:choose>
		Credit ratings for subsidiaries are currently unavailable.
	</td>
</tr>
<tr>
	<td class="data" align="left" valign="top" nowrap="nowrap">
		<xsl:choose>
		<xsl:when test="$colCount>0"><xsl:attribute name="colspan"><xsl:value-of select="$colCount"/></xsl:attribute></xsl:when>
		<xsl:otherwise><xsl:attribute name="colspan"><xsl:value-of select="5"/></xsl:attribute></xsl:otherwise>
		</xsl:choose>
		<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
	</td>
</tr>
</xsl:if>

<xsl:if test="count(Subsidiaries[KeyCreditRatingTranche != 1]) > 0">
<tr>
	<td class="botbord data" align="left" valign="bottom" height="25px">&#160;</td>
	<xsl:for-each select="Subsidiaries[not(RatingAgency = preceding-sibling::Subsidiaries/RatingAgency) and not(contains(RatingAgency, 'DBRS'))]">

		<td class="botbord data" align="right" valign="bottom">
			<span class="defaultbold"><xsl:value-of select="RatingAgency" disable-output-escaping="yes"/></span>
		</td>

	</xsl:for-each>
	</tr>

	<xsl:for-each select="Subsidiaries[not(KeyInstn = preceding-sibling::Subsidiaries/KeyInstn) and KeyCreditRatingTranche != 1]">
	<xsl:sort select="StandardizedName" order="ascending"/>
	<xsl:variable name="KeyInstnSubsidiary" select="KeyInstn"/>
	<tr>
		<td class="data" align="left" valign="bottom" colspan="6">
			<xsl:variable name="url3" select="concat('creditratingsdetail.aspx?IID=', ../Company/KeyInstn, '&amp;Key=', KeyInstn, '&amp;Sub=1')"/>
			<a>
			<xsl:attribute name="href"><xsl:value-of select="$url3" /></xsl:attribute>
				<xsl:value-of select="StandardizedName" disable-output-escaping="yes"/>
			</a>
		</td>
	</tr>

		<xsl:for-each select="../Subsidiaries[not(CreditRatingTranche = preceding-sibling::Subsidiaries/CreditRatingTranche) and KeyCreditRatingTranche != 1]">
      <!--<xsl:sort select="preceding-sibling::FormOrder" order="ascending"/>
      <xsl:sort select="preceding-sibling::StandardizedName" order="ascending"/>
		<xsl:sort select="preceding-sibling::CreditRatingTranche" order="ascending"/>-->
      <xsl:sort select="FormOrder" order="ascending" data-type="number"/>
      <xsl:variable name="CreditRatingTrancheKeySub" select="KeyCreditRatingTranche"/>
      <xsl:variable name="position" select="position()"/>

		<xsl:if test="FormOrder != '0' and count(../Subsidiaries[KeyInstn = $KeyInstnSubsidiary and KeyCreditRatingTranche = $CreditRatingTrancheKeySub]) > 0">
		<tr>
			<td align="left" valign="top" nowrap="" class="data">
			<xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">datashade</xsl:attribute></xsl:if>
				<img SRC="/images/Interactive/blank.gif" WIDTH="16" HEIGHT="8"  alt=""/><xsl:value-of select="CreditRatingTranche" disable-output-escaping="yes"/>
			</td>
			<xsl:for-each select="../Subsidiaries[not(contains(RatingAgency, 'DBRS'))]">
			<xsl:if test="$CreditRatingTrancheKeySub = KeyCreditRatingTranche and KeyInstn = $KeyInstnSubsidiary">

				<td align="right" valign="top" class="data">
					<xsl:if test="$position mod 2 = 0"><xsl:attribute name="class">datashade</xsl:attribute></xsl:if>
					<xsl:value-of select="CreditRating"/><br/><xsl:value-of select="RatingsAsOf"/>
				</td>

			</xsl:if>
			</xsl:for-each>
		</tr>
		</xsl:if>
		</xsl:for-each>

	</xsl:for-each>

	<xsl:if test="Subsidiaries/RatingsWatchLegend != ''">
	<tr><td class="data"><xsl:choose>
		<xsl:when test="$colCount>0"><xsl:attribute name="colspan"><xsl:value-of select="$colCount"/></xsl:attribute></xsl:when>
		<xsl:otherwise><xsl:attribute name="colspan"><xsl:value-of select="5"/></xsl:attribute></xsl:otherwise>
		</xsl:choose>&#160;</td></tr>
	<tr>
		<td class="data" align="left" valign="top"><!-- "RatingsWatchLegend"(given in the footer) is aligned to left Dt.07/01/2008 JR -->
		<xsl:choose>
		<xsl:when test="$colCount>0"><xsl:attribute name="colspan"><xsl:value-of select="$colCount"/></xsl:attribute></xsl:when>
		<xsl:otherwise><xsl:attribute name="colspan"><xsl:value-of select="5"/></xsl:attribute></xsl:otherwise>
		</xsl:choose>
			<xsl:value-of disable-output-escaping="yes" select="Subsidiaries/RatingsWatchLegend"/>
		</td>
	</tr>
	</xsl:if>

</xsl:if>

<!--start script for financial strength ratings -->
<xsl:if test="count(Subsidiaries[KeyCreditRatingTranche = 1]) > 0">
<tr>
	<td class="surr datashade" align="left" valign="bottom">
	<xsl:choose>
	<xsl:when test="$colCount>0"><xsl:attribute name="colspan"><xsl:value-of select="$colCount"/></xsl:attribute></xsl:when>
	<xsl:otherwise><xsl:attribute name="colspan"><xsl:value-of select="5"/></xsl:attribute></xsl:otherwise>
	</xsl:choose>
		<span class="defaultbold">Financial Strength Ratings (Subsidiaries)</span>
	</td>
</tr>
</xsl:if>

<xsl:if test="count(Subsidiaries[KeyCreditRatingTranche = 1]) > 0">
	<tr>
		<td class="botbord data" align="left" valign="bottom" height="25px">&#160;</td>
		<xsl:for-each select="Subsidiaries[not(RatingAgency = preceding-sibling::Subsidiaries/RatingAgency) and not(contains(RatingAgency, 'DBRS'))]">

		<td class="botbord data" align="right" valign="bottom">
			<span class="defaultbold"><xsl:value-of select="RatingAgency" disable-output-escaping="yes"/></span>
		</td>

		</xsl:for-each>
	</tr>

	<xsl:for-each select="Subsidiaries[not(KeyInstn = preceding-sibling::Subsidiaries/KeyInstn)]">
	<xsl:sort select="StandardizedName" order="ascending"/>
	<xsl:variable name="KeyInstnSubsidiary" select="KeyInstn"/>
	<xsl:variable name="position" select="position()"/>
	<tr>
		<td class="data" align="left" valign="top">
			<xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">datashade</xsl:attribute></xsl:if>
			<xsl:variable name="url4" select="concat('creditratingsdetail.aspx?IID=', ../Company/KeyInstn, '&amp;Key=', KeyInstn, '&amp;Sub=1')"/>
			<a>
			<xsl:attribute name="href"><xsl:value-of select="$url4" /></xsl:attribute>
				<xsl:value-of select="StandardizedName" disable-output-escaping="yes"/>
			</a>
		</td>
		<xsl:for-each select="../Subsidiaries[KeyCreditRatingTranche = 1]">
		<xsl:if test="KeyInstn = $KeyInstnSubsidiary">

			<td class="data" align="right" valign="top">
				<xsl:if test="$position mod 2 = 0"><xsl:attribute name="class">datashade</xsl:attribute></xsl:if>
				<xsl:value-of select="CreditRating"/><br/><xsl:value-of select="RatingsAsOf"/>
			</td>

		</xsl:if>
		</xsl:for-each>
	</tr>
	</xsl:for-each>
</xsl:if>
</table>
</xsl:template>



<xsl:template name="CR_FOOTER2">
<xsl:if test="Company/Footer != ''">
<tr class="default" align="left">
	<td class="data" colspan="2"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span></td>
</tr>
</xsl:if>
</xsl:template>

  <!-- Template ONE Ends here. -->



<!-- Creditratings temp3 -->
<xsl:template name="CreditRatings_Data3">
<table border="0" cellspacing="0" cellpadding="3" width="100%" class="table2">

<xsl:if test="Company/DisplayingCompanysName = ''">
	<tr>
		<td class="table1_item datashade" align="left" valign="top" colspan="2" nowrap="nowrap">
			<span class="defaultbold"><xsl:value-of select="Company/InstnName" disable-output-escaping="yes"/></span>
		</td>
	</tr>
</xsl:if>
<xsl:if test="Company/DisplayingCompanysName != ''">
	<tr>
		<td class="table1_item datashade" align="left" valign="top" colspan="2" nowrap="nowrap">
			<span class="defaultbold">
				<xsl:value-of select="Company/DisplayingCompanysName" disable-output-escaping="yes"/>
			</span>
		</td>
	</tr>				
</xsl:if>

<xsl:variable name="colCount" select="count(Agencies)+1"/>
<tr>
	<td class="table1_item datashade" align="left" valign="bottom">
		<xsl:choose>
		<xsl:when test="$colCount>0"><xsl:attribute name="colspan"><xsl:value-of select="$colCount"/></xsl:attribute></xsl:when>
		<xsl:otherwise><xsl:attribute name="colspan"><xsl:value-of select="2"/></xsl:attribute>
		</xsl:otherwise>
		</xsl:choose>
		<table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<td class="datashade" align="left" valign="bottom"><span class="defaultbold">Credit Ratings</span></td>
			<td align="right" class="datashade" nowrap="">
				<a class="defaultbold" title="View Ratings Scale">
				<xsl:variable name="keyinstn" select="Company/KeyInstn"/>
				<xsl:variable name="url1" select="concat('creditratingsscale.aspx?iid=', $keyinstn)"/>
				<xsl:attribute name="href"><xsl:value-of select="$url1"/></xsl:attribute>View Ratings Scale</a>
			</td>
		</tr>
		</table>
	</td>
</tr>
<xsl:if test="count(CreditRatings) = 0">
<tr>
	<td class="data">
	<xsl:if test="$colCount>0"><xsl:attribute name="colspan"><xsl:value-of select="$colCount"/></xsl:attribute></xsl:if>
		Credit ratings are currently unavailable.
	</td>
</tr>
<tr>
	<td class="data" align="left" valign="top" nowrap="nowrap">
	<xsl:if test="$colCount>0"><xsl:attribute name="colspan"><xsl:value-of select="$colCount"/></xsl:attribute></xsl:if>
		<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
	</td>
</tr>
</xsl:if>

<xsl:if test="count(CreditRatings) > 0">
<tr>
	<td class="table1_item data" align="left" valign="bottom" height="25px">&#160;</td>
	<xsl:for-each select="CreditRatings[not(RatingAgency = preceding-sibling::CreditRatings/RatingAgency) and not(contains(RatingAgency, 'DBRS'))]">
	<td class="table1_item data" align="right" valign="bottom"><span class="defaultbold"><xsl:value-of select="RatingAgency" disable-output-escaping="yes"/></span></td>

	</xsl:for-each>
</tr>
</xsl:if>

<xsl:for-each select="CreditRatings[not(CreditRatingTranche = preceding-sibling::CreditRatings/CreditRatingTranche)]">
<xsl:sort select="FormOrder" order="ascending" data-type="number"/>
<xsl:sort select="CreditRatingTranche" order="ascending"/>
<xsl:variable name="CreditRatingTrancheKey" select="KeyCreditRatingTranche"/>
<xsl:variable name="position" select="position()"/>
<xsl:if test="FormOrder > '0'">
<tr>
	<td align="left" valign="top" class="data">
		<xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">datashade</xsl:attribute></xsl:if>
		<xsl:variable name="url2" select="concat('creditratingsdetail.aspx?IID=', KeyInstn, '&amp;Key=', KeyInstn, '&amp;Sub=0')"/>
		<a><xsl:attribute name="href"><xsl:value-of select="$url2" /></xsl:attribute>
			<xsl:value-of select="CreditRatingTranche" disable-output-escaping="yes"/>
		</a>
	</td>
	<xsl:for-each select="../CreditRatings[not(contains(RatingAgency, 'DBRS'))]">
	<xsl:if test="$CreditRatingTrancheKey = KeyCreditRatingTranche">

		<td align="right" valign="top" class="data">
		<xsl:if test="$position mod 2 = 0"><xsl:attribute name="class">datashade</xsl:attribute></xsl:if>
			<xsl:value-of select="CreditRating"/><br/><xsl:value-of select="RatingsAsOf"/>
		</td>

	</xsl:if>
	</xsl:for-each>
</tr>
</xsl:if>
</xsl:for-each>

<tr>
	<td class="report_top table1_item datashade" align="left" valign="bottom">
		<xsl:choose>
		<xsl:when test="$colCount>0"><xsl:attribute name="colspan"><xsl:value-of select="$colCount"/></xsl:attribute></xsl:when>
		<xsl:otherwise><xsl:attribute name="colspan"><xsl:value-of select="5"/></xsl:attribute></xsl:otherwise>
		</xsl:choose>
		<span class="defaultbold">Credit Ratings (Subsidiaries)</span>
	</td>
</tr>

<xsl:if test="count(Subsidiaries[KeyCreditRatingTranche != 1]) = 0">
<tr>
	<td class="data">
		<xsl:choose>
		<xsl:when test="$colCount>0"><xsl:attribute name="colspan"><xsl:value-of select="$colCount"/></xsl:attribute></xsl:when>
		<xsl:otherwise><xsl:attribute name="colspan"><xsl:value-of select="5"/></xsl:attribute></xsl:otherwise>
		</xsl:choose>
		Credit ratings for subsidiaries are currently unavailable.
	</td>
</tr>
<tr>
	<td class="data" align="left" valign="top" nowrap="nowrap">
		<xsl:choose>
		<xsl:when test="$colCount>0"><xsl:attribute name="colspan"><xsl:value-of select="$colCount"/></xsl:attribute></xsl:when>
		<xsl:otherwise><xsl:attribute name="colspan"><xsl:value-of select="5"/></xsl:attribute></xsl:otherwise>
		</xsl:choose>
		<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
	</td>
</tr>
</xsl:if>

<xsl:if test="count(Subsidiaries[KeyCreditRatingTranche != 1]) > 0">
<tr>
	<td class="table1_item datashade" align="left" valign="bottom" height="25px">&#160;</td>
	<xsl:for-each select="Subsidiaries[not(RatingAgency = preceding-sibling::Subsidiaries/RatingAgency) and not(contains(RatingAgency, 'DBRS'))]">

		<td class="table1_item datashade" align="right" valign="bottom">
			<span class="defaultbold"><xsl:value-of select="RatingAgency" disable-output-escaping="yes"/></span>
		</td>

	</xsl:for-each>
	</tr>

	<xsl:for-each select="Subsidiaries[not(KeyInstn = preceding-sibling::Subsidiaries/KeyInstn) and KeyCreditRatingTranche != 1]">
	<xsl:sort select="StandardizedName" order="ascending"/>
	<xsl:variable name="KeyInstnSubsidiary" select="KeyInstn"/>
	<tr>
		<td class="data" align="left" valign="bottom" colspan="6">
			<xsl:variable name="url3" select="concat('creditratingsdetail.aspx?IID=', ../Company/KeyInstn, '&amp;Key=', KeyInstn, '&amp;Sub=1')"/>
			<a>
			<xsl:attribute name="href"><xsl:value-of select="$url3" /></xsl:attribute>
				<xsl:value-of select="StandardizedName" disable-output-escaping="yes"/>
			</a>
		</td>
	</tr>

		<xsl:for-each select="../Subsidiaries[not(CreditRatingTranche = preceding-sibling::Subsidiaries/CreditRatingTranche) and KeyCreditRatingTranche != 1]">
      <!--<xsl:sort select="preceding-sibling::FormOrder" order="ascending"/>
      <xsl:sort select="preceding-sibling::StandardizedName" order="ascending"/>
		<xsl:sort select="preceding-sibling::CreditRatingTranche" order="ascending"/>-->
      <xsl:sort select="FormOrder" order="ascending" data-type="number"/>
      <xsl:variable name="CreditRatingTrancheKeySub" select="KeyCreditRatingTranche"/>
      <xsl:variable name="position" select="position()"/>

		<xsl:if test="FormOrder != '0' and count(../Subsidiaries[KeyInstn = $KeyInstnSubsidiary and KeyCreditRatingTranche = $CreditRatingTrancheKeySub]) > 0">
		<tr>
			<td align="left" valign="top" nowrap="" class="data">
			<xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">datashade</xsl:attribute></xsl:if>
				<img SRC="/images/Interactive/blank.gif" WIDTH="16" HEIGHT="8"  alt=""/><xsl:value-of select="CreditRatingTranche" disable-output-escaping="yes"/>
			</td>
			<xsl:for-each select="../Subsidiaries[not(contains(RatingAgency, 'DBRS'))]">
			<xsl:if test="$CreditRatingTrancheKeySub = KeyCreditRatingTranche and KeyInstn = $KeyInstnSubsidiary">

				<td align="right" valign="top" class="data">
					<xsl:if test="$position mod 2 = 0"><xsl:attribute name="class">datashade</xsl:attribute></xsl:if>
					<xsl:value-of select="CreditRating"/><br/><xsl:value-of select="RatingsAsOf"/>
				</td>

			</xsl:if>
			</xsl:for-each>
		</tr>
		</xsl:if>
		</xsl:for-each>

	</xsl:for-each>

	<xsl:if test="Subsidiaries/RatingsWatchLegend != ''">
	<tr><td class="data"><xsl:choose>
		<xsl:when test="$colCount>0"><xsl:attribute name="colspan"><xsl:value-of select="$colCount"/></xsl:attribute></xsl:when>
		<xsl:otherwise><xsl:attribute name="colspan"><xsl:value-of select="5"/></xsl:attribute></xsl:otherwise>
		</xsl:choose>&#160;</td></tr>
	<tr>
		<td class="data" align="left" valign="top"><!-- "RatingsWatchLegend"(given in the footer) is aligned to left Dt.07/01/2008 JR -->
		<xsl:choose>
		<xsl:when test="$colCount>0"><xsl:attribute name="colspan"><xsl:value-of select="$colCount"/></xsl:attribute></xsl:when>
		<xsl:otherwise><xsl:attribute name="colspan"><xsl:value-of select="5"/></xsl:attribute></xsl:otherwise>
		</xsl:choose>
			<xsl:value-of disable-output-escaping="yes" select="Subsidiaries/RatingsWatchLegend"/>
		</td>
	</tr>
	</xsl:if>

</xsl:if>

<!--start script for financial strength ratings -->
<xsl:if test="count(Subsidiaries[KeyCreditRatingTranche = 1]) > 0">
<tr>
	<td class="report_top table1_item datashade" align="left" valign="bottom">
	<xsl:choose>
	<xsl:when test="$colCount>0"><xsl:attribute name="colspan"><xsl:value-of select="$colCount"/></xsl:attribute></xsl:when>
	<xsl:otherwise><xsl:attribute name="colspan"><xsl:value-of select="5"/></xsl:attribute></xsl:otherwise>
	</xsl:choose>
		<span class="defaultbold">Financial Strength Ratings (Subsidiaries)</span>
	</td>
</tr>
</xsl:if>

<xsl:if test="count(Subsidiaries[KeyCreditRatingTranche = 1]) > 0">
	<tr>
		<td class="table1_item data" align="left" valign="bottom" height="25px">&#160;</td>
		<xsl:for-each select="Subsidiaries[not(RatingAgency = preceding-sibling::Subsidiaries/RatingAgency) and not(contains(RatingAgency, 'DBRS'))]">

		<td class="table1_item data" align="right" valign="bottom">
			<span class="defaultbold"><xsl:value-of select="RatingAgency" disable-output-escaping="yes"/></span>
		</td>

		</xsl:for-each>
	</tr>

	<xsl:for-each select="Subsidiaries[not(KeyInstn = preceding-sibling::Subsidiaries/KeyInstn)]">
	<xsl:sort select="StandardizedName" order="ascending"/>
	<xsl:variable name="KeyInstnSubsidiary" select="KeyInstn"/>
	<xsl:variable name="position" select="position()"/>
	<tr>
		<td class="data" align="left" valign="top">
			<xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">datashade</xsl:attribute></xsl:if>
			<xsl:variable name="url4" select="concat('creditratingsdetail.aspx?IID=', ../Company/KeyInstn, '&amp;Key=', KeyInstn, '&amp;Sub=1')"/>
			<a>
			<xsl:attribute name="href"><xsl:value-of select="$url4" /></xsl:attribute>
				<xsl:value-of select="StandardizedName" disable-output-escaping="yes"/>
			</a>
		</td>
		<xsl:for-each select="../Subsidiaries[KeyCreditRatingTranche = 1 and not(contains(RatingAgency, 'DBRS'))]">
		<xsl:if test="KeyInstn = $KeyInstnSubsidiary">

			<td class="data" align="right" valign="top">
				<xsl:if test="$position mod 2 = 0"><xsl:attribute name="class">datashade</xsl:attribute></xsl:if>
				<xsl:value-of select="CreditRating"/><br/><xsl:value-of select="RatingsAsOf"/>
			</td>

		</xsl:if>
		</xsl:for-each>
	</tr>
	</xsl:for-each>
</xsl:if>
</table>
</xsl:template>


<xsl:template name="CR_FOOTER3">
<xsl:if test="Company/Footer != ''">
<tr class="default" align="left">
	<td class="data" colspan="2"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span></td>
</tr>
</xsl:if>
</xsl:template>

<!--  End Template 3 -->





<!-- Main XSLT templates are here. these are the templates which are actually being displayed in the page. this is for TOP LEVEL editing. if you dont need to use the individual templates above you can do your main editing here

when pulling THESE templates below you must carry the top comment with them and uncomment them when you drop it into the company specific xslt.




-->



<xsl:template match="irw:CreditRatings">

<xsl:variable name="TemplateName" select="Company/TemplateName"/>
<div id="CreditRatings">
  <xsl:choose>
    <!-- Template 1 -->
    <xsl:when test="$TemplateName = 'AltCreditRate1'">
      <xsl:call-template name="TemplateONEstylesheet" />
      <table border="0" cellspacing="0" cellpadding="3" width="100%">
        <xsl:call-template name="Title_S2"/>
        <tr>
          <td colspan="2" class="leftTOPbord">
            <table border="0" cellspacing="0" cellpadding="4" width="100%" class="data">
              <tr align="left">
                <td valign="top" class="data"><xsl:call-template name="CreditRatings_Data2"/></td>
              </tr>
            </table>
          </td>
        </tr>
        <xsl:if test="Company/Footer != ''">
          <xsl:call-template name="CR_FOOTER2"/>
        </xsl:if>
      </table>
      <xsl:call-template name="Copyright"/>
    </xsl:when>
    <!-- End Template 1 -->    
    <!-- Template 3 -->
    <xsl:when test="$TemplateName = 'AltCreditRate3'">
      <xsl:call-template name="TemplateTHREEstylesheet" />
      <xsl:call-template name="Title_T3"/>
      <table border="0" cellspacing="0" cellpadding="3" width="100%" class="table2">
        <tr align="center">
          <td valign="top" class="data">
            <table border="0" cellspacing="0" cellpadding="4" width="100%" class="data">
              <tr align="left">
                <td valign="top" class="data"><xsl:call-template name="CreditRatings_Data3"/></td>
              </tr>
            </table>
          </td>
        </tr>
        <xsl:if test="Company/Footer != ''">
          <xsl:call-template name="CR_FOOTER3"/>
        </xsl:if>
      </table>
      <xsl:call-template name="Copyright"/>
    </xsl:when>
    <!-- End Template 3 -->
    
    <!-- Template Default -->
    <xsl:otherwise>
      <table border="0" cellspacing="0" cellpadding="3" width="100%" class="colordark">
        <xsl:call-template name="Title_S1"/>
        <tr>
          <td colspan="2">
            <table border="0" cellspacing="0" cellpadding="4" width="100%">
              <tr align="center">
                <td valign="top" class="colorlight">
                  <xsl:call-template name="CreditRatings_Data"/>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      <xsl:call-template name="Copyright"/>
    </xsl:otherwise>
  </xsl:choose>
</div>  
</xsl:template>

</xsl:stylesheet>
