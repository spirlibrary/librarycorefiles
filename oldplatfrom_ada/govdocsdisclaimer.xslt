<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.ord/TR/REC-html40"
	xmlns:irw="http://www.snl.com/xml/irw">

<xsl:output method="html" media-type="text/html" indent="no"/>

<xsl:template match="GovDocsDisclaimer">
<HTML>
<head><title>Disclaimer</title></head>
<xsl:variable name="KeyInstn" select="Company/KeyInstn"/>
<xsl:variable name="anchortag1" select="concat('/interactive/lookandfeel/', $KeyInstn, '/style.css')"/>
<xsl:variable name="anchortag2" select="concat('/Interactive/LookAndFeel/IRStyles/live_', $KeyInstn, '.css')"/>
<xsl:variable name="anchortag3" select="concat('/Interactive/LookAndFeel/IRStyles/live_', $KeyInstn, '_suppl.css')"/>
<link REL="stylesheet" TYPE="text/css"><xsl:attribute name="href"><xsl:value-of select="$anchortag1"/></xsl:attribute></link>
<link REL="stylesheet" TYPE="text/css"><xsl:attribute name="href"><xsl:value-of select="$anchortag2"/></xsl:attribute></link>
<link REL="stylesheet" TYPE="text/css"><xsl:attribute name="href"><xsl:value-of select="$anchortag3"/></xsl:attribute></link>
<body>

<xsl:variable name="TemplateName" select="Company/TemplateName"/>
<xsl:choose>
<!-- Template 2 -->
<xsl:when test="$TemplateName = 'AltDisclaim1'">
<SCRIPT LANGUAGE="JavaScript">
	function OpenDisclaimerWindow(url) {
	var page = url;
	var winprops = "toolbar=yes,location=yes,directories=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes";
	window.open(page, "", winprops);
	self.close();
	}
</SCRIPT>
<xsl:call-template name="TemplateONEstylesheet" />
<xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" CLASS="">
<TR>
<TD CLASS="title2colordark titletest" nowrap="" align="left" valign="top">Gov Docs Disclaimer<br /><IMG SRC="" WIDTH="2" HEIGHT="1" alt="" /><span class="title2colordark titletest2"><xsl:value-of select="$CompanyNameCaps"/>&#160;(<xsl:value-of select="Company/Exchange"/>&#160;-&#160;<xsl:value-of select="Company/Ticker"/>)&#160;</span></TD>
</TR>
	<TR class="default" align="left">
		<TD class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Header" disable-output-escaping="yes"/></span></TD>
	</TR>
	<TR ALIGN="CENTER">
		<TD CLASS="leftTOPbord" colspan="2">
			<TABLE border="0" cellspacing="0" cellpadding="2"  width="100%">
				<tr>
					<td class="colorlight"><br />
						<span class="default"><xsl:value-of select="Disclaimer/DisclaimerText" disable-output-escaping="yes"/></span>
					<br /><br /></td>
				</tr>
				<tr>
					<td	align="center" class="colorlight" colspan="4">
						<a href="#" class="fielddef" onClick="self.close()">
							Back
						</a>
						&#160;&#160;&#160;&#160;&#160;&#160;
						<xsl:variable name="url" select="UISettings/website" />
						<xsl:choose>
						<xsl:when test="$url = ''">
							<a href="#" class="fielddef" onClick="self.close()">
								Continue
							</a>
						</xsl:when>
						<xsl:otherwise>
							<a href="#" class="fielddef" onClick="OpenDisclaimerWindow('{$url}'); return true">
								Continue
							</a>
						</xsl:otherwise>
						</xsl:choose>
						<br /><br />
					</td>	
				</tr>
			</TABLE>
		</TD>
	</TR>
</TABLE>
</xsl:when>
<!-- End Template 2 -->
<!-- Template 3 -->
<xsl:when test="$TemplateName = 'AltDisclaim2'">
<SCRIPT LANGUAGE="JavaScript">
	function OpenDisclaimerWindow(url) {
	var page = url;
	var winprops = "toolbar=yes,location=yes,directories=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes";
	window.open(page, "", winprops);
	self.close();
	}
</SCRIPT>
<xsl:call-template name="TemplateTWOstylesheet" />
<xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" CLASS="">
<TR>
<TD CLASS="title2 titletest" nowrap="" align="left" valign="top">Markets Disclaimer<br /><IMG SRC="" WIDTH="2" HEIGHT="1" alt="" /><span class="title2 titletest2"><xsl:value-of select="$CompanyNameCaps"/>&#160;(<xsl:value-of select="Company/Exchange"/>&#160;-&#160;<xsl:value-of select="Company/Ticker"/>)&#160;</span></TD>
</TR>
	<TR class="default" align="left">
		<TD class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Header" disable-output-escaping="yes"/></span></TD>
	</TR>
	<TR ALIGN="CENTER">
		<TD CLASS="leftTOPbord" colspan="2">
			<TABLE border="0" cellspacing="0" cellpadding="2"  width="100%">
				<tr>
					<td class="colorlight"><br />
						<span class="default"><xsl:value-of select="Disclaimer/DisclaimerText" disable-output-escaping="yes"/></span>
					<br /><br /></td>
				</tr>
				<tr>
					<td	align="center" class="colorlight" colspan="4">
						<a href="#" class="fielddef" onClick="self.close()">
							Back
						</a>
						&#160;&#160;&#160;&#160;&#160;&#160;
						<xsl:variable name="url" select="UISettings/website" />
						<xsl:choose>
						<xsl:when test="$url = ''">
							<a href="#" class="fielddef" onClick="self.close()">
								Continue
							</a>
						</xsl:when>
						<xsl:otherwise>
							<a href="#" class="fielddef" onClick="OpenDisclaimerWindow('{$url}'); return true">
								Continue
							</a>
						</xsl:otherwise>
						</xsl:choose>
						<br /><br />
					</td>	
				</tr>
			</TABLE>
		</TD>
	</TR>
</TABLE>
</xsl:when>
<!--End  Template 3 -->

<!-- Template Default -->
<xsl:otherwise>
<SCRIPT LANGUAGE="JavaScript">
	function OpenDisclaimerWindow(url) {
	var page = url;
	var winprops = "toolbar=yes,location=yes,directories=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes";
	window.open(page, "", winprops);
	self.close();
	}
</SCRIPT>


<xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>

<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" CLASS="colordark">
	<TR>
		<TD CLASS="colordark" nowrap="1">
			<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
			<SPAN CLASS="title1light">Gov Docs Disclaimer</SPAN>
	    </TD>
		<TD CLASS="colordark" nowrap="1" align="right" valign="top">
			<span class="subtitlelight"><xsl:value-of select="$CompanyNameCaps" /> (<xsl:value-of select="Company/Exchange"/> - <xsl:value-of select="Company/Ticker"/>)</span>
			<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
		</TD>
	</TR>
	<TR ALIGN="CENTER">
		<TD CLASS="colordark" colspan="2">
			<TABLE border="0" cellspacing="0" cellpadding="2" class="data" width="100%">
				<tr>
					<td class="colorlight"><br />
						<span class="default"><xsl:value-of select="Disclaimer/DisclaimerText" disable-output-escaping="yes"/></span>
					<br /><br /></td>
				</tr>
				<tr>
					<td	align="center" class="colorlight" colspan="4">
						<a href="#" class="fielddef" onClick="self.close()">
							Back
						</a>
						&#160;&#160;&#160;&#160;&#160;&#160;
						<xsl:variable name="url" select="UISettings/website" />
						<xsl:choose>
						<xsl:when test="$url = ''">
							<a href="#" class="fielddef" onClick="self.close()">
								Continue
							</a>
						</xsl:when>
						<xsl:otherwise>
							<a href="#" class="fielddef" onClick="OpenDisclaimerWindow('{$url}'); return true">
								Continue
							</a>
						</xsl:otherwise>
						</xsl:choose>
						<br /><br />
					</td>	
				</tr>
			</TABLE>
		</TD>
	</TR>
</TABLE>
</xsl:otherwise>


</xsl:choose>



<xsl:call-template name="Copyright"/>

</body>
</HTML>
</xsl:template>

</xsl:stylesheet>