<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.ord/TR/REC-html40"
	xmlns:irw="http://www.snl.com/xml/irw">

<xsl:output method="html" media-type="text/html" indent="no"/>

<xsl:template name="BranchMini">
<xsl:call-template name="Template2stylesheet"/>
<xsl:call-template name="TemplateONEstylesheet"/>
<xsl:variable name="keyinstn" select="Company/KeyInstn"/>
<xsl:variable name="formaction" select="concat('BranchLocatorMap.aspx?IID=', $keyinstn, '&amp;generateMap=1')"></xsl:variable>
<FORM name="branchsearch" id="branchsearch" method="POST" target="_blank">
<xsl:attribute name="action">
<xsl:value-of select="$formaction"/>
</xsl:attribute>
<TABLE border="0" cellpadding="3" cellspacing="0" width="100px">
	<TR align="center">
		<TD class="biggysmall">ATM &amp; Branch Locations</TD>
	</TR>
	<TR>
		<TD colspan="2">
			<TABLE width="100%" border="0" cellspacing="0" cellpadding="2">
				<TR>
					<TD class="small" nowrap="" valign="bottom">
						<label class="visuallyhidden" for="ATM">ATM</label><input class="data" type="checkbox" id="ATM" name="FacilityType" value="1"/>ATMs</TD>
					<TD class="small" nowrap="" valign="bottom" align="right">
						<label class="visuallyhidden" for="Branch">Branch</label><input class="data" type="checkbox" id="Branch" name="FacilityType" checked="checked" value="0"/>Branches</TD>
				</TR>
        <TR>
          <TD colspan="2" class="small" nowrap="" align="center" valign="bottom">
			  <label class="visuallyhidden" for="LoanOffices">Loan Offices</label>
            <input class="data" type="checkbox" id="LoanOffices" name="FacilityType" value="2"/>Loan Offices
          </TD>
        </TR>
				<TR>
					<!-- <TD class="small" nowrap="" valign="bottom"><span class="defaultbold">ZIP Code:</span></TD> -->
					
					<TD class="" colspan="2" valign="bottom" align="center"><br /><label class="visuallyhidden" for="ZIP">ZIP</label><input onFocus="clearText()" class="data" type="text" size="5" name="ZIP" id="ZIP" value="Enter Zip"/>&#160;<input class="submit" type="submit" align="left" value="Submit" ></input></TD>
				</TR>
				<TR>
					<TD colspan="2" class="small" nowrap="" align="center" valign="bottom"><xsl:variable name="URL" select="concat('BranchLocatorMap.aspx?IID=',$keyinstn)"/><A style="text-decoration: none;font-size:11px;" target="_blank" title="More Options"><xsl:attribute name="href"><xsl:value-of select="$URL"/></xsl:attribute><Strong>More options</Strong></A></TD>
				</TR>
			</TABLE>
			<input type="hidden" name="Radius" id="Radius" value="50"/>
		</TD>
	</TR>
</TABLE>
</FORM>
</xsl:template>

<xsl:template match="BranchLocatorMapMini">
<xsl:variable name="TemplateName" select="Company/TemplateName"/>


<HTML>
<head>
<script type="text/javascript">
    function clearText() {
      if(document.getElementById('ZIP').value=="Enter Zip") {
      	document.getElementById('ZIP').value=""
      		}
           }
  </script>

<title>
<xsl:value-of select="Company/Ticker" />&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
</title>
</head>
<body leftmargin="0" topmargin="0" marginheight="0" marginwidth="0">
<table cellpadding="0" cellspacing="3" border="0" width="175px" align="center">
	<tr>
		<td><xsl:call-template name="BranchMini"/></td>
	</tr>
</table>
</body>
</HTML>

</xsl:template>
</xsl:stylesheet>
