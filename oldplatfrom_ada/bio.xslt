<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.ord/TR/REC-html40"
	xmlns:irw="http://www.snl.com/xml/irw">
	
<xsl:output method="html" media-type="text/html" indent="no"/>

<!-- SP 78905 RC: 08/10/2009  
"Information not available" sentence print two times. This bug shows all templates.
-->

<!-- Start Default Template here-->

<!--
//This is the default template for the bio page. Most changes are to either remove the company name, ticker, or exchange. you can also change the name of the page or remove this row all together. 
//These templates below are broken up how the page is displayed.

-->

<xsl:template name="Bio_Title">
<xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
	<TR>
		<TD CLASS="colordark" NOWRAP="NOWRAP" align="left"><SPAN CLASS="title1light"><xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/></SPAN> 
	    </TD>
		<TD CLASS="colordark" NOWRAP="NOWRAP" align="right" valign="top"><span class="subtitlelight"><xsl:value-of select="$CompanyNameCaps"/> (<xsl:value-of select="Company/Exchange"/> - <xsl:value-of select="Company/Ticker"/>)</span><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></TD>
	</TR>
		<TR class="default" align="left">
			<TD class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Header" disable-output-escaping="yes"/></span></TD>
	</TR>
</xsl:template>



<xsl:template name="Bio_Data">
      <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="4" WIDTH="100%"> 
		<TR ALIGN="CENTER">
			<TD CLASS="colorlight" valign="top">
				<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="4" WIDTH="100%">
				<xsl:choose>				
				<xsl:when test="Bio/BioCount &lt; 1">    
					<TR>
						<TD CLASS="colorlight"><span class="defaultbold">Information not available.</span></TD>
					</TR>
				</xsl:when> 
				<xsl:otherwise>
					<TR>
						<TD CLASS="colorlight" valign="top"><span class="title2"><xsl:value-of select="Bio/Title" disable-output-escaping="yes"/></span></TD>
					</TR>
					<TR>
						<TD valign="top">
							<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="4" WIDTH="100%">
							<TR>
								<TD CLASS="header" colspan="2" valign="top"><xsl:value-of select="Bio/PersonName" disable-output-escaping="yes"/></TD>
							</TR>
							<TR>
							<xsl:if test="Bio/OnlineImageFilePath != ''"> 
							<TD valign="top" class="data">
								<IMG BORDER="0" alt="">
								<xsl:attribute name="SRC">
								<xsl:value-of select="Bio/OnlineImageFilePath" disable-output-escaping="yes"/>
								</xsl:attribute>
								</IMG>
							</TD>
							</xsl:if>
							<xsl:if test="Bio/PersonBio != ''"> 
									<TD CLASS="data" valign="top" width="100%"><xsl:value-of select="Bio/PersonBio" disable-output-escaping="yes"/></TD>							
							</xsl:if>  	
							<xsl:if test="Bio/PersonBio = ''"> 
									<TD CLASS="data" valign="top">Information Not Available</TD>
					
							</xsl:if>  
							</TR> 
							</TABLE>
						</TD>							
					</TR>
					</xsl:otherwise>
				</xsl:choose>
				
				<xsl:choose>
				<xsl:when test="(LpvInfo/LpvString != '') and (LpvInfo/SelectedChars = 'CommitteeChartPage')">
					<TR CLASS="colorlight" ALIGN="center">
					<TD>
						<A title="Committee Chart">
							<xsl:attribute name="href">
							javascript:history.back()
							<!--/irweblinkx/committeechart.aspx?IID=<xsl:value-of select="Company/KeyInstn"/>-->
							</xsl:attribute>
							<span class="Default_Big"><strong>Committee Chart</strong>
						</span>
						</A>
					</TD>
					</TR>								
				</xsl:when>
				<xsl:when test="(LpvInfo/LpvString != '') and (LpvInfo/SelectedChars = 'FactSheetPage')">
					<TR CLASS="colorlight" ALIGN="center">
					<TD>
						<A title="Fact Sheet">
							<xsl:attribute name="href">
							javascript:history.back()
							<!--/irweblinkx/factsheet.aspx?IID=<xsl:value-of select="Company/KeyInstn"/>-->
							</xsl:attribute>
							<span class="Default_Big">
								<strong>Fact Sheet</strong>
											</span>
						</A>
					</TD>
					</TR>								
				</xsl:when>
				<xsl:otherwise>
					<TR CLASS="colorlight" ALIGN="center">
					<TD>
						<A title="All Officers and Directors">
							<xsl:attribute name="href">
							javascript:history.back()
							<!--od.aspx?IID=<xsl:value-of select="Company/KeyInstn"/>-->
							</xsl:attribute>
							<span class="Default_Big"><strong>All Officers and Directors</strong>
						</span>
						</A>
					</TD>
					</TR>										
				</xsl:otherwise>
				</xsl:choose>				
				</TABLE>
		    </TD>  
	    </TR>
	  </TABLE>
</xsl:template> 


<!-- Default Template Ends here. all templates below are ONLY if you are using the 'Style Template' section in the console. -->



















<!-- This is Template ONE option in the console under 'Style Template'
//This template is a striped down version of the main template. Everything (data) is displayed in single rows.
//The sections will be commented on where the rows are. you should be able to just move rows (entire chunks of data) where you need too. Edits to this section are rare, mostly due to size (width of the page).
-->
<xsl:template name="Bio_Title_2">
<xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
	<TR>
		<TD CLASS="colordark" NOWRAP="NOWRAP" align="left"><SPAN CLASS="title1light"><xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/></SPAN> 
	    </TD>
		<TD CLASS="colordark" NOWRAP="NOWRAP" align="right" valign="top"><span class="subtitlelight"><xsl:value-of select="$CompanyNameCaps"/> (<xsl:value-of select="Company/Exchange"/> - <xsl:value-of select="Company/Ticker"/>)</span><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></TD>
	</TR>
		<TR class="default" align="left">
			<TD class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Header" disable-output-escaping="yes"/></span></TD>
	</TR>
</xsl:template>


<xsl:template name="Bio_Data_2">
	<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="4" WIDTH="100%"> 
	<xsl:choose>
	<xsl:when test="Bio/BioCount &lt; 1">    
		<TR>
			<TD CLASS="data"><span class="defaultbold">Information not available.</span></TD>
		</TR>
	</xsl:when>
	<xsl:otherwise>
		<TR>
			<TD CLASS="data" valign="top"><span class="title2"><xsl:value-of select="Bio/Title" disable-output-escaping="yes"/></span></TD>
		</TR>
		<!--<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>-->					
		<TR>
			<TD valign="top" class="data"><!--<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>-->
				<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="4" WIDTH="100%">
				<!--<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>-->
				<TR>
					<TD CLASS="surr datashade" colspan="2">
						<strong><xsl:value-of select="Bio/PersonName" disable-output-escaping="yes"/>
										</strong>
									</TD>
				</TR>
				<!--<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>-->
				<TR>
				<xsl:if test="Bio/OnlineImageFilePath != ''"> 
				<TD valign="top">
					<IMG BORDER="0" alt="">
					<xsl:attribute name="SRC">
					<xsl:value-of select="Bio/OnlineImageFilePath" disable-output-escaping="yes"/>
					</xsl:attribute>
					</IMG>
				</TD>
				</xsl:if>
				<xsl:if test="Bio/PersonBio != ''"> 
					<TD valign="top" CLASS="data" width="100%"><xsl:value-of select="Bio/PersonBio" disable-output-escaping="yes"/></TD>	
				</xsl:if>  	
				<xsl:if test="Bio/PersonBio = ''"> 
						<TD CLASS="data">Information Not Available</TD>				
				</xsl:if>  
				</TR> 
				</TABLE>
			</TD>							
		</TR>
		</xsl:otherwise>    
	</xsl:choose>
	
	<xsl:choose>
	<xsl:when test="(LpvInfo/LpvString != '') and (LpvInfo/SelectedChars = 'CommitteeChartPage')">
		<TR CLASS="data" ALIGN="center">
		<TD>
			<A title="Committee Chart">
				<xsl:attribute name="href">
				javascript:history.back()
				<!--/irweblinkx/committeechart.aspx?IID=<xsl:value-of select="Company/KeyInstn"/>-->
				</xsl:attribute>
				<span class="Default_Big">
					<strong>Committee Chart</strong>
								</span>
			</A>
		</TD>
		</TR>								
	</xsl:when>
	<xsl:when test="(LpvInfo/LpvString != '') and (LpvInfo/SelectedChars = 'FactSheetPage')">
		<TR CLASS="data" ALIGN="center">
		<TD>
			<A title="Fact Sheet">
				<xsl:attribute name="href">
				javascript:history.back()
				<!--/irweblinkx/factsheet.aspx?IID=<xsl:value-of select="Company/KeyInstn"/>-->
				</xsl:attribute>
				<span class="Default_Big">
					<strong>Fact Sheet</strong>
								</span>
			</A>
		</TD>
		</TR>								
	</xsl:when>
	<xsl:otherwise>
		<TR CLASS="data" ALIGN="center">
		<TD>
			<A title="All Officers and Directors">
				<xsl:attribute name="href">
				javascript:history.back()
				<!--od.aspx?IID=<xsl:value-of select="Company/KeyInstn"/>-->
				</xsl:attribute>
				<span class="Default_Big">
					<strong>All Officers and Directors</strong>
								</span>
			</A>
		</TD>
		</TR>										
	</xsl:otherwise>
	</xsl:choose>				
	</TABLE>

</xsl:template> 

<!-- Template ONE Ends here. -->



<!-- Start Template THREE Name-Templates  -->

<xsl:template name="Bio_Title_3">
<xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
	<TR>
		<TD CLASS="colordark" NOWRAP="NOWRAP" align="left"><SPAN CLASS="title1light"><xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/></SPAN>
	    </TD>
		<TD CLASS="colordark" NOWRAP="NOWRAP" align="right" valign="top"><span class="subtitlelight"><xsl:value-of select="$CompanyNameCaps"/> (<xsl:value-of select="Company/Exchange"/> - <xsl:value-of select="Company/Ticker"/>)</span><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></TD>
	</TR>
		<TR class="default" align="left">
			<TD class="data" colspan="2"><span class="default"><xsl:value-of select="Company/Header" disable-output-escaping="yes"/></span></TD>
	</TR>
</xsl:template>


<xsl:template name="Bio_Data_3">
      <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%">
		<TR>
			<TD CLASS="data" valign="top">
				<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%">
				<xsl:choose>
				<xsl:when test="Bio/BioCount &lt; 1">    
					<TR>
						<TD CLASS="data"><span class="defaultbold">Information not available.</span></TD>
					</TR>
					<xsl:choose>
					<xsl:when test="(LpvInfo/LpvString != '') and (LpvInfo/SelectedChars = 'CommitteeChartPage')">
						<TR CLASS="Table1_item" ALIGN="left">
						
						<TD CLASS=" data" colspan="3">
							<A class="boldfielddef" title="Committee Chart">
								<xsl:attribute name="href">
								javascript:history.back()
								<!--/irweblinkx/committeechart.aspx?IID=<xsl:value-of select="Company/KeyInstn"/>-->
								</xsl:attribute><br/>
								<strong>Committee Chart</strong>
							</A>
						</TD>
						</TR>
					</xsl:when>
					<xsl:when test="(LpvInfo/LpvString != '') and (LpvInfo/SelectedChars = 'FactSheetPage')">
						<TR CLASS="Table1_item" ALIGN="left">
						<TD CLASS="Table1_item data">&#160;</TD>
						<TD CLASS="Table1_item data">
							<A class="boldfielddef" title="Fact Sheet">
								<xsl:attribute name="href">
								javascript:history.back()
								<!--/irweblinkx/factsheet.aspx?IID=<xsl:value-of select="Company/KeyInstn"/>-->
								</xsl:attribute><br/>
								<strong>Fact Sheet</strong>
							</A>
						</TD>
						</TR>
					</xsl:when>
					<xsl:otherwise>
						<TR CLASS="" ALIGN="left">
						
						<TD CLASS="data" colspan="2" align="center">
							<A class="boldfielddef" title="Return to Officers and Directors">
								<xsl:attribute name="href">
								javascript:history.back()
								<!--od.aspx?IID=<xsl:value-of select="Company/KeyInstn"/>-->
								</xsl:attribute><br/>
								<strong>Return to Officers and Directors</strong>
							</A>
						</TD>
						</TR>
					</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:otherwise>
					<TR>
						<xsl:if test="Bio/OnlineImageFilePath != ''">
							<TD valign="top" width="180">
								<IMG BORDER="1" alt="">
								<xsl:attribute name="SRC">
								<xsl:value-of select="Bio/OnlineImageFilePath" disable-output-escaping="yes"/>
								</xsl:attribute>
								</IMG><br />
								<span class="title2colordark"><xsl:value-of select="Bio/PersonName" disable-output-escaping="yes"/>
								<br />
								<xsl:value-of select="Bio/Title" disable-output-escaping="yes"/></span></TD>
						</xsl:if>
						<xsl:if test="Bio/OnlineImageFilePath = ''">
						<TD valign="top" width="180">
						<span class="defaultbold"><xsl:value-of select="Bio/PersonName" disable-output-escaping="yes"/>
						<br />
						<xsl:value-of select="Bio/Title" disable-output-escaping="yes"/></span></TD>
						</xsl:if>
						<TD valign="top" class="data">
							<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
								<TR>
									<xsl:if test="Bio/PersonBio != ''">
									<TD CLASS="data" width="100%" valign="top"><xsl:value-of select="Bio/PersonBio" disable-output-escaping="yes"/></TD>
								</xsl:if>
								<xsl:if test="Bio/PersonBio = ''">
									<TD CLASS="data">Information Not Available</TD>

								</xsl:if>
							</TR>
							<xsl:choose>
							<xsl:when test="(LpvInfo/LpvString != '') and (LpvInfo/SelectedChars = 'CommitteeChartPage')">
								<TR CLASS="Table1_item" ALIGN="left">
								
								<TD CLASS=" data" colspan="3">
									<A class="boldfielddef" title="Committee Chart">
										<xsl:attribute name="href">
										javascript:history.back()
										<!--/irweblinkx/committeechart.aspx?IID=<xsl:value-of select="Company/KeyInstn"/>-->
										</xsl:attribute><br/>
										<strong>Committee Chart</strong>
									</A>
								</TD>
								</TR>
							</xsl:when>
							<xsl:when test="(LpvInfo/LpvString != '') and (LpvInfo/SelectedChars = 'FactSheetPage')">
								<TR CLASS="Table1_item" ALIGN="left">
								<TD CLASS="Table1_item data">&#160;</TD>
								<TD CLASS="Table1_item data">
									<A class="boldfielddef" title="Fact Sheet">
										<xsl:attribute name="href">
										javascript:history.back()
										<!--/irweblinkx/factsheet.aspx?IID=<xsl:value-of select="Company/KeyInstn"/>-->
										</xsl:attribute><br/>
										<strong>Fact Sheet</strong>
									</A>
								</TD>
								</TR>
							</xsl:when>
							<xsl:otherwise>
								<TR CLASS="" ALIGN="left">
								
								<TD CLASS="data" colspan="2" align="center">
									<A class="boldfielddef" title="Return to Officers and Directors">
										<xsl:attribute name="href">
										javascript:history.back()
										<!--od.aspx?IID=<xsl:value-of select="Company/KeyInstn"/>-->
										</xsl:attribute><br/>
										<strong>Return to Officers and Directors</strong>
									</A>
								</TD>
								</TR>
							</xsl:otherwise>
							</xsl:choose>

							</TABLE>
						</TD>
					</TR>
					</xsl:otherwise>
				</xsl:choose>
				</TABLE>
		    </TD>
	    </TR>
	  </TABLE>
</xsl:template>


<!-- END Template THREE Name-Templates  -->


<!-- Main XSLT templates are here. these are the templates which are actually being displayed in the page. this is for TOP LEVEL editing. if you dont need to use the individual templates above you can do your main editing here when pulling THESE templates below you must carry the top comment with them and uncomment them when you drop it into the company specific xslt.
-->

<xsl:template match="irw:Bios">
<xsl:variable name="TemplateName" select="Company/TemplateName"/>
<xsl:choose>

<xsl:when test="$TemplateName = 'AltOd1'">
<!-- Copy from here down
<xsl:template match="irw:Bios">
-->
<xsl:call-template name="TemplateONEstylesheet" />
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%">
<xsl:call-template name="Title_S2"/>
<TR ALIGN="CENTER"> 
	<TD CLASS="leftTOPbord data" colspan="2">
		<xsl:call-template name="Bio_Data_2"/>
	</TD>
</TR>	    	
<xsl:if test="Company/Footer != ''">
<TR align="left">
	<TD class="data" colspan="2"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span></TD>
</TR>
</xsl:if>
</TABLE> 
<xsl:call-template name="Copyright"/>	

  <!-- 
</xsl:template> 
End Copy Here
-->

</xsl:when>


<!-- Start Template THREE Call-Templates -->
<xsl:when test="$TemplateName = 'AltOd3'">
<xsl:call-template name="TemplateTHREEstylesheet" />
<!-- Copy from here down
<xsl:template match="irw:Bios">
-->
<xsl:call-template name="Title_T3"/>
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%" CLASS="table2">
	<TR>
		<TD CLASS="data"><xsl:call-template name="Bio_Data_3"/></TD>
	</TR>
<xsl:if test="Company/Footer != ''">	
	<TR>
		<TD class="data" align="left"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span></TD>
	</TR>
</xsl:if>
</TABLE>
<xsl:call-template name="Copyright"/>
<!--
</xsl:template>
End Copy Here
-->
</xsl:when>
<!-- End Template THREE Call-Templates -->

<xsl:otherwise>
<!-- Copy from here down
<xsl:template match="irw:Bios">
-->
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" CLASS="colordark">
<xsl:call-template name="Title_S1"/>
  <TR ALIGN="CENTER"> 
    <TD CLASS="colordark default" colspan="2"><xsl:call-template name="Bio_Data"/></TD>
  </TR>
<xsl:if test="Company/Footer != ''">
	<TR>
		<TD class="colorlight default" align="left" colspan="2"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span></TD>
	</TR>
</xsl:if>
</TABLE> 
<xsl:call-template name="Copyright"/>	
<!-- 

</xsl:template> 

End Copy Here
-->
</xsl:otherwise>
</xsl:choose>
</xsl:template>
	
</xsl:stylesheet>