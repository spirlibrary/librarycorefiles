<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns="http://www.w3.ord/TR/REC-html40"
xmlns:irw="http://www.snl.com/xml/irw">

<!-- <xsl:output method="html" media-type="text/html" indent="no"/>
<xsl:output method="html"  doctype-system="http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd" doctype-public="-//W3C//Dtd XHTML 1.0 Transitional//EN"/> -->

<xsl:output method="html" doctype-public="-//W3C//Dtd XHTML 1.0 Transitional//EN" indent="no"/>

<!-- TEMPLATE ONE -->
<xsl:template name="InvCalc_Title">
<xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
	<TR>
		<TD CLASS="colordark" NOWRAP="true" border="0"><SPAN CLASS="title1light"><xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/></SPAN></TD>
		<TD CLASS="colordark" NOWRAP="true" align="right" valign="top" border="0"><span class="subtitlelight"><xsl:value-of select="$CompanyNameCaps"/> (<xsl:value-of select="Company/Exchange"/> - <xsl:value-of select="Company/Ticker"/>)</span><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></TD>
	</TR>

	<TR class="default">
		<TD class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Header" disable-output-escaping="yes"/></span></TD>
	</TR>
</xsl:template>

<xsl:template name="Invcalc_footer">
<TABLE  border="0" cellpadding="3" cellspacing="0" width="100%">
	<TR>
		<TD ALIGN="CENTER"><IMG SRC="/images/interactive/blank.gif" WIDTH="100%" HEIGHT="1" BORDER="0" alt=""/><BR/></TD>
	</TR>
	<TR class="colorlight">
		<TD colspan="2"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span></TD>
	</TR>
</TABLE>
</xsl:template>



<xsl:template name="InvCalc_data">
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" CLASS="data" WIDTH="100%">
	<TR>
		<TD CLASS="colorlight" COLSPAN="3"><SPAN CLASS="defaultbold">For a split adjusted share price, enter the original stock price and date of purchase:</SPAN></TD>
	</TR>
	<TR>
		<TD CLASS="data">
			<TABLE BORDER="0" CELLPADDING="3" CELLSPACING="0" WIDTH="100%" CLASS="data">
				<TR>
					<TD CLASS="header">Date of Purchase:</TD>
					<TD CLASS="header">Price at Purchase: *</TD>
					<TD CLASS="header"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></TD>
				</TR>
				<TR>
					<TD CLASS="data" VALIGN="top">
						<label class="visuallyhidden" for="Date">Date</label><INPUT type="text" id="Date" name="Date" class="irwDatePicker"/></TD>
					<TD CLASS="data" VALIGN="top">
						<label class="visuallyhidden" for="Price">Price</label><INPUT type="text" id="Price" name="Price" /></TD>
					<TD CLASS="data"><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;</xsl:text><input type="submit" name="Submit" value="Submit"/></TD>
				</TR>
				<TR>
					<TD CLASS="data" COLSPAN="3"><SPAN CLASS="default"><BR/>* Optional.  If price at purchase is left blank, basis calculation will be made based on purchase price of $1.<BR/><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></SPAN></TD>
				</TR>

				<xsl:variable name="Basis2Compare" select="dtInvCalc/Basis"/>
				<xsl:variable name="Basis2CompareTo" select="''"/>
				<xsl:if test="string-length(normalize-space($Basis2Compare)) > 0">
					<tr>
						<td colspan="3">
							<table border="0" cellpadding="0" cellspacing="0" width="30%">
								<TR>
									<TD CLASS="colorlight" COLSPAN="2"><SPAN CLASS="large">Basis:</SPAN></TD>
									<TD CLASS="colorlight"><SPAN CLASS="large"><xsl:value-of select="dtInvCalc/Basis"/></SPAN></TD>
								</TR>
								<TR>
									<TD CLASS="colorlight" COLSPAN="2"><SPAN CLASS="large">Current price:</SPAN></TD>
									<TD CLASS="colorlight"><SPAN CLASS="large"><xsl:value-of select="dtInvCalc/CurrentPrice"/></SPAN></TD>
								</TR>
							</table>
						</td>
					</tr>              
					<TR>
						<TD CLASS="colorlight" COLSPAN="3"><SPAN CLASS="defaultbold">As of: <xsl:value-of select="dtInvCalc/CurrentDate"/><xsl:if test="contains(dtInvCalc/CurrentDate, ':')"><xsl:choose><xsl:when test="contains(dtInvCalc/CurrentDate, 'EST')"></xsl:when><xsl:otherwise> EST</xsl:otherwise></xsl:choose></xsl:if></SPAN></TD>
					</TR>
					<xsl:variable name="PriceAppreciation" select="translate(dtInvCalc/PriceAppreciation, ',', '')"/>
					<xsl:variable name="PriceAppreciationNegative" select="translate(dtInvCalc/PriceAppreciation, '-', '')"/>
					<TR>
						<TD CLASS="data" VALIGN="bottom" COLSPAN="2" nowrap="1"><BR/>Price appreciation (depreciation) over period:</TD>
						<TD CLASS="colorlight" VALIGN="bottom">
						<SPAN CLASS="large">
						<xsl:choose>
							<xsl:when test="$PriceAppreciation >= 0">
								<xsl:value-of select="dtInvCalc/PriceAppreciation"/>%
							</xsl:when>
							<xsl:otherwise>
								(<xsl:value-of select="$PriceAppreciationNegative"/>%)
							</xsl:otherwise>
						</xsl:choose>
						</SPAN>
						</TD>
					</TR>
				</xsl:if>
				<tr>
					<td colspan="3" height="30"></td>
				</tr>	
				<TR>
					<TD CLASS="data" colspan="3"><SPAN CLASS="defaultbold">Consult Your Tax Advisor:&#160;</SPAN>This information does not constitute tax advice. It does not purport to be complete or to describe the consequences that may apply to particular categories of shareholders. You should consult a tax advisor regarding the calculation of your tax basis.</TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
</TABLE>
</xsl:template>
<!-- END TEMPLATE ONE -->






<!-- START TEMPLATE TWO -->
<xsl:template name="InvCalc_Title2">
<xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
	<TR>
		<TD CLASS="title2colordark titletest" nowrap="" colspan="2"><xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/><br /><IMG SRC="" WIDTH="2" HEIGHT="1" alt=""/><span class="title2colordark titletest2"><xsl:value-of select="$CompanyNameCaps"/>&#160;(<xsl:value-of select="Company/Exchange"/>&#160;-&#160;<xsl:value-of select="Company/Ticker"/>)&#160;</span></TD>
	</TR>
	<TR class="default">
		<TD class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Header" disable-output-escaping="yes"/></span></TD>
	</TR>
</xsl:template>

<xsl:template name="InvCalc_data2">
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" CLASS="data" WIDTH="100%">
	<TR>
		<TD CLASS="colorlight" COLSPAN="3"><SPAN CLASS="defaultbold">For a split adjusted share price, enter the original stock price and date of purchase:</SPAN></TD>
	</TR>
	<TR>
		<TD CLASS="data">
			<TABLE BORDER="0" CELLPADDING="3" CELLSPACING="0" WIDTH="100%" CLASS="data">
				<TR>
					<TD CLASS="surrleft datashade"><span class="titletest2">Date of Purchase:</span></TD>
					<TD CLASS="surr_topbot datashade"><span class="titletest2">Price at Purchase: *</span></TD>
					<TD CLASS="surrright datashade"><span class="titletest2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></span></TD>
				</TR>
				<TR>
					<TD CLASS="data">
						<label class="visuallyhidden" for="Date">Date</label><INPUT type="text" id="Date" name="Date" class="input datashade" /></TD>
					<TD CLASS="data" VALIGN="top">
						<label class="visuallyhidden" for="Price">Price</label><INPUT type="text" id="Price" name="Price" class="input datashade" /></TD>
					<TD CLASS="data"><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;</xsl:text><input type="submit" name="Submit" value="Submit" CLASS="SUBMIT DATASHADE" /></TD>
				</TR>
				<TR>
					<TD CLASS="data" COLSPAN="3"><SPAN CLASS="default"><BR/>* Optional.  If price at purchase is left blank, basis calculation will be made based on purchase price of $1.<BR/><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></SPAN></TD>
				</TR>
				<TR>
					<td colspan="3" >
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<xsl:variable name="Basis2Compare" select="dtInvCalc/Basis"/>
						<xsl:variable name="Basis2CompareTo" select="''"/>
						<xsl:if test="string-length(normalize-space($Basis2Compare)) > 0">
							<tr>
								<td colspan="3">
									<table border="0" cellpadding="0" cellspacing="0" width="30%">
										<TR>
							      				<TD CLASS="" COLSPAN="2"><SPAN CLASS="large">Basis:</SPAN></TD>
							      				<TD align="right"><SPAN CLASS="large"><xsl:value-of select="dtInvCalc/Basis"/></SPAN></TD>
									    	</TR>
							    			<TR>
							    	  			<TD CLASS="" COLSPAN="2"><SPAN CLASS="large">Current price:</SPAN></TD>
							      				<TD align="right"><SPAN CLASS="large"><xsl:value-of select="dtInvCalc/CurrentPrice"/></SPAN></TD>
								    		</TR>
									</table>
								</td>
							</tr>
							<xsl:variable name="PriceAppreciation" select="translate(dtInvCalc/PriceAppreciation, ',', '')"/>
							<xsl:variable name="PriceAppreciationNegative" select="translate(dtInvCalc/PriceAppreciation, '-', '')"/>
							<TR>
								<td colspan="3" class="surr white">
									<table border="0" width="100%" cellpadding="2" cellspacing="0">
										<tr>
											<TD CLASS="white" VALIGN="bottom" COLSPAN="2"><SPAN CLASS="defaultbold">As of: <xsl:value-of select="dtInvCalc/CurrentDate"/><xsl:if test="contains(dtInvCalc/CurrentDate, ':')"><xsl:choose><xsl:when test="contains(dtInvCalc/CurrentDate, 'EST')"></xsl:when><xsl:otherwise> EST</xsl:otherwise></xsl:choose></xsl:if></SPAN><br /><SPAN CLASS="data" style="white-space: no-wrap;">Price appreciation (depreciation) over period:</SPAN></TD>
											<TD CLASS="white" VALIGN="bottom" align="right">
												<SPAN CLASS="large">
												<xsl:choose>
													<xsl:when test="$PriceAppreciation >= 0">
														<xsl:value-of select="dtInvCalc/PriceAppreciation"/>%
													</xsl:when>
													<xsl:otherwise>
														(<xsl:value-of select="$PriceAppreciationNegative"/>%)
													</xsl:otherwise>
												</xsl:choose>
												</SPAN>
											</TD>
										</tr>
									</table>
								</td>
							</TR>
						</xsl:if>
					</table>
				</td>
			</TR>
			<tr>
				<td colspan="3" height="30"></td>
			</tr>			
			<TR>
				<TD CLASS="data" colspan="3" class="surr">
					<table cellpadding="2" cellspacing="0" width="100%">
						<tr>
							<td class="surr datashade data"><SPAN CLASS="defaultbold">Consult Your Tax Advisor: </SPAN>This information does not constitute tax advice. It does not purport to be complete or to describe the consequences that may apply to particular categories of shareholders. You should consult a tax advisor regarding the calculation of your tax basis.</td>
						</tr>
					</table>
				</TD>
			</TR>
		</TABLE>
	</TD>
	</TR>
</TABLE>

</xsl:template>

<!-- END TEMPLATE TWO -->



<!-- START TEMPLATE 3 -->
<xsl:template name="InvCalc_data3">
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" CLASS="data" WIDTH="100%">
	<TR>
		<TD CLASS="colorlight" COLSPAN="3"><SPAN CLASS="defaultbold">For a split adjusted share price, enter the original stock price and date of purchase:</SPAN></TD>
	</TR>
	<TR>
		<TD CLASS="data">
			<TABLE BORDER="0" CELLPADDING="3" CELLSPACING="0" WIDTH="100%" CLASS="data table2x">
				<TR>
					<TD CLASS="surrleft header" style="text-align:left">Date of Purchase:</TD>
					<TD CLASS="surr_topbot header" colspan="2">Price at Purchase: *</TD>
				</TR>
				<TR>
					<TD CLASS="data" VALIGN="top">
						<label class="visuallyhidden" for="Date">Date</label><INPUT type="text" id="Date" name="Date" CLASS="input irwDatePicker"  />
				</TD>
					<TD CLASS="data" VALIGN="top">
						<label class="visuallyhidden" for="Price">Price</label><INPUT type="text" id="Price" name="Price" CLASS="input"  /></TD>
					<TD CLASS="data"><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;</xsl:text><input type="submit" name="Submit" value="Submit" /></TD>
				</TR>
				<TR>
					<TD CLASS="data" COLSPAN="3"><SPAN CLASS="default"><BR/>* Optional.  If price at purchase is left blank, basis calculation will be made based on purchase price of $1.<BR/><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></SPAN></TD>
				</TR>
				<TR>
					<td colspan="3" >
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<xsl:variable name="Basis2Compare" select="dtInvCalc/Basis"/>
						<xsl:variable name="Basis2CompareTo" select="''"/>
						<xsl:if test="string-length(normalize-space($Basis2Compare)) > 0">
							<tr>
								<td colspan="3">
									<table border="0" cellpadding="0" cellspacing="0" width="30%">
										<TR>
										  	<TD CLASS="" COLSPAN="2"><SPAN CLASS="large">Basis:</SPAN></TD>
											<TD align="right"><SPAN CLASS="large"><xsl:value-of select="dtInvCalc/Basis"/></SPAN></TD>
										</TR>

										<TR>
											<TD CLASS="" COLSPAN="2"><SPAN CLASS="large">Current price:</SPAN></TD>
											<TD align="right"><SPAN CLASS="large"><xsl:value-of select="dtInvCalc/CurrentPrice"/></SPAN></TD>
										</TR>
									</table>
								</td>
							</tr>                    
							<xsl:variable name="PriceAppreciation" select="translate(dtInvCalc/PriceAppreciation, ',', '')"/>
							<xsl:variable name="PriceAppreciationNegative" select="translate(dtInvCalc/PriceAppreciation, '-', '')"/>
							<TR>
								<td colspan="3" class="surr white">
									<table border="0" width="100%" cellpadding="2" cellspacing="0">
										<tr>
											<TD CLASS="white" VALIGN="bottom" COLSPAN="2"><SPAN CLASS="defaultbold">As of: <xsl:value-of select="dtInvCalc/CurrentDate"/><xsl:if test="contains(dtInvCalc/CurrentDate, ':')"><xsl:choose><xsl:when test="contains(dtInvCalc/CurrentDate, 'EST')"></xsl:when><xsl:otherwise> EST</xsl:otherwise></xsl:choose></xsl:if></SPAN><br /><SPAN CLASS="large" style="white-space:no-wrap;">Price appreciation (depreciation) over period:</SPAN></TD>
											<TD CLASS="white" VALIGN="bottom" align="right"><SPAN CLASS="large"><xsl:choose><xsl:when test="$PriceAppreciation>=0"><xsl:value-of select="dtInvCalc/PriceAppreciation"/>%</xsl:when><xsl:otherwise>(<xsl:value-of select="$PriceAppreciationNegative"/>%)</xsl:otherwise></xsl:choose></SPAN></TD>
										</tr>
									</table>
								</td>
							</TR>
						</xsl:if>
					</table>
				</td>
			</TR>
			<tr>
				<td colspan="3" height="30"></td>
			</tr>
			<TR>
				<TD colspan="3" class="data datashade">
					<table cellpadding="2" cellspacing="0" width="100%">
						<tr>
							<TD CLASS="datashade data" colspan="3"><SPAN CLASS="defaultbold">Consult Your Tax Advisor:&#160;</SPAN>This information does not constitute tax advice. It does not purport to be complete or to describe the consequences that may apply to particular categories of shareholders. You should consult a tax advisor regarding the calculation of your tax basis.</TD>
						</tr>
					</table>
				</TD>
			</TR>
		</TABLE>
	</TD>
</TR>
</TABLE>

</xsl:template>
<!-- END TEMPLATE 3 -->


<xsl:template match="irw:Invcalc">
<xsl:variable name="TemplateName" select="Company/TemplateName"/>
<link rel="stylesheet" href="javascript/calendar/ui.datepicker.css" type="text/css"/>
<script src="javascript/jquery-1.3.1.min.js"></script>
<script type="text/javascript" src="javascript/ui.datepicker.js"></script>
<script type="text/javascript" src="javascript/datepicker.js"></script>
<xsl:choose>
<!-- Template 1 -->
<xsl:when test="$TemplateName = 'AltInvCalc1'">
<xsl:call-template name="TemplateONEstylesheet" />
<xsl:variable name="KeyInstn" select="Company/KeyInstn"/>
<TABLE BORDER="0" CELLPADDING="3" CELLSPACING="0" WIDTH="100%">
<xsl:variable name="FormAction" select="concat('invcalc.aspx?IID=', $KeyInstn)"></xsl:variable>
	<xsl:call-template name="Title_S2"/>
	<TR>
		<TD CLASS="leftTOPbord" COLSPAN="3">
      <FORM METHOD="post">
        <xsl:attribute name="action"><xsl:value-of select="$FormAction"/></xsl:attribute>
        <input type="hidden" name="xml" value="1"/>
        <xsl:call-template name="InvCalc_data2"/>
      </FORM>
  </TD>
	</TR>
</TABLE>
<xsl:call-template name="Invcalc_footer"/>
<xsl:call-template name="Copyright"/>
</xsl:when>
<!-- End Template 1 -->


<!-- Template 3 -->
<xsl:when test="$TemplateName = 'AltInvCalc3'">
<xsl:call-template name="TemplateTHREEstylesheet" />
<xsl:variable name="KeyInstn" select="Company/KeyInstn"/>
<xsl:call-template name="Title_T3"/>
<TABLE BORDER="0" CELLPADDING="3" CELLSPACING="0" WIDTH="100%" class="table2">
<xsl:variable name="FormAction" select="concat('invcalc.aspx?IID=', $KeyInstn)"></xsl:variable>
	<TR>
		<TD CLASS="" COLSPAN="3">
      <FORM METHOD="post">
        <xsl:attribute name="action"><xsl:value-of select="$FormAction"/></xsl:attribute>
        <input type="hidden" name="xml" value="1"/>
        <xsl:call-template name="InvCalc_data3"/>
      </FORM>
    </TD>
	</TR>
</TABLE>
<xsl:call-template name="Invcalc_footer"/>
<xsl:call-template name="Copyright"/>
</xsl:when>
<!-- End Template 3 -->



<!-- Template Default -->
<xsl:otherwise>
<xsl:variable name="KeyInstn" select="Company/KeyInstn"/>

<TABLE BORDER="0" CELLPADDING="3" CELLSPACING="0" WIDTH="100%">
<xsl:variable name="FormAction" select="concat('invcalc.aspx?IID=', $KeyInstn)"></xsl:variable>
	<xsl:call-template name="Title_S1"/>
	<TR>
		<TD CLASS="colorlight" COLSPAN="3">
      <FORM METHOD="post">
        <xsl:attribute name="action"><xsl:value-of select="$FormAction"/></xsl:attribute>
        <input type="hidden" name="xml" value="1"/>
			<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" CLASS="data" WIDTH="100%">
				<TR>
					<TD CLASS="colorlight" COLSPAN="3"><SPAN CLASS="defaultbold">For a split adjusted share price, enter the original stock price and date of purchase:</SPAN></TD>
				</TR>
				<TR>
					<TD CLASS="data">
						<TABLE BORDER="0" CELLPADDING="3" CELLSPACING="0" WIDTH="100%" CLASS="data">
							<TR>
								<TD CLASS="header">Date of Purchase:</TD>
								<TD CLASS="header">Price at Purchase: *</TD>
								<TD CLASS="header"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></TD>
							</TR>
							<TR>
								<TD CLASS="data" VALIGN="top">
									<label class="visuallyhidden" for="Date">Date</label><INPUT type="text" id="Date" name="Date" class="irwDatePicker"/></TD>
								<TD CLASS="data" VALIGN="top">
									<label class="visuallyhidden" for="Price">Price</label><INPUT type="text" id="Price" name="Price" /></TD>
								<TD CLASS="data"><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;</xsl:text><input type="submit" name="Submit" value="Submit"/></TD>
							</TR>
						</TABLE>	
						<TABLE BORDER="0" CELLPADDING="3" CELLSPACING="0" WIDTH="100%" CLASS="data">
							<TR>
								<TD CLASS="data" COLSPAN="3"><SPAN CLASS="default"><BR/>* Optional.  If price at purchase is left blank, basis calculation will be made based on purchase price of $1.<BR/><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></SPAN></TD>
							</TR>
							<xsl:variable name="Basis2Compare" select="dtInvCalc/Basis"/>
							<xsl:variable name="Basis2CompareTo" select="''"/>
							<xsl:if test="$Basis2Compare != $Basis2CompareTo">
							<tr>
								<td colspan="3">
									<table border="0" cellpadding="0" cellspacing="0" width="30%">
										<TR>
											<TD CLASS="colorlight" COLSPAN="2"><SPAN CLASS="large">Basis:</SPAN></TD>
											<TD CLASS="colorlight"><SPAN CLASS="large"><xsl:value-of select="dtInvCalc/Basis"/></SPAN></TD>
										</TR>
										<TR>
											<TD CLASS="colorlight" COLSPAN="2"><SPAN CLASS="large">Current price:</SPAN>										</TD>
											<TD CLASS="colorlight"><SPAN CLASS="large"><xsl:value-of select="dtInvCalc/CurrentPrice"/>										</SPAN></TD>
										</TR>
									</table>
								</td>
							</tr>
							<TR>
								<TD CLASS="colorlight" COLSPAN="3"><SPAN CLASS="defaultbold">As of: <xsl:value-of select="dtInvCalc/CurrentDate"/><xsl:if test="contains(dtInvCalc/CurrentDate, ':')"><xsl:choose><xsl:when test="contains(dtInvCalc/CurrentDate, 'EST')"></xsl:when><xsl:otherwise> EST</xsl:otherwise></xsl:choose></xsl:if></SPAN></TD>
							</TR>

							<xsl:variable name="PriceAppreciation" select="translate(dtInvCalc/PriceAppreciation, ',', '')"/>
							<xsl:variable name="PriceAppreciationNegative" select="translate(dtInvCalc/PriceAppreciation, '-', '')"/>
							
							<TR>
								<TD CLASS="data" VALIGN="bottom" COLSPAN="2" nowrap="1"><BR/>Price appreciation (depreciation) over period:</TD>
								<TD CLASS="colorlight" VALIGN="bottom">
									<SPAN CLASS="large">
									<xsl:choose>
										<xsl:when test="$PriceAppreciation>=0">
											<xsl:value-of select="dtInvCalc/PriceAppreciation"/>%
										</xsl:when>
										<xsl:otherwise>
											(<xsl:value-of select="$PriceAppreciationNegative"/>%)
										</xsl:otherwise>
									</xsl:choose>
									</SPAN>
								</TD>
							</TR>
						</xsl:if>
						<tr>
							<td colspan="3" height="30"></td>
						</tr>
							<TR>
								<TD CLASS="data" colspan="3"><SPAN CLASS="defaultbold">Consult Your Tax Advisor:&#160;</SPAN>This information does not constitute tax advice. It does not purport to be complete or to describe the consequences that may apply to particular categories of shareholders. You should consult a tax advisor regarding the calculation of your tax basis.</TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
      </FORM>
		</TD>
	</TR>
</TABLE>
<xsl:call-template name="Invcalc_footer"/>
<xsl:call-template name="Copyright"/>
</xsl:otherwise>
</xsl:choose>
</xsl:template>
</xsl:stylesheet>
