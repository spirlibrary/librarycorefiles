<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.ord/TR/REC-html40"
	xmlns:irw="http://www.snl.com/xml/irw">

  <xsl:output method="html" media-type="text/html" indent="no"/>

  <xsl:template name="ARChart_Header">
    <xsl:if test="Company/Header != ''">
      <tr>
        <td valign="top">
          <span class="default">
            <xsl:value-of select="Company/Header" disable-output-escaping="yes"/>
          </span>
        </td>
      </tr>
    </xsl:if>
  </xsl:template>
  <xsl:template name="ARChart_Footer">
    <tr>
      <td valign="top">
        <span class="default">
          <xsl:value-of select="Company/Footer" disable-output-escaping="yes"/>
        </span>
      </td>
    </tr>
    <xsl:if test="Company/Footer != ''">
    </xsl:if>
  </xsl:template>

  <xsl:template name="ARChart_Resource">
    <style type="text/css">
        #ARCIFrame { min-height:350px; }
      </style>
    <xsl:comment><![CDATA[[if lte IE 7.0]><style type="text/css">#ARCIFrame{ min-height: 350px; height:expression(this.currentStyle.getAttribute('minHeight'));}</style><![endif]]]></xsl:comment>
    <script type="text/javascript" src="javascript/GraphNew/jquery.min.js"></script>
    <script type="text/javascript" src="javascript/GraphNew/jquery.blockUI.js"></script>
    <script language="JavaScript">
      <![CDATA[      
    
   var jQueryChart = jQuery.noConflict();    
   // document.body.style.cursor = 'progress';
   //jQueryChart.blockUI({ message: "<img src='Images/GraphNew/ajax-loader.gif'>", baseZ: 999999, fadeIn: 0, fadeOut: 100, showOverlay: true });
   
    jQueryChart(document).ready(function() {    
    jQueryChart('div#loader').block({ message: "<img src='Images/GraphNew/ajax-loader.gif'>", baseZ: 999999, fadeIn: 0, fadeOut: 100, showOverlay: true });
    }); 
   
     function CloseBlockUI()
     {
        jQueryChart.unblockUI();
        jQueryChart(".blockUI").hide();
     }
    
		  ]]>
    </script>
  </xsl:template>
  <xsl:template match="irw:ARChart">
    <xsl:variable name="KeyInstn" select="Company/KeyInstn" />
    <xsl:variable name="KeyPage" select="Company/KeyPage" />
    <xsl:variable name="Preview" select="Company/IsPreview" />
    <xsl:variable name="Height" select="(Params/Height + 500)" />
    <xsl:variable name="KeyTable" select="(Params/KeyTable)" />
    <xsl:variable name="Color" select="(Params/Color)" />
    <xsl:variable name="KeyItem" select="(Params/KeyItem)" />     
    <xsl:variable name="Periods" select="(Params/Periods)" />
    <xsl:call-template name="TemplateTHREEstylesheet" />
    <xsl:call-template name="ARChart_Resource"/>
    <xsl:call-template name="Title_T3"/>
    <table cellpadding="3" cellspacing="0" border="0" width="100%">
      <tr>
        <td valign="top">
          <xsl:choose>
            <xsl:when test="Params">
              <div id="loader">
                <iframe width="100%" onload="CloseBlockUI();" scrolling="no" frameborder="0" name="ARCIFrame" id="ARCIFrame" allowtransparency="true" >
                  <xsl:choose >
                    <xsl:when test="contains(Company/URL, 'print=1') or contains(Company/URL, 'Print=1')">
                      <xsl:attribute name="style">
                            min-height:<xsl:value-of select="$Height"/>px;
                          </xsl:attribute>
                      <xsl:attribute name="src">
                          AsReportedChartIFrame.aspx?pdf=1&amp;KeyInstn=<xsl:value-of select="$KeyInstn"/>&amp;KeyPage=<xsl:value-of select="$KeyPage"/>&amp;Preview=<xsl:value-of select="$Preview"/>&amp;KeyTable=<xsl:value-of select="$KeyTable"/>&amp;KeyItemCT=<xsl:value-of select="$KeyItem"/>&amp;PeriodsCT=<xsl:value-of select="$Periods"/>&amp;ColorCT=<xsl:value-of select="$Color"/>
                          </xsl:attribute>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:attribute name="src">
                          AsReportedChartIFrame.aspx?KeyInstn=<xsl:value-of select="$KeyInstn"/>&amp;KeyPage=<xsl:value-of select="$KeyPage"/>&amp;Preview=<xsl:value-of select="$Preview"/>&amp;KeyTable=<xsl:value-of select="$KeyTable"/>&amp;KeyItemCT=<xsl:value-of select="$KeyItem"/>&amp;PeriodsCT=<xsl:value-of select="$Periods"/>&amp;ColorCT=<xsl:value-of select="$Color"/>
                          </xsl:attribute>
                    </xsl:otherwise>
                  </xsl:choose>
                </iframe>
              </div>
            </xsl:when>
            <xsl:otherwise>
              <table cellpadding="5" cellspacing="3" border="0" width="100%">
                <tr>
                  <td>
                          There is currently no financial information available for the periods selected.
                        </td>
                </tr>
              </table>
            </xsl:otherwise>
          </xsl:choose>
        </td>
      </tr>
      <xsl:call-template name="ARChart_Footer"/>
      <tr>
        <td valign="top">
          <xsl:call-template name="Copyright"/>
        </td>
      </tr>
    </table>
  </xsl:template>
</xsl:stylesheet>
