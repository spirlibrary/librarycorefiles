<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.ord/TR/REC-html40" xmlns:irw="http://www.snl.com/xml/irw">
<xsl:import href="Copyright.xslt" />

<xsl:output method="html" media-type="text/html" />

<!--
SP : 75025, JR [ Date : 2 July, 2009 ]
===========================================
Fixed below mentioned Bugs :
1) The top border is missing in Template-3
2) Page width problem in narrow skin. ( All Templates )
3) Reduced the width of "Institution (Headquarters)" Column from 60% to 40%
4) The Colspan of every Table header was 6 , instead of "9" , which is now fixed because every table has 9 columns.
-->
<!-- Start Default Template here-->

<!--
//This is the default template. Most changes are to either remove the company name, ticker, or exchange. you can also change the name of the page or remove this row all together. 
//These templates below are broken up how the page is displayed.
 
-->


<xsl:template name="DepositMarketShare_Title">
<xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')" />
	<TR>
		<TD class="colordark"><SPAN class="title1light"><xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/></SPAN></TD>
				<TD class="colordark" align="right" valign="top">
					<span class="subtitlelight"><xsl:value-of select="$CompanyNameCaps" /> (<xsl:value-of select="Company/Exchange" /> - <xsl:value-of select="Company/Ticker" />)</span>
					<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
				</TD>
			</TR>
			<TR class="default" align="left">
				<TD class="colorlight" colspan="2">
					<span class="default">
						<xsl:value-of select="Company/Header" disable-output-escaping="yes" />
					</span>
				</TD>
			</TR>
			
</xsl:template>

			<xsl:template name="DepositMarketShare_Data">
			<xsl:variable name="Year1" select="Years/Year1" />
			<xsl:variable name="Year2" select="Years/Year2" />
			<xsl:variable name="MaxResultsPerMarket" select="10"/>
			<table border="0" cellspacing="0" cellpadding="2" width="100%">
					<tr>
						<td class="colorlight">
							<span class="title2">Deposit Market	Share for Selected Markets</span>
							<br />
							<span class="default">
							<xsl:if test="Company/OwnershipSetting = 'C'">Current</xsl:if>
							<xsl:if test="Company/OwnershipSetting != 'C'">Pending</xsl:if>
							Ownership, including Bank, Savings Bank, Thrift Branches<br />
							Ownership as of	
							<xsl:value-of select="Company/PriceDate"/>,
							Branches and Deposits as of	
							<xsl:value-of select="Company/DepositsAsOf"/>
							</span>
							<br />
						</td>
					</tr>
					<xsl:for-each select="Markets">
					<xsl:if test="string-length(Name) = 0"></xsl:if>
					<xsl:if test="string-length(Name) != 0">
					<tr>
						<td class="colorlight">
							<table cellspacing="1" border="0" cellpadding="4" width="100%" class="header header_trans">
								<tr valign="top">
									<td class="header" valign="middle" colspan="9">
										<xsl:value-of select="Name"/>
									</td>
								</tr>
								<tr class="datashade">
									<td class="datashade" align="right" valign="bottom">
										<span class="defaultbold"><xsl:value-of select="$Year1" /> Rank</span>
									</td>
									<td class="datashade" align="right" valign="bottom">
										<span class="defaultbold"><xsl:value-of select="$Year2" /> Rank</span>
									</td>
									<td class="datashade" align="left" valign="bottom" width="40%">
										<span class="defaultbold">Institution (Headquarters)</span>
									</td>
									<td class="datashade" align="left" valign="bottom">
										<span class="defaultbold">Type</span>
									</td>
									<td class="datashade" align="right" valign="bottom">
										<span class="defaultbold"><xsl:value-of select="$Year1" /><br />Number of Branches</span>
									</td>
									<td class="datashade" align="right" valign="bottom">
										<span class="defaultbold"><xsl:value-of select="$Year1" /> Total Deposits in Market ($M)</span>
									</td>
									<td class="datashade" align="right" valign="bottom">
										<span class="defaultbold"><xsl:value-of select="$Year1" /> Total Market Share (%)</span>
									</td>
									<td class="datashade" align="right" valign="bottom">
										<span class="defaultbold"><xsl:value-of select="$Year2" /> Total Deposits in Market ($M)</span>
									</td>
									<td class="datashade" align="right" valign="bottom">
										<span class="defaultbold"><xsl:value-of select="$Year2" /> Total Market Share (%)</span>
									</td>
								</tr>
								<xsl:for-each select="MarketData">
								<xsl:if test="Rank &lt; $MaxResultsPerMarket + 1 or ../../Company/KeyInstn = CurrentKeyInstn">
								<xsl:if test="../../Company/KeyInstn != CurrentKeyInstn">
								<tr class="data">
									<td class="data" align="right" valign="top">
										<xsl:value-of select="Rank"/>
									</td>
									<td class="data" align="right" valign="top">
										<xsl:value-of select="YearAgoRank"/>
									</td>
									<td class="data" align="left" valign="top" width="40%">
										<xsl:value-of select="Name" disable-output-escaping="yes"/>
									</td>
									<td class="data" align="left" valign="top">
										<xsl:value-of select="InstitutionType"/>
									</td>
									<td class="data" align="right" valign="top">
										<xsl:value-of select="Branches"/>
									</td>
									<td class="data" align="right" valign="top">
										<xsl:value-of select="format-number(Deposits div 1000, '#,##0.0')"/>
									</td>
									<td class="data" align="right" valign="top">
										<xsl:value-of select="format-number(CurrentMarketShare, '##0.00')"/>
									</td>
									<td class="data" align="right" valign="top">
										<xsl:value-of select="format-number(YearAgoDeposits div 1000, '#,##0.0')"/>
									</td>
									<td class="data" align="right" valign="top">
										<xsl:value-of select="format-number(YearAgoMarketShare, '##0.00')"/>
									</td>
								</tr>
								</xsl:if>
								<xsl:if test="../../Company/KeyInstn = CurrentKeyInstn">
								<tr class="data">
									<td class="data" align="right" valign="top">
										<span class="defaultbold">
										<xsl:value-of select="Rank"/>
										</span> 
									</td>
									<td class="data" align="right" valign="top">
										<span class="defaultbold"><xsl:value-of select="YearAgoRank"/></span> 
									</td>
									<td class="data" align="left" valign="top" width="40%">
										<span class="defaultbold">
										<xsl:value-of select="Name" disable-output-escaping="yes"/>
										</span>
									</td>
									<td class="data" align="left" valign="top">
										<span class="defaultbold">
										<xsl:value-of select="InstitutionType"/>
										</span>
									</td>
									<td class="data" align="right" valign="top">
										<span class="defaultbold">
										<xsl:value-of select="Branches"/>
										</span>
									</td>
									<td class="data" align="right" valign="top">
										<span class="defaultbold">
										<xsl:value-of select="format-number(Deposits div 1000, '#,##0.0')"/>
										</span>
									</td>
									<td class="data" align="right" valign="top">
										<span class="defaultbold">
										<xsl:value-of select="format-number(CurrentMarketShare, '##0.00')"/>
										</span>
									</td>
									<td class="colorlight" align="right" valign="top">
										<span class="defaultbold">
										<xsl:value-of select="format-number(YearAgoDeposits div 1000, '#,##0.0')"/>
										</span>
									</td>
									<td class="colorlight" align="right" valign="top">
										<span class="defaultbold">
										<xsl:value-of select="format-number(YearAgoMarketShare, '##0.00')"/>
										</span>
									</td>
								</tr>
								</xsl:if>
								</xsl:if>
								</xsl:for-each>  
				        <tr class="datashade">
					        <td class="datashade" rowspan="2" valign="top"><span class="defaultbold"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></span></td>
					        <td class="datashade" rowspan="2" valign="top"><span class="defaultbold">&#160;</span></td>
					        <td class="datashade" valign="top"><span class="defaultbold">Total for <!--xsl:value-of select="TotalInstitutions"/-->institutions in market<br/><br/></span></td>
					        <td class="datashade" align="right" valign="top"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					        <td class="datashade" align="right" valign="top">
						        <span class="defaultbold">
							        <span class="defaultbold"><xsl:value-of select="TotalBranches"/></span>
						        </span>
					        </td>
					        <td class="datashade" align="right" valign="top">
						        <span class="defaultbold">
							        <xsl:value-of select="format-number(TotalDeposits div 1000, '#,###.0')"/>
						        </span>
					        </td>
					        <td class="datashade"><span class="defaultbold"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></span></td>
					        <td class="datashade" align="right" valign="top">
						        <span class="defaultbold">
							        <xsl:value-of select="format-number(YearAgoTotalDeposits div 1000, '#,###.0')"/>
						        </span>
					        </td>
					        <td class="datashade" valign="top"><span class="defaultbold"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></span></td>
				        </tr>								
							</table>            
							<br />
						</td>
					</tr>
					</xsl:if>
					</xsl:for-each>
				</table>
</xsl:template>
<!-- Default Template Ends here. all templates below are ONLY if you are using the 'Style Template' section in the console. -->



















<!-- This is Template ONE option in the console under 'Style Template'
//This template is a striped down version of the main template. Everything (data) is displayed in single rows.
//The sections will be commented on where the rows are. you should be able to just move rows (entire chunks of data) where you need too. Edits to this section are rare, mostly due to size (width of the page).
-->

<xsl:template name="DepositMarketShare_Title2">
<xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')" />
	<TR>
		<TD class="colordark"><SPAN class="title1light"><xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/></SPAN></TD>
				<TD class="colordark" align="right" valign="top">
					<span class="subtitlelight"><xsl:value-of select="$CompanyNameCaps" /> (<xsl:value-of select="Company/Exchange" /> - <xsl:value-of select="Company/Ticker" />)</span>
					<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
				</TD>
			</TR>
			<TR class="default" align="left">
				<TD class="colorlight" colspan="2">
					<span class="default">
						<xsl:value-of select="Company/Header" disable-output-escaping="yes" />
					</span>
				</TD>
			</TR>
			
</xsl:template>

			<xsl:template name="DepositMarketShare_Data2">
			<xsl:variable name="Year1" select="Years/Year1" />
			<xsl:variable name="Year2" select="Years/Year2" />
			<xsl:variable name="MaxResultsPerMarket" select="10"/>
			<table border="0" cellspacing="0" cellpadding="2" width="100%">
					<tr>
						<td class="data">
							<span class="title2">Deposit Market	Share for Selected Markets</span>
							<br />
							<span class="default">
							<xsl:if test="Company/OwnershipSetting = 'C'">Current</xsl:if>
							<xsl:if test="Company/OwnershipSetting != 'C'">Pending</xsl:if>
							Ownership, including Bank, Savings Bank, Thrift Branches<br />
							Ownership as of	
							<xsl:value-of select="Company/PriceDate"/>,
							Branches and Deposits as of	
							<xsl:value-of select="Company/DepositsAsOf"/>
							</span>
							<br />
						</td>
					</tr>
					<xsl:for-each select="Markets">
					<xsl:if test="string-length(Name) = 0"></xsl:if>
					<xsl:if test="string-length(Name) != 0">
					<tr>
						<td class="data">
							<table cellspacing="0" border="0" cellpadding="4" width="100%" class="data header_trans">
								<tr valign="top">
									<td class="surr datashade" valign="middle" colspan="9">
										<span class="defaultbold"><xsl:value-of select="Name"/></span>
									</td>
								</tr>
								<tr class="data">
									<td class="data botbord" align="right" valign="bottom">
										<span class="defaultbold"><xsl:value-of select="$Year1" /> Rank</span>
									</td>
									<td class="data botbord" align="right" valign="bottom">
										<span class="defaultbold"><xsl:value-of select="$Year2" /> Rank</span>
									</td>
									<td class="data botbord" align="left" valign="bottom" width="40%">
										<span class="defaultbold">Institution (Headquarters)</span>
									</td>
									<td class="data botbord" align="left" valign="bottom">
										<span class="defaultbold">Type</span>
									</td>
									<td class="data botbord" align="right" valign="bottom">
										<span class="defaultbold"><xsl:value-of select="$Year1" /> Number of Branches</span>
									</td>
									<td class="data botbord" align="right" valign="bottom">
										<span class="defaultbold"><xsl:value-of select="$Year1" /> Total Deposits in Market ($M)</span>
									</td>
									<td class="data botbord" align="right" valign="bottom">
										<span class="defaultbold"><xsl:value-of select="$Year1" /> Total Market Share (%)</span>
									</td>
									<td class="data botbord" align="right" valign="bottom">
										<span class="defaultbold"><xsl:value-of select="$Year2" /> Total Deposits in Market ($M)</span>
									</td>
									<td class="data botbord" align="right" valign="bottom">
										<span class="defaultbold"><xsl:value-of select="$Year2" /> Total Market Share (%)</span>
									</td>
								</tr>
								<xsl:for-each select="MarketData">
								<xsl:if test="Rank &lt; $MaxResultsPerMarket + 1 or ../../Company/KeyInstn = CurrentKeyInstn">
								<xsl:if test="../../Company/KeyInstn != CurrentKeyInstn">
								<tr class="data">
									<td class="data" align="right" valign="top">
										<xsl:value-of select="Rank"/>
									</td>
									<td class="data" align="right" valign="top">
										<xsl:value-of select="YearAgoRank"/>
									</td>
									<td class="data" align="left" valign="top" width="40%">
										<xsl:value-of select="Name" disable-output-escaping="yes"/>
									</td>
									<td class="data" align="left" valign="top">
										<xsl:value-of select="InstitutionType"/>
									</td>
									<td class="data" align="right" valign="top">
										<xsl:value-of select="Branches"/>
									</td>
									<td class="data" align="right" valign="top">
										<xsl:value-of select="format-number(Deposits div 1000, '#,##0.0')"/>
									</td>
									<td class="data" align="right" valign="top">
										<xsl:value-of select="format-number(CurrentMarketShare, '##0.00')"/>
									</td>
									<td class="data" align="right" valign="top">
										<xsl:value-of select="format-number(YearAgoDeposits div 1000, '#,##0.0')"/>
									</td>
									<td class="data" align="right" valign="top">
										<xsl:value-of select="format-number(YearAgoMarketShare, '##0.00')"/>
									</td>
								</tr>
								</xsl:if>
								<xsl:if test="../../Company/KeyInstn = CurrentKeyInstn">
								<tr class="data">
									<td class="data" align="right" valign="top">
										<span class="defaultbold">
										<xsl:value-of select="Rank"/>
										</span> 
									</td>
									<td class="data" align="right" valign="top">
										<span class="defaultbold"><xsl:value-of select="YearAgoRank"/></span> 
									</td>
									<td class="data" align="left" valign="top" width="40%">
										<span class="defaultbold">
										<xsl:value-of select="Name" disable-output-escaping="yes"/>
										</span>
									</td>
									<td class="data" align="left" valign="top">
										<span class="defaultbold">
										<xsl:value-of select="InstitutionType"/>
										</span>
									</td>
									<td class="data" align="right" valign="top">
										<span class="defaultbold">
										<xsl:value-of select="Branches"/>
										</span>
									</td>
									<td class="data" align="right" valign="top">
										<span class="defaultbold">
										<xsl:value-of select="format-number(Deposits div 1000, '#,##0.0')"/>
										</span>
									</td>
									<td class="data" align="right" valign="top">
										<span class="defaultbold">
										<xsl:value-of select="format-number(CurrentMarketShare, '##0.00')"/>
										</span>
									</td>
									<td class="data" align="right" valign="top">
										<span class="defaultbold">
										<xsl:value-of select="format-number(YearAgoDeposits div 1000, '#,##0.0')"/>
										</span>
									</td>
									<td class="data" align="right" valign="top">
										<span class="defaultbold">
										<xsl:value-of select="format-number(YearAgoMarketShare, '##0.00')"/>
										</span>
									</td>
								</tr>
								</xsl:if>
								</xsl:if>
								</xsl:for-each>	
				        <tr class="datashade">
					        <td class="datashade topbord" rowspan="2"><span class="defaultbold"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></span></td>
					        <td class="datashade topbord" rowspan="2"><span class="defaultbold">&#160;</span></td>
					        <td class="datashade topbord" valign="top"><span class="defaultbold">Total for <!--xsl:value-of select="TotalInstitutions"/-->institutions in market<br/><br/></span></td>
					        <td class="datashade topbord" align="right" valign="top"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					        <td class="datashade topbord" align="right" valign="top">
						        <span class="defaultbold">
							        <span CLASS="defaultbold"><xsl:value-of select="TotalBranches"/></span>
						        </span>
					        </td>
					        <td class="datashade topbord" align="right" valign="top">
						        <span class="defaultbold">
							        <xsl:value-of select="format-number(TotalDeposits div 1000, '#,###.0')"/>
						        </span>
					        </td>
					        <td class="datashade topbord"><span class="defaultbold"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></span></td>
					        <td class="datashade topbord" align="right" valign="top">
						        <span class="defaultbold">
							        <xsl:value-of select="format-number(YearAgoTotalDeposits div 1000, '#,###.0')"/>
						        </span>
					        </td>
					        <td class="datashade topbord"><span class="defaultbold"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></span></td>
				        </tr>					
							</table>              
							<br />
						</td>
					</tr>
					</xsl:if>
					</xsl:for-each>
				</table>
</xsl:template>


  <!-- Template ONE Ends here. -->
  
  
  


<!-- This is Template 3  -->
<xsl:template name="DepositMarketShare_Data3">
<xsl:variable name="Year1" select="Years/Year1" />
<xsl:variable name="Year2" select="Years/Year2" />
<xsl:variable name="MaxResultsPerMarket" select="10"/>
<table border="0" cellspacing="0" cellpadding="2" width="100%">
	<tr>
		<td class="data">
			<span class="title2">Deposit Market	Share for Selected Markets</span>
			<br />
			<span class="default">
			<xsl:if test="Company/OwnershipSetting = 'C'">Current</xsl:if>
			<xsl:if test="Company/OwnershipSetting != 'C'">Pending</xsl:if>
			Ownership, including Bank, Savings Bank, Thrift Branches<br />
			Ownership as of	
			<xsl:value-of select="Company/PriceDate"/>,
			Branches and Deposits as of	
			<xsl:value-of select="Company/DepositsAsOf"/>
			</span>
			<br />
		</td>
	</tr>
	<xsl:for-each select="Markets">
	<xsl:if test="string-length(Name) = 0"></xsl:if>
	<xsl:if test="string-length(Name) != 0">
	<tr>
		<td class="data">
			<table cellspacing="0" border="0" cellpadding="3" width="100%" class="table2 data header_trans">
				<tr valign="top">
					<td class="datashade table1_item" valign="middle" colspan="9">
						<span class="defaultbold"><xsl:value-of select="Name"/></span>
					</td>
				</tr>
				<tr>
					<td class="data table1_item" align="right" valign="bottom">
						<span class="defaultbold"><xsl:value-of select="$Year1" /> Rank</span>
					</td>
					<td class="data divider_left table1_item" align="right" valign="bottom">
						<span class="defaultbold"><xsl:value-of select="$Year2" /> Rank</span>
					</td>
					<td class="data divider_left table1_item" align="left" valign="bottom" width="40%">
						<span class="defaultbold">Institution (Headquarters)</span>
					</td>
					<td class="data divider_left table1_item" align="left" valign="bottom">
						<span class="defaultbold">Type</span>
					</td>
					<td class="data divider_left table1_item" align="right" valign="bottom">
						<span class="defaultbold"><xsl:value-of select="$Year1" /> Number of Branches</span>
					</td>
					<td class="data divider_left table1_item" align="right" valign="bottom">
						<span class="defaultbold"><xsl:value-of select="$Year1" /> Total Deposits in Market	($M)</span>
					</td>
					<td class="data divider_left table1_item" align="right" valign="bottom">
						<span class="defaultbold"><xsl:value-of select="$Year1" /> Total Market Share (%)</span>
					</td>
					<td class="data divider_left table1_item" align="right" valign="bottom">
						<span class="defaultbold"><xsl:value-of select="$Year2" /> Total Deposits in Market ($M)</span>
					</td>
					<td class="data divider_left table1_item" align="right" valign="bottom">
						<span class="defaultbold"><xsl:value-of select="$Year2" /> Total Market Share (%)</span>
					</td>
				</tr>
				<xsl:for-each select="MarketData">
				
				<xsl:if test="Rank &lt; $MaxResultsPerMarket + 1 or ../../Company/KeyInstn = CurrentKeyInstn">
				<xsl:if test="../../Company/KeyInstn != CurrentKeyInstn">
				<tr class="data">
					<td class="data table1_item" align="right" valign="top">
						<xsl:value-of select="Rank"/>
					</td>
					<td class="data divider_left table1_item" align="right" valign="top">
						<xsl:value-of select="YearAgoRank"/>
					</td>
					<td class="data divider_left table1_item" align="left" valign="top" width="40%">
						<xsl:value-of select="Name" disable-output-escaping="yes"/>
					</td>
					<td class="data divider_left table1_item" align="left" valign="top">
						<xsl:value-of select="InstitutionType"/>
					</td>
					<td class="data divider_left table1_item" align="right" valign="top">
						<xsl:value-of select="Branches"/>
					</td>
					<td class="data divider_left table1_item" align="right" valign="top">
						<xsl:value-of select="format-number(Deposits div 1000, '#,##0.0')"/>
					</td>
					<td class="data divider_left table1_item" align="right" valign="top">
						<xsl:value-of select="format-number(CurrentMarketShare, '##0.00')"/>
					</td>
					<td class="data divider_left table1_item" align="right" valign="top">
						<xsl:value-of select="format-number(YearAgoDeposits div 1000, '#,##0.0')"/>
					</td>
					<td class="data divider_left table1_item" align="right" valign="top">
						<xsl:value-of select="format-number(YearAgoMarketShare, '##0.00')"/>
					</td>
				</tr>
				</xsl:if>
				<xsl:if test="../../Company/KeyInstn = CurrentKeyInstn">
				<tr class="data">
					<td class="data table1_item" align="right" valign="top">
						<span class="defaultbold">
						<xsl:value-of select="Rank"/>
						</span> 
					</td>
					<td class="data divider_left table1_item" align="right" valign="top">
						<span class="defaultbold"><xsl:value-of select="YearAgoRank"/></span> 
					</td>
					<td class="data divider_left table1_item" align="left" valign="top" width="40%">
						<span class="defaultbold">
						<xsl:value-of select="Name" disable-output-escaping="yes"/>
						</span>
					</td>
					<td class="data divider_left table1_item" align="left" valign="top">
						<span class="defaultbold">
						<xsl:value-of select="InstitutionType"/>
						</span>
					</td>
					<td class="data divider_left table1_item" align="right" valign="top">
						<span class="defaultbold">
						<xsl:value-of select="Branches"/>
						</span>
					</td>
					<td class="data divider_left table1_item" align="right" valign="top">
						<span class="defaultbold">
						<xsl:value-of select="format-number(Deposits div 1000, '#,##0.0')"/>
						</span>
					</td>
					<td class="data divider_left table1_item" align="right" valign="top">
						<span class="defaultbold">
						<xsl:value-of select="format-number(CurrentMarketShare, '##0.00')"/>
						</span>
					</td>
					<td class="data divider_left table1_item" align="right" valign="top">
						<span class="defaultbold">
						<xsl:value-of select="format-number(YearAgoDeposits div 1000, '#,##0.0')"/>
						</span>
					</td>
					<td class="data divider_left table1_item" align="right" valign="top">
						<span class="defaultbold">
						<xsl:value-of select="format-number(YearAgoMarketShare, '##0.00')"/>
						</span>
					</td>
				</tr>
				</xsl:if>
				</xsl:if>
				</xsl:for-each>                 

				<tr class="datashade">
					<td class="datashade" rowspan="2" valign="top"><span class="defaultbold"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></span></td>
					<td class="datashade divider_left" rowspan="2" valign="top"><span class="defaultbold">&#160;</span></td>
					<td class="datashade divider_left" valign="top"><span class="defaultbold">Total for <!--xsl:value-of select="TotalInstitutions"/-->institutions in market<br/><br/></span></td>
					<td class="datashade divider_left" align="right" valign="top"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					<td class="datashade divider_left" align="right" valign="top">
						<span class="defaultbold">
							<span class="defaultbold"><xsl:value-of select="TotalBranches"/></span>
						</span>
					</td>
					<td class="datashade divider_left" align="right" valign="top">
						<span class="defaultbold">
							<xsl:value-of select="format-number(TotalDeposits div 1000, '#,###.0')"/>
						</span>
					</td>
					<td class="datashade divider_left"><span class="defaultbold"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></span></td>
					<td class="datashade divider_left" align="right" valign="top">
						<span class="defaultbold">
							<xsl:value-of select="format-number(YearAgoTotalDeposits div 1000, '#,###.0')"/>
						</span>
					</td>
					<td class="datashade divider_left" valign="top"><span class="defaultbold"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></span></td>
				</tr>
			</table>            
      <br />
		</td>
	</tr>
	</xsl:if>
	</xsl:for-each>
</table>
</xsl:template>

  <!-- Template 3 Ends here. -->
  





<!-- Main XSLT templates are here. these are the templates which are actually being displayed in the page. this is for TOP LEVEL editing. if you dont need to use the individual templates above you can do your main editing here 

when pulling THESE templates below you must carry the top comment with them and uncomment them when you drop it into the company specific xslt.




-->



<xsl:template match="irw:DepositMarketShare">	
<xsl:variable name="TemplateName" select="Company/TemplateName"/>
<xsl:choose>
<!-- Template 1 -->
<xsl:when test="$TemplateName = 'AltDepositM1'">
<xsl:call-template name="TemplateONEstylesheet" />
<SCRIPT LANGUAGE="JavaScript">
function openWindow (url,name,widgets) 
{
	popupWin = window.open (url,name,widgets);
	popupWin.opener.top.name="opener";
	popupWin.focus();
}
</SCRIPT>
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" class="">
	<xsl:call-template name="Title_S2"/>
	<TR ALIGN="CENTER">
		<TD class="leftTOPbord data" colspan="2"><xsl:call-template name="DepositMarketShare_Data2" /></TD>
	</TR>
</TABLE>
<TABLE border="0" cellpadding="3" cellspacing="0" width="100%">
	<TR class="default" align="left">
		<TD class="data" colspan="2"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes" /></span></TD>
	</TR>
</TABLE>
<xsl:call-template name="Copyright" />
</xsl:when>
<!-- End Template 1 -->



<!-- Template 3 -->
<xsl:when test="$TemplateName = 'AltDepositM3'">
<xsl:call-template name="TemplateTHREEstylesheet" />
<SCRIPT LANGUAGE="JavaScript">
	function openWindow (url,name,widgets) {
		popupWin = window.open (url,name,widgets);
		popupWin.opener.top.name="opener";
		popupWin.focus();
	}
</SCRIPT>
<xsl:call-template name="Title_T3"/>
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" class="bottom_cap report_top">
	<TR ALIGN="CENTER">
		<TD class="" colspan="2"><xsl:call-template name="DepositMarketShare_Data3" />	</TD>
	</TR>
</TABLE>
<TABLE border="0" cellpadding="3" cellspacing="0" width="100%">
	<TR class="default" align="left">
		<TD class="data" colspan="2"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes" /></span></TD>
	</TR>
</TABLE>
<xsl:call-template name="Copyright" />
</xsl:when>
<!-- End Template 3 -->


<!-- Template Default -->
<xsl:otherwise>
	<SCRIPT LANGUAGE="JavaScript">
		function openWindow (url,name,widgets) {
			popupWin = window.open (url,name,widgets);
			popupWin.opener.top.name="opener";
			popupWin.focus();
		}
	</SCRIPT>
	
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" class="colordark">
<xsl:call-template name="Title_S1"/>
	<TR ALIGN="CENTER">
		<TD class="colordark" colspan="2"><xsl:call-template name="DepositMarketShare_Data" /></TD>
	</TR>
</TABLE>
<TABLE border="0" cellpadding="3" cellspacing="0" width="100%">
	<TR class="default" align="left">
		<TD class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes" /></span></TD>
	</TR>
</TABLE>
<xsl:call-template name="Copyright" />
</xsl:otherwise>
</xsl:choose>
</xsl:template>
</xsl:stylesheet>