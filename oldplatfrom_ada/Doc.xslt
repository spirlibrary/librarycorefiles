<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns="http://www.w3.ord/TR/REC-html40"
xmlns:irw="http://www.snl.com/xml/irw">

<xsl:output method="html" media-type="text/html" indent="no"/>

<xsl:template name="Doc_Title">
<xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
	<tr>
		<td class="colordark" NOWRAP="NOWRAP"><span class="title1light"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>Document Details</span></td>
		<td class="colordark" NOWRAP="NOWRAP" align="right" valign="top"><span class="subtitlelight"><xsl:value-of select="$CompanyNameCaps"/> (<xsl:value-of select="Company/Exchange"/> - <xsl:value-of select="Company/Ticker"/>)</span><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
	</tr>
	<tr class="default" align="left">
		<td class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Header" disable-output-escaping="yes"/></span></td>
	</tr>
</xsl:template>

<xsl:template name="doc_detail">
<table BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" class="colordark">
	<xsl:call-template name="Title_S1"/>
	<tr ALIGN="CENTER">
		<td class="colordark" colspan="2">
			<table BORDER="0" CELLSPACING="0" CELLPADDING="4" WIDTH="100%">
				<tr ALIGN="CENTER">
					<td class="data">
						<table BORDER="0" CELLSPACING="1" CELLPADDING="0" WIDTH="100%" class="data">
							<tr>
								<td class="data">
									<table BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%">
										<tr>
											<td COLSPAN="2" class="header"> Document ID <xsl:value-of select="Document/KeyDoc"/> </td>
										</tr>
										<tr>
											<td class="data" nowrap="1"><span class="defaultbold">Filing Type</span> </td>
											<td class="data"><xsl:if test="Document/DocTypeDescription != '(none)'"><a class="fielddef" title="{Document/DocTypeDescription}"><xsl:attribute name="href"> <xsl:value-of select="Document/FilingTypeLink"/> </xsl:attribute><xsl:value-of select="Document/DocTypeDescription"/> </a></xsl:if><xsl:if test="Document/DocTypeDescription = '(none)'"><xsl:value-of select="Document/DocTypeDescription"/></xsl:if></td>
										</tr>
										<tr>
											<td class="data"><span class="defaultbold">Company</span> </td>
											<td class="data"><xsl:value-of select="Document/CityState" disable-output-escaping="yes"/> </td>
										</tr>
										<xsl:if test="Document/DocEventDate != ''">
										<tr>
											<td class="data"><span class="defaultbold">Event Date</span> </td>
											<td class="data"><xsl:value-of select="Document/DocEventDate"/> </td>
										</tr>
										</xsl:if>
										<xsl:if test="Document/DocFilingDate != ''">
										<tr>
											<td class="data"><span class="defaultbold">Filing Date</span> </td>
											<td class="data"><xsl:value-of select="Document/DocFilingDate"/> </td>
										</tr>
										</xsl:if>
										<xsl:if test="Document/DocAbstract != ''">
										<tr>
											<td VALIGN="top" class="data"><span class="defaultbold">Abstract</span> </td>
											<td class="data"><xsl:value-of select="Document/DocAbstract" disable-output-escaping="yes"/> </td>
										</tr>
										</xsl:if>
									</table>
								</td>
							</tr>
						</table>
						<br />
						<xsl:for-each select="KeyFiles">
						<table border="0" cellspacing="1" cellpadding="3" width="100%" class="data">
							<tr>
								<td class="header">Download</td>
								<td class="header">File Sections</td>
							</tr>
							<xsl:variable name="KeyFile" select="KeyFile"/>
							<tr>
								<td class="data" valign="top" align="center" nowrap="">
								<xsl:if test="FileFormat != 'SGML' and FileFormat != 'HTML'">
									<table border="0" cellspacing="0" cellpadding="3" width="80">
										<tr>
											<td align="center" valign="top" class="data"  nowrap=""><a><xsl:if test="FileFormat != 'PDF'"><xsl:attribute name="TARGET"><xsl:value-of select="_new"/> </xsl:attribute></xsl:if><xsl:attribute name="href"> file.aspx?iid=<xsl:value-of select="../Company/KeyInstn"/>&amp;fid=<xsl:value-of select="$KeyFile"/>&amp;osid=9&amp;o=<xsl:value-of select="KeyFileFormat"/> </xsl:attribute><img border="0"><xsl:attribute name="src"><xsl:if test="FileFormat = 'XML'">/images/interactive/ir/HTML.gif</xsl:if><xsl:if test="FileFormat != 'XML'">/images/interactive/ir/<xsl:value-of select="FileFormat"/>.gif</xsl:if></xsl:attribute><xsl:attribute name="alt"><xsl:value-of select="FileFormat"/></xsl:attribute></img></a><br />(<xsl:value-of select="format-number(Size, '#.0')"/>K)</td>
											<xsl:if test="normalize-space(PDFConvertible) = 'true'">
											<td align="center" valign="top" class="data"><img width="32px" height="32px" src="/images/interactive/blank.gif" border="0"/>&#160;</td>
											</xsl:if>
										</tr>
									</table>
								</xsl:if>
								<xsl:if test="FileFormat = 'SGML' or FileFormat = 'HTML'">
									<table border="0" cellspacing="0" cellpadding="3" width="80">
										<tr>
											<td align="center" valign="top" class="data"  nowrap=""><a><xsl:attribute name="href">file.aspx?IID=<xsl:value-of select="../Company/KeyInstn"/>&amp;FID=<xsl:value-of select='$KeyFile'/>&amp;O=2&amp;OSID=9 </xsl:attribute><xsl:attribute name="target">_new</xsl:attribute><img src="/images/interactive/ir/html.gif" border="0" alt="HTML"/></a><br />(<xsl:value-of select="format-number(Size, '#.0')"/>K)</td>
											<xsl:if test="normalize-space(PDFConvertible) = 'true'">
											<td align="center" valign="top" class="data"><a><xsl:attribute name="href">file.aspx?IID=<xsl:value-of select="../Company/KeyInstn"/>&amp;FID=<xsl:value-of select='KeyFile'/>&amp;O=3&amp;OSID=9</xsl:attribute><xsl:attribute name="target">_new</xsl:attribute><img src="/images/interactive/ir/pdf.gif" border="0" alt="PDF"/> </a>&#160;</td>
											</xsl:if>
										</tr>
									</table>
								</xsl:if>
								</td>
								<td class="data" valign="top" style="padding:0px" width="100%">
								<xsl:if test="SectionCount > 0">
									<table cellpadding="3" cellspacing="0" border="0" width="100%" class="data">
									<xsl:for-each select="../Sections[KeyFile = $KeyFile]">
										<tr class="data">
										<xsl:if test="position() mod 2 = 0">
										<xsl:attribute name="class">datashade</xsl:attribute>
										</xsl:if>
											<td align="right" class="defaultbold" nowrap=""><xsl:value-of select="position()"/>.</td>
											<td class="default" align="left" width="100%"><xsl:value-of select="Label"/></td>
										</tr>
									</xsl:for-each>
									</table>
								</xsl:if>
								</td>
							</tr>
							<xsl:if test="FileFormat = 'HTML'">
							<xsl:if test="normalize-space(Edgar) = 'true'">
							<tr>
								<td colspan="3" class="data"><span class="defaultbold">Note: </span><span class="default">This document was filed with the SEC in HTML format, as allowed by the recent EDGAR system modernization. SNL cannot take responsibility for its appearance, layout, or legibility.</span> </td>
							</tr>
							</xsl:if>
							<xsl:if test="normalize-space(Edgar) = 'false'">
							<tr>
								<td colspan="3" class="data"> <xsl:if test="SectionCount > 1"> <span class="defaultbold">Note: </span><span class="default">Some or all of this document was retrieved from the filer directly in HTML format. SNL can only provide those parts in this format and cannot take responsibility for their appearance, layout, or legibility, or for the validity of links to images or to other pages on the filer's web site. </span> </xsl:if> <xsl:if test="SectionCount = 1"> <span class="defaultbold">Note: </span><span class="default">This document was retrieved from the filer directly in HTML format. SNL can provide it in this format only and cannot take responsibility for its appearance, layout, or legibility, or for the validity of links to images or to other pages on the filer's web site. </span> </xsl:if> </td>
							</tr>
							</xsl:if>
							</xsl:if>
						</table>
						<br/>
						</xsl:for-each>
					</td>
				</tr>
				<xsl:if test="Document/XBRLDoc = 'true'">
				<tr>
					<td>
						<table cellspacing="0" cellpadding="3" border="0" width="100%" class="data">
							<tr>
								<td class="header" align="left" valign="middle" colspan="3"><img src="/images/interactive/ir/xbrl.gif" border="0" alt="XBRL" style="vertical-align:middle; padding-right:5px;"/> XBRL Options</td>
							</tr>
							<tr class="data" align="left" valign="top">
								<td colspan="3">Select the XBRL icon to open the full XBRL Filing in SNL's XBRL Reader.  Select the XML icon to download the raw xml feed, to open in another reader.</td>
							</tr>
							<tr class="data" align="left" valign="top">
								<td><a><xsl:attribute name="href"><xsl:value-of select="Document/XBRLRenderUrl"/></xsl:attribute><xsl:attribute name="target">_new</xsl:attribute><img src="/images/interactive/ir/xbrl.gif" border="0" alt="XBRL" title="Open this Section in SNL Reader"/></a></td>
								<td>1.</td>
								<td>Full XBRL Filing</td>
							</tr>
							<xsl:for-each select="XbrlSections">
							<tr class="data" align="left" valign="top">
							<xsl:if test="position() mod 2 = 1">
							<xsl:attribute name="class">datashade</xsl:attribute>
							</xsl:if>
								<td><a><xsl:attribute name="href"><xsl:value-of select="Url"/></xsl:attribute><xsl:attribute name="target">_new</xsl:attribute><img src="/images/interactive/ir/xml.gif" border="0" alt="XML" title="Open this Section in XML Format"/></a></td>
								<td class="defaultbold"><xsl:value-of select="Number"/>.</td>
								<td width="100%"><xsl:value-of select="Label"/></td>
							</tr>
							</xsl:for-each>
							<tr>
								<td colspan="3" class="data"><span class="defaultbold">Note: </span><span class="default">This document was filed with the SEC in XBRL format, as allowed by the recent EDGAR system modernization. SNL cannot take responsibility for its appearance, layout, or legibility.</span></td>
							</tr>
						</table>
					</td>
				</tr>
				</xsl:if>
				<xsl:if test="count(DocumentTables) &gt; 0">
				<tr class="colorlight" >
					<td class="data" align="left" valign="top">
						<table cellspacing="0" cellpadding="0" border="0" width="100%" class="data">
							<tr>
								<td class="data" align="left" valign="top">
									<table cellspacing="0" cellpadding="3" border="0" width="100%" class="data">
										<tr>
											<td class="header"><img src="/images/interactive/ir/excel.gif" border="0"/> </td>
											<td class="header" width="100%">Export Options</td>
										</tr>
									</table>
									<table cellspacing="0" cellpadding="3" border="0" width="50" align="left">
										<tr>
											<td align="left" class="data">Highlight the tables you wish to export then select the Export button.  You may select multiple tables by holding down the Ctrl key while making your selections.</td>
										</tr>
										<xsl:variable name="KeyInstn" select="Company/KeyInstn"/>
										<xsl:variable name="FormAction" select="concat('excelview.aspx?IID=', $KeyInstn)"></xsl:variable>
										<xsl:variable name="FirstKey" select="DocumentTables/Key" />
										<form name="frmDocTables" method="post">
										<xsl:attribute name="action"><xsl:value-of select="$FormAction"/></xsl:attribute>
										<tr>
											<td class="data" align="left" valign="top">
												<label class="visuallyhidden" for="DocTables">DocTables</label><select name="DocTables" MULTIPLE="yes" SIZE="5" id="DocTables">										<xsl:for-each select="DocumentTables"><option value="{Key}"><xsl:if test="string(Key) = string($FirstKey)"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="ExtractedDocumentTable" disable-output-escaping="yes" /></option></xsl:for-each></select></td>
										</tr>
										<tr class="colorlight">
											<td class="data" align="right" valign="bottom" nowrap="" height="40px"><input type="submit" value="Export"/></td>
										</tr>
										</form>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				</xsl:if>
				<xsl:if test="count(RelatedDocs) &gt; 0">
				<tr>
					<td>
						<table cellspacing="0" cellpadding="0" border="0" width="100%" class="data">
							<tr>
								<td class="header" align="left" valign="top" colspan="3">Related Document(s)</td>
							</tr>
							<xsl:for-each select="RelatedDocs">
							<tr class="data" align="left" valign="top">
							<xsl:if test="position() mod 2 = 1">
							<xsl:attribute name="class">datashade</xsl:attribute>
							</xsl:if>
								<td class="data" align="left" valign="top"><xsl:value-of select="Type"/></td>
								<td><a> <xsl:attribute name="href"> doc.aspx?IID=<xsl:value-of select="../Company/KeyInstn"/>&amp;DID=<xsl:value-of select="KeyDoc"/> </xsl:attribute> <xsl:attribute name="target">_new</xsl:attribute> View Document Details </a> </td>
							</tr>
							</xsl:for-each>
						</table>
					</td>
				</tr>
				</xsl:if>
				<tr>
					<td class="data" align="center"><a> <xsl:attribute name="href">docs.aspx?iid=<xsl:value-of select="Company/KeyInstn"/> <!--javascript:history.back()--> </xsl:attribute> <b>View all documents</b> </a> </td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<xsl:if test="Company/Footer != ''">
<table border="0" cellpadding="3" cellspacing="0" width="100%">
	<tr class="default" align="left">
		<td class="colorlight"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span></td>
	</tr>
</table>
</xsl:if>
<xsl:call-template name="Copyright"/>
</xsl:template>
<!-- Default Template Ends here. -->

<!-- Template ONE -->
<xsl:template name="doc_detail2">
<table BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" class="data">
	<xsl:call-template name="Title_S2"/>
	<tr ALIGN="CENTER">
		<td colspan="2" class="leftTOPbord data">
			<table BORDER="0" CELLSPACING="0" CELLPADDING="4" WIDTH="100%">
				<tr ALIGN="CENTER">
					<td class="data">
						<table BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
							<tr>
								<td class="data">
									<table BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" class="data">
										<tr>
											<td colspan="2" class="surr datashade"><span class="defaultbold">Document ID <xsl:value-of select="Document/KeyDoc"/></span></td>
										</tr>
										<tr>
											<td class="data" nowrap="1"><span class="defaultbold">Filing Type</span> </td>
											<td class="data" width="100%"><xsl:if test="Document/DocTypeDescription != '(none)'"> <a class="fielddef"  title="{Document/FilingTypeLink}"> <xsl:attribute name="href"> <xsl:value-of select="Document/FilingTypeLink"/> </xsl:attribute> <xsl:value-of select="Document/DocTypeDescription"/> </a> </xsl:if> <xsl:if test="Document/DocTypeDescription = '(none)'"> <xsl:value-of select="Document/DocTypeDescription"/> </xsl:if> </td>
										</tr>
										<tr>
											<td class="data"><span class="defaultbold">Company</span> </td>
											<td class="data"><xsl:value-of select="Document/CityState" disable-output-escaping="yes"/> </td>
										</tr>
										<xsl:if test="Document/DocEventDate != ''">
										<tr>
											<td class="data"><span class="defaultbold">Event Date</span> </td>
											<td class="data"><xsl:value-of select="Document/DocEventDate"/> </td>
										</tr>
										</xsl:if>
										<xsl:if test="Document/DocFilingDate != ''">
										<tr>
											<td class="data"><span class="defaultbold">Filing Date</span> </td>
											<td class="data"><xsl:value-of select="Document/DocFilingDate"/> </td>
										</tr>
										</xsl:if>
										<xsl:if test="Document/DocAbstract != ''">
										<tr>
											<td VALIGN="top" class="data"><span class="defaultbold">Abstract</span> </td>
											<td class="data"><xsl:value-of select="Document/DocAbstract" disable-output-escaping="yes"/> </td>
										</tr>
										</xsl:if>
									</table>
								</td>
							</tr>
						</table>
						<br />
						<xsl:for-each select="KeyFiles">
						<table border="0" cellspacing="0" cellpadding="3" width="100%" class="table1">
							<tr>
								<td class="surrleft datashade"><span class="defaultbold">Download</span></td>
								<td class="surrright datashade"><span class="defaultbold">File Sections</span></td>
							</tr>
							<xsl:variable name="KeyFile" select="KeyFile"/>
							<tr>
								<td class="surrmid data" style="" valign="top" align="center" nowrap="">
								<xsl:if test="count(../Sections[KeyFile = $KeyFile]) &lt;= 1">
								<xsl:attribute name="style">border-bottom:0px</xsl:attribute>
								</xsl:if>
								<xsl:if test="count(../Sections[KeyFile = $KeyFile]) > 5">
								<xsl:attribute name="class">surrmid data</xsl:attribute>
								</xsl:if>
								<xsl:if test="FileFormat != 'SGML' and FileFormat != 'HTML'">
									<table border="0" cellspacing="0" cellpadding="3" width="80">
										<tr>
											<td align="center" valign="top" class="data"  nowrap=""> <a> <xsl:if test="FileFormat != 'PDF'"> <xsl:attribute name="TARGET"> <xsl:value-of select="_new"/> </xsl:attribute> </xsl:if> <xsl:attribute name="href"> file.aspx?iid=<xsl:value-of select="../Company/KeyInstn"/>&amp;fid=<xsl:value-of select="$KeyFile"/>&amp;osid=9&amp;o=<xsl:value-of select="KeyFileFormat"/> </xsl:attribute> <img border="0"> <xsl:attribute name="src"> <xsl:if test="FileFormat = 'XML'">						/images/interactive/ir/HTML.gif</xsl:if> <xsl:if test="FileFormat != 'XML'">/images/interactive/ir/<xsl:value-of select="FileFormat"/>.gif</xsl:if> </xsl:attribute> <xsl:attribute name="alt"> <xsl:value-of select="FileFormat"/> </xsl:attribute> </img></a><br />(<xsl:value-of select="format-number(Size, '#.0')"/>K) </td>
											<xsl:if test="normalize-space(PDFConvertible) = 'true'">
											<td align="center" valign="top" class="data"><img width="32px" height="32px" src="/images/interactive/blank.gif" border="0"/>&#160;</td>
											</xsl:if>
										</tr>
									</table>
								</xsl:if>
								<xsl:if test="FileFormat = 'SGML' or FileFormat = 'HTML'">
									<table border="0" cellspacing="0" cellpadding="3" width="80">
										<tr>
											<td align="center" valign="top" class="data"  nowrap=""><a> <xsl:attribute name="href">file.aspx?IID=<xsl:value-of select="../Company/KeyInstn"/>&amp;FID=<xsl:value-of select='$KeyFile'/>&amp;O=2&amp;OSID=9 </xsl:attribute> <xsl:attribute name="target">_new</xsl:attribute> <img src="/images/interactive/ir/html.gif" border="0" alt="HTML"/> </a><br />(<xsl:value-of select="format-number(Size, '#.0')"/>K) </td>
											<xsl:if test="normalize-space(PDFConvertible) = 'true'">
											<td align="center" valign="top" class="data"> <a> <xsl:attribute name="href">file.aspx?IID=<xsl:value-of select="../Company/KeyInstn"/>&amp;FID=<xsl:value-of select='KeyFile'/>&amp;O=3&amp;OSID=9</xsl:attribute> <xsl:attribute name="target">_new</xsl:attribute> <img src="/images/interactive/ir/pdf.gif" border="0" alt="PDF"/> </a>&#160; </td>
											</xsl:if>
										</tr>
									</table>
								</xsl:if>
								</td>
								<td class="rightbord data" valign="top" style="padding:0px" width="100%">
								<xsl:if test="SectionCount > 0">
									<table cellpadding="3" cellspacing="0" border="0" width="100%" class="data">
									<xsl:for-each select="../Sections[KeyFile = $KeyFile]">
										<tr>
											<td align="right" class="botbord data defaultbold" nowrap=""> <xsl:if test="position() mod 2 = 0"> <xsl:attribute name="class">botbord defaultbold datashade</xsl:attribute> </xsl:if> <xsl:value-of select="position()"/>.</td>
											<td class="botbord data" align="left" width="100%"> <xsl:if test="position() mod 2 = 0"> <xsl:attribute name="class">botbord datashade</xsl:attribute> </xsl:if> <xsl:value-of select="Label"/></td>
										</tr>
									</xsl:for-each>
									</table>
								</xsl:if>
								</td>
							</tr>
							<tr>
								<td colspan="3" class="data"> <xsl:if test="count(../Sections[KeyFile = $KeyFile]) &lt; 5"> <xsl:attribute name="class">topbord data</xsl:attribute> </xsl:if> <xsl:if test="FileFormat = 'HTML'"> <xsl:if test="normalize-space(Edgar) = 'true'"> <span class="defaultbold">Note: </span><span class="default">This document was filed with the SEC in HTML format, as allowed by the recent EDGAR system modernization. SNL cannot take responsibility for its appearance, layout, or legibility.</span> </xsl:if> <xsl:if test="normalize-space(Edgar) = 'false'"> <xsl:if test="SectionCount > 1"> <span class="defaultbold">Note: </span><span class="default">Some or all of this document was retrieved from the filer directly in HTML format. SNL can only provide those parts in this format and cannot take responsibility for their appearance, layout, or legibility, or for the validity of links to images or to other pages on the filer's web site. </span> </xsl:if> <xsl:if test="SectionCount = 1"> <span class="defaultbold">Note: </span><span class="default">This document was retrieved from the filer directly in HTML format. SNL can provide it in this format only and cannot take responsibility for its appearance, layout, or legibility, or for the validity of links to images or to other pages on the filer's web site. </span> </xsl:if> </xsl:if> </xsl:if>&#160; </td>
							</tr>
						</table>
						<br/>
						</xsl:for-each>
					</td>
				</tr>
				<xsl:if test="Document/XBRLDoc = 'true'">
				<tr>
					<td>
						<table cellspacing="0" cellpadding="3" border="0" width="100%" class="data">
							<tr>
								<td class="surr datashade" align="left" valign="middle" colspan="3"> <img src="/images/interactive/ir/xbrl.gif" border="0" alt="XBRL" style="vertical-align:middle; padding-right:5px;"/> <span class="defaultbold">XBRL Options</span></td>							</tr>
							<tr class="data" align="left" valign="top">
								<td class="surrmid surright" colspan="3">Select the XBRL icon to open the full XBRL Filing in SNL's XBRL Reader.  Select the XML icon to download the raw xml feed, to open in another reader.</td>
							</tr>
							<tr class="data" align="left" valign="top">
								<td class="surrmid"><a><xsl:attribute name="href"> <xsl:value-of select="Document/XBRLRenderUrl"/> </xsl:attribute> <xsl:attribute name="target">_new</xsl:attribute> <img src="/images/interactive/ir/xbrl.gif" border="0" alt="XBRL" title="Open this Section in SNL Reader"/></a></td>
								<td class="botbord">1.</td>
								<td class="surrmid">Full XBRL Filing</td>
							</tr>
							<xsl:for-each select="XbrlSections">
							<tr class="data" align="left" valign="top">
							<xsl:if test="position() mod 2 = 0">
							<xsl:attribute name="class">datashade</xsl:attribute>
							</xsl:if>
								<td class="surrmid"> <a> <xsl:attribute name="href"> <xsl:value-of select="Url"/> </xsl:attribute> <xsl:attribute name="target">_new</xsl:attribute> <img src="/images/interactive/ir/xml.gif" border="0" alt="XML" title="Open this Section in XML Format"/> </a> </td>
								<td class="botbord defaultbold"> <xsl:value-of select="Number"/>. </td>
								<td class="surrmid" width="100%"> <xsl:value-of select="Label"/> </td>
							</tr>
							</xsl:for-each>
							<tr>
								<td align="left" colspan="3" class="surrmid data"> <span class="defaultbold">Note: </span> <span class="default">This document was filed with the SEC in XBRL format, as allowed by the recent EDGAR system modernization. SNL cannot take responsibility for its appearance, layout, or legibility.</span> </td>
							</tr>
						</table>
					</td>
				</tr>
				</xsl:if>
				<xsl:if test="count(DocumentTables) &gt; 0">
				<tr class="data" >
					<td class="data" align="left" valign="top">
						<table cellspacing="0" cellpadding="0" border="0" width="100%" class="table2">
							<tr>
								<td class="data" align="left" valign="top">
									<table cellspacing="0" cellpadding="3" border="0" width="100%" class="data">
										<tr>
											<td class="surrleft datashade"><img src="/images/interactive/ir/excel.gif" border="0"/> </td>
											<td class="surrright datashade" width="100%"><span class="defaultbold">Export Options</span></td>
										</tr>
									</table>
									<table cellspacing="0" cellpadding="3" border="0" width="50" align="left">
										<tr>
											<td align="left" class="data">Highlight the tables you wish to export then select the Export button.  You may select multiple tables by holding down the Ctrl key while making your selections.</td>
										</tr>
										<xsl:variable name="KeyInstn" select="Company/KeyInstn"/>
										<xsl:variable name="FormAction" select="concat('excelview.aspx?IID=', $KeyInstn)"></xsl:variable>
										<xsl:variable name="FirstKey" select="DocumentTables/Key" />
										<form name="frmDocTables" method="post">
										<xsl:attribute name="action"><xsl:value-of select="$FormAction"/></xsl:attribute>
										<tr>
											<td class="data" align="left" valign="top">
												<label class="visuallyhidden" for="DocTables">DocTables</label>
												<select id="DocTables" name="DocTables" MULTIPLE="yes" SIZE="5"> <xsl:for-each select="DocumentTables"> <option value="{Key}"> <xsl:if test="string(Key) = string($FirstKey)"> <xsl:attribute name="selected">selected</xsl:attribute> </xsl:if> <xsl:value-of select="ExtractedDocumentTable" disable-output-escaping="yes" /> </option> </xsl:for-each> </select> </td>
										</tr>
										<tr class="data">
											<td class="data" align="right" valign="bottom" nowrap="" height="40px"> <input type="submit" value="Export"/> </td>
										</tr>
										</form>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				</xsl:if>
				<xsl:if test="count(RelatedDocs) &gt; 0">
				<tr>
					<td>
						<table cellspacing="0" cellpadding="0" border="0" width="100%" class="topbord data">
							<tr>
								<td class="surrmid header" align="left" valign="top" colspan="2">Related Document(s)</td>
							</tr>
							<xsl:for-each select="RelatedDocs">
							<tr class="data" align="left" valign="top">
							<xsl:if test="position() mod 2 = 1">
							<xsl:attribute name="class">datashade</xsl:attribute>
							</xsl:if>
								<td class="surrmid data" align="left" valign="top"> <xsl:value-of select="Type"/> </td>
								<td class="rightBOTbord"> <a> <xsl:attribute name="href"> doc.aspx?IID=<xsl:value-of select="../Company/KeyInstn"/>&amp;DID=<xsl:value-of select="KeyDoc"/> </xsl:attribute> <xsl:attribute name="target">_new</xsl:attribute> View Document Details </a> </td>
							</tr>
							</xsl:for-each>
						</table>
					</td>
				</tr>
				</xsl:if>
				<tr>
					<td class="data" align="center"> <a> <xsl:attribute name="href">docs.aspx?iid=<xsl:value-of select="Company/KeyInstn"/> <!--javascript:history.back()--> </xsl:attribute> <b>View all documents</b> </a> </td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</xsl:template>
<!-- End  Template ONE -->

<!-- Template 3 -->
<xsl:template name="doc_detail3">
<table BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" class="table2">
	<tr ALIGN="CENTER">
		<td>
			<table BORDER="0" CELLSPACING="0" CELLPADDING="4" WIDTH="100%">
				<tr ALIGN="CENTER">
					<td class="data">
						<table BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
							<tr>
								<td class="data">
									<table BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" class="table2">
										<tr>
											<td colspan="2" class="table1_item datashade"><span class="defaultbold">Document ID <xsl:value-of select="Document/KeyDoc"/></span></td>
										</tr>
										<tr>
											<td class="data" nowrap="1"><span class="defaultbold">Filing Type</span> </td>
											<td class="data" width="100%"><xsl:if test="Document/DocTypeDescription != '(none)'"> <a class="fielddef"  title="{Document/FilingTypeLink}"> <xsl:attribute name="href"> <xsl:value-of select="Document/FilingTypeLink"/> </xsl:attribute> <xsl:value-of select="Document/DocTypeDescription"/> </a> </xsl:if> <xsl:if test="Document/DocTypeDescription = '(none)'"> <xsl:value-of select="Document/DocTypeDescription"/> </xsl:if> </td>
										</tr>
										<tr>
											<td class="data"><span class="defaultbold">Company</span> </td>
											<td class="data"><xsl:value-of select="Document/CityState" disable-output-escaping="yes"/> </td>
										</tr>
										<xsl:if test="Document/DocEventDate != ''">
										<tr>
											<td class="data"><span class="defaultbold">Event Date</span> </td>
											<td class="data"><xsl:value-of select="Document/DocEventDate"/> </td>
										</tr>
										</xsl:if>
										<xsl:if test="Document/DocFilingDate != ''">
										<tr>
											<td class="data"><span class="defaultbold">Filing Date</span> </td>
											<td class="data"><xsl:value-of select="Document/DocFilingDate"/> </td>
										</tr>
										</xsl:if>
										<xsl:if test="Document/DocAbstract != ''">
										<tr>
											<td VALIGN="top" class="data"><span class="defaultbold">Abstract</span> </td>
											<td class="data"><xsl:value-of select="Document/DocAbstract" disable-output-escaping="yes"/> </td>
										</tr>
										</xsl:if>
									</table>
								</td>
							</tr>
						</table>
						<br />
						<xsl:for-each select="KeyFiles">
						<table border="0" cellspacing="0" cellpadding="3" width="100%" class="table1">
							<tr>
								<td class="table1_item datashade"><span class="defaultbold">Download</span></td>
								<td class="table1_item datashade"><span class="defaultbold">File Sections</span></td>
							</tr>
							<xsl:variable name="KeyFile" select="KeyFile"/>
							<tr>
								<td class="table1_item divider data" valign="top" align="center" nowrap="">
								<xsl:if test="FileFormat != 'SGML' and FileFormat != 'HTML'">
									<table border="0" cellspacing="0" cellpadding="3" width="80">
										<tr>
											<td align="center" valign="top" class="data"  nowrap=""> <a> <xsl:if test="FileFormat != 'PDF'"> <xsl:attribute name="TARGET"> <xsl:value-of select="_new"/> </xsl:attribute> </xsl:if> <xsl:attribute name="href"> file.aspx?iid=<xsl:value-of select="../Company/KeyInstn"/>&amp;fid=<xsl:value-of select="$KeyFile"/>&amp;osid=9&amp;o=<xsl:value-of select="KeyFileFormat"/> </xsl:attribute> <img border="0"> <xsl:attribute name="src"> <xsl:if test="FileFormat = 'XML'">/images/interactive/ir/HTML.gif</xsl:if> <xsl:if test="FileFormat != 'XML'">/images/interactive/ir/<xsl:value-of select="FileFormat"/>.gif</xsl:if> </xsl:attribute> <xsl:attribute name="alt"> <xsl:value-of select="FileFormat"/> </xsl:attribute> </img> </a><br />(<xsl:value-of select="format-number(Size, '#.0')"/>K) </td>
											<xsl:if test="normalize-space(PDFConvertible) = 'true'">
											<td align="center" valign="top" class="data"> <img width="32px" height="32px" src="/images/interactive/blank.gif" border="0"/>&#160; </td>
											</xsl:if>
										</tr>
									</table>
								</xsl:if>
								<xsl:if test="FileFormat = 'SGML' or FileFormat = 'HTML'">
									<table border="0" cellspacing="0" cellpadding="3" width="80">
										<tr>
											<td align="center" valign="top" class="data"  nowrap=""> <a> <xsl:attribute name="href">file.aspx?IID=<xsl:value-of select="../Company/KeyInstn"/>&amp;FID=<xsl:value-of select='$KeyFile'/>&amp;O=2&amp;OSID=9 </xsl:attribute> <xsl:attribute name="target">_new</xsl:attribute> <img src="/images/interactive/ir/html.gif" border="0" alt="HTML"/> </a><br />(<xsl:value-of select="format-number(Size, '#.0')"/>K) </td>
											<xsl:if test="normalize-space(PDFConvertible) = 'true'">
											<td align="center" valign="top" class="data"> <a> <xsl:attribute name="href">file.aspx?IID=<xsl:value-of select="../Company/KeyInstn"/>&amp;FID=<xsl:value-of select='KeyFile'/>&amp;O=3&amp;OSID=9</xsl:attribute> <xsl:attribute name="target">_new</xsl:attribute> <img src="/images/interactive/ir/pdf.gif" border="0" alt="PDF"/> </a>&#160; </td>
											</xsl:if>
										</tr>
									</table>
								</xsl:if>
								</td>
								<td class="data" valign="top" style="padding:0px" width="100%">
								<xsl:if test="count(../Sections[KeyFile = $KeyFile]) &lt; 5">
								<xsl:attribute name="class">table1_item data</xsl:attribute>
								</xsl:if>
								<xsl:if test="SectionCount > 0">
									<table cellpadding="3" cellspacing="0" border="0" width="100%" class="data">
									<xsl:for-each select="../Sections[KeyFile = $KeyFile]">
										<tr>
											<td align="right" class="divider table1_item data defaultbold" nowrap=""> <xsl:if test="position() mod 2 = 0"> <xsl:attribute name="class">divider table1_item defaultbold datashade</xsl:attribute> </xsl:if> <xsl:value-of select="position()"/>.</td>
											<td class="table1_item data" align="left" width="100%">
											<xsl:if test="position() mod 2 = 0">
											<xsl:attribute name="class">table1_item datashade</xsl:attribute>
											</xsl:if>
											<xsl:value-of select="Label"/>
											</td>
										</tr>
									</xsl:for-each>
									</table>
								</xsl:if>
								</td>
							</tr>
							<xsl:if test="FileFormat = 'HTML'">
							<xsl:if test="normalize-space(Edgar) = 'true'">
							<tr>
								<td colspan="3" class="table1_item data"><span class="defaultbold">Note: </span><span class="default">This document was filed with the SEC in HTML format, as allowed by the recent EDGAR system modernization. SNL cannot take responsibility for its appearance, layout, or legibility.</span> </td>
							</tr>
							</xsl:if>
							<xsl:if test="normalize-space(Edgar) = 'false'">
							<tr>
								<td colspan="3" class="table1_item data"><xsl:if test="SectionCount > 1"> <span class="defaultbold">Note: </span><span class="default">Some or all of this document was retrieved from the filer directly in HTML format. SNL can only provide those parts in this format and cannot take responsibility for their appearance, layout, or legibility, or for the validity of links to images or to other pages on the filer's web site. </span> </xsl:if> <xsl:if test="SectionCount = 1"> <span class="defaultbold">Note: </span><span class="default">This document was retrieved from the filer directly in HTML format. SNL can provide it in this format only and cannot take responsibility for its appearance, layout, or legibility, or for the validity of links to images or to other pages on the filer's web site. </span> </xsl:if> </td>
							</tr>
							</xsl:if>
							</xsl:if>
						</table>
						<br/>
						</xsl:for-each>
					</td>
				</tr>
				<xsl:if test="Document/XBRLDoc = 'true'">
				<tr>
					<td>
						<table cellspacing="0" cellpadding="3" border="0" width="100%" class="table1">
							<tr>
								<td class="table1_item datashade" align="left" valign="middle" colspan="3"> <img src="/images/interactive/ir/xbrl.gif" border="0" alt="XBRL" style="vertical-align:middle; padding-right:5px;"/> <span class="defaultbold">XBRL Options</span></td>
							</tr>
							<tr class="data" align="left" valign="top">
								<td class="table1_item" colspan="3"> Select the XBRL icon to open the full XBRL Filing in SNL's XBRL Reader.  Select the XML icon to download the raw xml feed, to open in another reader. </td>
							</tr>
							<tr class="data" align="left" valign="top">
								<td class="table1_item divider"> <a> <xsl:attribute name="href"> <xsl:value-of select="Document/XBRLRenderUrl"/> </xsl:attribute> <xsl:attribute name="target">_new</xsl:attribute> <img src="/images/interactive/ir/xbrl.gif" border="0" alt="XBRL" title="Open this Section in SNL Reader"/> </a> </td>
								<td class="table1_item divider">1.</td>
								<td class="table1_item">Full XBRL Filing</td>
							</tr>
							<xsl:for-each select="XbrlSections">
							<tr class="data" align="left" valign="top">
							<xsl:if test="position() mod 2 = 0">
							<xsl:attribute name="class">datashade</xsl:attribute>
							</xsl:if>
								<td class="table1_item divider"> <a> <xsl:attribute name="href"> <xsl:value-of select="Url"/> </xsl:attribute> <xsl:attribute name="target">_new</xsl:attribute> <img src="/images/interactive/ir/xml.gif" border="0" alt="XML" title="Open this Section in XML Format"/> </a> </td>
								<td class="table1_item divider defaultbold"> <xsl:value-of select="Number"/>. </td>
								<td class="table1_item" width="100%"> <xsl:value-of select="Label"/> </td>
							</tr>
							</xsl:for-each>
							<tr>
								<td align="left" colspan="3" class="table1_item data"> <span class="defaultbold">Note: </span> <span class="default">This document was filed with the SEC in XBRL format, as allowed by the recent EDGAR system modernization. SNL cannot take responsibility for its appearance, layout, or legibility.</span> </td>
							</tr>
						</table>
					</td>
				</tr>
				</xsl:if>
				<xsl:if test="count(DocumentTables) &gt; 0">
				<tr class="data" >
					<td class="data" align="left" valign="top">
						<table cellspacing="0" cellpadding="0" border="0" width="100%" class="table2">
							<tr>
								<td class="data" align="left" valign="top">
									<table cellspacing="0" cellpadding="3" border="0" width="100%" class="data">
										<tr>
											<td class="table1_item datashade"><img src="/images/interactive/ir/excel.gif" border="0"/> </td>
											<td class="table1_item datashade" width="100%"><span class="defaultbold">Export Options</span></td>
										</tr>
									</table>
									<table cellspacing="0" cellpadding="3" border="0" width="50" align="left">
										<tr>
											<td align="left" class="data">Highlight the tables you wish to export then select the Export button.  You may select multiple tables by holding down the Ctrl key while making your selections.</td>
										</tr>
										<xsl:variable name="KeyInstn" select="Company/KeyInstn"/>
										<xsl:variable name="FormAction" select="concat('excelview.aspx?IID=', $KeyInstn)"></xsl:variable>
										<xsl:variable name="FirstKey" select="DocumentTables/Key" />
										<form name="frmDocTables" method="post">
										<xsl:attribute name="action"><xsl:value-of select="$FormAction"/></xsl:attribute>
										<tr>
											<td class="data" align="left" valign="top">
												<label class="visuallyhidden" for="DocTables">DocTables</label><select  id="DocTables" name="DocTables" MULTIPLE="yes" SIZE="5"> <xsl:for-each select="DocumentTables"> <option value="{Key}"> <xsl:if test="string(Key) = string($FirstKey)"> <xsl:attribute name="selected">selected</xsl:attribute> </xsl:if> <xsl:value-of select="ExtractedDocumentTable" disable-output-escaping="yes" /> </option> </xsl:for-each> </select> </td>
										</tr>
										<tr class="data">
											<td class="data" align="right" valign="bottom" nowrap="" height="40px"> <input type="submit" value="Export"/> </td>
										</tr>
										</form>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				</xsl:if>
				<xsl:if test="count(RelatedDocs) &gt; 0">
				<tr>
					<td>
						<table cellspacing="0" cellpadding="0" border="0" width="100%" class="data table1">
							<tr>
								<td class="table1_item header" align="left" valign="top" colspan="3">Related Document(s)</td>
							</tr>
							<xsl:for-each select="RelatedDocs">
							<tr class="data" align="left" valign="top">
							<xsl:if test="position() mod 2 = 1">
							<xsl:attribute name="class">datashade</xsl:attribute>
							</xsl:if>
								<td class="table1_item divider data" align="left" valign="top"> <xsl:value-of select="Type"/> </td>
								<td  class="table1_item data"> <a> <xsl:attribute name="href"> doc.aspx?IID=<xsl:value-of select="../Company/KeyInstn"/>&amp;DID=<xsl:value-of select="KeyDoc"/> </xsl:attribute> <xsl:attribute name="target">_new</xsl:attribute> View Document Details </a> </td>
							</tr>
							</xsl:for-each>
						</table>
					</td>
				</tr>
				</xsl:if>
				<tr>
					<td class="data" align="center"> <a> <xsl:attribute name="href">docs.aspx?iid=<xsl:value-of select="Company/KeyInstn"/> </xsl:attribute> <b>View all documents</b> </a> </td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</xsl:template>
<!-- Template THREE Ends here. -->

<!-- Main XSLT templates are here. these are the templates which are actually being displayed in the page. this is for TOP LEVEL editing. if you dont need to use the individual templates above you can do your main editing here when pulling THESE templates below you must carry the top comment with them and uncomment them when you drop it into the company specific xslt.-->



<xsl:template match="irw:Doc">
<xsl:variable name="TemplateName" select="Company/TemplateName"/>
<xsl:choose>
<!-- Template 2 -->
<xsl:when test="$TemplateName = 'AltDoc1'">
<xsl:call-template name="TemplateONEstylesheet" />
<SCRIPT LANGUAGE="JavaScript">
function openWindow(url,name,widgets) {
popupWin = window.open (url,name,widgets);
popupWin.opener.top.name="opener";
popupWin.focus();
}
</SCRIPT>
<xsl:choose>
<xsl:when test="count(Document) > 0">
<xsl:call-template name="doc_detail2"/>
</xsl:when>
<xsl:otherwise>
<table BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" class="">
	<xsl:call-template name="Title_S2"/>
	<tr	ALIGN="CENTER">
		<td	class="" colspan="2">
			<table BORDER="0" CELLSPACING="0" CELLPADDING="4" WIDTH="100%">
				<tr ALIGN="CENTER">
					<td class="leftTOPbord">
						<table BORDER="0" CELLSPACING="0" CELLPADDING="4" WIDTH="100%">
							<tr>
								<td class="data" nowrap="1"> <span class="defaultbold">No data available for this document</span> </td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</xsl:otherwise>
</xsl:choose>
<xsl:if test="Company/Footer != ''">
<table border="0" cellpadding="3" cellspacing="0" width="100%">
	<tr class="default" align="left">
		<td class="data"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span></td>
	</tr>
</table>
</xsl:if>
<xsl:call-template name="Copyright"/>
</xsl:when>
<!-- End Template 2 -->

<!-- Template 3 -->
<xsl:when test="$TemplateName = 'AltDoc3'">
<xsl:call-template name="TemplateTHREEstylesheet" />
<SCRIPT LANGUAGE="JavaScript">
function openWindow(url,name,widgets) {
popupWin = window.open (url,name,widgets);
popupWin.opener.top.name="opener";
popupWin.focus();
}
</SCRIPT>
<xsl:call-template name="Title_T3"/>
<xsl:choose>
<xsl:when test="count(Document) > 0">
<xsl:call-template name="doc_detail3"/>
</xsl:when>
<xsl:otherwise>
<table BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%" class="table2">
	<tr>
		<td>
			<table BORDER="0" CELLSPACING="0" CELLPADDING="4" WIDTH="100%">
				<tr>
					<td class="data" nowrap="1"> <span class="defaultbold">No data available for this document</span> </td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</xsl:otherwise>
</xsl:choose>
<xsl:if test="Company/Footer != ''">
<table border="0" cellpadding="3" cellspacing="0" width="100%">
	<tr class="default" align="left">
		<td class="data"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span></td>
	</tr>
</table>
</xsl:if>
<xsl:call-template name="Copyright"/>
</xsl:when>
<!-- End Template 3 -->
  
<!-- Template 4 -->
  <xsl:when test="$TemplateName = 'AltDoc4'">    
    <xsl:call-template name="Doc_4" />
  </xsl:when>
<!-- End Template 4 -->
<xsl:otherwise>

<SCRIPT LANGUAGE="JavaScript">
function openWindow(url,name,widgets) {
popupWin = window.open (url,name,widgets);
popupWin.opener.top.name="opener";
popupWin.focus();
}
</SCRIPT>
<xsl:choose>
<xsl:when test="count(Document) > 0">
<xsl:call-template name="doc_detail"/>
</xsl:when>
<xsl:otherwise>
<table BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" class="colordark">
	<xsl:call-template name="Title_S1"/>
	<tr	ALIGN="CENTER">
		<td class="colordark" colspan="2">
			<table BORDER="0" CELLSPACING="0" CELLPADDING="4" WIDTH="100%">
				<tr ALIGN="CENTER">
					<td class="colorlight">
						<table BORDER="0" CELLSPACING="0" CELLPADDING="4" WIDTH="100%">
							<tr>
								<td class="data" nowrap="1"> <span class="defaultbold">No data available for this document</span> </td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</xsl:otherwise>
</xsl:choose>
</xsl:otherwise>
</xsl:choose>
</xsl:template>

  <!-- Doc Template Style 4 -->
  <xsl:template name="Doc_4">
    <xsl:call-template name="DocScriptResource_4"/>
    <div id="outer">
      <div id="Docs">
        <xsl:call-template name="Toolkit_Section_4"/>
        <div id="DocSection">
          <xsl:call-template name="Title_Head_4"/>
          <xsl:call-template name="Header_4"/>
          <xsl:call-template name="DocDetails_4"/>
          <xsl:call-template name="Footer_4"/>
          <xsl:call-template name="Copyright_4"/>
        </div>			
      </div>		
    </div>
  </xsl:template>
  
  <xsl:template name="DocScriptResource_4">
    <xsl:call-template name="CommonScript_4" />
    <xsl:call-template name="CommonDocScript_4" />
    <xsl:call-template name="OverLibmws_4" />
    
	<script language="javascript">
	<![CDATA[
	function openWindow(url,name,widgets) {
		var FilingType = "]]><xsl:value-of select="Document/DocTypeDescription"/><![CDATA[";		
		return overlib(OLiframeContent(url, 450, 300, 'if2', 0, 'auto'),WIDTH,450, TEXTPADDING,0, BORDER,1, STICKY, DRAGGABLE, CLOSECLICK, CAPTIONPADDING,4, CAPTION,'Type: '+FilingType+'',MIDX,0, MIDY,0,STATUS,'S&P Global Market Intelligence ');
	}
	]]>
	</script>
	
  </xsl:template>

  <xsl:template name="DocDetails_4">
	<xsl:choose>
	<xsl:when test="count(Document) > 0">
		<xsl:call-template name="DocDocumentDetails_4"/>
		<xsl:call-template name="DocDownload_4"/>
		<xsl:call-template name="DocXBRL_4"/>
		<xsl:call-template name="DocExport_4"/>
		<xsl:call-template name="DocRelatedDoc_4"/>
		<xsl:call-template name="DocFooter_4"/>
	</xsl:when>
	<xsl:otherwise>
		<div id="Doc_Details" class="NoData">
			No data available for this document
		</div> 
	</xsl:otherwise>
	</xsl:choose>
  </xsl:template>
  
  <xsl:template name="DocDocumentDetails_4">
	<div id="Doc_Details">
	  <table border="0" cellspacing="0" cellpadding="0" width="100%" id="DocsSelectBox">
	<tr>
		<td valign="top">
			<table border="0" cellspacing="0" cellpadding="0" width="100%" class="head_table">
				<tr>
					<td colspan="2" class="bold"><span class="bold DocDocumentId">Document ID <xsl:value-of select="Document/KeyDoc"/></span></td>
				</tr>
				<tr>
					<td width="70px" valign="top" nowrap="1">Filing Type</td>
					<td>
						<xsl:if test="Document/DocTypeDescription != '(none)'">
							<a href="javascript:void(0);" class="box" onclick="{Document/FilingTypeLink}"   title="{Document/DocTypeDescription}">
								<xsl:value-of select="Document/DocTypeDescription"/>
							</a>
						</xsl:if>
						<xsl:if test="Document/DocTypeDescription = '(none)'">
							<xsl:value-of select="Document/DocTypeDescription"/>
						</xsl:if>
					</td>
				</tr>
				<tr>
					<td valign="top" nowrap="1">Company</td>
					<td><xsl:value-of select="Document/CityState" disable-output-escaping="yes"/> </td>
				</tr>
				<xsl:if test="Document/DocEventDate != ''">
				<tr>
					<td valign="top" nowrap="1">Event Date</td>
					<td><xsl:value-of select="Document/DocEventDate"/> </td>
				</tr>
				</xsl:if>
				<xsl:if test="Document/DocFilingDate != ''">
				<tr>
					<td valign="top" nowrap="1">Filing Date</td>
					<td><xsl:value-of select="Document/DocFilingDate"/> </td>
				</tr>
				</xsl:if>
				<xsl:if test="Document/DocAbstract != ''">
				<tr>
					<td valign="top" nowrap="1">Abstract</td>
					<td><xsl:value-of select="Document/DocAbstract" disable-output-escaping="yes"/> </td>
				</tr>
				</xsl:if>
			</table>
		</td>
	</tr>
	</table>
	</div>
  </xsl:template>
  
  <xsl:template name="DocDownload_4">
	<div id="Doc_Download">
	<table border="0" cellspacing="0" cellpadding="0" width="100%" id="DocsSelectBox">
	<tr>
		<td valign="top">
			<xsl:for-each select="KeyFiles">
				<table cellspacing="0" cellpadding="0" border="0" width="100%" class="head_table">
				<tr>
					<td class="bold"><span class="bold DocDownload">Download</span></td>
					<td class="bold"><span class="bold DocFileSections">File Sections</span></td>
				</tr>
				<xsl:variable name="KeyFile" select="KeyFile"/>
				<tr>
					<td valign="top" align="center" nowrap="">
						<xsl:if test="FileFormat != 'SGML' and FileFormat != 'HTML'">
							<table border="0" cellspacing="0" cellpadding="3" width="80">
							<tr>
								<td align="center" valign="top" nowrap=""> <a> <xsl:if test="FileFormat != 'PDF'"> <xsl:attribute name="TARGET"> <xsl:value-of select="_new"/> </xsl:attribute> </xsl:if> <xsl:attribute name="href"> file.aspx?iid=<xsl:value-of select="../Company/KeyInstn"/>&amp;fid=<xsl:value-of select="$KeyFile"/>&amp;osid=9&amp;o=<xsl:value-of select="KeyFileFormat"/> </xsl:attribute> <img border="0"> <xsl:attribute name="src"> <xsl:if test="FileFormat = 'XML'">/images/interactive/ir/HTML.gif</xsl:if> <xsl:if test="FileFormat != 'XML'">/images/interactive/ir/<xsl:value-of select="FileFormat"/>.gif</xsl:if> </xsl:attribute> <xsl:attribute name="alt"> <xsl:value-of select="FileFormat"/> </xsl:attribute> </img> </a><br />(<xsl:value-of select="format-number(Size, '#.0')"/>K) </td>
								<xsl:if test="normalize-space(PDFConvertible) = 'true'">
									<td align="center" valign="top" class="data"> <img width="32px" height="32px" src="/images/interactive/blank.gif" border="0"/>&#160; </td>
								</xsl:if>
							</tr>
							</table>
						</xsl:if>
						
						<xsl:if test="FileFormat = 'SGML' or FileFormat = 'HTML'">
							<table border="0" cellspacing="0" cellpadding="0" width="80">
							<tr>
								<td align="center" valign="top" class="data"  nowrap=""> <a> <xsl:attribute name="href">file.aspx?IID=<xsl:value-of select="../Company/KeyInstn"/>&amp;FID=<xsl:value-of select='$KeyFile'/>&amp;O=2&amp;OSID=9 </xsl:attribute> <xsl:attribute name="target">_new</xsl:attribute> <img src="/images/interactive/ir/html.gif" border="0" alt="HTML"/> </a><br />(<xsl:value-of select="format-number(Size, '#.0')"/>K) </td>
								<xsl:if test="normalize-space(PDFConvertible) = 'true'">
									<td align="center" valign="top" class="data"> <a> <xsl:attribute name="href">file.aspx?IID=<xsl:value-of select="../Company/KeyInstn"/>&amp;FID=<xsl:value-of select='KeyFile'/>&amp;O=3&amp;OSID=9</xsl:attribute> <xsl:attribute name="target">_new</xsl:attribute> <img src="/images/interactive/ir/pdf.gif" border="0" alt="PDF"/> </a>&#160; </td>
								</xsl:if>
							</tr>
							</table>
						</xsl:if>
					</td>
					<td class="bord_left" valign="top" style="padding:0px" width="100%">
						<xsl:if test="SectionCount > 0">
							<table cellpadding="0" cellspacing="0" border="0" width="100%" id="altdatashade">
							<tbody rel="altdatashade">
								<xsl:for-each select="../Sections[KeyFile = $KeyFile]">
								<tr>
									<td align="right" width="15px" nowrap="1"><strong><xsl:value-of select="position()"/>.</strong></td>
									<td align="left"><xsl:value-of select="Label"/></td>
								</tr>
								</xsl:for-each>
							</tbody>
							</table>
						</xsl:if>
					</td>
				</tr>
				</table>
				<xsl:if test="FileFormat = 'HTML'">
					<xsl:if test="normalize-space(Edgar) = 'true'">
						<div class="Doc_note"><strong>Note: </strong>This document was filed with the SEC in HTML format, as allowed by the recent EDGAR system modernization. SNL cannot take responsibility for its appearance, layout, or legibility.</div>
					</xsl:if>
					<xsl:if test="normalize-space(Edgar) = 'false'">
						<xsl:if test="SectionCount > 1"> 
							<div class="Doc_note"><strong>Note: </strong>Some or all of this document was retrieved from the filer directly in HTML format. SNL can only provide those parts in this format and cannot take responsibility for their appearance, layout, or legibility, or for the validity of links to images or to other pages on the filer's web site.</div>
						</xsl:if> 
						<xsl:if test="SectionCount = 1">
							<div class="Doc_note"><strong>Note: </strong>This document was retrieved from the filer directly in HTML format. SNL can provide it in this format only and cannot take responsibility for its appearance, layout, or legibility, or for the validity of links to images or to other pages on the filer's web site.</div>
						</xsl:if>
					</xsl:if>
				</xsl:if>				
			</xsl:for-each>
		</td>
	</tr>
	</table>
	</div>
  </xsl:template>
  
  <xsl:template name="DocXBRL_4">
	<xsl:if test="Document/XBRLDoc = 'true'">
	<div id="Doc_XBRL">
	<table border="0" cellspacing="0" cellpadding="0" width="100%" id="DocsSelectBox">
	<tr>
		<td valign="top">
			<table cellspacing="0" cellpadding="0" border="0" width="100%" class="head_table">
			<tr>
				<td class="bold" width="100%"><span class="bold doc_XBRL">&#160;</span><span class="bold DocXBRLOptions">XBRL Options</span></td>
			</tr>
			<tr>
				<td valign="top" class="bord_bottom">
					<div class="Doc_note_txt">Select the XBRL icon to open the full XBRL Filing in SNL's XBRL Reader.  Select the XML icon to download the raw xml feed, to open in another reader. </div>
				</td>
			</tr>
			<tr>
				<td valign="top" class="padding_none">
					<table cellpadding="0" cellspacing="0" border="0" width="100%" id="altdatashade">
					<tbody rel="altdatashade">
					<tr>
						<td width="40px" align="center"> <a><xsl:attribute name="href"> <xsl:value-of select="Document/XBRLRenderUrl"/> </xsl:attribute> <xsl:attribute name="target">_new</xsl:attribute> <img src="/images/interactive/ir/xbrl.gif" border="0" alt="XBRL" title="Open this Section in SNL Reader"/> </a> </td>
						<td class="bord_left" align="right" width="15px" nowrap="1"><strong>1.</strong></td>
						<td>Full XBRL Filing</td>
					</tr>
					<xsl:for-each select="XbrlSections">
						<tr>				
							<td align="center"> <a> <xsl:attribute name="href"> <xsl:value-of select="Url"/> </xsl:attribute> <xsl:attribute name="target">_new</xsl:attribute> <img src="/images/interactive/ir/xml.gif" border="0" alt="XML" title="Open this Section in XML Format"/> </a> </td>
							<td class="bord_left" align="right"><strong><xsl:value-of select="Number"/>. </strong></td>
							<td align="left"> <xsl:value-of select="Label"/> </td>
						</tr>
						</xsl:for-each>
					</tbody>
					</table>
				</td>
			</tr>
			</table>
			<div class="Doc_note"><strong>Note: </strong>This document was filed with the SEC in XBRL format, as allowed by the recent EDGAR system modernization. SNL cannot take responsibility for its appearance, layout, or legibility.</div>
		</td>
	</tr>
	</table>
	</div>
	</xsl:if>
  </xsl:template>
  
  <xsl:template name="DocExport_4">
	<xsl:if test="count(DocumentTables) &gt; 0">
	<div id="Doc_Export">
	<table border="0" cellspacing="0" cellpadding="0" width="100%" id="DocsSelectBox">
	<tr>
		<td align="left" valign="top">
			
			<!----><table cellspacing="0" cellpadding="0" border="0" width="100%" class="head_table">
			<tr>
				<td class="bold" width="100%"><span class="bold doc_excel">&#160;</span><span class="bold DocExportOptions">Export Options</span></td>
			</tr>
			<tr>
				<td>
					<div class="Doc_note_txt">
						Highlight the tables you wish to export then select the Export button.  You may select multiple tables by holding down the Ctrl key while making your selections.
					</div>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<xsl:variable name="KeyInstn" select="Company/KeyInstn"/>
					<xsl:variable name="FormAction" select="concat('excelview.aspx?IID=', $KeyInstn)"></xsl:variable>
					<xsl:variable name="FirstKey" select="DocumentTables/Key" />
					<form name="frmDocTables" method="post"><xsl:attribute name="action"><xsl:value-of select="$FormAction"/></xsl:attribute>
					<div id="selectbox">
						<label class="visuallyhidden" for="DocTables">DocTables</label>
						<select name="DocTables" class="selectbox" multiple="yes" size="5" id="DocTables">
						<!--<option>Export Options</option>-->
							<xsl:for-each select="DocumentTables">
							<option value="{Key}"><xsl:if test="string(Key) = string($FirstKey)"> <xsl:attribute name="selected">selected</xsl:attribute> </xsl:if> <xsl:value-of select="ExtractedDocumentTable" disable-output-escaping="yes" /> </option> 
							</xsl:for-each>
						</select>
					</div>
					<div id="Export_but"><input type="submit" name="Export" id="Export" value="Export" /></div>
					</form>
				</td>
			</tr>
			</table><!---->
		</td>
	</tr>
	</table>
	</div>
	</xsl:if>	
  </xsl:template>
  
  <xsl:template name="DocRelatedDoc_4">
	<xsl:if test="count(RelatedDocs) &gt; 0">
	<div id="Doc_RelatedDoc">
	<table border="0" cellspacing="0" cellpadding="0" width="100%" id="DocsSelectBox">
	<tr>
		<td valign="top">
			<table cellspacing="0" cellpadding="0" border="0" width="100%" class="head_table" id="altdatashade">
			<tr>
				<td class="bold" align="left" valign="top" colspan="3"><span class="bold DocRelatedDocument">Related Document(s)</span></td>
			</tr>
			<tbody rel="altdatashade">
			<xsl:for-each select="RelatedDocs">
				<tr>
					<td align="left" valign="top"> <xsl:value-of select="Type"/> </td>
					<td> <a> <xsl:attribute name="href"> doc.aspx?IID=<xsl:value-of select="../Company/KeyInstn"/>&amp;DID=<xsl:value-of select="KeyDoc"/> </xsl:attribute> <xsl:attribute name="target">_new</xsl:attribute> View Document Details </a> </td>
				</tr>
			</xsl:for-each>
			</tbody>
			</table>
		</td>
	</tr>
	</table>
	</div>
	</xsl:if>	
  </xsl:template>
  
  <xsl:template name="DocFooter_4">
	<div id="Doc_Footer" align="center">
		<a href="docs.aspx?iid={Company/KeyInstn}" class="DocViewAllDocuments" title="View all documents">View all documents</a>
	</div>
  </xsl:template>

  <!-- End Docs Template Style 4 -->
</xsl:stylesheet>

