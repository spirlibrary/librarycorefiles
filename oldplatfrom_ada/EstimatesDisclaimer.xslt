<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.ord/TR/REC-html40"
	xmlns:irw="http://www.snl.com/xml/irw">

<xsl:output method="html" media-type="text/html" indent="no"/>

<xsl:template match="irw:Disclaimer">

<xsl:variable name="TemplateName" select="Company/TemplateName"/>
<xsl:choose>
<!-- Template 1 -->
<xsl:when test="$TemplateName = 'AltCover1'">
<xsl:call-template name="TemplateONEstylesheet" />
<xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" CLASS="">
  <xsl:call-template name="Title_S2" />
  <TR class="default" align="left">
    <TD class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Header" disable-output-escaping="yes"/></span></TD>
	</TR>
	<TR ALIGN="CENTER">
		<TD CLASS="leftTOPbord" colspan="2">
			<TABLE border="0" cellspacing="0" cellpadding="2"  width="100%">
				<tr>
					<td class="colorlight"><br />
						<span class="default"><xsl:value-of select="Disclaimer/DisclaimerText" disable-output-escaping="yes"/></span>
					<br /><br /></td>
				</tr>
				<tr>
					<td	align="center" class="colorlight" colspan="4">
						<a title="Continue">
						<xsl:variable name="keyinstn" select="Disclaimer/KeyInstn" />
						<xsl:variable name="url" select="concat('AnalystCoverage.aspx?IID=', $keyinstn)" />
						<xsl:attribute name="href">
							<xsl:value-of select="$url" />
						</xsl:attribute>
						Continue</a><br /><br />
					</td>	
				</tr>
			</TABLE>
		</TD>
	</TR>
</TABLE>
</xsl:when>
<!-- End Template 1 -->

<!--  Template 3 -->
<xsl:when test="$TemplateName = 'AltCover3'">
<xsl:call-template name="TemplateTHREEstylesheet" />
<xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>

<xsl:call-template name="Title_T3" />
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" CLASS="table2">	
	<tr>
		<td align="center" class="colorlight" colspan="4">
			<TABLE border="0" cellspacing="0" cellpadding="2" class="data" width="100%">
				<tr>
					<td class="colorlight"><br /><span class="default"><xsl:value-of select="Disclaimer/DisclaimerText" disable-output-escaping="yes"/></span><br /><br /></td>
				</tr>
				<tr>
					<td	align="center" class="colorlight" colspan="4">
						<a title="Continue">
						<xsl:variable name="keyinstn" select="Disclaimer/KeyInstn" />
						<xsl:variable name="url" select="concat('AnalystCoverage.aspx?IID=', $keyinstn)" />
						<xsl:attribute name="href">
						<xsl:value-of select="$url" />
						</xsl:attribute>
						Continue</a><br /><br />
					</td>	
				</tr>
			</TABLE>
		</td>
	</tr>
</TABLE>
</xsl:when>
<!-- End Template 3 -->





<!-- Template Default -->
<xsl:otherwise>


<xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>

<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" CLASS="colordark">
  <!-- <TR>
		<TD CLASS="colordark" nowrap="1">
			<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
			<SPAN CLASS="title1light">Estimates Disclaimer</SPAN>
	    </TD>
		<TD CLASS="colordark" nowrap="1" align="right" valign="top">
			<span class="subtitlelight"><xsl:value-of select="$CompanyNameCaps" /> (<xsl:value-of select="Company/Exchange"/> - <xsl:value-of select="Company/Ticker"/>)</span>
			<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
		</TD>
	</TR> -->
  <xsl:call-template name="Title_S1" />
	<TR ALIGN="CENTER">
		<TD CLASS="colordark" colspan="2">
			<TABLE border="0" cellspacing="0" cellpadding="2" class="data" width="100%">
				<tr>
					<td class="colorlight"><br />
						<span class="default"><xsl:value-of select="Disclaimer/DisclaimerText" disable-output-escaping="yes"/></span>
					<br /><br /></td>
				</tr>
				<tr>
					<td	align="center" class="colorlight" colspan="4">
						<a title="Continue">
						<xsl:variable name="keyinstn" select="Disclaimer/KeyInstn" />
						<xsl:variable name="url" select="concat('AnalystCoverage.aspx?IID=', $keyinstn)" />
						<xsl:attribute name="href">
							<xsl:value-of select="$url" />
						</xsl:attribute>
						Continue</a><br /><br />
					</td>	
				</tr>
			</TABLE>
		</TD>
	</TR>
</TABLE>
</xsl:otherwise>


</xsl:choose>



<xsl:call-template name="Copyright"/>

</xsl:template>

</xsl:stylesheet>