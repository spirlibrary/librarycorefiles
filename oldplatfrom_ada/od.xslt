<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.ord/TR/REC-html40"
	xmlns:irw="http://www.snl.com/xml/irw">

<xsl:output method="html" media-type="text/html" indent="no"/>
<xsl:output method="html" media-type="text/html" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"/>

<!-- SP 93129 [RC: 03/02/2010] Description: when we expand any data Above Expand All text doesnt change to Collapse All.  please fix it. -->

 <!-- SP 93044 [RC: 02/24/2010] Description: 
I noticed that if i expand a few items, and then click the "Expand All" link at the top, it actually closes the few items I've already expanded. I would expect that this should expand the remaining items i've not yet opened.
Similarly, if I expand all items the text of the link changes to "Collapse All", but if i then collapse all the items manually, the text remains "Collapse All", but if I click on that link it actually expands all the items.
The text of the hyperlink should always correspond to the action it will perform if a user clicks on it at that moment.
 -->
 
<!-- SP 92888 [RC: 02/20/2010] Description: 
Officers and Directors page has a new feature where the click through contents (i.e. the Bio.aspx page contents) are now available on the OD.aspx page -  
Clicking an officers name explands the section and displays the bio info - However, after the first few clicks the functionality shows some strange blinking on collapse - 
Can this be fixed?
 -->

<!-- SP 92861 [RC: 02/20/2010] Description: Please add expand all/collapse all links to the new OD.aspx version. thanks -->

<!-- Start Default Template here-->
<!--
//This is the default template. Most changes are to either remove the company name, ticker, or exchange. you can also change the name of the page or remove this row all together. 
//These templates below are broken up how the page is displayed.
-->

<xsl:template name="HEADER_OD">
<xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>

	<tr>
		<td class="colordark" nowrap="1"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text><SPAN class="title1light"><xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/></SPAN></td>
		<td class="colordark" nowrap="1" align="right" valign="top"><span class="subtitlelight"><xsl:value-of select="$CompanyNameCaps"/>&#160;(<xsl:value-of select="Company/Exchange"/>&#160;-&#160;<xsl:value-of select="Company/Ticker"/>)</span><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
	</tr>
	<tr class="default" align="left">
		<td class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Header" disable-output-escaping="yes"/></span></td>
	</tr>
</xsl:template>


<xsl:template name="Print_Officers">
<xsl:param name="SName"/>
<xsl:for-each select="../Members">
<xsl:if test="DisplayOrder mod 2=1 and $SName=SectionName">
<xsl:call-template name="List_Officers"><xsl:with-param name="SName"><xsl:value-of select="$SName" disable-output-escaping="yes"/></xsl:with-param></xsl:call-template>
</xsl:if>
</xsl:for-each>
</xsl:template>



<xsl:template name="List_Officers">	
<xsl:param name="SName"/>	 
<tr>
	<td class="data" valign="top" width="50%"> 			
		<xsl:if test="HasBio='true'">		
			<div class="technology" id="ir_{KeyPerson}">
				<xsl:if test="string-length(normalize-space(OnlineImageFilePath)) > 0"><img width="70" border="0" src="{OnlineImageFilePath}" alt="{PersonName}" /><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text>
				</xsl:if>
				<span class="defaultbold"><xsl:value-of select="PersonName" disable-output-escaping="yes"/></span><br />
				<span class="default"><xsl:value-of select="Title" disable-output-escaping="yes"/></span>
			</div>
			<div class="thelanguage" id="ir{KeyPerson}" style="display:none"><br />
				<table border="0" cellpadding="3" cellspacing="0" width="100%" class="data">
				<tr>
					<td align="left" valign="top" class="data bioimg">
						<xsl:value-of select="PersonBio" disable-output-escaping="yes"/>
					</td>
				</tr>
				</table><br />
			</div> 	
		</xsl:if>
		<xsl:if test="HasBio='false'">
			<div class="technology" style="background: none;cursor:default">
        <xsl:if test="string-length(normalize-space(OnlineImageFilePath)) > 0">
          <img width="70" border="0" src="{OnlineImageFilePath}" alt="{Title}" />
          <xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text>
        </xsl:if>
				<span class="defaultbold"><xsl:value-of select="PersonName" disable-output-escaping="yes"/></span><br /> 
				<span class="default"><xsl:value-of select="Title" disable-output-escaping="yes"/></span>
			</div>
		</xsl:if>  
	</td>
	<td valign="top" class="data" width="50%"> 
	<xsl:if test="normalize-space(following-sibling::node())">
			<xsl:variable name="sImage" select="following-sibling::node()/OnlineImageFilePath"/>
			<xsl:variable name="hasImage" select="following-sibling::node()/HasImage"/>
			<xsl:variable name="displayOrder" select="following-sibling::node()/DisplayOrder"/>
			<xsl:variable name="person" select="following-sibling::node()/KeyPerson" />		
			<xsl:variable name="hasBio" select="following-sibling::node()/HasBio"/>
			<xsl:variable name="personName" select="following-sibling::node()/PersonName"/>
			<xsl:variable name="title" select="following-sibling::node()/Title"/>
			
			<xsl:if test="(string-length($hasBio)=4 or string-length($hasBio)=5) and $displayOrder mod 2=0 and $SName=SectionName">
				<xsl:if test="string-length($hasBio)=4 and $displayOrder mod 2=0 and $SName=SectionName">
					<div class="technology" id="ir_{$person}">
					<xsl:if test="string-length($hasImage)=4 and string-length(normalize-space($sImage)) > 0 and $displayOrder mod 2=0 and $SName=SectionName">
						<img width="70" border="0" alt="{$sImage}"><xsl:attribute name="src"><xsl:value-of select="$sImage" disable-output-escaping="yes"/></xsl:attribute></img><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text>
					</xsl:if>		
						<span class="defaultbold"><xsl:value-of select="$personName" disable-output-escaping="yes"/></span><br />
						<span class="default"><xsl:value-of select="$title" disable-output-escaping="yes"/></span>
					</div>
					<div class="thelanguage" id="ir{$person}" style="display:none"><br />
						<table border="0" cellpadding="3" cellspacing="0" width="100%" class="data">
						<tr>
							<td align="left" valign="top" class="data bioimg">
								<xsl:value-of select="following-sibling::node()/PersonBio" disable-output-escaping="yes"/>
							</td>
						</tr>
						</table><br />
					</div> 
				</xsl:if>		
				<xsl:if test="string-length($hasBio)=5  and $displayOrder mod 2=0 and $SName=SectionName">
					<div class="technology" style="background: none;cursor:default">
            <xsl:if test="string-length($hasImage)=4 and string-length(normalize-space($sImage)) > 0 and $displayOrder mod 2=0 and $SName=SectionName">
              <img width="70" border="0" alt="{$sImage}">
                <xsl:attribute name="src">
                  <xsl:value-of select="$sImage" disable-output-escaping="yes"/>
                </xsl:attribute>
              </img>
              <xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text>
            </xsl:if>
            <span class="defaultbold"><xsl:value-of select="$personName" disable-output-escaping="yes"/></span><br />
						<span class="default"><xsl:value-of select="$title" disable-output-escaping="yes"/></span>
					</div>
				</xsl:if>
			</xsl:if>
	</xsl:if> 
	<xsl:if test="not(normalize-space(following-sibling::node()))">&#160;</xsl:if> 
	</td>	
</tr>
</xsl:template>


<xsl:template name="OD_click">	
<tr><td class="colorlight" colspan="2"><span class="default">Click on a person's name or image to view his or her biography. </span></td></tr>
<tr valign="bottom">
	<td colspan="2" class="colorlight" align="right" valign="bottom"><a class="toggleShowHideAll" href="javascript:;" title="toggleShowHideAll"></a></td>
</tr>
</xsl:template>

<!-- Default Template Ends here. all templates below are ONLY if you are using the 'Style Template' section in the console. -->


<!-- This is Template ONE option in the console under 'Style Template'
//This template is a striped down version of the main template. Everything (data) is displayed in single rows.
//The sections will be commented on where the rows are. you should be able to just move rows (entire chunks of data) where you need too. Edits to this section are rare, mostly due to size (width of the page).
-->

	<xsl:template name="HEADER_OD2">
<xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
<tr>
<td class=" titletest" nowrap="" colspan="2"><xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/><br /><IMG SRC="" width="2" HEIGHT="1" alt="{$CompanyNameCaps}" /><span class=" titletest2"><xsl:value-of select="$CompanyNameCaps"/>&#160;(<xsl:value-of select="Company/Exchange"/>&#160;-&#160;<xsl:value-of select="Company/Ticker"/>)&#160;</span></td>

</tr>
	<tr class="default" align="left">
		<td class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Header" disable-output-escaping="yes"/></span></td>
	</tr>
</xsl:template>
	
	

<xsl:template name="Print_Officers2">
<xsl:param name="SName"/>
<xsl:for-each select="../Members">
<xsl:if test="$SName=SectionName"> 
	<xsl:call-template name="List_Officers2"><xsl:with-param name="SName"><xsl:value-of select="$SName" disable-output-escaping="yes"/></xsl:with-param></xsl:call-template> 
</xsl:if>
</xsl:for-each>
</xsl:template>
	

<xsl:template name="List_Officers2">	
<xsl:param name="SName"/>	
<tr>
	<td valign="top" class="data"> 		
		<xsl:if test="HasBio='true'">
			<div class="technology" id="ir_{KeyPerson}">
				<xsl:if test="string-length(normalize-space(OnlineImageFilePath)) > 0"><img width="70" border="0" src="{OnlineImageFilePath}" alt="{PersonName}" /><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text>
				</xsl:if>			
				<span class="defaultbold"><xsl:value-of select="PersonName" disable-output-escaping="yes"/></span><br />
				<span class="default"><xsl:value-of select="Title" disable-output-escaping="yes"/></span>
			</div>		
			<div class="thelanguage" id="ir{KeyPerson}" style="display:none"><br />
				<table border="0" cellpadding="3" cellspacing="0" width="100%" class="data">
				<tr>
					<td align="left" valign="top" class="data bioimg">
						<xsl:value-of select="PersonBio" disable-output-escaping="yes"/>
					</td>
				</tr>
				</table><br />
			</div>
		</xsl:if>    
		
		<xsl:if test="HasBio='false'">
			<div class="technology" style="background: none;cursor:default">
        <xsl:if test="string-length(normalize-space(OnlineImageFilePath)) > 0">
          <img width="70" border="0" src="{OnlineImageFilePath}" alt="{PersonName}" />
          <xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text>
        </xsl:if>
        <span class="defaultbold"><xsl:value-of select="PersonName" disable-output-escaping="yes"/></span><br />
				<span class="default"><xsl:value-of select="Title" disable-output-escaping="yes"/></span>
			</div>
		</xsl:if>    
	</td>	 
</tr>
</xsl:template>

<xsl:template name="OD_click2">	
<tr><td colspan="2" class="data" align="left">Click on a person's name or image to view his or her biography. </td></tr>
<tr valign="bottom">
	<td colspan="2" class="data" align="right" valign="bottom"><a class="toggleShowHideAll" href="javascript:;" title="toggleShowHideAll"></a>
	</td>
</tr>
</xsl:template>
<!-- Template ONE Ends here. -->


<!-- Start Template THREE -->
<xsl:template name="HEADER_OD3">
<xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
<table border="0" cellspacing="0" cellpadding="3" width="100%" class="table1">
<tr class="header">
<td><xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/><IMG SRC="" width="2" HEIGHT="1" alt="{$CompanyNameCaps}" /></td>
<td align="right"><xsl:value-of select="$CompanyNameCaps"/>&#160;(<xsl:value-of select="Company/Exchange"/>&#160;-&#160;<xsl:value-of select="Company/Ticker"/>)&#160;
<span class=""><xsl:value-of select="Company/Header" disable-output-escaping="yes"/></span></td></tr></table>
</xsl:template>


<xsl:template name="Print_Officers3">
<xsl:param name="SName"/>
<xsl:for-each select="../Members">
<xsl:if test="DisplayOrder mod 2=1 and $SName=SectionName">
	<xsl:call-template name="List_Officers3">
	<xsl:with-param name="SName"><xsl:value-of select="$SName" disable-output-escaping="yes"/></xsl:with-param>  
	</xsl:call-template>
</xsl:if>
</xsl:for-each>
</xsl:template>



<xsl:template name="List_Officers3">	
	<xsl:param name="SName"/>	 
	<tr>
		<td class="data table1_item" valign="top" width="50%">
			<xsl:if test="HasBio='true'">		
				<div class="technology" id="ir_{KeyPerson}">
					<xsl:if test="string-length(normalize-space(OnlineImageFilePath)) > 0"><img width="70" border="0" src="{OnlineImageFilePath}" alt="{PersonName}" /><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text>
					</xsl:if>
					<span class="defaultbold"><xsl:value-of select="PersonName" disable-output-escaping="yes"/></span><br />
					<span class="default"><xsl:value-of select="Title" disable-output-escaping="yes"/></span>
				</div>
				<div class="thelanguage" id="ir{KeyPerson}" style="display:none"><br />
					<table border="0" cellpadding="3" cellspacing="0" width="100%" class="data">
					<tr>
						<td align="left" valign="top" class="data bioimg">
							<xsl:value-of select="PersonBio" disable-output-escaping="yes"/>
						</td>
					</tr>
					</table><br />
				</div> 	
			</xsl:if>
			<xsl:if test="HasBio='false'">	
				<div class="technology" style="background: none;cursor:default">
			<xsl:if test="string-length(normalize-space(OnlineImageFilePath)) > 0">
			  <img width="70" border="0" src="{OnlineImageFilePath}" alt="{PersonName}" />
			  <xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text>
			</xsl:if>
					<span class="defaultbold"><xsl:value-of select="PersonName" disable-output-escaping="yes"/></span><br /> 
					<span class="default"><xsl:value-of select="Title" disable-output-escaping="yes"/></span>
				</div>
			</xsl:if>  
		</td>
		<td class="data table1_item" valign="top" width="50%"> 
		<xsl:if test="normalize-space(following-sibling::node())">
				<xsl:variable name="sImage" select="following-sibling::node()/OnlineImageFilePath"/>
				<xsl:variable name="hasImage" select="following-sibling::node()/HasImage"/>
				<xsl:variable name="displayOrder" select="following-sibling::node()/DisplayOrder"/>
				<xsl:variable name="person" select="following-sibling::node()/KeyPerson" />		
				<xsl:variable name="hasBio" select="following-sibling::node()/HasBio"/>
				<xsl:variable name="personName" select="following-sibling::node()/PersonName"/>
				<xsl:variable name="title" select="following-sibling::node()/Title"/>
				
				<xsl:if test="(string-length($hasBio)=4 or string-length($hasBio)=5) and $displayOrder mod 2=0 and $SName=SectionName">
					<xsl:if test="string-length($hasBio)=4 and $displayOrder mod 2=0 and $SName=SectionName">
						<div class="technology" id="ir_{$person}">
						<xsl:if test="string-length($hasImage)=4 and string-length(normalize-space($sImage)) > 0 and $displayOrder mod 2=0 and $SName=SectionName">
							<img width="70" border="0" alt="{$personName}"><xsl:attribute name="src"><xsl:value-of select="$sImage" disable-output-escaping="yes"/></xsl:attribute></img><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text>
						</xsl:if>		
							<span class="defaultbold"><xsl:value-of select="$personName" disable-output-escaping="yes"/></span><br />
							<span class="default"><xsl:value-of select="$title" disable-output-escaping="yes"/></span>
						</div>
						<div class="thelanguage" id="ir{$person}" style="display:none"><br />
							<table border="0" cellpadding="3" cellspacing="0" width="100%" class="data">
							<tr>
								<td align="left" valign="top" class="data bioimg">
									<xsl:value-of select="following-sibling::node()/PersonBio" disable-output-escaping="yes"/>
								</td>
							</tr>
							</table><br />
						</div> 
					</xsl:if>		
					<xsl:if test="string-length($hasBio)=5  and $displayOrder mod 2=0 and $SName=SectionName">
						<div class="technology" style="background: none;cursor:default">
				<xsl:if test="string-length($hasImage)=4 and string-length(normalize-space($sImage)) > 0 and $displayOrder mod 2=0 and $SName=SectionName">
				  <img width="70" border="0" alt="{$sImage}">
					<xsl:attribute name="src">
					  <xsl:value-of select="$sImage" disable-output-escaping="yes"/>
					</xsl:attribute>
				  </img>
				  <xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text>
				</xsl:if>
				<span class="defaultbold"><xsl:value-of select="$personName" disable-output-escaping="yes"/></span><br />
							<span class="default"><xsl:value-of select="$title" disable-output-escaping="yes"/></span>
						</div>
					</xsl:if>
				</xsl:if>
		</xsl:if> 
		<!-- content to blank cells, which allows the bottom border class to be applied. Do not remove. RC -->
		<xsl:if test="not(normalize-space(following-sibling::node())) or normalize-space(following-sibling::node()/DisplayOrder) =1">&#160;</xsl:if>
		</td>	
	</tr>	
	</xsl:template>


<xsl:template name="OD_click3">	
<table border="0" cellspacing="0" cellpadding="4" width="100%" id="table2">		
<tr><td class="data">Click on a person's name or image to view his or her biography. </td></tr>
<tr valign="bottom">
	<td class="data" align="right" valign="bottom"><a class="toggleShowHideAll" href="javascript:;" title="toggleShowHideAll"></a>
	</td>
</tr>
</table>
	
</xsl:template>

<!-- Template THRE Ends here -->
  
<!-- Common Template for adding javascript file start here -->

  <xsl:template name="od_script">
 <xsl:variable name="MemberCount" select="count(Members)"/>
<!-- Javascript for displaying Sho/Hide Data Efeects -->
<style>
 .bioimg  img{ float:left; margin: 0 5px;}
</style>
<script src="javascript/jquery-1.4.1.min.js"></script>
<link REL="stylesheet" HREF="javascript/committee/JqueryStyle.css" TYPE="text/css"/>
<script type="text/javascript">
<![CDATA[
$(document).ready(function() {		
	var Expand='Expand All';
	var Collapse='Collapse All';
	var CollExpand = '';	
	var divName = $('div.technology').next('div.thelanguage');	
	var divthelang = $('div.thelanguage'); 	
	$('.toggleShowHideAll').text(Expand)
	CollExpand = Expand;  
	inc = divthelang.length;
  if ( inc < 1){    	    
    	$('.toggleShowHideAll').hide();
  }	
	$('div.thelanguage').hide();
	$('div.technology').addClass('irToggleOpen');/* hide only Bio data*/	
	
	$('div.technology').click(
		function()
		{			
			var answer = $(this).next('div.thelanguage');
			if (answer.is(':visible')) {
				answer.slideUp();				
				$(this).removeClass('irToggleClose');
				$(this).addClass('irToggleOpen');
				inc = inc + 1;
				if ( inc == divthelang.length ){
					$('.toggleShowHideAll').text(Expand);
					CollExpand = Expand;
					inc = divthelang.length;
				} 
			} 
			else 
			{
				answer.slideDown();					
				$(this).removeClass('irToggleOpen');
				$(this).addClass('irToggleClose');
				inc = inc - 1;
				if ( inc == 0 ){
					$('.toggleShowHideAll').text(Collapse);
					CollExpand = Collapse;
					inc = 0; 
				}  
			} 
		}
	);
	$('.toggleShowHideAll').toggle(
		function()
		{	  
			if(CollExpand == 'Expand All')
			{
				$('.thelanguage').slideDown();
				$('.toggleShowHideAll').text(Collapse);
				$('div.technology').removeClass('irToggleOpen');
				$('div.technology').addClass('irToggleClose');
				CollExpand = Collapse;
				inc = 0;
			}  
			else 
			{ 
				$('.thelanguage').slideUp();	
				$('.toggleShowHideAll').text(Expand); 
				$('div.technology').removeClass('irToggleClose');
				$('div.technology').addClass('irToggleOpen');
				CollExpand = Expand;
				inc = divthelang.length;
			} 
		},
		function()
		{  
			if(CollExpand == 'Collapse All')
			{ 
				$('.thelanguage').slideUp();	
				$('.toggleShowHideAll').text(Expand); 
				$('div.technology').removeClass('irToggleClose');
				$('div.technology').addClass('irToggleOpen');
				CollExpand = Expand;
				inc = divthelang.length;
			} 
			else 
			{ 
				$('.thelanguage').slideDown();
				$('.toggleShowHideAll').text(Collapse);
				$('div.technology').removeClass('irToggleOpen');
				$('div.technology').addClass('irToggleClose');
				CollExpand = Collapse;
				inc = 0;
			} 
		}
	);	
});
]]>
</script>
</xsl:template>
  <!-- Common Template for adding javascript file end here -->

  <!-- Main XSLT templates are here. these are the templates which are actually being displayed in the page. this is for TOP LEVEL editing. if you dont need to use the individual templates above you can do your main editing here when pulling THESE templates below you must carry the top comment with them and uncomment them when you drop it into the company specific xslt.
	   
	   
-->


<xsl:template match="irw:OD">
 <xsl:call-template name="od_script"/>
<xsl:variable name="TemplateName" select="Company/TemplateName"/>
<xsl:choose>
<!-- Template 1 -->
<xsl:when test="$TemplateName = 'AltOd1'">
<xsl:call-template name="TemplateONEstylesheet" />
<table border="0" cellspacing="0" cellpadding="3" width="100%" class="">
<xsl:call-template name="Title_S2"/>
<xsl:call-template name="OD_click2"/>
	<tr>
		<td class="leftTOPbord data" colspan="2" align="left"> 
			<table border="0" cellspacing="0" cellpadding="4" width="100%" id="Table1">
				<tr> 
					<td class="data" valign="top"> 
						<div id="thelanguage_od">
						<xsl:for-each select="Sections">
						<table border="0" cellspacing="0" cellpadding="4" width="100%" id="Table2">
							<tr valign="bottom">
								<td class="surr datashade" valign="bottom"><span class="defaultbold"><xsl:value-of select="SectionName" disable-output-escaping="yes"/></span></td>
							</tr>
							 <xsl:call-template name="Print_Officers2">
							  <xsl:with-param name="SName"><xsl:value-of select="SectionName" disable-output-escaping="yes"/></xsl:with-param>
			 		 		</xsl:call-template>
							<tr>
								<td height="1" class="data">
									<table width="100%" cellpadding="0" cellspacing="0" height="1">
										<tr>
											<td class="colordark" height="1" ></td>
										</tr>
									</table> 
								</td>
							</tr>
							<tr valign="top">
								<td class="data">&#160;</td>
							</tr>    
	 	 	    			</table>
						</xsl:for-each>
						</div>
    					</td>
  				</tr>
			</table>     
 		</td>
  	</tr>
	<xsl:if test="Company/Footer != ''">
  	<tr>
		<td colspan="2" class="data" align="left"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span></td>
  	</tr>
	</xsl:if>
</table>  
<xsl:call-template name="Copyright"/>
</xsl:when>
<!-- End Template 1 -->

<!-- Template 3 -->
<xsl:when test="$TemplateName = 'AltOd3'">
<xsl:call-template name="TemplateTHREEstylesheet" />

<xsl:call-template name="Title_T3"/>
<table border="0" cellspacing="0" cellpadding="3" width="100%" class="table2">
<tr> 
	<td class="data" valign="top">
		<xsl:call-template name="OD_click3"/>
		<div id="thelanguage_od">
		<xsl:for-each select="Sections">
		<table border="0" cellspacing="0" cellpadding="4" width="100%" id="Table2" class="table1">
			<tr valign="bottom">
				<td class="datashade table1_item" colspan="2" valign="bottom"><span class="defaultbold"><xsl:value-of select="SectionName" disable-output-escaping="yes"/></span></td>
			</tr>
			<xsl:call-template name="Print_Officers3">
				<xsl:with-param name="SName"><xsl:value-of select="SectionName" disable-output-escaping="yes"/></xsl:with-param>
			</xsl:call-template>
		</table><br/>
		</xsl:for-each>
		</div>
	</td>
</tr>
<tr align="left">
	<td valign="top" class="data"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span></td>
</tr>
</table>  
<xsl:call-template name="Copyright"/>
</xsl:when>
<!-- End Template 3 -->

<!-- Template Default -->
<xsl:otherwise>
	<table border="0" cellspacing="0" cellpadding="3" width="100%" class="colordark">
	<xsl:call-template name="Title_S1"/>
	<tr>
		<td class="colordark" colspan="2"> 
			<table border="0" cellspacing="0" cellpadding="4" width="100%" id="Table1">
			<tr> 
				<td class="colorlight"> 
					<div id="thelanguage_od">
					<table border="0" cellspacing="0" cellpadding="4" width="100%" id="Table2">
					<xsl:call-template name="OD_click"/>
					<xsl:for-each select="Sections">
						<tr valign="bottom"><td class="header" colspan="2" valign="bottom"><xsl:value-of select="SectionName" disable-output-escaping="yes"/></td></tr>
						<xsl:call-template name="Print_Officers">
							<xsl:with-param name="SName">
								<xsl:value-of select="SectionName" disable-output-escaping="yes"/>
							</xsl:with-param>
						</xsl:call-template>
						<tr valign="top"><td class="colorlight" colspan="2">&#160;</td></tr>    
					</xsl:for-each>
					</table>
					</div>
				</td>
			</tr>
			</table>     
		</td>
	</tr>
	<xsl:if test="Company/Footer != ''">
	<tr align="left">
		<td colspan="2" class="colorlight"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span></td>
	</tr>
	</xsl:if>
	</table>
	<xsl:call-template name="Copyright"/>
</xsl:otherwise>
</xsl:choose>
</xsl:template>

</xsl:stylesheet>