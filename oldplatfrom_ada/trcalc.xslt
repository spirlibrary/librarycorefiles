<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.ord/TR/REC-html40"
	xmlns:irw="http://www.snl.com/xml/irw">

<xsl:output method="html" media-type="text/html" indent="no"/>
<xsl:decimal-format name="data" NaN="NA"/>
<!-- Start Default Template here-->

<!--
//This is the default template for the total return calculator page. Most changes are to either remove the company name, ticker, or exchange. you can also change the name of the page or remove this row all together.
//These templates below are broken up how the page is displayed.

-->
<!--  Start Default Template -->
<xsl:template name="trc_data">
<table border="0" cellpadding="2" cellspacing="0" width="100%" class="colorlight">
	<tr>
		<td class="defaultbold" align="left">For a total return percentage, enter date range and amount invested for a desired frequency:</td>
	</tr>

	<xsl:variable name="keyinstn" select="Company/KeyInstn"/>
	<xsl:variable name="formaction" select="concat('trcalc.aspx?IID=', $keyinstn)">

	</xsl:variable>
	<form method="POST" id="form1" name="form1">
	<xsl:attribute name="action">
	<xsl:value-of select="$formaction"/>
	</xsl:attribute>
	<tr>
		<td align="center" valign="top">
			<table border="0" cellpadding="2" cellspacing="0" width="100%">
				<tr>
					<td colspan="2"><img src="images/blank.gif" width="1" height="10" alt="layout background" /><br />
						<table border="0" cellpadding="2" cellspacing="0" width="100%">

						<xsl:choose>
						<xsl:when test="TradedIssues/RowCount > 1">
							<tr>
								<td colspan="3">&#160;</td>
								<td class="defaultbold" align="right">Security :<img src="images/blank.gif" width="5" height="1" alt="layout background" /></td>
								<td>
								<xsl:variable name="CurrentKeyFndg" select="FormData/Security"/>
									<label class="visuallyhidden" for="KeyFndg">Keyfunding</label>
								<select NAME="KeyFndg" ID="KeyFndg" SIZE="1" class="input">
								<xsl:for-each select="TradedIssues">
								<xsl:value-of select="KeyFndg"/>
								<option><xsl:attribute name="value">
								<xsl:value-of select="KeyFndg"/>
								</xsl:attribute>
								<xsl:if test="$CurrentKeyFndg=KeyFndg"><xsl:attribute name="SELECTED">1</xsl:attribute></xsl:if>
								<xsl:value-of select="TradingSymbol"/>
								</option>
								</xsl:for-each>
								</select>
								</td>
							</tr>
							</xsl:when>
							<xsl:otherwise>
							</xsl:otherwise>
							</xsl:choose>
							<xsl:variable name="startdate" select="FormData/TRStartDate"/>
							<xsl:variable name="enddate" select="FormData/TREndDate"/>
							<tr>
								<td class="defaultbold" nowrap="">Date Range</td>
								<td valign="top" align="left">
									<label class="visuallyhidden" for="TRStartDate"> Start Date</label>
									<input class="input" type="text" name="TRStartDate" id="TRStartDate" size="10">
									<xsl:attribute name="value">
									<xsl:value-of select="$startdate"/>
									</xsl:attribute>
									</input>
								</td>
								<td class="defaultbold" align="center">to</td>
								<td valign="top" align="left">
									<label class="visuallyhidden" for="TREndDate"> End Date</label>
									<input class="input" type="text" name="TREndDate" id="TREndDate" size="10">
									<xsl:attribute name="value">
									<xsl:value-of select="$enddate"/>
									</xsl:attribute>
									</input>
								</td>
								<td>&#160;</td>
							</tr>
							<tr>
								<td colspan="5"><img src="images/blank.gif" width="1" height="10" alt="layout background" /></td>
							</tr>

							<tr>
								<td class="defaultbold">Frequency</td>
								<td>
									<label class="visuallyhidden" for="afrequency">Frequency</label>
									<select id="afrequency" name="Frequency" class="input">
									<xsl:choose>
									<xsl:when test="FormData/Frequency='Daily'">
									<option VALUE="Daily" SELECTED="1">Daily</option>
									</xsl:when>
									<xsl:otherwise>
									<option VALUE="Daily">Daily</option>
									</xsl:otherwise>
									</xsl:choose>
									<xsl:choose>
									<xsl:when test="FormData/Frequency='Weekly'">
									<option VALUE="Weekly" SELECTED="1" >Weekly</option>
									</xsl:when>
									<xsl:otherwise>
									<option VALUE="Weekly">Weekly</option>
									</xsl:otherwise>
									</xsl:choose>
									<xsl:choose>
									<xsl:when test="FormData/Frequency='Monthly'">
									<option VALUE="Monthly" SELECTED="1" >Monthly</option>
									</xsl:when>
									<xsl:otherwise>
									<option VALUE="Monthly">Monthly</option>
									</xsl:otherwise>
									</xsl:choose>
									<xsl:choose>
									<xsl:when test="FormData/Frequency='Quarterly'">
									<option VALUE="Quarterly" SELECTED="1" >Quarterly</option>
									</xsl:when>
									<xsl:otherwise>
									<option VALUE="Quarterly">Quarterly</option>
									</xsl:otherwise>
									</xsl:choose>
									<xsl:choose>
									<xsl:when test="FormData/Frequency='Yearly'">
									<option VALUE="Yearly" SELECTED="1" >Yearly</option>
									</xsl:when>
									<xsl:otherwise>
									<option VALUE="Yearly">Yearly</option>
									</xsl:otherwise>
									</xsl:choose>
									</select>
								</td>
								<td class="defaultbold" align="center" nowrap="">Amount<br />Invested:</td>
								<td>
									<label class="visuallyhidden" for="AmountInvested">Amount Invested</label><INPUT class="input" type="text" id="AmountInvested" name="Amount" size="10"><xsl:attribute name="value"><xsl:if test="number(FormData/AmountInvested) > 0"><xsl:value-of select="FormData/AmountInvested"/></xsl:if></xsl:attribute></INPUT></td>
								<td align="left"><input class="submit" type="submit" name="Submit" value="Submit" /></td>
							</tr>
						</table>
					</td>
				</tr>
				</table>
			</td>
		</tr>
		</form>

		<tr>
			<td align="left" colspan="2">
				<span class="subscript_bold"><xsl:value-of disable-output-escaping="yes" select="FormData/ErrorMsg"/></span>
			</td>
		</tr>
		<tr>
			<td class="default"><span class="defaultbold">Consult Your Tax Advisor:</span> This information does not constitute tax advice. It does not purport to be complete or to describe the consequences that may apply to particular categories of shareholders. You should consult a tax advisor regarding the calculation of your tax basis.<br /></td>
		</tr>
		<tr>
			<td><xsl:call-template name="trc_output" /></td>
		</tr>
</table>
</xsl:template>


<xsl:template name="trc_output">
<xsl:if test="Returns/Price != ''">
<br/>
<table border="0" cellpadding="2" cellspacing="0" width="100%" >
	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="2" cellspacing="0" width="400">
				<tr>
					<td class="default">Current Value <span class="subscript">(dividends reinvested)</span>:</td>
					<td><img src="images/blank.gif" width="150" height="1" alt="layout background"/></td>
					<td class="data" nowrap="1">$ <xsl:value-of select="TRCalc/CurrentValue" /></td>
				</tr>
				<tr>
					<td class="default">Share Price Change:</td>
					<td>&#160;</td>
					<td class="data" nowrap="1"><xsl:value-of select="TRCalc/Change" /> %</td>
				</tr>
				<tr>
					<td class="default">Total Return <span class="subscript">(dividends reinvested)</span>:</td>
					<td>&#160;</td>
					<td class="data" nowrap="1"><xsl:value-of select="TRCalc/TotalReturn" /> %</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td><br />
			<table class="colordark" cellpadding="0" cellspacing="0" height="1" width="100%">
				<tr>
					<td><img src="images/blank.gif" width="1" height="1" alt="layout background"/></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="2" cellspacing="0" width="400">
				<tr>
					<td class="defaultbold" align="center">
					<xsl:value-of select="Table1/Ticker" />&#160;Total Return Performance
					 <br />
					 <span class="subscript">Holding Period&#160;(<xsl:value-of select="FormData/TRStartDate" /> - <xsl:value-of select="FormData/TREndDate" />) </span></td>
				</tr>
				<tr>
					<td align="center">
					<img>
						<xsl:attribute name="src">
							<xsl:value-of select="Table1/ImageURL"/>
						</xsl:attribute>
					</img>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="2" cellspacing="0" width="400">
				<tr>
					<td class="defaultbold">Pricing Date</td>
					<td><img src="images/blank.gif" width="150" height="1" alt="layout background"/></td>
					<td class="defaultbold"><xsl:value-of select="Table1/Ticker" /></td>
				</tr>
				<xsl:for-each select="Returns">
				<tr>
					<td class="data"><xsl:value-of select="Date2"/></td>
					<td><img src="images/blank.gif" width="150" height="1" alt="layout background"/></td>
					<td class="data"><xsl:value-of select="format-number(Return,'#,##0.00 ;(#,##0.00)', 'data')"/></td>
				</tr>
				</xsl:for-each>
			</table>
		</td>
	</tr>
</table>
</xsl:if>

</xsl:template>

<!-- End Default -->
<!-- Default Template Ends here. all templates below are ONLY if you are using the 'Style Template' section in the console. -->



<!-- Template 1 -->
<xsl:template name="trc_data1">
<table border="0" cellpadding="2" cellspacing="0" width="100%" class="colorlight">
	<tr>
		<td class="defaultbold" align="left">For a total return percentage, enter date range and amount invested for a desired frequency:</td>
	</tr>
	<xsl:variable name="keyinstn" select="Company/KeyInstn"/>
	<!--<xsl:variable name="formaction" select="concat('trcalc.aspx?IID=', $keyinstn, '&amp;xml=1')">-->
	<xsl:variable name="formaction" select="concat('trcalc.aspx?IID=', $keyinstn)"></xsl:variable>
	<form method="POST" id="form1" name="form1">
	<xsl:attribute name="action">
	<xsl:value-of select="$formaction"/>
	</xsl:attribute>
	<tr>
		<td align="center" valign="top">
			<table border="0" cellpadding="2" cellspacing="0" width="100%">
				<tr>
					<td colspan="2"><img src="images/blank.gif" width="1" height="10" alt="layout background"/><br />
						<table border="0" cellpadding="2" cellspacing="0" width="100%">
						<xsl:choose>
						<xsl:when test="TradedIssues/RowCount > 1">
							<tr>
								<td colspan="3">&#160;</td>
								<td class="defaultbold" align="right">Security :<img src="images/blank.gif" width="5" height="1" alt="layout background"/></td>
								<td>
								<xsl:variable name="CurrentKeyFndg" select="FormData/Security"/>
									<label class="visuallyhidden" for="KeyFndg">Keyfunding</label>
								<select NAME="KeyFndg" ID="KeyFndg" SIZE="1" class="input">
								<xsl:for-each select="TradedIssues">
								<xsl:value-of select="KeyFndg"/>
								<option><xsl:attribute name="value">
								<xsl:value-of select="KeyFndg"/>
								</xsl:attribute>
								<xsl:if test="$CurrentKeyFndg=KeyFndg"><xsl:attribute name="SELECTED">1</xsl:attribute></xsl:if>
								<xsl:value-of select="TradingSymbol"/>
								</option>
								</xsl:for-each>
								</select>
								</td>
							</tr>
							</xsl:when>
							<xsl:otherwise>

							</xsl:otherwise>
							</xsl:choose>
							<xsl:variable name="startdate" select="FormData/TRStartDate"/>
							<xsl:variable name="enddate" select="FormData/TREndDate"/>
							<tr>
								<td class="defaultbold" nowrap="">Date Range</td>
								<td valign="top" align="left">
									<label class="visuallyhidden" for="TRStartDate">Start Date</label>
									<input type="text" name="TRStartDate" id="TRStartDate" size="10" class="input">
									<xsl:attribute name="value">
									<xsl:value-of select="$startdate"/>
									</xsl:attribute>
									</input>
								</td>
								<td class="defaultbold" align="center">to</td>
								<td valign="top" align="left">
									<label class="visuallyhidden" for="TREndDate">End Date</label>
									<input type="text" name="TREndDate" id="TREndDate" size="10" class="input">
									<xsl:attribute name="value">
									<xsl:value-of select="$enddate"/>
									</xsl:attribute>
									</input>
								</td>
								<td>&#160;</td>
							</tr>
							<tr>
								<td colspan="5"><img src="images/blank.gif" width="1" height="10" alt="layout background"/></td>
							</tr>

							<tr>
								<td class="defaultbold">Frequency</td>
								<td>
									<label class="visuallyhidden" for="afrequency">Frequency</label>
									<select id="afrequency" name="Frequency" class="input">
									<xsl:choose>
									<xsl:when test="FormData/Frequency='Daily'">
									<option VALUE="Daily" SELECTED="1">Daily</option>
									</xsl:when>
									<xsl:otherwise>
									<option VALUE="Daily">Daily</option>
									</xsl:otherwise>
									</xsl:choose>
									<xsl:choose>
									<xsl:when test="FormData/Frequency='Weekly'">
									<option VALUE="Weekly" SELECTED="1" >Weekly</option>
									</xsl:when>
									<xsl:otherwise>
									<option VALUE="Weekly">Weekly</option>
									</xsl:otherwise>
									</xsl:choose>
									<xsl:choose>
									<xsl:when test="FormData/Frequency='Monthly'">
									<option VALUE="Monthly" SELECTED="1" >Monthly</option>
									</xsl:when>
									<xsl:otherwise>
									<option VALUE="Monthly">Monthly</option>
									</xsl:otherwise>
									</xsl:choose>
									<xsl:choose>
									<xsl:when test="FormData/Frequency='Quarterly'">
									<option VALUE="Quarterly" SELECTED="1" >Quarterly</option>
									</xsl:when>
									<xsl:otherwise>
									<option VALUE="Quarterly">Quarterly</option>
									</xsl:otherwise>
									</xsl:choose>
									<xsl:choose>
									<xsl:when test="FormData/Frequency='Yearly'">
									<option VALUE="Yearly" SELECTED="1" >Yearly</option>
									</xsl:when>
									<xsl:otherwise>
									<option VALUE="Yearly">Yearly</option>
									</xsl:otherwise>
									</xsl:choose>
									</select>
								</td>
								<td class="defaultbold" align="center" nowrap="">Amount<br />Invested:</td>
								<td>
									<label class="visuallyhidden" for="AmountInvested">Amount Invested</label><INPUT type="text" id="AmountInvested" name="Amount" size="10"  class="input"><xsl:attribute name="value"><xsl:if test="number(FormData/AmountInvested) > 0"><xsl:value-of select="FormData/AmountInvested"/></xsl:if></xsl:attribute></INPUT></td>
								<td align="left"><input type="submit" name="Submit" value="Submit" class="submit"/></td>
							</tr>
						</table>
					</td>
				</tr>
				</table>
			</td>
		</tr>
		</form>

		<tr>
			<td align="left" colspan="2">
				<span class="subscript_bold"><xsl:value-of disable-output-escaping="yes" select="FormData/ErrorMsg"/></span>
			</td>
		</tr>
		<tr>
			<td class="default"><span class="defaultbold">Consult Your Tax Advisor:</span> This information does not constitute tax advice. It does not purport to be complete or to describe the consequences that may apply to particular categories of shareholders. You should consult a tax advisor regarding the calculation of your tax basis.<br /></td>
		</tr>
		<tr>
			<td><xsl:call-template name="trc_output1" /></td>
		</tr>
</table>
</xsl:template>


<xsl:template name="trc_output1">
<xsl:if test="Returns/Price != ''">
<br/>
<table border="0" cellpadding="2" cellspacing="0" width="100%" >
	<tr>
		<td valign="top" align="center">
						<!--	<xsl:variable name="bprice" select="the begining price thats returned"/>
							<xsl:variable name="treturn" select="the return"/>		-->
			<table border="0" cellpadding="2" cellspacing="0" width="400">
				<tr>
					<td class="default">Current Value <span class="subscript">(dividends reinvested)</span>:</td>
					<td><img src="images/blank.gif" width="150" height="1" alt="layout background"/></td>
					<td class="data" nowrap="1">$ <xsl:value-of select="TRCalc/CurrentValue" /></td>
				</tr>
				<tr>
					<td class="default">Share Price Change:</td>
					<td>&#160;</td>
					<td class="data" nowrap="1"><xsl:value-of select="TRCalc/Change" /> %</td>
				</tr>
				<tr>
					<td class="default">Total Return <span class="subscript">(dividends reinvested)</span>:</td>
					<td>&#160;</td>
					<td class="data" nowrap="1"><xsl:value-of select="TRCalc/TotalReturn" /> %</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td><br />
			<table class="colordark" cellpadding="0" cellspacing="0" height="1" width="100%">
				<tr>
					<td><img src="images/blank.gif" width="1" height="1" alt="layout background"/></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="2" cellspacing="0" width="400">
				<tr>
					<td class="defaultbold" align="center">
					<xsl:value-of select="Table1/Ticker" />&#160;Total Return Performance
					 <br />
					 <span class="subscript">Holding Period&#160;(<xsl:value-of select="FormData/TRStartDate" /> - <xsl:value-of select="FormData/TREndDate" />) </span></td>
				</tr>
				<tr>
					<td align="center">
					<img>
						<xsl:attribute name="src">
							<xsl:value-of select="Table1/ImageURL"/>
						</xsl:attribute>
					</img>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="2" cellspacing="0" width="400">
				<tr>
					<td class="defaultbold">Pricing Date</td>
					<td><img src="images/blank.gif" width="150" height="1" alt="layout background" /></td>
					<td class="defaultbold"><xsl:value-of select="Table1/Ticker" /></td>
				</tr>
				<xsl:for-each select="Returns">
				<tr>
					<td class="data"><xsl:value-of select="Date2"/></td>
					<td><img src="images/blank.gif" width="150" height="1" alt="layout background"/></td>
					<td class="data"><xsl:value-of select="format-number(Return,'#,##0.00 ;(#,##0.00)', 'data')"/></td>
				</tr>
				</xsl:for-each>
			</table>
		</td>
	</tr>
</table>
</xsl:if>

</xsl:template>
<!-- End Template 1 -->





<!-- This is Template THREE option in the console under 'Style Template' -->
<!-- Template 3 -->
<xsl:template name="trc_data3">
<table border="0" cellpadding="2" cellspacing="0" width="100%" class="colorlight">
	<tr>
		<td class="defaultbold" align="left">For a total return percentage, enter date range and amount invested for a desired frequency:</td>
	</tr>

	<xsl:variable name="keyinstn" select="Company/KeyInstn"/>
	<xsl:variable name="formaction" select="concat('trcalc.aspx?IID=', $keyinstn)">

	</xsl:variable>
	<form method="POST" id="form1" name="form1">
	<xsl:attribute name="action">
	<xsl:value-of select="$formaction"/>
	</xsl:attribute>
	<tr>
		<td align="center" valign="top">
			<table border="0" cellpadding="2" cellspacing="0" width="100%">
				<tr>
					<td colspan="2"><img src="images/blank.gif" width="1" height="10" alt="layout background"/><br />
						<table border="0" cellpadding="2" cellspacing="0" width="100%">

						<xsl:choose>
						<xsl:when test="TradedIssues/RowCount > 1">
							<tr>
								<td colspan="3">&#160;</td>
								<td class="defaultbold" align="right">Security :<img src="images/blank.gif" width="5" height="1" alt="layout background"/></td>
								<td>
								<xsl:variable name="CurrentKeyFndg" select="FormData/Security"/>
									<label class="visuallyhidden" for="KeyFndg">Keyfunding</label>
								<select NAME="KeyFndg" ID="KeyFndg" SIZE="1" class="input">
								<xsl:for-each select="TradedIssues">
								<xsl:value-of select="KeyFndg"/>
								<option><xsl:attribute name="value">
								<xsl:value-of select="KeyFndg"/>
								</xsl:attribute>
								<xsl:if test="$CurrentKeyFndg=KeyFndg"><xsl:attribute name="SELECTED">1</xsl:attribute></xsl:if>
								<xsl:value-of select="TradingSymbol"/>
								</option>
								</xsl:for-each>
								</select>
								</td>
							</tr>
							</xsl:when>
							<xsl:otherwise>

							</xsl:otherwise>
							</xsl:choose>
							<xsl:variable name="startdate" select="FormData/TRStartDate"/>
							<xsl:variable name="enddate" select="FormData/TREndDate"/>
							<tr>
								<td class="defaultbold" nowrap="">Date Range</td>
								<td valign="top" align="left">
									<label class="visuallyhidden" for="TRStartDate"> Start Date</label>
									<input class="input" type="text" name="TRStartDate" id="TRStartDate" size="10">
									<xsl:attribute name="value">
									<xsl:value-of select="$startdate"/>
									</xsl:attribute>
									</input>
								</td>
								<td class="defaultbold" align="center">to</td>
								<td valign="top" align="left">
									<label class="visuallyhidden" for="TREndDate"> End Date</label>
									<input class="input" type="text" name="TREndDate" id="TREndDate" size="10">
									<xsl:attribute name="value">
									<xsl:value-of select="$enddate"/>
									</xsl:attribute>
									</input>
								</td>
								<td>&#160;</td>
							</tr>
							<tr>
								<td colspan="5"><img src="images/blank.gif" width="1" height="10" alt="layout background" /></td>
							</tr>

							<tr>
								<td class="defaultbold">Frequency</td>
								<td>
									<label class="visuallyhidden" for="afrequency">Frequency</label>
									<select id="afrequency" name="Frequency" class="input">
									<xsl:choose>
									<xsl:when test="FormData/Frequency='Daily'">
									<option VALUE="Daily" SELECTED="1">Daily</option>
									</xsl:when>
									<xsl:otherwise>
									<option VALUE="Daily">Daily</option>
									</xsl:otherwise>
									</xsl:choose>
									<xsl:choose>
									<xsl:when test="FormData/Frequency='Weekly'">
									<option VALUE="Weekly" SELECTED="1" >Weekly</option>
									</xsl:when>
									<xsl:otherwise>
									<option VALUE="Weekly">Weekly</option>
									</xsl:otherwise>
									</xsl:choose>
									<xsl:choose>
									<xsl:when test="FormData/Frequency='Monthly'">
									<option VALUE="Monthly" SELECTED="1" >Monthly</option>
									</xsl:when>
									<xsl:otherwise>
									<option VALUE="Monthly">Monthly</option>
									</xsl:otherwise>
									</xsl:choose>
									<xsl:choose>
									<xsl:when test="FormData/Frequency='Quarterly'">
									<option VALUE="Quarterly" SELECTED="1" >Quarterly</option>
									</xsl:when>
									<xsl:otherwise>
									<option VALUE="Quarterly">Quarterly</option>
									</xsl:otherwise>
									</xsl:choose>
									<xsl:choose>
									<xsl:when test="FormData/Frequency='Yearly'">
									<option VALUE="Yearly" SELECTED="1" >Yearly</option>
									</xsl:when>
									<xsl:otherwise>
									<option VALUE="Yearly">Yearly</option>
									</xsl:otherwise>
									</xsl:choose>
									</select>
								</td>
								<td class="defaultbold" align="center" nowrap="">Amount<br />Invested:</td>
								<td>
									<label class="visuallyhidden" for="AmountInvested">Amount Invested</label>
									<INPUT class="input" type="text" id="AmountInvested" name="Amount" size="10"><xsl:attribute name="value"><xsl:if test="number(FormData/AmountInvested) > 0"><xsl:value-of select="FormData/AmountInvested"/></xsl:if></xsl:attribute></INPUT></td>
								<td align="left"><input class="submit" type="submit" name="Submit" value="Submit"/></td>
							</tr>
						</table>
					</td>
				</tr>
				</table>
			</td>
		</tr>
		</form>

		<tr>
			<td align="left" colspan="2">
				<span class="subscript_bold"><xsl:value-of disable-output-escaping="yes" select="FormData/ErrorMsg"/></span>
			</td>
		</tr>
		<tr>
			<td class="default"><span class="defaultbold">Consult Your Tax Advisor:</span> This information does not constitute tax advice. It does not purport to be complete or to describe the consequences that may apply to particular categories of shareholders. You should consult a tax advisor regarding the calculation of your tax basis.<br /></td>
		</tr>
		<tr>
			<td><xsl:call-template name="trc_output3" /></td>
		</tr>
</table>
</xsl:template>


<xsl:template name="trc_output3">
<xsl:if test="Returns/Price != ''">
<br/>
<table border="0" cellpadding="2" cellspacing="0" width="100%" >
	<tr>
		<td valign="top" align="center">
						<!--	<xsl:variable name="bprice" select="the begining price thats returned"/>
							<xsl:variable name="treturn" select="the return"/>		-->
			<table border="0" cellpadding="2" cellspacing="0" width="400">
				<tr>
					<td class="default">Current Value <span class="subscript">(dividends reinvested)</span>:</td>
					<td><img src="images/blank.gif" width="150" height="1" alt="layout background"/></td>
					<td class="data" nowrap="1">$ <xsl:value-of select="TRCalc/CurrentValue" /></td>
				</tr>
				<tr>
					<td class="default">Share Price Change:</td>
					<td>&#160;</td>
					<td class="data" nowrap="1"><xsl:value-of select="TRCalc/Change" /> %</td>
				</tr>
				<tr>
					<td class="default">Total Return <span class="subscript">(dividends reinvested)</span>:</td>
					<td>&#160;</td>
					<td class="data" nowrap="1"><xsl:value-of select="TRCalc/TotalReturn" /> %</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td><br />
			<table class="colordark" cellpadding="0" cellspacing="0" height="1" width="100%">
				<tr>
					<td><img src="images/blank.gif" width="1" height="1" alt="layout background"/></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="2" cellspacing="0" width="400">
				<tr>
					<td class="defaultbold" align="center">
					<xsl:value-of select="Table1/Ticker" />&#160;Total Return Performance
					 <br />
					 <span class="subscript">Holding Period&#160;(<xsl:value-of select="FormData/TRStartDate" /> - <xsl:value-of select="FormData/TREndDate" />) </span></td>
				</tr>
				<tr>
					<td align="center">
					<img>
						<xsl:attribute name="src">
							<xsl:value-of select="Table1/ImageURL"/>
						</xsl:attribute>
					</img>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="2" cellspacing="0" width="400">
				<tr>
					<td class="defaultbold">Pricing Date</td>
					<td><img src="images/blank.gif" width="150" height="1" alt="layout background"/></td>
					<td class="defaultbold"><xsl:value-of select="Table1/Ticker" /></td>
				</tr>
				<xsl:for-each select="Returns">
				<tr>
					<td class="data"><xsl:value-of select="Date2"/></td>
					<td><img src="images/blank.gif" width="150" height="1" alt="layout background"/></td>
					<td class="data"><xsl:value-of select="format-number(Return,'#,##0.00 ;(#,##0.00)', 'data')"/></td>
				</tr>
				</xsl:for-each>
			</table>
		</td>
	</tr>
</table>
</xsl:if>

</xsl:template>
<!-- End Template 3 -->

  <xsl:template name="trc_footer">
    <xsl:if test="Company/Footer != ''">
      <tr><td align="left" class="data" colspan="2"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span></td></tr>
    </xsl:if>
  </xsl:template>

  <xsl:template name="trc_footer3">
    <xsl:if test="Company/Footer != ''">
      <tr><td align="left" class="data"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span></td></tr>
    </xsl:if>
  </xsl:template>
  
  <xsl:template name="trc_common">
  <STYLE TYPE="text/css">
    DUMMY
    {
    }
    .subscript
    {
    FONT-SIZE: 9px;
    FONT-WEIGHT: normal;
    TEXT-DECORATION: none
    }
    .subscript_bold
    {
    COLOR: #ff0000;
    FONT-SIZE: 9px;
    FONT-WEIGHT: bold;
    TEXT-DECORATION: none
    }
  </STYLE>
  </xsl:template>


<xsl:template match="irw:TotalReturnCalculator">
  <xsl:variable name="TemplateName" select="Company/TemplateName"/>
  <xsl:call-template name="trc_common" />
  <xsl:choose>
    <!-- template 1 -->
    <xsl:when test="$TemplateName = 'AltTRCalc1'">
      <xsl:call-template name="TemplateONEstylesheet" />
      <table border="0" cellspacing="0" cellpadding="3" width="100%" class="">
        <xsl:call-template name="Title_S2" />
        <tr class="default" align="left">
          <td class="leftTOPbord" colspan="2">
            <table border="0" cellspacing="0" cellpadding="3" width="100%" class="">
              <tr align="center">
                <td class="data" valign="top"><xsl:call-template name="trc_data1"/></td>
              </tr>
              <xsl:call-template name="trc_footer3" />
            </table>
          </td>
        </tr>
      </table>
      <xsl:call-template name="Copyright"/>
    </xsl:when>
    <!-- end template 1 -->
    <!-- template 3 -->
    <xsl:when test="$TemplateName = 'AltTRCalc3'">
      <xsl:call-template name="TemplateTHREEstylesheet" />
      <xsl:call-template name="Title_T3" />
      <table border="0" cellspacing="0" cellpadding="3" width="100%" class=" table2">
        <tr align="center">
          <td class="data" valign="top"><xsl:call-template name="trc_data3" /></td>
        </tr>
        <xsl:call-template name="trc_footer3" />
      </table>
      <xsl:call-template name="Copyright"/>
    </xsl:when>
    <!-- end template 3 -->
    
    <xsl:otherwise>
      <!-- Start Default  -->
      <table border="0" cellspacing="0" cellpadding="3" width="100%" class="colordark">
        <xsl:call-template name="Title_S1"/>
        <tr align="center">
          <td class="colordark" colspan="2" valign="top"><xsl:call-template name="trc_data" /></td>
        </tr>
        <xsl:call-template name="trc_footer" />
      </table>
      <xsl:call-template name="Copyright"/>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>
<!-- End Default -->


</xsl:stylesheet>


