<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns="http://www.w3.ord/TR/REC-html40"
  xmlns:irw="http://www.snl.com/xml/irw">

<xsl:output method="html" media-type="text/html" indent="no"/>


<!-- Start Default Template here-->

<!--
//This is the default template for the corporate profile page. Most changes are to either remove the company name, ticker, or exchange. you can also change the name of the page or remove this row all together.
//These templates below are broken up how the page is displayed.
Header = Page Name/Company Name/Ticker
WebLinks_top = Additional Links for use with Corporate Profile (rarely used)
WebLinks_header = Used when adding extra data in the Header/Footer section of the irconsole
Company Description =
Market Summary =
Home Page Text =
Front Page Graph =
WebLinks_footer =
WebLinks_footer_main =
-->


<!-- <script language="JavaScript" type="text/JavaScript">
<![CDATA[


function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}


]]></script> -->


<xsl:template name="HEADER_CORP">
<xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
  <tr>
    <td CLASS="colordark" nowrap="1"><SPAN CLASS="title1light"><xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/></SPAN></td>
    <td CLASS="colordark" nowrap="1" align="right" valign="top"><span class="subtitlelight"><xsl:value-of select="$CompanyNameCaps"/>&#160;(<xsl:value-of select="Company/Exchange"/>&#160;-&#160;<xsl:value-of select="Company/Ticker"/>)</span><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
  </tr>
</xsl:template>

<xsl:template name="WebLinks_top">
  <tr class="default" align="left">
    <td class="colorlight"><span class="default"><xsl:value-of select="WebLinks/TopLink" disable-output-escaping="yes"/></span></td>
  </tr>
</xsl:template>

<xsl:template name="WebLinks_header">
  <tr class="default" align="left">
    <td class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Header" disable-output-escaping="yes"/></span></td>
  </tr>
</xsl:template>

  <xsl:template name="Webcasts_table">
    <xsl:for-each select="Presentation">
      <table border="0" cellpadding="0" cellspacing="3">
        <tr>
          <td valign="top">
            <br/>
            <a target="_blank" title="{Link}">
              <xsl:attribute name="href">
                <xsl:value-of select="Link"/>
              </xsl:attribute>
              <img border="0" name="click here" alt="Click here">
                <xsl:attribute name="src">
                  <xsl:value-of select="IconPath"/>
                </xsl:attribute>
              </img>
            </a>
          </td>
          <td>
            <font size="-2" face="arial, helvetica, sans-serif" color="#666666">
              <a target="_blank" title="{Link}">
                <xsl:attribute name="href">
                  <xsl:value-of select="Link"/>
                </xsl:attribute>
                <xsl:if test="LinkTitle != ''">
                  <strong>
                    <xsl:value-of select="LinkTitle"/>
                  </strong>
                </xsl:if>
                <xsl:if test="LinkTitle = ''">
                  <strong>Webcast</strong>
                </xsl:if>
              </a>
              <br/>
              <xsl:if test="AltLink != ''">
                <a target="_blank" title="{Link}">
                  <xsl:attribute name="href">
                    <xsl:value-of select="AltLink"/>
                  </xsl:attribute>
                  <xsl:if test="AltLinkTitle != ''">
                    <strong>
                      <xsl:value-of select="AltLinkTitle"/>
                    </strong>
                  </xsl:if>
                  <xsl:if test="AltLinkTitle = ''">
                    <strong>Webcast</strong>
                  </xsl:if>
                </a>
                <br/>
              </xsl:if>
              <xsl:if test="Title != ''">
                <xsl:value-of select="Title"/>
                <br/>
              </xsl:if>
              <xsl:if test="BodyText != ''">
                <xsl:value-of select="BodyText" disable-output-escaping="yes"/>
                <br/>
              </xsl:if>
              <xsl:value-of select="Date"/>
              <br/>
              <xsl:value-of select="Time"/>&#160;<xsl:value-of select="TimeZone"/>
            </font>
          </td>
        </tr>
      </table>
      <br/>
    </xsl:for-each>
    <br/>
    <br/>
  </xsl:template>


  <xsl:template name="Company_Description">
  <td class="colorlight" Valign="TOP" width="90%">
    <xsl:if test="count(Presentation) &gt; 0">
    <xsl:call-template name="Webcasts_table"/>
    </xsl:if>
  <xsl:if test="WebLinks/TopLink != ''">
  <span class="default"><xsl:value-of select="WebLinks/TopLink" disable-output-escaping="yes"/></span><br />
  </xsl:if>
  <SPAN CLASS="large"><xsl:value-of select="Company/CompanyDescription" disable-output-escaping="yes"/></SPAN></td>
</xsl:template>

<xsl:template name="Chart_Drop_Down_stock">
  <tr CLASS="data">
    <td CLASS="data" align="center">
    <label class="visuallyhidden" for="period">Period</label>
      Period: <select id="period" name="period"><xsl:choose><xsl:when test="Company/GraphPeriod='_pp:3'"><option VALUE="_pp:3" SELECTED="1" >One Month</option></xsl:when><xsl:otherwise><option VALUE="_pp:3">One Month</option></xsl:otherwise></xsl:choose><xsl:choose><xsl:when test="Company/GraphPeriod='_pp:4'"><option VALUE="_pp:4" SELECTED="1" >Three Month</option></xsl:when><xsl:otherwise><option VALUE="_pp:4">Three Month</option></xsl:otherwise></xsl:choose><xsl:choose><xsl:when test="Company/GraphPeriod='_pp:10'"><option VALUE="_pp:10" SELECTED="1" >YTD</option></xsl:when><xsl:otherwise><option VALUE="_pp:10">YTD</option></xsl:otherwise></xsl:choose><xsl:choose><xsl:when test="Company/GraphPeriod='_pp:5'"><option VALUE="_pp:5" SELECTED="1" >One Year</option></xsl:when><xsl:otherwise><option VALUE="_pp:5">One Year</option></xsl:otherwise></xsl:choose><xsl:choose><xsl:when test="Company/GraphPeriod='_pp:6'"><option VALUE="_pp:6" SELECTED="1" >Three Year</option></xsl:when><xsl:otherwise><option VALUE="_pp:6">Three Year</option></xsl:otherwise></xsl:choose></select><label class="visuallyhidden" for"Submit2">Submit</label> <input TYPE="submit" NAME="submit" VALUE="Apply" ID="Submit2"/>
    </td>
  </tr>
</xsl:template>


<xsl:template name="Market_Summary">
  <table border="0" cellpadding="3" cellspacing="0" width="100%">
    <tr>
      <td class="header">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td class="header" align="left" valign="top" nowrap="">Market Summary</td>
            <td class="header" align="right" valign="top" height="18">
              <a class="light" valign="middle" href="javascript:;" onMouseOver="javascript:window.status='Desktop Summary'; return true;" onfocus="javascript:window.status='Desktop Summary'; return true;" onMouseOut="javascript:window.status='';return true;" onblur="javascript:window.status='';return true;" title="Desktop Summary">
              <xsl:attribute name="OnClick">window.open('marketsummary.aspx?iid=<xsl:value-of select="Company/KeyInstn"/>', 'MarketSummary', 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,width=185,height=175');</xsl:attribute><img alt="Desktop Summary" title="Desktop Summary" border="0" src="images/msum.gif"/></a>
            </td>
          </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="3">
          <tr>
            <td class="data" width="135" nowrap=""><span class="defaultbold">Trading Symbol</span></td>
            <td class="data" colspan="3" width="65"><span class="default"><xsl:value-of select="Company/Ticker"/></span></td>
          </tr>
          <tr class="data">
            <td class="data" width="135" nowrap=""><span class="defaultbold">Exchange</span></td>
            <td class="data" colspan="3" width="65"><span class="default"><xsl:value-of select="Company/Exchange"/></span></td>
          </tr>
          <tr class="data">
            <td width="135" nowrap=""><span class="defaultbold">Market Value ($M)</span></td>
            <td colspan="3" nowrap="" width="65"><span class="default"><nobr><xsl:value-of select="format-number(StockQuote/MarketValueAtCurrentPrice, '###,##0.00')"/></nobr></span></td>
          </tr>
          <tr class="data">
            <td class="data" width="135" nowrap=""><span class="defaultbold">Stock Quote</span></td>
            <td class="data" colspan="3" nowrap="" width="65"><span class="default"><nobr>$&#160;<xsl:value-of select="format-number(StockQuote/CurrentPrice, '###,##0.00')"/></nobr></span></td>
          </tr>
          <tr class="data">
            <td class="data" width="135" nowrap="" valign="top"><span class="defaultbold">Change</span></td>
            <td class="data" nowrap="" align="right"><span class="default">
                    <xsl:variable name="NetChangeFromPreviousClose" select="StockQuote/NetChangeFromPreviousClose"/>
                    <xsl:choose><xsl:when test="$NetChangeFromPreviousClose != 0">&#36;&#160;<xsl:value-of select="$NetChangeFromPreviousClose"/>&#160;</xsl:when><xsl:otherwise>NA</xsl:otherwise></xsl:choose>
                    <xsl:variable name="PercentChangeFromPreviousClose" select="StockQuote/PercentChangeFromPreviousClose"/>
                    <xsl:choose><xsl:when test="$PercentChangeFromPreviousClose != 0"><BR /><xsl:choose><xsl:when test="$PercentChangeFromPreviousClose &gt; 0"><xsl:if test="StockQuote/NetChangeDirection = 'negative'">(</xsl:if><xsl:value-of select="format-number($PercentChangeFromPreviousClose, '###,##0.00')"/><xsl:if test="StockQuote/NetChangeDirection = 'negative'">)</xsl:if>&#160;<span class="default">&#37;</span></xsl:when><xsl:otherwise><xsl:value-of select="format-number($PercentChangeFromPreviousClose, '###,##0.00')"/>&#160;<span class="default">&#37;</span></xsl:otherwise></xsl:choose></xsl:when><xsl:otherwise><BR />NA</xsl:otherwise></xsl:choose>
                </span>
              </td>
            <td class="data" nowrap="">
            <xsl:if test="StockQuote/NetChangeDirection = 'positive'"><span class="default"><IMG SRC="/images/Interactive/IR/uptriangle.gif" width="11" HEIGHT="10" border="0" ALT="Up" /></span></xsl:if>
            <xsl:if test="StockQuote/NetChangeDirection = 'negative'"><span class="default"><IMG SRC="/images/Interactive/IR/downtriangle.gif" width="11" HEIGHT="10" border="0" ALT="Down" /></span></xsl:if>
            </td>
          </tr>
          <tr class="data">
            <td class="data" width="135" nowrap=""><span class="defaultbold">Volume</span></td>
            <td class="data" colspan="3" nowrap="" width="65"><span class="default"><nobr><xsl:value-of select="StockQuote/VolumeToday"/></nobr></span></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr align="center">
      <td class="colorlight" nowrap="" colspan="2"><p><span class="default">As of <xsl:value-of select="StockQuote/QuoteTimeStamp"/><BR />Minimum 20 minute delay.</span><BR /><BR /></p></td>
    </tr>
  </table>
</xsl:template>

<xsl:template name="Home_PG_Text">
<table border="0" cellpadding="3" cellspacing="0" width="100%">
<tr>
<td align="center"><P><span class="defaultbold"> </span></P><span class="large"><xsl:value-of select="Company/IRHomePageText" disable-output-escaping="yes"/><xsl:value-of select="Company/Address" disable-output-escaping="yes"/><xsl:value-of select="Company/IRContact" disable-output-escaping="yes"/><xsl:value-of select="Company/TransferAgent" disable-output-escaping="yes"/></span></td>
</tr>
</table>
</xsl:template>

<xsl:template name="Front_PG_Graph">
<xsl:variable name="GraphType" select="GraphInfo/Type"/>
<xsl:if test="$GraphType != ''">
<xsl:if test="$GraphType != 'NoGraph'">  
<table width="85px" border="0" cellpadding="3" cellspacing="0">
<tr>
	<td class="" nowrap="">
		<table width="85px" cellpadding="2" cellspacing="0" border="0">
		<xsl:if test="$GraphType='SP/VolGraphs'">
			<xsl:if test="Prices1/ImageURL != ''">
			<tr>
				<td class="data" align="center">
					<img border="0">
					<xsl:attribute name="src">
						<xsl:value-of select="Prices1/ImageURL"/>
					</xsl:attribute>

					<xsl:if test="Prices1/Width != ''">
						<xsl:attribute name="width">
							<xsl:value-of select="Prices1/Width"/>
						</xsl:attribute>
					</xsl:if>

					<xsl:if test="Prices1/Height != ''">
						<xsl:attribute name="height">
							<xsl:value-of select="Prices1/Height"/>
						</xsl:attribute>
					</xsl:if>
					</img>
				</td>
			</tr>
			</xsl:if>

			<xsl:if test="Volume/ImageURL != ''">
			<tr>
				<td class="data" align="center">
					<img border="0">
					<xsl:attribute name="src">
						<xsl:value-of select="Volume/ImageURL"/>
					</xsl:attribute>

					<xsl:if test="Volume/Width != ''">
						<xsl:attribute name="width">
							<xsl:value-of select="Volume/Width"/>
						</xsl:attribute>
					</xsl:if>

					<xsl:if test="Volume/Height != ''">
						<xsl:attribute name="height">
							<xsl:value-of select="Volume/Height"/>
						</xsl:attribute>
					</xsl:if>
					</img>
				</td>
			</tr>
			</xsl:if>

			<xsl:if test="Company/ComparativeIndexShortName != '' and Company/IsSNLIndex='true'">
			<tr>
				<td class="data" align="center">
				<xsl:variable name="keyinstn" select="Company/KeyInstn" />
				<xsl:variable name="keyindex" select="Company/KeyIndex" />
				<xsl:variable name="url" select="concat('index.aspx?iid=', $keyinstn, '&amp;KeyIndex=', $keyindex)" />
					<br />
					<a>
					<xsl:attribute name="href"><xsl:value-of select="$url" /></xsl:attribute>
					<xsl:attribute name="target">_new</xsl:attribute>
						List of all companies in The <xsl:value-of select="Company/ComparativeIndexShortName" disable-output-escaping="yes"/> Index
					</a>
					<br />
				</td>
			</tr>
			</xsl:if>

      <tr>
        <td class="data" align="center">
			<label class="visuallyhidden" for="period">Period</label>
          Period: <select id="period" name="period" onchange="document.form1.submit();" >
            <xsl:choose>
              <xsl:when test="Company/GraphPeriod='_pp:3'">
                <option VALUE="_pp:3" SELECTED="1" >One Month</option>
              </xsl:when>
              <xsl:otherwise>
                <option VALUE="_pp:3">One Month</option>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
              <xsl:when test="Company/GraphPeriod='_pp:4'">
                <option VALUE="_pp:4" SELECTED="1" >Three Month</option>
              </xsl:when>
              <xsl:otherwise>
                <option VALUE="_pp:4">Three Month</option>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
              <xsl:when test="Company/GraphPeriod='_pp:10'">
                <option VALUE="_pp:10" SELECTED="1" >YTD</option>
              </xsl:when>
              <xsl:otherwise>
                <option VALUE="_pp:10">YTD</option>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
              <xsl:when test="Company/GraphPeriod='_pp:5'">
                <option VALUE="_pp:5" SELECTED="1" >One Year</option>
              </xsl:when>
              <xsl:otherwise>
                <option VALUE="_pp:5">One Year</option>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
              <xsl:when test="Company/GraphPeriod='_pp:6'">
                <option VALUE="_pp:6" SELECTED="1" >Three Year</option>
              </xsl:when>
              <xsl:otherwise>
                <option VALUE="_pp:6">Three Year</option>
              </xsl:otherwise>
            </xsl:choose>
          </select>
        </td>
      </tr>

		</xsl:if>

		<xsl:if test="$GraphType = 'FPGraph'">
			<xsl:if test="FPGraph/ImageURL != ''">
			<tr>
				<td class="data" align="center">
					<img border="0">
					<xsl:attribute name="src">
						<xsl:value-of select="FPGraph/ImageURL"/>
					</xsl:attribute>

					<xsl:if test="FPGraph/Width != ''">
						<xsl:attribute name="width">
							<xsl:value-of select="FPGraph/Width"/>
						</xsl:attribute>
					</xsl:if>

					<xsl:if test="FPGraph/Height != ''">
						<xsl:attribute name="height">
							<xsl:value-of select="FPGraph/Height"/>
						</xsl:attribute>
					</xsl:if>
					</img>
				</td>
			</tr>
			</xsl:if>
		</xsl:if>
		</table>
	</td>
  </tr>
</table>
</xsl:if>
</xsl:if>
</xsl:template>

<xsl:template name="WebLinks_footer">
<tr class="colorlight" align="center">
  <td colspan="2"><span class="large"><xsl:value-of select="WebLinks/BottomLink" disable-output-escaping="yes"/></span></td>
</tr>
</xsl:template>

<xsl:template name="WebLinks_footer_main">
<table  border="0" cellpadding="3" cellspacing="0" width="100%">
<tr>
<td class="colorlight" align="CENTER"><img src="/images/interactive/blank.gif" width="100%" HEIGHT="5" border="0" alt=""/></td>
</tr>
<xsl:if test="Company/Footer != ''">
<tr>
  <td class="colorlight" align="left"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span></td>
</tr>
</xsl:if>
</table>
</xsl:template>

 <xsl:template name="Company_News">
  <xsl:if test="count(PressRelease) > 0">
  <hr size="1" width="220px" />
 <table border="0" cellspacing="0" cellpadding="3" width="220px" class="header">
  <tr>
    <xsl:if test="normalize-space(RSSLinks/InfoLink)">
      <td class="header" Valign="TOP" norap="1" colspan="2">
      <table border="0" cellspacing="0" cellpadding="0" width="100%">
      <tr>
      <td class="header">Press Releases</td>
      <td align="right" class="" Valign="TOP">
       <a title="{RSSLinks/FeedLink}">
              <xsl:attribute name="href"><xsl:value-of select="RSSLinks/FeedLink"/>
              </xsl:attribute>
              <img src="images/rss_xml.gif" border="0" alt="RSS XML"/></a>
              <a>
      	<xsl:variable name="Keyinstn" select="Company/KeyInstn" />
      	<xsl:variable name="RSSLinks" select="RSSLinks/InfoLink" />
      	<xsl:variable name="RSSurl" select="concat($RSSLinks, '?iid=', $Keyinstn)" />
              <xsl:attribute name="href"><xsl:value-of select="$RSSurl"/></xsl:attribute>
        <img src="images/rss_button.gif" border="0" alt="RSS Button"/></a>
      </td></tr></table>
      </td>
    </xsl:if>
    <xsl:if test="not(normalize-space(RSSLinks/InfoLink))">
      <td class="header" Valign="TOP" colspan="2">Press Releases</td>
    </xsl:if>

  </tr>
  <xsl:for-each select="PressRelease">
    <tr class="data">
      <td class="data" valign="top" width="15%"><xsl:value-of select="Date" disable-output-escaping="yes"/></td>
      <td class="data" valign="top" align="left"><a><xsl:variable name="url" select="Link" /><xsl:attribute name="target">_new</xsl:attribute><xsl:attribute name="href"><xsl:value-of select="$url" /></xsl:attribute><xsl:value-of select="Headline" disable-output-escaping="yes"/></a><br /><br /></td>
    </tr>
  </xsl:for-each>

  </table><br />
    </xsl:if>
     <xsl:if test="count(PressRelease) = '0'"></xsl:if>
</xsl:template>

<xsl:template name="Company_Calendar">
  <xsl:variable name="RowCount" select="Calendar/CompanyRowCount"/>
  <xsl:variable name="RowCountEmpty" select="0"/>
  <xsl:variable name="KeyInstnValue" select="Company/KeyInstn"/>

  <xsl:if test="$RowCount != $RowCountEmpty">
  <script language="javascript" type="text/javascript" src="javascript/frontbox/fbox_conf.js"></script>
  <script language="javascript" type="text/javascript" src="javascript/frontbox/fbox_engine-min.js"></script>
  <link  type="text/css" rel="stylesheet" href="javascript/frontbox/fbox.css" />
    <table border="0" cellspacing="0" cellpadding="3" width="220px" class="header">
      <TR>
        <TD width="50%" CLASS="header" align="left" valign="bottom">Event</TD>
        <TD width="50%" CLASS="header" align="right" valign="bottom" colspan="4">Date</TD>
      </TR>

      <xsl:for-each select="Calendar">

        <xsl:choose>
          <xsl:when test="$RowCount = $RowCountEmpty">
            <TR>
              <TD COLSPAN="5" CLASS="data">
                <strong>There are no events currently scheduled.</strong>
              </TD>
            </TR>
          </xsl:when>

          <xsl:when test="$RowCount != $RowCountEmpty">
            <xsl:variable name="CompanyCalendarTitle" select="CompanyCalendarTitle"/>
            <xsl:variable name="KeyValue" select="Key"/>

            <xsl:variable name="StartDate" select="CompanyCalendarDate"/>
            <xsl:variable name="StartDay" select="substring-before($StartDate,' ')"/>
            <xsl:variable name="StartTime" select="substring-after($StartDate,' ')"/>

            <xsl:variable name="EndDate" select="CompanyCalendarDateEnd"/>
            <xsl:variable name="EndDay" select="substring-before($EndDate,' ')"/>
            <xsl:variable name="EndTime" select="substring-after($EndDate,' ')"/>

            <xsl:variable name="EventName" select="concat('Event_', $KeyValue)"></xsl:variable>
            <xsl:variable name="RemindName" select="concat('Remind_', $KeyValue)"></xsl:variable>
            <xsl:variable name="RemindValue" select="CalendarNotifyDaysPrior"></xsl:variable>
            <xsl:variable name="StartDateName" select="concat('StartDate_', $KeyValue)"></xsl:variable>
            <xsl:variable name="EventType" select="EventType"/>

            <xsl:variable name="url" select="concat('calendardetail.aspx?IID=', $KeyInstnValue,'&amp;Key=', $KeyValue )"/>
            <xsl:variable name="url2" select="concat('caldesc.aspx?IID=', $KeyInstnValue,'&amp;Key=', $KeyValue )"/>

            <!-- Meridiem variable Print (AM/PM) value -->
            <xsl:variable name="Meridiem" select="substring-after($StartTime,' ')"/>

            <TR>
              <td CLASS="data" align="left" valign="top">
                <xsl:choose>
                  <xsl:when test="$EventType = '0'">
                    <a>
						<xsl:attribute name="href">
						<xsl:value-of select="$url" />&amp;KeyType=3
						</xsl:attribute>
						<span name="frontbox"  height="400" width="700" class="frontbox">
						</span>
					  <!--<a href="javascript:;" class="fielddef" onClick="DefWindowAlt({$KeyInstnValue}, {$KeyValue}, '3'); return true">-->
						<xsl:value-of select="CompanyCalendarTitle"/>
                      </a>
                  </xsl:when>
                  <xsl:when test="$EventType = '1'">
                    <a>
						<xsl:attribute name="href">
						<xsl:value-of select="$url" />&amp;KeyType=2
						</xsl:attribute>
						<span name="frontbox" height="400" width="700" class="frontbox">
						</span>
					  	<!--<a href="javascript:;" class="fielddef" onClick="DefWindowAlt({$KeyInstnValue}, {$KeyValue}, '2'); return true">-->
						<xsl:value-of select="CompanyCalendarTitle"/>
                    </a>
                  </xsl:when>
                  <xsl:when test="$EventType = '2'">
                    <a>
						<xsl:attribute name="href">
						<xsl:value-of select="$url" />&amp;KeyType=1
						</xsl:attribute>
						<span name="frontbox" height="400" width="700" class="frontbox">
						</span>
					  <!--<a href="javascript:;" class="fielddef" onClick="DefWindowAlt({$KeyInstnValue}, {$KeyValue}, '1'); return true">-->
						<xsl:value-of select="CompanyCalendarTitle"/>
                      </a>
                  </xsl:when>
                  <xsl:when test="$EventType = '3'">
                     <a>
					<xsl:attribute name="href">
            <xsl:if test="../Company/IsPreview = 'true'">
              <xsl:value-of select="concat($url,'&amp;preview=1')"/>&amp;KeyType=4
            </xsl:if>
              <xsl:if test="../Company/IsPreview = 'false'">
                <xsl:value-of select="$url"/>&amp;KeyType=4
            </xsl:if>
					</xsl:attribute>
					<span name="frontbox" height="400" width="700" class="frontbox">
					</span>
				  <!--<a href="javascript:;" class="fielddef" onClick="DefWindowAlt({$KeyInstnValue}, {$KeyValue}, '4'); return true">-->
					<xsl:value-of select="CompanyCalendarTitle"/>
                      </a>
                  </xsl:when>
                  <xsl:otherwise>
                    <a>
					<xsl:attribute name="href">
            <xsl:if test="../Company/IsPreview = 'true'">
              <xsl:value-of select="concat($url2,'&amp;preview=1')"/>
            </xsl:if>
            <xsl:if test="../Company/IsPreview = 'false'">
              <xsl:value-of select="$url2"/>
            </xsl:if>
					</xsl:attribute>
					<span name="frontbox" height="400" width="700" class="frontbox">
					</span>
				  	<!--<a href="javascript:;" class="fielddef" onClick="DefWindow({$KeyInstnValue}, {$KeyValue}); return true">-->
					<xsl:value-of select="CompanyCalendarTitle"/>
                     </a>
                  </xsl:otherwise>
                </xsl:choose>
              </td>
              <xsl:choose>
                <xsl:when test="$EndDate != '' and ($EndDate != $StartDate or $EndTime != $StartTime) and CompanyCalendarDate != CompanyCalendarDateEnd">
                  <td CLASS="data" align="right" valign="top">
                    <xsl:value-of select="$StartDay"/>
                    <xsl:if test="$StartTime != '12:00:00 AM' and $StartTime != ''">
                      <br/>
                      <!-- Code Display Hour and Minute and Hide Secounds -->
                      <xsl:choose>
                        <xsl:when test="string-length($StartTime) = '10'"><xsl:value-of select="substring($StartTime, 0, 5)"/></xsl:when>
                        <xsl:otherwise><xsl:value-of select="substring($StartTime, 0, 6)"/></xsl:otherwise>
                      </xsl:choose>&#160;<xsl:value-of select="$Meridiem"/>&#160;<xsl:value-of select="TimeZoneLabel"/>
                    </xsl:if>&#160;-&#160;<xsl:value-of select="$EndDay"/>
                    <xsl:if test="$EndTime != '12:00:00 AM' and $EndTime != ''">
                      <br/><xsl:value-of select="$EndTime"/>&#160;<xsl:value-of select="TimeZoneLabel"/>
                    </xsl:if>
                  </td>
                </xsl:when>
                <xsl:otherwise>
                  <td CLASS="data" align="right" valign="top">
                    <xsl:value-of select="$StartDay"/>
                    <xsl:if test="$StartTime != '12:00:00 AM' and $StartTime != ''">
                      <br/>
                      <!-- Code Display Hour and Minute and Hide Secounds -->
                      <xsl:choose>
                        <xsl:when test="string-length($StartTime) = '10'"><xsl:value-of select="substring($StartTime, 0, 5)"/></xsl:when>
                        <xsl:otherwise><xsl:value-of select="substring($StartTime, 0, 6)"/></xsl:otherwise>
                      </xsl:choose>&#160;<xsl:value-of select="$Meridiem"/>&#160;<xsl:value-of select="TimeZoneLabel"/>
                    </xsl:if>
                  </td>
                </xsl:otherwise>
              </xsl:choose>
            </TR>
          </xsl:when>
        </xsl:choose>
      </xsl:for-each>
      <INPUT TYPE="hidden" NAME="EventCount">
        <xsl:attribute name="value">
          <xsl:value-of select="Calendar/CompanyRowCount"/>
        </xsl:attribute>
      </INPUT>
    </table>
    <Br />
  </xsl:if>
</xsl:template>

<!-- Default Template Ends here. all templates below are ONLY if you are using the 'Style Template' section in the console. -->








<!-- Template ONE -->
<!-- This is Template ONE option in the console under 'Style Template'
//This template is a striped down version of the main template for Corporate Profile. Everything (data) is displayed in single rows.
//The sections will be commented on where the rows are. you should be able to just move rows (entire chunks of data) where you need too. Edits to this section are rare, mostly due to size (width of the page).
//These templates below are broken up how the page is displayed.
Header = Page Name/Company Name/Ticker
Market Summary =
WebLinks_header = Used when adding extra data in the Header/Footer section of the irconsole
Company Description =
Company News = Link to news page (latest news stories)
Front Page Graph =
Company Contact Info =
WebLinks_footer =
WebLinks_footer_main =
-->
  <xsl:template name="HEADER_CORP_2">
  <xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
	<tr>
		<td CLASS="title2colordark titletest" nowrap="" valign="top"><xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/><br /><IMG SRC="" width="2" HEIGHT="1" alt="" /><span class="title2colordark titletest2"><xsl:value-of select="$CompanyNameCaps"/>&#160;(<xsl:value-of select="Company/Exchange"/>&#160;-&#160;<xsl:value-of select="Company/Ticker"/>)&#160;</span></td>
		<td nowrap="" valign="top">
			<table>
				<tr>
					<td><span class="default" nowrap=""><strong>Market Value ($M)</strong></span></td>
					<td align="right"><span class="default"><nobr><xsl:value-of select="format-number(StockQuote/MarketValueAtCurrentPrice, '###,##0.00')"/></nobr></span></td>
				</tr>
				<tr>
					<td><span class="default"><strong>Stock Quote</strong></span></td>
					<td align="right"><span class="default"><nobr>$&#160;<xsl:value-of select="format-number(StockQuote/CurrentPrice, '###,##0.00')"/></nobr></span></td>
				</tr>
				<tr>
					<td><span class="default"><strong>Change</strong></span></td>
		          		<td align="right">
          					<xsl:choose><xsl:when test="StockQuote/NetChangeFromPreviousClose = '0'"><span class="default">NA</span></xsl:when>
                      <xsl:otherwise>
                        <span class="default"><xsl:variable name="NetChangeFromPreviousClose" select="StockQuote/NetChangeFromPreviousClose"/>
                          <xsl:choose><xsl:when test="$NetChangeFromPreviousClose &gt; 0">&#36;&#160;<xsl:value-of select="$NetChangeFromPreviousClose"/></xsl:when><xsl:otherwise>NA</xsl:otherwise></xsl:choose>
                          <xsl:if test="StockQuote/NetChangeDirection = 'positive'">&#160;<IMG SRC="/images/Interactive/IR/uptriangle.gif" width="11" HEIGHT="10" border="0" ALT="Up" /></xsl:if><xsl:if test="StockQuote/NetChangeDirection = 'negative'">&#160;<IMG SRC="/images/Interactive/IR/downtriangle.gif" width="11" HEIGHT="10" border="0" ALT="Down" /></xsl:if>
                          <xsl:variable name="PercentChangeFromPreviousClose" select="StockQuote/PercentChangeFromPreviousClose"/>
                          <xsl:choose><xsl:when test="$PercentChangeFromPreviousClose != 0"><xsl:choose><xsl:when test="$PercentChangeFromPreviousClose &gt; 0">&#160;<xsl:if test="StockQuote/NetChangeDirection = 'negative'">(</xsl:if><xsl:value-of select="format-number($PercentChangeFromPreviousClose, '###,##0.00')"/><xsl:if test="StockQuote/NetChangeDirection = 'negative'">)</xsl:if>&#160;<span class="default">&#37;</span></xsl:when><xsl:otherwise>&#160;<xsl:value-of select="format-number($PercentChangeFromPreviousClose, '###,##0.00')"/>&#160;<span class="default">&#37;</span></xsl:otherwise></xsl:choose></xsl:when><xsl:otherwise>&#160;NA</xsl:otherwise></xsl:choose></span>
							</xsl:otherwise>
			</xsl:choose>
						</td>
					</tr>
					<tr class="data">
						<td><span class="default"><strong>Volume</strong></span></td>
						<td align="right"><span class="default"><xsl:value-of select="StockQuote/VolumeToday"/></span></td>
					</tr>
				</table>
			</td>
		</tr>
</xsl:template>

<xsl:template name="WebLinks_header_2">
  <tr class="default" align="left">
    <td class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Header" disable-output-escaping="yes"/></span></td>
  </tr>

</xsl:template>

  <xsl:template name="Webcasts_table_2">
    <xsl:for-each select="Presentation">
      <table border="0" cellpadding="0" cellspacing="3">
        <tr>
          <td valign="top">
            <a target="_blank" title="{Link}">
              <xsl:attribute name="href">
                <xsl:value-of select="Link"/>
              </xsl:attribute>
              <img border="0" name="click here" alt="Click Here">
                <xsl:attribute name="src">
                  <xsl:value-of select="IconPath"/>
                </xsl:attribute>
              </img>
            </a>
          </td>
          <td>
            <font size="-2" face="arial, helvetica, sans-serif" color="#666666">
              <a target="_blank" title="{Link}">
                <xsl:attribute name="href">
                  <xsl:value-of select="Link"/>
                </xsl:attribute>
                <xsl:if test="LinkTitle != ''">
                  <strong>
                    <xsl:value-of select="LinkTitle"/>
                  </strong>
                </xsl:if>
                <xsl:if test="LinkTitle = ''">
                  <strong>Webcast</strong>
                </xsl:if>
              </a>
              <br/>
              <xsl:if test="AltLink != ''">
                <a target="_blank" title="{Link}">
                  <xsl:attribute name="href">
                    <xsl:value-of select="AltLink"/>
                  </xsl:attribute>
                  <xsl:if test="AltLinkTitle != ''">
                    <strong>
                      <xsl:value-of select="AltLinkTitle"/>
                    </strong>
                  </xsl:if>
                  <xsl:if test="AltLinkTitle = ''">
                    <strong>Webcast</strong>
                  </xsl:if>
                </a>
                <br/>
              </xsl:if>
              <xsl:if test="Title != ''">
                <xsl:value-of select="Title"/>
                <br/>
              </xsl:if>
              <xsl:if test="BodyText != ''">
                <xsl:value-of select="BodyText" disable-output-escaping="yes"/>
                <br/>
              </xsl:if>
              <xsl:value-of select="Date"/>
              <br/>
              <xsl:value-of select="Time"/>&#160;<xsl:value-of select="TimeZone"/>
            </font>
          </td>
        </tr>
      </table>
      <br/>
    </xsl:for-each>
    <br/>
    <br/>
  </xsl:template>

<xsl:template name="Company_Description_2">
  <tr>
    <td class="data" valign="top">
    <xsl:if test="count(Presentation) &gt; 0">
      <xsl:call-template name="Webcasts_table_2"/>
    </xsl:if>
    <xsl:if test="WebLinks/TopLink != ''">
      <span class="default"><xsl:value-of select="WebLinks/TopLink" disable-output-escaping="yes"/></span><br />
    </xsl:if>
      <span class="large"><xsl:value-of select="Company/CompanyDescription" disable-output-escaping="yes"/></span><br/><br/>
    </td>
  </tr>
  <xsl:if test="count(PressRelease) != 0">
  <tr>
    <td class="data" valign="top"><xsl:call-template name="Company_News_2"/></td>
  </tr>
  </xsl:if>
  <xsl:if test="count(Calendar) > 0">
    <tr>
      <td class="data" valign="top">
        <xsl:call-template name="Company_Calendar_2"/>
      </td>
    </tr>
  </xsl:if>
</xsl:template>


 <xsl:template name="Company_News_2">
  <xsl:if test="count(PressRelease) > 0">
    <hr size="1" width="100%" />
 <table border="0" cellspacing="0" cellpadding="3" width="100%" class="data">
  <!-- <tr>
    <td class="header" colspan="2">Latest News Stories</td>
  </tr> -->
  <tr>
    <xsl:if test="normalize-space(RSSLinks/InfoLink)">
      <td class="header" Valign="TOP" colspan="2">
        <table border="0" cellspacing="0" cellpadding="0" width="100%">
        <tr>
          <td class="header" align="left">Press Releases</td>
          <td class="header" align="right">
          <a>
	         <xsl:attribute name="href"><xsl:value-of select="RSSLinks/FeedLink"/>
	         </xsl:attribute>
	         <img src="images/rss_xml.gif" border="0" alt="RSS XML"/></a>
	         <a>
	 	<xsl:variable name="Keyinstn" select="Company/KeyInstn" />
	 	<xsl:variable name="RSSLinks" select="RSSLinks/InfoLink" />
	 	<xsl:variable name="RSSurl" select="concat($RSSLinks, '?iid=', $Keyinstn)" />
	         <xsl:attribute name="href"><xsl:value-of select="$RSSurl"/></xsl:attribute>
        <img src="images/rss_button.gif" border="0" alt="RSS Button"/></a>
          </td>
        </tr>
        </table>
      </td>
    </xsl:if>
    <xsl:if test="not(normalize-space(RSSLinks/InfoLink))">
      <td class="header" Valign="TOP" colspan="2">Press Releases</td>
    </xsl:if>
  </tr>
  <xsl:for-each select="PressRelease">
    <tr class="data">
      <td class="data" valign="top" width="15%"><xsl:value-of select="Date" disable-output-escaping="yes"/></td>
      <td class="data" valign="top" align="left"><a><xsl:variable name="url" select="Link" /><xsl:attribute name="target">_new</xsl:attribute><xsl:attribute name="href"><xsl:value-of select="$url" /></xsl:attribute><xsl:value-of select="Headline" disable-output-escaping="yes"/></a><br /><br /></td>
    </tr>
  </xsl:for-each>

  </table><br />
  </xsl:if>
</xsl:template>

<xsl:template name="Company_Calendar_2">
  <xsl:variable name="RowCount" select="Calendar/CompanyRowCount"/>
  <xsl:variable name="RowCountEmpty" select="0"/>
  <xsl:variable name="KeyInstnValue" select="Company/KeyInstn"/>

  <xsl:if test="$RowCount != $RowCountEmpty">
  <script language="javascript" type="text/javascript" src="javascript/frontbox/fbox_conf.js"></script>
  <script language="javascript" type="text/javascript" src="javascript/frontbox/fbox_engine-min.js"></script>
  <link  type="text/css" rel="stylesheet" href="javascript/frontbox/fbox.css" />
    <hr size="1" width="100%" />
    <table border="0" cellspacing="0" cellpadding="3" class="data" width="100%">
      <TR>
        <TD CLASS="header" align="left" valign="bottom">Event</TD>
        <TD CLASS="header" align="right" valign="bottom">Date</TD>
      </TR>
      <xsl:for-each select="Calendar">
        <xsl:choose>
          <xsl:when test="$RowCount = $RowCountEmpty">
            <TR>
              <TD COLSPAN="2" CLASS="data">
                <strong>There are no events currently scheduled.</strong>
              </TD>
            </TR>
          </xsl:when>
          <xsl:when test="$RowCount != $RowCountEmpty">
            <xsl:variable name="CompanyCalendarTitle" select="CompanyCalendarTitle"/>
            <xsl:variable name="KeyValue" select="Key"/>
            <xsl:variable name="StartDate" select="CompanyCalendarDate"/>
            <xsl:variable name="StartDay" select="substring-before($StartDate,' ')"/>
            <xsl:variable name="StartTime" select="substring-after($StartDate,' ')"/>
            <xsl:variable name="EndDate" select="CompanyCalendarDateEnd"/>
            <xsl:variable name="EndDay" select="substring-before($EndDate,' ')"/>
            <xsl:variable name="EndTime" select="substring-after($EndDate,' ')"/>
            <xsl:variable name="EventName" select="concat('Event_', $KeyValue)"></xsl:variable>
            <xsl:variable name="RemindName" select="concat('Remind_', $KeyValue)"></xsl:variable>
            <xsl:variable name="RemindValue" select="CalendarNotifyDaysPrior"></xsl:variable>
            <xsl:variable name="StartDateName" select="concat('StartDate_', $KeyValue)"></xsl:variable>
            <xsl:variable name="EventType" select="EventType"/>
			      <xsl:variable name="url" select="concat('calendardetail.aspx?IID=', $KeyInstnValue,'&amp;Key=', $KeyValue )"/>
            <xsl:variable name="url2" select="concat('caldesc.aspx?IID=', $KeyInstnValue,'&amp;Key=', $KeyValue )"/>
            <!-- Meridiem variable Print (AM/PM) value -->
            <xsl:variable name="Meridiem" select="substring-after($StartTime,' ')"/>

            <TR>
              <td class="data" align="left" valign="top">
                <xsl:choose>
                  <xsl:when test="$EventType = '0'">
                   <a>
                     <xsl:attribute name="href">
                       <xsl:if test="../Company/IsPreview = 'true'">
                         <xsl:value-of select="concat($url,'&amp;preview=1')"/>&amp;KeyType=3
                       </xsl:if>
                       <xsl:if test="../Company/IsPreview = 'false'">
                         <xsl:value-of select="$url"/>&amp;KeyType=3
                       </xsl:if>
                     </xsl:attribute>
                     <span name="frontbox" height="400" width="700" class="frontbox"></span>
                     <!--<a href="javascript:;" class="fielddef" onClick="DefWindowAlt({$KeyInstnValue}, {$KeyValue}, '3'); return true">--><xsl:value-of select="CompanyCalendarTitle"/>
                   </a>
                  </xsl:when>
                  <xsl:when test="$EventType = '1'">
                  <a>
                    <xsl:attribute name="href">
                      <xsl:if test="../Company/IsPreview = 'true'">
                        <xsl:value-of select="concat($url,'&amp;preview=1')"/>&amp;KeyType=2
                      </xsl:if>
                      <xsl:if test="../Company/IsPreview = 'false'">
                        <xsl:value-of select="$url"/>&amp;KeyType=2
                      </xsl:if>
                    </xsl:attribute>
                    <span name="frontbox" height="400" width="700" class="frontbox"></span>
                    <!--<a href="javascript:;" class="fielddef" onClick="DefWindowAlt({$KeyInstnValue}, {$KeyValue}, '2'); return true">--><xsl:value-of select="CompanyCalendarTitle"/>
                  </a>
                  </xsl:when>
                  <xsl:when test="$EventType = '2'">
                   <a>
                     <xsl:attribute name="href">
                       <xsl:if test="../Company/IsPreview = 'true'">
                         <xsl:value-of select="concat($url,'&amp;preview=1')"/>&amp;KeyType=1
                       </xsl:if>
                       <xsl:if test="../Company/IsPreview = 'false'">
                         <xsl:value-of select="$url"/>&amp;KeyType=1
                       </xsl:if>
                     </xsl:attribute>
                     <span name="frontbox" height="400" width="700" class="frontbox"></span>
                     <!--<a href="javascript:;" class="fielddef" onClick="DefWindowAlt({$KeyInstnValue}, {$KeyValue}, '1'); return true">--><xsl:value-of select="CompanyCalendarTitle"/>
                   </a>
                  </xsl:when>
                  <xsl:when test="$EventType = '3'">
                   <a>
                     <xsl:attribute name="href">
                       <xsl:if test="../Company/IsPreview = 'true'">
                         <xsl:value-of select="concat($url,'&amp;preview=1')"/>&amp;KeyType=4
                       </xsl:if>
                       <xsl:if test="../Company/IsPreview = 'false'">
                         <xsl:value-of select="$url"/>&amp;KeyType=4
                       </xsl:if>&amp;KeyType=4</xsl:attribute>
                     <span name="frontbox" height="400" width="700" class="frontbox"></span><!--<a href="javascript:;" class="fielddef" onClick="DefWindowAlt({$KeyInstnValue}, {$KeyValue}, '4'); return true">--><xsl:value-of select="CompanyCalendarTitle"/>
                   </a>
                  </xsl:when>
                  <xsl:otherwise>
                    <a><xsl:attribute name="href"><xsl:if test="../Company/IsPreview = 'true'">
                      <xsl:value-of select="concat($url2,'&amp;preview=1')"/>
                    </xsl:if>
                    <xsl:if test="../Company/IsPreview = 'false'">
                      <xsl:value-of select="$url2"/>
                    </xsl:if>
                  </xsl:attribute>
                      <span name="frontbox" height="400" width="700" class="frontbox"></span><!--<a href="javascript:;" class="fielddef" onClick="DefWindow({$KeyInstnValue}, {$KeyValue}); return true">--><xsl:value-of select="CompanyCalendarTitle"/>
                    </a>
                  </xsl:otherwise>
                </xsl:choose>
              </td>
              <xsl:choose>
                <xsl:when test="$EndDate != '' and ($EndDate != $StartDate or $EndTime != $StartTime) and CompanyCalendarDate != CompanyCalendarDateEnd">
                  <td class="data" align="right" valign="top">
                    <xsl:value-of select="$StartDay"/>
                    <xsl:if test="$StartTime != '12:00:00 AM' and $StartTime != ''">
                      <br/>
                      <!-- Code Display Hour and Minute and Hide Secounds -->
                      <xsl:choose>
                        <xsl:when test="string-length($StartTime) = '10'"><xsl:value-of select="substring($StartTime, 0, 5)"/></xsl:when>
                        <xsl:otherwise><xsl:value-of select="substring($StartTime, 0, 6)"/></xsl:otherwise>
                      </xsl:choose>&#160;<xsl:value-of select="$Meridiem"/>&#160;<xsl:value-of select="TimeZoneLabel"/>
                    </xsl:if>&#160;-&#160;<xsl:value-of select="$EndDay"/>
                    <xsl:if test="$EndTime != '12:00:00 AM' and $EndTime != ''">
                      <br/><xsl:value-of select="$EndTime"/>&#160;<xsl:value-of select="TimeZoneLabel"/>
                    </xsl:if>
                  </td>
                </xsl:when>
                <xsl:otherwise>
                  <td class="data" align="right" valign="top">
                    <xsl:value-of select="$StartDay"/>
                    <xsl:if test="$StartTime != '12:00:00 AM' and $StartTime != ''">
                      <br/>
                      <!-- Code Display Hour and Minute and Hide Secounds -->
                      <xsl:choose>
                        <xsl:when test="string-length($StartTime) = '10'"><xsl:value-of select="substring($StartTime, 0, 5)"/></xsl:when>
                        <xsl:otherwise><xsl:value-of select="substring($StartTime, 0, 6)"/></xsl:otherwise>
                      </xsl:choose>&#160;<xsl:value-of select="$Meridiem"/>&#160;<xsl:value-of select="TimeZoneLabel"/>
                    </xsl:if>
                  </td>
                </xsl:otherwise>
              </xsl:choose>
            </TR>
          </xsl:when>
        </xsl:choose>
      </xsl:for-each>
      <INPUT TYPE="hidden" NAME="EventCount">
        <xsl:attribute name="value">
          <xsl:value-of select="Calendar/CompanyRowCount"/>
        </xsl:attribute>
      </INPUT>
    </table>
    <Br />
  </xsl:if>
</xsl:template>

<xsl:template name="Front_PG_Graph_2">
<xsl:variable name="GraphType" select="normalize-space(GraphInfo/Type)"/>
<xsl:if test="$GraphType != ''">
<xsl:if test="$GraphType != 'NoGraph'">  
<tr>
	<td valign="top" class="data">    
		<table width="100%" cellpadding="2" cellspacing="0" border="0">
				<xsl:if test="$GraphType = 'SP/VolGraphs'">
					<xsl:if test="Prices1/ImageURL != ''">
					<tr>
						<td class="data" align="center">
							<img border="0" alt="">
							<xsl:attribute name="src">
								<xsl:value-of select="Prices1/ImageURL"/>
							</xsl:attribute>

							<xsl:if test="Prices1/Width != ''">
								<xsl:attribute name="width">
									<xsl:value-of select="Prices1/Width"/>
								</xsl:attribute>
							</xsl:if>

							<xsl:if test="Prices1/Height != ''">
								<xsl:attribute name="height">
									<xsl:value-of select="Prices1/Height"/>
								</xsl:attribute>
							</xsl:if>
							</img>
						</td>
					</tr>
					</xsl:if>

					<xsl:if test="Volume/ImageURL != ''">
					<tr>
						<td class="data" align="center">
							<img border="0" alt="">
							<xsl:attribute name="src">
								<xsl:value-of select="Volume/ImageURL"/>
							</xsl:attribute>

							<xsl:if test="Volume/Width != ''">
								<xsl:attribute name="width">
									<xsl:value-of select="Volume/Width"/>
								</xsl:attribute>
							</xsl:if>

							<xsl:if test="Volume/Height != ''">
								<xsl:attribute name="height">
									<xsl:value-of select="Volume/Height"/>
								</xsl:attribute>
							</xsl:if>
							</img>
						</td>
					</tr>
					</xsl:if>

				</xsl:if>

				<xsl:if test="$GraphType = 'FPGraph'">
					<xsl:if test="FPGraph/ImageURL != ''">
					<tr>
						<td class="data" align="center">
							<img border="0" alt="">
							<xsl:attribute name="src">
								<xsl:value-of select="FPGraph/ImageURL"/>
							</xsl:attribute>

							<xsl:if test="FPGraph/Width != ''">
								<xsl:attribute name="width">
									<xsl:value-of select="FPGraph/Width"/>
								</xsl:attribute>
							</xsl:if>

							<xsl:if test="FPGraph/Height != ''">
								<xsl:attribute name="height">
									<xsl:value-of select="FPGraph/Height"/>
								</xsl:attribute>
							</xsl:if>
							</img>
						</td>
					</tr>
					</xsl:if>
				</xsl:if>

				<xsl:choose>
				<xsl:when test="Company/ComparativeIndexShortName != '' and Company/IsSNLIndex='true'">
					<xsl:if test="$GraphType='SP/VolGraphs'">
					<tr>
						<td class="data" align="center">
						<xsl:variable name="keyinstn" select="Company/KeyInstn" />
						<xsl:variable name="keyindex" select="Company/KeyIndex" />
						<xsl:variable name="url" select="concat('index.aspx?iid=', $keyinstn, '&amp;KeyIndex=', $keyindex)" />
							<br />
							<a>
							<xsl:attribute name="href"><xsl:value-of select="$url" /></xsl:attribute>
							<xsl:attribute name="target">_new</xsl:attribute>
								List of all companies in The <xsl:value-of select="Company/ComparativeIndexShortName" disable-output-escaping="yes"/> Index
							</a>
							<br />
						</td>
					</tr>
					</xsl:if>
				</xsl:when>
				</xsl:choose>

				<xsl:if test="$GraphType = 'SP/VolGraphs'">
					<tr>
						<td class="data" align="center">
							<label class="visuallyhidden" for="period">Period</label>Period: <select id="period" name="period" onchange="document.form1.submit();" ><xsl:choose><xsl:when test="Company/GraphPeriod='_pp:3'"><option VALUE="_pp:3" SELECTED="1" >One Month</option></xsl:when><xsl:otherwise><option VALUE="_pp:3">One Month</option></xsl:otherwise></xsl:choose><xsl:choose><xsl:when test="Company/GraphPeriod='_pp:4'"><option VALUE="_pp:4" SELECTED="1" >Three Month</option></xsl:when><xsl:otherwise><option VALUE="_pp:4">Three Month</option></xsl:otherwise></xsl:choose><xsl:choose><xsl:when test="Company/GraphPeriod='_pp:10'"><option VALUE="_pp:10" SELECTED="1" >YTD</option></xsl:when><xsl:otherwise><option VALUE="_pp:10">YTD</option></xsl:otherwise></xsl:choose><xsl:choose><xsl:when test="Company/GraphPeriod='_pp:5'"><option VALUE="_pp:5" SELECTED="1" >One Year</option></xsl:when><xsl:otherwise><option VALUE="_pp:5">One Year</option></xsl:otherwise></xsl:choose><xsl:choose><xsl:when test="Company/GraphPeriod='_pp:6'"><option VALUE="_pp:6" SELECTED="1" >Three Year</option></xsl:when><xsl:otherwise><option VALUE="_pp:6">Three Year</option></xsl:otherwise></xsl:choose></select> <!--<input TYPE="submit" NAME="submit" VALUE="Apply" ID="Submit2"/>-->
						</td>
					</tr>
				</xsl:if>
				</table>
	</td>
</tr>
</xsl:if>
</xsl:if>
<tr>
  <td class="data" valign="top"><xsl:call-template name="Contact_Info_2"/></td>
</tr>
</xsl:template>


<xsl:template name="Contact_Info_2">
  <hr size="1" width="100%" />
  <table border="0" cellpadding="3" cellspacing="0" width="100%">
  <tr><td colspan="2" class="header" valign="top">Contact Information</td></tr>
  <tr><td colspan="2" class="data" valign="top"></td></tr>
  <tr>
    <td valign="top" class="data">
      <table border="0" cellpadding="1" cellspacing="0">
      <tr>
        <td class="data">&#160;</td>
        <td class="data"><span class="large"><xsl:value-of select="Company/IRHomePageText" disable-output-escaping="yes"/><xsl:value-of select="Company/Address" disable-output-escaping="yes"/></span></td>
      </tr>
      </table>
    </td>
    <td class="data"><xsl:value-of select="Company/IRContact" disable-output-escaping="yes"/><xsl:value-of select="Company/TransferAgent" disable-output-escaping="yes"/></td>
  </tr>
  </table>
</xsl:template>

<xsl:template name="WebLinks_footer_2">
<tr class="colorlight" align="center">
<td colspan="2"><span class="large"><xsl:value-of select="WebLinks/BottomLink" disable-output-escaping="yes"/></span></td>
</tr>

</xsl:template>

<xsl:template name="Company_footer_2">
<table  border="0" cellpadding="3" cellspacing="0" width="100%">
<tr>
<td align="CENTER" class="data"><IMG SRC="/images/interactive/blank.gif" width="100%" HEIGHT="5" border="0" alt=""/></td>
</tr>
<tr>
<td class="data" align="left"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span></td>
</tr>
</table>
</xsl:template>

<!-- Template ONE Ends here. -->

<!-- Template TWO Starts  -->
<!-- As template TWO is the same as Template Three only inverted on this page there is no need to reference identical templates. Please See IRW match below. -->
<!-- Template TWO Ends here. -->

<!-- Template THREE Starts  -->

<xsl:template name="Market_Summary_3">
<table width="100%" border="0" cellpadding="3" cellspacing="0" class="table1">  
  <tr>
    <td valign="top" class="header" colspan="2">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td class="header">Market Summary</td>
          <td class="header" align="right">
            <a class="light" href="javascript:;" onMouseOver="javascript:window.status='Desktop Summary'; return true;" onfocus="javascript:window.status='Desktop Summary'; return true;" onMouseOut="javascript:window.status='';return true;" onblur="javascript:window.status='';return true;" title="Desktop Summary">
            <xsl:attribute name="OnClick">window.open('marketsummary.aspx?iid=<xsl:value-of select="Company/KeyInstn"/>', 'MarketSummary', 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,width=185,height=175');window.status='Desktop Summary'; return true;</xsl:attribute><img alt="Desktop Summary" title="Desktop Summary" border="0" src="images/msum.gif"/>

            </a>
          </td>
        </tr> 
      </table>
    </td>
  </tr>
  <tr >
    <td class="table1_item"><span class="default">Trading Symbol</span></td>
    <td class="table1_item"><span class="default"><xsl:value-of select="Company/Ticker"/></span></td>
  </tr>
  <tr class="table1_item">
    <td class="table1_item"><span class="default">Exchange</span></td>
    <td class="table1_item"><span class="default"><xsl:value-of select="Company/Exchange"/></span></td>
  </tr>
  <tr class="table1_item">
    <td class="table1_item"> <span class="default">Market Value ($M)</span></td>
    <td class="table1_item"><span class="default"><nobr><xsl:value-of select="format-number(StockQuote/MarketValueAtCurrentPrice, '###,##0.00')"/></nobr></span></td>
  </tr>
  <tr>
    <td class="table1_item"><span class="default">Stock Quote</span></td>
    <td class="table1_item"><span class="default"><nobr>$&#160;<xsl:value-of select="format-number(StockQuote/CurrentPrice, '###,##0.00')"/></nobr></span></td>
  </tr>
  <tr>
    <td class="table1_item"><span class="default">Change&#160;<xsl:if test="StockQuote/NetChangeDirection = 'positive'"><IMG SRC="/images/Interactive/IR/uptriangle.gif" width="11" HEIGHT="10" border="0" ALT="Up" /></xsl:if><xsl:if test="StockQuote/NetChangeDirection = 'negative'"><IMG SRC="/images/Interactive/IR/downtriangle.gif" width="11" HEIGHT="10" border="0" ALT="Down" /></xsl:if></span></td>
    <td class="table1_item"><span class="default">
      <xsl:variable name="NetChangeFromPreviousClose" select="StockQuote/NetChangeFromPreviousClose"/>
      <xsl:choose><xsl:when test="$NetChangeFromPreviousClose != 0">&#36;&#160;<xsl:value-of select="$NetChangeFromPreviousClose"/></xsl:when><xsl:otherwise>NA</xsl:otherwise></xsl:choose>
      <xsl:variable name="PercentChangeFromPreviousClose" select="StockQuote/PercentChangeFromPreviousClose"/>
      <xsl:choose>
        <xsl:when test="$PercentChangeFromPreviousClose != 0"><BR /><xsl:choose><xsl:when test="$PercentChangeFromPreviousClose &gt; 0"><xsl:if test="StockQuote/NetChangeDirection = 'negative'">(</xsl:if><xsl:value-of select="format-number($PercentChangeFromPreviousClose, '###,##0.00')"/><xsl:if test="StockQuote/NetChangeDirection = 'negative'">)</xsl:if>&#160;<span class="default">&#37;</span></xsl:when><xsl:otherwise><xsl:value-of select="format-number($PercentChangeFromPreviousClose, '###,##0.00')"/>&#160;<span class="default">&#37;</span></xsl:otherwise></xsl:choose></xsl:when><xsl:otherwise><BR />NA</xsl:otherwise></xsl:choose></span>
    </td>
  </tr>
  <tr>
    <td class="table1_item"><span class="default">Volume</span></td>
    <td class="table1_item"><span class="default"><xsl:value-of select="StockQuote/VolumeToday"/></span></td>
  </tr>
  <!-- <tr>
    <td class="table1_item">&#160;</td>
    <td class="table1_item">0.39&#37;</td>
  </tr>
   <tr align="center">
    <td colspan="2" class="data" height="1"><img src="images/graph.jpg" alt="click to view full size" width="180" height="101" /></td>
  </tr>
  <tr align="center">
    <td  class="data" colspan="2" height="1"><img src="images/graph1.jpg" alt="click to view full size" width="180" height="45" /></td>
  </tr> -->
  <tr align="center">
    <td class="table1_item datashade" height="45px" colspan="2" align="center" valign="middle"><span class="default">As of <xsl:value-of select="StockQuote/QuoteTimeStamp"/><BR />Minimum 20 minute delay.</span></td>
  </tr>
</table><br />
</xsl:template>

  <xsl:template name="Webcasts_table_3">
    <xsl:for-each select="Presentation">
      <table border="0" cellpadding="0" cellspacing="3">
        <tr>
          <td valign="top">
            <br/>
            <a target="_blank" title="{Link}">
              <xsl:attribute name="href">
                <xsl:value-of select="Link"/>
              </xsl:attribute>
              <img border="0" name="click here" alt="Click Here">
                <xsl:attribute name="src">
                  <xsl:value-of select="IconPath"/>
                </xsl:attribute>
              </img>
            </a>
          </td>
          <td>
            <font size="-2" face="arial, helvetica, sans-serif" color="#666666">
              <a target="_blank" title="{Link}">
                <xsl:attribute name="href">
                  <xsl:value-of select="Link"/>
                </xsl:attribute>
                <xsl:if test="LinkTitle != ''"><strong><xsl:value-of select="LinkTitle"/></strong></xsl:if>
                <xsl:if test="LinkTitle = ''"> <strong>Webcast</strong></xsl:if>
              </a>
              <br/>
              <xsl:if test="AltLink != ''">
                <a target="_blank" title="{Link}">
                  <xsl:attribute name="href">
                    <xsl:value-of select="AltLink"/>
                  </xsl:attribute>
                  <xsl:if test="AltLinkTitle != ''"><strong><xsl:value-of select="AltLinkTitle"/></strong></xsl:if>
                  <xsl:if test="AltLinkTitle = ''"><strong>Webcast</strong></xsl:if>
                </a>
                <br/>
              </xsl:if>
              <xsl:if test="Title != ''">
                <xsl:value-of select="Title"/>
                <br/>
              </xsl:if>
              <xsl:if test="BodyText != ''">
                <xsl:value-of select="BodyText" disable-output-escaping="yes"/>
                <br/>
              </xsl:if>
              <xsl:value-of select="Date"/>
              <br/>
              <xsl:value-of select="Time"/>&#160;<xsl:value-of select="TimeZone"/>
            </font>
          </td>
        </tr>
      </table>
      <br/>
    </xsl:for-each>
    <br/>
    <br/>
  </xsl:template>

<xsl:template name="Company_Description_3">

  <xsl:if test="count(Presentation) &gt; 0">
    <xsl:call-template name="Webcasts_table_3"/>
  </xsl:if>
  <xsl:if test="WebLinks/TopLink != ''">
  <span class="default"><xsl:value-of select="WebLinks/TopLink" disable-output-escaping="yes"/></span><br />
  </xsl:if>
  <SPAN CLASS="large"><xsl:value-of select="Company/CompanyDescription" disable-output-escaping="yes"/></SPAN>
</xsl:template>

<xsl:template name="Company_News_3">
<xsl:if test="count(PressRelease) > 0">
<table border="0" cellspacing="0" cellpadding="3" width="100%" class="table2">
  <!-- <tr>
    <td class="header" colspan="2">Latest News Stories</td>
  </tr> -->
  <tr>
    <xsl:if test="normalize-space(RSSLinks/InfoLink)">
      <td class="header" Valign="TOP" colspan="2">
      <table border="0" cellspacing="0" cellpadding="0" width="100%">
      <tr>
      <td class="header">Press Releases</td>
      <td align="right" class="" Valign="TOP">
    <a>
           <xsl:attribute name="href"><xsl:value-of select="RSSLinks/FeedLink"/>
           </xsl:attribute>
           <img src="images/rss_xml.gif" border="0" alt="RSS XML"/></a>
           <a>
   	<xsl:variable name="Keyinstn" select="Company/KeyInstn" />
   	<xsl:variable name="RSSLinks" select="RSSLinks/InfoLink" />
   	<xsl:variable name="RSSurl" select="concat($RSSLinks, '?iid=', $Keyinstn)" />
           <xsl:attribute name="href"><xsl:value-of select="$RSSurl"/></xsl:attribute>
        <img src="images/rss_button.gif" border="0" alt="RSS button"/></a>
      </td></tr></table>
      </td>
    </xsl:if>
    <xsl:if test="not(normalize-space(RSSLinks/InfoLink))">
      <td class="header" Valign="TOP" colspan="2">Press Releases</td>
    </xsl:if>
  </tr>
  <xsl:for-each select="PressRelease">
    <tr class="data">
      <td class="data" valign="top" width="15%"><xsl:value-of select="Date" disable-output-escaping="yes"/></td>
      <td class="data" valign="top" align="left"><a><xsl:variable name="url" select="Link" /><xsl:attribute name="target">_new</xsl:attribute><xsl:attribute name="href"><xsl:value-of select="$url" /></xsl:attribute><xsl:value-of select="Headline" disable-output-escaping="yes"/></a><br /><br /></td>
    </tr>
  </xsl:for-each>
</table><br />
</xsl:if>
<xsl:if test="count(PressRelease) = '0'"></xsl:if>

<!-- <xsl:if test="count(NewsSummary) > 0">
<table border="0" cellspacing="0" cellpadding="3" width="200" class="table2">
  <tr>
    <td class="header" Valign="TOP" colspan="2">News Summaries</td>
  </tr>
  <xsl:for-each select="NewsSummary">
  <tr class="data">
    <td class="data" valign="top" width="15%"><xsl:value-of select="Date" disable-output-escaping="yes"/></td>
    <td class="data" valign="top" align="left">
      <a>
        <xsl:variable name="url" select="Link" />
        <xsl:attribute name="target">_new</xsl:attribute>
        <xsl:attribute name="href">
        <xsl:value-of select="$url" />
        </xsl:attribute>
        <xsl:value-of select="Headline" disable-output-escaping="yes"/>
      </a>
    </td>
  </tr>
  </xsl:for-each>
 </table> <br />
</xsl:if>

<xsl:if test="count(NewsSummary) = '0'"></xsl:if> -->
</xsl:template>

<xsl:template name="Company_Calendar_3">
  <xsl:variable name="RowCount" select="Calendar/CompanyRowCount"/>
  <xsl:variable name="RowCountEmpty" select="0"/>
  <xsl:variable name="KeyInstnValue" select="Company/KeyInstn"/>
  <xsl:if test="$RowCount != $RowCountEmpty">
  <script language="javascript" type="text/javascript" src="javascript/frontbox/fbox_conf.js"></script>
  <script language="javascript" type="text/javascript" src="javascript/frontbox/fbox_engine-min.js"></script>
  <link  type="text/css" rel="stylesheet" href="javascript/frontbox/fbox.css" />
    <table border="0" cellspacing="0" cellpadding="3" width="100%" class="table2">
      <TR>
        <TD width="50%" CLASS="header" align="left" valign="bottom">Event</TD>
        <TD width="50%" CLASS="header" align="right" valign="bottom" colspan="4">Date</TD>
      </TR>

      <xsl:for-each select="Calendar">
        <xsl:choose>
          <xsl:when test="$RowCount = $RowCountEmpty">
            <TR>
              <TD COLSPAN="5" CLASS="data">
                <strong>There are no events currently scheduled.</strong>
              </TD>
            </TR>
          </xsl:when>

          <xsl:when test="$RowCount != $RowCountEmpty">
            <xsl:variable name="CompanyCalendarTitle" select="CompanyCalendarTitle"/>
            <xsl:variable name="KeyValue" select="Key"/>

            <xsl:variable name="StartDate" select="CompanyCalendarDate"/>
            <xsl:variable name="StartDay" select="substring-before($StartDate,' ')"/>
            <xsl:variable name="StartTime" select="substring-after($StartDate,' ')"/>

            <xsl:variable name="EndDate" select="CompanyCalendarDateEnd"/>
            <xsl:variable name="EndDay" select="substring-before($EndDate,' ')"/>
            <xsl:variable name="EndTime" select="substring-after($EndDate,' ')"/>

            <xsl:variable name="EventName" select="concat('Event_', $KeyValue)"></xsl:variable>
            <xsl:variable name="RemindName" select="concat('Remind_', $KeyValue)"></xsl:variable>
            <xsl:variable name="RemindValue" select="CalendarNotifyDaysPrior"></xsl:variable>
            <xsl:variable name="StartDateName" select="concat('StartDate_', $KeyValue)"></xsl:variable>
            <xsl:variable name="EventType" select="EventType"/>

			<xsl:variable name="url" select="concat('calendardetail.aspx?IID=', $KeyInstnValue,'&amp;Key=', $KeyValue )"/>
              <xsl:variable name="url2" select="concat('caldesc.aspx?IID=', $KeyInstnValue,'&amp;Key=', $KeyValue )"/>
            <!-- Meridiem variable Print (AM/PM) value -->
            <xsl:variable name="Meridiem" select="substring-after($StartTime,' ')"/>

            <TR>
              <td CLASS="data" align="left" valign="top">
                <xsl:choose>
                  <xsl:when test="$EventType = '0'">
                    <a>
					<xsl:attribute name="href">
					<xsl:value-of select="$url" />&amp;KeyType=3
					</xsl:attribute>
					<span name="frontbox" height="400" width="700" class="frontbox">
					</span>
					<!--<a href="javascript:;" class="fielddef" onClick="DefWindowAlt({$KeyInstnValue}, {$KeyValue}, '3'); return true">-->
					<xsl:value-of select="CompanyCalendarTitle"/>
					</a>
					</xsl:when>

                  <xsl:when test="$EventType = '1'">
                    <a>
					<xsl:attribute name="href">
					<xsl:value-of select="$url" />&amp;KeyType=2
					</xsl:attribute>
					<span name="frontbox" height="400" width="700" class="frontbox"></span>
					<!--<a href="javascript:;" class="fielddef" onClick="DefWindowAlt({$KeyInstnValue}, {$KeyValue}, '2'); return true">-->
					<xsl:value-of select="CompanyCalendarTitle"/>
                    </a>
                  </xsl:when>

                  <xsl:when test="$EventType = '2'">
					<a>
					<xsl:attribute name="href">
					<xsl:value-of select="$url" />&amp;KeyType=1
					</xsl:attribute>
				   	<span name="frontbox" height="400" width="700" class="frontbox"></span>
					<!--<a href="javascript:;" class="fielddef" onClick="DefWindowAlt({$KeyInstnValue}, {$KeyValue}, '1'); return true">-->
					<xsl:value-of select="CompanyCalendarTitle"/>
                    </a>
                  </xsl:when>

                  <xsl:when test="$EventType = '3'">
                    <a>
					<xsl:attribute name="href">
            <xsl:if test="../Company/IsPreview = 'true'">
              <xsl:value-of select="concat($url,'&amp;preview=1')"/>&amp;KeyType=4
            </xsl:if>
            <xsl:if test="../Company/IsPreview = 'false'">
              <xsl:value-of select="$url"/>&amp;KeyType=4
            </xsl:if>
					</xsl:attribute>
					<span name="frontbox" height="400" width="700" class="frontbox"></span>
					<!--<a href="javascript:;" class="fielddef" onClick="DefWindowAlt({$KeyInstnValue}, {$KeyValue}, '4'); return true">-->
					<xsl:value-of select="CompanyCalendarTitle"/>
                    </a>
                  </xsl:when>

                  <xsl:otherwise>
                    <a>
					<xsl:attribute name="href">
            <xsl:if test="../Company/IsPreview = 'true'">
              <xsl:value-of select="concat($url2,'&amp;preview=1')"/>
            </xsl:if>
            <xsl:if test="../Company/IsPreview = 'false'">
              <xsl:value-of select="$url2"/>
            </xsl:if>
					</xsl:attribute>
					<span name="frontbox" height="400" width="700" class="frontbox"></span>
					  <!--<a href="javascript:;" class="fielddef" onClick="DefWindow({$KeyInstnValue}, {$KeyValue}); return true">-->
						<xsl:value-of select="CompanyCalendarTitle"/>
                    </a>

                  </xsl:otherwise>
                </xsl:choose>
              </td>
              <xsl:choose>
                <xsl:when test="$EndDate != '' and ($EndDate != $StartDate or $EndTime != $StartTime) and CompanyCalendarDate != CompanyCalendarDateEnd">
                  <td CLASS="data" align="right" valign="top">
                    <xsl:value-of select="$StartDay"/>
                    <xsl:if test="$StartTime != '12:00:00 AM' and $StartTime != ''">
                      <br/>
                      <!-- Code Display Hour and Minute and Hide Secounds -->
                      <xsl:choose>
                        <xsl:when test="string-length($StartTime) = '10'"><xsl:value-of select="substring($StartTime, 0, 5)"/></xsl:when>
                        <xsl:otherwise><xsl:value-of select="substring($StartTime, 0, 6)"/></xsl:otherwise>
                      </xsl:choose>&#160;<xsl:value-of select="$Meridiem"/>&#160;<xsl:value-of select="TimeZoneLabel"/>
                    </xsl:if>&#160;-&#160;<xsl:value-of select="$EndDay"/>
                    <xsl:if test="$EndTime != '12:00:00 AM' and $EndTime != ''">
                      <br/><xsl:value-of select="$EndTime"/>&#160;<xsl:value-of select="TimeZoneLabel"/>
                    </xsl:if>
                  </td>
                </xsl:when>
                <xsl:otherwise>
                  <td CLASS="data" align="right" valign="top">
                    <xsl:value-of select="$StartDay"/>
                    <xsl:if test="$StartTime != '12:00:00 AM' and $StartTime != ''">
                      <br/>
                      <!-- Code Display Hour and Minute and Hide Secounds -->
                      <xsl:choose>
                        <xsl:when test="string-length($StartTime) = '10'"><xsl:value-of select="substring($StartTime, 0, 5)"/></xsl:when>
                        <xsl:otherwise><xsl:value-of select="substring($StartTime, 0, 6)"/></xsl:otherwise>
                      </xsl:choose>&#160;<xsl:value-of select="$Meridiem"/>&#160;<xsl:value-of select="TimeZoneLabel"/>
                    </xsl:if>
                  </td>
                </xsl:otherwise>
              </xsl:choose>
            </TR>
          </xsl:when>
        </xsl:choose>
      </xsl:for-each>
      <INPUT TYPE="hidden" NAME="EventCount">
        <xsl:attribute name="value">
          <xsl:value-of select="Calendar/CompanyRowCount"/>
        </xsl:attribute>
      </INPUT>
    </table>
    <Br />
  </xsl:if>
</xsl:template>

<xsl:template name="Front_PG_Graph_3">
<xsl:variable name="GraphType" select="GraphInfo/Type"/>
<xsl:if test="$GraphType != ''">
<xsl:if test="$GraphType != 'NoGraph'">  
	<table width="85px" cellpadding="2" cellspacing="0" border="0">
	<xsl:if test="$GraphType = 'SP/VolGraphs'">
<xsl:attribute name="class">table2</xsl:attribute>
		<xsl:if test="Prices1/ImageURL != ''">
		<tr>
			<td class="data" align="center">
			<xsl:if test="Volume/ImageURL = ''">
				<xsl:attribute name="class">data</xsl:attribute>
			</xsl:if>
				<img border="0" alt="">
				<xsl:attribute name="src">
					<xsl:value-of select="Prices1/ImageURL"/>
				</xsl:attribute>

				<xsl:if test="Prices1/Width != ''">
					<xsl:attribute name="width">
						<xsl:value-of select="Prices1/Width"/>
					</xsl:attribute>
				</xsl:if>

				<xsl:if test="Prices1/Height != ''">
					<xsl:attribute name="height">
						<xsl:value-of select="Prices1/Height"/>
					</xsl:attribute>
				</xsl:if>
				</img>
			</td>
		</tr>
		</xsl:if>

		<xsl:if test="Volume/ImageURL != ''">
		<tr>
			<td class="bottom_cap data" align="center">
			<xsl:if test="Prices1/ImageURL = ''">
				<xsl:attribute name="class">table2 data</xsl:attribute>
			</xsl:if>
				<img border="0" alt="">
				<xsl:attribute name="src">
					<xsl:value-of select="Volume/ImageURL"/>
				</xsl:attribute>

				<xsl:if test="Volume/Width != ''">
					<xsl:attribute name="width">
						<xsl:value-of select="Volume/Width"/>
					</xsl:attribute>
				</xsl:if>

				<xsl:if test="Volume/Height != ''">
					<xsl:attribute name="height">
						<xsl:value-of select="Volume/Height"/>
					</xsl:attribute>
				</xsl:if>
				</img>
			</td>
		</tr>
		</xsl:if>

	</xsl:if>

	<xsl:if test="$GraphType = 'FPGraph'">
		<xsl:if test="FPGraph/ImageURL != ''">
		<tr>
			<td class="table2 data" align="center">
				<img border="0" alt="">
				<xsl:attribute name="src">
					<xsl:value-of select="FPGraph/ImageURL"/>
				</xsl:attribute>

				<xsl:if test="FPGraph/Width != ''">
					<xsl:attribute name="width">
						<xsl:value-of select="FPGraph/Width"/>
					</xsl:attribute>
				</xsl:if>

				<xsl:if test="FPGraph/Height != ''">
					<xsl:attribute name="height">
						<xsl:value-of select="FPGraph/Height"/>
					</xsl:attribute>
				</xsl:if>
				</img>
			</td>
		</tr>
		</xsl:if>
	</xsl:if>

	<xsl:choose>
	<xsl:when test="Company/ComparativeIndexShortName != '' and Company/IsSNLIndex='true'">
		<xsl:if test="$GraphType='SP/VolGraphs'">
		<tr>
			<td class="data" align="center">
			<xsl:variable name="keyinstn" select="Company/KeyInstn" />
			<xsl:variable name="keyindex" select="Company/KeyIndex" />
			<xsl:variable name="url" select="concat('index.aspx?iid=', $keyinstn, '&amp;KeyIndex=', $keyindex)" />
				<br />
				<a>
				<xsl:attribute name="href"><xsl:value-of select="$url" /></xsl:attribute>
				<xsl:attribute name="target">_new</xsl:attribute>
					List of all companies in The <xsl:value-of select="Company/ComparativeIndexShortName" disable-output-escaping="yes"/> Index
				</a>
				<br />
			</td>
		</tr>
		</xsl:if>
	</xsl:when>
	</xsl:choose>

	<xsl:if test="$GraphType = 'SP/VolGraphs'">
		<tr>
			<td class="data" align="center">
				<label class="visuallyhidden" for="period">Period</label>Period: <select id="period" name="period" onchange="document.form1.submit();" ><xsl:choose><xsl:when test="Company/GraphPeriod='_pp:3'"><option VALUE="_pp:3" SELECTED="1" >One Month</option></xsl:when><xsl:otherwise><option VALUE="_pp:3">One Month</option></xsl:otherwise></xsl:choose><xsl:choose><xsl:when test="Company/GraphPeriod='_pp:4'"><option VALUE="_pp:4" SELECTED="1" >Three Month</option></xsl:when><xsl:otherwise><option VALUE="_pp:4">Three Month</option></xsl:otherwise></xsl:choose><xsl:choose><xsl:when test="Company/GraphPeriod='_pp:10'"><option VALUE="_pp:10" SELECTED="1" >YTD</option></xsl:when><xsl:otherwise><option VALUE="_pp:10">YTD</option></xsl:otherwise></xsl:choose><xsl:choose><xsl:when test="Company/GraphPeriod='_pp:5'"><option VALUE="_pp:5" SELECTED="1" >One Year</option></xsl:when><xsl:otherwise><option VALUE="_pp:5">One Year</option></xsl:otherwise></xsl:choose><xsl:choose><xsl:when test="Company/GraphPeriod='_pp:6'"><option VALUE="_pp:6" SELECTED="1" >Three Year</option></xsl:when><xsl:otherwise><option VALUE="_pp:6">Three Year</option></xsl:otherwise></xsl:choose></select> <!-- <input TYPE="submit" NAME="submit" VALUE="Apply" ID="Submit2"/> -->
			</td>
		</tr>
	</xsl:if>
	</table>
</xsl:if>
</xsl:if>
</xsl:template>

<xsl:template name="Contact_Info_3">
<table width="100%" border="0" cellpadding="3" cellspacing="0" class="table2">
  <tr>
    <td colspan="2" valign="top" class="header">Contact Information</td>
  </tr>
  <tr>
    <td valign="top" class="data">
      <table border="0" cellpadding="3" cellspacing="0">
        <tr>
          <td class="data"><span class="large"><xsl:value-of select="Company/IRHomePageText" disable-output-escaping="yes"/><xsl:value-of select="Company/Address" disable-output-escaping="yes"/></span></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td class="data"><xsl:value-of select="Company/IRContact" disable-output-escaping="yes"/><xsl:value-of select="Company/TransferAgent" disable-output-escaping="yes"/></td>
  </tr>
</table>

</xsl:template>

<xsl:template name="WebLinks_footer_3">
<tr class="colorlight" align="center">
<td colspan="2" class="data"><span class="large"><xsl:value-of select="WebLinks/BottomLink" disable-output-escaping="yes"/></span></td>
</tr>

</xsl:template>

<xsl:template name="Company_footer_3">
<table  border="0" cellpadding="3" cellspacing="0" width="100%">
<tr>
<td class="data" align="CENTER"><IMG SRC="/images/interactive/blank.gif" width="100%" HEIGHT="5" border="0"  alt=""/></td>
</tr>
<xsl:if test="Company/Footer != ''">
<tr>
  <td class="data" align="left"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span></td>
</tr>
</xsl:if>
</table>
</xsl:template>

<!-- End Template THREE -->

<!-- Main XSLT templates are here. these are the templates which are actually being displayed in the page. this is for TOP LEVEL editing. if you dont need to use the individual templates above you can do your main editing here when pulling THESE templates below you must carry the top comment with them and uncomment them when you drop it into the company specific xslt. -->


<xsl:template match="irw:IRW/irw:CorporateProfile">
<xsl:variable name="TemplateName" select="Company/TemplateName"/>
<xsl:variable name="keyinstn" select="Company/KeyInstn" />
<xsl:variable name="keyindex" select="Company/KeyIndex" />
<xsl:variable name="formaction" select="concat('corporateprofile.aspx?IID=', $keyinstn)">
</xsl:variable>
<!--<form method="POST" id="form1" name="form1"><xsl:attribute name="action"><xsl:value-of select="$formaction"/></xsl:attribute>-->

  <SCRIPT language="javascript" type="text/javascript">
    function DefWindow(KeyInstn, Key) {
    var page = "caldesc.aspx?IID=" + KeyInstn + "&amp;Key=" + Key;
    var winprops = "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=640,height=250";
    window.open(page, "", winprops);
    }

    function DefWindowAlt(KeyInstn, Key, KeyType) {
    var page = "calendardetail.aspx?IID=" + KeyInstn + "&amp;Key=" + Key + "&amp;KeyType=" + KeyType;
    var winprops = "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=640,height=300";
    window.open(page, "", winprops);
    }
  </SCRIPT>


<xsl:choose>
<!-- Start Template 4 -->
<xsl:when test="$TemplateName = 'AltCorp4'">
	<xsl:call-template name="CorporateProfile_4"/>
</xsl:when>
<!-- End Template 4 -->
  
<!-- Template ONE -->
<xsl:when test="$TemplateName = 'AltCorp1'">
<xsl:call-template name="TemplateONEstylesheet" />
<table border="0" cellspacing="0" cellpadding="3" width="100%" CLASS="data">
<xsl:call-template name="Title_S2"/>
<xsl:if test="Company/Header != ''">
<xsl:call-template name="WebLinks_header_2"/>
</xsl:if>
  <tr>
    <td CLASS="leftTOPbord" colspan="2">
      <table border="0" width="100%" cellpadding="6" cellspacing="0">
        <xsl:call-template name="Company_Description_2"/>

        <form method="POST" id="form1" name="form1"><xsl:attribute name="action"><xsl:value-of select="$formaction"/></xsl:attribute>
        <xsl:call-template name="Front_PG_Graph_2"/>
        </form>

        <xsl:if test="WebLinks/BottomLink != ''">
        <xsl:call-template name="WebLinks_footer_2"/>
        </xsl:if>
    </table>
    </td>
  </tr>
</table>
<xsl:if test="Company/Footer != ''">
<xsl:call-template name="Company_footer_2"/>
</xsl:if>
<br />
<xsl:call-template name="Copyright"/>
</xsl:when>
<!-- End Template ONE -->

<!-- Start Template TWO -->
<!-- Template TWO refers to Template THREE because it is a variation of Template THREE. The Market Summary and Company news tables have been moved to the opposite side of the page. -->
<xsl:when test="$TemplateName = 'AltCorp2'">
<xsl:call-template name="TemplateTHREEstylesheet" />

<table border="0" cellspacing="0" cellpadding="3" width="100%" class="data">
  <tr>
    <td valign="top" class="data">
      <xsl:call-template name="Title_T2"/>
      <table border="0" cellpadding="0" cellspacing="0" width="100%" class="data">
        <tr>
          <td valign="top" class="data"><xsl:call-template name="Company_Description_3"/></td>
        </tr>
        <tr>
           <td valign="top" class="data" align="center">
            <form method="POST" id="form1" name="form1"><xsl:attribute name="action"><xsl:value-of select="$formaction"/></xsl:attribute>
           	  <xsl:call-template name="Front_PG_Graph_3"/>
            </form>
           </td>
        </tr>
      </table>
    </td>
    <td valign="top" width="200px" class="data">
      <xsl:call-template name="Market_Summary_3"/>
      <xsl:call-template name="Company_News_3"/>
      <xsl:call-template name="Company_Calendar_3"/>
      <xsl:call-template name="Contact_Info_3"/>
    </td>

    <xsl:if test="WebLinks/BottomLink != ''">
    <xsl:call-template name="WebLinks_footer_3"/>
    </xsl:if>
  </tr>
  </table>
<xsl:if test="Company/Footer != ''">
<xsl:call-template name="Company_footer_3"/>
</xsl:if>
<br />
<xsl:call-template name="Copyright"/>
</xsl:when>
<!-- End Template TWO -->

<!-- Start Template THREE -->

<xsl:when test="$TemplateName = 'AltCorp3'">
<xsl:call-template name="TemplateTHREEstylesheet" />

<table border="0" cellspacing="0" cellpadding="3" width="100%" class="data">
  <tr>
    <td valign="top" width="200px" class="data">
      <xsl:call-template name="Market_Summary_3"/>
      <xsl:call-template name="Company_News_3"/>
      <xsl:call-template name="Company_Calendar_3"/>
      <xsl:call-template name="Contact_Info_3"/>
    </td>
    <td valign="top" class="data">
      <!-- Title_T2 is for keeping toolkit in temp - 3 -->
      <xsl:call-template name="Title_T2"/>
      <table border="0" cellpadding="0" cellspacing="0" width="100%" class="data">
        <tr>
          <td valign="top" class="data"><xsl:call-template name="Company_Description_3"/></td>
        </tr>
        <tr>
           <td valign="top" class="data" align="center">
             <form method="POST" id="form1" name="form1"><xsl:attribute name="action"><xsl:value-of select="$formaction"/></xsl:attribute>
           	    <xsl:call-template name="Front_PG_Graph_3"/>
             </form>
           </td>
        </tr>
      </table>
    </td>
    <xsl:if test="WebLinks/BottomLink != ''">
    <xsl:call-template name="WebLinks_footer_3"/>
    </xsl:if>
  </tr>
  </table>
<xsl:if test="Company/Footer != ''">
<xsl:call-template name="Company_footer_3"/>
</xsl:if>
<br />
<xsl:call-template name="Copyright"/>
</xsl:when>
<!-- End Template THREE -->


<!-- Default Template  -->
<xsl:otherwise>
<!-- Copy from here down

<xsl:template match="irw:IRW/irw:CorporateProfile">
<xsl:variable name="keyinstn" select="Company/KeyInstn" />
<xsl:variable name="keyindex" select="Company/KeyIndex" />
<xsl:variable name="formaction" select="concat('corporateprofile.aspx?IID=', $keyinstn)">
</xsl:variable>
<form method="POST" id="form1" name="form1"><xsl:attribute name="action"><xsl:value-of select="$formaction"/>
</xsl:attribute>
-->
<table border="0" cellspacing="0" cellpadding="3" width="100%" CLASS="colordark">

<!-- <xsl:call-template name="HEADER_CORP"/> -->

<xsl:call-template name="Title_S1"/>
  <tr align="">
    <td CLASS="colordark" colspan="2">
      <table border="0" width="100%" cellpadding="6" cellspacing="0">
        <tr>
          <xsl:call-template name="Company_Description"/>
          <td CLASS="colorlight" align="" Valign="TOP" width="25%" rowspan="2">
          <xsl:call-template name="Market_Summary"/>
            <xsl:call-template name="Company_News"/>
            <xsl:call-template name="Company_Calendar"/>
          <xsl:call-template name="Home_PG_Text"/>
          </td>
        </tr>
        <tr>
      <xsl:choose>
      <xsl:when test="Company/SuppressIRHomePageGraph='True'">
      <td class="colorlight"></td>
      </xsl:when>
      <xsl:otherwise>
          <td class="colorlight" align="center" width="54%" valign="top">
            <form method="POST" id="form1" name="form1"><xsl:attribute name="action"><xsl:value-of select="$formaction"/></xsl:attribute>
			        <xsl:call-template name="Front_PG_Graph"/>
            </form>
          </td>
      </xsl:otherwise>
      </xsl:choose>
        </tr>
        <xsl:if test="WebLinks/BottomLink != ''">
        <xsl:call-template name="WebLinks_footer"/>
        </xsl:if>
      </table>
    </td>
  </tr>
</table>
<xsl:call-template name="WebLinks_footer_main"/>
<br />
<xsl:call-template name="Copyright"/>
<!--
</form>
</xsl:template>

End Copy Here
-->
</xsl:otherwise>

<!--End  Default Template -->

</xsl:choose>
<!--</form>-->
</xsl:template>



  
<!-- CorporateProfile Template Style 4 -->

<xsl:template name="CorpScriptResource_4">
  <xsl:variable name="KeyInstn" select="Company/KeyInstn"/>	
  <xsl:call-template name="CommonScript_4" /> 
  <xsl:if test="not(contains(translate(Company/URL,$ucletters,$lcletters), 'print=1'))">
	  <!--
	  PRformat = "mmmm dS, yyyy"; // June 22nd, 2011 
	  PRformat = "m/dd/yy"; // 6/09/11
	  PRformat = "mm/dd/yyyy"; // 06/09/2011
	  PRformat = "dddd, mmmm dS, yyyy, h:MM:ss TT"; // Saturday, June 9th, 2007, 5:46:21 PM
	  PRformat = "isoDateTime"); // 2007-06-09T17:46:21
	  PRformat =  "fullDate"; // Saturday, June 9, 2007
	  PRformat = "longTime"; // 5:46:22 PM EST
	  PRformat = "longTime", true; // Both lines return, 10:46:21 PM UTC
	  PRformat = "UTC:h:MM:ss TT Z"; // 10:46:21 PM UTC				
	  -->
	<SCRIPT language="javascript" type="text/javascript">
	  <![CDATA[
		var KeyInstn ="]]><xsl:value-of select="$KeyInstn"/><![CDATA[";
    var devPath ="]]><xsl:value-of select="$devPath"/><![CDATA[";
		var num = "";
		var ScriptPath = "]]><xsl:value-of select="$ScriptPath"/><![CDATA[";
		var CssPath = "]]><xsl:value-of select="$CssPath"/><![CDATA[";
		st1=new String(window.location);
		i1=st1.indexOf('&style=');		
		for(j=i1 + 7;j<=i1 + 7;j++) { i3 = st1.charAt(j); num = num + i3; }		
		if(num == '/' || num == ''){ num = ''; }
		if(num == '1'){			
			PRshow = "true"; // true/false
			PRformat = "mm/dd/yyyy";
			document.write('<script language="javascript" type="text/javascript" src="'+ScriptPath+'clientside1.js"><\/script>');
			document.write('<link href="'+CssPath+'clientside1.css" rel="stylesheet" type="text/css" />');
			document.write('<script language="javascript" type="text/javascript" src="'+ScriptPath+'date.format.js"><\/script>');
			document.write('<link href="/Interactive/LookAndFeel/'+KeyInstn+'/'+devPath+'corp_c.css" rel="stylesheet" type="text/css" />');
			document.write('<script language="javascript" type="text/javascript" src="/Interactive/LookAndFeel/'+KeyInstn+'/'+devPath+'corp_c.js"><\/script>');
		} else if(num == '0'){
			document.write('<script language="javascript" type="text/javascript" src="'+ScriptPath+'clientside0.js"><\/script>');
			document.write('<link href="'+CssPath+'clientside0.css" rel="stylesheet" type="text/css" />');
			document.write('<link href="/Interactive/LookAndFeel/'+KeyInstn+'/'+devPath+'corp_ir.css" rel="stylesheet" type="text/css" />');
			document.write('<script language="javascript" type="text/javascript" src="/Interactive/LookAndFeel/'+KeyInstn+'/'+devPath+'corp_ir.js"><\/script>');
		} else {
			document.write('<script language="javascript" type="text/javascript" src="'+ScriptPath+'clientside0.js"><\/script>');
			document.write('<link href="'+CssPath+'clientside0.css" rel="stylesheet" type="text/css" />');
		}
	  ]]>
	</SCRIPT>
  </xsl:if>
</xsl:template>

<xsl:template name="CorporateProfile_4">  
    <xsl:call-template name="CorpScriptResource_4"/>
 <xsl:variable name="TemplateName" select="Company/TemplateName"/> 
	<div id="outer">		
		<div id="corporate_profile">
			<xsl:call-template name="Toolkit_Section_4"/>
			<xsl:call-template name="Header_4"/>
			<xsl:call-template name="MarketSummary_4"/>
			<xsl:call-template name="CompanyDescription_4"/>
			<xsl:call-template name="CompanyNews_4"/>			
			<xsl:call-template name="FrontPGGraph_4"/>
			<xsl:call-template name="CompanyCalendar_4"/>
			<xsl:call-template name="ContactInfo_4"/>
			<xsl:call-template name="Footer_4"/>
			<xsl:call-template name="Copyright_4"/>
			<xsl:call-template name="PRlightboxes"/>
		</div>			
	</div>
</xsl:template>

<xsl:template name="PRlightboxes">
<div id="PRlightboxes">
<!-- Start of Login Dialog -->  
<div id="PRdialog" class="window">
	<div id="PRClose"><a href="#" class="close" title="Close Button"><img src="javascript/Template4/images/common/spacer.gif" id="closeButton"  alt="" /></a></div>
	<div id="PRboxes">
	<iframe id="showpr" src="" frameborder="0" width="100%" height="100%" allowtransparency="true" scrolling="auto"></iframe>
	</div>
</div>
<!-- End of Login Dialog --> 
<!-- Mask to cover the whole screen -->
<div id="PRmask"></div>
</div>
</xsl:template>

<xsl:template name="MarketSummary_4">
	<div id="msum_top_horz">
		<div class="msum_top_horz">
			<h3>Stock Quote</h3>
			<ul>
				<li class="ms_titile"><span class="small"><xsl:value-of select="Company/Exchange"/></span><span class="big"><xsl:value-of select="Company/Ticker"/></span></li>
				<li><span class="bold">Market Value ($M)</span><span><nobr><xsl:value-of select="format-number(StockQuote/MarketValueAtCurrentPrice, '###,##0.00')"/></nobr></span></li>
				<li><span class="bold">Change (&#36;)</span>
          <span><xsl:variable name="NetChangeFromPreviousClose" select="StockQuote/NetChangeFromPreviousClose"/>
                    <xsl:choose>
                      <xsl:when test="$NetChangeFromPreviousClose != 0"><xsl:value-of select="$NetChangeFromPreviousClose"/></xsl:when>
                      <xsl:otherwise>NA</xsl:otherwise>
                    </xsl:choose></span></li>
				<li>
					<span class="bold">Change (&#37;)</span>
					<span>
           <xsl:if test="StockQuote/NetChangeDirection = 'positive'"><xsl:attribute name="class">up_arrw_txt</xsl:attribute></xsl:if>
					 <xsl:if test="StockQuote/NetChangeDirection = 'negative'"><xsl:attribute name="class">down_arrw_txt</xsl:attribute></xsl:if>
						<xsl:variable name="PercentChangeFromPreviousClose" select="StockQuote/PercentChangeFromPreviousClose"/>
            <xsl:choose>
              <xsl:when test="$PercentChangeFromPreviousClose != 0">                        
                  <xsl:choose>
                    <xsl:when test="$PercentChangeFromPreviousClose &gt; 0">
                      <xsl:if test="StockQuote/NetChangeDirection = 'negative'">(</xsl:if><xsl:value-of select="format-number($PercentChangeFromPreviousClose, '###,##0.00')"/><xsl:if test="StockQuote/NetChangeDirection = 'negative'">)</xsl:if>
                    </xsl:when>
                    <xsl:otherwise><xsl:value-of select="format-number($PercentChangeFromPreviousClose, '###,##0.00')"/></xsl:otherwise>
                  </xsl:choose>
              </xsl:when>
              <xsl:otherwise>NA</xsl:otherwise>
            </xsl:choose>
						  <img src="javascript/Template4/images/common/spacer.gif"  alt="">
						  <xsl:if test="StockQuote/NetChangeDirection = 'positive'"><xsl:attribute name="class">up_arrw</xsl:attribute></xsl:if>
						  <xsl:if test="StockQuote/NetChangeDirection = 'negative'"><xsl:attribute name="class">down_arrw</xsl:attribute></xsl:if>
						  </img>
					</span>
				</li>
				<li><span class="bold">Volume</span><span><xsl:value-of select="StockQuote/VolumeToday"/></span></li>
				<li><span class="bold">Stock Quote (&#36;)</span><span><nobr><xsl:value-of select="format-number(StockQuote/CurrentPrice, '###,##0.00')"/></nobr></span></li>
			</ul>
      <div id="QuoteTimeStamp">As of <xsl:value-of select="StockQuote/QuoteTimeStamp"/><span class="hypen"> -</span> Minimum 20 minute delay.</div>
		</div>
	</div>
</xsl:template>

  <xsl:template name="Webcasts_table4">
    <xsl:if test="count(Presentation) &gt; 0">
    <div id="Webcasts">
      <div class="Webcasts">
        <xsl:for-each select="Presentation">
          <table border="0" cellpadding="0" cellspacing="0" id="Webcaststable" width="100%">
            <tr>
              <td valign="top" class="WebcastImg">
                <a target="_blank" href="{Link}" class="WebcastImg" title="Click Here"><img border="0" name="click here" src="{IconPath}" /></a>
              </td>
              <td valign="top" class="WebcastTxt">
                <font size="-2" face="arial, helvetica, sans-serif" color="#666666">
                  <a target="_blank" href="{Link}" class="WebcastLink" title="{LinkTitle}">
                    <xsl:if test="LinkTitle != ''"><xsl:value-of select="LinkTitle"/></xsl:if>
                    <xsl:if test="LinkTitle = ''">Webcast</xsl:if>
                  </a><br/>
                  <xsl:if test="AltLink != ''">
                    <a target="_blank" href="{AltLink}"  class="WebcastAltLink" title="{AltLinkTitle}">
                      <xsl:if test="AltLinkTitle != ''"><xsl:value-of select="AltLinkTitle"/></xsl:if>
                      <xsl:if test="AltLinkTitle = ''">Webcast</xsl:if>
                    </a><br/>
                  </xsl:if>
                  <xsl:if test="Title != ''"><xsl:value-of select="Title"/><br/>
                  </xsl:if>
                  <xsl:if test="BodyText != ''"><xsl:value-of select="BodyText" disable-output-escaping="yes"/><br/></xsl:if>
                  <xsl:value-of select="Date"/><br/>
                  <xsl:value-of select="Time"/>&#160;<xsl:value-of select="TimeZone"/>
                </font>
              </td>
            </tr>
          </table>
        </xsl:for-each>
      </div>
    </div>
    </xsl:if>
  </xsl:template>
  
  <xsl:template name="Web_TopLink4">
    <xsl:if test="WebLinks/TopLink != ''">
      <div id="WebTopLink"><xsl:value-of select="WebLinks/TopLink" disable-output-escaping="yes"/></div>
  </xsl:if>
  </xsl:template>
  
  <xsl:template name="CompanyDescription_4">
	  <xsl:if test="Company/CompanyDescription != ''">
		  <div id="ir_desc">
			  <div class="ir_desc">        
				  <xsl:call-template name="Title_Head_4"/>
          <xsl:call-template name="Webcasts_table4"/>  
          <xsl:call-template name="Web_TopLink4"/>  
				  <xsl:value-of select="Company/CompanyDescription" disable-output-escaping="yes"/>
			  </div>
		  </div>
	  </xsl:if>
  </xsl:template>

<xsl:template name="CompanyNews_4">
<xsl:if test="count(PressRelease) > 0"> 
	<div id="press_rel">
		<div class="press_rel">
			<h3 class="news">News Releases</h3>
			<xsl:if test="normalize-space(RSSLinks/InfoLink)">
				<div class="xml_rss news">
					<a href="{RSSLinks/FeedLink}" title="XML"><img class="xml_button" src="javascript/Template4/images/common/spacer.gif" width="30" height="13" border="0" alt="XML" /></a>
					<a>
					<xsl:variable name="Keyinstn" select="Company/KeyInstn" /><xsl:variable name="RSSLinks" select="RSSLinks/InfoLink" />
					<xsl:variable name="RSSurl" select="concat($RSSLinks, '?iid=', $Keyinstn)" />
					<xsl:attribute name="href"><xsl:value-of select="$RSSurl"/></xsl:attribute>
						<img class="rss_button" src="javascript/Template4/images/common/spacer.gif" width="30" height="13" border="0" alt="RSS"/>
					</a>
				</div>
			</xsl:if>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<thead>
					<tr>
						<td width="14%" class="bold">Date</td>
						<td width="1%"></td>
						<td width="84%" class="bold">Title</td>
						<td width="1%"></td>
					</tr>
				</thead>
				<tbody>
				<xsl:for-each select="PressRelease">
					<tr>
					<xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">alt</xsl:attribute></xsl:if>
						<td>
						<xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">alt</xsl:attribute></xsl:if>
							<xsl:value-of select="Date" disable-output-escaping="yes"/>
						</td>
						<td></td>
						<td>
						<xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">alt</xsl:attribute></xsl:if>
							<a href="#PRdialog"  name="modal">
							<xsl:variable name="url" select="Link" /><xsl:attribute name="rel"><xsl:value-of select="$url" /></xsl:attribute>
							<xsl:value-of select="Headline" disable-output-escaping="yes"/></a>
						</td>
						<td></td>
					</tr>
				</xsl:for-each>
				</tbody>
			</table>
		</div>
		<!-- UI Widget -->
		<xsl:call-template name="Company_News_4_widget"/>
	</div>		
</xsl:if>
</xsl:template>

<xsl:template name="CompanyCalendar_4">
  <xsl:variable name="RowCount" select="Calendar/CompanyRowCount"/>
  <xsl:variable name="RowCountEmpty" select="0"/>
  <xsl:variable name="KeyInstnValue" select="Company/KeyInstn"/>
  <xsl:if test="$RowCount != $RowCountEmpty">
    <script language="javascript" type="text/javascript" src="javascript/frontbox/fbox_conf.js"></script>
    <script language="javascript" type="text/javascript" src="javascript/frontbox/fbox_engine-min.js"></script>
    <link  type="text/css" rel="stylesheet" href="javascript/frontbox/fbox.css" />
    <div id="CorpEvent">
      <div class="CorpEvent">
        <h3 class="event">Events</h3>
          <table border="0" cellspacing="0" cellpadding="0" width="100%">
          <TR>
            <TD width="50%" class="bold" align="left" valign="bottom">Event</TD>
            <TD width="50%" class="bold" align="right" valign="bottom" colspan="4">Date</TD>
          </TR>
            <xsl:for-each select="Calendar">
              <xsl:choose>
                <xsl:when test="$RowCount = $RowCountEmpty">
                  <TR>
                    <TD COLSPAN="5">
                      <strong>There are no events currently scheduled.</strong>
                   </TD>
                  </TR>
                </xsl:when>
                
                <xsl:when test="$RowCount != $RowCountEmpty">
                  <xsl:variable name="CompanyCalendarTitle" select="CompanyCalendarTitle"/>
                  <xsl:variable name="KeyValue" select="Key"/>
                  
                  <xsl:variable name="StartDate" select="CompanyCalendarDate"/>
                  <xsl:variable name="StartDay" select="substring-before($StartDate,' ')"/>
                  <xsl:variable name="StartTime" select="substring-after($StartDate,' ')"/>

                  <xsl:variable name="EndDate" select="CompanyCalendarDateEnd"/>
                  <xsl:variable name="EndDay" select="substring-before($EndDate,' ')"/>
                  <xsl:variable name="EndTime" select="substring-after($EndDate,' ')"/>
                  
                  <xsl:variable name="EventName" select="concat('Event_', $KeyValue)"></xsl:variable>
                  <xsl:variable name="RemindName" select="concat('Remind_', $KeyValue)"></xsl:variable>
                  <xsl:variable name="RemindValue" select="CalendarNotifyDaysPrior"></xsl:variable>
                  <xsl:variable name="StartDateName" select="concat('StartDate_', $KeyValue)"></xsl:variable>
                  <xsl:variable name="EventType" select="EventType"/>

            			<xsl:variable name="url" select="concat('calendardetail.aspx?IID=', $KeyInstnValue,'&amp;Key=', $KeyValue )"/>
                  <xsl:variable name="url2" select="concat('caldesc.aspx?IID=', $KeyInstnValue,'&amp;Key=', $KeyValue )"/>
                  <!-- Meridiem variable Print (AM/PM) value -->
                  <xsl:variable name="Meridiem" select="substring-after($StartTime,' ')"/>

            <TR>
              <td align="left" valign="top">
                <xsl:choose>
                  <xsl:when test="$EventType = '0'">
                    <a><xsl:attribute name="href">
                      <xsl:if test="../Company/IsPreview = 'true'">
                        <xsl:value-of select="concat($url,'&amp;preview=1')"/>&amp;KeyType=3
                      </xsl:if>
                      <xsl:if test="../Company/IsPreview = 'false'">
                        <xsl:value-of select="$url"/>&amp;KeyType=3
                      </xsl:if>
                    </xsl:attribute>
                      <span name="frontbox" height="400" width="700" class="frontbox"></span>
                      <!--<a href="javascript:;" class="fielddef" onClick="DefWindowAlt({$KeyInstnValue}, {$KeyValue}, '3'); return true">--><xsl:value-of select="CompanyCalendarTitle"/></a>
                  </xsl:when>
                  <xsl:when test="$EventType = '1'">
                    <a><xsl:attribute name="href">
                      <xsl:if test="../Company/IsPreview = 'true'">
                        <xsl:value-of select="concat($url,'&amp;preview=1')"/>&amp;KeyType=2
                      </xsl:if>
                      <xsl:if test="../Company/IsPreview = 'false'">
                        <xsl:value-of select="$url"/>&amp;KeyType=2
                      </xsl:if>
                    </xsl:attribute>
                      <span name="frontbox" height="400" width="700" class="frontbox"></span>
                      <!--<a href="javascript:;" class="fielddef" onClick="DefWindowAlt({$KeyInstnValue}, {$KeyValue}, '2'); return true">--><xsl:value-of select="CompanyCalendarTitle"/></a>
                  </xsl:when>
                  <xsl:when test="$EventType = '2'">
                    <a><xsl:attribute name="href">
                      <xsl:if test="../Company/IsPreview = 'true'">
                        <xsl:value-of select="concat($url,'&amp;preview=1')"/>&amp;KeyType=1
                      </xsl:if>
                      <xsl:if test="../Company/IsPreview = 'false'">
                        <xsl:value-of select="$url"/>&amp;KeyType=1
                      </xsl:if>
                    </xsl:attribute>
                      <span name="frontbox" height="400" width="700" class="frontbox"></span>
                      <!--<a href="javascript:;" class="fielddef" onClick="DefWindowAlt({$KeyInstnValue}, {$KeyValue}, '1'); return true">--><xsl:value-of select="CompanyCalendarTitle"/></a>
                  </xsl:when>
                  <xsl:when test="$EventType = '3'">
                    <a><xsl:attribute name="href">
                      <xsl:if test="../Company/IsPreview = 'true'">
                        <xsl:value-of select="concat($url,'&amp;preview=1')"/>&amp;KeyType=4
                      </xsl:if>
                      <xsl:if test="../Company/IsPreview = 'false'">
                        <xsl:value-of select="$url"/>&amp;KeyType=4
                      </xsl:if>
                    </xsl:attribute>
                      <span name="frontbox" height="400" width="700" class="frontbox"></span>
                      <!--<a href="javascript:;" class="fielddef" onClick="DefWindowAlt({$KeyInstnValue}, {$KeyValue}, '4'); return true">--><xsl:value-of select="CompanyCalendarTitle"/></a>
                  </xsl:when>
                  <xsl:otherwise>
                    <a><xsl:attribute name="href">
                      <xsl:if test="../Company/IsPreview = 'true'">
                        <xsl:value-of select="concat($url2,'&amp;preview=1')"/>
                      </xsl:if>
                      <xsl:if test="../Company/IsPreview = 'false'">
                        <xsl:value-of select="$url2"/>
                      </xsl:if>
                    </xsl:attribute>
                      <span name="frontbox" height="400" width="700" class="frontbox"></span>
                      <!--<a href="javascript:;" class="fielddef" onClick="DefWindow({$KeyInstnValue}, {$KeyValue}); return true">--><xsl:value-of select="CompanyCalendarTitle"/></a>
                  </xsl:otherwise>
                </xsl:choose>
              </td>
              <xsl:choose>
                <xsl:when test="$EndDate != '' and ($EndDate != $StartDate or $EndTime != $StartTime) and CompanyCalendarDate != CompanyCalendarDateEnd">
                  <td CLASS="data eventdate" align="right" valign="top">
                    <xsl:value-of select="$StartDay"/>                    
                    <xsl:if test="$StartTime != '12:00:00 AM' and $StartTime != ''">
                      <br/>
                      <!-- Code Display Hour and Minute and Hide Secounds -->
                      <xsl:choose>
                        <xsl:when test="string-length($StartTime) = '10'"><xsl:value-of select="substring($StartTime, 0, 5)"/></xsl:when>
                        <xsl:otherwise><xsl:value-of select="substring($StartTime, 0, 6)"/></xsl:otherwise>
                      </xsl:choose>&#160;<xsl:value-of select="$Meridiem"/>&#160;<xsl:value-of select="TimeZoneLabel"/>
                    </xsl:if>&#160;-&#160;<xsl:value-of select="$EndDay"/>
                    <xsl:if test="$EndTime != '12:00:00 AM' and $EndTime != ''">
                      <br/><xsl:value-of select="$EndTime"/>&#160;<xsl:value-of select="TimeZoneLabel"/>
                    </xsl:if>
                  </td>
                </xsl:when>
                <xsl:otherwise>
                  <td CLASS="data eventdate" align="right" valign="top">
                    <xsl:value-of select="$StartDay"/>
                    <xsl:if test="$StartTime != '12:00:00 AM' and $StartTime != ''">
                      <br/>
                      <!-- Code Display Hour and Minute and Hide Secounds -->
                      <xsl:choose>
                        <xsl:when test="string-length($StartTime) = '10'"><xsl:value-of select="substring($StartTime, 0, 5)"/></xsl:when>
                        <xsl:otherwise><xsl:value-of select="substring($StartTime, 0, 6)"/></xsl:otherwise>
                      </xsl:choose>&#160;<xsl:value-of select="$Meridiem"/>&#160;<xsl:value-of select="TimeZoneLabel"/>
                    </xsl:if>
                  </td>
                </xsl:otherwise>
              </xsl:choose>
            </TR>
          </xsl:when>
        </xsl:choose>
      </xsl:for-each>
          <INPUT TYPE="hidden" NAME="EventCount" value="{Calendar/CompanyRowCount}"/>
        </table>
      </div>
    </div>
  </xsl:if>
</xsl:template>

<xsl:template name="FrontPGGraph_4">
<xsl:variable name="GraphType" select="GraphInfo/Type"/>
<xsl:variable name="keyinstn" select="Company/KeyInstn" />
<xsl:variable name="keyindex" select="Company/KeyIndex" />
<xsl:variable name="formaction" select="concat('corporateprofile.aspx?IID=', $keyinstn)"></xsl:variable>
<xsl:if test="$GraphType != '' and $GraphType != 'NoGraph'">
<div id="graph">
	<div class="graph">
		<form method="POST" id="form1" name="form1">
		<xsl:attribute name="action"><xsl:value-of select="$formaction"/></xsl:attribute>
		<ul class="graph">
		<xsl:if test="$GraphType = 'SP/VolGraphs'">
		<!--<xsl:attribute name="class">table2</xsl:attribute>-->
			<xsl:if test="Prices1/ImageURL != ''">
			<li id="chart">		
				<xsl:if test="Volume/ImageURL = ''">
					<xsl:attribute name="class">data</xsl:attribute>
				</xsl:if>
					<img border="0" alt="">
					<xsl:attribute name="src">
						<xsl:value-of select="Prices1/ImageURL"/>
					</xsl:attribute>

					<xsl:if test="Prices1/Width != ''">
						<xsl:attribute name="width">
							<xsl:value-of select="Prices1/Width"/>
						</xsl:attribute>
					</xsl:if>

					<xsl:if test="Prices1/Height != ''">
						<xsl:attribute name="height">
							<xsl:value-of select="Prices1/Height"/>
						</xsl:attribute>
					</xsl:if>
					</img>			
			</li>
			</xsl:if>

			<xsl:if test="Volume/ImageURL != ''">
			<li id="chart">
				<xsl:if test="Prices1/ImageURL = ''">
					<xsl:attribute name="class">table2 data</xsl:attribute>
				</xsl:if>
				<img border="0" alt="">
				<xsl:attribute name="src">
					<xsl:value-of select="Volume/ImageURL"/>
				</xsl:attribute>

				<xsl:if test="Volume/Width != ''">
					<xsl:attribute name="width">
						<xsl:value-of select="Volume/Width"/>
					</xsl:attribute>
				</xsl:if>

				<xsl:if test="Volume/Height != ''">
					<xsl:attribute name="height">
						<xsl:value-of select="Volume/Height"/>
					</xsl:attribute>
				</xsl:if>
				</img>
			</li>
			</xsl:if>

		</xsl:if>

		<xsl:if test="$GraphType = 'FPGraph'">
			<xsl:if test="FPGraph/ImageURL != ''">
			<li id="chart">
				<img border="0" alt="">
				<xsl:attribute name="src">
					<xsl:value-of select="FPGraph/ImageURL"/>
				</xsl:attribute>

				<xsl:if test="FPGraph/Width != ''">
					<xsl:attribute name="width">
						<xsl:value-of select="FPGraph/Width"/>
					</xsl:attribute>
				</xsl:if>

				<xsl:if test="FPGraph/Height != ''">
					<xsl:attribute name="height">
						<xsl:value-of select="FPGraph/Height"/>
					</xsl:attribute>
				</xsl:if>
				</img>
			</li>
			</xsl:if>
		</xsl:if>

		<xsl:choose>
		<xsl:when test="Company/ComparativeIndexShortName != '' and Company/IsSNLIndex='true'">
			<xsl:if test="$GraphType='SP/VolGraphs'">
			<li>
				<xsl:variable name="url" select="concat('index.aspx?iid=', $keyinstn, '&amp;KeyIndex=', $keyindex)" />
				<br />
				<a>
				<xsl:attribute name="href"><xsl:value-of select="$url" /></xsl:attribute>
				<xsl:attribute name="target">_new</xsl:attribute>
					List of all companies in The <xsl:value-of select="Company/ComparativeIndexShortName" disable-output-escaping="yes"/> Index
				</a>
				<br />
			</li>
			</xsl:if>
		</xsl:when>
		</xsl:choose>

		<xsl:if test="$GraphType = 'SP/VolGraphs'">
			<li id="chartPeriod">
				<span class="period">Period: </span>
				<label class="visuallyhidden" for="period">Period</label><select id="period" name="period" onchange="document.form1.submit();" ><xsl:choose><xsl:when test="Company/GraphPeriod='_pp:3'"><option VALUE="_pp:3" SELECTED="1" >One Month</option></xsl:when><xsl:otherwise><option VALUE="_pp:3">One Month</option></xsl:otherwise></xsl:choose><xsl:choose><xsl:when test="Company/GraphPeriod='_pp:4'"><option VALUE="_pp:4" SELECTED="1" >Three Month</option></xsl:when><xsl:otherwise><option VALUE="_pp:4">Three Month</option></xsl:otherwise></xsl:choose><xsl:choose><xsl:when test="Company/GraphPeriod='_pp:10'"><option VALUE="_pp:10" SELECTED="1" >YTD</option></xsl:when><xsl:otherwise><option VALUE="_pp:10">YTD</option></xsl:otherwise></xsl:choose><xsl:choose><xsl:when test="Company/GraphPeriod='_pp:5'"><option VALUE="_pp:5" SELECTED="1" >One Year</option></xsl:when><xsl:otherwise><option VALUE="_pp:5">One Year</option></xsl:otherwise></xsl:choose><xsl:choose><xsl:when test="Company/GraphPeriod='_pp:6'"><option VALUE="_pp:6" SELECTED="1" >Three Year</option></xsl:when><xsl:otherwise><option VALUE="_pp:6">Three Year</option></xsl:otherwise></xsl:choose></select> <!-- <input TYPE="submit" NAME="submit" VALUE="Apply" ID="Submit2"/> -->			
			</li>
		</xsl:if>
		</ul>
		</form>
	</div>
</div>
</xsl:if>
</xsl:template>

<xsl:template name="ContactInfo_4">
	<div id="contact_info">
		<div class="contact_info">
			<h3>Contact Information</h3>
			<xsl:value-of select="Company/IRHomePageText" disable-output-escaping="yes"/><xsl:value-of select="Company/Address" disable-output-escaping="yes"/>
			<xsl:value-of select="Company/IRContact" disable-output-escaping="yes"/><xsl:value-of select="Company/TransferAgent" disable-output-escaping="yes"/>
		</div>
	</div>
</xsl:template>

<xsl:template name="Custom_Template_4">
	<xsl:if test="Company/Header != ''">
		<div id="prime_rate">
			<div class="prime_rate">
				<xsl:value-of select="Company/Header" disable-output-escaping="yes"/>
			</div>
		</div>
	</xsl:if>
</xsl:template>


<!-- UI Widget -->

<xsl:template name="Company_News_4_widget">
<xsl:if test="count(PressRelease) > 0">
	<div class="press_rel_widget">
		<div class="snl_sliderContent">		
			<h3 class="news">News Releases</h3>
			<xsl:if test="normalize-space(RSSLinks/InfoLink)">
				<div class="xml_rss news">
					<a href="{RSSLinks/FeedLink}" title="XML"><img src="{$ImagePath}xml_button.gif" border="0" alt="XML" /></a>
					<a>
					<xsl:variable name="Keyinstn" select="Company/KeyInstn" /><xsl:variable name="RSSLinks" select="RSSLinks/InfoLink" />
					<xsl:variable name="RSSurl" select="concat($RSSLinks, '?iid=', $Keyinstn)" />
					<xsl:attribute name="href"><xsl:value-of select="$RSSurl"/></xsl:attribute>
						<img src="{$ImagePath}rss_button.gif" border="0" alt="RSS"/>
					</a>
				</div>
			</xsl:if>
			<div class="l_arrow"></div>
			<div class="snl_slidemain">			
				<div id="snl_slider">
					<ul>
					<xsl:for-each select="PressRelease">
						<li>
							<span id="prdata"><xsl:value-of select="Date" disable-output-escaping="yes"/></span>
							<p><a href="#PRdialog"  name="modal" rel="{Link}" title="{Headline}"><xsl:value-of select="Headline" disable-output-escaping="yes"/></a></p>
						</li>
					</xsl:for-each>					
					</ul>
				</div>
			</div>
			<div class="r_arrow"></div>
		</div>
	</div>
</xsl:if>
</xsl:template>
<!-- End CorporateProfile Template Style 4 -->
  

</xsl:stylesheet>
