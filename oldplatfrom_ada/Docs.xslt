<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
]>

<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.ord/TR/REC-html40"
	xmlns:v="urn:schemas-microsoft-com:vml"
	xmlns:irw="http://www.snl.com/xml/irw">

  <xsl:output method="html" media-type="text/html" indent="no"/>

  <!-- Start Default Template here-->

  <!-- //This is the default template. Most changes are to either remove the company name, ticker, or exchange. you can also change the name of the page or remove this row all together. //These templates below are broken up how the page is displayed. -->

  <xsl:template name="Docs_Title">
    <xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>

    <TR>
      <TD class="colordark" NOWRAP="NOWRAP">
        <SPAN class="title1light">
          <xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/>
        </SPAN>
      </TD>
      <TD class="colordark" NOWRAP="NOWRAP" align="right" valign="top">
        <span class="subtitlelight">
          <xsl:value-of select="$CompanyNameCaps"/> (<xsl:value-of select="Company/Exchange"/> - <xsl:value-of select="Company/Ticker"/>)
        </span>
        <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
      </TD>
    </TR>
    <TR class="default" align="left">
      <TD class="colorlight" colspan="2">
        <span class="default">
          <xsl:value-of select="Company/Header" disable-output-escaping="yes"/>
        </span>
      </TD>
    </TR>
  </xsl:template>

  <xsl:template name="Docs_Highlights">

    <TR>
      <TD colspan="4" class="colorlight">
        <span class="title2">Highlights</span>
      </TD>
    </TR>

    <tr>
      <TD WIDTH="40%" class="header">Filing Description</TD>
      <TD WIDTH="15%" class="header" NOWRAP="1">Filing Type</TD>
      <TD ALIGN="RIGHT" class="header" WIDTH="15%" NOWRAP="1">Filing Date</TD>
      <TD ALIGN="RIGHT" class="header" WIDTH="15%" NOWRAP="1">Event Date</TD>
      <TD ALIGN="center" class="header" WIDTH="15%" NOWRAP="1">Options</TD>
    </tr>

    <xsl:for-each select="Highlight">

      <TR VALIGN="TOP">
        <TD WIDTH="40%" class="data">
          <xsl:if test="KeyDoc != '0'">
            <a>
              <xsl:attribute name="href"><xsl:value-of select="DocLink"/></xsl:attribute>
              <xsl:value-of select="FilingDescription"/>
            </a>
          </xsl:if>
          <xsl:if test="KeyDoc = '0'">
            <xsl:value-of select="FilingDescription"/>
          </xsl:if>
        </TD>
        <TD WIDTH="15%" class="data">
          <xsl:if test="FilingType != '(none)'">
            <A class="fielddef" title="{FilingType}">
              <xsl:attribute name="href"><xsl:value-of select="FilingTypeLink"/>
              </xsl:attribute>
              <xsl:value-of select="FilingType"/>
            </A>
            <!-- <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
             [<a class="fielddef">
              <xsl:attribute name="href">
                <xsl:value-of select="../Company/ShowAllLink"/>&amp;mask=<xsl:value-of select="FilingType"/>
              </xsl:attribute>filter</a>]-->
          </xsl:if>
          <xsl:if test="FilingType = '(none)'">
            <xsl:value-of select="FilingType"/>
          </xsl:if>
        </TD>
        <TD ALIGN="RIGHT" WIDTH="15%" class="data">
          <xsl:value-of select="FilingDate"/>
        </TD>
        <TD ALIGN="RIGHT" WIDTH="15%" class="data">
          <xsl:value-of select="EventDate"/>
        </TD>
        <TD ALIGN="left" class="data" NOWRAP="1">          
<!--=====================================================================-->
			<table border="0" cellpadding="0" cellspacing="2">
				<tr>
					<td>
						<xsl:choose>					
						<xsl:when test="KeyFileFormat = '2'">
							<a href="file.aspx?IID={../Company/KeyInstn}&amp;FID={KeyFile}&amp;O=2&amp;OSID=9" target="_new"><img class="irhtml" src="/images/interactive/ir/html.gif" alt="Download SEC Filing to HTML" title="Download SEC Filing to HTML" border="0"/></a>
						</xsl:when>
							<xsl:when test="KeyFileFormat = '1'">
								<a href="file.aspx?IID={../Company/KeyInstn}&amp;FID={KeyFile}&amp;O=2&amp;OSID=9" target="_new"><img class="irhtml" src="/images/interactive/ir/html.gif" alt="Download SEC Filing to HTML" title="Download SEC Filing to HTML" border="0"/></a>
							</xsl:when>
							<xsl:when test="KeyFileFormat = '7'">
                <a href="file.aspx?IID={../Company/KeyInstn}&amp;FID={KeyFile}&amp;O=2&amp;OSID=9" target="_new"><img class="irhtml" src="/images/interactive/ir/html.gif" alt="Download SEC Filing to HTML" title="Download SEC Filing to HTML" border="0"/></a>
						</xsl:when>
						<xsl:otherwise>
              <IMG class="irhtml_gray" SRC="/images/interactive/ir/html_gray.gif" alt="HTML SEC Filing Unavailable" title="HTML SEC Filing Unavailable"  border="0"/>
						</xsl:otherwise>			
						</xsl:choose>
					</td>
					
					<td>
						<xsl:choose>
							<xsl:when test="KeyFileFormat = '1'">
								<xsl:if test="normalize-space(PDFConvertible) = 'true'">
									<a href="file.aspx?IID={../Company/KeyInstn}&amp;FID={KeyFile}&amp;O=3&amp;OSID=9" target="_new"><img class="irpdf" src="/images/interactive/ir/pdf.gif" alt="Download SEC Filing to Adobe PDF" title="Download SEC Filing to Adobe PDF" border="0"/></a>
								</xsl:if>
								<xsl:if test="normalize-space(PDFConvertible) = 'false'">
									<IMG class="irpdf_gray" SRC="/images/interactive/ir/pdf_gray.gif" alt="Adobe PDF SEC Filing Unavailable" title="Adobe PDF SEC Filing Unavailable" border="0"/>
								</xsl:if>
							</xsl:when>
							<xsl:when test="KeyFileFormat = '2'">
								<xsl:if test="normalize-space(PDFConvertible) = 'true'">
									<a href="file.aspx?IID={../Company/KeyInstn}&amp;FID={KeyFile}&amp;O=3&amp;OSID=9" target="_new"><img class="irpdf" src="/images/interactive/ir/pdf.gif" alt="Download SEC Filing to Adobe PDF" title="Download SEC Filing to Adobe PDF" border="0"/></a>
								</xsl:if>
								<xsl:if test="normalize-space(PDFConvertible) = 'false'">
									<IMG class="irpdf_gray" SRC="/images/interactive/ir/pdf_gray.gif" alt="Adobe PDF SEC Filing Unavailable" title="Adobe PDF SEC Filing Unavailable" border="0"/>
								</xsl:if>
							</xsl:when>
							<xsl:when test="KeyFileFormat = '3'">
								<a href="file.aspx?IID={../Company/KeyInstn}&amp;FID={KeyFile}&amp;O=3&amp;OSID=9" target="_new"><img class="irpdf_gray" src="/images/interactive/ir/pdf.gif" alt="Download SEC Filing to Adobe PDF" title="Download SEC Filing to Adobe PDF" border="0"/></a>
							</xsl:when>
							<xsl:otherwise>
								<IMG class="irpdf_gray" SRC="/images/interactive/ir/pdf_gray.gif" alt="Adobe PDF SEC Filing Unavailable" title="Adobe PDF SEC Filing Unavailable" border="0"/>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:if test="ExcelFileFormat = '1'"> <!-- and (../Company/GAAP = 21 or ../Company/GAAP = 14 or ../Company/GAAP = 20 or FilingType = '10-Q' or FilingType = '10-K')"> -->
							<a style="margin-right: 0px;" href="{DocLink}"><img src="/images/interactive/ir/excel.gif" alt="Download SEC Filing to Excel" title="Download SEC Filing to Excel" border="0"/></a>
						</xsl:if>
						<xsl:if test="ExcelFileFormat = '0'"> <!-- and (../Company/GAAP = 21 or ../Company/GAAP = 14 or ../Company/GAAP = 20) "> -->
							<IMG SRC="/images/interactive/ir/excel_gray.gif" alt="Excel SEC Filing Unavailable" title="Excel SEC Filing Unavailable"  border="0"/>
						</xsl:if>
					</td>
          <td>
            <xsl:if test="XBRLFileFormat = 'True'">
              <a href="{DocLink}"><img class="irxbrl" src="/images/interactive/ir/xbrl.gif" border="0" alt="Download XBRL Content" title="Download SEC Filing to XBRL"/></a>
            </xsl:if>
            <xsl:if test="XBRLFileFormat = 'False'">
              <IMG class="irxbrl_gray" SRC="/images/interactive/ir/xbrl_gray.gif" border="0" alt="XBRL SEC Filing Unavailable" title="XBRL SEC Filing Unavailable"/>
            </xsl:if>
          </td>
				</tr>
			</table>
<!--=====================================================================-->
        </TD>
      </TR>

    </xsl:for-each>

  </xsl:template>

  <xsl:template name="Docs_Links">
    <xsl:if test="Company/ShowEmailNotificationLink = 'true'">
      <!--<xsl:for-each select="CLIENTEMAILPREFS">-->
      <xsl:if test="CLIENTEMAILPREFS/NotificationType = '2' or CLIENTEMAILPREFS/NotificationType = '30' or CLIENTEMAILPREFS/NotificationType = '31'">
        <TR>
          <TD colspan="5" class="default" nowrap="1">
            <a><xsl:attribute name="href">email.aspx?IID=<xsl:value-of select="Company/KeyInstn"/></xsl:attribute>Notify me of new Regulatory Filings and Reports posted to this site</a>.
          </TD>
        </TR>
      </xsl:if>
      <!--</xsl:for-each>-->
    </xsl:if>

    <TR>
      <TD colspan="5" class="default" nowrap="1">
        <xsl:if test="Company/ShowInsiderDocsLink = 'true'">
          <a><xsl:attribute name="href">insiders.aspx?IID=<xsl:value-of select="Company/KeyInstn"/></xsl:attribute>View Insider related Regulatory Filings: Forms 3, 4 &amp; 5</a>.
        </xsl:if>
      </TD>
     </TR>
     <TR>
      <TD colspan="5" ALIGN="left" class="default"><br/>
        <a target="_blank">
          <xsl:attribute name="href">docdescriptions.aspx?IID=<xsl:value-of select="Company/KeyInstn"/>
          </xsl:attribute>
          View Document Grouping descriptions
        </a>
      </TD>
    </TR>

    <tr>
      <td colspan="5" nowrap="1">
        <table cellspacing="0" cellpadding="3" border="0" width="100">
          <tr>
          <!--added 2 for condition as by default this will have All Companies also-->
          <xsl:if test="count(AvailableCompanies) > 2">
            <td nowrap="1"><BR/>
              <xsl:attribute name="action">
                <xsl:choose>
                  <xsl:when  test="not(contains(Company/URL, 'print=1') or contains(Company/URL, 'Print=1'))">
                    <xsl:value-of select="concat('docs.aspx?IID=', Company/KeyInstn)"/>
                  </xsl:when>
                  <xsl:otherwise><xsl:value-of select="concat('docs.aspx?IID=', Company/KeyInstn,'&amp;print=1')"/></xsl:otherwise>
                </xsl:choose>
              </xsl:attribute>
              <!--<span class="defaultbold">Company Filters: </span>-->
              <select name="CompanyFilter" id="CompanyFilter" onchange="document.getElementById('MaskForm').submit();">
          <xsl:for-each select="AvailableCompanies">
            <option value="{KeyInstn}">
              <xsl:if test="Selected != ''">
                <xsl:attribute name="selected"><xsl:value-of select="Selected"/></xsl:attribute>
              </xsl:if>
              <xsl:value-of select="CompanyName"/>
            </option>
          </xsl:for-each>
        </select>
            </td>
          </xsl:if>  
          
          <xsl:if test="count(AvailableTypes) > 0">
            <td nowrap="1"><BR/>
              <!--<xsl:attribute name="action"><xsl:value-of select="concat('docs.aspx?IID=', Company/KeyInstn)"/></xsl:attribute>-->
              <!--<span class="defaultbold">Display: </span>-->
              <select name="TypeMask" id="TypeMask" onchange="document.getElementById('MaskForm').submit();">
              <xsl:for-each select="AvailableTypes">
                <option value="{TypeName}">
                  <xsl:if test="Selected != ''">
                    <xsl:attribute name="selected"><xsl:value-of select="Selected"/></xsl:attribute>
                  </xsl:if>
                  <xsl:value-of select="TypeName"/>
                </option>
              </xsl:for-each>
            </select>
            </td>
            <td nowrap="1"><BR/>
              <!--<xsl:attribute name="action"><xsl:value-of select="concat('docs.aspx?&amp;IID=', Company/KeyInstn)"/></xsl:attribute>-->
              <!--<span class="defaultbold">Year: </span>-->
              <select name="YearMask" id="YearMask" onchange="document.getElementById('MaskForm').submit();">
              <xsl:for-each select="AvailableYears">
                <option value="{Year}">
                  <xsl:if test="Selected != ''">
                    <xsl:attribute name="selected"><xsl:value-of select="Selected"/></xsl:attribute>
                  </xsl:if>
                  <xsl:value-of select="Year"/>
                </option>
              </xsl:for-each>
            </select>
            </td>
          </xsl:if>
          </tr>
        </table>
      </td>
    </tr>
  </xsl:template>

  <xsl:template name="Docs_Other_Filings">
    <TR>
      <xsl:if test="Company/Mask = ''">
        <TD colspan="4" class="colorlight">
          <span class="title2">Other Filings</span>
        </TD>
      </xsl:if>
      <xsl:if test="Company/Mask != '' and Company/Mask != '345' and Company/Mask != '3' and Company/Mask != '4' and Company/Mask != '5'">
        <TD colspan="4" class="colorlight">
          <span class="title2">
            Type: <xsl:value-of select="Company/Mask"/> Filings
          </span>
        </TD>
      </xsl:if>
      <xsl:if test="Company/Mask = '345' or Company/Mask = '3' or Company/Mask = '4' or Company/Mask = '5'">
        <TD colspan="4" class="colorlight">
          <span class="title2">Insider Ownership Filings</span>
        </TD>
      </xsl:if>
    </TR>

    <tr>
      <TD WIDTH="40%" class="header">Filing Description</TD>
      <TD WIDTH="15%" class="header" NOWRAP="1">Filing Type</TD>
      <TD ALIGN="RIGHT" class="header" WIDTH="15%" NOWRAP="1">Filing Date</TD>
      <TD ALIGN="RIGHT" class="header" WIDTH="15%" NOWRAP="1">Event Date</TD>
      <TD ALIGN="center" class="header" WIDTH="15%" NOWRAP="1">Options</TD>
    </tr>

    <xsl:for-each select="OtherFiling">

      <TR VALIGN="TOP">
        <TD WIDTH="40%" class="data">
          <xsl:if test="position() mod 2 = 0">
            <xsl:attribute name="class">datashade</xsl:attribute>
          </xsl:if>
          <a>
            <xsl:attribute name="href"><xsl:value-of select="DocLink"/>
            </xsl:attribute>
            <xsl:value-of select="FilingDescription"/>
          </a>
        </TD>
        <TD WIDTH="15%" class="data">
          <xsl:if test="position() mod 2 = 0">
            <xsl:attribute name="class">datashade</xsl:attribute>
          </xsl:if>
          <A class="fielddef">
            <xsl:attribute name="href"><xsl:value-of select="FilingTypeLink"/>
            </xsl:attribute>
            <xsl:value-of select="FilingType"/>
          </A>
        </TD>
        <TD ALIGN="RIGHT" WIDTH="15%" class="data">
          <xsl:if test="position() mod 2 = 0">
            <xsl:attribute name="class">datashade</xsl:attribute>
          </xsl:if>
          <xsl:value-of select="FilingDate"/>
        </TD>
        <TD ALIGN="RIGHT" WIDTH="15%" class="data">
          <xsl:if test="position() mod 2 = 0">
            <xsl:attribute name="class">datashade</xsl:attribute>
          </xsl:if>
          <xsl:value-of select="EventDate"/>
        </TD>
        <TD ALIGN="left" NOWRAP="1" class="data">
          <xsl:if test="position() mod 2 = 0">
            <xsl:attribute name="class">datashade</xsl:attribute>
          </xsl:if>
<!--=====================================================================-->
			<table border="0" cellpadding="0" cellspacing="2">
				<tr>
					<td>
						<xsl:choose>
							<xsl:when test="KeyFileFormat = '2'">
								<a href="file.aspx?IID={../Company/KeyInstn}&amp;FID={KeyFile}&amp;O=2&amp;OSID=9" target="_new"><img class="irhtml" src="/images/interactive/ir/html.gif" alt="Download SEC Filing to HTML" title="Download SEC Filing to HTML" border="0"/></a>
							</xsl:when>
							<xsl:when test="KeyFileFormat = '1'">
								<a href="file.aspx?IID={../Company/KeyInstn}&amp;FID={KeyFile}&amp;O=2&amp;OSID=9" target="_new"><img class="irhtml" src="/images/interactive/ir/html.gif" alt="Download SEC Filing to HTML" title="Download SEC Filing to HTML" border="0"/></a>
							</xsl:when>
							<xsl:when test="KeyFileFormat = '7'">
								<a href="file.aspx?IID={../Company/KeyInstn}&amp;FID={KeyFile}&amp;O=2&amp;OSID=9" target="_new"><img class="irhtml" src="/images/interactive/ir/html.gif" alt="Download SEC Filing to HTML" title="Download SEC Filing to HTML" border="0"/></a>
							</xsl:when>
							<xsl:otherwise>
								<IMG class="irhtml_gray" SRC="/images/interactive/ir/html_gray.gif" alt="HTML SEC Filing Unavailable" title="HTML SEC Filing Unavailable" border="0"/>
							</xsl:otherwise>			
						</xsl:choose>
					</td>
					<!--<td>
						<xsl:choose>
							<xsl:when test="KeyFileFormat = '10'">
								<a href="file.aspx?IID={../Company/KeyInstn}&amp;FID={KeyFile}&amp;O=10&amp;OSID=9" target="_new">
									<img class="irppt" src="/images/interactive/ir/ppt.gif" alt="Download SEC Filing to Word" title="Download SEC Filing to Word" border="0"/>
								</a>
							</xsl:when>
							<xsl:otherwise>
              <img class="irppt_gray" src="/images/interactive/ir/ppt_gray.gif" alt="Word version of SEC Filing is Unavailable" title="Download SEC Filing to Word" border="0"/>								
							</xsl:otherwise>
						</xsl:choose>
					</td>-->
					<td>
						<xsl:choose>
							<xsl:when test="KeyFileFormat = '1'">
								<xsl:if test="normalize-space(PDFConvertible) = 'true'">
									<a href="file.aspx?IID={../Company/KeyInstn}&amp;FID={KeyFile}&amp;O=3&amp;OSID=9" target="_new"><img class="irpdf" src="/images/interactive/ir/pdf.gif" alt="Download SEC Filing to Adobe PDF"  title="Download SEC Filing to Adobe PDF" border="0"/></a>
								</xsl:if>
								<xsl:if test="normalize-space(PDFConvertible) = 'false'">
									<img class="irpdf_gray" SRC="/images/interactive/ir/pdf_gray.gif" alt="Adobe PDF SEC Filing Unavailable" title="Adobe PDF SEC Filing Unavailable" border="0"/>
								</xsl:if>
							</xsl:when>
							<xsl:when test="KeyFileFormat = '2'">
								<xsl:if test="normalize-space(PDFConvertible) = 'true'">
									<a href="file.aspx?IID={../Company/KeyInstn}&amp;FID={KeyFile}&amp;O=3&amp;OSID=9" target="_new"><img src="/images/interactive/ir/pdf.gif" alt="Download SEC Filing to Adobe PDF" title="Download SEC Filing to Adobe PDF" border="0"/></a>
								</xsl:if>
								<xsl:if test="normalize-space(PDFConvertible) = 'false'">
									<img class="irpdf_gray" SRC="/images/interactive/ir/pdf_gray.gif" alt="Adobe PDF SEC Filing Unavailable" title="Adobe PDF SEC Filing Unavailable" border="0"/>
								</xsl:if>
							</xsl:when>
							<xsl:when test="KeyFileFormat = '3'">
								<a href="file.aspx?IID={../Company/KeyInstn}&amp;FID={KeyFile}&amp;O=3&amp;OSID=9" target="_new"><img class="irpdf" src="/images/interactive/ir/pdf.gif" alt="Download SEC Filing to Adobe PDF" title="Download SEC Filing to Adobe PDF" border="0"/></a>
							</xsl:when>
							<xsl:otherwise>
								<img class="irpdf_gray" SRC="/images/interactive/ir/pdf_gray.gif" alt="Adobe PDF SEC Filing Unavailable" title="Adobe PDF SEC Filing Unavailable" border="0"/>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:if test="ExcelFileFormat = '1'"> <!-- and (../Company/GAAP = 21 or ../Company/GAAP = 14 or ../Company/GAAP = 20 or FilingType = '10-Q' or FilingType = '10-K')"> -->
							<a href="{DocLink}"><img class="irexcel" src="/images/interactive/ir/excel.gif" alt="Download SEC Filing to Excel" title="Download SEC Filing to Excel" border="0"/></a>
						</xsl:if>
						<xsl:if test="ExcelFileFormat = '0'"> <!-- and (../Company/GAAP = 21 or ../Company/GAAP = 14 or ../Company/GAAP = 20) "> -->
							<img class="irexcel_gray" src="/images/interactive/ir/excel_gray.gif" alt="Excel SEC Filing Unavailable" title="Excel SEC Filing Unavailable"  border="0"/>              
						</xsl:if>
					</td>
          <td>
            <xsl:if test="XBRLFileFormat = 'True'">
              <a style="margin-right: 0px;" href="{DocLink}">
                <img class="irxbrl" src="/images/interactive/ir/xbrl.gif" border="0" alt="Download XBRL Content" title="Download SEC Filing to XBRL"/>
              </a>
            </xsl:if>
            <xsl:if test="XBRLFileFormat = 'False'">
              <img class="irxbrl_gray" src="/images/interactive/ir/xbrl_gray.gif" border="0" alt="XBRL SEC Filing Unavailable" title="XBRL SEC Filing Unavailable"/>
            </xsl:if>
          </td>
				</tr>
			</table>		
<!--=====================================================================-->
        </TD>
      </TR>
      <xsl:choose>
        <xsl:when test="(../Company/CompanyDisplayType='0')">
          <TR VALIGN="TOP">
            <TD class="data" colspan="5">
              <xsl:if test="position() mod 2 = 0">
                <xsl:attribute name="class">datashade</xsl:attribute>
              </xsl:if>
              <span class="italicfont">
                <xsl:value-of select="InstnName"/>
              </span>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="((../Company/CompanyDisplayType='1') and (keyInstn != TreeID))">
          <TR>
            <TD class="data" colspan="5">
              <xsl:if test="position() mod 2 = 0">
                <xsl:attribute name="class">datashade</xsl:attribute>
              </xsl:if>
              <span class="italicfont">
                <xsl:value-of select="InstnName"/>
              </span>
            </TD>
          </TR>
        </xsl:when>
      </xsl:choose>
      <xsl:if test="Abstract != ''">
        <TR VALIGN="TOP" class="data">
          <TD colspan="4" class="data">
            <xsl:if test="position() mod 2 = 0">
              <xsl:attribute name="class">datashade</xsl:attribute>
            </xsl:if>              
              <xsl:if test="Abstract != ''">Abstract: <xsl:value-of select="Abstract"/></xsl:if>
          </TD>
          <TD>
            <xsl:if test="position() mod 2 = 0">
              <xsl:attribute name="class">datashade</xsl:attribute>
            </xsl:if>
          </TD>
        </TR>
      </xsl:if>
    </xsl:for-each>

    <xsl:variable name="EndPage" select="Company/Start + Company/RowsPerPage - 1"/>

    <TR VALIGN="TOP" class="data"> 
    	<TD colspan="2" class="defaultbold"> &#160; </TD> 
    	<TD colspan="2" ALIGN="RIGHT"> 
    	<xsl:variable name="selected_year" select="AvailableYears[Selected = 'selected']/Year" />
    	<xsl:for-each select="Pagination"> 
    	<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text> 
    	<xsl:choose> 
    	<xsl:when test="CurrentPage = 'false'"> <a target="_self"> <xsl:attribute name="href"> <xsl:value-of select="PageLink"/> <xsl:if test="$selected_year != '' and $selected_year != 'All Years'">&amp;YearMask&#61;<xsl:value-of select="$selected_year"/></xsl:if>   </xsl:attribute> <xsl:value-of select="PageLabel"/> </a> </xsl:when> <xsl:otherwise> <strong> <xsl:value-of select="PageLabel"/> </strong> </xsl:otherwise> </xsl:choose> </xsl:for-each> </TD>
    </TR>

  </xsl:template>
  <xsl:template name="Docs_Footer">
    <TABLE  border="0" cellpadding="3" cellspacing="0" width="100%">
      <TR class="default" align="left">
        <TD class="colorlight" colspan="2">
          <span class="default">
            <xsl:value-of select="Company/Footer" disable-output-escaping="yes"/>
          </span>
        </TD>
      </TR>
    </TABLE>
  </xsl:template>

  <!-- Default Template Ends here. all templates below are ONLY if you are using the 'Style Template' section in the console. -->



















  <!-- This is Template ONE option in the console under 'Style Template'
//This template is a striped down version of the main template. Everything (data) is displayed in single rows.
//The sections will be commented on where the rows are. you should be able to just move rows (entire chunks of data) where you need too. Edits to this section are rare, mostly due to size (width of the page).
-->

  <xsl:template name="Docs_Title2">
    <xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
    <TR>
      <TD class=" titletest" nowrap="" colspan="2">
        <xsl:value-of select="TitleInfo/TitleName" disable-output-escaping="yes"/>
        <br />
        <IMG SRC="" WIDTH="2" HEIGHT="1" alt="{$CompanyNameCaps}" />
        <span class=" titletest2">
          <xsl:value-of select="$CompanyNameCaps"/>&#160;(<xsl:value-of select="Company/Exchange"/>&#160;-&#160;<xsl:value-of select="Company/Ticker"/>)&#160;
        </span>
      </TD>

    </TR>
    <TR class="default" align="left">
      <TD class="colorlight" colspan="2">
        <span class="default">
          <xsl:value-of select="Company/Header" disable-output-escaping="yes"/>
        </span>
      </TD>
    </TR>


  </xsl:template>




  <xsl:template name="Docs_Links2">
      <form name="MaskForm" id="MaskForm"  method="Post">
        <xsl:attribute name="action">
        <xsl:choose>
            <xsl:when  test="not(contains(Company/URL, 'print=1') or contains(Company/URL, 'Print=1'))">
            <xsl:value-of select="concat('docs.aspx?IID=', Company/KeyInstn)"/>
            </xsl:when>
            <xsl:otherwise><xsl:value-of select="concat('docs.aspx?IID=', Company/KeyInstn,'&amp;print=1')"/></xsl:otherwise>
        </xsl:choose>
        </xsl:attribute>
      <TABLE cellpadding="0" cellspacing="0" border="0" width="100%">
        <xsl:if test="Company/ShowEmailNotificationLink = 'true'">
          <!--<xsl:for-each select="CLIENTEMAILPREFS[NotificationType = '2']">-->
          <xsl:if test="CLIENTEMAILPREFS/NotificationType = '2' or CLIENTEMAILPREFS/NotificationType = '30' or CLIENTEMAILPREFS/NotificationType = '31'">
            <TR>
              <TD colspan="4" class="default" valign="top">
                <a>
                  <xsl:attribute name="href">
                    email.aspx?IID=<xsl:value-of select="Company/KeyInstn"/>
                  </xsl:attribute>
                  Notify me of new Regulatory Filings and Reports posted to this site</a>.
              </TD>
            </TR>
          </xsl:if>
          <!--</xsl:for-each>-->
        </xsl:if>
  
        <TR>
          <TD colspan="4" class="default" valign="top">
            <xsl:if test="Company/ShowInsiderDocsLink = 'true'">
              <a>
                <xsl:attribute name="href">
                  insiders.aspx?IID=<xsl:value-of select="Company/KeyInstn"/>
                </xsl:attribute>
                View Insider related Regulatory Filings: Forms 3, 4 &amp; 5</a>.
            </xsl:if>
          </TD>
         </TR>
         <TR>
          <TD colspan="4" ALIGN="left" class="default"><br/>
            <a target="_blank">
              <xsl:attribute name="href">
                docdescriptions.aspx?IID=<xsl:value-of select="Company/KeyInstn"/>
              </xsl:attribute>
              View Document Grouping descriptions
            </a>
          </TD>
        </TR>
        <tr>
          <td colspan="5" nowrap="1">
            <table cellspacing="0" cellpadding="3" border="0" width="100">
              <tr>           
                <!--added 2 for condition as by default this will have All Companies also-->
                <xsl:if test="count(AvailableCompanies) > 2">
                  <td nowrap="1">
                    <BR/>
                    <!--<xsl:attribute name="action">
                      <xsl:choose>
                        <xsl:when  test="not(contains(Company/URL, 'print=1') or contains(Company/URL, 'Print=1'))">
                          <xsl:value-of select="concat('docs.aspx?IID=', Company/KeyInstn)"/>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:value-of select="concat('docs.aspx?IID=', Company/KeyInstn,'&amp;print=1')"/>
                        </xsl:otherwise>
                      </xsl:choose>
                    </xsl:attribute>-->
                    <!--<span class="defaultbold">Company Filters: </span>-->
                    <select name="CompanyFilter" id="CompanyFilter" onchange="document.getElementById('MaskForm').submit();">
                      <xsl:for-each select="AvailableCompanies">
                        <option value="{KeyInstn}">
                          <xsl:if test="Selected != ''">
                            <xsl:attribute name="selected"><xsl:value-of select="Selected"/></xsl:attribute>
                          </xsl:if>
                          <xsl:value-of select="CompanyName"/>
                        </option>
                      </xsl:for-each>
                    </select>
                  </td>
                </xsl:if>
                <td nowrap="1">
                  <BR/>
                  <!--<xsl:attribute name="action"><xsl:value-of select="concat('docs.aspx?IID=', Company/KeyInstn)"/></xsl:attribute>-->
                  <!--<span class="defaultbold">Display: </span>-->
                  <select name="TypeMask" id="TypeMask" onchange="document.getElementById('MaskForm').submit();">
                    <xsl:for-each select="AvailableTypes">
                      <option value="{TypeName}">
                        <xsl:if test="Selected != ''">
                          <xsl:attribute name="selected"><xsl:value-of select="Selected"/></xsl:attribute>
                        </xsl:if>
                        <xsl:value-of select="TypeName"/>
                      </option>
                    </xsl:for-each>
                  </select>
                </td>
                <td nowrap="1">
                  <BR/>
                  <!--<xsl:attribute name="action"><xsl:value-of select="concat('docs.aspx?&amp;IID=', Company/KeyInstn)"/></xsl:attribute>-->
                  <!--<span class="defaultbold">Year: </span>-->
                  <select name="YearMask" id="YearMask" onchange="document.getElementById('MaskForm').submit();">
                    <xsl:for-each select="AvailableYears">
                      <option>
                        <xsl:attribute name="value"><xsl:value-of select="Year"/></xsl:attribute>
                        <xsl:if test="Selected != ''">
                          <xsl:attribute name="selected"><xsl:value-of select="Selected"/></xsl:attribute>
                        </xsl:if>
                        <xsl:value-of select="Year"/>
                      </option>
                    </xsl:for-each>
                  </select>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </TABLE>
      </form>
  </xsl:template>


<xsl:template name="Docs_Highlights2">
    <TABLE cellpadding="4" cellspacing="0" width="100%">
      <TR>
        <TD colspan="4" class="colorlight">
          <span class="title2">Highlights</span>
        </TD>
      </TR>
      <tr>
        <TD WIDTH="40%" class="surrleft datashade">
          <span class="titletest2 ">Filing Description</span>
        </TD>
        <TD WIDTH="20%" class="surr_topbot datashade" NOWRAP="1">
          <span class="titletest2 ">Filing Type</span>
        </TD>
        <TD ALIGN="RIGHT" class="surr_topbot datashade" WIDTH="20%" NOWRAP="1">
          <span class="titletest2 ">Filing Date</span>
        </TD>
        <TD ALIGN="RIGHT" class="surr_topbot datashade" WIDTH="20%" NOWRAP="1">
          <span class="titletest2 ">Event Date</span>
        </TD>
        <TD ALIGN="RIGHT" class="surrright datashade" WIDTH="20%" NOWRAP="1"><span class="titletest2 ">Options</span></TD>
      </tr>
      <xsl:for-each select="Highlight">
        <TR VALIGN="TOP">
          <TD WIDTH="40%" class="data">
            <xsl:if test="KeyDoc != '0'">
              <a>
                <xsl:attribute name="href">
                  <xsl:value-of select="DocLink"/>
                </xsl:attribute>
                <xsl:value-of select="FilingDescription"/>
              </a>
            </xsl:if>
            <xsl:if test="KeyDoc = '0'">
              <xsl:value-of select="FilingDescription"/>
            </xsl:if>            
          </TD>
          <TD WIDTH="20%" class="data">
            <xsl:if test="FilingType != '(none)'">
              <A class="fielddef">
                <xsl:attribute name="href">
                  <xsl:value-of select="FilingTypeLink"/>
                </xsl:attribute>
                <xsl:value-of select="FilingType"/>
              </A>
            </xsl:if>
            <xsl:if test="FilingType = '(none)'">
              <xsl:value-of select="FilingType"/>
            </xsl:if>
          </TD>
          <TD ALIGN="RIGHT" WIDTH="20%" class="data">
            <xsl:value-of select="FilingDate"/>
          </TD>
          <TD ALIGN="RIGHT" WIDTH="20%" class="data">
            <xsl:value-of select="EventDate"/>
          </TD>
          <TD ALIGN="left" class="data" NOWRAP="1">
<!--=====================================================================-->
			<table border="0" cellpadding="0" cellspacing="2">
				<tr>
					<td>
						<xsl:choose>					
						<xsl:when test="KeyFileFormat = '2'">
							<a href="file.aspx?IID={../Company/KeyInstn}&amp;FID={KeyFile}&amp;O=2&amp;OSID=9" target="_new"><img class="irhtml" src="/images/interactive/ir/html.gif" alt="Download SEC Filing to HTML" title="Download SEC Filing to HTML" border="0"/></a>
						</xsl:when>
							<xsl:when test="KeyFileFormat = '1'">
								<a href="file.aspx?IID={../Company/KeyInstn}&amp;FID={KeyFile}&amp;O=2&amp;OSID=9" target="_new"><img class="irhtml" src="/images/interactive/ir/html.gif" alt="Download SEC Filing to HTML" title="Download SEC Filing to HTML" border="0"/></a>
							</xsl:when>
							<xsl:when test="KeyFileFormat = '7'">
                <a href="file.aspx?IID={../Company/KeyInstn}&amp;FID={KeyFile}&amp;O=2&amp;OSID=9" target="_new"><img class="irhtml" src="/images/interactive/ir/html.gif" alt="Download SEC Filing to HTML" title="Download SEC Filing to HTML" border="0"/></a>
						</xsl:when>
						<xsl:otherwise>
              <IMG class="irhtml_gray" SRC="/images/interactive/ir/html_gray.gif" alt="HTML SEC Filing Unavailable" title="HTML SEC Filing Unavailable"  border="0"/>
						</xsl:otherwise>			
						</xsl:choose>
					</td>
					
					<td>
						<xsl:choose>
							<xsl:when test="KeyFileFormat = '1'">
								<xsl:if test="normalize-space(PDFConvertible) = 'true'">
									<a href="file.aspx?IID={../Company/KeyInstn}&amp;FID={KeyFile}&amp;O=3&amp;OSID=9" target="_new"><img class="irpdf" src="/images/interactive/ir/pdf.gif" alt="Download SEC Filing to Adobe PDF" title="Download SEC Filing to Adobe PDF" border="0"/></a>
								</xsl:if>
								<xsl:if test="normalize-space(PDFConvertible) = 'false'">
									<IMG class="irpdf_gray" SRC="/images/interactive/ir/pdf_gray.gif" alt="Adobe PDF SEC Filing Unavailable" title="Adobe PDF SEC Filing Unavailable" border="0"/>
								</xsl:if>
							</xsl:when>
							<xsl:when test="KeyFileFormat = '2'">
								<xsl:if test="normalize-space(PDFConvertible) = 'true'">
									<a href="file.aspx?IID={../Company/KeyInstn}&amp;FID={KeyFile}&amp;O=3&amp;OSID=9" target="_new"><img class="irpdf" src="/images/interactive/ir/pdf.gif" alt="Download SEC Filing to Adobe PDF" title="Download SEC Filing to Adobe PDF" border="0"/></a>
								</xsl:if>
								<xsl:if test="normalize-space(PDFConvertible) = 'false'">
									<IMG class="irpdf_gray" SRC="/images/interactive/ir/pdf_gray.gif" alt="Adobe PDF SEC Filing Unavailable" title="Adobe PDF SEC Filing Unavailable" border="0"/>
								</xsl:if>
							</xsl:when>
							<xsl:when test="KeyFileFormat = '3'">
								<a href="file.aspx?IID={../Company/KeyInstn}&amp;FID={KeyFile}&amp;O=3&amp;OSID=9" target="_new"><img class="irpdf_gray" src="/images/interactive/ir/pdf.gif" alt="Download SEC Filing to Adobe PDF" title="Download SEC Filing to Adobe PDF" border="0"/></a>
							</xsl:when>
							<xsl:otherwise>
								<IMG class="irpdf_gray" SRC="/images/interactive/ir/pdf_gray.gif" alt="Adobe PDF SEC Filing Unavailable" title="Adobe PDF SEC Filing Unavailable" border="0"/>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:if test="ExcelFileFormat = '1'"> <!-- and (../Company/GAAP = 21 or ../Company/GAAP = 14 or ../Company/GAAP = 20 or FilingType = '10-Q' or FilingType = '10-K')"> -->
							<a style="margin-right: 0px;" href="{DocLink}"><img src="/images/interactive/ir/excel.gif" alt="Download SEC Filing to Excel" title="Download SEC Filing to Excel" border="0"/></a>
						</xsl:if>
						<xsl:if test="ExcelFileFormat = '0'"> <!-- and (../Company/GAAP = 21 or ../Company/GAAP = 14 or ../Company/GAAP = 20) "> -->
							<IMG SRC="/images/interactive/ir/excel_gray.gif" alt="Excel SEC Filing Unavailable" title="Excel SEC Filing Unavailable"  border="0"/>
						</xsl:if>
					</td>
          <td>
            <xsl:if test="XBRLFileFormat = 'True'">
              <a href="{DocLink}"><img class="irxbrl" src="/images/interactive/ir/xbrl.gif" border="0" alt="Download XBRL Content" title="Download SEC Filing to XBRL"/></a>
            </xsl:if>
            <xsl:if test="XBRLFileFormat = 'False'">
              <IMG class="irxbrl_gray" SRC="/images/interactive/ir/xbrl_gray.gif" border="0" alt="XBRL SEC Filing Unavailable" title="XBRL SEC Filing Unavailable"/>
            </xsl:if>
          </td>
				</tr>
			</table>
<!--=====================================================================-->
          </TD>
        </TR>
      </xsl:for-each>
    </TABLE>
</xsl:template>

<xsl:template name="Docs_Other_Filings2">
    <TABLE cellpadding="4" cellspacing="0" width="100%">

    <TR>
      <xsl:if test="Company/Mask = ''">
        <TD colspan="4" class="colorlight">
          <span class="title2">Other Filings</span>
        </TD>
      </xsl:if>
      <xsl:if test="Company/Mask != '' and Company/Mask != '345' and Company/Mask != '3' and Company/Mask != '4' and Company/Mask != '5'">
        <TD colspan="4" class="colorlight">
          <span class="title2">
            Type: <xsl:value-of select="Company/Mask"/> Filings
          </span>
        </TD>
        </xsl:if>
        <xsl:if test="Company/Mask = '345' or Company/Mask = '3' or Company/Mask = '4' or Company/Mask = '5'">
          <TD colspan="4" class="colorlight">
            <span class="title2">Insider Ownership Filings</span>
          </TD>
        </xsl:if>
    </TR>

    <tr>
      <TD WIDTH="40%" class="surrleft datashade">
        <span class="titletest2 ">Filing Description</span>
      </TD>
      <TD WIDTH="20%" class="surr_topbot datashade" NOWRAP="1">
        <span class="titletest2 ">Filing Type</span>
      </TD>
      <TD ALIGN="RIGHT" class="surr_topbot datashade" WIDTH="20%" NOWRAP="1">
        <span class="titletest2 ">Filing Date</span>
      </TD>
      <TD ALIGN="RIGHT" class="surr_topbot datashade" WIDTH="20%" NOWRAP="1">
        <span class="titletest2 ">Event Date</span>
      </TD>
      <TD ALIGN="RIGHT" class="surrright datashade" WIDTH="20%" NOWRAP="1">
        <span class="titletest2 ">Options</span>
      </TD>
    </tr>


    <xsl:for-each select="OtherFiling">

      <TR VALIGN="TOP">
        <TD WIDTH="40%" class="data">
          <xsl:if test="position() mod 2 = 0">
            <xsl:attribute name="class">datashade</xsl:attribute>
          </xsl:if>
          <a>
            <xsl:attribute name="href">
              <xsl:value-of select="DocLink"/>
            </xsl:attribute>
            <xsl:value-of select="FilingDescription"/>
          </a>
        </TD>
        <TD WIDTH="20%" class="data">
          <xsl:if test="position() mod 2 = 0">
            <xsl:attribute name="class">datashade</xsl:attribute>
          </xsl:if>
          <A class="fielddef">
            <xsl:attribute name="href">
              <xsl:value-of select="FilingTypeLink"/>
            </xsl:attribute>
            <xsl:value-of select="FilingType"/>
          </A>
        </TD>
        <TD ALIGN="RIGHT" WIDTH="20%" class="data">
          <xsl:if test="position() mod 2 = 0">
            <xsl:attribute name="class">datashade</xsl:attribute>
          </xsl:if>
          <xsl:value-of select="FilingDate"/>
        </TD>
        <TD ALIGN="RIGHT" WIDTH="20%" class="data">
          <xsl:if test="position() mod 2 = 0">
            <xsl:attribute name="class">datashade</xsl:attribute>
          </xsl:if>
          <xsl:value-of select="EventDate"/>
        </TD>
        <TD ALIGN="left" NOWRAP="1" class="data">
          <xsl:if test="position() mod 2 = 0">
            <xsl:attribute name="class">datashade</xsl:attribute>
          </xsl:if>
<!--=====================================================================-->
			<table border="0" cellpadding="0" cellspacing="2">
				<tr>
					<td>
						<xsl:choose>
							<xsl:when test="KeyFileFormat = '2'">
								<a href="file.aspx?IID={../Company/KeyInstn}&amp;FID={KeyFile}&amp;O=2&amp;OSID=9" target="_new"><img class="irhtml" src="/images/interactive/ir/html.gif" alt="Download SEC Filing to HTML" title="Download SEC Filing to HTML" border="0"/></a>
							</xsl:when>
							<xsl:when test="KeyFileFormat = '1'">
								<a href="file.aspx?IID={../Company/KeyInstn}&amp;FID={KeyFile}&amp;O=2&amp;OSID=9" target="_new"><img class="irhtml" src="/images/interactive/ir/html.gif" alt="Download SEC Filing to HTML" title="Download SEC Filing to HTML" border="0"/></a>
							</xsl:when>
							<xsl:when test="KeyFileFormat = '7'">
								<a href="file.aspx?IID={../Company/KeyInstn}&amp;FID={KeyFile}&amp;O=2&amp;OSID=9" target="_new"><img class="irhtml" src="/images/interactive/ir/html.gif" alt="Download SEC Filing to HTML" title="Download SEC Filing to HTML" border="0"/></a>
							</xsl:when>
							<xsl:otherwise>
								<IMG class="irhtml_gray" SRC="/images/interactive/ir/html_gray.gif" alt="HTML SEC Filing Unavailable" title="HTML SEC Filing Unavailable" border="0"/>
							</xsl:otherwise>			
						</xsl:choose>
					</td>
					<!--<td>
						<xsl:choose>
							<xsl:when test="KeyFileFormat = '10'">
								<a href="file.aspx?IID={../Company/KeyInstn}&amp;FID={KeyFile}&amp;O=10&amp;OSID=9" target="_new">
									<img class="irppt" src="/images/interactive/ir/ppt.gif" alt="Download SEC Filing to Word" title="Download SEC Filing to Word" border="0"/>
								</a>
							</xsl:when>
							<xsl:otherwise>
              <img class="irppt_gray" src="/images/interactive/ir/ppt_gray.gif" alt="Word version of SEC Filing is Unavailable" title="Download SEC Filing to Word" border="0"/>								
							</xsl:otherwise>
						</xsl:choose>
					</td>-->
					<td>
						<xsl:choose>
							<xsl:when test="KeyFileFormat = '1'">
								<xsl:if test="normalize-space(PDFConvertible) = 'true'">
									<a href="file.aspx?IID={../Company/KeyInstn}&amp;FID={KeyFile}&amp;O=3&amp;OSID=9" target="_new"><img class="irpdf" src="/images/interactive/ir/pdf.gif" alt="Download SEC Filing to Adobe PDF"  title="Download SEC Filing to Adobe PDF" border="0"/></a>
								</xsl:if>
								<xsl:if test="normalize-space(PDFConvertible) = 'false'">
									<img class="irpdf_gray" SRC="/images/interactive/ir/pdf_gray.gif" alt="Adobe PDF SEC Filing Unavailable" title="Adobe PDF SEC Filing Unavailable" border="0"/>
								</xsl:if>
							</xsl:when>
							<xsl:when test="KeyFileFormat = '2'">
								<xsl:if test="normalize-space(PDFConvertible) = 'true'">
									<a href="file.aspx?IID={../Company/KeyInstn}&amp;FID={KeyFile}&amp;O=3&amp;OSID=9" target="_new"><img src="/images/interactive/ir/pdf.gif" alt="Download SEC Filing to Adobe PDF" title="Download SEC Filing to Adobe PDF" border="0"/></a>
								</xsl:if>
								<xsl:if test="normalize-space(PDFConvertible) = 'false'">
									<img class="irpdf_gray" SRC="/images/interactive/ir/pdf_gray.gif" alt="Adobe PDF SEC Filing Unavailable" title="Adobe PDF SEC Filing Unavailable" border="0"/>
								</xsl:if>
							</xsl:when>
							<xsl:when test="KeyFileFormat = '3'">
								<a href="file.aspx?IID={../Company/KeyInstn}&amp;FID={KeyFile}&amp;O=3&amp;OSID=9" target="_new"><img class="irpdf" src="/images/interactive/ir/pdf.gif" alt="Download SEC Filing to Adobe PDF" title="Download SEC Filing to Adobe PDF" border="0"/></a>
							</xsl:when>
							<xsl:otherwise>
								<img class="irpdf_gray" SRC="/images/interactive/ir/pdf_gray.gif" alt="Adobe PDF SEC Filing Unavailable" title="Adobe PDF SEC Filing Unavailable" border="0"/>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:if test="ExcelFileFormat = '1'"> <!-- and (../Company/GAAP = 21 or ../Company/GAAP = 14 or ../Company/GAAP = 20 or FilingType = '10-Q' or FilingType = '10-K')"> -->
							<a href="{DocLink}"><img class="irexcel" src="/images/interactive/ir/excel.gif" alt="Download SEC Filing to Excel" title="Download SEC Filing to Excel" border="0"/></a>
						</xsl:if>
						<xsl:if test="ExcelFileFormat = '0'"> <!-- and (../Company/GAAP = 21 or ../Company/GAAP = 14 or ../Company/GAAP = 20) "> -->
							<img class="irexcel_gray" src="/images/interactive/ir/excel_gray.gif" alt="Excel SEC Filing Unavailable" title="Excel SEC Filing Unavailable"  border="0"/>              
						</xsl:if>
					</td>
          <td>
            <xsl:if test="XBRLFileFormat = 'True'">
              <a style="margin-right: 0px;" href="{DocLink}">
                <img class="irxbrl" src="/images/interactive/ir/xbrl.gif" border="0" alt="Download XBRL Content" title="Download SEC Filing to XBRL"/>
              </a>
            </xsl:if>
            <xsl:if test="XBRLFileFormat = 'False'">
              <img class="irxbrl_gray" src="/images/interactive/ir/xbrl_gray.gif" border="0" alt="XBRL SEC Filing Unavailable" title="XBRL SEC Filing Unavailable"/>
            </xsl:if>
          </td>
				</tr>
			</table>		
<!--=====================================================================-->
        </TD>
      </TR>
      <xsl:choose>
        <xsl:when test="(../Company/CompanyDisplayType='0')">
          <TR VALIGN="TOP">
            <TD class="data" colspan="5">
              <xsl:if test="position() mod 2 = 0">
                <xsl:attribute name="class">datashade</xsl:attribute>
              </xsl:if>
              <span class="italicfont">
                <xsl:value-of select="InstnName"/>
              </span>
            </TD>
          </TR>
        </xsl:when>
        <xsl:when test="((../Company/CompanyDisplayType='1') and (keyInstn != TreeID)) ">
          <TR>
            <TD class="data" colspan="5">
              <xsl:if test="position() mod 2 = 0">
                <xsl:attribute name="class">datashade</xsl:attribute>
              </xsl:if>
              <span class="italicfont">
                <xsl:value-of select="InstnName"/>
              </span
            </TD>
          </TR>
        </xsl:when>
      </xsl:choose>
      <xsl:if test="Abstract != '' ">
        <TR VALIGN="TOP" class="data">
          <TD colspan="4">
            <xsl:if test="position() mod 2 = 0">
              <xsl:attribute name="class">datashade</xsl:attribute>
            </xsl:if>             
              <xsl:if test="Abstract != ''"><span class="titletest2">Abstract:</span>&#160; <xsl:value-of select="Abstract"/></xsl:if>
          </TD>
            <TD>
            <xsl:if test="position() mod 2 = 0">
              <xsl:attribute name="class">datashade</xsl:attribute>
            </xsl:if>
          </TD>
        </TR>
      </xsl:if>
    </xsl:for-each>
	<!-- START :: Pagination Part -->
    <xsl:variable name="EndPage" select="Company/Start + Company/RowsPerPage - 1"/>
    <TR VALIGN="TOP" class="data">
      <TD colspan="2" class="defaultbold">
        &#160;
      </TD>
      <TD colspan="2" ALIGN="RIGHT" class="irwPagination">
      <xsl:variable name="selected_year" select="AvailableYears[Selected = 'selected']/Year" />
        <xsl:for-each select="Pagination">
          <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          <xsl:choose>
            <xsl:when test="CurrentPage = 'false'">
              <a target="_self">
                <xsl:attribute name="href">
                  <xsl:value-of select="PageLink"/>
                  <xsl:if test="$selected_year != '' and $selected_year != 'All Years'">&amp;YearMask&#61;<xsl:value-of select="$selected_year"/></xsl:if>  
                </xsl:attribute>
                <!--<span class="i">-->
                  <xsl:value-of select="PageLabel"/>
                <!--</span>-->
              </a>
            </xsl:when>
            <xsl:otherwise>
              <span class="titletest2">
                <xsl:value-of select="PageLabel"/>
              </span>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:for-each>
      </TD>
    </TR>
    </TABLE>
</xsl:template>
<!--==================== 	END :: Template ONE Ends here. ===================-->



  <!-- Template ONE Ends here. -->

<!--====================	Template : 3 	==================================-->
<!--	START :: This is Template 3 -->
  <xsl:template name="Docs_Links3">
    <form name="MaskForm" id="MaskForm"  method="Post">
    <xsl:attribute name="action">
    <xsl:choose>
        <xsl:when  test="not(contains(Company/URL, 'print=1') or contains(Company/URL, 'Print=1'))">
        <xsl:value-of select="concat('docs.aspx?IID=', Company/KeyInstn)"/>
        </xsl:when>
        <xsl:otherwise><xsl:value-of select="concat('docs.aspx?IID=', Company/KeyInstn,'&amp;print=1')"/></xsl:otherwise>
    </xsl:choose>
    </xsl:attribute>
      <TABLE border ="0" cellpadding="0" cellspacing="0"  width="100%">
        <xsl:if test="Company/ShowEmailNotificationLink = 'true'">
          <!--<xsl:for-each select="CLIENTEMAILPREFS[NotificationType = '2']">-->
          <xsl:if test="CLIENTEMAILPREFS/NotificationType = '2' or CLIENTEMAILPREFS/NotificationType = '30' or CLIENTEMAILPREFS/NotificationType = '31'">
            <TR>
              <TD colspan="6" class="default" valign="top">
                <a> <xsl:attribute name="href"> email.aspx?IID=<xsl:value-of select="Company/KeyInstn"/> </xsl:attribute> Notify me of new Regulatory Filings and Reports posted to this site</a>.
              </TD>
            </TR>
          </xsl:if>
          <!--</xsl:for-each>-->
        </xsl:if>
        <TR>
          <TD colspan="6" class="default" valign="top">
            <xsl:if test="Company/ShowInsiderDocsLink = 'true'">
              <a> <xsl:attribute name="href"> insiders.aspx?IID=<xsl:value-of select="Company/KeyInstn"/> </xsl:attribute> View Insider related Regulatory Filings: Forms 3, 4 &amp; 5</a>.
            </xsl:if>
          </TD>
        </TR>
        <TR>
          <TD colspan="6" ALIGN="left"  class="default"><br/>
            <a target="_blank">
              <xsl:attribute name="href">
                docdescriptions.aspx?IID=<xsl:value-of select="Company/KeyInstn"/>
              </xsl:attribute>
               View Document Grouping descriptions
            </a>
          </TD>
        </TR>

        <tr>
          <td colspan="5" nowrap="1">
            <table cellspacing="0" cellpadding="3" border="0" width="100">
              <tr>     
                <!--added 2 for condition as by default this will have All Companies also-->
                <xsl:if test="count(AvailableCompanies) > 2">
                  <td nowrap="1">
                    <BR/>
                    <!--<xsl:attribute name="action"><xsl:value-of select="concat('docs.aspx?IID=', Company/KeyInstn)"/></xsl:attribute>--> 
                    <!--<span class="defaultbold">Company Filters:</span>-->
                    <select name="CompanyFilter" id="CompanyFilter" onchange="document.getElementById('MaskForm').submit();">
                      <xsl:for-each select="AvailableCompanies">
                        <option value="{KeyInstn}">
                          <xsl:if test="Selected != ''">
                            <xsl:attribute name="selected"><xsl:value-of select="Selected"/></xsl:attribute>
                          </xsl:if>
                          <xsl:value-of select="CompanyName"/>
                        </option>
                      </xsl:for-each>
                    </select>
                  </td>
                </xsl:if>
                <td nowrap="1">
                  <BR/>
                  <!--<xsl:attribute name="action"><xsl:value-of select="concat('docs.aspx?IID=', Company/KeyInstn)"/></xsl:attribute>-->
                  <!--<span class="defaultbold">Display: </span>-->
                  <select name="TypeMask" id="TypeMask" onchange="document.getElementById('MaskForm').submit();">
                    <xsl:for-each select="AvailableTypes">
                      <option value="{TypeName}">
                        <xsl:if test="Selected != ''">
                          <xsl:attribute name="selected"><xsl:value-of select="Selected"/></xsl:attribute>
                        </xsl:if>
                        <xsl:value-of select="TypeName"/>
                      </option>
                    </xsl:for-each>
                  </select>
                </td>
                <td nowrap="1">
                  <BR/>
                  <!--<xsl:attribute name="action"><xsl:value-of select="concat('docs.aspx?&amp;IID=', Company/KeyInstn)"/></xsl:attribute>-->
                  <!--<span class="defaultbold">Year: </span>-->
                  <select name="YearMask" id="YearMask" onchange="document.getElementById('MaskForm').submit();">
                    <xsl:for-each select="AvailableYears">
                      <option value="{Year}">
                        <xsl:if test="Selected != ''">
                          <xsl:attribute name="selected"><xsl:value-of select="Selected"/></xsl:attribute>
                        </xsl:if>
                        <xsl:value-of select="Year"/>
                      </option>
                    </xsl:for-each>
                  </select>
                </td>
              </tr>
             </table>
            </td>
          </tr>
      </TABLE>
    </form>
</xsl:template>

<xsl:template name="Docs_Highlights3">
    <TABLE cellpadding="4" cellspacing="0" width="100%" class="table2">
      <TR>
        <TD colspan="5" class="header">Highlights</TD>
      </TR>
      <tr>
        <TD WIDTH="40%" valign="top" class="table1_item defaultbold">Filing Description</TD>
        <TD WIDTH="20%" valign="top" class="table1_item defaultbold" NOWRAP="1">Filing Type</TD>
        <TD ALIGN="RIGHT" valign="top" class="table1_item defaultbold" WIDTH="20%" NOWRAP="1">Filing Date</TD>
        <TD ALIGN="RIGHT" valign="top" class="table1_item defaultbold" WIDTH="20%" NOWRAP="1">Event Date</TD>
        <TD ALIGN="RIGHT" valign="top" class="table1_item defaultbold" WIDTH="20%" NOWRAP="1">Options</TD>
      </tr>
      <xsl:for-each select="Highlight">
        <TR VALIGN="TOP">
          <TD WIDTH="40%" class="data">
            <xsl:if test="KeyDoc != '0'">
              <a>
                <xsl:attribute name="href">
                  <xsl:value-of select="DocLink"/>
                </xsl:attribute>
                <xsl:value-of select="FilingDescription"/>
              </a>
            </xsl:if>
            <xsl:if test="KeyDoc = '0'">
              <xsl:value-of select="FilingDescription"/>
            </xsl:if>            
          </TD>
          <TD WIDTH="20%" class="data">
            <xsl:if test="FilingType != '(none)'">
              <A class="fielddef">
                <xsl:attribute name="href">
                  <xsl:value-of select="FilingTypeLink"/>
                </xsl:attribute>
                <xsl:value-of select="FilingType"/>
              </A>
            </xsl:if>
            <xsl:if test="FilingType = '(none)'">
              <xsl:value-of select="FilingType"/>
            </xsl:if>
          </TD>
          <TD ALIGN="RIGHT" WIDTH="20%" class="data">
            <xsl:value-of select="FilingDate"/>
          </TD>
          <TD ALIGN="RIGHT" WIDTH="20%" class="data">
            <xsl:value-of select="EventDate"/>
          </TD>
          <TD ALIGN="left" class="data" NOWRAP="1">
<!--=====================================================================-->
			<table border="0" cellpadding="0" cellspacing="2">
				<tr>
					<td>
						<xsl:choose>					
						<xsl:when test="KeyFileFormat = '2'">
							<a href="file.aspx?IID={../Company/KeyInstn}&amp;FID={KeyFile}&amp;O=2&amp;OSID=9" target="_new"><img class="irhtml" src="/images/interactive/ir/html.gif" alt="Download SEC Filing to HTML" title="Download SEC Filing to HTML" border="0"/></a>
						</xsl:when>
							<xsl:when test="KeyFileFormat = '1'">
								<a href="file.aspx?IID={../Company/KeyInstn}&amp;FID={KeyFile}&amp;O=2&amp;OSID=9" target="_new"><img class="irhtml" src="/images/interactive/ir/html.gif" alt="Download SEC Filing to HTML" title="Download SEC Filing to HTML" border="0"/></a>
							</xsl:when>
							<xsl:when test="KeyFileFormat = '7'">
                <a href="file.aspx?IID={../Company/KeyInstn}&amp;FID={KeyFile}&amp;O=2&amp;OSID=9" target="_new"><img class="irhtml" src="/images/interactive/ir/html.gif" alt="Download SEC Filing to HTML" title="Download SEC Filing to HTML" border="0"/></a>
						</xsl:when>
						<xsl:otherwise>
              <IMG class="irhtml_gray" SRC="/images/interactive/ir/html_gray.gif" alt="HTML SEC Filing Unavailable" title="HTML SEC Filing Unavailable"  border="0"/>
						</xsl:otherwise>			
						</xsl:choose>
					</td>
					
					<td>
						<xsl:choose>
							<xsl:when test="KeyFileFormat = '1'">
								<xsl:if test="normalize-space(PDFConvertible) = 'true'">
									<a href="file.aspx?IID={../Company/KeyInstn}&amp;FID={KeyFile}&amp;O=3&amp;OSID=9" target="_new"><img class="irpdf" src="/images/interactive/ir/pdf.gif" alt="Download SEC Filing to Adobe PDF" title="Download SEC Filing to Adobe PDF" border="0"/></a>
								</xsl:if>
								<xsl:if test="normalize-space(PDFConvertible) = 'false'">
									<IMG class="irpdf_gray" SRC="/images/interactive/ir/pdf_gray.gif" alt="Adobe PDF SEC Filing Unavailable" title="Adobe PDF SEC Filing Unavailable" border="0"/>
								</xsl:if>
							</xsl:when>
							<xsl:when test="KeyFileFormat = '2'">
								<xsl:if test="normalize-space(PDFConvertible) = 'true'">
									<a href="file.aspx?IID={../Company/KeyInstn}&amp;FID={KeyFile}&amp;O=3&amp;OSID=9" target="_new"><img class="irpdf" src="/images/interactive/ir/pdf.gif" alt="Download SEC Filing to Adobe PDF" title="Download SEC Filing to Adobe PDF" border="0"/></a>
								</xsl:if>
								<xsl:if test="normalize-space(PDFConvertible) = 'false'">
									<IMG class="irpdf_gray" SRC="/images/interactive/ir/pdf_gray.gif" alt="Adobe PDF SEC Filing Unavailable" title="Adobe PDF SEC Filing Unavailable" border="0"/>
								</xsl:if>
							</xsl:when>
							<xsl:when test="KeyFileFormat = '3'">
								<a href="file.aspx?IID={../Company/KeyInstn}&amp;FID={KeyFile}&amp;O=3&amp;OSID=9" target="_new"><img class="irpdf_gray" src="/images/interactive/ir/pdf.gif" alt="Download SEC Filing to Adobe PDF" title="Download SEC Filing to Adobe PDF" border="0"/></a>
							</xsl:when>
							<xsl:otherwise>
								<IMG class="irpdf_gray" SRC="/images/interactive/ir/pdf_gray.gif" alt="Adobe PDF SEC Filing Unavailable" title="Adobe PDF SEC Filing Unavailable" border="0"/>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:if test="ExcelFileFormat = '1'"> <!-- and (../Company/GAAP = 21 or ../Company/GAAP = 14 or ../Company/GAAP = 20 or FilingType = '10-Q' or FilingType = '10-K')"> -->
							<a style="margin-right: 0px;" href="{DocLink}"><img src="/images/interactive/ir/excel.gif" alt="Download SEC Filing to Excel" title="Download SEC Filing to Excel" border="0"/></a>
						</xsl:if>
						<xsl:if test="ExcelFileFormat = '0'"> <!-- and (../Company/GAAP = 21 or ../Company/GAAP = 14 or ../Company/GAAP = 20) "> -->
							<IMG SRC="/images/interactive/ir/excel_gray.gif" alt="Excel SEC Filing Unavailable" title="Excel SEC Filing Unavailable"  border="0"/>
						</xsl:if>
					</td>
          <td>
            <xsl:if test="XBRLFileFormat = 'True'">
              <a href="{DocLink}"><img class="irxbrl" src="/images/interactive/ir/xbrl.gif" border="0" alt="Download XBRL Content" title="Download SEC Filing to XBRL"/></a>
            </xsl:if>
            <xsl:if test="XBRLFileFormat = 'False'">
              <IMG class="irxbrl_gray" SRC="/images/interactive/ir/xbrl_gray.gif" border="0" alt="XBRL SEC Filing Unavailable" title="XBRL SEC Filing Unavailable"/>
            </xsl:if>
          </td>
				</tr>
			</table>
<!--=====================================================================-->
          </TD>
        </TR>

      </xsl:for-each>
    </TABLE>
    <br />
  </xsl:template>

  <xsl:template name="Docs_Other_Filings3">
    <table BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" class="table2">
      <TR>
        <xsl:if test="Company/Mask = ''">
          <TD colspan="5" class="header">Other Filings</TD>
        </xsl:if>
        <xsl:if test="Company/Mask != '' and Company/Mask != '345' and Company/Mask != '3' and Company/Mask != '4' and Company/Mask != '5'">
          <TD colspan="5" class="header"> Type: <xsl:value-of select="Company/Mask"/> Filings</TD>       
        </xsl:if>
        <xsl:if test="Company/Mask = '345' or Company/Mask = '3' or Company/Mask = '4' or Company/Mask = '5'">
          <TD colspan="4" class="colorlight"><span class="title2">Insider Ownership Filings</span></TD>
        </xsl:if>
      </TR>

      <tr>
        <TD WIDTH="40%" class="table1_item defaultbold">Filing Description</TD>
        <TD WIDTH="20%" class="table1_item defaultbold" NOWRAP="1">Filing Type</TD>
        <TD ALIGN="RIGHT" class="table1_item defaultbold" WIDTH="20%" NOWRAP="1">Filing Date</TD>
        <TD ALIGN="RIGHT" class="table1_item defaultbold" WIDTH="20%" NOWRAP="1">Event Date</TD>
        <TD ALIGN="RIGHT" class="table1_item defaultbold" WIDTH="20%" NOWRAP="1">Options</TD>
      </tr>

      <xsl:for-each select="OtherFiling">

        <TR VALIGN="TOP">
          <TD WIDTH="40%" class="data">
            <xsl:if test="position() mod 2 = 0">
              <xsl:attribute name="class">datashade</xsl:attribute>
            </xsl:if>
            <a>
              <xsl:attribute name="href">
                <xsl:value-of select="DocLink"/>
              </xsl:attribute>
              <xsl:value-of select="FilingDescription"/>
            </a>
          </TD>
          <TD WIDTH="20%" class="data">
            <xsl:if test="position() mod 2 = 0">
              <xsl:attribute name="class">datashade</xsl:attribute>
            </xsl:if>
            <A class="fielddef">
              <xsl:attribute name="href">
                <xsl:value-of select="FilingTypeLink"/>
              </xsl:attribute>
              <xsl:value-of select="FilingType"/>
            </A>
          </TD>
          <TD ALIGN="RIGHT" WIDTH="20%" class="data">
            <xsl:if test="position() mod 2 = 0">
              <xsl:attribute name="class">datashade</xsl:attribute>
            </xsl:if>
            <xsl:value-of select="FilingDate"/>
          </TD>
          <TD ALIGN="RIGHT" WIDTH="20%" class="data">
            <xsl:if test="position() mod 2 = 0">
              <xsl:attribute name="class">datashade</xsl:attribute>
            </xsl:if>
            <xsl:value-of select="EventDate"/>
          </TD>
          <TD ALIGN="left" NOWRAP="1" class="data" valign="bottom">
            <xsl:if test="position() mod 2 = 0">
              <xsl:attribute name="class">datashade</xsl:attribute>
            </xsl:if>
<!--=====================================================================-->
			<table border="0" cellpadding="0" cellspacing="2">
				<tr>
					<td>
						<xsl:choose>
							<xsl:when test="KeyFileFormat = '2'">
								<a href="file.aspx?IID={../Company/KeyInstn}&amp;FID={KeyFile}&amp;O=2&amp;OSID=9" target="_new"><img class="irhtml" src="/images/interactive/ir/html.gif" alt="Download SEC Filing to HTML" title="Download SEC Filing to HTML" border="0"/></a>
							</xsl:when>
							<xsl:when test="KeyFileFormat = '1'">
								<a href="file.aspx?IID={../Company/KeyInstn}&amp;FID={KeyFile}&amp;O=2&amp;OSID=9" target="_new"><img class="irhtml" src="/images/interactive/ir/html.gif" alt="Download SEC Filing to HTML" title="Download SEC Filing to HTML" border="0"/></a>
							</xsl:when>
							<xsl:when test="KeyFileFormat = '7'">
								<a href="file.aspx?IID={../Company/KeyInstn}&amp;FID={KeyFile}&amp;O=2&amp;OSID=9" target="_new"><img class="irhtml" src="/images/interactive/ir/html.gif" alt="Download SEC Filing to HTML" title="Download SEC Filing to HTML" border="0"/></a>
							</xsl:when>
							<xsl:otherwise>
								<IMG class="irhtml_gray" SRC="/images/interactive/ir/html_gray.gif" alt="HTML SEC Filing Unavailable" title="HTML SEC Filing Unavailable" border="0"/>
							</xsl:otherwise>			
						</xsl:choose>
					</td>
					<!--<td>
						<xsl:choose>
							<xsl:when test="KeyFileFormat = '10'">
								<a href="file.aspx?IID={../Company/KeyInstn}&amp;FID={KeyFile}&amp;O=10&amp;OSID=9" target="_new">
									<img class="irppt" src="/images/interactive/ir/ppt.gif" alt="Download SEC Filing to Word" title="Download SEC Filing to Word" border="0"/>
								</a>
							</xsl:when>
							<xsl:otherwise>
              <img class="irppt_gray" src="/images/interactive/ir/ppt_gray.gif" alt="Word version of SEC Filing is Unavailable" title="Download SEC Filing to Word" border="0"/>								
							</xsl:otherwise>
						</xsl:choose>
					</td>-->
					<td>
						<xsl:choose>
							<xsl:when test="KeyFileFormat = '1'">
								<xsl:if test="normalize-space(PDFConvertible) = 'true'">
									<a href="file.aspx?IID={../Company/KeyInstn}&amp;FID={KeyFile}&amp;O=3&amp;OSID=9" target="_new"><img class="irpdf" src="/images/interactive/ir/pdf.gif" alt="Download SEC Filing to Adobe PDF"  title="Download SEC Filing to Adobe PDF" border="0"/></a>
								</xsl:if>
								<xsl:if test="normalize-space(PDFConvertible) = 'false'">
									<img class="irpdf_gray" SRC="/images/interactive/ir/pdf_gray.gif" alt="Adobe PDF SEC Filing Unavailable" title="Adobe PDF SEC Filing Unavailable" border="0"/>
								</xsl:if>
							</xsl:when>
							<xsl:when test="KeyFileFormat = '2'">
								<xsl:if test="normalize-space(PDFConvertible) = 'true'">
									<a href="file.aspx?IID={../Company/KeyInstn}&amp;FID={KeyFile}&amp;O=3&amp;OSID=9" target="_new"><img src="/images/interactive/ir/pdf.gif" alt="Download SEC Filing to Adobe PDF" title="Download SEC Filing to Adobe PDF" border="0"/></a>
								</xsl:if>
								<xsl:if test="normalize-space(PDFConvertible) = 'false'">
									<img class="irpdf_gray" SRC="/images/interactive/ir/pdf_gray.gif" alt="Adobe PDF SEC Filing Unavailable" title="Adobe PDF SEC Filing Unavailable" border="0"/>
								</xsl:if>
							</xsl:when>
							<xsl:when test="KeyFileFormat = '3'">
								<a href="file.aspx?IID={../Company/KeyInstn}&amp;FID={KeyFile}&amp;O=3&amp;OSID=9" target="_new"><img class="irpdf" src="/images/interactive/ir/pdf.gif" alt="Download SEC Filing to Adobe PDF" title="Download SEC Filing to Adobe PDF" border="0"/></a>
							</xsl:when>
							<xsl:otherwise>
								<img class="irpdf_gray" SRC="/images/interactive/ir/pdf_gray.gif" alt="Adobe PDF SEC Filing Unavailable" title="Adobe PDF SEC Filing Unavailable" border="0"/>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:if test="ExcelFileFormat = '1'"> <!-- and (../Company/GAAP = 21 or ../Company/GAAP = 14 or ../Company/GAAP = 20 or FilingType = '10-Q' or FilingType = '10-K')"> -->
							<a href="{DocLink}"><img class="irexcel" src="/images/interactive/ir/excel.gif" alt="Download SEC Filing to Excel" title="Download SEC Filing to Excel" border="0"/></a>
						</xsl:if>
						<xsl:if test="ExcelFileFormat = '0'"> <!-- and (../Company/GAAP = 21 or ../Company/GAAP = 14 or ../Company/GAAP = 20) "> -->
							<img class="irexcel_gray" src="/images/interactive/ir/excel_gray.gif" alt="Excel SEC Filing Unavailable" title="Excel SEC Filing Unavailable"  border="0"/>              
						</xsl:if>
					</td>
          <td>
            <xsl:if test="XBRLFileFormat = 'True'">
              <a style="margin-right: 0px;" href="{DocLink}">
                <img class="irxbrl" src="/images/interactive/ir/xbrl.gif" border="0" alt="Download XBRL Content" title="Download SEC Filing to XBRL"/>
              </a>
            </xsl:if>
            <xsl:if test="XBRLFileFormat = 'False'">
              <img class="irxbrl_gray" src="/images/interactive/ir/xbrl_gray.gif" border="0" alt="XBRL SEC Filing Unavailable" title="XBRL SEC Filing Unavailable"/>
            </xsl:if>
          </td>
				</tr>
			</table>		
<!--=====================================================================-->
          </TD>
        </TR>
        <xsl:choose>
          <xsl:when test="(../Company/CompanyDisplayType='0')">
            <TR VALIGN="TOP">
              <TD class="data" colspan="5">
                <xsl:if test="position() mod 2 = 0">
                  <xsl:attribute name="class">datashade</xsl:attribute>
                </xsl:if>
                <span class="italicfont">
                  <xsl:value-of select="InstnName"/>
                </span
              </TD>
            </TR>
          </xsl:when>
          <xsl:when test="((../Company/CompanyDisplayType='1') and (keyInstn != TreeID))">
            <TR>
              <TD class="data" colspan="5">
                <xsl:if test="position() mod 2 = 0">
                  <xsl:attribute name="class">datashade</xsl:attribute>
                </xsl:if>
                <span class="italicfont">
                  <xsl:value-of select="InstnName"/>
                </span>
              </TD>
            </TR>
          </xsl:when>
        </xsl:choose>
        <xsl:if test="Abstract != ''">
          <TR VALIGN="TOP" class="data">
            <TD colspan="4">
              <xsl:if test="position() mod 2 = 0">
                <xsl:attribute name="class">datashade</xsl:attribute>
              </xsl:if>              
              <xsl:if test="Abstract != ''"><span class="defaultbold titletest2">Abstract: </span> <xsl:value-of select="Abstract"/></xsl:if>
            </TD>
            <TD>
              <xsl:if test="position() mod 2 = 0">
                <xsl:attribute name="class">datashade</xsl:attribute>
              </xsl:if>
            </TD>
          </TR>
        </xsl:if>
      </xsl:for-each>
      	<xsl:variable name="EndPage" select="Company/Start + Company/RowsPerPage - 1"/>
   	<xsl:variable name="Selected-Year" select="Year"/>
      	<TR VALIGN="TOP" class="data">
        	<TD colspan="2" class="defaultbold">&#160;</TD>
		<TD colspan="2" ALIGN="RIGHT" class="irwPagination">
	  
	
	
	
		<!--<xsl:for-each select="AvailableYears">
		<xsl:choose>	
			<xsl:when test="(Selected ='selected') and (Year !='All Years')">
			<br /><xsl:value-of select="Year"/>
			</xsl:when>
		</xsl:choose>	
		</xsl:for-each>-->
    <xsl:variable name="selected_year" select="AvailableYears[Selected = 'selected']/Year" />

		<xsl:for-each select="Pagination">
			<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
			<xsl:choose>
				<xsl:when test="CurrentPage = 'false'">
					<a target="_self">
					<xsl:attribute name="href">
					<xsl:value-of select="PageLink"/>
            <xsl:if test="$selected_year != '' and $selected_year != 'All Years'">&amp;YearMask&#61;<xsl:value-of select="$selected_year"/></xsl:if>  
					</xsl:attribute>
					<!--<span class="i">-->
					<xsl:value-of select="PageLabel"/>
					<!--</span>-->
					</a>
				</xsl:when>
				<xsl:otherwise>
					<span class="titletest2">
				<xsl:value-of select="PageLabel"/>
				</span>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
		</TD>
      </TR>
    </table>
    <br />
  </xsl:template>

  <!-- Template 3 Ends here. -->
  <!-- Main XSLT templates are here. these are the templates which are actually being displayed in the page. this is for TOP LEVEL editing. if you dont need to use the individual templates above you can do your main editing here when pulling THESE templates below you must carry the top comment with them and uncomment them when you drop it into the company specific xslt. -->

<xsl:template match="irw:Docs">
<style type="text/css">
/*----------------------------------*/
/*    CSS for Pagination     */
/*----------------------------------*/
.irwPagination {
	padding: 10px;
	margin: 3px;
}
.irwPagination a, .irwPagination a:link, .irwPagination a:visited, .irwPagination a:hover {
	border: 1px solid #CCCCCC;
	padding: 3px 6px 3px 6px;
}
.irwPagination a:hover {
	background: #DDDDDD;	
}
.irwPagination span {
	border: 1px solid #CCCCCC;
	padding: 3px 6px 3px 6px;
	background: #DDDDDD;
	font-weight: bold;
}
</style>  
    <xsl:variable name="TemplateName" select="Company/TemplateName"/>
    <xsl:choose>
      <!-- Template 2 -->
      <xsl:when test="$TemplateName = 'AltDocs1'">
        <xsl:call-template name="TemplateONEstylesheet" />
        <SCRIPT language="javascript" type="text/javascript">
          function openWindow (url,name,widgets) {
          popupWin = window.open (url,name,widgets);
          popupWin.opener.top.name="opener";
          popupWin.focus();
          }
        </SCRIPT>
        <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" class="">
          <xsl:call-template name="Title_S2"/>
          <TR>
            <TD align="left">
              <table cellpadding="4" cellspacing="0" border="0" width="100%">
                <tr>
                  <td class="surr datashade" valign="top" nowrap="1">
                    <xsl:call-template name="Docs_Links2"/>
                  </td>
                </tr>
              </table>
            </TD>
          </TR>
          <TR>
            <TD valign="top" class="leftTOPbord">

			  <xsl:variable name="Highlight" select="count(Highlight)"/>
				<xsl:if test="$Highlight > 0">
					<xsl:call-template name="Docs_Highlights2"/>
				</xsl:if>
			  <xsl:variable name="OtherFiling" select="count(OtherFiling)"/>
				<xsl:if test="$OtherFiling > 0">
					<xsl:call-template name="Docs_Other_Filings2"/>
				</xsl:if>
              <xsl:if test="$OtherFiling = 0">
              <table cellpadding="4" cellspacing="0" border="0" width="100%">
                <tr>
                  <td NOWRAP="1">
                    <span class="title2">Other Filings</span>
                    </td>
                  </tr>
                  <tr>
                  	<td class="data">There are no documents found within search criteria</td>
                  </tr>
                 </table>
              </xsl:if>
            </TD>
          </TR>
        </TABLE>
        <xsl:call-template name="Docs_Footer"/>
        <xsl:call-template name="Copyright"/>
      </xsl:when>
      <!-- End Template 2 -->

      <!-- Template 3 -->
      <xsl:when test="$TemplateName = 'AltDocs3'">
        <xsl:call-template name="TemplateTHREEstylesheet" />
        <SCRIPT language="javascript" type="text/javascript">
          function openWindow (url,name,widgets) {
          popupWin = window.open (url,name,widgets);
          popupWin.opener.top.name="opener";
          popupWin.focus();
          }
        </SCRIPT>
        <xsl:call-template name="Title_T3"/>
        <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="4" WIDTH="100%" class="table2">
          <TR>
            <TD align="left">
              <table cellpadding="3" cellspacing="0" border="0" width="100%">
                <tr>
                  <td valign="top" nowrap="1">
                    <xsl:call-template name="Docs_Links3"/>
                  </td>
                </tr>
              </table>
            </TD>
          </TR>
          <TR>
            <TD valign="top">

              <xsl:variable name="Highlight" select="count(Highlight)"/>
              <xsl:if test="$Highlight > 0">
                <xsl:call-template name="Docs_Highlights3"/>
              </xsl:if>
              <xsl:variable name="OtherFiling" select="count(OtherFiling)"/>
              <xsl:if test="$OtherFiling > 0">
                <xsl:call-template name="Docs_Other_Filings3"/>
              </xsl:if>
              <xsl:if test="$OtherFiling = 0">
              <table cellpadding="4" cellspacing="0" border="0" width="100%" class="table2">
                <tr>
                  <td NOWRAP="1" class="header">
                    Other Filings
                  </td>
                </tr>
                <tr>
                	<td class="data">There are no documents found within search criteria</td>
                </tr>
               </table>
              </xsl:if>

            </TD>
          </TR>
        </TABLE>
        <xsl:call-template name="Docs_Footer"/>
        <xsl:call-template name="Copyright"/>
      </xsl:when>
      <!-- End Template 3 -->
      
      <!-- Template 4 -->
      <xsl:when test="$TemplateName = 'AltDocs4'">        
        <xsl:call-template name="Docs_4" />
      </xsl:when>
      <!-- End Template 4 -->
      
      <!-- Template Default -->
      <xsl:otherwise>

        <SCRIPT language="javascript" type="text/javascript">
          function openWindow (url,name,widgets) {
          popupWin = window.open (url,name,widgets);
          popupWin.opener.top.name="opener";
          popupWin.focus();
          }
        </SCRIPT>

        <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" class="colordark">
          <xsl:call-template name="Title_S1"/>
          <TR>
            <TD class="colordark" colspan="2">
              <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="4" WIDTH="100%">
                <tr>
                  <td class="colorlight">
                    <form name="MaskForm" id="MaskForm"  method="Post">                    
                      <xsl:attribute name="action">
                        <xsl:choose>
                          <xsl:when  test="not(contains(Company/URL, 'print=1') or contains(Company/URL, 'Print=1'))">
                            <xsl:value-of select="concat('docs.aspx?IID=', Company/KeyInstn)"/>
                          </xsl:when>
                          <xsl:otherwise><xsl:value-of select="concat('docs.aspx?IID=', Company/KeyInstn,'&amp;print=1')"/></xsl:otherwise>
                        </xsl:choose>
                      </xsl:attribute>
                      <table border="0" cellspacing="0" cellpadding="4" width="100%">
                        <xsl:call-template name="Docs_Links"/>
                        <xsl:variable name="Highlight" select="count(Highlight)"/>
                        <xsl:if test="$Highlight > 0">
                          <xsl:call-template name="Docs_Highlights"/>
                          <TR>
                            <TD>
                              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                            </TD>
                          </TR>
                        </xsl:if>
                        <xsl:variable name="OtherFiling" select="count(OtherFiling)"/>
                        <xsl:if test="$OtherFiling > 0">
                          <xsl:call-template name="Docs_Other_Filings"/>
                        </xsl:if>
                        <xsl:if test="$OtherFiling = 0">
                          <tr>
                            <td NOWRAP="1" colspan="5" class="default">
                            <span class="title2">Other Filings</span><br/>There are no documents found within search criteria
                          </td>
                        </tr>
                      </xsl:if>
                    </table>
                    </form>
                  </td>
                </tr>
              </TABLE>
            </TD>
          </TR>
        </TABLE>
        <xsl:call-template name="Docs_Footer"/>
        <xsl:call-template name="Copyright"/>
      </xsl:otherwise>
    </xsl:choose>
</xsl:template>



<!-- Docs Template Style 4 -->
  <xsl:template name="Docs_4">
    <xsl:call-template name="DocsScriptResource_4"/>
	<div id="outer">
		<div id="Docs">
			<xsl:call-template name="Toolkit_Section_4"/>
			<div id="DocsSection">
				<xsl:call-template name="Title_Head_4"/>
        <xsl:call-template name="Header_4"/>
				<xsl:call-template name="DocsLinks_4"/>
				<xsl:call-template name="DocsHighlights_4"/>
				<xsl:call-template name="DocsOtherFilings_4"/>	
			  <xsl:call-template name="Footer_4"/>	
        <xsl:call-template name="Copyright_4"/>
			</div>			
		</div>		
	</div>
  </xsl:template>  
  
  <xsl:template name="DocsScriptResource_4">
    <xsl:variable name="KeyInstn" select="Company/KeyInstn"/>	
    <xsl:call-template name="CommonScript_4" /> 
      <xsl:if test="not(contains(translate(Company/URL,$ucletters,$lcletters), 'print=1'))">
        <xsl:call-template name="DocsTablesorter_4" />
        <script language="javascript" type="text/javascript" src="{$ScriptPath}docs_temp1.js"></script>	
        <xsl:if test="$devPath != ''">
          <script language="javascript" type="text/javascript" src="/Interactive/LookAndFeel/{$KeyInstn}/{$devPath}docs_temp1.js"></script>	
        </xsl:if>
	    <SCRIPT language="javascript" type="text/javascript">
        <![CDATA[
          var KeyInstn ="]]><xsl:value-of select="$KeyInstn"/><![CDATA[";
          var devPath ="]]><xsl:value-of select="$devPath"/><![CDATA[";
          var num = "";
          st1=new String(window.location);
          i1=st1.indexOf('&style=');		
          for(j=i1 + 7;j<=i1 + 7;j++) { i3 = st1.charAt(j); num = num + i3; }		
          if(num == '/' || num == ''){ num = ''; }
          if(num == '1'){
            document.write('<link href="/Interactive/LookAndFeel/'+KeyInstn+'/'+devPath+'docs_c.css" rel="stylesheet" type="text/css" />');
            document.write('<script language="javascript" type="text/javascript" src="/Interactive/LookAndFeel/'+KeyInstn+'/'+devPath+'docs_c.js"><\/script>'); 
          } else if(num == '0'){
            document.write('<link href="/Interactive/LookAndFeel/'+KeyInstn+'/'+devPath+'docs_ir.css" rel="stylesheet" type="text/css" />');
            document.write('<script language="javascript" type="text/javascript" src="/Interactive/LookAndFeel/'+KeyInstn+'/'+devPath+'docs_ir.js"><\/script>');
          }
        ]]>
      </SCRIPT>
    </xsl:if>
    <!--<xsl:call-template name="OverLibmws_4" />-->
    <SCRIPT language="javascript" type="text/javascript">
      function openWindow (url,name,widgets) {
      popupWin = window.open (url,name,widgets);
      popupWin.opener.top.name="opener";
      popupWin.focus();
      }
    </SCRIPT>
    </xsl:template>
  
    <xsl:template name="DocsTablesorter_4">
      <script language="javascript" type="text/javascript" src="{$ScriptPath}datatables/jquery.tablesorter.js"></script>
      <script language="javascript" type="text/javascript" src="{$ScriptPath}/datatables/jquery.dataTables.min.js"></script>
      <script language="javascript" type="text/javascript">
        <![CDATA[
        $(document).ready( function() {
          $('#OtherFilings').dataTable({
            "bJQueryUI": false,
		        "bSort": false,
		        "bLengthChange": false,
		        "bStateSave": false,				
		        "bProcessing": false,
		        "bFilte": false,
		        "bInfo": false,
		        "bScrollCollapse": true,
		        "bPaginate": true
	        });
        });
        ]]>
      </script>
      <script language="javascript" type="text/javascript" src="javascript/jquery.alternate.min.js"></script>
    </xsl:template>
    
  <xsl:template name="DocsLinks_4">
	<div id="DocsLinks">		
		<xsl:if test="Company/ShowInsiderDocsLink = 'true'">
		  <p><a href="insiders.aspx?IID={Company/KeyInstn}">View Insider related Regulatory Filings: Forms 3, 4 &amp; 5</a>.</p>
		</xsl:if>			
		<p><a target="_blank" href="docdescriptions.aspx?IID={Company/KeyInstn}">View Document Grouping descriptions</a></p>
	</div>
  </xsl:template>
  
  <xsl:template name="DocsHighlights_4">  
	<div id="DocsSelectBox">
		<div class="DocsSelectBox">
			<form name="MaskForm" id="MaskForm"  method="Post">
              <xsl:attribute name="action">
                <xsl:choose>
                  <xsl:when  test="not(contains(Company/URL, 'print=1') or contains(Company/URL, 'Print=1'))">
                    <xsl:value-of select="concat('docs.aspx?IID=', Company/KeyInstn)"/>
                  </xsl:when>
                  <xsl:otherwise><xsl:value-of select="concat('docs.aspx?IID=', Company/KeyInstn,'&amp;print=1')"/></xsl:otherwise>
                </xsl:choose>
              </xsl:attribute>
			<div class="DisplayNews">				
				<!--<span class="defaultbold">Display: </span>-->
				<select class="activeselectbox" name="TypeMask" id="TypeMask" onchange="document.getElementById('MaskForm').submit();">
				  <xsl:for-each select="AvailableTypes">
					<option value="{TypeName}">
					  <xsl:if test="Selected != ''"><xsl:attribute name="selected"><xsl:value-of select="Selected"/></xsl:attribute></xsl:if>
						<xsl:value-of select="TypeName"/>
					</option>
				  </xsl:for-each>
				</select>
				<img src="javascript/Template4/images/common/spacer.gif" width="10" height="24" class="DocsInfo" alt="Docinfo" />
			</div>
        <!--added 2 for condition as by default this will have All Companies also-->
          <xsl:if test="count(AvailableCompanies) > 2">
            <div class="Companies">
              <!--<xsl:attribute name="action">
                <xsl:choose>
                  <xsl:when  test="not(contains(Company/URL, 'print=1') or contains(Company/URL, 'Print=1'))"><xsl:value-of select="concat('docs.aspx?IID=', Company/KeyInstn)"/></xsl:when>
                  <xsl:otherwise><xsl:value-of select="concat('docs.aspx?IID=', Company/KeyInstn,'&amp;print=1')"/></xsl:otherwise>
                </xsl:choose>
              </xsl:attribute>-->
              <!--<span class="defaultbold">Company Filters: </span>-->
              <select class="activeselectbox" name="CompanyFilter" id="CompanyFilter" onchange="document.getElementById('MaskForm').submit();">
                <xsl:for-each select="AvailableCompanies">
                  <option value="{KeyInstn}">
                    <xsl:if test="Selected != ''">
                      <xsl:attribute name="selected"><xsl:value-of select="Selected"/></xsl:attribute>
                    </xsl:if>
                    <xsl:value-of select="CompanyName"/>
                  </option>
                </xsl:for-each>
              </select>
              </div>
          </xsl:if>
        
			<div class="YearNews">
			  <!--<xsl:attribute name="action"><xsl:value-of select="concat('docs.aspx?template=1&amp;IID=', Company/KeyInstn)"/></xsl:attribute>-->
			  <!--<span class="defaultbold">Year: </span>-->
			  <select class="activeselectbox" name="YearMask" id="YearMask" onchange="document.getElementById('MaskForm').submit();">
				<xsl:for-each select="AvailableYears">
				  <option value="{Year}">
				  <xsl:if test="Selected != ''"><xsl:attribute name="selected"><xsl:value-of select="Selected"/></xsl:attribute></xsl:if>
					<xsl:value-of select="Year"/>
				  </option>
				</xsl:for-each>
			  </select>
			</div>
			</form>
		</div>
		<xsl:variable name="Highlight" select="count(Highlight)"/>
		<xsl:if test="$Highlight > 0"><xsl:call-template name="DocsHighlightsDetail_4"/></xsl:if>
	</div>
  </xsl:template>
  
  <xsl:template name="DocsHighlightsDetail_4">
    <TABLE id="DocsHighlight" class="display" cellpadding="0" cellspacing="0" width="100%">
	  <thead>
      <tr>
        <td width="30%" valign="top" class="headline">Filing Description</td>
        <td width="30%" valign="top" class="headline" nowrap="1">Company</td>
        <!--<td width="10%" valign="top" class="headline" nowrap="1">Filing Type</td>-->
        <td width="10%" align="right" valign="top" class="headline" nowrap="1">Filing Date</td>
        <td width="10%" align="right" valign="top" class="headline" nowrap="1">Event Date</td>
        <td width="20%" align="right" valign="top" class="headline" nowrap="1">Options</td>
      </tr>
	  </thead>
	  <tbody>
      <xsl:for-each select="Highlight">
        <TR VALIGN="TOP">
          <TD>
          <xsl:if test="contains(translate(../Company/URL,$ucletters,$lcletters), 'print=1')">
            <xsl:if test="position() mod 2 = 0">
              <xsl:attribute name="class">Docs_altdatashade</xsl:attribute>
            </xsl:if>
          </xsl:if>
            <xsl:if test="KeyDoc != '0'">
              <a href="{DocLink}"><xsl:value-of select="FilingDescription"/></a>
            </xsl:if>
            <xsl:if test="KeyDoc = '0'"><xsl:value-of select="FilingDescription"/></xsl:if>
          </TD>
          <td valign="top">
          <xsl:if test="contains(translate(../Company/URL,$ucletters,$lcletters), 'print=1')">
            <xsl:if test="position() mod 2 = 0">
              <xsl:attribute name="class">Docs_altdatashade</xsl:attribute>
            </xsl:if>
          </xsl:if><span id="CompanyName"><xsl:value-of select="CompanyName"/></span></td>
          <!--<TD>
            <xsl:if test="FilingType != '(none)'">
              <xsl:variable name="FilingTypeL" select="FilingTypeLink" />
              <xsl:variable name="FilingTypeAfter" select="substring-after($FilingTypeL, 'aspx?')" />
              <xsl:variable name="FilingTypeBefore" select="substring-before($FilingTypeAfter, 'IID=')" />
              <xsl:variable name="FilingTypeLink">popup.aspx?<xsl:value-of select="$FilingTypeBefore"/>iid=<xsl:value-of select="../Company/KeyInstn"/></xsl:variable>
              <a href="javascript:void(0);" class="box"><xsl:attribute name="onclick">return overlib(OLiframeContent('<xsl:value-of select="$FilingTypeLink"/>', 450, 300, 'if2', 0, 'auto'),WIDTH,450, TEXTPADDING,0, BORDER,1, STICKY, DRAGGABLE, CLOSECLICK, CAPTIONPADDING,4, CAPTION,'Type: <xsl:value-of select="FilingType"/>',MIDX,0, MIDY,0,STATUS,'SNL Financial');</xsl:attribute><xsl:value-of select="FilingType"/></a>
            </xsl:if>
            <xsl:if test="FilingType = '(none)'"><xsl:value-of select="FilingType"/></xsl:if>
          </TD>-->
          <TD ALIGN="right">
          <xsl:if test="contains(translate(../Company/URL,$ucletters,$lcletters), 'print=1')">
            <xsl:if test="position() mod 2 = 0">
              <xsl:attribute name="class">Docs_altdatashade</xsl:attribute>
            </xsl:if>
          </xsl:if>
            <xsl:value-of select="FilingDate"/>
          </TD>
          <TD ALIGN="right">
          <xsl:if test="contains(translate(../Company/URL,$ucletters,$lcletters), 'print=1')">
            <xsl:if test="position() mod 2 = 0">
              <xsl:attribute name="class">Docs_altdatashade</xsl:attribute>
            </xsl:if>
          </xsl:if>
            <xsl:value-of select="EventDate"/>
          </TD>
          <TD ALIGN="right" NOWRAP="1">
          <xsl:if test="contains(translate(../Company/URL,$ucletters,$lcletters), 'print=1')">
            <xsl:if test="position() mod 2 = 0">
              <xsl:attribute name="class">Docs_altdatashade</xsl:attribute>
            </xsl:if>
          </xsl:if>
			      <!--=====================================================================-->
			      <ul id="optionIcon">
				      <li>
					      <xsl:if test="XBRLFileFormat = 'True'">
						      <a href="{DocLink}"><img src="/images/interactive/ir/xbrl.gif" border="0" alt="Download XBRL Content" title="Download SEC Filing to XBRL"/></a>
					      </xsl:if>
					      <xsl:if test="XBRLFileFormat = 'False'">
						      <IMG SRC="/images/interactive/ir/xbrl_gray.gif" border="0" alt="XBRL SEC Filing Unavailable" title="XBRL SEC Filing Unavailable"/>
					      </xsl:if>
				      </li>
				      <li>
					      <xsl:if test="ExcelFileFormat = '1'"> <!-- and (../Company/GAAP = 21 or ../Company/GAAP = 14 or ../Company/GAAP = 20 or FilingType = '10-Q' or FilingType = '10-K')"> -->
						      <a style="margin-right: 0px;" href="{DocLink}"><img src="/images/interactive/ir/excel.gif" alt="Download SEC Filing to Excel" title="Download SEC Filing to Excel" border="0"/></a>
					      </xsl:if>
					      <xsl:if test="ExcelFileFormat = '0'"> <!-- and (../Company/GAAP = 21 or ../Company/GAAP = 14 or ../Company/GAAP = 20) "> -->
						      <IMG SRC="/images/interactive/ir/excel_gray.gif" alt="Excel SEC Filing Unavailable" title="Excel SEC Filing Unavailable"  border="0"/>
					      </xsl:if>
				      </li>
				      <li>
					      <xsl:choose>
						      <xsl:when test="KeyFileFormat = '1'">
							      <xsl:if test="normalize-space(PDFConvertible) = 'true'">
								      <a target="_new">
								      <xsl:attribute name="href">file.aspx?IID=<xsl:value-of select="../Company/KeyInstn"/>&amp;FID=<xsl:value-of select='KeyFile'/>&amp;O=3&amp;OSID=9</xsl:attribute>
									      <img src="/images/interactive/ir/pdf.gif" alt="Download SEC Filing to Adobe PDF" title="Download SEC Filing to Adobe PDF" border="0"/>
								      </a>
							      </xsl:if>
							      <xsl:if test="normalize-space(PDFConvertible) = 'false'"><IMG SRC="/images/interactive/ir/pdf_gray.gif" alt="Adobe PDF SEC Filing Unavailable" title="Adobe PDF SEC Filing Unavailable" border="0"/></xsl:if>
						      </xsl:when>
						      <xsl:when test="KeyFileFormat = '2'">
							      <xsl:if test="normalize-space(PDFConvertible) = 'true'">
								      <a target="_new">
								      <xsl:attribute name="href">file.aspx?IID=<xsl:value-of select="../Company/KeyInstn"/>&amp;FID=<xsl:value-of select='KeyFile'/>&amp;O=3&amp;OSID=9</xsl:attribute>										
									      <img src="/images/interactive/ir/pdf.gif" alt="Download SEC Filing to Adobe PDF" title="Download SEC Filing to Adobe PDF" border="0"/>
								      </a>
							      </xsl:if>
							      <xsl:if test="normalize-space(PDFConvertible) = 'false'">
								      <IMG SRC="/images/interactive/ir/pdf_gray.gif" alt="Adobe PDF SEC Filing Unavailable" title="Adobe PDF SEC Filing Unavailable" border="0"/>
							      </xsl:if>
						      </xsl:when>
						      <xsl:when test="KeyFileFormat = '3'">
							      <a target="_new">
							      <xsl:attribute name="href">file.aspx?IID=<xsl:value-of select="../Company/KeyInstn"/>&amp;FID=<xsl:value-of select='KeyFile'/>&amp;O=3&amp;OSID=9</xsl:attribute>									
								      <img src="/images/interactive/ir/pdf.gif" alt="Download SEC Filing to Adobe PDF" title="Download SEC Filing to Adobe PDF" border="0"/>
							      </a>
						      </xsl:when>
						      <xsl:otherwise>
							      <IMG SRC="/images/interactive/ir/pdf_gray.gif" alt="Adobe PDF SEC Filing Unavailable" title="Adobe PDF SEC Filing Unavailable" border="0"/>
						      </xsl:otherwise>
					      </xsl:choose>
				      </li>
				      <li>			
					      <xsl:choose>
					      <xsl:when test="KeyFileFormat = '2'">
						      <a target="_new">
						      <xsl:attribute name="href">file.aspx?IID=<xsl:value-of select="../Company/KeyInstn"/>&amp;FID=<xsl:value-of select='KeyFile'/>&amp;O=2&amp;OSID=9</xsl:attribute>
							      <img src="/images/interactive/ir/html.gif" alt="Download SEC Filing to HTML" title="Download SEC Filing to HTML" border="0"/>
						      </a>
					      </xsl:when>
					      <xsl:when test="KeyFileFormat = '1'">
						      <a target="_new">
						      <xsl:attribute name="href">file.aspx?IID=<xsl:value-of select="../Company/KeyInstn"/>&amp;FID=<xsl:value-of select='KeyFile'/>&amp;O=2&amp;OSID=9</xsl:attribute>
							      <img src="/images/interactive/ir/html.gif" alt="Download SEC Filing to HTML" title="Download SEC Filing to HTML" border="0"/>
						      </a>
					      </xsl:when>
					      <xsl:when test="KeyFileFormat = '7'">
						      <a target="_new">
						      <xsl:attribute name="href">file.aspx?IID=<xsl:value-of select="../Company/KeyInstn"/>&amp;FID=<xsl:value-of select='KeyFile'/>&amp;O=2&amp;OSID=9</xsl:attribute>
							      <img src="/images/interactive/ir/html.gif" alt="Download SEC Filing to HTML" title="Download SEC Filing to HTML" border="0"/>
						      </a>
					      </xsl:when>
					      <xsl:otherwise>
						      <IMG SRC="/images/interactive/ir/html_gray.gif" alt="HTML SEC Filing Unavailable" title="HTML SEC Filing Unavailable"  border="0"/>
					      </xsl:otherwise>			
					      </xsl:choose>
				      </li>
			      </ul>
			      <!--=====================================================================-->
            
          </TD>
        </TR>
      </xsl:for-each>
	  </tbody>
    </TABLE>
  </xsl:template>
  
  <xsl:template name="DocsOtherFilings_4">
  <xsl:variable name="OtherFiling" select="count(OtherFiling)"/>   
  <xsl:variable name="Abstract" select="count(OtherFiling[Abstract != ''])"/>  
	<div id="DocsOtherFilings">    
		<xsl:choose>
		<xsl:when test="$OtherFiling = 0">
			<h3>Other Filings</h3>
			<p>There are no documents found within search criteria</p>
		</xsl:when>
		<xsl:otherwise>	
			<table border="0" cellspacing="0" cellpadding="3" width="100%">
			<TR>
				<xsl:if test="Company/Mask = ''">
				<TD class="subhead"><h3>Other Filings</h3></TD>
				</xsl:if>
				<xsl:if test="Company/Mask != '' and Company/Mask != '345' and Company/Mask != '3' and Company/Mask != '4' and Company/Mask != '5'">
				<TD class="subhead"><h3>Type: <xsl:value-of select="Company/Mask"/> Filings</h3></TD>       
				</xsl:if>
				<xsl:if test="Company/Mask = '345' or Company/Mask = '3' or Company/Mask = '4' or Company/Mask = '5'">
				<TD class="subhead"><h3>Insider Ownership Filings</h3></TD>
				</xsl:if>
			</TR>
			</table>      
			<div class="ExpandColps">
        <xsl:if test="$Abstract &gt; 0">          
				  <a href="javascript:;" class="ExpandAll">Expand All</a> | <a href="javascript:;" class="CollapseAll">Collapse All</a>
        </xsl:if>
			</div>
			<div class="full_width">
			<table id="OtherFilings" class="display tablesorter" border="0" cellspacing="0" cellpadding="3" width="100%">
			<thead>
			  <tr>
				<td width="30%" valign="top" class="headline"><span>Filing Description</span></td>
				<td width="30%" valign="top" class="headline" nowrap="1"><span>Company</span></td>
				<!--<td width="10%" valign="top" class="headline" nowrap="1">Filing Type</td>-->
				<td width="10%" align="right" valign="top" class="headline" nowrap="1"><span>Filing Date</span></td>
				<td width="10%" align="right" valign="top" class="headline" nowrap="1"><span>Event Date</span></td>
				<td width="20%" align="right" valign="top" class="headline" nowrap="1"><span>Options</span></td>
			  </tr>
			</thead>
			<tbody>        
			<xsl:for-each select="OtherFiling">
			<TR valign="top">
				<TD>
          <xsl:choose>
          <xsl:when test="Abstract != ''">
            <xsl:attribute name="class">AbstractTD</xsl:attribute>
            <xsl:if test="contains(translate(../Company/URL,$ucletters,$lcletters), 'print=1')">
              <xsl:if test="position() mod 2 = 0">
                <xsl:attribute name="class">AbstractTD Docs_altdatashade</xsl:attribute>
              </xsl:if>
            </xsl:if>
          </xsl:when>
          <xsl:otherwise>
            <xsl:if test="contains(translate(../Company/URL,$ucletters,$lcletters), 'print=1')">
              <xsl:if test="position() mod 2 = 0">
                <xsl:attribute name="class">Docs_altdatashade</xsl:attribute>
              </xsl:if>
            </xsl:if>
          </xsl:otherwise>
          </xsl:choose>          
					<a href="{DocLink}"><xsl:value-of select="FilingDescription"/></a>
					<xsl:if test="Abstract != ''">&#160;<a href="javascript:;" class="Abstract">(less)</a></xsl:if>
					
					<xsl:if test="Abstract != ''">
            <br />
						<div id="Abstract">
						  <strong>Abstract: </strong><xsl:value-of select="Abstract"/>
						</div>
					</xsl:if>
				</TD>
				<td valign="top">
          <xsl:if test="contains(translate(../Company/URL,$ucletters,$lcletters), 'print=1')">
            <xsl:if test="position() mod 2 = 0">
              <xsl:attribute name="class">Docs_altdatashade</xsl:attribute>
            </xsl:if>
          </xsl:if>
          <xsl:choose>
            <xsl:when test="(../Company/CompanyDisplayType='0')">
              <div id="InstnName"><xsl:value-of select="InstnName"/></div>
            </xsl:when>
            <xsl:when test="((../Company/CompanyDisplayType='1') and (keyInstn != TreeID))">
              <div id="InstnName"><xsl:value-of select="InstnName"/></div>
            </xsl:when>
            <xsl:otherwise>&#160;</xsl:otherwise>
          </xsl:choose> 
        </td>
				<!--<TD>
				  <xsl:variable name="FilingTypeL" select="FilingTypeLink" />
				  <xsl:variable name="FilingTypeAfter" select="substring-after($FilingTypeL, 'aspx?')" />
				  <xsl:variable name="FilingTypeBefore" select="substring-before($FilingTypeAfter, 'IID=')" />
				  <xsl:variable name="FilingTypeLink">popup.aspx?<xsl:value-of select="$FilingTypeBefore"/>iid=<xsl:value-of select="../Company/KeyInstn"/></xsl:variable>
				  <a href="javascript:void(0);" class="box"><xsl:attribute name="onclick">return overlib(OLiframeContent('<xsl:value-of select="$FilingTypeLink"/>', 450, 300, 'if2', 0, 'auto'),WIDTH,450, TEXTPADDING,0, BORDER,1, STICKY, DRAGGABLE, CLOSECLICK, CAPTIONPADDING,4, CAPTION,'Type: <xsl:value-of select="FilingType"/>',MIDX,0, MIDY,0,STATUS,'SNL Financial');</xsl:attribute><xsl:value-of select="FilingType"/></a>
				</TD>-->
				<TD ALIGN="right">
          <xsl:if test="contains(translate(../Company/URL,$ucletters,$lcletters), 'print=1')">
            <xsl:if test="position() mod 2 = 0">
              <xsl:attribute name="class">Docs_altdatashade</xsl:attribute>
            </xsl:if>
          </xsl:if><xsl:value-of select="FilingDate"/></TD>
				<TD ALIGN="right">
          <xsl:if test="contains(translate(../Company/URL,$ucletters,$lcletters), 'print=1')">
            <xsl:if test="position() mod 2 = 0">
              <xsl:attribute name="class">Docs_altdatashade</xsl:attribute>
            </xsl:if>
          </xsl:if><xsl:value-of select="EventDate"/></TD>
				<TD ALIGN="right" NOWRAP="1" valign="top">
          <xsl:if test="contains(translate(../Company/URL,$ucletters,$lcletters), 'print=1')">
            <xsl:if test="position() mod 2 = 0">
              <xsl:attribute name="class">Docs_altdatashade</xsl:attribute>
            </xsl:if>
          </xsl:if>
					<!--=====================================================================-->
					<ul id="optionIcon">
						<li>
							<xsl:if test="XBRLFileFormat = 'True'">
							  <a style="margin-right: 0px;">
								<xsl:attribute name="href">
								  <xsl:value-of select="DocLink"/>
								</xsl:attribute>
								<img src="/images/interactive/ir/xbrl.gif" border="0" alt="Download XBRL Content" title="Download SEC Filing to XBRL"/>
							  </a>
							</xsl:if>
							<xsl:if test="XBRLFileFormat = 'False'">
							  <IMG SRC="/images/interactive/ir/xbrl_gray.gif" border="0" alt="XBRL SEC Filing Unavailable" title="XBRL SEC Filing Unavailable"/>
							</xsl:if>
						</li>
						<li>
							<xsl:if test="ExcelFileFormat = '1'"> <!-- and (../Company/GAAP = 21 or ../Company/GAAP = 14 or ../Company/GAAP = 20 or FilingType = '10-Q' or FilingType = '10-K')"> -->
								<a>
									<xsl:attribute name="href">
										<xsl:value-of select="DocLink"/>
									</xsl:attribute>
									<img src="/images/interactive/ir/excel.gif" alt="Download SEC Filing to Excel" title="Download SEC Filing to Excel" border="0"/>
								</a>
							</xsl:if>
							<xsl:if test="ExcelFileFormat = '0'"> <!-- and (../Company/GAAP = 21 or ../Company/GAAP = 14 or ../Company/GAAP = 20) "> -->
								<IMG SRC="/images/interactive/ir/excel_gray.gif" alt="Excel SEC Filing Unavailable" title="Excel SEC Filing Unavailable"  border="0"/>
							</xsl:if>
						</li>
						<li>
							<xsl:choose>
								<xsl:when test="KeyFileFormat = '1'">
									<xsl:if test="normalize-space(PDFConvertible) = 'true'">
										<a>
											<xsl:attribute name="href">
												file.aspx?IID=<xsl:value-of select="../Company/KeyInstn"/>&amp;FID=<xsl:value-of select='KeyFile'/>&amp;O=3&amp;OSID=9
											</xsl:attribute>
											<xsl:attribute name="target">_new</xsl:attribute>
											<img src="/images/interactive/ir/pdf.gif" alt="Download SEC Filing to Adobe PDF" title="Download SEC Filing to Adobe PDF" border="0"/>
										</a>
									</xsl:if>
									<xsl:if test="normalize-space(PDFConvertible) = 'false'">
										<IMG SRC="/images/interactive/ir/pdf_gray.gif" alt="Adobe PDF SEC Filing Unavailable" title="Adobe PDF SEC Filing Unavailable" border="0"/>
									</xsl:if>
								</xsl:when>
								<xsl:when test="KeyFileFormat = '2'">
									<xsl:if test="normalize-space(PDFConvertible) = 'true'">
										<a>
											<xsl:attribute name="href">
												file.aspx?IID=<xsl:value-of select="../Company/KeyInstn"/>&amp;FID=<xsl:value-of select='KeyFile'/>&amp;O=3&amp;OSID=9
											</xsl:attribute>
											<xsl:attribute name="target">_new</xsl:attribute>
											<img src="/images/interactive/ir/pdf.gif" alt="Download SEC Filing to Adobe PDF" title="Download SEC Filing to Adobe PDF" border="0"/>
										</a>
									</xsl:if>
									<xsl:if test="normalize-space(PDFConvertible) = 'false'">
										<IMG SRC="/images/interactive/ir/pdf_gray.gif" alt="Adobe PDF SEC Filing Unavailable" title="Adobe PDF SEC Filing Unavailable" border="0"/>
									</xsl:if>
								</xsl:when>
								<xsl:when test="KeyFileFormat = '3'">
									<a>
										<xsl:attribute name="href">
										file.aspx?IID=<xsl:value-of select="../Company/KeyInstn"/>&amp;FID=<xsl:value-of select='KeyFile'/>&amp;O=3&amp;OSID=9
										</xsl:attribute>
										<xsl:attribute name="target">_new</xsl:attribute>
										<img src="/images/interactive/ir/pdf.gif" alt="Download SEC Filing to Adobe PDF" title="Download SEC Filing to Adobe PDF" border="0"/>
									</a>
								</xsl:when>
								<xsl:otherwise>
									<IMG SRC="/images/interactive/ir/pdf_gray.gif" alt="Adobe PDF SEC Filing Unavailable" title="Adobe PDF SEC Filing Unavailable" border="0"/>
								</xsl:otherwise>
							</xsl:choose>
						</li>
						<li>
							<xsl:choose>
								<xsl:when test="KeyFileFormat = '2'">
									<a>
										<xsl:attribute name="href">
											file.aspx?IID=<xsl:value-of select="../Company/KeyInstn"/>&amp;FID=<xsl:value-of select='KeyFile'/>&amp;O=2&amp;OSID=9
										</xsl:attribute>
										<xsl:attribute name="target">_new</xsl:attribute>
										<img src="/images/interactive/ir/html.gif" alt="Download SEC Filing to HTML" title="Download SEC Filing to HTML" border="0"/>
									</a>
								</xsl:when>
								<xsl:when test="KeyFileFormat = '1'">
									<a>
										<xsl:attribute name="href">
											file.aspx?IID=<xsl:value-of select="../Company/KeyInstn"/>&amp;FID=<xsl:value-of select='KeyFile'/>&amp;O=2&amp;OSID=9
										</xsl:attribute>
										<xsl:attribute name="target">_new</xsl:attribute>
										<img src="/images/interactive/ir/html.gif" alt="Download SEC Filing to HTML" title="Download SEC Filing to HTML" border="0"/>
									</a>
								</xsl:when>
								<xsl:when test="KeyFileFormat = '7'">
									<a>
										<xsl:attribute name="href">
											file.aspx?IID=<xsl:value-of select="../Company/KeyInstn"/>&amp;FID=<xsl:value-of select='KeyFile'/>&amp;O=2&amp;OSID=9
										</xsl:attribute>
										<xsl:attribute name="target">_new</xsl:attribute>
										<img src="/images/interactive/ir/html.gif" alt="Download SEC Filing to HTML" title="Download SEC Filing to HTML" border="0"/>
									</a>
								</xsl:when>
								<xsl:otherwise>
									<IMG SRC="/images/interactive/ir/html_gray.gif" alt="HTML SEC Filing Unavailable" title="HTML SEC Filing Unavailable" border="0"/>
								</xsl:otherwise>			
							</xsl:choose>
						</li>
					</ul>
          <!--=====================================================================-->
				</TD>
			</TR>			
			</xsl:for-each>
			</tbody>
			</table>
				<xsl:call-template name="Docs_SourceTag_4" />
			</div>
		</xsl:otherwise>
		</xsl:choose>
	</div>
  </xsl:template>

  <xsl:template name="Docs_SourceTag_4">
	<div id="Docs_Foot">
		<xsl:choose>
		<xsl:when test="Company/Total &gt; Company/RowsPerPage">
			<xsl:variable name="EndPage" select="Company/Start + Company/RowsPerPage - 1"/>
			<div class="Docs_Pagination_left">
              Showing <span class="pagevalue"><xsl:value-of select="Company/Start"/></span> to
              <span class="pagevalue"><xsl:choose>
                <xsl:when test="$EndPage &gt; Company/Total">
                  <xsl:value-of select="Company/Total"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="$EndPage"/>
                </xsl:otherwise>
              </xsl:choose></span>
              of <span class="pagevalue"><xsl:value-of select="Company/Total"/></span> entries
			</div>
			<div class="Docs_Pagination_right">				
				<div class="PRPage"> 
				<xsl:variable name="Selected-Year" select="Year"/>
				<xsl:variable name="selected_year" select="AvailableYears[Selected = 'selected']/Year" />
				<xsl:variable name="Start" select="Company/Start" />
				<xsl:variable name="RowsPerPage" select="Company/RowsPerPage" />
				<xsl:variable name="Prev">
					<xsl:choose>
						<xsl:when test="Company/Start &gt;=1"><xsl:value-of select="$Start - $RowsPerPage" /></xsl:when>
						<xsl:otherwise>0</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="Next">
					<xsl:choose>
						<xsl:when test="Company/Start &lt;= $EndPage"><xsl:value-of select="$Start + $RowsPerPage" /></xsl:when>
						<xsl:otherwise><xsl:value-of select="Company/Start" /></xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="total" select="round(((Company/Total) - $RowsPerPage) div $RowsPerPage)"/>
				<xsl:variable name="totalPage" select="round($total * $RowsPerPage) + 1"/>
				<xsl:variable name="LastPage">
					<xsl:value-of select="$totalPage" />
				</xsl:variable>	
					<!-- Next Last code -->
					<xsl:for-each select="Pagination">
					<xsl:variable name="Last">
						<xsl:if test="position() = last()"><xsl:value-of select="substring-after(PageLink,'start=')" /></xsl:if>
					</xsl:variable>
					<xsl:variable name="LastPages">
					  <xsl:if test="position() = last()"><xsl:value-of select="substring-before($Last,'&amp;')" /></xsl:if>
					</xsl:variable>
            
					<xsl:if test="position() = last()">
					<div id="NextLast">
						<xsl:choose>
							<xsl:when test="../Company/Start &lt; $Last or ../Company/Start &lt; $LastPages">
								<a href="javascript:;" class="Pgnext" target="_self">
								<xsl:attribute name="href"><xsl:value-of select="concat('docs.aspx?IID=', ../Company/KeyInstn,'&amp;start=', $Next)"/></xsl:attribute>Next</a>								
								<!--<a class="Pglast" target="_self">
								<xsl:attribute name="href"><xsl:value-of select="concat('docs.aspx?IID=', ../Company/KeyInstn,'&amp;start=', $Last)"/></xsl:attribute>Last</a>-->
							</xsl:when>
							<xsl:otherwise>
								<span class="Pgnext unactive">Next</span>
								<!--<span class="Pglast unactive">Last</span>-->
							</xsl:otherwise>
						</xsl:choose>
					</div>
					</xsl:if>					
					</xsl:for-each>	
					<!-- End Next Last code -->
					
					<ul id="pagination_ul" class="pages">					
					<xsl:for-each select="Pagination">
					<xsl:variable name="PageLabel">
						<xsl:choose>
						<xsl:when test="PageLabel = '...'">
							<xsl:value-of select=" position()"/>
						</xsl:when>
						<xsl:otherwise><xsl:value-of select="PageLabel"/></xsl:otherwise>
						</xsl:choose>
					</xsl:variable>					
					<xsl:choose>
					<xsl:when test="CurrentPage = 'false'">
						<li class="pagenum"><a target="_self">
						<xsl:attribute name="href"><xsl:value-of select="PageLink"/><xsl:if test="$selected_year != '' and $selected_year != 'All Years'">&amp;YearMask&#61;<xsl:value-of select="$selected_year"/></xsl:if></xsl:attribute>
							<xsl:value-of select="$PageLabel"/>
						</a></li>
					</xsl:when>
					<xsl:otherwise>						
						<li class="pagenum select">
						<xsl:value-of select="$PageLabel"/></li>
					</xsl:otherwise>
					</xsl:choose>
					</xsl:for-each>	
					</ul>
				
					<!-- First Previous code -->
					<div id="FirstPrevious">
					<xsl:choose>
						<xsl:when test="Company/Start &lt;= 1">
							<!--<span class="Pgfirst unactive">First</span>-->
							<span class="Pgprev unactive">Previous</span>
						</xsl:when>
						<xsl:otherwise>
							<!--<a class="Pgfirst" target="_self">
							<xsl:attribute name="href"><xsl:value-of select="concat('docs.aspx?IID=', Company/KeyInstn)"/></xsl:attribute>First</a>-->
							<a href="javascript:;"  class="Pgprev" target="_self">
							<xsl:attribute name="href"><xsl:value-of select="concat('docs.aspx?IID=', Company/KeyInstn,'&amp;start=', $Prev)"/></xsl:attribute>Previous</a>
						</xsl:otherwise>
					</xsl:choose>
					</div>
					<!-- End First Previous code -->					
					
				</div>
			</div>
		  </xsl:when>
		</xsl:choose>
	</div>
  </xsl:template>
  
  <!-- End Docs Template Style 4 -->

</xsl:stylesheet>
