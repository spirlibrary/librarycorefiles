<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.ord/TR/REC-html40"
	xmlns:irw="http://www.snl.com/xml/irw">

<xsl:output method="html" media-type="text/html" indent="no"/>
<xsl:template name="PRInfoScriptResource_4">
  <style type="text/css">
    #PRInfobot ul, #PRInfobot ul li{ list-style: none outside none; margin: 0; padding: 0; }
    #PRInfobot ul li { float: left; line-height: 15px; padding: 3px; vertical-align: top;}
  </style>
</xsl:template>
  
<xsl:template match="irw:IRW/irw:RSS.PRInfo">
  <xsl:call-template name="PRInfoScriptResource_4" />  
  <div id="outer">
    <div id="PRInfo">
      <h3 class="title1light PRinfoTitle">RSS Feeds</h3>
      <div class="PRInfo">        
        <P><SPAN class="defaultbold">What are RSS feeds?</SPAN> </P>
        <P><span class="default">
          RSS, which stands for Really Simple Syndication, provides you with a new way
          of keeping on top of S&amp;P Global Market Intelligence (S&amp;P) news. RSS allows you to view constantly updated
          headlines from S&amp;P, in addition to headlines from other news publications, in
          one place. You can choose which feeds you would like to display.
        </span></P>
        <P><SPAN class="defaultbold">How do I access the feeds?</SPAN> </P>
        <P><span class="default">You will need special software to access RSS feeds. RSS news readers allow 
        you to view the service you select in one place and, by automatically retrieving 
        updates, stay current with new content soon after it is published. There are 
        many readers available and most are free. Some popular readers for articles 
        include: <A href="http://www.feeddemon.com/feeddemon/index.asp" target="pop" title="Feed Demon">Feed 
        Demon</A>, <A href="http://www.feedreader.com/" target="pop" title="FeedReader">FeedReader</A>, <A 
        href="http://my.yahoo.com/" target="pop" title="My Yahoo!">My Yahoo!</A>, <A 
        href="http://www.newsgator.com/" target="pop" title="NewsGator">NewsGator</A> and <A 
        href="http://www.newzcrawler.com/" target="pop" title="NewzCrawler">NewzCrawler</A>. </span></P>
        <P><span class="default">
          After setting up your news reader, you will be ready to use S&amp;P's RSS feeds.
          To subscribe to one or more feeds below, follow the instructions for your
          particular news reader.</span> </P>
        <P><SPAN class="defaultbold">Terms of Use</SPAN> </P>
        <P><span class="default">
          These RSS feeds are provided by S&amp;P Global Market Intelligence ("S&amp;P") solely for the
          purpose of allowing you to view headlines from S&amp;P within newsreaders for your
          personal and noncommercial use. If you are not a subscriber to S&amp;P’s services,
          no other rights are granted to you. If you are a subscriber to S&amp;P’s services,
          then any additional rights you have with respect to content from S&amp;P are found
          in your subscription agreement with S&amp;P.</span></P>
        <P><span class="default">
          It is understood that S&amp;P does not guarantee or warrant the correctness,
          completeness, currentness, merchantability or fitness for a particular purpose
          of the information it provides. S&amp;P shall not be liable for any loss or injury
          arising out of or caused, in whole or in part, directly or indirectly, by S&amp;P's
          negligent acts or omissions in procuring, compiling, collecting, interpreting,
          reporting, communicating, or delivering any information. It is further agreed
          that S&amp;P shall not be liable for interruptions in an RSS feed Service for any
          cause or reason.</span></P>
        <P><span class="default">THE RSS FEEDS AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
        KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO THE IMPLIED 
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.</span></P>
        <div id="PRInfobot">
          <ul class="PRInfobot">
            <li>Press releases</li>
            <li><a><xsl:attribute name="href"><xsl:value-of select="concat('http://www.snl.com/irweblinkx/rss/prfeed.aspx?iid=', Company/KeyInstn)"/></xsl:attribute><IMG height="14" src="images/rss_xml.gif" width="36" border="0" alt="RSS XML"/></a></li>
            <li><xsl:value-of select="concat('http://www.snl.com/irweblinkx/rss/prfeed.aspx?iid=', Company/KeyInstn)"/></li>
          </ul>
        </div>
      </div>
    </div>
  </div> 
</xsl:template>
</xsl:stylesheet>

  