<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.ord/TR/REC-html40"
	xmlns:irw="http://www.snl.com/xml/irw">

<xsl:output method="html" media-type="text/html" indent="no"/>





<xsl:template match="irw:SiteMap">
<!-- Template 1 -->
<xsl:variable name="TemplateName" select="Company/TemplateName"/>
<xsl:choose>
<!-- Template 1-->
<xsl:when test="$TemplateName = 'AltMap1'">
<xsl:call-template name="TemplateONEstylesheet"/>
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" CLASS="">
	<xsl:call-template name="Title_S2"/>
	<TR ALIGN="">
	<TD CLASS="leftTOPbord" colspan="2" valign="top">
			<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="4" WIDTH="100%">
				<tr>
					<td class="colorlight">
						<table border="0" cellspacing="0" cellpadding="4" width="100%">
							<xsl:for-each select="MenuEntry[MenuParent='0']">
							<!--<xsl:sort select="FormOrder"/>-->
								<tr>
									<td><xsl:text disable-output-escaping="yes">&#8226;&amp;nbsp;</xsl:text>
										<a class="fielddef" title="{Description}">
											<xsl:attribute name="href">
											<xsl:value-of disable-output-escaping="yes" select="Href"/>
											</xsl:attribute>
											
											<xsl:value-of disable-output-escaping="yes" select="Description"/>
										</a>		
								<xsl:variable name="MenuKey" select="MenuKey"/>
								<xsl:if test="$MenuKey != 0">
									<xsl:for-each select="../MenuEntry[MenuParent=$MenuKey]">
										<br/>
										<xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text>
										<xsl:text disable-output-escaping="yes">&#8226;&amp;nbsp;</xsl:text>
										<a class="fielddef" title="{Description}">
											<xsl:attribute name="href">
											<xsl:value-of disable-output-escaping="yes" select="Href"/>
											</xsl:attribute>
											
											<xsl:value-of disable-output-escaping="yes" select="Description"/>
										</a>
									</xsl:for-each>
								</xsl:if>
									</td>
								</tr>
							</xsl:for-each>
						</table>
					</td>
				</tr>
			</TABLE>
		</TD>
	</TR>
	<TR class="default" align="left">
		<TD class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span></TD>
	</TR>
</TABLE>
<xsl:call-template name="Copyright"/>	
</xsl:when>
<!-- End Template 1 -->

<!-- Template 3 -->
<xsl:when test="$TemplateName = 'AltMap3'">
<xsl:call-template name="TemplateTHREEstylesheet"/>
<xsl:call-template name="Title_T3"/>
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" CLASS=" table2">
<TR ALIGN="">
<TD CLASS="" colspan="2" valign="top">
			<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="4" WIDTH="100%">
				<tr>
					<td class="colorlight">
						<table border="0" cellspacing="0" cellpadding="4" width="100%">
							<xsl:for-each select="MenuEntry[MenuParent='0']">
						<!--<xsl:sort select="FormOrder"/>-->
								<tr>
									<td><xsl:text disable-output-escaping="yes">&#8226;&amp;nbsp;</xsl:text>
										<a class="fielddef" title="{Description}">
											<xsl:attribute name="href">
											<xsl:value-of disable-output-escaping="yes" select="Href"/>
											</xsl:attribute>
											
											<xsl:value-of disable-output-escaping="yes" select="Description"/>
										</a>		
								<xsl:variable name="MenuKey" select="MenuKey"/>
								<xsl:if test="$MenuKey != 0">
									<xsl:for-each select="../MenuEntry[MenuParent=$MenuKey]">
										<br/>
										<xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text>
										<xsl:text disable-output-escaping="yes">&#8226;&amp;nbsp;</xsl:text>
										<a class="fielddef" title="{Description}">
											<xsl:attribute name="href">
											<xsl:value-of disable-output-escaping="yes" select="Href"/>
											</xsl:attribute>
											
											<xsl:value-of disable-output-escaping="yes" select="Description"/>
										</a>
									</xsl:for-each>
								</xsl:if>
									</td>
								</tr>
							</xsl:for-each>
						</table>
					</td>
				</tr>
			</TABLE>
		</TD>
	</TR>
	<TR class="default" align="left">
		<TD class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span></TD>
	</TR>
</TABLE>
<xsl:call-template name="Copyright"/>	
</xsl:when>
<!-- End Template 3 -->

<!-- Default Template -->
<xsl:otherwise>
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%" CLASS="colordark">
<xsl:variable name="CompanyNameCaps" select="translate(Company/InstnName, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
	<xsl:call-template name="Title_S1"/>
	<TR	ALIGN="">
		<TD CLASS="colordark" colspan="2"> 
			<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" WIDTH="100%">
				<tr>
					<td class="colorlight">
						<table border="0" cellspacing="0" cellpadding="3" width="100%">
							<xsl:for-each select="MenuEntry[MenuParent='0']">
							<!--<xsl:sort select="FormOrder"/>-->
								<tr>
									<td><xsl:text disable-output-escaping="yes">&#8226;&amp;nbsp;</xsl:text>
										<a class="fielddef" title="{Description}">
											<xsl:attribute name="href">
											<xsl:value-of disable-output-escaping="yes" select="Href"/>
											</xsl:attribute>											
											<xsl:value-of disable-output-escaping="yes" select="Description"/>
										</a>		
										<xsl:variable name="MenuKey" select="MenuKey"/>
										<xsl:if test="$MenuKey != 0">
										<xsl:for-each select="../MenuEntry[MenuParent=$MenuKey]">
										<br/>
										<xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text>
										<xsl:text disable-output-escaping="yes">&#8226;&amp;nbsp;</xsl:text>
										<a class="fielddef" title="{Description}">
											<xsl:attribute name="href">
											<xsl:value-of disable-output-escaping="yes" select="Href"/>
											</xsl:attribute>											
											<xsl:value-of disable-output-escaping="yes" select="Description"/>
										</a>
										</xsl:for-each>
									</xsl:if>
									</td>
								</tr>
							</xsl:for-each>
						</table>
					</td>
				</tr>
			</TABLE>
		</TD>
	</TR>
	<TR class="default" align="left">
		<TD class="colorlight" colspan="2"><span class="default"><xsl:value-of select="Company/Footer" disable-output-escaping="yes"/></span></TD>
	</TR>
</TABLE>
<xsl:call-template name="Copyright"/>	
</xsl:otherwise>
</xsl:choose>
<!-- End Default Template -->
</xsl:template>





</xsl:stylesheet>
  
