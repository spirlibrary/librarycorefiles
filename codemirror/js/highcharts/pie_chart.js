jQuery(document).ready(function () {

    // Pie chart container
    Highcharts.chart('pie_chart_container', {

        // Set colors of pie 
        colors: ['rgb(214,0,42)', 'rgb(60,60,59)', 'rgb(167,152,134)', 'rgb(26,62,119)', 'rgb(168,185,24)'],

        // Set styling of Chart container
        chart: {
            type: 'pie',
            height: 500
        },

        // Set styling of Title
        title: {
            text: 'Pie chart example',
            useHTML: true,
            style: {
                color: 'rgb(255,255,255)',
                fontSize: '30px',
                backgroundColor: "rgb(214,0,42)",
                border: '2px solid rgb(214,0,42)',
                lineHeight: '35px',
                padding: '5px 30px'
            }
        },

        //Set styling of Tooltip
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },

        // Set the pie attributes
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    
                }
            }
        },

        // Data of the pie
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [{
                name: 'Data1',
                y: 56.33
            }, {
                name: 'Data2',
                y: 24.03,
                sliced: true,
                selected: true
            }, {
                name: 'Data3',
                y: 10.38
            }, {
                name: 'Data4',
                y: 4.77
            }, {
                name: 'Data5',
                y: 0.91
            }, {
                name: 'Data6',
                y: 0.2
            }]
        }]
    });
});