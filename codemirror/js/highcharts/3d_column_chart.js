
// Set up the chart
var chart = new Highcharts.Chart({

    //Set colors of column
    colors: ['rgb(214,0,42)'],

    chart: {
        //Give Id in which you want to Draw chart
        renderTo: '3d_column_chart_container',
        type: 'column',

        //Enable 3D option to give 3d effect
        options3d: {
            enabled: true,
            alpha: 15,
            beta: 15,
            depth: 50,
            viewDistance: 25
        },
        height: 500,
    },

    //Customize Title
    title: {
        text: '3D Column Chart',
        useHTML: true,
        style: {
            color: 'rgb(255,255,255)',
            fontSize: '30px',
            backgroundColor: "rgb(214,0,42)",
            border: '2px solid rgb(214,0,42)',
            lineHeight: '35px',
            padding: '5px 30px'
        }
    },

    //Customize SubTitle
    subtitle: {
        text: 'Test options by dragging the sliders below',
        style: {
            color: 'rgb(214,0,42)',
            fontSize: '18px',

        }
    },

    //Customize column's Depth
    plotOptions: {
        column: {
            depth: 25
        }
    },

    //Data of columns
    series: [{
        data: [29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
    }]
});

//Funtions to handle 3D effects
function showValues() {
    $('#alpha-value').html(chart.options.chart.options3d.alpha);
    $('#beta-value').html(chart.options.chart.options3d.beta);
    $('#depth-value').html(chart.options.chart.options3d.depth);
}

// Activate the sliders
$('#sliders input').on('input change', function () {
    chart.options.chart.options3d[this.id] = this.value;
    showValues();
    chart.redraw(false);
});

showValues();
