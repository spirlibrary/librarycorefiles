$(function () {

    $('#Show_first_and_last_data_point_value').highcharts({
        // Set colors of pie 
        colors: ['rgb(214,0,42)', 'rgb(60,60,59)', 'rgb(167,152,134)', 'rgb(26,62,119)', 'rgb(168,185,24)'],
        chart: {
            
        },
        // Set styling of Title
        title: {
            text: 'Show first and last data point value on line chart',
            useHTML: true,
            style: {
                color: 'rgb(255,255,255)',
                fontSize: '30px',
                backgroundColor: "rgb(214,0,42)",
                border: '2px solid rgb(214,0,42)',
                lineHeight: '35px',
                padding: '5px 30px'
            }
        },
        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        },
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,

                }
            },
            line: {
                dataLabels: {
                    enabled: true,
                    formatter: function () {
                        var first = this.series.data[0],
                            last = this.series.data[this.series.data.length - 1];
                        if ((this.point.category === first.category && this.point.y === first.y) ||
                            (this.point.category === last.category && this.point.y === last.y)) {
                            return this.point.y;
                        }
                        return "";
                    }
                },
            },
        },
        series: [{
            data: [29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
        }]
    });
    
});