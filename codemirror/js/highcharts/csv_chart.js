
// Give path of the csv file in .get method
$.get('codemirror/js/highcharts/csv/11_30_Total_GLA.csv', function (csv) {

    //Defien id of the container where you want to load chart
    $('#csv_chart_container').highcharts({

        // Set color of column
        colors: ['rgb(214,0,42)'],

        //Set chart type
        chart: {
            backgroundColor: 'transparent',
            type: 'column',
            
        },

        //Define csv file's variable
        data: {
            csv: csv
        },

        //Customize title
        title: {
            text: '129% Growth',
            useHTML: true,
            style: {
                color: 'rgb(255,255,255)',
                fontSize: '30px',
                backgroundColor: "rgb(214,0,42)",
                border: '2px solid rgb(214,0,42)',
                lineHeight: '35px',
                padding: '5px 30px'
            }
        },

        //Customize xAxis
        xAxis: {

            // Set categories of xAxis
            categories: ['FY 2010', 'FY 2011', 'FY 2012', 'FY 2013', 'FY 2014', 'FY 2015', 'FY 2016', 'Current'],

            //xAxis Title
            title: {
                enabled: true,
                text: 'Years'
            },

            // Color of xAxis title
            labels: {
                style: {
                    color: '#000000',
                    fontSize: '14px',
                },
                autoRotation: [0, -20, -30, -40, -50, -60, -70, -80, -90],
            },
        },
        yAxis: {

            //The pixel width of the major tick marks.
            tickWidth: 0,

            //The interval of the tick marks in axis units. When null, the tick interval is computed to approximately follow the tickPixelInterval on linear and datetime axes. On categorized axes, a null tickInterval will default to 1, one category. Note that datetime axes are based on milliseconds, so for example an interval of one day is expressed as 24 * 3600 * 1000.

            //On logarithmic axes, the tickInterval is based on powers, so a tickInterval of 1 means one tick on each of 0.1, 1, 10, 100 etc. A tickInterval of 2 means a tick of 0.1, 10, 1000 etc. A tickInterval of 0.2 puts a tick on 0.1, 0.2, 0.4, 0.6, 0.8, 1, 2, 4, 6, 8, 10, 20, 40 etc.

            //If the tickInterval is too dense for labels to be drawn, Highcharts may remove ticks.
            tickInterval: 2.0,

            //The width of the grid lines extending the ticks across the plot area. Defaults to 1.
            gridLineWidth: 0,

            //The lowest allowed value for automatically computed axis extremes. Defaults to null.
            floor: 6.0,

            //The highest allowed value for automatically computed axis extremes.
            ceiling: 18.0,

            //The width of the line marking the axis itself. Defaults to 0.
            lineWidth: 0,

            //Width of the minor, secondary grid lines.
            minorGridLineWidth: 0,

            // Title of the yAxis
            title: {
                text: 'Total Square Feet (in Millions)',
                margin: 25,
                style: {
                    color: '#000000',
                    fontSize: '14px',
                    
                }
            },

            //Labels of yAxis
            labels: {
                style: {
                    color: '#000000',
                    fontSize: '14px',
                    
                },
                format: '{value:.1f}',
                padding: {

                }

            }
        },

        //Tooltip customize
        tooltip: {

            //Callback function to format the text of the tooltip
            formatter: function () {
                
                if (this.x == 2017) {
                    return "<b>CURRENT </b>" + '<br/>' + this.series.name + ': ' + Highcharts.numberFormat(this.y, 1);
                }
                else {
                    return "<b>" + this.x + "</b>" + '<br/>' + this.series.name + ': ' + Highcharts.numberFormat(this.y, 1);
                }
            }
        },

        //Disable export option
        exporting: { enabled: false },


        plotOptions: {
            //The width of the border surrounding each column or bar.
            column: {
                borderWidth: 0
            }
        },

        //Disable legend
        legend: {
            enabled: false
        },

        //Disable credit
        credits: {
            enabled: false
        },

        series: [{
            //The maximum allowed pixel width for a column, translated to the height of a bar in a bar chart. This prevents the columns from becoming too wide when there is a small number of points in the chart. Defaults to null.
            maxPointWidth: 20
        }]
    });
});