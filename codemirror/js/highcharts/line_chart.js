jQuery(document).ready(function () {

   
    // Line Chart script
    Highcharts.chart('line_chart_container', {

        // Set colors of line
        colors: ['rgb(214,0,42)', 'rgb(60,60,59)', 'rgb(167,152,134)', 'rgb(26,62,119)', 'rgb(168,185,24)'],

        // Set styling of Chart container
        chart: {
            backgroundColor: 'transparent',
            type: 'line',
            spacingBottom: 15,
            spacingTop: 10,
            spacingLeft: 10,
            spacingRight: 10,
            height: 500,
            marginTop: 90

        },

        // Set styling of Title
        title: {
            text: 'Line Chart Example',
            useHTML: true,
            style: {
                color: 'rgb(255,255,255)',
                fontSize: '30px',
                backgroundColor: "rgb(214,0,42)",
                border: '2px solid rgb(214,0,42)',
                lineHeight: '35px',
                padding: '5px 30px'
            }
        },

        // Set styling of subtitle
        subtitle: {
            text: 'This is subtitle',
            useHTML: true,
            style: {
                color: 'rgb(214,0,42)',
                fontSize: '18px',

            }
        },

        // Set styling of yAxis
        yAxis: {
            title: {
                text: 'yAxis - Number of Employees'
            },
            labels: {
                style: {
                    color: '#000000',
                    fontSize: '14px',
                },
                autoRotation: [0, -20, -30, -40, -50, -60, -70, -80, -90],
            }
        },

        // Set styling of xAxis
        xAxis: {
            title: {
                text: 'xAxis - Years'
            },
            labels: {
                style: {
                    color: '#000000',
                    fontSize: '14px',
                },
                autoRotation: [0, -20, -30, -40, -50, -60, -70, -80, -90],
            }
        },

        // Set styling of Legend
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },

        // Set the series of xAxis
        plotOptions: {
            series: {
                pointStart: 2010
            }
        },

        //Set styling of Tooltip
        tooltip: {
            formatter: function () {
                var s = '<b>' + this.x + '</b>';
                s += '<br/>' + this.series.name + ': ' + this.y;
                return s;
            }
        },

        // Data of the chart
        series: [{
            name: 'Data1',
            data: [43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175]
        }, {
            name: 'Data2',
            data: [24916, 24064, 29742, 29851, 32490, 30282, 38121, 40434]
        }, {
            name: 'Data3',
            data: [11744, 17722, 16005, 19771, 20185, 24377, 32147, 39387]
        }, {
            name: 'Data4',
            data: [null, null, 7988, 12169, 15112, 22452, 34400, 34227]
        }, {
            name: 'Data5',
            data: [12908, 5948, 8105, 11248, 8989, 11816, 18274, 18111]
        }]

    });
});