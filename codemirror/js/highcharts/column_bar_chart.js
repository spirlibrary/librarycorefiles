    Highcharts.chart('column_bar_chart_container', {
        //Set color of columns 
        colors: ['rgb(214,0,42)', 'rgb(60,60,59)', 'rgb(167,152,134)', 'rgb(168,185,24)', 'rgb(168,185,24)'],

        chart: {
            type: 'column',
            height:500,
        },

        // Customize Title
        title: {
            text: 'Column bar chart example',
            useHTML: true,
            style: {
                color: 'rgb(255,255,255)',
                fontSize: '30px',
                backgroundColor: "rgb(214,0,42)",
                border: '2px solid rgb(214,0,42)',
                lineHeight: '35px',
                padding: '5px 30px'
            }
        },

        // Customize SubTitle
        subtitle: {
            text: 'This is subtitle',
            style: {
                color: 'rgb(214,0,42)',
                fontSize: '18px',

            }
        },

        // Define xAxix categories
        xAxis: {
            categories: [
                'Jan',
                'Feb',
                'Mar',
                'Apr',
                'May',
                'Jun',
                'Jul',
                'Aug',
                'Sep',
                'Oct',
                'Nov',
                'Dec'
            ],
            crosshair: true
        },

        // Define yAxix values
        yAxis: {
            min: 0,
            title: {
                text: 'Rainfall (mm)'
            }
        },

        // Customize Tooltip
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },


        plotOptions: {
            column: {
                //Padding between columns
                pointPadding: 0.1,
                //Border of the columns
                borderWidth: 0.3,
                borderColor:'black'
            }
        },

        //Data to draw columns
        series: [{
            name: 'Tokyo',
            data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]

        }, {
            name: 'New York',
            data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3]

        }, {
            name: 'London',
            data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2]

        }, {
            name: 'Berlin',
            data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1]

        }]
    });