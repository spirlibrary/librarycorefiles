$(function () {
    // Define max
    var max = 0;
    $('#container-get-last-value').highcharts({
        // Set colors of pie 
        colors: ['rgb(214,0,42)', 'rgb(60,60,59)', 'rgb(167,152,134)', 'rgb(26,62,119)', 'rgb(168,185,24)'],
        chart: {
            
        },
        // Set styling of Title
        title: {
            text: 'Get Last Data Point',
            useHTML: true,
            style: {
                color: 'rgb(255,255,255)',
                fontSize: '30px',
                backgroundColor: "rgb(214,0,42)",
                border: '2px solid rgb(214,0,42)',
                lineHeight: '35px',
                padding: '5px 30px'
            }
        },
        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        },
        plotOptions: {
            series: {
                // code to get last data point value
                dataLabels: {
                    enabled: true,
                    formatter: function () {
                        max = this.y;
                        return null;
                    }
                }
            }
        },
        series: [{
            data: [29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
        }]
    });
    // Container where you want to last data point value
    $('#report').html(max)
});