jQuery(document).ready(function () {


        Highcharts.chart('datatable_chart', {
            //Set color of columns
            colors: ['rgb(214,0,42)', 'rgb(60,60,59)', 'rgb(167,152,134)', 'rgb(168,185,24)', 'rgb(168,185,24)'],
            data: {
                //Define id of the table
                table: 'datatable'
            },
            chart: {
                // Define chart type
                type: 'column',
                height: 500
            },

            //Styling of Title
            title: {
                text: 'Column Chart using Data table',
                useHTML: true,
                style: {
                    color: 'rgb(255,255,255)',
                    fontSize: '30px',
                    backgroundColor: "rgb(214,0,42)",
                    border: '2px solid rgb(214,0,42)',
                    lineHeight: '35px',
                    padding: '5px 30px'
                }
            },
            //Customize XAxis
            yAxis: {
                allowDecimals: false,
                title: {
                    text: 'Units'
                }
            },
            //Customize tooltip

            tooltip: {
                formatter: function () {
                    return '<b>' + this.series.name + '</b><br/>' +
                        this.point.y + ' ' + this.point.name.toLowerCase();
                }
            }
        });

    });
