//You can do customization quickly in fiddle link:http://jsfiddle.net/L1yqthqr/7/
$(document).ready(function () {
    x = null;
    // Display states in Maps
    // You can get 'hc-key' of each states from /Interactive/newlookandfeel/4098794/js/minemap/us-all.js
    var data = [{
        'hc-key': 'us-ky',
        value: 1
    },
    {
        'hc-key': 'us-il',
        value: 2
    },
    {
        'hc-key': 'us-in',
        value: 3
    },
    {
        'hc-key': 'us-oh',
        value: 4,
        color: "rgb(41,133,140)"
    },
    {
        'hc-key': 'us-pa',
        value: 5,

    },
    {
        'hc-key': 'us-wv',
        value: 6
    },
    {
        'hc-key': 'us-va',
        value: 7,
        color: "rgb(119,149,0)"
    },
    {
        'hc-key': 'us-md',
        value: 8,
        dataLabels: {
            align: "right"
        }

    }];
    // Initiate the chart
    Highcharts.mapChart('map_container', {
        colors: ['white'],
        chart: {
            plotBorderWidth: 0,
            backgroundColor: 'rgb(167,152,134)',
            zoomType: null,
            ignoreHiddenSeries: false,
            plotBackgroundColor: null,

            events: {
                //Initialize tooltip events
                load: function () {
                    this.myTooltip = new Highcharts.Tooltip(this, this.options.tooltip);
                },

                click: function () {
                    this.myTooltip.hide();
                },
            }
        },
        colors: ['rgb(60,60,59)'],
        title: {
            text: ''
        },

        exporting: { enabled: false },
        credits: {
            enabled: false
        },

        mapNavigation: {
            //Zoom +/- functionality
            enableDoubleClickZoom: false,
            enableDoubleClickZoomTo: false,
            enableMouseWheelZoom: false,
            enabled: true,
            buttons: {
                zoomIn: {
                    // the lower the value, the greater the zoom in
                    onclick: function () { this.mapZoom(0.8); }
                },
                zoomOut: {
                    // the higher the value, the greater the zoom out
                    onclick: function () { this.mapZoom(10); }
                },
            },
            buttonOptions: {
                style: {
                    color: "#fff",

                },
                theme: {
                    fill: '#333333',
                    'stroke-width': 1,
                    stroke: '#222222',
                    r: 0,
                    states: {
                        hover: {
                            fill: '#222222'
                        },
                        select: {
                            stroke: '#333333',
                            fill: '#222222'
                        }
                    }
                },
                verticalAlign: 'bottom'
            }
        },

        legend: {
            // Bottom right legends
            enabled: true,
            align: 'right',
            verticalAlign: 'bottom',
            layout: 'vertical',
            floating: true,
            itemHiddenStyle: { "color": "#cccccc" }
        },

        tooltip: {
            hideDelay: 100,
            enabled: true,
            backgroundColor: '#f4f4f4',
            borderRadius: 50,
            headerFormat: '',
            useHTML: true,
            followPointer: false,

            style: {
                // It must be 'auto' to click links in tooltip
                pointerEvents: 'auto',
                width:250,
            },
            // Customization of each tooltip
            formatter: function () {
                if (this.point.name == "HAMILTON COMPLEX") {
                    return '<div id="HAMILTON_COMPLEX" class="tooltipCustom"><div class="tooltiptext"><b>' + this.point.name + '</b><p>Hamilton County Coal, LLC operates Mine #1, which is an underground mine located near the city of Dahlgren in Hamilton County, Illinois. Mine #1 utilizes longwall mining techniques to produce high-sulfur coal. The preparation plant at Hamilton County Coal has a throughput capacity of 2,000 tons of raw coal per hour.</p></div></div>'
                }
                else if (this.point.name == "RIVER VIEW COMPLEX") {
                    return '<div id="RIVER_VIEW" class="tooltipCustom"><div class="tooltiptext"><b>' + this.point.name + '</b><p>River View Coal, LLC operates the River View mine, which is an underground mine located near Morganfield in Union County, Kentucky. River View utilizes continuous mining units employing room-and-pillar mining techniques to produce high-sulfur coal and is the largest mine of its type in the nation. </p></div></div>'
                }
                else if (this.point.name == "DOTIKI COMPLEX") {
                    return '<div id="DOTIKI_COMPLEX" class="tooltipCustom"><div class="tooltiptext"><b>' + this.point.name + '</b><p>Webster County Coal, LLC operates Dotiki, which is an underground mine located near the city of Providence in Webster County, Kentucky. Dotiki utilizes continuous mining units employing room-and-pillar mining techniques to produce high-sulfur coal. </p></div></div>'
                }
                else if (this.point.name == "GIBSON COMPLEX SOUTH") {
                    return '<div id="GIBSON_COMPLEX_SOUTH" class="tooltipCustom"><div class="tooltiptext"><b>' + this.point.name + '</b><p>Gibson County Coal, LLC operates the Gibson South Mine, located near the city of Princeton in Gibson County, Indiana. Gibson South is an underground mine and utilizes continuous mining units employing room-and-pillar mining techniques to produce low/medium-sulfur coal.</p></div></div>'
                }
                else if (this.point.name == "GIBSON COMPLEX NORTH") {
                    return '<div id="GIBSON_COMPLEX_NORTH" class="tooltipCustom"><div class="tooltiptext"><b>' + this.point.name + '</b><p>Gibson County Coal, LLC operates the Gibson North mine, an underground mining complex located near the city of Princeton in Gibson County, Indiana. When active, the mine utilizes continuous mining units employing room-and-pillar mining techniques to produce medium-sulfur coal</p></div></div>'
                }
                else if (this.point.name == "WARRIOR COMPLEX") {
                    return '<div id="WARRIOR_COMPLEX" class="tooltipCustom"><div class="tooltiptext"><b>' + this.point.name + '</b><p>Warrior Coal, LLC operates the Cardinal mine, located near Madisonville in Hopkins County, Kentucky. Warrior utilizes continuous mining units employing room-and-pillar mining techniques. Warrior\'s preparation plant has a throughput capacity of 1,200 tons of raw coal per hour.</p></div></div>'
                }
                else if (this.point.name == "ONTON") {
                    return '<div id="ONTON" class="tooltipCustom"><div class="tooltiptext"><b>' + this.point.name + '</b><p>Sebree Mining, LLC operated the Onton #9 mine, located near Sebree in Webster County, Kentucky. When active, the Onton #9 mine utilizes continuous miner units employing room-and-pillar mining techniques. The preparation plant has a throughput capacity of 750 tons per hour.</p></div></div>'
                }
                else if (this.point.name == "MT. VERNON") {
                    return '<div id="MT_VERNON" class="tooltipCustom"><div class="tooltiptext"><b>' + this.point.name + '</b><p>The Mt. Vernon Transfer Terminal is a coal-loading terminal on the Ohio River at Mt. Vernon, Indiana. Coal is delivered to Mt. Vernon by both rail and truck. The terminal has a capacity of 8 million tons per year with existing ground storage.</p></div></div>'
                }
                else if (this.point.name == "MC MINING") {
                    return '<div id="MC_MINING" class="tooltipCustom"><div class="tooltiptext"><b>' + this.point.name + '</b><p>Excel Mining, LLC operates MC Mining, located near the city of Pikeville in Pike County, Kentucky. The underground operation utilizes continuous mining units employing room-and-pillar mining techniques to produce low-sulfur coal. </p></div></div>'
                }
                else if (this.point.name == "TUNNEL RIDGE") {
                    return '<div id="TUNNEL_RIDGE" class="tooltipCustom"><div class="tooltiptext"><b>' + this.point.name + '</b><p>Tunnel Ridge, LLC operates the Tunnel Ridge Mine, located near Wheeling, West Virginia. Tunnel Ridge utilizes the longwall mining technique to produce medium/high-sulfur coal. The Tunnel Ridge preparation plant has a throughput capacity of 2,000 tons of raw coal per hour.</p></div></div>'
                }
                else {
                    return '<div id="METTIKI" class="tooltipCustom"><div class="tooltiptext"><b>' + this.point.name + '</b><p>Mettiki Coal (WV), LLC operated the Mountain View Mine, located in Tucker County, West Virginia. The Mountain View mine utilizes the longwall mining technique to produce medium-sulfur coal. Mettiki is capable of producing a coal for both the steam coal and metallurgical coal markets.</p></div></div>'
                }
            }
        },
        plotOptions: {
            series: {
                borderColor: '#222222',
                stickyTracking: false,
                states: {
                    //Hover effect on states
                    hover: {
                        color: '#222222',
                        borderColor: '#004678'
                    }
                },


            }
        },

        series: [{

            data: data,//Assign data which is defined at top
            mapData: Highcharts.maps['countries/us/us-all'],
            joinBy: 'hc-key',//It will join each states in map which is defined at top. You can get 'hc-key' of each states from /Interactive/newlookandfeel/4098794/js/minemap/us-all.js
            allAreas: false,
            name: '',
            enableMouseTracking: false,
            showInLegend: false,
            dataLabels: {
                // To display name of each states
                style: { "textOutline": "0", "color": "rgba(190,183,169,0.5)", "fontWeight": "bold", "fontStyle": "italic" },
                allowOverlap: true,
                enabled: true,
                className: "minemap-series",
                formatter: function () {
                    if (this.point.value) {
                        return this.point.name;
                    }
                }
            },
        }, {
            // Specify points using lat/lon
            cursor: 'pointer',
            type: 'mappoint',
            name: 'ILLINOIS BASIN',
            color: '#ffffff',
            data: [{
                name: 'HAMILTON COMPLEX',
                lat: 38.170900,
                lon: -88.602297,
                dataLabels: {
                    align: "left",
                    y: 12,
                    x: 2,
                }
            }, {
                name: 'RIVER VIEW COMPLEX',
                lat: 37.742983,
                lon: -87.887961,
                dataLabels: {
                    align: "left",
                    y: 12,
                    x: -130,
                },

            }, {
                name: 'DOTIKI COMPLEX',
                lat: 37.453354,
                lon: -87.774104,
                dataLabels: {
                    align: "left",
                    y: 12,
                    x: -105,
                },
            }, {
                name: 'GIBSON COMPLEX SOUTH',
                lat: 38.30666667,
                lon: -87.699722,
                dataLabels: {
                    align: "left",
                    y: 12,
                    x: 2,
                }
            },

            {
                name: 'WARRIOR COMPLEX',
                lat: 37.328118,
                lon: -87.498910,
                dataLabels: {
                    align: "left",
                    y: 12,
                    x: 2,
                }
            }, {
                name: 'ONTON',
                lat: 37.607122,
                lon: -87.528681,
                dataLabels: {
                    align: "left",
                    y: 12,
                    x: 2,
                }
            }, {
                name: 'MT. VERNON',
                lat: 37.922751,
                lon: -87.864726,
                dataLabels: {
                    align: "left",
                    y: 12,
                    x: 2,

                }
            }],
            dataLabels: {
                style: {
                    "color": "white", "fontWeight": "normal", "textOutline": "0", "cursor": "pointer"
                },
                className: "illinos-series",
                allowOverlap: true,
                color: 'white',
                shadow: false,
                crop: true,
            },
            events: {
                //Events to show/hide tooltips
                click: function (evt) {
                    this.chart.myTooltip.refresh(evt.point, evt);
                },
                //mouseOver: function () {
                //    this.chart.myTooltip.hide();
                //},


            }

        }, {
            // Specify points using lat/lon
            type: 'mappoint',
            cursor: 'pointer',
            name: 'APPALACHIA',
            color: 'rgb(214,0,42)',
            data: [{
                name: 'MC MINING',
                lat: 37.591513,
                lon: -82.492947,
                color: 'rgb(214,0,42)',
            }, {
                name: 'TUNNEL RIDGE',
                lat: 40.118898,
                lon: -80.587843,
                color: 'rgb(214,0,42)',
            }, {
                name: 'METTIKI',
                lat: 39.263252,
                lon: -79.428611,
                color: 'rgb(214,0,42)',

            }],
            dataLabels: {
                style: {
                    "color": "white", "fontWeight": "normal", "textOutline": "0", "cursor": "pointer"
                },
                className: "appalachia-series",
                allowOverlap: true,
                color: 'rgb(214,0,42)',
                align: 'left',
                shadow: false,
                crop: true,
                y: 12,
                x: 2,
            },
            events: {
                //Events to show/hide tooltips
                click: function (evt) {
                    this.chart.myTooltip.refresh(evt.point, evt);
                },
                //mouseOver: function () {
                //    this.chart.myTooltip.hide();
                //},


            }

        }]
    });

});