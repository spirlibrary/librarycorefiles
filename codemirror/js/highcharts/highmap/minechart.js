$(document).ready(function () {
    x = null;
    var data = [{
        'hc-key': 'us-ky',
        value: 1
    },
    {
        'hc-key': 'us-il',
        value: 2
    },
    {
        'hc-key': 'us-in',
        value: 3
    },
    {
        'hc-key': 'us-oh',
        value: 4,
        color: "#145583"
    },
    {
        'hc-key': 'us-pa',
        value: 5
    },
    {
        'hc-key': 'us-wv',
        value: 6
    },
    {
        'hc-key': 'us-va',
        value: 7,
        color: "#145583"
    },
    {
        'hc-key': 'us-md',
        value: 8,
        
    }];
    // Initiate the chart
    Highcharts.mapChart('mineContainer', {
        colors: ['white'],
        chart: {
            plotBorderWidth: 0,
            backgroundColor: null,
            zoomType : null,
            ignoreHiddenSeries: false,
            plotBackgroundColor: null,
            events: {
                load: function () {
                    this.myTooltip = new Highcharts.Tooltip(this, this.options.tooltip);
                },
                 
                click: function () {
                    this.myTooltip.hide();
                },
            }
        },
        colors: ['#004678'],
        title: {
            text: ''
        },
        
        exporting: { enabled: false },
        credits: {
            enabled: false
        },
        mapNavigation: {
            enabled: true,
            buttons: {
                zoomIn: {
                    // the lower the value, the greater the zoom in
                    onclick: function () { this.mapZoom(0.5); }
                },
                zoomOut: {
                    // the higher the value, the greater the zoom out
                    onclick: function () { this.mapZoom(10); }
                },
            },
            buttonOptions: {
                style: {
                    color: "#fff",
                    
                },
                theme: {
                    fill: '#004678',
                    'stroke-width': 1,
                    stroke: '#222222',
                    r: 0,
                    states: {
                        hover: {
                            fill: '#222222'
                        },
                        select: {
                            stroke: '#004678',
                            fill: '#222222'
                        }
                    }
                },
                verticalAlign: 'bottom'
            }
        },
        legend: {
            enabled: true,
            align: 'right',
            verticalAlign: 'bottom',
            layout: 'vertical',
            floating: true,
        },
        tooltip: {
            enabled:false,
            backgroundColor: '#f4f4f4',
            borderRadius: 50,
            headerFormat: '',
            useHTML: true,
            followPointer: false,
            style: {
                pointerEvents: 'auto'
            },
            formatter: function () {
                if (this.point.name == "HAMILTON COMPLEX") {
                    return '<div id="HAMILTON_COMPLEX" class="tooltipCustom"><div class="tooltiptext"><b>' + this.point.name + '</b><p>Hamilton County Coal, LLC operates Mine #1, which is an underground mine located near the city of Dahlgren in Hamilton County, Illinois. Mine #1 utilizes longwall mining techniques to produce high-sulfur coal. The preparation plant at Hamilton County Coal has a throughput capacity of 2,000 tons of raw coal per hour.</p><a class="minelink" target="_blank" href="#">Read More..</a></div><div class="tooltipimage"><img src="/Interactive/newlookandfeel/4098794/images/Hamilton_tooltip.jpg"/></div></div>'
                }
                else if (this.point.name == "RIVER VIEW COMPLEX") {
                    return '<div id="RIVER_VIEW" class="tooltipCustom"><div class="tooltiptext"><b>' + this.point.name + '</b><p>River View Coal, LLC operates the River View mine, which is an underground mine located near Morganfield in Union County, Kentucky. River View utilizes continuous mining units employing room-and-pillar mining techniques to produce high-sulfur coal and is the largest mine of its type in the nation. </p><a class="minelink" target="_blank" href="#">Read More..</a></div><div class="tooltipimage"><img src="/Interactive/newlookandfeel/4098794/images/RiverView_tooltip.jpg"/></div></div>'
                }
                else if (this.point.name == "DOTIKI COMPLEX") {
                    return '<div id="DOTIKI_COMPLEX" class="tooltipCustom"><div class="tooltiptext"><b>' + this.point.name + '</b><p>Webster County Coal, LLC operates Dotiki, which is an underground mine located near the city of Providence in Webster County, Kentucky. Dotiki utilizes continuous mining units employing room-and-pillar mining techniques to produce high-sulfur coal. </p><a class="minelink" target="_blank" href="#">Read More..</a></div><div class="tooltipimage"><img src="/Interactive/newlookandfeel/4098794/images/Dotiki_tooltip.jpg"/></div></div>'
                }
                else if (this.point.name == "GIBSON COMPLEX SOUTH") {
                    return '<div id="GIBSON_COMPLEX_SOUTH" class="tooltipCustom"><div class="tooltiptext"><b>' + this.point.name + '</b><p>Gibson County Coal, LLC operates the Gibson South Mine, located near the city of Princeton in Gibson County, Indiana. Gibson South is an underground mine and utilizes continuous mining units employing room-and-pillar mining techniques to produce low/medium-sulfur coal.</p><a class="minelink" target="_blank" href="#">Read More..</a></div><div class="tooltipimage"><img src="/Interactive/newlookandfeel/4098794/images/GibsonSouth_tooltip.jpg"/></div></div>'
                }
                else if (this.point.name == "GIBSON COMPLEX NORTH") {
                    return '<div id="GIBSON_COMPLEX_NORTH" class="tooltipCustom"><div class="tooltiptext"><b>' + this.point.name + '</b><p>Gibson County Coal, LLC operates the Gibson North mine, an underground mining complex located near the city of Princeton in Gibson County, Indiana. When active, the mine utilizes continuous mining units employing room-and-pillar mining techniques to produce medium-sulfur coal</p><a class="minelink" target="_blank" href="#">Read More..</a></div><div class="tooltipimage"><img src="/Interactive/newlookandfeel/4098794/images/GibsonNorth_tooltip.jpg"/></div></div>'
                }
                else if (this.point.name == "WARRIOR COMPLEX") {
                    return '<div id="WARRIOR_COMPLEX" class="tooltipCustom"><div class="tooltiptext"><b>' + this.point.name + '</b><p>Warrior Coal, LLC operates the Cardinal mine, located near Madisonville in Hopkins County, Kentucky. Warrior utilizes continuous mining units employing room-and-pillar mining techniques. Warrior\'s preparation plant has a throughput capacity of 1,200 tons of raw coal per hour.</p><a class="minelink" target="_blank" href="#">Read More..</a></div><div class="tooltipimage"><img src="/Interactive/newlookandfeel/4098794/images/Warrior_tooltip.jpg"/></div></div>'
                }
                else if (this.point.name == "ONTON") {
                    return '<div id="ONTON" class="tooltipCustom"><div class="tooltiptext"><b>' + this.point.name + '</b><p>Sebree Mining, LLC operated the Onton #9 mine, located near Sebree in Webster County, Kentucky. When active, the Onton #9 mine utilizes continuous miner units employing room-and-pillar mining techniques. The preparation plant has a throughput capacity of 750 tons per hour.</p><a class="minelink" target="_blank" href="#">Read More..</a></div><div class="tooltipimage"><img src="/Interactive/newlookandfeel/4098794/images/Onton_tooltip.jpg"/></div></div>'
                }
                else if (this.point.name == "MT. VERNON") {
                    return '<div id="MT_VERNON" class="tooltipCustom"><div class="tooltiptext"><b>' + this.point.name + '</b><p>The Mt. Vernon Transfer Terminal is a coal-loading terminal on the Ohio River at Mt. Vernon, Indiana. Coal is delivered to Mt. Vernon by both rail and truck. The terminal has a capacity of 8 million tons per year with existing ground storage.</p><a class="minelink" target="_blank" href="#">Read More..</a></div><div class="tooltipimage"><img src="/Interactive/newlookandfeel/4098794/images/MtVernon_tooltip.jpg"/></div></div>'
                }
                else if (this.point.name == "MC MINING") {
                    return '<div id="MC_MINING" class="tooltipCustom"><div class="tooltiptext"><b>' + this.point.name + '</b><p>Excel Mining, LLC operates MC Mining, located near the city of Pikeville in Pike County, Kentucky. The underground operation utilizes continuous mining units employing room-and-pillar mining techniques to produce low-sulfur coal. </p><a class="minelink" target="_blank" href="#">Read More..</a></div><div class="tooltipimage"><img src="/Interactive/newlookandfeel/4098794/images/MC_Mining_tooltip.jpg"/></div></div>'
                }
                else if (this.point.name == "TUNNEL RIDGE") {
                    return '<div id="TUNNEL_RIDGE" class="tooltipCustom"><div class="tooltiptext"><b>' + this.point.name + '</b><p>Tunnel Ridge, LLC operates the Tunnel Ridge Mine, located near Wheeling, West Virginia. Tunnel Ridge utilizes the longwall mining technique to produce medium/high-sulfur coal. The Tunnel Ridge preparation plant has a throughput capacity of 2,000 tons of raw coal per hour.</p><a class="minelink" target="_blank" href="#">Read More..</a></div><div class="tooltipimage"><img src="/Interactive/newlookandfeel/4098794/images/Tunnel_Ridge_tooltip.jpg"/></div></div>'
                }
                else {
                    return '<div id="METTIKI" class="tooltipCustom"><div class="tooltiptext"><b>' + this.point.name + '</b><p>Mettiki Coal (WV), LLC operated the Mountain View Mine, located in Tucker County, West Virginia. The Mountain View mine utilizes the longwall mining technique to produce medium-sulfur coal. Mettiki is capable of producing a coal for both the steam coal and metallurgical coal markets.</p><a class="minelink" target="_blank" href="#">Read More..</a></div><div class="tooltipimage"><img src="/Interactive/newlookandfeel/4098794/images/Mettiki_tooltip.jpg"/></div></div>'
                }
            }
        },
        plotOptions: {
            series: {
                borderColor: '#222222',
                stickyTracking: false,
                states: {
                    hover: {
                        color: '#222222',
                        borderColor: '#004678'
                    }
                },
                
                           
            }
        },
        
        series: [{
            data: data,
            mapData: Highcharts.maps['countries/us/us-all'],
            joinBy: 'hc-key',
            allAreas: false,
            name: '',
            enableMouseTracking: true,
            showInLegend: false,
            dataLabels: {
                style: { "textOutline": "0", "color": "#3381b9", "fontWeight": "normal", "fontStyle": "italic" },
                allowOverlap: true,
                enabled: true,

                formatter: function () {
                    if (this.point.value) {
                        return this.point.name;
                    }
                }
            },
        }, {
            // Specify points using lat/lon
            type: 'mappoint',
            name: 'ILLINOIS BASIN',
            color: '#ffffff',
            data: [{
                name: 'HAMILTON COMPLEX',
                lat: 38.170900,
                lon: -88.602297
            }, {
                name: 'RIVER VIEW COMPLEX',
                lat: 37.742983, 
                lon: -87.887961
            }, {
                name: 'DOTIKI COMPLEX',
                lat: 37.453354, 
                lon: -87.774104
            }, {
                name: 'GIBSON COMPLEX SOUTH',
                lat: 40.794441, 
                lon: -87.005766
            }, {
                name: 'GIBSON COMPLEX NORTH',
                lat: 40.794441,
                lon: -87.005766
            }, {
                name: 'WARRIOR COMPLEX',
                lat: 37.328118, 
                lon: -87.498910
            }, {
                name: 'ONTON',
                lat: 37.607122,
                lon: -87.528681
            }, {
                name: 'MT. VERNON',
                lat: 37.922751,
                lon: -87.864726
            }],
            dataLabels: {
                style: {
                    "color": "white", "fontWeight": "normal", "textOutline": "0"
                },
               
                allowOverlap: false,
                color: 'white',
                align: 'left',
                shadow: false,
                crop:true,
                y: 10,
                x: 5,
            },
            events: {
                click: function (evt) {
                    this.chart.myTooltip.refresh(evt.point, evt);
                },
                mouseOver: function () {
                    this.chart.myTooltip.hide();
                },


            }
            
        }, {
            // Specify points using lat/lon
            type: 'mappoint',
            name: 'APPALACHIA',
            color: '#fed136',
            data: [{
                name: 'MC MINING',
                lat: 37.591513,
                lon: -82.492947,
                color: '#fed136',
            }, {
                name: 'TUNNEL RIDGE',
                lat: 40.118898,
                lon: -80.587843,
                color: '#fed136',
            }, {
                name: 'METTIKI',
                lat: 39.263252,
                lon: -79.428611,
                color: '#fed136',

            }],
            dataLabels: {
                style: {
                    "color": "white", "fontWeight": "normal", "textOutline": "0"
                },

                allowOverlap: false,
                color: '#fed136',
                align: 'left',
                shadow: false,
                crop: false,
                y: 10,
                x: 5,
            },
            events: {
                click: function (evt) {
                    this.chart.myTooltip.refresh(evt.point, evt);
                },
                mouseOver: function () {
                    this.chart.myTooltip.hide();
                },


            }

        }]
    });

});