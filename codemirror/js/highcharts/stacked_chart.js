Highcharts.chart('stacked_chart_container', {
//Set colors of columns
colors: ['rgb(214,0,42)', 'rgb(60,60,59)', 'rgb(167,152,134)'],

//Define chart type
chart: {
    type: 'column',
    height:500
},

//Customize title
title: {
    text: 'Stacked column chart',
    useHTML: true,
    style: {
        color: 'rgb(255,255,255)',
        fontSize: '30px',
        backgroundColor: "rgb(214,0,42)",
        border: '2px solid rgb(214,0,42)',
        lineHeight: '35px',
        padding: '5px 30px'
    }
},

//Define xAxis categories and style xAxis labels
xAxis: {
    categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas'],
    labels: {
        style: {
            color: '#000000',
            fontSize: '14px',
        },
        autoRotation: [0, -20, -30, -40, -50, -60, -70, -80, -90],
    },
},

//Define yAxis categories and style yAxis labels
yAxis: {
    min: 0,
    title: {
        text: 'Total fruit consumption'
    },
    stackLabels: {
        enabled: true,
        style: {
            color: '#000000',
            fontSize: '14px',
        }
    },
    labels: {
        style: {
            color: '#000000',
            fontSize: '14px',
        },
        autoRotation: [0, -20, -30, -40, -50, -60, -70, -80, -90],
    },
},

//Define Legends categories and set position & styling
legend: {
    align: 'right',
    x: -30,
    verticalAlign: 'top',
    y: 25,
    floating: true,
    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
    borderColor: '#CCC',
    borderWidth: 1,
    shadow: false
},

//Customize tooltip
tooltip: {
    headerFormat: '<b>{point.x}</b><br/>',
    pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
},

//Customize ploting
plotOptions: {
    column: {
        stacking: 'normal',
        //Set color of labels of columns
        dataLabels: {
            enabled: true,
            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
        }
    }
},

//Data to draw columns
series: [{
    name: 'John',
    data: [5, 3, 4, 7, 2]
}, {
    name: 'Jane',
    data: [2, 2, 3, 2, 1]
}, {
    name: 'Joe',
    data: [3, 4, 4, 2, 5]
}]
});
